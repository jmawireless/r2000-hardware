/**
 * 3Z Telecom Inc
 * 
 * Author: Faisal Sikder
 * Date Created: June 19, 2018
 * Email: fsikder@3ztelecom.com 
 * 
 * 
 * Please install the Arduino IDE to run the code using an Arduino. Neverthless, the code may be opened
 * Using a text editor to see the format for USART communication if using another uController.
 *
 *
 * This sample example demonstrate how to configure the AMP and 
 * read Azimuth, tilt, roll, temperature, humidity and status from
 * the AMP.
 * 
 * Important node: 
 * 10 ms delay between each command is required to process the
 * command correclty by the AMP.
 * 
 */