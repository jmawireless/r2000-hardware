/**
 * 3Z Telecom Inc
 * 
 * Author: Faisal Sikder
 * Date Created: June 19, 2018
 * Email: fsikder@3ztelecom.com 
 * 
 * 
 * This sample example demonstrate how to configure the AMP and 
 * read Azimuth, tilt, roll, temperature, humidity and status from
 * the AMP.
 * 
 * Important node: 
 * 10 ms delay between each command is required to process the
 * command correclty by the AMP.
 * 
 */
 
#include <string.h>
#include <stdlib.h>
 
uint8_t read_data_now = 0;

// the setup function runs once when you press reset or power the board
void AMP_setup(){
  Serial.println("Setting the AMP parameter values..............");
  delay(10);
  // setup the target azimuth # 60 degree 
  Serial2.write("60a\n");
  delay(10);
  // setup the target tilt # 0 degree
  Serial2.write("0t\n");
  delay(10);
  // setup the target roll # 0 degree
  Serial2.write("0r\n");
  delay(10);
  // setup the azimuth alarm delta limit # 5 degree before set alarm
  Serial2.write("5b\n");
  delay(10);
  // setup the tilt alarm delta limit  # 2 degree before set alarm
  Serial2.write("2u\n");
  delay(10);
  // setup the roll alarm delta limit # 2 degree before set alarm
  Serial2.write("2s\n");
  delay(10);
  // setup the front direction # +Z is the antenna facing direction # value 4
  Serial2.write("4v\n");  
  // When direction change is set. AMP restart the whole calculation process
  // So user should put at least 50ms delay before any further command is sent.
  delay(50);  
  // setup the ALARM update period # per seconds # value 0
  Serial2.write("0o\n");  
  delay(10);
  // set up the average array length # average array length 20 samples
  Serial2.write("20h\n");
  delay(10);
  // setup the AMP sample update period every 150 ms which is 1
  Serial2.write("1g\n");
  delay(10);
  // save value into the flash register
  // this will save the value in the case of power loss
  // saving require more time then normal operation
  Serial2.write("f\n");
  delay(20);
  // this is save the offset value and other values
  // in the case of power loss we will be able to retrive values
  Serial2.write("0y\n");
  delay(20);
  Serial.println("End of setup ..............");
  delay(100);
}

void AMP_calibrate_and_commission(){
  Serial.println("Sent AMP calibration and commision command ..............");
  // 'c' command is a toggle command for calibration and commission
  // if AMP is not commission then 'c' command will calibrate and comission the AMP
  // if AMP is ALREADY commission then 'c' command will de-comission the AMP
  Serial2.write("c\n");
  delay(20);
  
  Serial.println("Check all the configuration values of AMP");
  delay(10);
  Serial2.write("x\n");
  delay(20);
}

void get_single_reading(String s, int pos, int len, char *data){
  memcpy(data,&s[pos],5);
  data[5] = '\0';
}

void Check_Status_Register(int RegVal){
  uint8_t mask = 0x80;
  Serial.print("(");
  for(int i = 0; i < 8; i++){
    if(mask & RegVal){
      Serial.print("1");
    }else{
      Serial.print("0");
    }
    mask >>= 1;
  }
  Serial.print(")");
}

void Process_AMP_Data_Output(String s){
  //Data Stream   -----,-----,-----,-----,-----,-----
  //Data Loc      0   4,6  10,12 16,18 22,24 28,30
  char singleReading[6];
  // azimuth is from 0-4
  get_single_reading(s, 0, 5, singleReading);
  Serial.print("Azimuth: ");
  Serial.print(singleReading);
  // Tilt is from 6-10
  get_single_reading(s, 6, 5, singleReading);
  Serial.print(", Tilt: ");
  Serial.print(singleReading);
  // Roll is from 12-116
  get_single_reading(s, 12, 5, singleReading);
  Serial.print(", Roll: ");
  Serial.print(singleReading);
  // Temperature is from 18-22
  get_single_reading(s, 18, 5, singleReading);
  Serial.print(", Temp: ");
  Serial.print(singleReading);
  // Humidity is from 24 - 28
  get_single_reading(s, 24, 5, singleReading);
  Serial.print(", Humidity: ");
  Serial.print(singleReading);
  // Status Register is from 30 - 34
  get_single_reading(s, 30, 5, singleReading);
  Serial.print(", Status Reg: ");
  Serial.print(singleReading);
  Check_Status_Register(atoi(singleReading));
  Serial.println("");
}

void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(13, OUTPUT);
  //Wire1.begin();        // join i2c bus (address optional for master)
  Serial.begin(115200);  // start serial for output
  // AMP is connected with ARDUINO DUE PIN 16 (TX) and PIN 17 (RX)
  // AMP GND <---> DUE GND
  // AMP RX  <---> DUE TX
  // AMP TX  <---> DUE RX 
  Serial2.begin(115200);  
  // setup
  AMP_setup();
  // calibrate and commission
  AMP_calibrate_and_commission();
}

// the loop function runs over and over again forever
void loop() {
  // send command to read data using command 'p'
  Serial2.write("p\n");

  // this will do the blinking
  digitalWrite(13, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);
  digitalWrite(13, LOW);   // turn the LED on (HIGH is the voltage level)
  delay(1000);
}

// this event will be called when AMP sends any data through the usart 
// Arduino DUE receives the data in the RX2 (PIN 17) and executes this event
void serialEvent2(){
   if(Serial2.available()){
    // AMP data output is end with a new line character '\n'
    // We will read data untill we find a new line
    String s = Serial2.readStringUntil('\n');
     if(!read_data_now){
      Serial.println(s);
      Serial.println("Now read data");
      read_data_now = 1;
    }else{
      Process_AMP_Data_Output(s);
    }
  }
}


/************************* Copyright @ 3Z Telecom Inc ********************************/


