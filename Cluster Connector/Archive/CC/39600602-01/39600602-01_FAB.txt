﻿Part Number: 39600602-01	
Revision: 1	

Layers: 4
Arrays: No
Dimensions: 2.68” x 1.08” (Non uniform edges) 

Route and Retain: No
Scoring: No
Finished Thickness: 0.062 inches +/-0.007 
Min Hole Size: 0.010 inches
Number Holes: 189 - PTH, 6 - NPTH 
Number Drill Sizes: 6
Buried/Blind Vias: No
Material: FR4
Surface Finish: Imersion Gold (ENIG)
Gold Fingers: No
Min Trace Width: 0.008 inches
Min Space: 0.008 inches
Copper Weight: 1 Oz
Outer Layer Copper Finish: 1 Oz
Solder Mask: Both Sides, LPI, Blue, Semi-Gloss
Silk Screen: Both Sides, White
Internal Slots/Cutouts: 1
Edge Plating: No
Controlled Impedance: No
Controlled Dielectric: No
Counter Bore: No
Counter Sink: No
Fill via : No
Conformal Coat: Yes
Via type: Class 3


Layer Stackup:

---------- Layer 1 (1 Oz Cu Finished)--------
           Core FR4 
---------- Layer 2(1 Oz Cu) ----------
           Prepreg FR4 
---------- Layer 3 (1 Oz Cu) ----------
           Core FR4 
---------- Layer 4 (1 Oz Cu Finished) ----------

Gerber Files: 

copper_top_l1.gbr         		Layer 1 (Top)
soldermask_top.gbr    			Layer 1 (Top) Solder Mask
silkscreen_top.gbr    			Layer 1 (Top) Silk Screen
solderpaste_top.gbr         		Layer 1 (Top) Solder Paste
copper_inner_l2.gbr           		Layer 2 
copper_inner_l3.gbr           		Layer 3 
copper_bottom_l4.gbr      		Layer 4 (Bottom)
soldermask_bottom.gbr 			Layer 4 (Bottom) Solder Mask
silkscreen_bottom.gbr 			Layer 4 (Bottom) Silk Screen
solderpaste_bottom.gbr      		Layer 4 (Bottom) Solder Paste
profile.gbr				Dimension layer, NPTH and outline
