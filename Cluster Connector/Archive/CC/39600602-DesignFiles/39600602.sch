<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.5.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting keepoldvectorfont="yes"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="5" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="no" active="no"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="no" active="no"/>
<layer number="103" name="tMap" color="7" fill="1" visible="no" active="no"/>
<layer number="104" name="Name" color="7" fill="1" visible="no" active="no"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="no" active="no"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="no" active="no"/>
<layer number="107" name="Crop" color="7" fill="1" visible="no" active="no"/>
<layer number="108" name="fp8" color="7" fill="1" visible="yes" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="yes" active="yes"/>
<layer number="110" name="110" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="111" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="7" fill="1" visible="yes" active="yes"/>
<layer number="114" name="FRNTMAAT1" color="7" fill="1" visible="no" active="no"/>
<layer number="115" name="FRNTMAAT2" color="7" fill="1" visible="no" active="no"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="no" active="no"/>
<layer number="117" name="BACKMAAT1" color="7" fill="1" visible="no" active="no"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="yes" active="yes"/>
<layer number="119" name="KAP_TEKEN" color="7" fill="1" visible="no" active="no"/>
<layer number="120" name="KAP_MAAT1" color="7" fill="1" visible="no" active="no"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="no" active="no"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="no" active="no"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="top_silk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="130" name="SMDSTROOK" color="7" fill="1" visible="no" active="no"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="133" name="bottom_silk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="DrillLegend" color="7" fill="1" visible="no" active="no"/>
<layer number="145" name="DrillLegend_01-16" color="2" fill="9" visible="yes" active="yes"/>
<layer number="146" name="DrillLegend_01-20" color="3" fill="9" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="14" fill="1" visible="no" active="no"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="no"/>
<layer number="201" name="201bmp" color="2" fill="1" visible="no" active="no"/>
<layer number="202" name="202bmp" color="3" fill="1" visible="no" active="no"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="no" active="no"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="no" active="no"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="no" active="no"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="no" active="no"/>
<layer number="207" name="207bmp" color="15" fill="10" visible="no" active="no"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="no" active="no"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="Eagle3D_PG1" color="7" fill="1" visible="no" active="no"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="no"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="OrgLBR" color="13" fill="1" visible="no" active="no"/>
<layer number="255" name="Accent" color="7" fill="1" visible="no" active="no"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="frames" urn="urn:adsk.eagle:library:229">
<description>&lt;b&gt;Frames for Sheet and Layout&lt;/b&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="A4L-LOC-JMA">
<wire x1="256.54" y1="3.81" x2="256.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="256.54" y1="8.89" x2="256.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="256.54" y1="13.97" x2="256.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="256.54" y1="19.05" x2="256.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="161.29" y1="3.81" x2="161.29" y2="24.13" width="0.1016" layer="94"/>
<wire x1="161.29" y1="24.13" x2="215.265" y2="24.13" width="0.1016" layer="94"/>
<wire x1="215.265" y1="24.13" x2="256.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="246.38" y1="3.81" x2="246.38" y2="8.89" width="0.1016" layer="94"/>
<wire x1="246.38" y1="8.89" x2="256.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="246.38" y1="8.89" x2="215.265" y2="8.89" width="0.1016" layer="94"/>
<wire x1="215.265" y1="8.89" x2="215.265" y2="3.81" width="0.1016" layer="94"/>
<wire x1="215.265" y1="8.89" x2="215.265" y2="13.97" width="0.1016" layer="94"/>
<wire x1="215.265" y1="13.97" x2="256.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="215.265" y1="13.97" x2="215.265" y2="19.05" width="0.1016" layer="94"/>
<wire x1="215.265" y1="19.05" x2="256.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="215.265" y1="19.05" x2="215.265" y2="24.13" width="0.1016" layer="94"/>
<text x="217.17" y="15.24" size="2.54" layer="94">&gt;DRAWING_NAME</text>
<text x="217.17" y="10.16" size="2.286" layer="94">&gt;LAST_DATE_TIME</text>
<text x="230.505" y="5.08" size="2.54" layer="94">&gt;SHEET</text>
<text x="216.916" y="4.953" size="2.54" layer="94">Sheet:</text>
<frame x1="0" y1="0" x2="260.35" y2="179.07" columns="6" rows="4" layer="94"/>
<text x="162.56" y="15.24" size="2.54" layer="94" ratio="15">Proprietory Information
JMA WIRELESS </text>
<text x="217.17" y="20.32" size="2.54" layer="94" font="fixed">RGANDHI</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="A4L-LOC-JMA" prefix="FRAME">
<gates>
<gate name="G$1" symbol="A4L-LOC-JMA" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="JMA_LBR" urn="urn:adsk.eagle:library:15620506">
<packages>
<package name="0.5MM_FIDUCIAL" urn="urn:adsk.eagle:footprint:15620619/1" library_version="1">
<wire x1="0" y1="1" x2="0" y2="-1" width="0.0508" layer="51"/>
<wire x1="-1" y1="0" x2="1" y2="0" width="0.0508" layer="51"/>
<circle x="0" y="0" radius="0.5" width="0" layer="1"/>
<circle x="0" y="0" radius="0.889" width="0" layer="29"/>
<circle x="0" y="0" radius="1" width="0.0508" layer="51"/>
<circle x="0" y="0" radius="1.0307" width="0" layer="41"/>
<circle x="0" y="0" radius="0.5" width="0.0508" layer="51"/>
</package>
</packages>
<packages3d>
<package3d name="0.5MM_FIDUCIAL" urn="urn:adsk.eagle:package:15620691/1" type="box" library_version="1">
<packageinstances>
<packageinstance name="0.5MM_FIDUCIAL"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="NC" urn="urn:adsk.eagle:symbol:15620544/1" library_version="1">
<wire x1="-0.635" y1="0.635" x2="0.635" y2="-0.635" width="0.254" layer="94"/>
<wire x1="0.635" y1="0.635" x2="-0.635" y2="-0.635" width="0.254" layer="94"/>
</symbol>
<symbol name="FIDUCIAL" urn="urn:adsk.eagle:symbol:15620531/1" library_version="1">
<circle x="0" y="0" radius="3.81" width="0" layer="94"/>
<circle x="0" y="0" radius="6.35" width="0.254" layer="94"/>
<text x="-4.826" y="13.97" size="1.778" layer="125">&gt;NAME</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="NC" urn="urn:adsk.eagle:component:15620740/1" prefix="NC" library_version="1">
<gates>
<gate name="G$1" symbol="NC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="0.5MM_FIDUCIAL" urn="urn:adsk.eagle:component:15620763/1" prefix="F" library_version="1">
<gates>
<gate name="G$1" symbol="FIDUCIAL" x="0" y="0"/>
</gates>
<devices>
<device name="" package="0.5MM_FIDUCIAL">
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15620691/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply2" urn="urn:adsk.eagle:library:372">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
Please keep in mind, that these devices are necessary for the
automatic wiring of the supply signals.&lt;p&gt;
The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND" urn="urn:adsk.eagle:symbol:26990/1" library_version="2">
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="-1.27" y2="0" width="0.254" layer="94"/>
<text x="-1.905" y="-3.175" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" urn="urn:adsk.eagle:component:27037/1" prefix="SUPPLY" library_version="2">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="GND" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1" urn="urn:adsk.eagle:library:371">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="AGND" urn="urn:adsk.eagle:symbol:26949/1" library_version="1">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<wire x1="-1.0922" y1="-0.508" x2="1.0922" y2="-0.508" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="AGND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="AGND" urn="urn:adsk.eagle:component:26977/1" prefix="AGND" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="VR1" symbol="AGND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="JMA_LBR_v2" urn="urn:adsk.eagle:library:8413580">
<packages>
<package name="GDTMELF5875" urn="urn:adsk.eagle:footprint:3890113/3" library_version="68">
<description>MELF, 5.865 mm length, 7.5 mm diameter
&lt;p&gt;MELF Resistor package with 5.865 mm length and 7.5 mm diameter&lt;/p&gt;</description>
<wire x1="1.7525" y1="4.1693" x2="-1.9303" y2="4.1693" width="0.12" layer="21"/>
<wire x1="-1.9303" y1="-4.1947" x2="1.7525" y2="-4.1947" width="0.12" layer="21"/>
<wire x1="3.015" y1="-3.85" x2="-3.015" y2="-3.85" width="0.12" layer="51"/>
<wire x1="-3.015" y1="-3.85" x2="-3.015" y2="3.85" width="0.12" layer="51"/>
<wire x1="-3.015" y1="3.85" x2="3.015" y2="3.85" width="0.12" layer="51"/>
<wire x1="3.015" y1="3.85" x2="3.015" y2="-3.85" width="0.12" layer="51"/>
<text x="0" y="4.8567" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-4.8567" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
<smd name="1" x="-3.016" y="0" dx="8.25" dy="1.78" layer="1" rot="R90" thermals="no"/>
<smd name="2" x="2.824" y="0" dx="8.25" dy="1.78" layer="1" rot="R90" thermals="no"/>
</package>
<package name="CAPC1608X55" urn="urn:adsk.eagle:footprint:3793129/2" library_version="68">
<description>CHIP, 1.6 X 0.8 X 0.55 mm body
&lt;p&gt;CHIP package with body size 1.6 X 0.8 X 0.55 mm&lt;/p&gt;</description>
<wire x1="0.85" y1="0.6516" x2="-0.85" y2="0.6516" width="0.12" layer="21"/>
<wire x1="0.85" y1="-0.6516" x2="-0.85" y2="-0.6516" width="0.12" layer="21"/>
<wire x1="0.85" y1="-0.45" x2="-0.85" y2="-0.45" width="0.12" layer="51"/>
<wire x1="-0.85" y1="-0.45" x2="-0.85" y2="0.45" width="0.12" layer="51"/>
<wire x1="-0.85" y1="0.45" x2="0.85" y2="0.45" width="0.12" layer="51"/>
<wire x1="0.85" y1="0.45" x2="0.85" y2="-0.45" width="0.12" layer="51"/>
<smd name="1" x="-0.7704" y="0" dx="0.8884" dy="0.9291" layer="1"/>
<smd name="2" x="0.7704" y="0" dx="0.8884" dy="0.9291" layer="1"/>
<text x="0" y="1.4136" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.4136" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC1005X60" urn="urn:adsk.eagle:footprint:3793110/1" library_version="68">
<description>CHIP, 1.025 X 0.525 X 0.6 mm body
&lt;p&gt;CHIP package with body size 1.025 X 0.525 X 0.6 mm&lt;/p&gt;</description>
<wire x1="0.55" y1="0.6325" x2="-0.55" y2="0.6325" width="0.12" layer="21"/>
<wire x1="0.55" y1="-0.6325" x2="-0.55" y2="-0.6325" width="0.12" layer="21"/>
<wire x1="0.55" y1="-0.3" x2="-0.55" y2="-0.3" width="0.12" layer="51"/>
<wire x1="-0.55" y1="-0.3" x2="-0.55" y2="0.3" width="0.12" layer="51"/>
<wire x1="-0.55" y1="0.3" x2="0.55" y2="0.3" width="0.12" layer="51"/>
<wire x1="0.55" y1="0.3" x2="0.55" y2="-0.3" width="0.12" layer="51"/>
<smd name="1" x="-0.4875" y="0" dx="0.5621" dy="0.6371" layer="1"/>
<smd name="2" x="0.4875" y="0" dx="0.5621" dy="0.6371" layer="1"/>
<text x="0" y="1.2675" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.2675" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC1220X107" urn="urn:adsk.eagle:footprint:3793123/1" library_version="68">
<description>CHIP, 1.25 X 2 X 1.07 mm body
&lt;p&gt;CHIP package with body size 1.25 X 2 X 1.07 mm&lt;/p&gt;</description>
<wire x1="0.725" y1="1.4217" x2="-0.725" y2="1.4217" width="0.12" layer="21"/>
<wire x1="0.725" y1="-1.4217" x2="-0.725" y2="-1.4217" width="0.12" layer="21"/>
<wire x1="0.725" y1="-1.1" x2="-0.725" y2="-1.1" width="0.12" layer="51"/>
<wire x1="-0.725" y1="-1.1" x2="-0.725" y2="1.1" width="0.12" layer="51"/>
<wire x1="-0.725" y1="1.1" x2="0.725" y2="1.1" width="0.12" layer="51"/>
<wire x1="0.725" y1="1.1" x2="0.725" y2="-1.1" width="0.12" layer="51"/>
<smd name="1" x="-0.552" y="0" dx="0.7614" dy="2.2153" layer="1"/>
<smd name="2" x="0.552" y="0" dx="0.7614" dy="2.2153" layer="1"/>
<text x="0" y="2.0567" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.0567" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC1632X168" urn="urn:adsk.eagle:footprint:3793133/1" library_version="68">
<description>CHIP, 1.6 X 3.2 X 1.68 mm body
&lt;p&gt;CHIP package with body size 1.6 X 3.2 X 1.68 mm&lt;/p&gt;</description>
<wire x1="0.9" y1="2.0217" x2="-0.9" y2="2.0217" width="0.12" layer="21"/>
<wire x1="0.9" y1="-2.0217" x2="-0.9" y2="-2.0217" width="0.12" layer="21"/>
<wire x1="0.9" y1="-1.7" x2="-0.9" y2="-1.7" width="0.12" layer="51"/>
<wire x1="-0.9" y1="-1.7" x2="-0.9" y2="1.7" width="0.12" layer="51"/>
<wire x1="-0.9" y1="1.7" x2="0.9" y2="1.7" width="0.12" layer="51"/>
<wire x1="0.9" y1="1.7" x2="0.9" y2="-1.7" width="0.12" layer="51"/>
<smd name="1" x="-0.786" y="0" dx="0.9434" dy="3.4153" layer="1"/>
<smd name="2" x="0.786" y="0" dx="0.9434" dy="3.4153" layer="1"/>
<text x="0" y="2.6567" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.6567" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC2012X127" urn="urn:adsk.eagle:footprint:3793140/1" library_version="68">
<description>CHIP, 2.01 X 1.25 X 1.27 mm body
&lt;p&gt;CHIP package with body size 2.01 X 1.25 X 1.27 mm&lt;/p&gt;</description>
<wire x1="1.105" y1="1.0467" x2="-1.105" y2="1.0467" width="0.12" layer="21"/>
<wire x1="1.105" y1="-1.0467" x2="-1.105" y2="-1.0467" width="0.12" layer="21"/>
<wire x1="1.105" y1="-0.725" x2="-1.105" y2="-0.725" width="0.12" layer="51"/>
<wire x1="-1.105" y1="-0.725" x2="-1.105" y2="0.725" width="0.12" layer="51"/>
<wire x1="-1.105" y1="0.725" x2="1.105" y2="0.725" width="0.12" layer="51"/>
<wire x1="1.105" y1="0.725" x2="1.105" y2="-0.725" width="0.12" layer="51"/>
<smd name="1" x="-0.8804" y="0" dx="1.1646" dy="1.4653" layer="1"/>
<smd name="2" x="0.8804" y="0" dx="1.1646" dy="1.4653" layer="1"/>
<text x="0" y="1.6817" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.6817" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC3215X168" urn="urn:adsk.eagle:footprint:3793146/1" library_version="68">
<description>CHIP, 3.2 X 1.5 X 1.68 mm body
&lt;p&gt;CHIP package with body size 3.2 X 1.5 X 1.68 mm&lt;/p&gt;</description>
<wire x1="1.7" y1="1.1286" x2="-1.7" y2="1.1286" width="0.12" layer="21"/>
<wire x1="1.7" y1="-1.1286" x2="-1.7" y2="-1.1286" width="0.12" layer="21"/>
<wire x1="1.7" y1="-0.8" x2="-1.7" y2="-0.8" width="0.12" layer="51"/>
<wire x1="-1.7" y1="-0.8" x2="-1.7" y2="0.8" width="0.12" layer="51"/>
<wire x1="-1.7" y1="0.8" x2="1.7" y2="0.8" width="0.12" layer="51"/>
<wire x1="1.7" y1="0.8" x2="1.7" y2="-0.8" width="0.12" layer="51"/>
<smd name="1" x="-1.4913" y="0" dx="1.1327" dy="1.6291" layer="1"/>
<smd name="2" x="1.4913" y="0" dx="1.1327" dy="1.6291" layer="1"/>
<text x="0" y="1.7636" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.7636" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC3225X168" urn="urn:adsk.eagle:footprint:3793150/1" library_version="68">
<description>CHIP, 3.2 X 2.5 X 1.68 mm body
&lt;p&gt;CHIP package with body size 3.2 X 2.5 X 1.68 mm&lt;/p&gt;</description>
<wire x1="1.7" y1="1.6717" x2="-1.7" y2="1.6717" width="0.12" layer="21"/>
<wire x1="1.7" y1="-1.6717" x2="-1.7" y2="-1.6717" width="0.12" layer="21"/>
<wire x1="1.7" y1="-1.35" x2="-1.7" y2="-1.35" width="0.12" layer="51"/>
<wire x1="-1.7" y1="-1.35" x2="-1.7" y2="1.35" width="0.12" layer="51"/>
<wire x1="-1.7" y1="1.35" x2="1.7" y2="1.35" width="0.12" layer="51"/>
<wire x1="1.7" y1="1.35" x2="1.7" y2="-1.35" width="0.12" layer="51"/>
<smd name="1" x="-1.4913" y="0" dx="1.1327" dy="2.7153" layer="1"/>
<smd name="2" x="1.4913" y="0" dx="1.1327" dy="2.7153" layer="1"/>
<text x="0" y="2.3067" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.3067" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC4520X168" urn="urn:adsk.eagle:footprint:3793156/1" library_version="68">
<description>CHIP, 4.5 X 2.03 X 1.68 mm body
&lt;p&gt;CHIP package with body size 4.5 X 2.03 X 1.68 mm&lt;/p&gt;</description>
<wire x1="2.375" y1="1.4602" x2="-2.375" y2="1.4602" width="0.12" layer="21"/>
<wire x1="2.375" y1="-1.4602" x2="-2.375" y2="-1.4602" width="0.12" layer="21"/>
<wire x1="2.375" y1="-1.14" x2="-2.375" y2="-1.14" width="0.12" layer="51"/>
<wire x1="-2.375" y1="-1.14" x2="-2.375" y2="1.14" width="0.12" layer="51"/>
<wire x1="-2.375" y1="1.14" x2="2.375" y2="1.14" width="0.12" layer="51"/>
<wire x1="2.375" y1="1.14" x2="2.375" y2="-1.14" width="0.12" layer="51"/>
<smd name="1" x="-2.1266" y="0" dx="1.2091" dy="2.2923" layer="1"/>
<smd name="2" x="2.1266" y="0" dx="1.2091" dy="2.2923" layer="1"/>
<text x="0" y="2.0952" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.0952" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC4532X218" urn="urn:adsk.eagle:footprint:3793162/1" library_version="68">
<description>CHIP, 4.5 X 3.2 X 2.18 mm body
&lt;p&gt;CHIP package with body size 4.5 X 3.2 X 2.18 mm&lt;/p&gt;</description>
<wire x1="2.375" y1="2.0217" x2="-2.375" y2="2.0217" width="0.12" layer="21"/>
<wire x1="2.375" y1="-2.0217" x2="-2.375" y2="-2.0217" width="0.12" layer="21"/>
<wire x1="2.375" y1="-1.7" x2="-2.375" y2="-1.7" width="0.12" layer="51"/>
<wire x1="-2.375" y1="-1.7" x2="-2.375" y2="1.7" width="0.12" layer="51"/>
<wire x1="-2.375" y1="1.7" x2="2.375" y2="1.7" width="0.12" layer="51"/>
<wire x1="2.375" y1="1.7" x2="2.375" y2="-1.7" width="0.12" layer="51"/>
<smd name="1" x="-2.1266" y="0" dx="1.2091" dy="3.4153" layer="1"/>
<smd name="2" x="2.1266" y="0" dx="1.2091" dy="3.4153" layer="1"/>
<text x="0" y="2.6567" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.6567" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC4564X218" urn="urn:adsk.eagle:footprint:3793169/1" library_version="68">
<description>CHIP, 4.5 X 6.4 X 2.18 mm body
&lt;p&gt;CHIP package with body size 4.5 X 6.4 X 2.18 mm&lt;/p&gt;</description>
<wire x1="2.375" y1="3.6452" x2="-2.375" y2="3.6452" width="0.12" layer="21"/>
<wire x1="2.375" y1="-3.6452" x2="-2.375" y2="-3.6452" width="0.12" layer="21"/>
<wire x1="2.375" y1="-3.325" x2="-2.375" y2="-3.325" width="0.12" layer="51"/>
<wire x1="-2.375" y1="-3.325" x2="-2.375" y2="3.325" width="0.12" layer="51"/>
<wire x1="-2.375" y1="3.325" x2="2.375" y2="3.325" width="0.12" layer="51"/>
<wire x1="2.375" y1="3.325" x2="2.375" y2="-3.325" width="0.12" layer="51"/>
<smd name="1" x="-2.1266" y="0" dx="1.2091" dy="6.6623" layer="1"/>
<smd name="2" x="2.1266" y="0" dx="1.2091" dy="6.6623" layer="1"/>
<text x="0" y="4.2802" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-4.2802" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC5650X218" urn="urn:adsk.eagle:footprint:3793172/1" library_version="68">
<description>CHIP, 5.64 X 5.08 X 2.18 mm body
&lt;p&gt;CHIP package with body size 5.64 X 5.08 X 2.18 mm&lt;/p&gt;</description>
<wire x1="2.895" y1="2.9852" x2="-2.895" y2="2.9852" width="0.12" layer="21"/>
<wire x1="2.895" y1="-2.9852" x2="-2.895" y2="-2.9852" width="0.12" layer="21"/>
<wire x1="2.895" y1="-2.665" x2="-2.895" y2="-2.665" width="0.12" layer="51"/>
<wire x1="-2.895" y1="-2.665" x2="-2.895" y2="2.665" width="0.12" layer="51"/>
<wire x1="-2.895" y1="2.665" x2="2.895" y2="2.665" width="0.12" layer="51"/>
<wire x1="2.895" y1="2.665" x2="2.895" y2="-2.665" width="0.12" layer="51"/>
<smd name="1" x="-2.6854" y="0" dx="1.1393" dy="5.3423" layer="1"/>
<smd name="2" x="2.6854" y="0" dx="1.1393" dy="5.3423" layer="1"/>
<text x="0" y="3.6202" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-3.6202" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC5563X218" urn="urn:adsk.eagle:footprint:3793175/1" library_version="68">
<description>CHIP, 5.59 X 6.35 X 2.18 mm body
&lt;p&gt;CHIP package with body size 5.59 X 6.35 X 2.18 mm&lt;/p&gt;</description>
<wire x1="2.92" y1="3.6202" x2="-2.92" y2="3.6202" width="0.12" layer="21"/>
<wire x1="2.92" y1="-3.6202" x2="-2.92" y2="-3.6202" width="0.12" layer="21"/>
<wire x1="2.92" y1="-3.3" x2="-2.92" y2="-3.3" width="0.12" layer="51"/>
<wire x1="-2.92" y1="-3.3" x2="-2.92" y2="3.3" width="0.12" layer="51"/>
<wire x1="-2.92" y1="3.3" x2="2.92" y2="3.3" width="0.12" layer="51"/>
<wire x1="2.92" y1="3.3" x2="2.92" y2="-3.3" width="0.12" layer="51"/>
<smd name="1" x="-2.6716" y="0" dx="1.2091" dy="6.6123" layer="1"/>
<smd name="2" x="2.6716" y="0" dx="1.2091" dy="6.6123" layer="1"/>
<text x="0" y="4.2552" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-4.2552" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC9198X218" urn="urn:adsk.eagle:footprint:3793179/1" library_version="68">
<description>CHIP, 9.14 X 9.82 X 2.18 mm body
&lt;p&gt;CHIP package with body size 9.14 X 9.82 X 2.18 mm&lt;/p&gt;</description>
<wire x1="4.76" y1="5.2799" x2="-4.76" y2="5.2799" width="0.12" layer="21"/>
<wire x1="4.76" y1="-5.2799" x2="-4.76" y2="-5.2799" width="0.12" layer="21"/>
<wire x1="4.76" y1="-4.91" x2="-4.76" y2="-4.91" width="0.12" layer="51"/>
<wire x1="-4.76" y1="-4.91" x2="-4.76" y2="4.91" width="0.12" layer="51"/>
<wire x1="-4.76" y1="4.91" x2="4.76" y2="4.91" width="0.12" layer="51"/>
<wire x1="4.76" y1="4.91" x2="4.76" y2="-4.91" width="0.12" layer="51"/>
<smd name="1" x="-4.4571" y="0" dx="1.314" dy="9.9318" layer="1"/>
<smd name="2" x="4.4571" y="0" dx="1.314" dy="9.9318" layer="1"/>
<text x="0" y="5.9149" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-5.9149" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="DO214AB_SMC" urn="urn:adsk.eagle:footprint:15618117/2" library_version="68">
<description>&lt;b&gt;DIODE&lt;/b&gt;</description>
<smd name="1" x="-3.5685" y="0" dx="2.794" dy="3.81" layer="1"/>
<smd name="2" x="3.5685" y="0" dx="2.794" dy="3.81" layer="1"/>
<wire x1="-4" y1="2.5" x2="-4" y2="3" width="0.127" layer="21"/>
<wire x1="-4" y1="3" x2="4" y2="3" width="0.127" layer="21"/>
<wire x1="4" y1="3" x2="4" y2="2.5" width="0.127" layer="21"/>
<wire x1="-4" y1="-2.5" x2="-4" y2="-3" width="0.127" layer="21"/>
<wire x1="-4" y1="-3" x2="4" y2="-3" width="0.127" layer="21"/>
<wire x1="4" y1="-3" x2="4" y2="-2.5" width="0.127" layer="21"/>
<wire x1="-4" y1="3" x2="4" y2="3" width="0.127" layer="51"/>
<wire x1="-4" y1="-3" x2="4" y2="-3" width="0.127" layer="51"/>
<wire x1="-4" y1="3" x2="-4" y2="2" width="0.127" layer="51"/>
<wire x1="-4" y1="2" x2="-5" y2="2" width="0.127" layer="51"/>
<wire x1="-5" y1="2" x2="-5" y2="-2" width="0.127" layer="51"/>
<wire x1="-5" y1="-2" x2="-4" y2="-2" width="0.127" layer="51"/>
<wire x1="-4" y1="-2" x2="-4" y2="-3" width="0.127" layer="51"/>
<wire x1="4" y1="3" x2="4" y2="2" width="0.127" layer="51"/>
<wire x1="4" y1="2" x2="5" y2="2" width="0.127" layer="51"/>
<wire x1="5" y1="2" x2="5" y2="-2" width="0.127" layer="51"/>
<wire x1="5" y1="-2" x2="4" y2="-2" width="0.127" layer="51"/>
<wire x1="4" y1="-2" x2="4" y2="-3" width="0.127" layer="51"/>
<wire x1="-5.5" y1="3" x2="-5.5" y2="-3" width="0.127" layer="51"/>
<wire x1="-5.5" y1="3" x2="-5.5" y2="-3" width="0.127" layer="21"/>
<wire x1="-5.5" y1="3" x2="-5.5" y2="-3" width="0.127" layer="51"/>
<text x="-5.08" y="3.81" size="0.8128" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="0" y="3.81" size="0.8128" layer="27" font="vector" ratio="15">&gt;VALUE</text>
</package>
<package name="DO214AA_SMB" urn="urn:adsk.eagle:footprint:15617778/3" library_version="68">
<description>&lt;b&gt;DIODE&lt;/b&gt;</description>
<smd name="A" x="-2.15" y="0" dx="2.5" dy="2.3" layer="1" rot="R180"/>
<smd name="K" x="2.15" y="0" dx="2.5" dy="2.3" layer="1" rot="R180"/>
<wire x1="-2.29" y1="1.97" x2="2.29" y2="1.97" width="0.127" layer="21"/>
<wire x1="-2.29" y1="-1.97" x2="2.29" y2="-1.97" width="0.127" layer="21"/>
<wire x1="-2.29" y1="1.97" x2="-2.29" y2="1.32" width="0.127" layer="21"/>
<wire x1="-2.29" y1="-1.97" x2="-2.29" y2="-1.32" width="0.127" layer="21"/>
<wire x1="2.29" y1="1.97" x2="2.29" y2="1.32" width="0.127" layer="21"/>
<wire x1="2.29" y1="-1.97" x2="2.29" y2="-1.32" width="0.127" layer="21"/>
<wire x1="2.7" y1="1.6" x2="2.8" y2="1.6" width="0.127" layer="21"/>
<wire x1="-2.29" y1="1.97" x2="2.29" y2="1.97" width="0.127" layer="51"/>
<wire x1="-2.29" y1="-1.97" x2="2.29" y2="-1.97" width="0.127" layer="51"/>
<wire x1="-2.29" y1="1.97" x2="-2.29" y2="1.32" width="0.127" layer="51"/>
<wire x1="-2.29" y1="-1.97" x2="-2.29" y2="-1.32" width="0.127" layer="51"/>
<wire x1="2.29" y1="1.97" x2="2.29" y2="1.32" width="0.127" layer="51"/>
<wire x1="2.29" y1="-1.97" x2="2.29" y2="-1.32" width="0.127" layer="51"/>
<wire x1="2.7" y1="1.6" x2="2.8" y2="1.6" width="0.127" layer="51"/>
<circle x="2.7" y="1.6" radius="0.1" width="0.127" layer="21"/>
<circle x="2.7" y="1.6" radius="0.14141875" width="0.127" layer="21"/>
<circle x="2.7" y="1.6" radius="0.1" width="0.127" layer="51"/>
<circle x="2.7" y="1.6" radius="0.14141875" width="0.127" layer="51"/>
<text x="-3" y="2.2" size="1.27" layer="25">&gt;NAME</text>
<text x="-3" y="-3.5" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="RES5750X230N" urn="urn:adsk.eagle:footprint:15618619/1" library_version="68">
<wire x1="2.925" y1="3.72" x2="2.925" y2="-3.72" width="0.127" layer="21"/>
<wire x1="2.925" y1="-3.72" x2="-2.925" y2="-3.72" width="0.127" layer="21"/>
<wire x1="-2.925" y1="-3.72" x2="-2.925" y2="3.72" width="0.127" layer="21"/>
<wire x1="-2.925" y1="3.72" x2="2.925" y2="3.72" width="0.127" layer="21"/>
<wire x1="2.925" y1="3.72" x2="2.925" y2="-3.72" width="0.127" layer="51"/>
<wire x1="2.925" y1="-3.72" x2="-2.925" y2="-3.72" width="0.127" layer="51"/>
<wire x1="-2.925" y1="-3.72" x2="-2.925" y2="3.72" width="0.127" layer="51"/>
<wire x1="-2.925" y1="3.72" x2="2.925" y2="3.72" width="0.127" layer="51"/>
<smd name="1" x="0" y="2.6" dx="1.4" dy="5.25" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-2.6" dx="1.4" dy="5.25" layer="1" roundness="25" rot="R270"/>
<text x="4" y="-2" size="0.635" layer="25" ratio="11" rot="R90">&gt;NAME</text>
<text x="-3" y="-2" size="0.635" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
</package>
<package name="MOLEX_5031590400_SD" urn="urn:adsk.eagle:footprint:8413689/1" library_version="68">
<pad name="P$1" x="0" y="0" drill="0.7"/>
<pad name="P$2" x="1.5" y="2" drill="0.7"/>
<pad name="P$3" x="3" y="0" drill="0.7"/>
<pad name="P$4" x="4.5" y="2" drill="0.7"/>
<wire x1="-2.75" y1="-1.4" x2="-2.75" y2="5.6" width="0.127" layer="21"/>
<wire x1="-2.75" y1="5.6" x2="7.25" y2="5.6" width="0.127" layer="21"/>
<wire x1="7.25" y1="5.6" x2="7.25" y2="-1.4" width="0.127" layer="21"/>
<wire x1="7.25" y1="-1.4" x2="-2.75" y2="-1.4" width="0.127" layer="21"/>
<wire x1="-2.75" y1="-1.4" x2="7.25" y2="-1.4" width="0.127" layer="51"/>
<wire x1="7.25" y1="-1.4" x2="7.25" y2="5.6" width="0.127" layer="51"/>
<wire x1="7.25" y1="5.6" x2="-2.75" y2="5.6" width="0.127" layer="51"/>
<wire x1="-2.75" y1="5.6" x2="-2.75" y2="-1.4" width="0.127" layer="51"/>
<text x="9.8" y="0.7" size="0.6096" layer="25" font="vector">&gt;NAME</text>
<text x="9.8" y="0" size="0.6096" layer="27" font="vector">&gt;VALUE</text>
<hole x="-1.65" y="3.8" drill="0.8"/>
</package>
<package name="SAMTEC_TSW-104-XX-X-S" urn="urn:adsk.eagle:footprint:8413711/1" library_version="68">
<wire x1="-5.209" y1="1.155" x2="5.209" y2="1.155" width="0.2032" layer="21"/>
<wire x1="5.209" y1="1.155" x2="5.209" y2="-1.155" width="0.2032" layer="21"/>
<wire x1="5.209" y1="-1.155" x2="-5.209" y2="-1.155" width="0.2032" layer="21"/>
<wire x1="-5.209" y1="-1.155" x2="-5.209" y2="1.155" width="0.2032" layer="21"/>
<wire x1="5.715" y1="0.9525" x2="5.715" y2="-0.9525" width="0.127" layer="21"/>
<wire x1="3.81" y1="1.5875" x2="5.715" y2="1.5875" width="0.127" layer="51"/>
<wire x1="5.715" y1="1.5875" x2="5.715" y2="0" width="0.127" layer="51"/>
<wire x1="-5.209" y1="1.155" x2="5.209" y2="1.155" width="0.2032" layer="51"/>
<wire x1="5.209" y1="1.155" x2="5.209" y2="-1.155" width="0.2032" layer="51"/>
<wire x1="5.209" y1="-1.155" x2="-5.209" y2="-1.155" width="0.2032" layer="51"/>
<wire x1="-5.209" y1="-1.155" x2="-5.209" y2="1.155" width="0.2032" layer="51"/>
<pad name="1" x="3.81" y="0" drill="1" diameter="1.5" shape="square" rot="R180"/>
<pad name="2" x="1.27" y="0" drill="1" diameter="1.5" rot="R180"/>
<pad name="3" x="-1.27" y="0" drill="1" diameter="1.5" rot="R180"/>
<pad name="4" x="-3.81" y="0" drill="1" diameter="1.5" rot="R180"/>
<text x="-5.715" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="8.255" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-4.16" y1="-0.35" x2="-3.46" y2="0.35" layer="51"/>
<rectangle x1="-1.62" y1="-0.35" x2="-0.92" y2="0.35" layer="51"/>
<rectangle x1="0.92" y1="-0.35" x2="1.62" y2="0.35" layer="51"/>
<rectangle x1="3.46" y1="-0.35" x2="4.16" y2="0.35" layer="51"/>
</package>
<package name="SAMTEC_TSW-104-XX-X-S_NO_OUTLINE" urn="urn:adsk.eagle:footprint:8413712/1" library_version="68">
<pad name="1" x="3.81" y="0" drill="1" diameter="1.5" rot="R180"/>
<pad name="2" x="1.27" y="0" drill="1" diameter="1.5" rot="R180"/>
<pad name="3" x="-1.27" y="0" drill="1" diameter="1.5" rot="R180"/>
<pad name="4" x="-3.81" y="0" drill="1" diameter="1.5" rot="R180"/>
<text x="-5.715" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="8.255" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-4.16" y1="-0.35" x2="-3.46" y2="0.35" layer="51"/>
<rectangle x1="-1.62" y1="-0.35" x2="-0.92" y2="0.35" layer="51"/>
<rectangle x1="0.92" y1="-0.35" x2="1.62" y2="0.35" layer="51"/>
<rectangle x1="3.46" y1="-0.35" x2="4.16" y2="0.35" layer="51"/>
<wire x1="5.08" y1="-0.3175" x2="5.08" y2="0.3175" width="0.254" layer="21"/>
<wire x1="5.08" y1="-0.3175" x2="5.08" y2="0.3175" width="0.254" layer="51"/>
</package>
<package name="SAMTEC_TSW-104-XX-X-S_FLEXSMD_PAD" urn="urn:adsk.eagle:footprint:8413716/1" library_version="68">
<pad name="1" x="3.81" y="0" drill="1" diameter="1.5" rot="R180"/>
<pad name="2" x="1.27" y="0" drill="1" diameter="1.5" rot="R180"/>
<pad name="3" x="-1.27" y="0" drill="1" diameter="1.5" rot="R180"/>
<pad name="4" x="-3.81" y="0" drill="1" diameter="1.5" rot="R180"/>
<text x="-5.715" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="8.255" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-4.16" y1="-0.35" x2="-3.46" y2="0.35" layer="51"/>
<rectangle x1="-1.62" y1="-0.35" x2="-0.92" y2="0.35" layer="51"/>
<rectangle x1="0.92" y1="-0.35" x2="1.62" y2="0.35" layer="51"/>
<rectangle x1="3.46" y1="-0.35" x2="4.16" y2="0.35" layer="51"/>
<smd name="P$1" x="3.81" y="1.27" dx="1.27" dy="0.635" layer="1" rot="R90"/>
<smd name="P$2" x="1.27" y="1.27" dx="1.27" dy="0.635" layer="1" rot="R90"/>
<smd name="P$3" x="-1.27" y="1.27" dx="1.27" dy="0.635" layer="1" rot="R90"/>
<smd name="P$4" x="-3.81" y="1.27" dx="1.27" dy="0.635" layer="1" rot="R90"/>
<wire x1="-3.81" y1="0" x2="-3.81" y2="1.27" width="0.6096" layer="1"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="1.27" width="0.6096" layer="1"/>
<wire x1="1.27" y1="0" x2="1.27" y2="1.27" width="0.6096" layer="1"/>
<wire x1="3.81" y1="0" x2="3.81" y2="1.27" width="0.6096" layer="1"/>
</package>
<package name="JST_B4P-SHF-1AA" urn="urn:adsk.eagle:footprint:8413723/1" library_version="68">
<pad name="1" x="3.75" y="-0.7" drill="0.9"/>
<pad name="2" x="1.25" y="-0.7" drill="0.9"/>
<pad name="3" x="-1.25" y="-0.7" drill="0.9"/>
<pad name="4" x="-3.75" y="-0.7" drill="0.9"/>
<wire x1="-5.25" y1="2.8" x2="5.25" y2="2.8" width="0.127" layer="21"/>
<wire x1="-5.25" y1="-2.8" x2="5.25" y2="-2.8" width="0.127" layer="21"/>
<wire x1="-5.25" y1="-2.8" x2="-5.25" y2="-2.1" width="0.127" layer="21"/>
<wire x1="-5.25" y1="-2.1" x2="-5.25" y2="2.8" width="0.127" layer="21"/>
<wire x1="5.25" y1="-2.8" x2="5.25" y2="-2.1" width="0.127" layer="21"/>
<wire x1="5.25" y1="-2.1" x2="5.25" y2="2.8" width="0.127" layer="21"/>
<wire x1="-5.25" y1="-2.1" x2="5.25" y2="-2.1" width="0.127" layer="21"/>
<wire x1="-5.25" y1="2.8" x2="5.25" y2="2.8" width="0.127" layer="51"/>
<wire x1="-5.25" y1="-2.8" x2="5.25" y2="-2.8" width="0.127" layer="51"/>
<wire x1="-5.25" y1="-2.8" x2="-5.25" y2="-2.1" width="0.127" layer="51"/>
<wire x1="-5.25" y1="-2.1" x2="-5.25" y2="2.8" width="0.127" layer="51"/>
<wire x1="5.25" y1="-2.8" x2="5.25" y2="-2.1" width="0.127" layer="51"/>
<wire x1="5.25" y1="-2.1" x2="5.25" y2="2.8" width="0.127" layer="51"/>
<wire x1="-5.25" y1="-2.1" x2="5.25" y2="-2.1" width="0.127" layer="51"/>
<text x="-0.55" y="-4.3" size="1.27" layer="25">&gt;NAME</text>
<text x="-7.75" y="-4.3" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="JST_BS4P-SHF-1AA" urn="urn:adsk.eagle:footprint:8413724/1" library_version="68">
<pad name="1" x="3.75" y="0" drill="0.9"/>
<pad name="2" x="1.25" y="0" drill="0.9"/>
<pad name="3" x="-1.25" y="0" drill="0.9"/>
<pad name="4" x="-3.75" y="0" drill="0.9"/>
<text x="-0.55" y="-4.5" size="1.27" layer="25">&gt;NAME</text>
<text x="-7.75" y="-4.5" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-5.21" y1="3" x2="5.25" y2="3" width="0.127" layer="21"/>
<wire x1="-5.21" y1="-3" x2="5.25" y2="-3" width="0.127" layer="21"/>
<wire x1="-5.21" y1="3" x2="-5.21" y2="-3" width="0.127" layer="21"/>
<wire x1="5.25" y1="3" x2="5.25" y2="-3" width="0.127" layer="21"/>
<wire x1="-5.21" y1="3" x2="5.25" y2="3" width="0.127" layer="51"/>
<wire x1="-5.21" y1="-3" x2="5.25" y2="-3" width="0.127" layer="51"/>
<wire x1="-5.21" y1="3" x2="-5.21" y2="-3" width="0.127" layer="51"/>
<wire x1="5.25" y1="3" x2="5.25" y2="-3" width="0.127" layer="51"/>
</package>
<package name="JST_F4P-HVQ" urn="urn:adsk.eagle:footprint:8413725/1" library_version="68">
<pad name="1" x="3.75" y="-0.7" drill="1"/>
<pad name="2" x="1.25" y="-0.7" drill="1"/>
<pad name="3" x="-1.25" y="-0.7" drill="1"/>
<pad name="4" x="-3.75" y="-0.7" drill="1"/>
<wire x1="-7.25" y1="-5.3" x2="-7.25" y2="2.8" width="0.127" layer="51"/>
<wire x1="7.25" y1="-5.3" x2="7.25" y2="2.8" width="0.127" layer="51"/>
<wire x1="-7.25" y1="2.8" x2="7.25" y2="2.8" width="0.127" layer="51"/>
<wire x1="-5.75" y1="-1.9" x2="5.75" y2="-1.9" width="0.127" layer="51"/>
<wire x1="-7.25" y1="-5.3" x2="-5.75" y2="-5.3" width="0.127" layer="51"/>
<wire x1="5.75" y1="-5.3" x2="7.25" y2="-5.3" width="0.127" layer="51"/>
<wire x1="-5.75" y1="-1.9" x2="-5.75" y2="-5.3" width="0.127" layer="51"/>
<wire x1="5.75" y1="-1.9" x2="5.75" y2="-5.3" width="0.127" layer="51"/>
<wire x1="-7.25" y1="-1.9" x2="-7.25" y2="2.8" width="0.127" layer="21"/>
<wire x1="7.25" y1="-1.9" x2="7.25" y2="2.8" width="0.127" layer="21"/>
<wire x1="-7.25" y1="2.8" x2="7.25" y2="2.8" width="0.127" layer="21"/>
<wire x1="-7.25" y1="-1.9" x2="7.25" y2="-1.9" width="0.127" layer="21"/>
<text x="-0.55" y="-6.95" size="1.27" layer="25">&gt;NAME</text>
<text x="-7.75" y="-6.95" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="JST_F4P-SHVQ" urn="urn:adsk.eagle:footprint:8413726/1" library_version="68">
<pad name="1" x="3.82" y="-3.35" drill="1"/>
<pad name="2" x="1.32" y="-3.35" drill="1"/>
<pad name="3" x="-1.18" y="-3.35" drill="1"/>
<pad name="4" x="-3.68" y="-3.35" drill="1"/>
<text x="1.3" y="-5.95" size="1.27" layer="25">&gt;NAME</text>
<text x="-6.2" y="-5.95" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-7.2" y1="3.05" x2="7.3" y2="3.05" width="0.127" layer="21"/>
<wire x1="-7.2" y1="-4.45" x2="-7.2" y2="3.05" width="0.127" layer="21"/>
<wire x1="7.3" y1="-4.45" x2="7.3" y2="3.05" width="0.127" layer="21"/>
<wire x1="7.3" y1="-4.45" x2="-7.2" y2="-4.45" width="0.127" layer="21"/>
<wire x1="-5.7" y1="3.05" x2="5.8" y2="3.05" width="0.127" layer="51"/>
<wire x1="-7.2" y1="-4.45" x2="-7.2" y2="4.85" width="0.127" layer="51"/>
<wire x1="7.3" y1="-4.45" x2="7.3" y2="4.85" width="0.127" layer="51"/>
<wire x1="7.3" y1="-4.45" x2="-7.2" y2="-4.45" width="0.127" layer="51"/>
<wire x1="-7.2" y1="4.85" x2="-5.7" y2="4.85" width="0.127" layer="51"/>
<wire x1="5.8" y1="4.85" x2="7.3" y2="4.85" width="0.127" layer="51"/>
<wire x1="-5.7" y1="4.85" x2="-5.7" y2="3.05" width="0.127" layer="51"/>
<wire x1="5.8" y1="4.85" x2="5.8" y2="3.05" width="0.127" layer="51"/>
</package>
<package name="JST_04JQ-BT" urn="urn:adsk.eagle:footprint:8413851/1" library_version="68">
<pad name="1" x="3.75" y="0" drill="0.9"/>
<pad name="2" x="1.25" y="0" drill="0.9"/>
<pad name="3" x="-1.25" y="0" drill="0.9"/>
<pad name="4" x="-3.75" y="0" drill="0.9"/>
<wire x1="-6.25" y1="2.7" x2="6.25" y2="2.7" width="0.127" layer="21"/>
<wire x1="6.25" y1="-3.3" x2="-6.25" y2="-3.3" width="0.127" layer="21"/>
<wire x1="-6.25" y1="2.7" x2="-6.25" y2="-3.3" width="0.127" layer="21"/>
<wire x1="6.25" y1="2.7" x2="6.25" y2="-3.3" width="0.127" layer="21"/>
<wire x1="-6.25" y1="2.7" x2="6.25" y2="2.7" width="0.127" layer="51"/>
<wire x1="6.25" y1="-3.3" x2="-6.25" y2="-3.3" width="0.127" layer="51"/>
<wire x1="-6.25" y1="2.7" x2="-6.25" y2="-3.3" width="0.127" layer="51"/>
<wire x1="6.25" y1="2.7" x2="6.25" y2="-3.3" width="0.127" layer="51"/>
<text x="0.5" y="2.93" size="1.27" layer="25">&gt;NAME</text>
<text x="-7.75" y="2.93" size="1.27" layer="27">&gt;VALUE</text>
<circle x="3.75" y="-3.85" radius="0.25" width="0.127" layer="21"/>
<circle x="3.75" y="-3.85" radius="0.13" width="0.127" layer="21"/>
<circle x="3.75" y="-3.85" radius="0.014140625" width="0.127" layer="21"/>
<circle x="3.75" y="-3.85" radius="0.25" width="0.127" layer="51"/>
<circle x="3.75" y="-3.85" radius="0.13" width="0.127" layer="51"/>
<circle x="3.75" y="-3.85" radius="0.014140625" width="0.127" layer="51"/>
</package>
<package name="JST_04JQ-ST" urn="urn:adsk.eagle:footprint:8413852/1" library_version="68">
<pad name="1" x="3.75" y="0" drill="0.9"/>
<pad name="2" x="1.25" y="0" drill="0.9"/>
<pad name="3" x="-1.25" y="0" drill="0.9"/>
<pad name="4" x="-3.75" y="0" drill="0.9"/>
<wire x1="-6.25" y1="-2.2" x2="6.25" y2="-2.2" width="0.127" layer="51"/>
<wire x1="-6.25" y1="2.6" x2="-5.35" y2="2.6" width="0.127" layer="51"/>
<wire x1="-5.35" y1="2.6" x2="5.35" y2="2.6" width="0.127" layer="51"/>
<wire x1="5.35" y1="2.6" x2="6.25" y2="2.6" width="0.127" layer="51"/>
<wire x1="6.25" y1="-2.2" x2="6.25" y2="2.6" width="0.127" layer="51"/>
<wire x1="-6.25" y1="-2.2" x2="-6.25" y2="2.6" width="0.127" layer="51"/>
<wire x1="-6.25" y1="-2.2" x2="6.25" y2="-2.2" width="0.127" layer="21"/>
<wire x1="-6.25" y1="2.6" x2="6.25" y2="2.6" width="0.127" layer="21"/>
<wire x1="6.25" y1="-2.2" x2="6.25" y2="2.6" width="0.127" layer="21"/>
<wire x1="-6.25" y1="-2.2" x2="-6.25" y2="2.6" width="0.127" layer="21"/>
<wire x1="-5.35" y1="2.6" x2="-5.35" y2="7.3" width="0.127" layer="51"/>
<wire x1="5.35" y1="2.6" x2="5.35" y2="7.3" width="0.127" layer="51"/>
<wire x1="-5.35" y1="7.3" x2="5.35" y2="7.3" width="0.127" layer="51"/>
<text x="-4.15" y="-3.82" size="1.27" layer="25">&gt;NAME</text>
<text x="-12.4" y="-3.82" size="1.27" layer="27">&gt;VALUE</text>
<circle x="3.75" y="-2.75" radius="0.25" width="0.127" layer="21"/>
<circle x="3.75" y="-2.75" radius="0.13" width="0.127" layer="21"/>
<circle x="3.75" y="-2.75" radius="0.014140625" width="0.127" layer="21"/>
<circle x="3.75" y="-2.75" radius="0.25" width="0.127" layer="51"/>
<circle x="3.75" y="-2.75" radius="0.13" width="0.127" layer="51"/>
<circle x="3.75" y="-2.75" radius="0.014140625" width="0.127" layer="51"/>
</package>
<package name="JST_B4B-XH-A" urn="urn:adsk.eagle:footprint:8413853/1" library_version="68">
<pad name="1" x="3.73" y="0" drill="0.9"/>
<pad name="2" x="1.23" y="0" drill="0.9"/>
<pad name="3" x="-1.27" y="0" drill="0.9"/>
<pad name="4" x="-3.77" y="0" drill="0.9"/>
<wire x1="-6.22" y1="2.35" x2="6.18" y2="2.35" width="0.127" layer="51"/>
<wire x1="-6.22" y1="-3.4" x2="6.18" y2="-3.4" width="0.127" layer="51"/>
<wire x1="-6.22" y1="2.35" x2="-6.22" y2="-3.4" width="0.127" layer="51"/>
<wire x1="6.18" y1="2.35" x2="6.18" y2="-3.4" width="0.127" layer="51"/>
<wire x1="-6.22" y1="2.35" x2="6.18" y2="2.35" width="0.127" layer="21"/>
<wire x1="-6.22" y1="-3.4" x2="6.18" y2="-3.4" width="0.127" layer="21"/>
<wire x1="-6.22" y1="2.35" x2="-6.22" y2="-3.4" width="0.127" layer="21"/>
<wire x1="6.18" y1="2.35" x2="6.18" y2="-3.4" width="0.127" layer="21"/>
<text x="-2.75" y="-4.94" size="1.27" layer="25">&gt;NAME</text>
<text x="-10" y="-4.94" size="1.27" layer="27">&gt;VALUE</text>
<circle x="3.73" y="-4" radius="0.25" width="0.127" layer="21"/>
<circle x="3.73" y="-4" radius="0.13" width="0.127" layer="21"/>
<circle x="3.73" y="-4" radius="0.014140625" width="0.127" layer="21"/>
<circle x="3.73" y="-4" radius="0.25" width="0.127" layer="51"/>
<circle x="3.73" y="-4" radius="0.13" width="0.127" layer="51"/>
<circle x="3.73" y="-4" radius="0.014140625" width="0.127" layer="51"/>
</package>
<package name="JST_S4B-XH-A-1" urn="urn:adsk.eagle:footprint:8413854/1" library_version="68">
<pad name="1" x="3.75" y="0" drill="0.9"/>
<pad name="2" x="1.25" y="0" drill="0.9"/>
<pad name="3" x="-1.25" y="0" drill="0.9"/>
<pad name="4" x="-3.75" y="0" drill="0.9"/>
<wire x1="-6.2" y1="7.6" x2="6.2" y2="7.6" width="0.127" layer="21"/>
<wire x1="-6.2" y1="-3.9" x2="6.2" y2="-3.9" width="0.127" layer="21"/>
<wire x1="-6.2" y1="-3.9" x2="-6.2" y2="7.6" width="0.127" layer="21"/>
<wire x1="6.2" y1="-3.9" x2="6.2" y2="7.6" width="0.127" layer="21"/>
<wire x1="-6.2" y1="7.6" x2="6.2" y2="7.6" width="0.127" layer="51"/>
<wire x1="-6.2" y1="-3.9" x2="6.2" y2="-3.9" width="0.127" layer="51"/>
<wire x1="-6.2" y1="-3.9" x2="-6.2" y2="7.6" width="0.127" layer="51"/>
<wire x1="6.2" y1="-3.9" x2="6.2" y2="7.6" width="0.127" layer="51"/>
<text x="0.2" y="-5.395" size="1.27" layer="25">&gt;NAME</text>
<text x="-7.3" y="-5.395" size="1.27" layer="27">&gt;VALUE</text>
<circle x="3.75" y="-2.125" radius="0.25" width="0.127" layer="21"/>
<circle x="3.75" y="-2.125" radius="0.13" width="0.127" layer="21"/>
<circle x="3.75" y="-2.125" radius="0.014140625" width="0.127" layer="21"/>
<circle x="3.75" y="-2.125" radius="0.25" width="0.127" layer="51"/>
<circle x="3.75" y="-2.125" radius="0.13" width="0.127" layer="51"/>
<circle x="3.75" y="-2.125" radius="0.014140625" width="0.127" layer="51"/>
</package>
<package name="JST_B04B-PASK" urn="urn:adsk.eagle:footprint:8413901/1" library_version="68">
<pad name="P$1" x="3" y="0" drill="0.7" rot="R90"/>
<pad name="P$2" x="1" y="0" drill="0.7" rot="R90"/>
<pad name="P$3" x="-1" y="0" drill="0.7" rot="R90"/>
<pad name="P$4" x="-3" y="0" drill="0.7" rot="R90"/>
<hole x="4.5" y="1.7" drill="1.1"/>
<wire x1="-5" y1="-3.05" x2="5" y2="-3.05" width="0.127" layer="21"/>
<wire x1="-5" y1="2.25" x2="4.03" y2="2.25" width="0.127" layer="21"/>
<wire x1="-5" y1="2.25" x2="-5" y2="-3.05" width="0.127" layer="21"/>
<wire x1="5" y1="-3.05" x2="5" y2="1.17" width="0.127" layer="21"/>
<wire x1="-5" y1="-3.05" x2="5" y2="-3.05" width="0.127" layer="51"/>
<wire x1="-5" y1="2.25" x2="4.03" y2="2.25" width="0.127" layer="51"/>
<wire x1="-5" y1="2.25" x2="-5" y2="-3.05" width="0.127" layer="51"/>
<wire x1="5" y1="-3.05" x2="5" y2="1.17" width="0.127" layer="51"/>
<text x="-2.5" y="2.48" size="1.27" layer="25">&gt;NAME</text>
<text x="-10.75" y="2.48" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="JST_S04B-PASK-2" urn="urn:adsk.eagle:footprint:8413902/1" library_version="68">
<pad name="1" x="3" y="-2.35" drill="0.7"/>
<pad name="2" x="1" y="-2.35" drill="0.7"/>
<pad name="3" x="-1" y="-2.35" drill="0.7"/>
<pad name="4" x="-3" y="-2.35" drill="0.7"/>
<hole x="4.5" y="-0.25" drill="1.1"/>
<hole x="-4.5" y="-0.25" drill="1.1"/>
<wire x1="-5" y1="5.85" x2="5" y2="5.85" width="0.127" layer="21"/>
<wire x1="-5" y1="-5.85" x2="5" y2="-5.85" width="0.127" layer="21"/>
<wire x1="-5" y1="5.85" x2="-5" y2="0.27" width="0.127" layer="21"/>
<wire x1="5" y1="5.85" x2="5" y2="0.27" width="0.127" layer="21"/>
<wire x1="-5" y1="-5.85" x2="-5" y2="-0.77" width="0.127" layer="21"/>
<wire x1="5" y1="-5.85" x2="5" y2="-0.77" width="0.127" layer="21"/>
<wire x1="-5" y1="5.85" x2="5" y2="5.85" width="0.127" layer="51"/>
<wire x1="-5" y1="-5.85" x2="5" y2="-5.85" width="0.127" layer="51"/>
<wire x1="-5" y1="5.85" x2="-5" y2="0.27" width="0.127" layer="51"/>
<wire x1="5" y1="5.85" x2="5" y2="0.27" width="0.127" layer="51"/>
<wire x1="-5" y1="-5.85" x2="-5" y2="-0.77" width="0.127" layer="51"/>
<wire x1="5" y1="-5.85" x2="5" y2="-0.77" width="0.127" layer="51"/>
<text x="-0.95" y="-7.35" size="1.27" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="-9.2" y="-7.35" size="1.27" layer="27" font="vector" ratio="15">&gt;VALUE</text>
<circle x="3" y="-4" radius="0.25" width="0.127" layer="21"/>
<circle x="3" y="-4" radius="0.13" width="0.127" layer="21"/>
<circle x="3" y="-4" radius="0.014140625" width="0.127" layer="21"/>
<circle x="3" y="-4" radius="0.25" width="0.127" layer="51"/>
<circle x="3" y="-4" radius="0.13" width="0.127" layer="51"/>
<circle x="3" y="-4" radius="0.014140625" width="0.127" layer="51"/>
</package>
<package name="JST_B4B-PH-K-S" urn="urn:adsk.eagle:footprint:8413934/1" library_version="68">
<pad name="1" x="3" y="-0.55" drill="0.7"/>
<pad name="2" x="1" y="-0.55" drill="0.7"/>
<pad name="3" x="-1" y="-0.55" drill="0.7"/>
<pad name="4" x="-3" y="-0.55" drill="0.7"/>
<wire x1="-4.95" y1="-2.25" x2="4.95" y2="-2.25" width="0.127" layer="21"/>
<wire x1="-4.95" y1="2.25" x2="4.95" y2="2.25" width="0.127" layer="21"/>
<wire x1="-4.95" y1="2.25" x2="-4.95" y2="-2.25" width="0.127" layer="21"/>
<wire x1="4.95" y1="2.25" x2="4.95" y2="-2.25" width="0.127" layer="21"/>
<wire x1="-4.95" y1="-2.25" x2="4.95" y2="-2.25" width="0.127" layer="51"/>
<wire x1="-4.95" y1="2.25" x2="4.95" y2="2.25" width="0.127" layer="51"/>
<wire x1="-4.95" y1="2.25" x2="-4.95" y2="-2.25" width="0.127" layer="51"/>
<wire x1="4.95" y1="2.25" x2="4.95" y2="-2.25" width="0.127" layer="51"/>
<text x="-4.55" y="-4.07" size="1.27" layer="25">&gt;NAME</text>
<text x="-12.8" y="-4.07" size="1.27" layer="27">&gt;VALUE</text>
<circle x="3" y="-2.8" radius="0.25" width="0.127" layer="21"/>
<circle x="3" y="-2.8" radius="0.13" width="0.127" layer="21"/>
<circle x="3" y="-2.8" radius="0.014140625" width="0.127" layer="21"/>
<circle x="3" y="-2.8" radius="0.25" width="0.127" layer="51"/>
<circle x="3" y="-2.8" radius="0.13" width="0.127" layer="51"/>
<circle x="3" y="-2.8" radius="0.014140625" width="0.127" layer="51"/>
</package>
<package name="JST_S4B-PH-K-S" urn="urn:adsk.eagle:footprint:8413935/1" library_version="68">
<pad name="1" x="3" y="-2.875" drill="0.7"/>
<pad name="2" x="1" y="-2.875" drill="0.7"/>
<pad name="3" x="-1" y="-2.875" drill="0.7"/>
<pad name="4" x="-3" y="-2.875" drill="0.7"/>
<text x="-3.3" y="-5.395" size="1.27" layer="25">&gt;NAME</text>
<text x="-10.55" y="-5.395" size="1.27" layer="27">&gt;VALUE</text>
<circle x="3" y="-4.375" radius="0.25" width="0.127" layer="21"/>
<circle x="3" y="-4.375" radius="0.13" width="0.127" layer="21"/>
<circle x="3" y="-4.375" radius="0.014140625" width="0.127" layer="21"/>
<circle x="3" y="-4.375" radius="0.25" width="0.127" layer="51"/>
<circle x="3" y="-4.375" radius="0.13" width="0.127" layer="51"/>
<circle x="3" y="-4.375" radius="0.014140625" width="0.127" layer="51"/>
<wire x1="-4.95" y1="-3.925" x2="4.95" y2="-3.925" width="0.127" layer="51"/>
<wire x1="-4.95" y1="3.925" x2="4.95" y2="3.925" width="0.127" layer="51"/>
<wire x1="-4.95" y1="3.925" x2="-4.95" y2="-3.925" width="0.127" layer="51"/>
<wire x1="4.95" y1="3.925" x2="4.95" y2="-3.925" width="0.127" layer="51"/>
<wire x1="-4.95" y1="-3.925" x2="4.95" y2="-3.925" width="0.127" layer="21"/>
<wire x1="-4.95" y1="3.925" x2="4.95" y2="3.925" width="0.127" layer="21"/>
<wire x1="-4.95" y1="3.925" x2="-4.95" y2="-3.925" width="0.127" layer="21"/>
<wire x1="4.95" y1="3.925" x2="4.95" y2="-3.925" width="0.127" layer="21"/>
</package>
<package name="JST_B04B-PSILE-A1_B04B-PSIY-B1_B04B-PSIR-C1" urn="urn:adsk.eagle:footprint:8413966/1" library_version="68">
<pad name="P$1" x="6" y="1" drill="1.65"/>
<pad name="P$2" x="2" y="1" drill="1.65"/>
<pad name="P$3" x="-2" y="1" drill="1.65"/>
<pad name="P$4" x="-6" y="1" drill="1.65"/>
<hole x="7.65" y="-3.5" drill="1.3"/>
<wire x1="-8.35" y1="4.55" x2="8.35" y2="4.55" width="0.127" layer="51"/>
<wire x1="-8.35" y1="-4.55" x2="8.35" y2="-4.55" width="0.127" layer="51"/>
<wire x1="-8.35" y1="4.55" x2="-8.35" y2="-4.55" width="0.127" layer="51"/>
<wire x1="8.35" y1="-3.07" x2="8.35" y2="4.55" width="0.127" layer="51"/>
<wire x1="8.35" y1="-4.55" x2="8.35" y2="-3.93" width="0.127" layer="51"/>
<wire x1="-8.35" y1="4.55" x2="8.35" y2="4.55" width="0.127" layer="21"/>
<wire x1="-8.35" y1="-4.55" x2="8.35" y2="-4.55" width="0.127" layer="21"/>
<wire x1="-8.35" y1="4.55" x2="-8.35" y2="-4.55" width="0.127" layer="21"/>
<wire x1="8.35" y1="-3.07" x2="8.35" y2="4.55" width="0.127" layer="21"/>
<wire x1="8.35" y1="-4.55" x2="8.35" y2="-3.93" width="0.127" layer="21"/>
<text x="2.45" y="-6.07" size="1.27" layer="25">&gt;NAME</text>
<text x="-4.8" y="-6.07" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="JST_B4PS-VH" urn="urn:adsk.eagle:footprint:8413982/1" library_version="68">
<pad name="P$1" x="-5.94" y="0" drill="1.65"/>
<pad name="P$2" x="-1.98" y="0" drill="1.65"/>
<pad name="P$3" x="1.98" y="0" drill="1.65"/>
<pad name="P$4" x="5.94" y="0" drill="1.65"/>
<text x="-7.96" y="3" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.96" y="3" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-7.89" y1="-7.2" x2="-7.89" y2="1.72" width="0.127" layer="51"/>
<wire x1="-7.36" y1="2.25" x2="7.92" y2="2.25" width="0.127" layer="51"/>
<wire x1="-7.89" y1="-7.2" x2="7.92" y2="-7.2" width="0.127" layer="51"/>
<wire x1="-7.89" y1="1.72" x2="-7.36" y2="2.25" width="0.127" layer="51"/>
<wire x1="7.92" y1="2.25" x2="7.92" y2="-7.2" width="0.127" layer="51"/>
<wire x1="-7.89" y1="-7.2" x2="-7.89" y2="1.72" width="0.127" layer="21"/>
<wire x1="-7.36" y1="2.25" x2="7.92" y2="2.25" width="0.127" layer="21"/>
<wire x1="-7.89" y1="-7.2" x2="7.92" y2="-7.2" width="0.127" layer="21"/>
<wire x1="-7.89" y1="1.72" x2="-7.36" y2="2.25" width="0.127" layer="21"/>
<wire x1="7.92" y1="2.25" x2="7.92" y2="-7.2" width="0.127" layer="21"/>
</package>
<package name="JST_B4P-VH" urn="urn:adsk.eagle:footprint:8413983/1" library_version="68">
<pad name="P$1" x="-5.94" y="0" drill="1.65"/>
<pad name="P$2" x="-1.98" y="0" drill="1.65"/>
<pad name="P$3" x="1.98" y="0" drill="1.65"/>
<pad name="P$4" x="5.94" y="0" drill="1.65"/>
<text x="-7.96" y="-7" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.96" y="-7" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-7.91" y1="-5.25" x2="-7.91" y2="2.5" width="0.127" layer="51"/>
<wire x1="-7.91" y1="2.5" x2="-7.91" y2="3.25" width="0.127" layer="51"/>
<wire x1="7.89" y1="3.25" x2="7.89" y2="2.5" width="0.127" layer="51"/>
<wire x1="7.89" y1="2.5" x2="7.9" y2="-5.25" width="0.127" layer="51"/>
<wire x1="-7.91" y1="3.25" x2="7.89" y2="3.25" width="0.127" layer="51"/>
<wire x1="-7.91" y1="-5.25" x2="7.9" y2="-5.25" width="0.127" layer="51"/>
<wire x1="-7.91" y1="2.5" x2="7.89" y2="2.5" width="0.127" layer="51"/>
<wire x1="-7.91" y1="-5.25" x2="-7.91" y2="2.5" width="0.127" layer="21"/>
<wire x1="-7.91" y1="2.5" x2="-7.91" y2="3.25" width="0.127" layer="21"/>
<wire x1="7.89" y1="3.25" x2="7.89" y2="2.5" width="0.127" layer="21"/>
<wire x1="7.89" y1="2.5" x2="7.9" y2="-5.25" width="0.127" layer="21"/>
<wire x1="-7.91" y1="3.25" x2="7.89" y2="3.25" width="0.127" layer="21"/>
<wire x1="-7.91" y1="-5.25" x2="7.9" y2="-5.25" width="0.127" layer="21"/>
<wire x1="-7.91" y1="2.5" x2="7.89" y2="2.5" width="0.127" layer="21"/>
</package>
<package name="JST_B4P-VH-FB-B" urn="urn:adsk.eagle:footprint:8413984/1" library_version="68">
<pad name="P$1" x="-5.94" y="0" drill="1.65"/>
<pad name="P$2" x="-1.98" y="0" drill="1.65"/>
<pad name="P$3" x="1.98" y="0" drill="1.65"/>
<pad name="P$4" x="5.94" y="0" drill="1.65"/>
<hole x="-7.44" y="-3.4" drill="1.4"/>
<wire x1="-8.86" y1="4.85" x2="8.86" y2="4.85" width="0.127" layer="21"/>
<wire x1="-8.86" y1="-4.85" x2="8.86" y2="-4.85" width="0.127" layer="21"/>
<wire x1="-8.86" y1="4.85" x2="-8.86" y2="-4.85" width="0.127" layer="21"/>
<wire x1="8.86" y1="4.85" x2="8.86" y2="-4.85" width="0.127" layer="21"/>
<wire x1="-8.86" y1="4.85" x2="8.86" y2="4.85" width="0.127" layer="51"/>
<wire x1="-8.86" y1="-4.85" x2="8.86" y2="-4.85" width="0.127" layer="51"/>
<wire x1="-8.86" y1="4.85" x2="-8.86" y2="-4.85" width="0.127" layer="51"/>
<wire x1="8.86" y1="4.85" x2="8.86" y2="-4.85" width="0.127" layer="51"/>
<text x="-8.96" y="5" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.96" y="5" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="JST_B04B-ZESK-1D" urn="urn:adsk.eagle:footprint:8414007/1" library_version="68">
<pad name="P$1" x="2.25" y="1.98" drill="0.7" rot="R180"/>
<pad name="P$2" x="0.75" y="-0.02" drill="0.7" rot="R180"/>
<pad name="P$3" x="-0.75" y="1.98" drill="0.7" rot="R180"/>
<pad name="P$4" x="-2.25" y="-0.02" drill="0.7" rot="R180"/>
<hole x="3.9" y="-1.82" drill="0.8"/>
<wire x1="-4.505" y1="3.115" x2="-4.505" y2="-3.105" width="0.127" layer="51"/>
<wire x1="4.505" y1="3.115" x2="4.505" y2="-3.105" width="0.127" layer="51"/>
<wire x1="-4.505" y1="3.115" x2="4.505" y2="3.115" width="0.127" layer="51"/>
<wire x1="-4.505" y1="-3.105" x2="4.505" y2="-3.105" width="0.127" layer="51"/>
<wire x1="-4.505" y1="3.115" x2="-4.505" y2="-3.105" width="0.127" layer="21"/>
<wire x1="4.505" y1="3.115" x2="4.505" y2="-3.105" width="0.127" layer="21"/>
<wire x1="-4.505" y1="3.115" x2="4.505" y2="3.115" width="0.127" layer="21"/>
<wire x1="-4.505" y1="-3.105" x2="4.505" y2="-3.105" width="0.127" layer="21"/>
<text x="-1.5" y="3.48" size="1.27" layer="25">&gt;NAME</text>
<text x="-9.75" y="3.48" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="JST_S04B-ZESK-2D" urn="urn:adsk.eagle:footprint:8414008/1" library_version="68">
<pad name="P$1" x="2.25" y="-7.5" drill="0.7"/>
<pad name="P$2" x="0.75" y="-3.8" drill="0.7"/>
<pad name="P$3" x="-0.75" y="-7.5" drill="0.7"/>
<pad name="P$4" x="-2.25" y="-3.8" drill="0.7"/>
<hole x="3.8" y="-5.65" drill="1.1"/>
<hole x="-3.8" y="-5.65" drill="1.1"/>
<wire x1="4.5" y1="7.22" x2="4.5" y2="6.47" width="0.127" layer="21"/>
<wire x1="4.5" y1="6.47" x2="4.5" y2="-8.38" width="0.127" layer="21"/>
<wire x1="-4.5" y1="7.22" x2="-4.5" y2="6.47" width="0.127" layer="21"/>
<wire x1="-4.5" y1="6.47" x2="-4.5" y2="-8.38" width="0.127" layer="21"/>
<wire x1="-4.5" y1="-8.38" x2="4.5" y2="-8.38" width="0.127" layer="21"/>
<wire x1="-4.5" y1="7.22" x2="4.5" y2="7.22" width="0.127" layer="21"/>
<wire x1="4.5" y1="7.22" x2="4.5" y2="6.47" width="0.127" layer="51"/>
<wire x1="4.5" y1="6.47" x2="4.5" y2="-8.38" width="0.127" layer="51"/>
<wire x1="-4.5" y1="7.22" x2="-4.5" y2="6.47" width="0.127" layer="51"/>
<wire x1="-4.5" y1="6.47" x2="-4.5" y2="-8.38" width="0.127" layer="51"/>
<wire x1="-4.5" y1="-8.38" x2="4.5" y2="-8.38" width="0.127" layer="51"/>
<wire x1="-4.5" y1="7.22" x2="4.5" y2="7.22" width="0.127" layer="51"/>
<wire x1="-4.5" y1="6.47" x2="4.5" y2="6.47" width="0.127" layer="21"/>
<wire x1="-4.5" y1="6.47" x2="4.5" y2="6.47" width="0.127" layer="51"/>
<text x="-1.2" y="-10" size="1.27" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="-9.45" y="-10" size="1.27" layer="27" font="vector" ratio="15">&gt;VALUE</text>
</package>
<package name="HDRRA4W64P254_1X4_1016X254X254B" urn="urn:adsk.eagle:footprint:15597976/1" library_version="68">
<description>Single-row, 4-pin Pin Header (Male) Right Angle, 2.54 mm (0.10 in) col pitch, 5.84 mm mating length, 10.16 X 2.54 X 2.54 mm body
&lt;p&gt;Single-row (1X4), 4-pin Pin Header (Male) Right Angle package with 2.54 mm (0.10 in) col pitch, 0.64 mm lead width, 2.29 mm tail length and 5.84 mm mating length with body size 10.16 X 2.54 X 2.54 mm, pin pattern - clockwise from top left&lt;/p&gt;</description>
<circle x="-1.3565" y="0" radius="0.25" width="0" layer="21"/>
<wire x1="8.89" y1="-4.06" x2="-1.27" y2="-4.06" width="0.12" layer="21"/>
<wire x1="-1.27" y1="-4.06" x2="-1.27" y2="-1.52" width="0.12" layer="21"/>
<wire x1="-1.27" y1="-1.52" x2="8.89" y2="-1.52" width="0.12" layer="21"/>
<wire x1="8.89" y1="-1.52" x2="8.89" y2="-4.06" width="0.12" layer="21"/>
<wire x1="0" y1="-4.12" x2="0" y2="-9.96" width="0.12" layer="21"/>
<wire x1="2.54" y1="-4.12" x2="2.54" y2="-9.96" width="0.12" layer="21"/>
<wire x1="5.08" y1="-4.12" x2="5.08" y2="-9.96" width="0.12" layer="21"/>
<wire x1="7.62" y1="-4.12" x2="7.62" y2="-9.96" width="0.12" layer="21"/>
<wire x1="8.89" y1="-4.06" x2="-1.27" y2="-4.06" width="0.12" layer="51"/>
<wire x1="-1.27" y1="-4.06" x2="-1.27" y2="-1.52" width="0.12" layer="51"/>
<wire x1="-1.27" y1="-1.52" x2="8.89" y2="-1.52" width="0.12" layer="51"/>
<wire x1="8.89" y1="-1.52" x2="8.89" y2="-4.06" width="0.12" layer="51"/>
<pad name="1" x="0" y="0" drill="1.1051" diameter="1.7051"/>
<pad name="2" x="2.54" y="0" drill="1.1051" diameter="1.7051"/>
<pad name="3" x="5.08" y="0" drill="1.1051" diameter="1.7051"/>
<pad name="4" x="7.62" y="0" drill="1.1051" diameter="1.7051"/>
<text x="0" y="1.4875" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-10.595" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="HDRV4W64P254_1X4_1016X508X1829B" urn="urn:adsk.eagle:footprint:15737104/1" library_version="68">
<description>Single-row, 4-pin Pin Header (Male) Straight, 2.54 mm (0.10 in) col pitch, 15.75 mm mating length, 10.16 X 5.08 X 18.29 mm body
&lt;p&gt;Single-row (1X4), 4-pin Pin Header (Male) Straight package with 2.54 mm (0.10 in) col pitch, 0.64 mm lead width, 2.79 mm tail length and 15.75 mm mating length with overall size 10.16 X 5.08 X 18.29 mm, pin pattern - clockwise from top left&lt;/p&gt;</description>
<circle x="0" y="3.044" radius="0.25" width="0" layer="21"/>
<wire x1="8.89" y1="-2.54" x2="-1.27" y2="-2.54" width="0.12" layer="21"/>
<wire x1="-1.27" y1="-2.54" x2="-1.27" y2="2.54" width="0.12" layer="21"/>
<wire x1="-1.27" y1="2.54" x2="8.89" y2="2.54" width="0.12" layer="21"/>
<wire x1="8.89" y1="2.54" x2="8.89" y2="-2.54" width="0.12" layer="21"/>
<wire x1="8.89" y1="-2.54" x2="-1.27" y2="-2.54" width="0.12" layer="51"/>
<wire x1="-1.27" y1="-2.54" x2="-1.27" y2="2.54" width="0.12" layer="51"/>
<wire x1="-1.27" y1="2.54" x2="8.89" y2="2.54" width="0.12" layer="51"/>
<wire x1="8.89" y1="2.54" x2="8.89" y2="-2.54" width="0.12" layer="51"/>
<pad name="1" x="0" y="0" drill="1.1051" diameter="1.7051"/>
<pad name="2" x="2.54" y="0" drill="1.1051" diameter="1.7051"/>
<pad name="3" x="5.08" y="0" drill="1.1051" diameter="1.7051"/>
<pad name="4" x="7.62" y="0" drill="1.1051" diameter="1.7051"/>
<text x="0" y="3.929" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-3.175" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CONNECTOR_SMD_4PIN" urn="urn:adsk.eagle:footprint:21311884/1" library_version="68">
<wire x1="-5.209" y1="1.155" x2="5.209" y2="1.155" width="0.2032" layer="21"/>
<wire x1="5.209" y1="1.155" x2="5.209" y2="-1.155" width="0.2032" layer="21"/>
<wire x1="5.209" y1="-1.155" x2="-5.209" y2="-1.155" width="0.2032" layer="21"/>
<wire x1="-5.209" y1="-1.155" x2="-5.209" y2="1.155" width="0.2032" layer="21"/>
<wire x1="5.715" y1="0.9525" x2="5.715" y2="-0.9525" width="0.127" layer="21"/>
<wire x1="3.81" y1="1.5875" x2="5.715" y2="1.5875" width="0.127" layer="51"/>
<wire x1="5.715" y1="1.5875" x2="5.715" y2="0" width="0.127" layer="51"/>
<wire x1="-5.209" y1="1.155" x2="5.209" y2="1.155" width="0.2032" layer="51"/>
<wire x1="5.209" y1="1.155" x2="5.209" y2="-1.155" width="0.2032" layer="51"/>
<wire x1="5.209" y1="-1.155" x2="-5.209" y2="-1.155" width="0.2032" layer="51"/>
<wire x1="-5.209" y1="-1.155" x2="-5.209" y2="1.155" width="0.2032" layer="51"/>
<rectangle x1="-4.16" y1="-0.35" x2="-3.46" y2="0.35" layer="51"/>
<rectangle x1="-1.62" y1="-0.35" x2="-0.92" y2="0.35" layer="51"/>
<rectangle x1="0.92" y1="-0.35" x2="1.62" y2="0.35" layer="51"/>
<rectangle x1="3.46" y1="-0.35" x2="4.16" y2="0.35" layer="51"/>
<smd name="P$1" x="3.81" y="0" dx="1.27" dy="1.27" layer="1" roundness="100" rot="R270" cream="no"/>
<smd name="P$2" x="1.27" y="0" dx="1.27" dy="1.27" layer="1" roundness="100" rot="R270" cream="no"/>
<smd name="P$3" x="-1.27" y="0" dx="1.27" dy="1.27" layer="1" roundness="100" rot="R270" cream="no"/>
<smd name="P$4" x="-3.81" y="0" dx="1.27" dy="1.27" layer="1" roundness="100" rot="R270" cream="no"/>
</package>
<package name="AISG_CLUSTER" urn="urn:adsk.eagle:footprint:22271604/8" library_version="83">
<pad name="1" x="3.5" y="0" drill="0.762"/>
<pad name="3" x="-3.5" y="0" drill="0.762"/>
<pad name="4" x="2.475" y="2.475" drill="0.762"/>
<pad name="5" x="-2.475" y="2.475" drill="0.762"/>
<pad name="6" x="2.475" y="-2.475" drill="0.762" thermals="no"/>
<pad name="7" x="-2.475" y="-2.475" drill="0.762" thermals="no"/>
<pad name="PAD1" x="0" y="6.0833" drill="0.9144" thermals="no"/>
<pad name="PAD2" x="0.381" y="6.0833" drill="0.9144" thermals="no"/>
<pad name="PAD3" x="-0.381" y="6.0833" drill="0.9144" thermals="no"/>
<pad name="PAD4" x="-0.762" y="6.0833" drill="0.9144" thermals="no"/>
<pad name="PAD5" x="0.762" y="6.0833" drill="0.9144" thermals="no"/>
<circle x="0" y="0" radius="9.6" width="0.127" layer="21"/>
<circle x="0" y="0" radius="6.5405" width="0.127" layer="51"/>
<wire x1="-1" y1="-5" x2="1" y2="-5" width="0.127" layer="21" curve="-180"/>
<wire x1="-1" y1="-5" x2="-1" y2="-6" width="0.127" layer="21"/>
<wire x1="1" y1="-5" x2="1" y2="-6" width="0.127" layer="21"/>
<wire x1="-1.5" y1="-6" x2="1.5" y2="-6" width="0.127" layer="21"/>
<text x="5.08" y="-0.5" size="1.4224" layer="21" font="vector" ratio="15">1</text>
<text x="-6" y="-0.5" size="1.4224" layer="21" font="vector" ratio="15">3</text>
<text x="4" y="2" size="1.4224" layer="21" font="vector" ratio="15">4</text>
<text x="4" y="-3" size="1.4224" layer="21" font="vector" ratio="15">6</text>
<text x="-5" y="-3" size="1.4224" layer="21" font="vector" ratio="15">7</text>
<text x="-5" y="2" size="1.4224" layer="21" font="vector" ratio="15">5</text>
<text x="6.1" y="-0.5" size="1.4224" layer="22" font="vector" ratio="15" rot="MR0">1</text>
<text x="-5" y="-0.5" size="1.4224" layer="22" font="vector" ratio="15" rot="MR0">3</text>
<text x="5" y="2" size="1.4224" layer="22" font="vector" ratio="15" rot="MR0">4</text>
<text x="-4" y="2" size="1.4224" layer="22" font="vector" ratio="15" rot="MR0">5</text>
<text x="-4" y="-3" size="1.4224" layer="22" font="vector" ratio="15" rot="MR0">7</text>
<text x="5" y="-3" size="1.4224" layer="22" font="vector" ratio="15" rot="MR0">6</text>
<text x="7.62" y="-7.62" size="1.4224" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="8.89" y="-5.08" size="1.4224" layer="27" font="vector" ratio="15">&gt;VALUE</text>
<text x="-1.27" y="-8.255" size="1.016" layer="25" font="vector" ratio="15">MALE</text>
<text x="0.6" y="2.46" size="1.4224" layer="22" font="vector" ratio="15" rot="MR0">2</text>
<text x="-0.42" y="2.46" size="1.4224" layer="21" font="vector" ratio="15">2</text>
<text x="-0.42" y="-2.04" size="1.4224" layer="21" font="vector" ratio="15">8</text>
<text x="0.6" y="-2.04" size="1.4224" layer="22" font="vector" ratio="15" rot="MR0">8</text>
<text x="-1.651" y="4.445" size="1.4224" layer="22" font="vector" ratio="15" rot="MR0">P</text>
<text x="-2.54" y="4.445" size="1.4224" layer="21" font="vector" ratio="15">P</text>
</package>
<package name="GDTMELF4450" urn="urn:adsk.eagle:footprint:3890132/2" library_version="83">
<description>MELF, 4.4 mm length, 5 mm diameter
&lt;p&gt;MELF Resistor package with 4.4 mm length and 5 mm diameter&lt;/p&gt;</description>
<wire x1="1.4868" y1="2.9952" x2="-1.4868" y2="2.9952" width="0.12" layer="21"/>
<wire x1="-1.4868" y1="-2.9952" x2="1.4868" y2="-2.9952" width="0.12" layer="21"/>
<wire x1="2.3" y1="-2.625" x2="-2.3" y2="-2.625" width="0.12" layer="51"/>
<wire x1="-2.3" y1="-2.625" x2="-2.3" y2="2.625" width="0.12" layer="51"/>
<wire x1="-2.3" y1="2.625" x2="2.3" y2="2.625" width="0.12" layer="51"/>
<wire x1="2.3" y1="2.625" x2="2.3" y2="-2.625" width="0.12" layer="51"/>
<text x="0" y="3.6302" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-3.6302" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
<smd name="1" x="-2" y="0" dx="5.6" dy="1.3" layer="1" rot="R90"/>
<smd name="2" x="2" y="0" dx="5.6" dy="1.3" layer="1" rot="R90"/>
</package>
<package name="DIOM5226X292" urn="urn:adsk.eagle:footprint:3793195/2" library_version="83">
<description>MOLDED BODY, 5.265 X 2.6 X 2.92 mm body
&lt;p&gt;MOLDED BODY package with body size 5.265 X 2.6 X 2.92 mm&lt;/p&gt;</description>
<wire x1="2.8" y1="1.45" x2="-2.8" y2="1.45" width="0.12" layer="21"/>
<wire x1="-3.6186" y1="1.4614" x2="-3.6186" y2="-1.4614" width="0.12" layer="21"/>
<wire x1="-2.8" y1="-1.45" x2="2.8" y2="-1.45" width="0.12" layer="21"/>
<wire x1="2.8" y1="-1.45" x2="-2.8" y2="-1.45" width="0.12" layer="51"/>
<wire x1="-2.8" y1="-1.45" x2="-2.8" y2="1.45" width="0.12" layer="51"/>
<wire x1="-2.8" y1="1.45" x2="2.8" y2="1.45" width="0.12" layer="51"/>
<wire x1="2.8" y1="1.45" x2="2.8" y2="-1.45" width="0.12" layer="51"/>
<smd name="K" x="-2.164" y="0" dx="2.2812" dy="1.5153" layer="1"/>
<smd name="A" x="2.164" y="0" dx="2.2812" dy="1.5153" layer="1"/>
<text x="0" y="2.085" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.085" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="DIOM5436X265" urn="urn:adsk.eagle:footprint:3793203/2" library_version="83">
<description>MOLDED BODY, 5.4 X 3.6 X 2.65 mm body
&lt;p&gt;MOLDED BODY package with body size 5.4 X 3.6 X 2.65 mm&lt;/p&gt;</description>
<wire x1="-2.795" y1="1.95" x2="2.795" y2="1.95" width="0.12" layer="21"/>
<wire x1="-3.6171" y1="1.95" x2="-3.6171" y2="-1.95" width="0.12" layer="21"/>
<wire x1="2.795" y1="-1.95" x2="-2.795" y2="-1.95" width="0.12" layer="21"/>
<wire x1="2.795" y1="-1.95" x2="-2.795" y2="-1.95" width="0.12" layer="51"/>
<wire x1="-2.795" y1="-1.95" x2="-2.795" y2="1.95" width="0.12" layer="51"/>
<wire x1="-2.795" y1="1.95" x2="2.795" y2="1.95" width="0.12" layer="51"/>
<wire x1="2.795" y1="1.95" x2="2.795" y2="-1.95" width="0.12" layer="51"/>
<smd name="K" x="-2.2127" y="0" dx="2.1808" dy="2.0291" layer="1"/>
<smd name="A" x="2.2127" y="0" dx="2.1808" dy="2.0291" layer="1"/>
<text x="0" y="2.585" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.585" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="DIOM7959X265" urn="urn:adsk.eagle:footprint:3793209/2" library_version="83">
<description>MOLDED BODY, 7.94 X 5.95 X 2.65 mm body
&lt;p&gt;MOLDED BODY package with body size 7.94 X 5.95 X 2.65 mm&lt;/p&gt;</description>
<wire x1="4.065" y1="3.15" x2="-4.07" y2="3.15" width="0.12" layer="21"/>
<wire x1="-4.8871" y1="3.15" x2="-4.8871" y2="-3.15" width="0.12" layer="21"/>
<wire x1="-4.07" y1="-3.15" x2="4.065" y2="-3.15" width="0.12" layer="21"/>
<wire x1="4.065" y1="-3.15" x2="-4.065" y2="-3.15" width="0.12" layer="51"/>
<wire x1="-4.065" y1="-3.15" x2="-4.065" y2="3.15" width="0.12" layer="51"/>
<wire x1="-4.065" y1="3.15" x2="4.065" y2="3.15" width="0.12" layer="51"/>
<wire x1="4.065" y1="3.15" x2="4.065" y2="-3.15" width="0.12" layer="51"/>
<smd name="K" x="-3.4827" y="0" dx="2.1808" dy="3.1202" layer="1"/>
<smd name="A" x="3.4827" y="0" dx="2.1808" dy="3.1202" layer="1"/>
<text x="0" y="3.785" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-3.785" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="SOD2512X100" urn="urn:adsk.eagle:footprint:3793220/2" library_version="83">
<description>SOD, 2.5 mm span, 1.7 X 1.25 X 1 mm body
&lt;p&gt;SOD package with 2.5 mm span with body size 1.7 X 1.25 X 1 mm&lt;/p&gt;</description>
<wire x1="-0.9" y1="0.675" x2="0.9" y2="0.675" width="0.12" layer="21"/>
<wire x1="-2.0217" y1="0.675" x2="-2.0217" y2="-0.675" width="0.12" layer="21"/>
<wire x1="0.9" y1="-0.675" x2="-0.9" y2="-0.675" width="0.12" layer="21"/>
<wire x1="0.9" y1="-0.675" x2="-0.9" y2="-0.675" width="0.12" layer="51"/>
<wire x1="-0.9" y1="-0.675" x2="-0.9" y2="0.675" width="0.12" layer="51"/>
<wire x1="-0.9" y1="0.675" x2="0.9" y2="0.675" width="0.12" layer="51"/>
<wire x1="0.9" y1="0.675" x2="0.9" y2="-0.675" width="0.12" layer="51"/>
<smd name="K" x="-1.1968" y="0" dx="1.0218" dy="0.4971" layer="1"/>
<smd name="A" x="1.1968" y="0" dx="1.0218" dy="0.4971" layer="1"/>
<text x="0" y="1.31" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.31" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="SOD3716X135" urn="urn:adsk.eagle:footprint:3793234/2" library_version="83">
<description>SOD, 3.7 mm span, 2.7 X 1.6 X 1.35 mm body
&lt;p&gt;SOD package with 3.7 mm span with body size 2.7 X 1.6 X 1.35 mm&lt;/p&gt;</description>
<wire x1="-1.425" y1="0.9" x2="1.425" y2="0.9" width="0.12" layer="21"/>
<wire x1="-2.5991" y1="0.9" x2="-2.5991" y2="-0.9" width="0.12" layer="21"/>
<wire x1="1.425" y1="-0.9" x2="-1.425" y2="-0.9" width="0.12" layer="21"/>
<wire x1="1.425" y1="-0.9" x2="-1.425" y2="-0.9" width="0.12" layer="51"/>
<wire x1="-1.425" y1="-0.9" x2="-1.425" y2="0.9" width="0.12" layer="51"/>
<wire x1="-1.425" y1="0.9" x2="1.425" y2="0.9" width="0.12" layer="51"/>
<wire x1="1.425" y1="0.9" x2="1.425" y2="-0.9" width="0.12" layer="51"/>
<smd name="K" x="-1.7116" y="0" dx="1.147" dy="0.7891" layer="1"/>
<smd name="A" x="1.7116" y="0" dx="1.147" dy="0.7891" layer="1"/>
<text x="0" y="1.535" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.535" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="SODFL4725X110" urn="urn:adsk.eagle:footprint:3921258/2" library_version="83">
<description>SODFL, 4.7 mm span, 3.8 X 2.5 X 1.1 mm body
&lt;p&gt;SODFL package with 4.7 mm span with body size 3.8 X 2.5 X 1.1 mm&lt;/p&gt;</description>
<wire x1="-3.0192" y1="1.35" x2="-3.0192" y2="-1.35" width="0.12" layer="21"/>
<wire x1="2" y1="-1.35" x2="-2" y2="-1.35" width="0.12" layer="51"/>
<wire x1="-2" y1="-1.35" x2="-2" y2="1.35" width="0.12" layer="51"/>
<wire x1="-2" y1="1.35" x2="2" y2="1.35" width="0.12" layer="51"/>
<wire x1="2" y1="1.35" x2="2" y2="-1.35" width="0.12" layer="51"/>
<smd name="K" x="-1.9923" y="0" dx="1.4257" dy="1.9202" layer="1"/>
<smd name="A" x="1.9923" y="0" dx="1.4257" dy="1.9202" layer="1"/>
<text x="0" y="1.985" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.985" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="DIOC0502X50" urn="urn:adsk.eagle:footprint:3948091/2" library_version="83">
<description>CHIP, 0.5 X 0.25 X 0.5 mm body
&lt;p&gt;CHIP package with body size 0.5 X 0.25 X 0.5 mm&lt;/p&gt;</description>
<wire x1="-0.754" y1="0.235" x2="-0.754" y2="-0.235" width="0.12" layer="21"/>
<wire x1="0.275" y1="-0.15" x2="-0.275" y2="-0.15" width="0.12" layer="51"/>
<wire x1="-0.275" y1="-0.15" x2="-0.275" y2="0.15" width="0.12" layer="51"/>
<wire x1="-0.275" y1="0.15" x2="0.275" y2="0.15" width="0.12" layer="51"/>
<wire x1="0.275" y1="0.15" x2="0.275" y2="-0.15" width="0.12" layer="51"/>
<smd name="K" x="-0.275" y="0" dx="0.45" dy="0.35" layer="1"/>
<smd name="A" x="0.275" y="0" dx="0.45" dy="0.35" layer="1"/>
<text x="0" y="1.124" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.124" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="DIOC1005X84" urn="urn:adsk.eagle:footprint:3948102/2" library_version="83">
<description>CHIP, 1.07 X 0.56 X 0.84 mm body
&lt;p&gt;CHIP package with body size 1.07 X 0.56 X 0.84 mm&lt;/p&gt;</description>
<wire x1="-1.0741" y1="0.4165" x2="-1.0741" y2="-0.4165" width="0.12" layer="21"/>
<wire x1="0.61" y1="-0.345" x2="-0.61" y2="-0.345" width="0.12" layer="51"/>
<wire x1="-0.61" y1="-0.345" x2="-0.61" y2="0.345" width="0.12" layer="51"/>
<wire x1="-0.61" y1="0.345" x2="0.61" y2="0.345" width="0.12" layer="51"/>
<wire x1="0.61" y1="0.345" x2="0.61" y2="-0.345" width="0.12" layer="51"/>
<smd name="K" x="-0.51" y="0" dx="0.6202" dy="0.713" layer="1"/>
<smd name="A" x="0.51" y="0" dx="0.6202" dy="0.713" layer="1"/>
<text x="0" y="1.3055" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.3055" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="DIOC1206X63" urn="urn:adsk.eagle:footprint:3948142/2" library_version="83">
<description>CHIP, 1.27 X 0.6 X 0.63 mm body
&lt;p&gt;CHIP package with body size 1.27 X 0.6 X 0.63 mm&lt;/p&gt;</description>
<wire x1="-1.1741" y1="0.4365" x2="-1.1741" y2="-0.4365" width="0.12" layer="21"/>
<wire x1="0.71" y1="-0.365" x2="-0.71" y2="-0.365" width="0.12" layer="51"/>
<wire x1="-0.71" y1="-0.365" x2="-0.71" y2="0.365" width="0.12" layer="51"/>
<wire x1="-0.71" y1="0.365" x2="0.71" y2="0.365" width="0.12" layer="51"/>
<wire x1="0.71" y1="0.365" x2="0.71" y2="-0.365" width="0.12" layer="51"/>
<smd name="K" x="-0.545" y="0" dx="0.7502" dy="0.753" layer="1"/>
<smd name="A" x="0.545" y="0" dx="0.7502" dy="0.753" layer="1"/>
<text x="0" y="1.3255" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.3255" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="DIOC1608X84" urn="urn:adsk.eagle:footprint:3948148/2" library_version="83">
<description>CHIP, 1.63 X 0.81 X 0.84 mm body
&lt;p&gt;CHIP package with body size 1.63 X 0.81 X 0.84 mm&lt;/p&gt;</description>
<wire x1="-1.5041" y1="0.5415" x2="-1.5041" y2="-0.5415" width="0.12" layer="21"/>
<wire x1="0.89" y1="-0.47" x2="-0.89" y2="-0.47" width="0.12" layer="51"/>
<wire x1="-0.89" y1="-0.47" x2="-0.89" y2="0.47" width="0.12" layer="51"/>
<wire x1="-0.89" y1="0.47" x2="0.89" y2="0.47" width="0.12" layer="51"/>
<wire x1="0.89" y1="0.47" x2="0.89" y2="-0.47" width="0.12" layer="51"/>
<smd name="K" x="-0.845" y="0" dx="0.8102" dy="0.963" layer="1"/>
<smd name="A" x="0.845" y="0" dx="0.8102" dy="0.963" layer="1"/>
<text x="0" y="1.4305" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.4305" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="DIOC2012X70" urn="urn:adsk.eagle:footprint:3948151/2" library_version="83">
<description>CHIP, 2 X 1.25 X 0.7 mm body
&lt;p&gt;CHIP package with body size 2 X 1.25 X 0.7 mm&lt;/p&gt;</description>
<wire x1="-1.7117" y1="0.7496" x2="-1.7117" y2="-0.7496" width="0.12" layer="21"/>
<wire x1="1.1" y1="-0.675" x2="-1.1" y2="-0.675" width="0.12" layer="51"/>
<wire x1="-1.1" y1="-0.675" x2="-1.1" y2="0.675" width="0.12" layer="51"/>
<wire x1="-1.1" y1="0.675" x2="1.1" y2="0.675" width="0.12" layer="51"/>
<wire x1="1.1" y1="0.675" x2="1.1" y2="-0.675" width="0.12" layer="51"/>
<smd name="K" x="-0.94" y="0" dx="1.0354" dy="1.3791" layer="1"/>
<smd name="A" x="0.94" y="0" dx="1.0354" dy="1.3791" layer="1"/>
<text x="0" y="1.6386" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.6386" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="DIOC3216X84" urn="urn:adsk.eagle:footprint:3948169/2" library_version="83">
<description>CHIP, 3.2 X 1.6 X 0.84 mm body
&lt;p&gt;CHIP package with body size 3.2 X 1.6 X 0.84 mm&lt;/p&gt;</description>
<wire x1="-2.2891" y1="0.9365" x2="-2.2891" y2="-0.9365" width="0.12" layer="21"/>
<wire x1="1.675" y1="-0.865" x2="-1.675" y2="-0.865" width="0.12" layer="51"/>
<wire x1="-1.675" y1="-0.865" x2="-1.675" y2="0.865" width="0.12" layer="51"/>
<wire x1="-1.675" y1="0.865" x2="1.675" y2="0.865" width="0.12" layer="51"/>
<wire x1="1.675" y1="0.865" x2="1.675" y2="-0.865" width="0.12" layer="51"/>
<smd name="K" x="-1.5131" y="0" dx="1.044" dy="1.753" layer="1"/>
<smd name="A" x="1.5131" y="0" dx="1.044" dy="1.753" layer="1"/>
<text x="0" y="1.8255" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.8255" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="DIOC5324X84" urn="urn:adsk.eagle:footprint:3948172/2" library_version="83">
<description>CHIP, 5.31 X 2.49 X 0.84 mm body
&lt;p&gt;CHIP package with body size 5.31 X 2.49 X 0.84 mm&lt;/p&gt;</description>
<wire x1="-3.3441" y1="1.3815" x2="-3.3441" y2="-1.3815" width="0.12" layer="21"/>
<wire x1="2.73" y1="-1.31" x2="-2.73" y2="-1.31" width="0.12" layer="51"/>
<wire x1="-2.73" y1="-1.31" x2="-2.73" y2="1.31" width="0.12" layer="51"/>
<wire x1="-2.73" y1="1.31" x2="2.73" y2="1.31" width="0.12" layer="51"/>
<wire x1="2.73" y1="1.31" x2="2.73" y2="-1.31" width="0.12" layer="51"/>
<smd name="K" x="-2.555" y="0" dx="1.0702" dy="2.643" layer="1"/>
<smd name="A" x="2.555" y="0" dx="1.0702" dy="2.643" layer="1"/>
<text x="0" y="2.2705" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.2705" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="SOT65P210X110-3" urn="urn:adsk.eagle:footprint:3809952/2" library_version="83">
<description>3-SOT23, 0.65 mm pitch, 2.1 mm span, 2 X 1.25 X 1.1 mm body
&lt;p&gt;3-pin SOT23 package with 0.65 mm pitch, 2.1 mm span with body size 2 X 1.25 X 1.1 mm&lt;/p&gt;</description>
<circle x="-1.0698" y="1.409" radius="0.25" width="0" layer="21"/>
<wire x1="-0.675" y1="1.219" x2="0.675" y2="1.219" width="0.12" layer="21"/>
<wire x1="0.675" y1="1.219" x2="0.675" y2="0.509" width="0.12" layer="21"/>
<wire x1="-0.675" y1="-1.219" x2="0.675" y2="-1.219" width="0.12" layer="21"/>
<wire x1="0.675" y1="-1.219" x2="0.675" y2="-0.509" width="0.12" layer="21"/>
<wire x1="0.675" y1="-1.05" x2="-0.675" y2="-1.05" width="0.12" layer="51"/>
<wire x1="-0.675" y1="-1.05" x2="-0.675" y2="1.05" width="0.12" layer="51"/>
<wire x1="-0.675" y1="1.05" x2="0.675" y2="1.05" width="0.12" layer="51"/>
<wire x1="0.675" y1="1.05" x2="0.675" y2="-1.05" width="0.12" layer="51"/>
<smd name="A" x="-0.9704" y="0.65" dx="0.9884" dy="0.51" layer="1"/>
<smd name="NC" x="-0.9704" y="-0.65" dx="0.9884" dy="0.51" layer="1"/>
<smd name="K" x="0.9704" y="0" dx="0.9884" dy="0.51" layer="1"/>
<text x="0" y="2.294" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.854" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="SOT95P280X135-3" urn="urn:adsk.eagle:footprint:3809968/2" library_version="83">
<description>3-SOT23, 0.95 mm pitch, 2.8 mm span, 2.9 X 1.6 X 1.35 mm body
&lt;p&gt;3-pin SOT23 package with 0.95 mm pitch, 2.8 mm span with body size 2.9 X 1.6 X 1.35 mm&lt;/p&gt;</description>
<circle x="-1.3538" y="1.7786" radius="0.25" width="0" layer="21"/>
<wire x1="-0.85" y1="1.5886" x2="0.85" y2="1.5886" width="0.12" layer="21"/>
<wire x1="0.85" y1="1.5886" x2="0.85" y2="0.5786" width="0.12" layer="21"/>
<wire x1="-0.85" y1="-1.5886" x2="0.85" y2="-1.5886" width="0.12" layer="21"/>
<wire x1="0.85" y1="-1.5886" x2="0.85" y2="-0.5786" width="0.12" layer="21"/>
<wire x1="0.85" y1="-1.5" x2="-0.85" y2="-1.5" width="0.12" layer="51"/>
<wire x1="-0.85" y1="-1.5" x2="-0.85" y2="1.5" width="0.12" layer="51"/>
<wire x1="-0.85" y1="1.5" x2="0.85" y2="1.5" width="0.12" layer="51"/>
<wire x1="0.85" y1="1.5" x2="0.85" y2="-1.5" width="0.12" layer="51"/>
<smd name="A" x="-1.2644" y="0.95" dx="1.1864" dy="0.6492" layer="1"/>
<smd name="NC" x="-1.2644" y="-0.95" dx="1.1864" dy="0.6492" layer="1"/>
<smd name="K" x="1.2644" y="0" dx="1.1864" dy="0.6492" layer="1"/>
<text x="0" y="2.6636" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.2236" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="TO457P1000X238-3" urn="urn:adsk.eagle:footprint:3920003/3" library_version="83">
<description>3-TO, DPAK, 4.57 mm pitch, 10 mm span, 6.6 X 6.1 X 2.38 mm body
&lt;p&gt;3-pin TO, DPAK package with 4.57 mm pitch, 10 mm span with body size 6.6 X 6.1 X 2.38 mm&lt;/p&gt;</description>
<circle x="-4.75" y="3.3836" radius="0.25" width="0" layer="21"/>
<wire x1="6.221" y1="3.0207" x2="6.221" y2="3.365" width="0.12" layer="21"/>
<wire x1="6.221" y1="3.365" x2="-1.015" y2="3.365" width="0.12" layer="21"/>
<wire x1="-1.015" y1="3.365" x2="-1.015" y2="-3.365" width="0.12" layer="21"/>
<wire x1="-1.015" y1="-3.365" x2="6.221" y2="-3.365" width="0.12" layer="21"/>
<wire x1="6.221" y1="-3.365" x2="6.221" y2="-3.0207" width="0.12" layer="21"/>
<wire x1="6.221" y1="-3.365" x2="-1.015" y2="-3.365" width="0.12" layer="51"/>
<wire x1="-1.015" y1="-3.365" x2="-1.015" y2="3.365" width="0.12" layer="51"/>
<wire x1="-1.015" y1="3.365" x2="6.221" y2="3.365" width="0.12" layer="51"/>
<wire x1="6.221" y1="3.365" x2="6.221" y2="-3.365" width="0.12" layer="51"/>
<smd name="1" x="-4.75" y="2.285" dx="1.6078" dy="1.1891" layer="1"/>
<smd name="2" x="-4.75" y="-2.285" dx="1.6078" dy="1.1891" layer="1"/>
<smd name="3" x="2.545" y="0" dx="6.0178" dy="5.5334" layer="1"/>
<text x="0" y="4.2686" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-4" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="RESMELF5875" urn="urn:adsk.eagle:package:3890106/3" type="model" library_version="68">
<description>MELF, 5.865 mm length, 7.5 mm diameter
&lt;p&gt;MELF Resistor package with 5.865 mm length and 7.5 mm diameter&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="GDTMELF5875"/>
</packageinstances>
</package3d>
<package3d name="CAPC1608X55" urn="urn:adsk.eagle:package:3793126/2" type="model" library_version="68">
<description>CHIP, 1.6 X 0.8 X 0.55 mm body
&lt;p&gt;CHIP package with body size 1.6 X 0.8 X 0.55 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC1608X55"/>
</packageinstances>
</package3d>
<package3d name="CAPC1005X60" urn="urn:adsk.eagle:package:3793108/1" type="model" library_version="68">
<description>CHIP, 1.025 X 0.525 X 0.6 mm body
&lt;p&gt;CHIP package with body size 1.025 X 0.525 X 0.6 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC1005X60"/>
</packageinstances>
</package3d>
<package3d name="CAPC1220X107" urn="urn:adsk.eagle:package:3793115/1" type="model" library_version="68">
<description>CHIP, 1.25 X 2 X 1.07 mm body
&lt;p&gt;CHIP package with body size 1.25 X 2 X 1.07 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC1220X107"/>
</packageinstances>
</package3d>
<package3d name="CAPC1632X168" urn="urn:adsk.eagle:package:3793131/1" type="model" library_version="68">
<description>CHIP, 1.6 X 3.2 X 1.68 mm body
&lt;p&gt;CHIP package with body size 1.6 X 3.2 X 1.68 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC1632X168"/>
</packageinstances>
</package3d>
<package3d name="CAPC2012X127" urn="urn:adsk.eagle:package:3793139/1" type="model" library_version="68">
<description>CHIP, 2.01 X 1.25 X 1.27 mm body
&lt;p&gt;CHIP package with body size 2.01 X 1.25 X 1.27 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC2012X127"/>
</packageinstances>
</package3d>
<package3d name="CAPC3215X168" urn="urn:adsk.eagle:package:3793145/1" type="model" library_version="68">
<description>CHIP, 3.2 X 1.5 X 1.68 mm body
&lt;p&gt;CHIP package with body size 3.2 X 1.5 X 1.68 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC3215X168"/>
</packageinstances>
</package3d>
<package3d name="CAPC3225X168" urn="urn:adsk.eagle:package:3793149/1" type="model" library_version="68">
<description>CHIP, 3.2 X 2.5 X 1.68 mm body
&lt;p&gt;CHIP package with body size 3.2 X 2.5 X 1.68 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC3225X168"/>
</packageinstances>
</package3d>
<package3d name="CAPC4520X168" urn="urn:adsk.eagle:package:3793154/1" type="model" library_version="68">
<description>CHIP, 4.5 X 2.03 X 1.68 mm body
&lt;p&gt;CHIP package with body size 4.5 X 2.03 X 1.68 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC4520X168"/>
</packageinstances>
</package3d>
<package3d name="CAPC4532X218" urn="urn:adsk.eagle:package:3793159/1" type="model" library_version="68">
<description>CHIP, 4.5 X 3.2 X 2.18 mm body
&lt;p&gt;CHIP package with body size 4.5 X 3.2 X 2.18 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC4532X218"/>
</packageinstances>
</package3d>
<package3d name="CAPC4564X218" urn="urn:adsk.eagle:package:3793166/1" type="model" library_version="68">
<description>CHIP, 4.5 X 6.4 X 2.18 mm body
&lt;p&gt;CHIP package with body size 4.5 X 6.4 X 2.18 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC4564X218"/>
</packageinstances>
</package3d>
<package3d name="CAPC5650X218" urn="urn:adsk.eagle:package:3793171/1" type="model" library_version="68">
<description>CHIP, 5.64 X 5.08 X 2.18 mm body
&lt;p&gt;CHIP package with body size 5.64 X 5.08 X 2.18 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC5650X218"/>
</packageinstances>
</package3d>
<package3d name="CAPC5563X218" urn="urn:adsk.eagle:package:3793174/1" type="model" library_version="68">
<description>CHIP, 5.59 X 6.35 X 2.18 mm body
&lt;p&gt;CHIP package with body size 5.59 X 6.35 X 2.18 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC5563X218"/>
</packageinstances>
</package3d>
<package3d name="CAPC9198X218" urn="urn:adsk.eagle:package:3793178/1" type="model" library_version="68">
<description>CHIP, 9.14 X 9.82 X 2.18 mm body
&lt;p&gt;CHIP package with body size 9.14 X 9.82 X 2.18 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC9198X218"/>
</packageinstances>
</package3d>
<package3d name="DO214AB" urn="urn:adsk.eagle:package:15618120/3" type="model" library_version="68">
<description>&lt;b&gt;DIODE&lt;/b&gt;</description>
<packageinstances>
<packageinstance name="DO214AB_SMC"/>
</packageinstances>
</package3d>
<package3d name="DO214AA" urn="urn:adsk.eagle:package:15617780/3" type="model" library_version="68">
<description>&lt;b&gt;DIODE&lt;/b&gt;</description>
<packageinstances>
<packageinstance name="DO214AA_SMB"/>
</packageinstances>
</package3d>
<package3d name="RES5750X230N" urn="urn:adsk.eagle:package:15618620/3" type="model" library_version="68">
<description>Chip, 6.60 X 5.00 X 2.50 mm body
&lt;p&gt;Chip package with body size 6.60 X 5.00 X 2.50 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RES5750X230N"/>
</packageinstances>
</package3d>
<package3d name="MOLEX_5031590400_SD" urn="urn:adsk.eagle:package:8414052/2" type="model" library_version="68">
<packageinstances>
<packageinstance name="MOLEX_5031590400_SD"/>
</packageinstances>
</package3d>
<package3d name="SAMTEC_TSW-104-XX-X-S" urn="urn:adsk.eagle:package:8414075/1" type="box" library_version="68">
<packageinstances>
<packageinstance name="SAMTEC_TSW-104-XX-X-S"/>
</packageinstances>
</package3d>
<package3d name="SAMTEC_TSW-104-XX-X-S_NO_OUTLINE" urn="urn:adsk.eagle:package:8414076/1" type="box" library_version="68">
<packageinstances>
<packageinstance name="SAMTEC_TSW-104-XX-X-S_NO_OUTLINE"/>
</packageinstances>
</package3d>
<package3d name="SAMTEC_TSW-104-XX-X-S_FLEXSMD_PAD" urn="urn:adsk.eagle:package:8414081/1" type="box" library_version="68">
<packageinstances>
<packageinstance name="SAMTEC_TSW-104-XX-X-S_FLEXSMD_PAD"/>
</packageinstances>
</package3d>
<package3d name="JST_B4P-SHF-1AA" urn="urn:adsk.eagle:package:8414089/2" type="model" library_version="68">
<packageinstances>
<packageinstance name="JST_B4P-SHF-1AA"/>
</packageinstances>
</package3d>
<package3d name="JST_BS4P-SHF-1AA" urn="urn:adsk.eagle:package:8414090/1" type="box" library_version="68">
<packageinstances>
<packageinstance name="JST_BS4P-SHF-1AA"/>
</packageinstances>
</package3d>
<package3d name="JST_F4P-HVQ" urn="urn:adsk.eagle:package:8414091/1" type="box" library_version="68">
<packageinstances>
<packageinstance name="JST_F4P-HVQ"/>
</packageinstances>
</package3d>
<package3d name="JST_F4P-SHVQ" urn="urn:adsk.eagle:package:8414092/1" type="box" library_version="68">
<packageinstances>
<packageinstance name="JST_F4P-SHVQ"/>
</packageinstances>
</package3d>
<package3d name="JST_04JQ-BT" urn="urn:adsk.eagle:package:8414218/2" type="model" library_version="68">
<packageinstances>
<packageinstance name="JST_04JQ-BT"/>
</packageinstances>
</package3d>
<package3d name="JST_04JQ-ST" urn="urn:adsk.eagle:package:8414219/2" type="model" library_version="68">
<packageinstances>
<packageinstance name="JST_04JQ-ST"/>
</packageinstances>
</package3d>
<package3d name="JST_B4B-XH-A" urn="urn:adsk.eagle:package:8414220/2" type="model" library_version="68">
<packageinstances>
<packageinstance name="JST_B4B-XH-A"/>
</packageinstances>
</package3d>
<package3d name="JST_S4B-XH-A-1" urn="urn:adsk.eagle:package:8414221/1" type="box" library_version="68">
<packageinstances>
<packageinstance name="JST_S4B-XH-A-1"/>
</packageinstances>
</package3d>
<package3d name="JST_B04B-PASK" urn="urn:adsk.eagle:package:8414269/2" type="model" library_version="68">
<packageinstances>
<packageinstance name="JST_B04B-PASK"/>
</packageinstances>
</package3d>
<package3d name="JST_S04B-PASK-2" urn="urn:adsk.eagle:package:8414270/1" type="box" library_version="68">
<packageinstances>
<packageinstance name="JST_S04B-PASK-2"/>
</packageinstances>
</package3d>
<package3d name="JST_B4B-PH-K-S" urn="urn:adsk.eagle:package:8414302/2" type="model" library_version="68">
<packageinstances>
<packageinstance name="JST_B4B-PH-K-S"/>
</packageinstances>
</package3d>
<package3d name="JST_S4B-PH-K-S" urn="urn:adsk.eagle:package:8414303/1" type="box" library_version="68">
<packageinstances>
<packageinstance name="JST_S4B-PH-K-S"/>
</packageinstances>
</package3d>
<package3d name="JST_B04B-PSILE-A1_B04B-PSIY-B1_B04B-PSIR-C1" urn="urn:adsk.eagle:package:8414334/2" type="model" library_version="68">
<packageinstances>
<packageinstance name="JST_B04B-PSILE-A1_B04B-PSIY-B1_B04B-PSIR-C1"/>
</packageinstances>
</package3d>
<package3d name="JST_B4PS-VH" urn="urn:adsk.eagle:package:8414350/2" type="model" library_version="68">
<packageinstances>
<packageinstance name="JST_B4PS-VH"/>
</packageinstances>
</package3d>
<package3d name="JST_B4P-VH" urn="urn:adsk.eagle:package:8414351/2" type="model" library_version="68">
<packageinstances>
<packageinstance name="JST_B4P-VH"/>
</packageinstances>
</package3d>
<package3d name="JST_B4P-VH-FB-B" urn="urn:adsk.eagle:package:8414352/2" type="model" library_version="68">
<packageinstances>
<packageinstance name="JST_B4P-VH-FB-B"/>
</packageinstances>
</package3d>
<package3d name="JST_B04B-ZESK-1D" urn="urn:adsk.eagle:package:8414376/2" type="model" library_version="68">
<packageinstances>
<packageinstance name="JST_B04B-ZESK-1D"/>
</packageinstances>
</package3d>
<package3d name="JST_S04B-ZESK-2D" urn="urn:adsk.eagle:package:8414377/1" type="box" library_version="68">
<packageinstances>
<packageinstance name="JST_S04B-ZESK-2D"/>
</packageinstances>
</package3d>
<package3d name="HDRRA4W64P254_1X4_1016X254X254B" urn="urn:adsk.eagle:package:15597948/1" type="model" library_version="68">
<description>Single-row, 4-pin Pin Header (Male) Right Angle, 2.54 mm (0.10 in) col pitch, 5.84 mm mating length, 10.16 X 2.54 X 2.54 mm body
&lt;p&gt;Single-row (1X4), 4-pin Pin Header (Male) Right Angle package with 2.54 mm (0.10 in) col pitch, 0.64 mm lead width, 2.29 mm tail length and 5.84 mm mating length with body size 10.16 X 2.54 X 2.54 mm, pin pattern - clockwise from top left&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="HDRRA4W64P254_1X4_1016X254X254B"/>
</packageinstances>
</package3d>
<package3d name="HDRV4W64P254_1X4_1016X508X1829B" urn="urn:adsk.eagle:package:15735943/2" type="model" library_version="68">
<description>Single-row, 4-pin Pin Header (Male) Straight, 2.54 mm (0.10 in) col pitch, 15.75 mm mating length, 10.16 X 5.08 X 18.29 mm body
&lt;p&gt;Single-row (1X4), 4-pin Pin Header (Male) Straight package with 2.54 mm (0.10 in) col pitch, 0.64 mm lead width, 2.79 mm tail length and 15.75 mm mating length with overall size 10.16 X 5.08 X 18.29 mm, pin pattern - clockwise from top left&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="HDRV4W64P254_1X4_1016X508X1829B"/>
</packageinstances>
</package3d>
<package3d name="CONNECTOR_SMD_4PIN" urn="urn:adsk.eagle:package:21311885/1" type="box" library_version="68">
<packageinstances>
<packageinstance name="CONNECTOR_SMD_4PIN"/>
</packageinstances>
</package3d>
<package3d name="AISG_CLUSTER" urn="urn:adsk.eagle:package:22271607/9" type="model" library_version="83">
<packageinstances>
<packageinstance name="AISG_CLUSTER"/>
</packageinstances>
</package3d>
<package3d name="RESMELF4450" urn="urn:adsk.eagle:package:3890127/2" type="model" library_version="83">
<description>MELF, 4.4 mm length, 5 mm diameter
&lt;p&gt;MELF Resistor package with 4.4 mm length and 5 mm diameter&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="GDTMELF4450"/>
</packageinstances>
</package3d>
<package3d name="DIOM5226X292" urn="urn:adsk.eagle:package:3793189/2" type="model" library_version="83">
<description>MOLDED BODY, 5.265 X 2.6 X 2.92 mm body
&lt;p&gt;MOLDED BODY package with body size 5.265 X 2.6 X 2.92 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="DIOM5226X292"/>
</packageinstances>
</package3d>
<package3d name="DIOM5436X265" urn="urn:adsk.eagle:package:3793201/2" type="model" library_version="83">
<description>MOLDED BODY, 5.4 X 3.6 X 2.65 mm body
&lt;p&gt;MOLDED BODY package with body size 5.4 X 3.6 X 2.65 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="DIOM5436X265"/>
</packageinstances>
</package3d>
<package3d name="DIOM7959X265" urn="urn:adsk.eagle:package:3793207/2" type="model" library_version="83">
<description>MOLDED BODY, 7.94 X 5.95 X 2.65 mm body
&lt;p&gt;MOLDED BODY package with body size 7.94 X 5.95 X 2.65 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="DIOM7959X265"/>
</packageinstances>
</package3d>
<package3d name="SOD2512X100" urn="urn:adsk.eagle:package:3793216/2" type="model" library_version="83">
<description>SOD, 2.5 mm span, 1.7 X 1.25 X 1 mm body
&lt;p&gt;SOD package with 2.5 mm span with body size 1.7 X 1.25 X 1 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="SOD2512X100"/>
</packageinstances>
</package3d>
<package3d name="SOD3716X135" urn="urn:adsk.eagle:package:3793231/2" type="model" library_version="83">
<description>SOD, 3.7 mm span, 2.7 X 1.6 X 1.35 mm body
&lt;p&gt;SOD package with 3.7 mm span with body size 2.7 X 1.6 X 1.35 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="SOD3716X135"/>
</packageinstances>
</package3d>
<package3d name="SODFL4725X110" urn="urn:adsk.eagle:package:3921253/2" type="model" library_version="83">
<description>SODFL, 4.7 mm span, 3.8 X 2.5 X 1.1 mm body
&lt;p&gt;SODFL package with 4.7 mm span with body size 3.8 X 2.5 X 1.1 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="SODFL4725X110"/>
</packageinstances>
</package3d>
<package3d name="DIOC0502X50" urn="urn:adsk.eagle:package:3948086/2" type="model" library_version="83">
<description>CHIP, 0.5 X 0.25 X 0.5 mm body
&lt;p&gt;CHIP package with body size 0.5 X 0.25 X 0.5 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="DIOC0502X50"/>
</packageinstances>
</package3d>
<package3d name="DIOC1005X84" urn="urn:adsk.eagle:package:3948100/2" type="model" library_version="83">
<description>CHIP, 1.07 X 0.56 X 0.84 mm body
&lt;p&gt;CHIP package with body size 1.07 X 0.56 X 0.84 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="DIOC1005X84"/>
</packageinstances>
</package3d>
<package3d name="DIOC1206X63" urn="urn:adsk.eagle:package:3948141/2" type="model" library_version="83">
<description>CHIP, 1.27 X 0.6 X 0.63 mm body
&lt;p&gt;CHIP package with body size 1.27 X 0.6 X 0.63 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="DIOC1206X63"/>
</packageinstances>
</package3d>
<package3d name="DIOC1608X84" urn="urn:adsk.eagle:package:3948147/2" type="model" library_version="83">
<description>CHIP, 1.63 X 0.81 X 0.84 mm body
&lt;p&gt;CHIP package with body size 1.63 X 0.81 X 0.84 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="DIOC1608X84"/>
</packageinstances>
</package3d>
<package3d name="DIOC2012X70" urn="urn:adsk.eagle:package:3948150/2" type="model" library_version="83">
<description>CHIP, 2 X 1.25 X 0.7 mm body
&lt;p&gt;CHIP package with body size 2 X 1.25 X 0.7 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="DIOC2012X70"/>
</packageinstances>
</package3d>
<package3d name="DIOC3216X84" urn="urn:adsk.eagle:package:3948168/2" type="model" library_version="83">
<description>CHIP, 3.2 X 1.6 X 0.84 mm body
&lt;p&gt;CHIP package with body size 3.2 X 1.6 X 0.84 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="DIOC3216X84"/>
</packageinstances>
</package3d>
<package3d name="DIOC5324X84" urn="urn:adsk.eagle:package:3948171/2" type="model" library_version="83">
<description>CHIP, 5.31 X 2.49 X 0.84 mm body
&lt;p&gt;CHIP package with body size 5.31 X 2.49 X 0.84 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="DIOC5324X84"/>
</packageinstances>
</package3d>
<package3d name="SOT65P210X110-3" urn="urn:adsk.eagle:package:3809951/2" type="model" library_version="83">
<description>3-SOT23, 0.65 mm pitch, 2.1 mm span, 2 X 1.25 X 1.1 mm body
&lt;p&gt;3-pin SOT23 package with 0.65 mm pitch, 2.1 mm span with body size 2 X 1.25 X 1.1 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="SOT65P210X110-3"/>
</packageinstances>
</package3d>
<package3d name="SOT95P280X135-3" urn="urn:adsk.eagle:package:3809961/2" type="model" library_version="83">
<description>3-SOT23, 0.95 mm pitch, 2.8 mm span, 2.9 X 1.6 X 1.35 mm body
&lt;p&gt;3-pin SOT23 package with 0.95 mm pitch, 2.8 mm span with body size 2.9 X 1.6 X 1.35 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="SOT95P280X135-3"/>
</packageinstances>
</package3d>
<package3d name="TO457P1000X238-3" urn="urn:adsk.eagle:package:3920001/3" type="model" library_version="83">
<description>3-TO, DPAK, 4.57 mm pitch, 10 mm span, 6.6 X 6.1 X 2.38 mm body
&lt;p&gt;3-pin TO, DPAK package with 4.57 mm pitch, 10 mm span with body size 6.6 X 6.1 X 2.38 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="TO457P1000X238-3"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="PIN" urn="urn:adsk.eagle:symbol:8413622/1" library_version="67">
<pin name="1" x="-5.08" y="0" visible="off"/>
<wire x1="-3.302" y1="0" x2="-1.27" y2="0" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="0" x2="2.54" y2="0" width="0.8128" layer="94"/>
<text x="-3.302" y="0.508" size="1.27" layer="95" ratio="15">&gt;NAME</text>
</symbol>
<symbol name="GDT_2POS" urn="urn:adsk.eagle:symbol:8413625/1" library_version="68">
<polygon width="0.254" layer="94">
<vertex x="-0.635" y="1.27"/>
<vertex x="0.635" y="1.27"/>
<vertex x="0.635" y="0.635"/>
<vertex x="-0.635" y="0.635"/>
</polygon>
<wire x1="-0.635" y1="-0.635" x2="-0.635" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-0.635" y1="-1.27" x2="0.635" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0.635" y1="-1.27" x2="0.635" y2="-0.635" width="0.254" layer="94"/>
<wire x1="0.635" y1="-0.635" x2="-0.635" y2="-0.635" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="2.54" y2="0" width="0.254" layer="94" curve="90"/>
<wire x1="2.54" y1="0" x2="0" y2="2.54" width="0.254" layer="94" curve="90"/>
<wire x1="0" y1="2.54" x2="-2.54" y2="0" width="0.254" layer="94" curve="90"/>
<wire x1="-2.54" y1="0" x2="0" y2="-2.54" width="0.254" layer="94" curve="90"/>
<pin name="1" x="0" y="5.08" visible="pad" length="short" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="pad" length="short" rot="R90"/>
<text x="5.08" y="-2.54" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
</symbol>
<symbol name="CAPACITOR" urn="urn:adsk.eagle:symbol:8413617/1" library_version="68">
<wire x1="-2.54" y1="0" x2="-0.762" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="0.762" y2="0" width="0.1524" layer="94"/>
<text x="-2.54" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-4.318" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-1.524" y1="-0.254" x2="2.54" y2="0.254" layer="94" rot="R90"/>
<rectangle x1="-2.54" y1="-0.254" x2="1.524" y2="0.254" layer="94" rot="R90"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="DIODE_ZENER" urn="urn:adsk.eagle:symbol:8413614/1" library_version="68">
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="0.635" y1="1.778" x2="1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="-1.27" x2="1.905" y2="-1.778" width="0.254" layer="94"/>
<text x="-2.54" y="1.905" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.778" layer="96">&gt;VALUE</text>
<pin name="A" x="-3.81" y="0" visible="off" length="short" direction="pas"/>
<pin name="K" x="3.81" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="DIODE_ZENER_BI-DIRECTIONAL" urn="urn:adsk.eagle:symbol:8413645/1" library_version="68">
<wire x1="-2.54" y1="-1.27" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-2.54" y2="1.27" width="0.254" layer="94"/>
<wire x1="-0.635" y1="1.778" x2="0" y2="1.27" width="0.254" layer="94"/>
<wire x1="0" y1="1.27" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="-2.54" y1="1.27" x2="-2.54" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="0.635" y2="-1.778" width="0.254" layer="94"/>
<wire x1="2.54" y1="1.27" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="2.54" y2="-1.27" width="0.254" layer="94"/>
<wire x1="2.54" y1="-1.27" x2="2.54" y2="1.27" width="0.254" layer="94"/>
<text x="-3.81" y="1.905" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.81" size="1.778" layer="96">&gt;VALUE</text>
<pin name="A" x="-5.08" y="0" visible="off" length="short" direction="pas"/>
<pin name="K" x="5.08" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GDT_2POS" urn="urn:adsk.eagle:component:8414479/2" prefix="F" uservalue="yes" library_version="83">
<gates>
<gate name="G$1" symbol="GDT_2POS" x="0" y="0"/>
</gates>
<devices>
<device name="BOURNS_2029-XX-SMLF" package="GDTMELF5875">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3890106/3"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="BOURNS_2035-XX-SM" package="GDTMELF4450">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3890127/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAPACITOR" urn="urn:adsk.eagle:component:8414470/2" prefix="C" uservalue="yes" library_version="68">
<gates>
<gate name="G$1" symbol="CAPACITOR" x="0" y="0"/>
</gates>
<devices>
<device name="0402" package="CAPC1005X60">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793108/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0508" package="CAPC1220X107">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793115/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="CAPC1608X55">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793126/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0612" package="CAPC1632X168">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793131/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="CAPC2012X127">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793139/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="CAPC3215X168">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793145/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1210" package="CAPC3225X168">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793149/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1808" package="CAPC4520X168">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793154/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1812" package="CAPC4532X218">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793159/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1825" package="CAPC4564X218">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793166/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2220" package="CAPC5650X218">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793171/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2225" package="CAPC5563X218">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793174/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3640" package="CAPC9198X218">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793178/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DIODE_ZENER" urn="urn:adsk.eagle:component:8414467/8" prefix="D" uservalue="yes" library_version="83">
<gates>
<gate name="G$1" symbol="DIODE_ZENER" x="0" y="0"/>
</gates>
<devices>
<device name="SMA" package="DIOM5226X292">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793189/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMB" package="DIOM5436X265">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793201/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMC" package="DIOM7959X265">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793207/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOD-323" package="SOD2512X100">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793216/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOD-123" package="SOD3716X135">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793231/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOT-323" package="SOT65P210X110-3">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3809951/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOT-23" package="SOT95P280X135-3">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3809961/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOD-128" package="SODFL4725X110">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3921253/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0201" package="DIOC0502X50">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3948086/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402" package="DIOC1005X84">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3948100/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0502" package="DIOC1206X63">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3948141/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="DIOC1608X84">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3948147/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="DIOC2012X70">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3948150/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="DIOC3216X84">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3948168/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2010" package="DIOC5324X84">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3948171/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TO-252-3" package="TO457P1000X238-3">
<connects>
<connect gate="G$1" pin="A" pad="1 2"/>
<connect gate="G$1" pin="K" pad="3"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3920001/3"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="DO214AB" package="DO214AB_SMC">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="K" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15618120/3"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DIODE_ZENER_BI-DIRECTIONAL" urn="urn:adsk.eagle:component:8414501/7" prefix="D" uservalue="yes" library_version="83">
<gates>
<gate name="G$1" symbol="DIODE_ZENER_BI-DIRECTIONAL" x="0" y="0"/>
</gates>
<devices>
<device name="SMA" package="DIOM5226X292">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793189/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMB" package="DIOM5436X265">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793201/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMC" package="DIOM7959X265">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793207/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOD-323" package="SOD2512X100">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793216/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOD-123" package="SOD3716X135">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793231/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOD-128" package="SODFL4725X110">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3921253/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0201" package="DIOC0502X50">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3948086/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402" package="DIOC1005X84">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3948100/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0502" package="DIOC1206X63">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3948141/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="DIOC1608X84">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3948147/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="DIOC2012X70">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3948150/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="DIOC3216X84">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3948168/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2010" package="DIOC5324X84">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3948171/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="DO214AA" package="DO214AA_SMB">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15617780/3"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2220" package="RES5750X230N">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="K" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15618620/3"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CONNECTOR_4POS" urn="urn:adsk.eagle:component:8414493/8" prefix="J" uservalue="yes" library_version="68">
<gates>
<gate name="-1" symbol="PIN" x="5.08" y="0"/>
<gate name="-2" symbol="PIN" x="5.08" y="-2.54"/>
<gate name="-3" symbol="PIN" x="5.08" y="-5.08"/>
<gate name="-4" symbol="PIN" x="5.08" y="-7.62"/>
</gates>
<devices>
<device name="MOLEX_5031590400" package="MOLEX_5031590400_SD">
<connects>
<connect gate="-1" pin="1" pad="P$1"/>
<connect gate="-2" pin="1" pad="P$2"/>
<connect gate="-3" pin="1" pad="P$3"/>
<connect gate="-4" pin="1" pad="P$4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414052/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SAMTEC_TSW-104-XX-X-S" package="SAMTEC_TSW-104-XX-X-S">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-2" pin="1" pad="2"/>
<connect gate="-3" pin="1" pad="3"/>
<connect gate="-4" pin="1" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414075/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SAMTEC_TSW-104-XX-X-S_NO_OUTLINE" package="SAMTEC_TSW-104-XX-X-S_NO_OUTLINE">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-2" pin="1" pad="2"/>
<connect gate="-3" pin="1" pad="3"/>
<connect gate="-4" pin="1" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414076/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SAMTEC_TSW-104-XX-X-S_FLEXSMD_PAD" package="SAMTEC_TSW-104-XX-X-S_FLEXSMD_PAD">
<connects>
<connect gate="-1" pin="1" pad="1 P$1"/>
<connect gate="-2" pin="1" pad="2 P$2"/>
<connect gate="-3" pin="1" pad="3 P$3"/>
<connect gate="-4" pin="1" pad="4 P$4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414081/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST_B4P-SHF-1AA" package="JST_B4P-SHF-1AA">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-2" pin="1" pad="2"/>
<connect gate="-3" pin="1" pad="3"/>
<connect gate="-4" pin="1" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414089/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST_BS4P-SHF-1AA" package="JST_BS4P-SHF-1AA">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-2" pin="1" pad="2"/>
<connect gate="-3" pin="1" pad="3"/>
<connect gate="-4" pin="1" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414090/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST_F4P-HVQ" package="JST_F4P-HVQ">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-2" pin="1" pad="2"/>
<connect gate="-3" pin="1" pad="3"/>
<connect gate="-4" pin="1" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414091/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST_F4P-SHVQ" package="JST_F4P-SHVQ">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-2" pin="1" pad="2"/>
<connect gate="-3" pin="1" pad="3"/>
<connect gate="-4" pin="1" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414092/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST_04JQ-BT" package="JST_04JQ-BT">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-2" pin="1" pad="2"/>
<connect gate="-3" pin="1" pad="3"/>
<connect gate="-4" pin="1" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414218/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST_04JQ-ST" package="JST_04JQ-ST">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-2" pin="1" pad="2"/>
<connect gate="-3" pin="1" pad="3"/>
<connect gate="-4" pin="1" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414219/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST_B4B-XH-A" package="JST_B4B-XH-A">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-2" pin="1" pad="2"/>
<connect gate="-3" pin="1" pad="3"/>
<connect gate="-4" pin="1" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414220/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST_S4B-XH-A-1" package="JST_S4B-XH-A-1">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-2" pin="1" pad="2"/>
<connect gate="-3" pin="1" pad="3"/>
<connect gate="-4" pin="1" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414221/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST_B04B-PASK" package="JST_B04B-PASK">
<connects>
<connect gate="-1" pin="1" pad="P$1"/>
<connect gate="-2" pin="1" pad="P$2"/>
<connect gate="-3" pin="1" pad="P$3"/>
<connect gate="-4" pin="1" pad="P$4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414269/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST_S04B-PASK-2" package="JST_S04B-PASK-2">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-2" pin="1" pad="2"/>
<connect gate="-3" pin="1" pad="3"/>
<connect gate="-4" pin="1" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414270/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST_B4B-PK-K-S" package="JST_B4B-PH-K-S">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-2" pin="1" pad="2"/>
<connect gate="-3" pin="1" pad="3"/>
<connect gate="-4" pin="1" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414302/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST_S4B-PH-K-S" package="JST_S4B-PH-K-S">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-2" pin="1" pad="2"/>
<connect gate="-3" pin="1" pad="3"/>
<connect gate="-4" pin="1" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414303/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST_B04B-PSILE-A1_B04B-PSIY-B1_B04B-PSIR-C1" package="JST_B04B-PSILE-A1_B04B-PSIY-B1_B04B-PSIR-C1">
<connects>
<connect gate="-1" pin="1" pad="P$1"/>
<connect gate="-2" pin="1" pad="P$2"/>
<connect gate="-3" pin="1" pad="P$3"/>
<connect gate="-4" pin="1" pad="P$4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414334/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST_B4PS-VH" package="JST_B4PS-VH">
<connects>
<connect gate="-1" pin="1" pad="P$1"/>
<connect gate="-2" pin="1" pad="P$2"/>
<connect gate="-3" pin="1" pad="P$3"/>
<connect gate="-4" pin="1" pad="P$4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414350/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST_B4P-VH" package="JST_B4P-VH">
<connects>
<connect gate="-1" pin="1" pad="P$1"/>
<connect gate="-2" pin="1" pad="P$2"/>
<connect gate="-3" pin="1" pad="P$3"/>
<connect gate="-4" pin="1" pad="P$4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414351/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST_B4P-VH-FB-B" package="JST_B4P-VH-FB-B">
<connects>
<connect gate="-1" pin="1" pad="P$1"/>
<connect gate="-2" pin="1" pad="P$2"/>
<connect gate="-3" pin="1" pad="P$3"/>
<connect gate="-4" pin="1" pad="P$4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414352/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST_B04B-ESK-1D" package="JST_B04B-ZESK-1D">
<connects>
<connect gate="-1" pin="1" pad="P$1"/>
<connect gate="-2" pin="1" pad="P$2"/>
<connect gate="-3" pin="1" pad="P$3"/>
<connect gate="-4" pin="1" pad="P$4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414376/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST_S04B-ZESK-2D" package="JST_S04B-ZESK-2D">
<connects>
<connect gate="-1" pin="1" pad="P$1"/>
<connect gate="-2" pin="1" pad="P$2"/>
<connect gate="-3" pin="1" pad="P$3"/>
<connect gate="-4" pin="1" pad="P$4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414377/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TSW-104-08-F-S-RA" package="HDRRA4W64P254_1X4_1016X254X254B">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-2" pin="1" pad="2"/>
<connect gate="-3" pin="1" pad="3"/>
<connect gate="-4" pin="1" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15597948/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="DW-04-09-F-S-512" package="HDRV4W64P254_1X4_1016X508X1829B">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-2" pin="1" pad="2"/>
<connect gate="-3" pin="1" pad="3"/>
<connect gate="-4" pin="1" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15735943/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="CONNECTOR_SMD_4PIN">
<connects>
<connect gate="-1" pin="1" pad="P$1"/>
<connect gate="-2" pin="1" pad="P$2"/>
<connect gate="-3" pin="1" pad="P$3"/>
<connect gate="-4" pin="1" pad="P$4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:21311885/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="AISG_CONNECTOR_CLUSTER" urn="urn:adsk.eagle:component:22271608/8" prefix="J" uservalue="yes" library_version="83">
<gates>
<gate name="G$1" symbol="PIN" x="5.08" y="0"/>
<gate name="G$3" symbol="PIN" x="5.08" y="-5.08"/>
<gate name="G$4" symbol="PIN" x="5.08" y="-7.62"/>
<gate name="G$5" symbol="PIN" x="5.08" y="-10.16"/>
<gate name="G$6" symbol="PIN" x="5.08" y="-12.7"/>
<gate name="G$7" symbol="PIN" x="5.08" y="-15.24"/>
<gate name="PAD" symbol="PIN" x="5.08" y="-20.32"/>
</gates>
<devices>
<device name="CLUSTER" package="AISG_CLUSTER">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$3" pin="1" pad="3"/>
<connect gate="G$4" pin="1" pad="4"/>
<connect gate="G$5" pin="1" pad="5"/>
<connect gate="G$6" pin="1" pad="6"/>
<connect gate="G$7" pin="1" pad="7"/>
<connect gate="PAD" pin="1" pad="PAD1 PAD2 PAD3 PAD4 PAD5"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:22271607/9"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="FRAME1" library="frames" library_urn="urn:adsk.eagle:library:229" deviceset="A4L-LOC-JMA" device=""/>
<part name="AGND6" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="AGND" device=""/>
<part name="AGND2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="AGND" device=""/>
<part name="AGND3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="AGND" device=""/>
<part name="AGND4" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="AGND" device=""/>
<part name="AGND5" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="AGND" device=""/>
<part name="SUPPLY5" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY6" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY1" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY2" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY3" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY4" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="AGND1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="AGND" device=""/>
<part name="NC1" library="JMA_LBR" library_urn="urn:adsk.eagle:library:15620506" deviceset="NC" device=""/>
<part name="F5" library="JMA_LBR" library_urn="urn:adsk.eagle:library:15620506" deviceset="0.5MM_FIDUCIAL" device="" package3d_urn="urn:adsk.eagle:package:15620691/1"/>
<part name="F6" library="JMA_LBR" library_urn="urn:adsk.eagle:library:15620506" deviceset="0.5MM_FIDUCIAL" device="" package3d_urn="urn:adsk.eagle:package:15620691/1"/>
<part name="F7" library="JMA_LBR" library_urn="urn:adsk.eagle:library:15620506" deviceset="0.5MM_FIDUCIAL" device="" package3d_urn="urn:adsk.eagle:package:15620691/1"/>
<part name="C1" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0603" package3d_urn="urn:adsk.eagle:package:3793126/2" value="15pf, 100V"/>
<part name="C2" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0603" package3d_urn="urn:adsk.eagle:package:3793126/2" value="15pf, 100V"/>
<part name="F1" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="GDT_2POS" device="BOURNS_2029-XX-SMLF" package3d_urn="urn:adsk.eagle:package:3890106/3" value="2029-09-SM-RPLF"/>
<part name="F2" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="GDT_2POS" device="BOURNS_2029-XX-SMLF" package3d_urn="urn:adsk.eagle:package:3890106/3" value="2029-09-SM-RPLF"/>
<part name="F3" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="GDT_2POS" device="BOURNS_2029-XX-SMLF" package3d_urn="urn:adsk.eagle:package:3890106/3" value="2029-09-SM-RPLF"/>
<part name="F4" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="GDT_2POS" device="BOURNS_2029-XX-SMLF" package3d_urn="urn:adsk.eagle:package:3890106/3" value="2029-09-SM-RPLF"/>
<part name="D2" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="DIODE_ZENER_BI-DIRECTIONAL" device="DO214AA" package3d_urn="urn:adsk.eagle:package:15617780/3" value="SMBJ11CA-13-F"/>
<part name="D3" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="DIODE_ZENER_BI-DIRECTIONAL" device="DO214AA" package3d_urn="urn:adsk.eagle:package:15617780/3" value="SMBJ11CA-13-F"/>
<part name="D1" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="DIODE_ZENER" device="DO214AB" package3d_urn="urn:adsk.eagle:package:15618120/3" value="1SMC33AT3G"/>
<part name="D4" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="DIODE_ZENER_BI-DIRECTIONAL" device="2220" package3d_urn="urn:adsk.eagle:package:15618620/3" value="VC15MA0340KBA"/>
<part name="J2" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CONNECTOR_4POS" device="MOLEX_5031590400" package3d_urn="urn:adsk.eagle:package:8414052/2" value="5031590400"/>
<part name="J3" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CONNECTOR_4POS" device="MOLEX_5031590400" package3d_urn="urn:adsk.eagle:package:8414052/2" value="5031590400"/>
<part name="NC2" library="JMA_LBR" library_urn="urn:adsk.eagle:library:15620506" deviceset="NC" device=""/>
<part name="J1" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="AISG_CONNECTOR_CLUSTER" device="CLUSTER" package3d_urn="urn:adsk.eagle:package:22271607/9"/>
</parts>
<sheets>
<sheet>
<plain>
<wire x1="66.04" y1="78.74" x2="48.26" y2="78.74" width="0.1524" layer="93"/>
<text x="48.26" y="119.38" size="1.4224" layer="97" align="bottom-right">A</text>
<text x="48.26" y="124.46" size="1.4224" layer="97" align="bottom-right">B</text>
<text x="48.26" y="129.54" size="1.4224" layer="97" align="bottom-right">DC RETURN</text>
<text x="48.26" y="134.62" size="1.4224" layer="97" align="bottom-right">10-30V</text>
<text x="48.26" y="139.7" size="1.4224" layer="97" align="bottom-right">+12V</text>
<text x="48.26" y="109.22" size="1.4224" layer="97" align="bottom-right">PAD</text>
<text x="48.26" y="144.78" size="1.4224" layer="97">AISG CONNECTOR INTERFACE + SURGE PROTECTION</text>
</plain>
<instances>
<instance part="FRAME1" gate="G$1" x="0" y="0" smashed="yes">
<attribute name="DRAWING_NAME" x="217.17" y="15.24" size="1.4224" layer="94"/>
<attribute name="LAST_DATE_TIME" x="217.17" y="10.16" size="1.4224" layer="94"/>
<attribute name="SHEET" x="230.505" y="5.08" size="1.4224" layer="94"/>
</instance>
<instance part="F5" gate="G$1" x="200.66" y="53.34" smashed="yes">
<attribute name="NAME" x="198.374" y="62.23" size="1.4224" layer="125"/>
</instance>
<instance part="F6" gate="G$1" x="220.98" y="53.34" smashed="yes">
<attribute name="NAME" x="218.694" y="62.23" size="1.4224" layer="125"/>
</instance>
<instance part="F7" gate="G$1" x="241.3" y="53.34" smashed="yes">
<attribute name="NAME" x="239.014" y="62.23" size="1.4224" layer="125"/>
</instance>
<instance part="AGND6" gate="VR1" x="127" y="71.12" smashed="yes">
<attribute name="VALUE" x="124.46" y="66.04" size="1.4224" layer="96" rot="R90"/>
</instance>
<instance part="AGND2" gate="VR1" x="96.52" y="71.12" smashed="yes">
<attribute name="VALUE" x="93.98" y="66.04" size="1.4224" layer="96" rot="R90"/>
</instance>
<instance part="AGND3" gate="VR1" x="104.14" y="71.12" smashed="yes">
<attribute name="VALUE" x="101.6" y="66.04" size="1.4224" layer="96" rot="R90"/>
</instance>
<instance part="AGND4" gate="VR1" x="111.76" y="71.12" smashed="yes">
<attribute name="VALUE" x="109.22" y="66.04" size="1.4224" layer="96" rot="R90"/>
</instance>
<instance part="AGND5" gate="VR1" x="119.38" y="71.12" smashed="yes">
<attribute name="VALUE" x="116.84" y="66.04" size="1.4224" layer="96" rot="R90"/>
</instance>
<instance part="SUPPLY5" gate="GND" x="177.8" y="93.98" smashed="yes">
<attribute name="VALUE" x="175.895" y="90.805" size="1.4224" layer="96"/>
</instance>
<instance part="SUPPLY6" gate="GND" x="185.42" y="93.98" smashed="yes">
<attribute name="VALUE" x="183.515" y="90.805" size="1.4224" layer="96"/>
</instance>
<instance part="SUPPLY1" gate="GND" x="83.82" y="68.58" smashed="yes">
<attribute name="VALUE" x="81.915" y="65.405" size="1.4224" layer="96"/>
</instance>
<instance part="SUPPLY2" gate="GND" x="134.62" y="71.12" smashed="yes">
<attribute name="VALUE" x="132.715" y="67.945" size="1.4224" layer="96"/>
</instance>
<instance part="SUPPLY3" gate="GND" x="142.24" y="71.12" smashed="yes">
<attribute name="VALUE" x="140.335" y="67.945" size="1.4224" layer="96"/>
</instance>
<instance part="SUPPLY4" gate="GND" x="149.86" y="71.12" smashed="yes">
<attribute name="VALUE" x="147.955" y="67.945" size="1.4224" layer="96"/>
</instance>
<instance part="AGND1" gate="VR1" x="73.66" y="68.58" smashed="yes">
<attribute name="VALUE" x="71.12" y="66.04" size="1.4224" layer="96"/>
</instance>
<instance part="NC1" gate="G$1" x="66.04" y="139.7" smashed="yes"/>
<instance part="C1" gate="G$1" x="177.8" y="106.68" smashed="yes" rot="R270">
<attribute name="NAME" x="175.26" y="101.6" size="1.4224" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="175.26" y="109.22" size="1.4224" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C2" gate="G$1" x="185.42" y="106.68" smashed="yes" rot="R270">
<attribute name="NAME" x="182.88" y="101.6" size="1.4224" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="182.88" y="109.22" size="1.4224" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="F1" gate="G$1" x="96.52" y="83.82" smashed="yes">
<attribute name="NAME" x="93.98" y="78.74" size="1.4224" layer="95" rot="R90"/>
<attribute name="VALUE" x="93.98" y="86.36" size="1.4224" layer="96" rot="R90"/>
</instance>
<instance part="F2" gate="G$1" x="104.14" y="83.82" smashed="yes">
<attribute name="NAME" x="101.6" y="78.74" size="1.4224" layer="95" rot="R90"/>
<attribute name="VALUE" x="101.6" y="86.36" size="1.4224" layer="96" rot="R90"/>
</instance>
<instance part="F3" gate="G$1" x="111.76" y="83.82" smashed="yes">
<attribute name="NAME" x="109.22" y="78.74" size="1.4224" layer="95" rot="R90"/>
<attribute name="VALUE" x="109.22" y="86.36" size="1.4224" layer="96" rot="R90"/>
</instance>
<instance part="F4" gate="G$1" x="119.38" y="83.82" smashed="yes">
<attribute name="NAME" x="116.84" y="78.74" size="1.4224" layer="95" rot="R90"/>
<attribute name="VALUE" x="116.84" y="86.36" size="1.4224" layer="96" rot="R90"/>
</instance>
<instance part="D2" gate="G$1" x="134.62" y="83.82" smashed="yes" rot="R270">
<attribute name="NAME" x="132.08" y="78.74" size="1.4224" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="132.08" y="86.36" size="1.4224" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="D3" gate="G$1" x="142.24" y="83.82" smashed="yes" rot="R270">
<attribute name="NAME" x="139.7" y="78.74" size="1.4224" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="139.7" y="86.36" size="1.4224" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="D1" gate="G$1" x="127" y="83.82" smashed="yes" rot="R90">
<attribute name="NAME" x="124.46" y="78.74" size="1.4224" layer="95" rot="R90"/>
<attribute name="VALUE" x="124.46" y="86.36" size="1.4224" layer="96" rot="R90"/>
</instance>
<instance part="D4" gate="G$1" x="149.86" y="83.82" smashed="yes" rot="R90">
<attribute name="NAME" x="147.32" y="78.74" size="1.4224" layer="95" rot="R90"/>
<attribute name="VALUE" x="147.32" y="86.36" size="1.4224" layer="96" rot="R90"/>
</instance>
<instance part="J2" gate="-1" x="157.48" y="58.42" smashed="yes">
<attribute name="NAME" x="154.178" y="58.928" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J2" gate="-2" x="157.48" y="53.34" smashed="yes">
<attribute name="NAME" x="154.178" y="53.848" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J2" gate="-3" x="157.48" y="48.26" smashed="yes">
<attribute name="NAME" x="154.178" y="48.768" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J2" gate="-4" x="157.48" y="43.18" smashed="yes">
<attribute name="NAME" x="154.178" y="43.688" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J3" gate="-1" x="157.48" y="55.88" smashed="yes">
<attribute name="NAME" x="154.178" y="56.388" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J3" gate="-2" x="157.48" y="50.8" smashed="yes">
<attribute name="NAME" x="154.178" y="51.308" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J3" gate="-3" x="157.48" y="45.72" smashed="yes">
<attribute name="NAME" x="154.178" y="46.228" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J3" gate="-4" x="157.48" y="40.64" smashed="yes">
<attribute name="NAME" x="154.178" y="41.148" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="NC2" gate="G$1" x="66.04" y="114.3" smashed="yes"/>
<instance part="J1" gate="G$1" x="53.34" y="139.7" smashed="yes" rot="R180">
<attribute name="NAME" x="56.642" y="139.192" size="1.27" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="J1" gate="G$3" x="53.34" y="124.46" smashed="yes" rot="R180">
<attribute name="NAME" x="56.642" y="123.952" size="1.27" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="J1" gate="G$4" x="53.34" y="114.3" smashed="yes" rot="R180">
<attribute name="NAME" x="56.642" y="113.792" size="1.27" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="J1" gate="G$5" x="53.34" y="119.38" smashed="yes" rot="R180">
<attribute name="NAME" x="56.642" y="118.872" size="1.27" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="J1" gate="G$6" x="53.34" y="134.62" smashed="yes" rot="R180">
<attribute name="NAME" x="56.642" y="134.112" size="1.27" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="J1" gate="G$7" x="53.34" y="129.54" smashed="yes" rot="R180">
<attribute name="NAME" x="56.642" y="129.032" size="1.27" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="J1" gate="PAD" x="53.34" y="109.22" smashed="yes" rot="R180">
<attribute name="NAME" x="56.642" y="108.712" size="1.27" layer="95" ratio="15" rot="R180"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="SUPPLY5" gate="GND" pin="GND"/>
<wire x1="177.8" y1="101.6" x2="177.8" y2="96.52" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="SUPPLY6" gate="GND" pin="GND"/>
<wire x1="185.42" y1="101.6" x2="185.42" y2="96.52" width="0.1524" layer="91"/>
<pinref part="C2" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="58.42" y1="129.54" x2="83.82" y2="129.54" width="0.1524" layer="91"/>
<wire x1="83.82" y1="129.54" x2="83.82" y2="71.12" width="0.1524" layer="91"/>
<pinref part="SUPPLY1" gate="GND" pin="GND"/>
<pinref part="F4" gate="G$1" pin="1"/>
<wire x1="83.82" y1="129.54" x2="119.38" y2="129.54" width="0.1524" layer="91"/>
<wire x1="119.38" y1="129.54" x2="119.38" y2="88.9" width="0.1524" layer="91"/>
<junction x="83.82" y="129.54"/>
<pinref part="J1" gate="G$7" pin="1"/>
</segment>
<segment>
<pinref part="SUPPLY2" gate="GND" pin="GND"/>
<pinref part="D2" gate="G$1" pin="K"/>
<wire x1="134.62" y1="78.74" x2="134.62" y2="73.66" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUPPLY3" gate="GND" pin="GND"/>
<pinref part="D3" gate="G$1" pin="K"/>
<wire x1="142.24" y1="78.74" x2="142.24" y2="73.66" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUPPLY4" gate="GND" pin="GND"/>
<pinref part="D4" gate="G$1" pin="A"/>
<wire x1="149.86" y1="73.66" x2="149.86" y2="78.74" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="152.4" y1="53.34" x2="147.32" y2="53.34" width="0.1524" layer="91"/>
<wire x1="147.32" y1="53.34" x2="139.7" y2="53.34" width="0.1524" layer="91"/>
<wire x1="152.4" y1="50.8" x2="147.32" y2="50.8" width="0.1524" layer="91"/>
<wire x1="147.32" y1="50.8" x2="147.32" y2="53.34" width="0.1524" layer="91"/>
<junction x="147.32" y="53.34"/>
<label x="139.7" y="53.34" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="J2" gate="-2" pin="1"/>
<pinref part="J3" gate="-2" pin="1"/>
</segment>
</net>
<net name="AGND" class="0">
<segment>
<pinref part="AGND6" gate="VR1" pin="AGND"/>
<wire x1="127" y1="80.01" x2="127" y2="73.66" width="0.1524" layer="91"/>
<pinref part="D1" gate="G$1" pin="A"/>
</segment>
<segment>
<pinref part="AGND2" gate="VR1" pin="AGND"/>
<wire x1="96.52" y1="78.74" x2="96.52" y2="73.66" width="0.1524" layer="91"/>
<pinref part="F1" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="AGND3" gate="VR1" pin="AGND"/>
<wire x1="104.14" y1="78.74" x2="104.14" y2="73.66" width="0.1524" layer="91"/>
<pinref part="F2" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="AGND4" gate="VR1" pin="AGND"/>
<wire x1="111.76" y1="78.74" x2="111.76" y2="73.66" width="0.1524" layer="91"/>
<pinref part="F3" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="AGND5" gate="VR1" pin="AGND"/>
<wire x1="119.38" y1="78.74" x2="119.38" y2="73.66" width="0.1524" layer="91"/>
<pinref part="F4" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="58.42" y1="109.22" x2="73.66" y2="109.22" width="0.1524" layer="91"/>
<wire x1="73.66" y1="109.22" x2="73.66" y2="71.12" width="0.1524" layer="91"/>
<pinref part="AGND1" gate="VR1" pin="AGND"/>
<pinref part="J1" gate="PAD" pin="1"/>
</segment>
</net>
<net name="AISG_N" class="0">
<segment>
<wire x1="58.42" y1="119.38" x2="111.76" y2="119.38" width="0.1524" layer="91"/>
<wire x1="111.76" y1="119.38" x2="111.76" y2="109.22" width="0.1524" layer="91"/>
<wire x1="111.76" y1="109.22" x2="111.76" y2="88.9" width="0.1524" layer="91"/>
<wire x1="111.76" y1="119.38" x2="185.42" y2="119.38" width="0.1524" layer="91"/>
<junction x="111.76" y="119.38"/>
<wire x1="142.24" y1="88.9" x2="142.24" y2="109.22" width="0.1524" layer="91"/>
<wire x1="142.24" y1="109.22" x2="111.76" y2="109.22" width="0.1524" layer="91"/>
<junction x="111.76" y="109.22"/>
<pinref part="F3" gate="G$1" pin="1"/>
<wire x1="185.42" y1="119.38" x2="195.58" y2="119.38" width="0.1524" layer="91"/>
<wire x1="185.42" y1="111.76" x2="185.42" y2="119.38" width="0.1524" layer="91"/>
<label x="195.58" y="119.38" size="1.4224" layer="95" xref="yes"/>
<junction x="185.42" y="119.38"/>
<pinref part="C2" gate="G$1" pin="1"/>
<pinref part="D3" gate="G$1" pin="A"/>
<pinref part="J1" gate="G$5" pin="1"/>
</segment>
<segment>
<wire x1="152.4" y1="48.26" x2="147.32" y2="48.26" width="0.1524" layer="91"/>
<wire x1="147.32" y1="48.26" x2="139.7" y2="48.26" width="0.1524" layer="91"/>
<wire x1="152.4" y1="45.72" x2="147.32" y2="45.72" width="0.1524" layer="91"/>
<wire x1="147.32" y1="45.72" x2="147.32" y2="48.26" width="0.1524" layer="91"/>
<junction x="147.32" y="48.26"/>
<label x="139.7" y="48.26" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="J2" gate="-3" pin="1"/>
<pinref part="J3" gate="-3" pin="1"/>
</segment>
</net>
<net name="AISG_P" class="0">
<segment>
<wire x1="58.42" y1="124.46" x2="104.14" y2="124.46" width="0.1524" layer="91"/>
<wire x1="104.14" y1="124.46" x2="104.14" y2="106.68" width="0.1524" layer="91"/>
<wire x1="104.14" y1="106.68" x2="104.14" y2="88.9" width="0.1524" layer="91"/>
<wire x1="104.14" y1="124.46" x2="177.8" y2="124.46" width="0.1524" layer="91"/>
<junction x="104.14" y="124.46"/>
<wire x1="134.62" y1="88.9" x2="134.62" y2="106.68" width="0.1524" layer="91"/>
<wire x1="134.62" y1="106.68" x2="104.14" y2="106.68" width="0.1524" layer="91"/>
<junction x="104.14" y="106.68"/>
<pinref part="F2" gate="G$1" pin="1"/>
<wire x1="177.8" y1="124.46" x2="195.58" y2="124.46" width="0.1524" layer="91"/>
<wire x1="177.8" y1="111.76" x2="177.8" y2="124.46" width="0.1524" layer="91"/>
<label x="195.58" y="124.46" size="1.4224" layer="95" xref="yes"/>
<junction x="177.8" y="124.46"/>
<pinref part="C1" gate="G$1" pin="1"/>
<pinref part="D2" gate="G$1" pin="A"/>
<pinref part="J1" gate="G$3" pin="1"/>
</segment>
<segment>
<wire x1="152.4" y1="43.18" x2="147.32" y2="43.18" width="0.1524" layer="91"/>
<wire x1="147.32" y1="43.18" x2="139.7" y2="43.18" width="0.1524" layer="91"/>
<wire x1="152.4" y1="40.64" x2="147.32" y2="40.64" width="0.1524" layer="91"/>
<wire x1="147.32" y1="40.64" x2="147.32" y2="43.18" width="0.1524" layer="91"/>
<junction x="147.32" y="43.18"/>
<label x="139.7" y="43.18" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="J2" gate="-4" pin="1"/>
<pinref part="J3" gate="-4" pin="1"/>
</segment>
</net>
<net name="10-30VIN" class="0">
<segment>
<wire x1="58.42" y1="134.62" x2="96.52" y2="134.62" width="0.1524" layer="91"/>
<wire x1="96.52" y1="134.62" x2="96.52" y2="88.9" width="0.1524" layer="91"/>
<wire x1="96.52" y1="134.62" x2="127" y2="134.62" width="0.1524" layer="91"/>
<junction x="96.52" y="134.62"/>
<wire x1="127" y1="134.62" x2="149.86" y2="134.62" width="0.1524" layer="91"/>
<wire x1="127" y1="87.63" x2="127" y2="134.62" width="0.1524" layer="91"/>
<junction x="127" y="134.62"/>
<pinref part="F1" gate="G$1" pin="1"/>
<wire x1="149.86" y1="88.9" x2="149.86" y2="134.62" width="0.1524" layer="91"/>
<junction x="149.86" y="134.62"/>
<label x="195.58" y="134.62" size="1.4224" layer="95" xref="yes"/>
<wire x1="149.86" y1="134.62" x2="195.58" y2="134.62" width="0.1524" layer="91"/>
<pinref part="D1" gate="G$1" pin="K"/>
<pinref part="D4" gate="G$1" pin="K"/>
<pinref part="J1" gate="G$6" pin="1"/>
</segment>
<segment>
<wire x1="152.4" y1="58.42" x2="147.32" y2="58.42" width="0.1524" layer="91"/>
<wire x1="147.32" y1="58.42" x2="139.7" y2="58.42" width="0.1524" layer="91"/>
<wire x1="152.4" y1="55.88" x2="147.32" y2="55.88" width="0.1524" layer="91"/>
<wire x1="147.32" y1="55.88" x2="147.32" y2="58.42" width="0.1524" layer="91"/>
<junction x="147.32" y="58.42"/>
<label x="139.7" y="58.42" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="J2" gate="-1" pin="1"/>
<pinref part="J3" gate="-1" pin="1"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<wire x1="58.42" y1="114.3" x2="66.04" y2="114.3" width="0.1524" layer="91"/>
<pinref part="J1" gate="G$4" pin="1"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<wire x1="66.04" y1="139.7" x2="58.42" y2="139.7" width="0.1524" layer="91"/>
<pinref part="J1" gate="G$1" pin="1"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
