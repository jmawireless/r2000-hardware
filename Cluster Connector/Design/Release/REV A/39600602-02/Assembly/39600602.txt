Partlist exported from C:/Users/sguntupalli/OneDrive - JMA Wireless/Desktop/Cluster Connector/Cluster Connector/Release/39600602-DesignFiles/39600602_rev2.sch at 1/4/2021 9:31 AM

Qty Value           Device                            Package             Parts          Description 
3   0.5MM_FIDUCIAL  0.5MM_FIDUCIAL                    0.5MM_FIDUCIAL      F5, F6, F7                 
2   15pf, 100V      CAPACITOR0603                     CAPC1608X55         C1, C2                     
1   16773           AISG_CONNECTOR_CLUSTERCLUSTER     AISG_CLUSTER        J1                         
1   1SMC33AT3G      DIODE_ZENERDO214AB                DO214AB_SMC         D1                         
4   2029-09-SM-RPLF GDT_2POSBOURNS_2029-XX-SMLF       GDTMELF5875         F1, F2, F3, F4             
2   5031590400      CONNECTOR_4POSMOLEX_5031590400    MOLEX_5031590400_SD J2, J3                     
2   SMBJ11CA-13-F   DIODE_ZENER_BI-DIRECTIONALDO214AA DO214AA_SMB         D2, D3                     
1   VC15MA0340KBA   DIODE_ZENER_BI-DIRECTIONAL2220    RES5750X230N        D4                         
