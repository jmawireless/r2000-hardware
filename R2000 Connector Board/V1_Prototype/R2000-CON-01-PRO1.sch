<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="8.7.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting keepoldvectorfont="yes"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="5" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="no" active="no"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="no" active="no"/>
<layer number="103" name="tMap" color="7" fill="1" visible="no" active="no"/>
<layer number="104" name="Name" color="7" fill="1" visible="no" active="no"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="no" active="no"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="no" active="no"/>
<layer number="107" name="Crop" color="7" fill="1" visible="no" active="no"/>
<layer number="108" name="fp8" color="7" fill="1" visible="yes" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="yes" active="yes"/>
<layer number="110" name="110" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="111" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="7" fill="1" visible="yes" active="yes"/>
<layer number="114" name="FRNTMAAT1" color="7" fill="1" visible="no" active="no"/>
<layer number="115" name="FRNTMAAT2" color="7" fill="1" visible="no" active="no"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="no" active="no"/>
<layer number="117" name="BACKMAAT1" color="7" fill="1" visible="no" active="no"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="yes" active="yes"/>
<layer number="119" name="KAP_TEKEN" color="7" fill="1" visible="no" active="no"/>
<layer number="120" name="KAP_MAAT1" color="7" fill="1" visible="no" active="no"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="no" active="no"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="no" active="no"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="top_silk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="130" name="SMDSTROOK" color="7" fill="1" visible="no" active="no"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="133" name="bottom_silk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="DrillLegend" color="7" fill="1" visible="no" active="no"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="14" fill="1" visible="no" active="no"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="no"/>
<layer number="201" name="201bmp" color="2" fill="1" visible="no" active="no"/>
<layer number="202" name="202bmp" color="3" fill="1" visible="no" active="no"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="no" active="no"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="no" active="no"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="no" active="no"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="no" active="no"/>
<layer number="207" name="207bmp" color="15" fill="10" visible="no" active="no"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="no" active="no"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="Eagle3D_PG1" color="7" fill="1" visible="no" active="no"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="no"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="OrgLBR" color="13" fill="1" visible="no" active="no"/>
<layer number="255" name="Accent" color="7" fill="1" visible="no" active="no"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="frames">
<description>&lt;b&gt;Frames for Sheet and Layout&lt;/b&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="A4L-LOC-JMA">
<wire x1="256.54" y1="3.81" x2="256.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="256.54" y1="8.89" x2="256.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="256.54" y1="13.97" x2="256.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="256.54" y1="19.05" x2="256.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="161.29" y1="3.81" x2="161.29" y2="24.13" width="0.1016" layer="94"/>
<wire x1="161.29" y1="24.13" x2="215.265" y2="24.13" width="0.1016" layer="94"/>
<wire x1="215.265" y1="24.13" x2="256.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="246.38" y1="3.81" x2="246.38" y2="8.89" width="0.1016" layer="94"/>
<wire x1="246.38" y1="8.89" x2="256.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="246.38" y1="8.89" x2="215.265" y2="8.89" width="0.1016" layer="94"/>
<wire x1="215.265" y1="8.89" x2="215.265" y2="3.81" width="0.1016" layer="94"/>
<wire x1="215.265" y1="8.89" x2="215.265" y2="13.97" width="0.1016" layer="94"/>
<wire x1="215.265" y1="13.97" x2="256.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="215.265" y1="13.97" x2="215.265" y2="19.05" width="0.1016" layer="94"/>
<wire x1="215.265" y1="19.05" x2="256.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="215.265" y1="19.05" x2="215.265" y2="24.13" width="0.1016" layer="94"/>
<text x="217.17" y="15.24" size="2.54" layer="94">&gt;DRAWING_NAME</text>
<text x="217.17" y="10.16" size="2.286" layer="94">&gt;LAST_DATE_TIME</text>
<text x="230.505" y="5.08" size="2.54" layer="94">&gt;SHEET</text>
<text x="216.916" y="4.953" size="2.54" layer="94">Sheet:</text>
<frame x1="0" y1="0" x2="260.35" y2="179.07" columns="6" rows="4" layer="94"/>
<text x="162.56" y="15.24" size="2.54" layer="94" ratio="15">Proprietory Information
JMA WIRELESS </text>
<text x="217.17" y="20.32" size="2.54" layer="94" font="fixed">RGANDHI</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="A4L-LOC-JMA" prefix="FRAME">
<gates>
<gate name="G$1" symbol="A4L-LOC-JMA" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="JMA_LBR">
<packages>
<package name="CAPC1005X55N">
<wire x1="-0.48" y1="-0.93" x2="-0.48" y2="0.93" width="0.127" layer="21"/>
<wire x1="-0.48" y1="0.93" x2="0.48" y2="0.93" width="0.127" layer="21"/>
<wire x1="0.48" y1="0.93" x2="0.48" y2="-0.93" width="0.127" layer="21"/>
<wire x1="0.48" y1="-0.93" x2="-0.48" y2="-0.93" width="0.127" layer="21" style="shortdash"/>
<smd name="1" x="0" y="0.45" dx="0.62" dy="0.62" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-0.45" dx="0.62" dy="0.62" layer="1" roundness="25" rot="R270"/>
<text x="0.635" y="1.27" size="0.889" layer="25" ratio="11" rot="R270">&gt;NAME</text>
<text x="-1.524" y="1.397" size="0.635" layer="27" font="vector" ratio="10" rot="R270">&gt;VALUE</text>
<wire x1="-0.48" y1="-0.93" x2="-0.48" y2="0.93" width="0.127" layer="51"/>
<wire x1="-0.48" y1="0.93" x2="0.48" y2="0.93" width="0.127" layer="51"/>
<wire x1="0.48" y1="0.93" x2="0.48" y2="-0.93" width="0.127" layer="51"/>
<wire x1="0.48" y1="-0.93" x2="-0.48" y2="-0.93" width="0.127" layer="51" style="shortdash"/>
</package>
<package name="CAPC1608X55N">
<wire x1="0.675" y1="1.47" x2="0.675" y2="-1.47" width="0.127" layer="21"/>
<wire x1="0.675" y1="-1.47" x2="-0.675" y2="-1.47" width="0.127" layer="21"/>
<wire x1="-0.675" y1="-1.47" x2="-0.675" y2="1.47" width="0.127" layer="21"/>
<wire x1="-0.675" y1="1.47" x2="0.675" y2="1.47" width="0.127" layer="21"/>
<smd name="1" x="0" y="0.762" dx="0.9" dy="0.95" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-0.762" dx="0.9" dy="0.95" layer="1" roundness="25" rot="R270"/>
<text x="0.889" y="1.27" size="0.889" layer="25" ratio="11" rot="R270">&gt;NAME</text>
<text x="-1.5875" y="1.27" size="0.635" layer="27" ratio="10" rot="R270">&gt;VALUE</text>
<rectangle x1="-1.27" y1="-0.508" x2="1.27" y2="0.508" layer="39" rot="R270"/>
<wire x1="0.675" y1="1.47" x2="0.675" y2="-1.47" width="0.127" layer="51"/>
<wire x1="0.675" y1="-1.47" x2="-0.675" y2="-1.47" width="0.127" layer="51"/>
<wire x1="-0.675" y1="-1.47" x2="-0.675" y2="1.47" width="0.127" layer="51"/>
<wire x1="-0.675" y1="1.47" x2="0.675" y2="1.47" width="0.127" layer="51"/>
</package>
<package name="AISG_C091-C006-804-2">
<pad name="1" x="3.5" y="0" drill="1.1"/>
<pad name="3" x="-3.5" y="0" drill="1.1"/>
<pad name="4" x="2.475" y="2.475" drill="1.1"/>
<pad name="5" x="-2.475" y="2.475" drill="1.1"/>
<pad name="6" x="2.475" y="-2.475" drill="1.1" thermals="no"/>
<pad name="7" x="-2.475" y="-2.475" drill="1.1" thermals="no"/>
<circle x="0" y="0" radius="9.6" width="0.127" layer="21"/>
<circle x="0" y="0" radius="6.5405" width="0.127" layer="51"/>
<pad name="PAD" x="-3.615" y="6.7262" drill="1.2" thermals="no"/>
<wire x1="-1" y1="-5" x2="1" y2="-5" width="0.127" layer="21" curve="-180"/>
<wire x1="-1" y1="-5" x2="-1" y2="-6" width="0.127" layer="21"/>
<wire x1="1" y1="-5" x2="1" y2="-6" width="0.127" layer="21"/>
<wire x1="-1.5" y1="-6" x2="1.5" y2="-6" width="0.127" layer="21"/>
<text x="5.08" y="-0.5" size="1.4224" layer="21" font="vector" ratio="15">1</text>
<text x="-6" y="-0.5" size="1.4224" layer="21" font="vector" ratio="15">3</text>
<text x="4" y="2" size="1.4224" layer="21" font="vector" ratio="15">4</text>
<text x="4" y="-3" size="1.4224" layer="21" font="vector" ratio="15">6</text>
<text x="-5" y="-3" size="1.4224" layer="21" font="vector" ratio="15">7</text>
<text x="-5" y="2" size="1.4224" layer="21" font="vector" ratio="15">5</text>
<text x="6.1" y="-0.5" size="1.4224" layer="22" font="vector" ratio="15" rot="MR0">1</text>
<text x="-5" y="-0.5" size="1.4224" layer="22" font="vector" ratio="15" rot="MR0">3</text>
<text x="5" y="2" size="1.4224" layer="22" font="vector" ratio="15" rot="MR0">4</text>
<text x="-4" y="2" size="1.4224" layer="22" font="vector" ratio="15" rot="MR0">5</text>
<text x="-4" y="-3" size="1.4224" layer="22" font="vector" ratio="15" rot="MR0">7</text>
<text x="5" y="-3" size="1.4224" layer="22" font="vector" ratio="15" rot="MR0">6</text>
<text x="7.62" y="-7.62" size="1.4224" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="8.89" y="-5.08" size="1.4224" layer="27" font="vector" ratio="15">&gt;VALUE</text>
<text x="-1.27" y="-8.255" size="1.016" layer="25" font="vector" ratio="15">MALE</text>
<pad name="2" x="0" y="3.5" drill="1.1"/>
<pad name="8" x="0" y="-0.7" drill="1.1"/>
<text x="0.6" y="5" size="1.4224" layer="22" font="vector" ratio="15" rot="MR0">2</text>
<text x="-0.42" y="5" size="1.4224" layer="21" font="vector" ratio="15">2</text>
<text x="-0.42" y="0.5" size="1.4224" layer="21" font="vector" ratio="15">8</text>
<text x="0.6" y="0.5" size="1.4224" layer="22" font="vector" ratio="15" rot="MR0">8</text>
<text x="-5.08" y="5.08" size="1.4224" layer="22" font="vector" ratio="15" rot="MR0">P</text>
<text x="-6.35" y="5.08" size="1.4224" layer="21" font="vector" ratio="15">P</text>
</package>
<package name="AISG_C091-G006-804-2">
<pad name="3" x="3.5" y="0" drill="1.1"/>
<pad name="1" x="-3.5" y="0" drill="1.1"/>
<pad name="5" x="2.475" y="2.475" drill="1.1"/>
<pad name="4" x="-2.475" y="2.475" drill="1.1"/>
<pad name="7" x="2.475" y="-2.475" drill="1.1" thermals="no"/>
<pad name="6" x="-2.475" y="-2.475" drill="1.1" thermals="no"/>
<circle x="0" y="0" radius="9.6" width="0.127" layer="21"/>
<circle x="0" y="0" radius="6.5405" width="0.127" layer="51"/>
<pad name="PAD" x="-4.123" y="7.2342" drill="1.2" thermals="no"/>
<wire x1="1" y1="-5.16" x2="-1" y2="-5.16" width="0.127" layer="21" curve="-180"/>
<wire x1="1" y1="-5.16" x2="1" y2="-4.16" width="0.127" layer="21"/>
<wire x1="-1" y1="-5.16" x2="-1" y2="-4.16" width="0.127" layer="21"/>
<text x="-6" y="-0.5" size="1.4224" layer="21" font="vector" ratio="15">1</text>
<text x="5" y="-0.5" size="1.4224" layer="21" font="vector" ratio="15">3</text>
<text x="-5" y="2" size="1.4224" layer="21" font="vector" ratio="15">4</text>
<text x="-5" y="-3" size="1.4224" layer="21" font="vector" ratio="15">6</text>
<text x="4" y="-3" size="1.4224" layer="21" font="vector" ratio="15">7</text>
<text x="4" y="2" size="1.4224" layer="21" font="vector" ratio="15">5</text>
<text x="-5" y="-0.5" size="1.4224" layer="22" font="vector" ratio="15" rot="MR0">1</text>
<text x="6" y="-0.5" size="1.4224" layer="22" font="vector" ratio="15" rot="MR0">3</text>
<text x="-4" y="2" size="1.4224" layer="22" font="vector" ratio="15" rot="MR0">4</text>
<text x="5" y="2" size="1.4224" layer="22" font="vector" ratio="15" rot="MR0">5</text>
<text x="5" y="-3" size="1.4224" layer="22" font="vector" ratio="15" rot="MR0">7</text>
<text x="-4" y="-3" size="1.4224" layer="22" font="vector" ratio="15" rot="MR0">6</text>
<text x="7.62" y="-7.62" size="1.4224" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="8.89" y="-5.08" size="1.4224" layer="27" font="vector" ratio="15">&gt;VALUE</text>
<text x="-2.54" y="-8.255" size="1.016" layer="25" font="vector" ratio="15">FEMALE</text>
<pad name="2" x="0" y="3.5" drill="1.1"/>
<pad name="8" x="0" y="-0.7" drill="1.1"/>
<text x="-0.5" y="5" size="1.4224" layer="21" font="vector" ratio="15">2</text>
<text x="0.5" y="5" size="1.4224" layer="22" font="vector" ratio="15" rot="MR0">2</text>
<text x="-0.5" y="0.5" size="1.4224" layer="21" font="vector" ratio="15">8</text>
<text x="0.5" y="0.5" size="1.4224" layer="22" font="vector" ratio="15" rot="MR0">8</text>
<text x="-6.35" y="5.08" size="1.4224" layer="21" font="vector" ratio="15">P</text>
<text x="-5.08" y="5.08" size="1.4224" layer="22" font="vector" ratio="15" rot="MR0">P</text>
</package>
<package name="DO-214AB_SMC">
<smd name="1" x="-3.5685" y="0" dx="2.794" dy="3.81" layer="1"/>
<smd name="2" x="3.5685" y="0" dx="2.794" dy="3.81" layer="1"/>
<wire x1="-4" y1="2.5" x2="-4" y2="3" width="0.127" layer="21"/>
<wire x1="-4" y1="3" x2="4" y2="3" width="0.127" layer="21"/>
<wire x1="4" y1="3" x2="4" y2="2.5" width="0.127" layer="21"/>
<wire x1="-4" y1="-2.5" x2="-4" y2="-3" width="0.127" layer="21"/>
<wire x1="-4" y1="-3" x2="4" y2="-3" width="0.127" layer="21"/>
<wire x1="4" y1="-3" x2="4" y2="-2.5" width="0.127" layer="21"/>
<wire x1="-4" y1="3" x2="4" y2="3" width="0.127" layer="51"/>
<wire x1="-4" y1="-3" x2="4" y2="-3" width="0.127" layer="51"/>
<wire x1="-4" y1="3" x2="-4" y2="2" width="0.127" layer="51"/>
<wire x1="-4" y1="2" x2="-5" y2="2" width="0.127" layer="51"/>
<wire x1="-5" y1="2" x2="-5" y2="-2" width="0.127" layer="51"/>
<wire x1="-5" y1="-2" x2="-4" y2="-2" width="0.127" layer="51"/>
<wire x1="-4" y1="-2" x2="-4" y2="-3" width="0.127" layer="51"/>
<wire x1="4" y1="3" x2="4" y2="2" width="0.127" layer="51"/>
<wire x1="4" y1="2" x2="5" y2="2" width="0.127" layer="51"/>
<wire x1="5" y1="2" x2="5" y2="-2" width="0.127" layer="51"/>
<wire x1="5" y1="-2" x2="4" y2="-2" width="0.127" layer="51"/>
<wire x1="4" y1="-2" x2="4" y2="-3" width="0.127" layer="51"/>
<wire x1="-5.5" y1="3" x2="-5.5" y2="-3" width="0.127" layer="51"/>
<wire x1="-5.5" y1="3" x2="-5.5" y2="-3" width="0.127" layer="21"/>
<text x="-5.08" y="3.81" size="0.8128" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="0" y="3.81" size="0.8128" layer="27" font="vector" ratio="15">&gt;VALUE</text>
<wire x1="-5.5" y1="3" x2="-5.5" y2="-3" width="0.127" layer="51"/>
</package>
<package name="RES5750X230N">
<wire x1="2.925" y1="3.72" x2="2.925" y2="-3.72" width="0.127" layer="21"/>
<wire x1="2.925" y1="-3.72" x2="-2.925" y2="-3.72" width="0.127" layer="21"/>
<wire x1="-2.925" y1="-3.72" x2="-2.925" y2="3.72" width="0.127" layer="21"/>
<wire x1="-2.925" y1="3.72" x2="2.925" y2="3.72" width="0.127" layer="21"/>
<smd name="1" x="0" y="2.6" dx="1.4" dy="5.25" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-2.6" dx="1.4" dy="5.25" layer="1" roundness="25" rot="R270"/>
<text x="4" y="-2" size="0.635" layer="25" ratio="11" rot="R90">&gt;NAME</text>
<text x="-3" y="-2" size="0.635" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<wire x1="2.925" y1="3.72" x2="2.925" y2="-3.72" width="0.127" layer="51"/>
<wire x1="2.925" y1="-3.72" x2="-2.925" y2="-3.72" width="0.127" layer="51"/>
<wire x1="-2.925" y1="-3.72" x2="-2.925" y2="3.72" width="0.127" layer="51"/>
<wire x1="-2.925" y1="3.72" x2="2.925" y2="3.72" width="0.127" layer="51"/>
</package>
<package name="CAPC2013X145N">
<wire x1="0.925" y1="1.72" x2="0.925" y2="-1.72" width="0.127" layer="21"/>
<wire x1="0.925" y1="-1.72" x2="-0.925" y2="-1.72" width="0.127" layer="21"/>
<wire x1="-0.925" y1="-1.72" x2="-0.925" y2="1.72" width="0.127" layer="21"/>
<wire x1="-0.925" y1="1.72" x2="0.925" y2="1.72" width="0.127" layer="21"/>
<smd name="1" x="0" y="0.9" dx="1.15" dy="1.45" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-0.9" dx="1.15" dy="1.45" layer="1" roundness="25" rot="R270"/>
<text x="2.111" y="-2.02" size="0.889" layer="25" ratio="11" rot="R90">&gt;NAME</text>
<text x="-1.1625" y="-1.52" size="0.635" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.27" y1="-0.508" x2="1.27" y2="0.508" layer="39" rot="R270"/>
<wire x1="0.925" y1="1.72" x2="0.925" y2="-1.72" width="0.127" layer="51"/>
<wire x1="0.925" y1="-1.72" x2="-0.925" y2="-1.72" width="0.127" layer="51"/>
<wire x1="-0.925" y1="-1.72" x2="-0.925" y2="1.72" width="0.127" layer="51"/>
<wire x1="-0.925" y1="1.72" x2="0.925" y2="1.72" width="0.127" layer="51"/>
</package>
<package name="0.5MM_FIDUCIAL">
<wire x1="0" y1="1" x2="0" y2="-1" width="0.0508" layer="51"/>
<wire x1="-1" y1="0" x2="1" y2="0" width="0.0508" layer="51"/>
<circle x="0" y="0" radius="0.5" width="0" layer="1"/>
<circle x="0" y="0" radius="0.889" width="0" layer="29"/>
<circle x="0" y="0" radius="1" width="0.0508" layer="51"/>
<circle x="0" y="0" radius="1.0307" width="0" layer="41"/>
<circle x="0" y="0" radius="0.5" width="0.0508" layer="51"/>
</package>
<package name="CAPC3216X190N">
<wire x1="1.27" y1="2.54" x2="1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="1.27" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-2.54" x2="-1.27" y2="2.54" width="0.127" layer="21"/>
<wire x1="-1.27" y1="2.54" x2="1.27" y2="2.54" width="0.127" layer="21"/>
<smd name="1" x="0" y="1.5" dx="1.15" dy="1.8" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-1.5" dx="1.15" dy="1.8" layer="1" roundness="25" rot="R270"/>
<text x="2.361" y="-2.02" size="0.889" layer="25" ratio="11" rot="R90">&gt;NAME</text>
<text x="-1.4125" y="-1.52" size="0.635" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.27" y1="-0.75" x2="1.27" y2="0.75" layer="39" rot="R270"/>
<wire x1="0.925" y1="1.72" x2="0.925" y2="-1.72" width="0.127" layer="51"/>
<wire x1="0.925" y1="-1.72" x2="-0.925" y2="-1.72" width="0.127" layer="51"/>
<wire x1="-0.925" y1="-1.72" x2="-0.925" y2="1.72" width="0.127" layer="51"/>
<wire x1="-0.925" y1="1.72" x2="0.925" y2="1.72" width="0.127" layer="51"/>
</package>
<package name="CAPC4532X279N">
<wire x1="2.175" y1="3.22" x2="2.175" y2="-3.22" width="0.127" layer="21"/>
<wire x1="2.175" y1="-3.22" x2="-2.175" y2="-3.22" width="0.127" layer="21"/>
<wire x1="-2.175" y1="-3.22" x2="-2.175" y2="3.22" width="0.127" layer="21"/>
<wire x1="-2.175" y1="3.22" x2="2.175" y2="3.22" width="0.127" layer="21"/>
<smd name="1" x="0" y="2.05" dx="1.4" dy="3.4" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-2.05" dx="1.4" dy="3.4" layer="1" roundness="25" rot="R270"/>
<text x="3.361" y="-2.02" size="0.889" layer="25" ratio="11" rot="R90">&gt;NAME</text>
<text x="-2.6625" y="-1.52" size="0.635" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-2.264" y1="-1.494" x2="2.256" y2="1.514" layer="39" rot="R270"/>
<wire x1="1.56" y1="2.331" x2="1.56" y2="-2.355" width="0.127" layer="51"/>
<wire x1="1.56" y1="-2.355" x2="-1.56" y2="-2.355" width="0.127" layer="51"/>
<wire x1="-1.56" y1="-2.355" x2="-1.56" y2="2.331" width="0.127" layer="51"/>
<wire x1="-1.56" y1="2.331" x2="1.56" y2="2.331" width="0.127" layer="51"/>
</package>
<package name="AISG_CUSTOM_C091-G006-804-2">
<pad name="3" x="3.5" y="0" drill="1.1"/>
<pad name="1" x="-3.5" y="0" drill="1.1"/>
<pad name="5" x="2.475" y="2.475" drill="1.1"/>
<pad name="4" x="-2.475" y="2.475" drill="1.1"/>
<pad name="7" x="2.475" y="-2.475" drill="1.1"/>
<pad name="6" x="-2.475" y="-2.475" drill="1.1"/>
<circle x="0" y="0" radius="6.5405" width="0.127" layer="51"/>
<pad name="PAD" x="-4.123" y="7.2342" drill="1.2"/>
<wire x1="1" y1="-5.16" x2="-1" y2="-5.16" width="0.127" layer="21" curve="-180"/>
<wire x1="1" y1="-5.16" x2="1" y2="-4.16" width="0.127" layer="21"/>
<wire x1="-1" y1="-5.16" x2="-1" y2="-4.16" width="0.127" layer="21"/>
<text x="-6" y="-0.5" size="1.4224" layer="21" font="vector" ratio="15">1</text>
<text x="5" y="-0.5" size="1.4224" layer="21" font="vector" ratio="15">3</text>
<text x="-5" y="2" size="1.4224" layer="21" font="vector" ratio="15">4</text>
<text x="-5" y="-3" size="1.4224" layer="21" font="vector" ratio="15">6</text>
<text x="4" y="-3" size="1.4224" layer="21" font="vector" ratio="15">7</text>
<text x="4" y="2" size="1.4224" layer="21" font="vector" ratio="15">5</text>
<text x="-5" y="-0.5" size="1.4224" layer="22" font="vector" ratio="15" rot="MR0">1</text>
<text x="6" y="-0.5" size="1.4224" layer="22" font="vector" ratio="15" rot="MR0">3</text>
<text x="-4" y="2" size="1.4224" layer="22" font="vector" ratio="15" rot="MR0">4</text>
<text x="5" y="2" size="1.4224" layer="22" font="vector" ratio="15" rot="MR0">5</text>
<text x="5" y="-3" size="1.4224" layer="22" font="vector" ratio="15" rot="MR0">7</text>
<text x="-4" y="-3" size="1.4224" layer="22" font="vector" ratio="15" rot="MR0">6</text>
<text x="10.16" y="3.81" size="1.4224" layer="25" font="vector" ratio="15" rot="R270">&gt;NAME</text>
<text x="7.62" y="3.81" size="1.4224" layer="27" font="vector" ratio="15" rot="R270">&gt;VALUE</text>
<text x="-2.54" y="6.985" size="1.016" layer="25" font="vector" ratio="15">FEMALE</text>
<pad name="2" x="0" y="3.5" drill="1.1"/>
<pad name="8" x="0" y="-0.7" drill="1.1"/>
<text x="-0.5" y="5" size="1.4224" layer="21" font="vector" ratio="15">2</text>
<text x="0.5" y="5" size="1.4224" layer="22" font="vector" ratio="15" rot="MR0">2</text>
<text x="-0.5" y="0.5" size="1.4224" layer="21" font="vector" ratio="15">8</text>
<text x="0.5" y="0.5" size="1.4224" layer="22" font="vector" ratio="15" rot="MR0">8</text>
<text x="-6.35" y="5.08" size="1.4224" layer="21" font="vector" ratio="15">P</text>
<text x="-5.08" y="5.08" size="1.4224" layer="22" font="vector" ratio="15" rot="MR0">P</text>
</package>
<package name="GDT_2029-XX-SMLF">
<smd name="1" x="-3.016" y="-0.254" dx="8.25" dy="1.78" layer="1" rot="R90" thermals="no"/>
<smd name="2" x="2.824" y="-0.254" dx="8.25" dy="1.78" layer="1" rot="R90" thermals="no"/>
<wire x1="-4.116" y1="4.146" x2="3.884" y2="4.146" width="0.127" layer="21"/>
<wire x1="3.884" y1="4.146" x2="3.884" y2="-4.654" width="0.127" layer="21"/>
<wire x1="3.884" y1="-4.654" x2="-4.116" y2="-4.654" width="0.127" layer="21"/>
<wire x1="-4.116" y1="-4.654" x2="-4.116" y2="4.146" width="0.127" layer="21"/>
<wire x1="-4.116" y1="4.146" x2="3.884" y2="4.146" width="0.127" layer="51"/>
<wire x1="3.884" y1="4.146" x2="3.884" y2="-4.654" width="0.127" layer="51"/>
<wire x1="3.884" y1="-4.654" x2="-4.116" y2="-4.654" width="0.127" layer="51"/>
<wire x1="-4.116" y1="-4.654" x2="-4.116" y2="4.146" width="0.127" layer="51"/>
<text x="-4.016" y="4.746" size="1.27" layer="25">&gt;NAME</text>
<text x="-4.016" y="6.746" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="JST_S04B-PASK-2">
<pad name="1" x="3" y="-2.35" drill="0.7"/>
<hole x="4.5" y="-0.25" drill="1.1"/>
<pad name="2" x="1" y="-2.35" drill="0.7"/>
<pad name="3" x="-1" y="-2.35" drill="0.7"/>
<pad name="4" x="-3" y="-2.35" drill="0.7"/>
<hole x="-4.5" y="-0.25" drill="1.1"/>
<wire x1="-5" y1="5.85" x2="5" y2="5.85" width="0.127" layer="21"/>
<wire x1="-5" y1="-5.85" x2="5" y2="-5.85" width="0.127" layer="21"/>
<wire x1="-5" y1="5.85" x2="-5" y2="0.27" width="0.127" layer="21"/>
<wire x1="5" y1="5.85" x2="5" y2="0.27" width="0.127" layer="21"/>
<wire x1="-5" y1="-5.85" x2="-5" y2="-0.77" width="0.127" layer="21"/>
<wire x1="5" y1="-5.85" x2="5" y2="-0.77" width="0.127" layer="21"/>
<wire x1="-5" y1="5.85" x2="5" y2="5.85" width="0.127" layer="51"/>
<wire x1="-5" y1="-5.85" x2="5" y2="-5.85" width="0.127" layer="51"/>
<wire x1="-5" y1="5.85" x2="-5" y2="0.27" width="0.127" layer="51"/>
<wire x1="5" y1="5.85" x2="5" y2="0.27" width="0.127" layer="51"/>
<wire x1="-5" y1="-5.85" x2="-5" y2="-0.77" width="0.127" layer="51"/>
<wire x1="5" y1="-5.85" x2="5" y2="-0.77" width="0.127" layer="51"/>
<text x="-0.95" y="-7.35" size="1.27" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="-9.2" y="-7.35" size="1.27" layer="27" font="vector" ratio="15">&gt;VALUE</text>
<circle x="3" y="-4" radius="0.25" width="0.127" layer="21"/>
<circle x="3" y="-4" radius="0.13" width="0.127" layer="21"/>
<circle x="3" y="-4" radius="0.014140625" width="0.127" layer="21"/>
<circle x="3" y="-4" radius="0.25" width="0.127" layer="51"/>
<circle x="3" y="-4" radius="0.13" width="0.127" layer="51"/>
<circle x="3" y="-4" radius="0.014140625" width="0.127" layer="51"/>
</package>
<package name="JST_B04B-PASK">
<pad name="P$1" x="3" y="0" drill="0.7" rot="R90"/>
<pad name="P$2" x="1" y="0" drill="0.7" rot="R90"/>
<pad name="P$3" x="-1" y="0" drill="0.7" rot="R90"/>
<pad name="P$4" x="-3" y="0" drill="0.7" rot="R90"/>
<hole x="4.5" y="1.7" drill="1.1"/>
<wire x1="-5" y1="-3.05" x2="5" y2="-3.05" width="0.127" layer="21"/>
<wire x1="-5" y1="2.25" x2="4.03" y2="2.25" width="0.127" layer="21"/>
<wire x1="-5" y1="2.25" x2="-5" y2="-3.05" width="0.127" layer="21"/>
<wire x1="5" y1="-3.05" x2="5" y2="1.17" width="0.127" layer="21"/>
<wire x1="-5" y1="-3.05" x2="5" y2="-3.05" width="0.127" layer="51"/>
<wire x1="-5" y1="2.25" x2="4.03" y2="2.25" width="0.127" layer="51"/>
<wire x1="-5" y1="2.25" x2="-5" y2="-3.05" width="0.127" layer="51"/>
<wire x1="5" y1="-3.05" x2="5" y2="1.17" width="0.127" layer="51"/>
<text x="-2.5" y="2.48" size="1.27" layer="25">&gt;NAME</text>
<text x="-10.75" y="2.48" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="DO-214AA_SMB">
<smd name="A" x="-2.15" y="0" dx="2.5" dy="2.3" layer="1" rot="R180"/>
<smd name="K" x="2.15" y="0" dx="2.5" dy="2.3" layer="1" rot="R180"/>
<wire x1="-2.29" y1="1.97" x2="2.29" y2="1.97" width="0.127" layer="21"/>
<wire x1="-2.29" y1="-1.97" x2="2.29" y2="-1.97" width="0.127" layer="21"/>
<wire x1="-2.29" y1="1.97" x2="-2.29" y2="1.32" width="0.127" layer="21"/>
<wire x1="-2.29" y1="-1.97" x2="-2.29" y2="-1.32" width="0.127" layer="21"/>
<wire x1="2.29" y1="1.97" x2="2.29" y2="1.32" width="0.127" layer="21"/>
<wire x1="2.29" y1="-1.97" x2="2.29" y2="-1.32" width="0.127" layer="21"/>
<circle x="2.7" y="1.6" radius="0.1" width="0.127" layer="21"/>
<wire x1="2.7" y1="1.6" x2="2.8" y2="1.6" width="0.127" layer="21"/>
<circle x="2.7" y="1.6" radius="0.14141875" width="0.127" layer="21"/>
<text x="-3" y="2.2" size="1.27" layer="25">&gt;NAME</text>
<text x="-3" y="-3.5" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-2.29" y1="1.97" x2="2.29" y2="1.97" width="0.127" layer="51"/>
<wire x1="-2.29" y1="-1.97" x2="2.29" y2="-1.97" width="0.127" layer="51"/>
<wire x1="-2.29" y1="1.97" x2="-2.29" y2="1.32" width="0.127" layer="51"/>
<wire x1="-2.29" y1="-1.97" x2="-2.29" y2="-1.32" width="0.127" layer="51"/>
<wire x1="2.29" y1="1.97" x2="2.29" y2="1.32" width="0.127" layer="51"/>
<wire x1="2.29" y1="-1.97" x2="2.29" y2="-1.32" width="0.127" layer="51"/>
<circle x="2.7" y="1.6" radius="0.1" width="0.127" layer="51"/>
<wire x1="2.7" y1="1.6" x2="2.8" y2="1.6" width="0.127" layer="51"/>
<circle x="2.7" y="1.6" radius="0.14141875" width="0.127" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="CAPACITOR">
<wire x1="-0.635" y1="-1.016" x2="-0.635" y2="0" width="0.254" layer="94"/>
<wire x1="-0.635" y1="0" x2="-0.635" y2="1.016" width="0.254" layer="94"/>
<wire x1="0.635" y1="1.016" x2="0.635" y2="0" width="0.254" layer="94"/>
<wire x1="0.635" y1="0" x2="0.635" y2="-1.016" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-0.635" y2="0" width="0.1524" layer="94"/>
<wire x1="0.635" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<text x="-1.27" y="1.27" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="PIN">
<pin name="1" x="-5.08" y="0" visible="off"/>
<wire x1="-3.302" y1="0" x2="-1.27" y2="0" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="0" x2="2.54" y2="0" width="0.8128" layer="94"/>
<text x="-3.302" y="0.508" size="1.27" layer="95" ratio="15">&gt;NAME</text>
</symbol>
<symbol name="NC">
<wire x1="-0.635" y1="0.635" x2="0.635" y2="-0.635" width="0.254" layer="94"/>
<wire x1="0.635" y1="0.635" x2="-0.635" y2="-0.635" width="0.254" layer="94"/>
</symbol>
<symbol name="VALUE_LABEL">
<text x="0" y="0" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="ZENER">
<wire x1="-1.27" y1="-1.524" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="1.524" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.524" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="-1.524" width="0.254" layer="94"/>
<text x="-7.62" y="1.27" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-5.08" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
<pin name="+" x="-5.08" y="0" visible="pin" length="short" direction="pas"/>
<pin name="-" x="5.08" y="0" visible="pin" length="short" direction="pas" rot="R180"/>
<wire x1="-1.27" y1="0" x2="-2.54" y2="0" width="0.15748125" layer="94"/>
<wire x1="1.27" y1="-1.397" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="1.397" width="0.254" layer="94"/>
<wire x1="1.27" y1="-1.397" x2="0.889" y2="-1.778" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.397" x2="1.651" y2="1.778" width="0.254" layer="94"/>
<wire x1="2.7178" y1="0" x2="1.1938" y2="0" width="0.15748125" layer="94"/>
</symbol>
<symbol name="GDT">
<polygon width="0.254" layer="94">
<vertex x="-0.635" y="1.27"/>
<vertex x="0.635" y="1.27"/>
<vertex x="0.635" y="0.635"/>
<vertex x="-0.635" y="0.635"/>
</polygon>
<wire x1="-0.635" y1="-0.635" x2="-0.635" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-0.635" y1="-1.27" x2="0.635" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0.635" y1="-1.27" x2="0.635" y2="-0.635" width="0.254" layer="94"/>
<wire x1="0.635" y1="-0.635" x2="-0.635" y2="-0.635" width="0.254" layer="94"/>
<pin name="1" x="0" y="5.08" visible="pad" length="short" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="pad" length="short" rot="R90"/>
<text x="5.08" y="-2.54" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<wire x1="0" y1="-2.54" x2="2.54" y2="0" width="0.254" layer="94" curve="90"/>
<wire x1="2.54" y1="0" x2="0" y2="2.54" width="0.254" layer="94" curve="90"/>
<wire x1="0" y1="2.54" x2="-2.54" y2="0" width="0.254" layer="94" curve="90"/>
<wire x1="-2.54" y1="0" x2="0" y2="-2.54" width="0.254" layer="94" curve="90"/>
</symbol>
<symbol name="VARISTOR_ESD">
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="0" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="1.905" x2="0" y2="0" width="0.254" layer="94"/>
<text x="-7.62" y="1.27" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-5.08" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
<pin name="1" x="-7.62" y="0" visible="off" length="short" direction="pas"/>
<pin name="2" x="7.62" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<wire x1="0" y1="0" x2="0" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-5.08" y2="0" width="0.15875" layer="94"/>
<wire x1="0" y1="0" x2="2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="5.08" y2="0" width="0.16001875" layer="94"/>
<wire x1="0" y1="1.905" x2="0.635" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-1.905" x2="-0.635" y2="-2.54" width="0.254" layer="94"/>
</symbol>
<symbol name="FIDUCIAL">
<circle x="0" y="0" radius="3.81" width="0" layer="94"/>
<circle x="0" y="0" radius="6.35" width="0.254" layer="94"/>
<text x="-4.826" y="8.89" size="1.778" layer="95">&gt;NAME</text>
</symbol>
<symbol name="ZENER_BIDIRECTIONAL">
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="0" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-2.54" y2="-2.54" width="0.254" layer="94"/>
<text x="-8.89" y="1.27" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="-6.35" y="-5.08" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
<pin name="+" x="-5.08" y="0" visible="pin" length="short" direction="pas"/>
<pin name="-" x="5.08" y="0" visible="pin" length="short" direction="pas" rot="R180"/>
<wire x1="-2.54" y1="0" x2="-3.81" y2="0" width="0.15875" layer="94"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="3.81" y2="0" width="0.15875" layer="94"/>
<wire x1="0" y1="1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-1.905" width="0.254" layer="94"/>
<wire x1="0" y1="1.905" x2="0.635" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-1.905" x2="-0.635" y2="-2.54" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CAP_SMD_" prefix="C" uservalue="yes">
<gates>
<gate name="G$1" symbol="CAPACITOR" x="0" y="0"/>
</gates>
<devices>
<device name="0402" package="CAPC1005X55N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="CAPC1608X55N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="CAPC2013X145N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="CAPC3216X190N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1812" package="CAPC4532X279N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="NC" prefix="NC">
<gates>
<gate name="G$1" symbol="NC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="AISG_" prefix="J" uservalue="yes">
<gates>
<gate name="-1" symbol="PIN" x="5.08" y="0"/>
<gate name="-PAD" symbol="PIN" x="5.08" y="-20.32"/>
<gate name="-3" symbol="PIN" x="5.08" y="-5.08"/>
<gate name="-4" symbol="PIN" x="5.08" y="-7.62"/>
<gate name="-5" symbol="PIN" x="5.08" y="-10.16"/>
<gate name="-6" symbol="PIN" x="5.08" y="-12.7"/>
<gate name="-7" symbol="PIN" x="5.08" y="-15.24"/>
<gate name="G$1" symbol="VALUE_LABEL" x="2.54" y="2.54"/>
<gate name="-2" symbol="PIN" x="5.08" y="-2.54"/>
<gate name="-8" symbol="PIN" x="5.08" y="-17.78"/>
</gates>
<devices>
<device name="MALE" package="AISG_C091-C006-804-2">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-2" pin="1" pad="2"/>
<connect gate="-3" pin="1" pad="3"/>
<connect gate="-4" pin="1" pad="4"/>
<connect gate="-5" pin="1" pad="5"/>
<connect gate="-6" pin="1" pad="6"/>
<connect gate="-7" pin="1" pad="7"/>
<connect gate="-8" pin="1" pad="8"/>
<connect gate="-PAD" pin="1" pad="PAD"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="FEMALE" package="AISG_C091-G006-804-2">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-2" pin="1" pad="2"/>
<connect gate="-3" pin="1" pad="3"/>
<connect gate="-4" pin="1" pad="4"/>
<connect gate="-5" pin="1" pad="5"/>
<connect gate="-6" pin="1" pad="6"/>
<connect gate="-7" pin="1" pad="7"/>
<connect gate="-8" pin="1" pad="8"/>
<connect gate="-PAD" pin="1" pad="PAD"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="FEMALE_NO_SILKSCREEN" package="AISG_CUSTOM_C091-G006-804-2">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-2" pin="1" pad="2"/>
<connect gate="-3" pin="1" pad="3"/>
<connect gate="-4" pin="1" pad="4"/>
<connect gate="-5" pin="1" pad="5"/>
<connect gate="-6" pin="1" pad="6"/>
<connect gate="-7" pin="1" pad="7"/>
<connect gate="-8" pin="1" pad="8"/>
<connect gate="-PAD" pin="1" pad="PAD"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ZENER_ONSEMI_1SMC5.0AT3G" prefix="D" uservalue="yes">
<gates>
<gate name="G$1" symbol="ZENER" x="0" y="-2.54"/>
</gates>
<devices>
<device name="" package="DO-214AB_SMC">
<connects>
<connect gate="G$1" pin="+" pad="2"/>
<connect gate="G$1" pin="-" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="AVX_VC15MA0340KBA" prefix="D" uservalue="yes">
<gates>
<gate name="G$1" symbol="VARISTOR_ESD" x="0" y="0"/>
</gates>
<devices>
<device name="2220" package="RES5750X230N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="0.5MM_FIDUCIAL" prefix="F">
<gates>
<gate name="G$1" symbol="FIDUCIAL" x="0" y="0"/>
</gates>
<devices>
<device name="" package="0.5MM_FIDUCIAL">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GDT_2029-XX-SMLF" prefix="F" uservalue="yes">
<gates>
<gate name="G$1" symbol="GDT" x="0" y="0"/>
</gates>
<devices>
<device name="" package="GDT_2029-XX-SMLF">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="JST_PA_4" prefix="J" uservalue="yes">
<gates>
<gate name="-1" symbol="PIN" x="5.08" y="0"/>
<gate name="-2" symbol="PIN" x="5.08" y="-2.54"/>
<gate name="-3" symbol="PIN" x="5.08" y="-5.08"/>
<gate name="-4" symbol="PIN" x="5.08" y="-7.62"/>
</gates>
<devices>
<device name="THT_TOP" package="JST_B04B-PASK">
<connects>
<connect gate="-1" pin="1" pad="P$1"/>
<connect gate="-2" pin="1" pad="P$2"/>
<connect gate="-3" pin="1" pad="P$3"/>
<connect gate="-4" pin="1" pad="P$4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="THT_SIDE" package="JST_S04B-PASK-2">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-2" pin="1" pad="2"/>
<connect gate="-3" pin="1" pad="3"/>
<connect gate="-4" pin="1" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DIODE_DO214AA-SMB" prefix="D" uservalue="yes">
<gates>
<gate name="G$1" symbol="ZENER_BIDIRECTIONAL" x="5.08" y="0"/>
</gates>
<devices>
<device name="" package="DO-214AA_SMB">
<connects>
<connect gate="G$1" pin="+" pad="A"/>
<connect gate="G$1" pin="-" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply2">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
Please keep in mind, that these devices are necessary for the
automatic wiring of the supply signals.&lt;p&gt;
The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND">
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="-1.27" y2="0" width="0.254" layer="94"/>
<text x="-1.905" y="-3.175" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="SUPPLY">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="GND" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="AGND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<wire x1="-1.0922" y1="-0.508" x2="1.0922" y2="-0.508" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="AGND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="AGND" prefix="AGND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="VR1" symbol="AGND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="FRAME1" library="frames" deviceset="A4L-LOC-JMA" device=""/>
<part name="J2" library="JMA_LBR" deviceset="AISG_" device="FEMALE" value="AISG_C091-G006-804-2(F)"/>
<part name="J1" library="JMA_LBR" deviceset="AISG_" device="MALE" value="AISG G091-G006-804-2(M)"/>
<part name="D1" library="JMA_LBR" deviceset="ZENER_ONSEMI_1SMC5.0AT3G" device="" value="1SMC33AT3G"/>
<part name="AGND6" library="supply1" deviceset="AGND" device=""/>
<part name="AGND2" library="supply1" deviceset="AGND" device=""/>
<part name="AGND3" library="supply1" deviceset="AGND" device=""/>
<part name="AGND4" library="supply1" deviceset="AGND" device=""/>
<part name="AGND5" library="supply1" deviceset="AGND" device=""/>
<part name="SUPPLY5" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY6" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY1" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY2" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY3" library="supply2" deviceset="GND" device=""/>
<part name="D4" library="JMA_LBR" deviceset="AVX_VC15MA0340KBA" device="2220" value="VC15MA0340KBA"/>
<part name="SUPPLY4" library="supply2" deviceset="GND" device=""/>
<part name="AGND1" library="supply1" deviceset="AGND" device=""/>
<part name="NC1" library="JMA_LBR" deviceset="NC" device=""/>
<part name="NC2" library="JMA_LBR" deviceset="NC" device=""/>
<part name="F5" library="JMA_LBR" deviceset="0.5MM_FIDUCIAL" device=""/>
<part name="F6" library="JMA_LBR" deviceset="0.5MM_FIDUCIAL" device=""/>
<part name="F7" library="JMA_LBR" deviceset="0.5MM_FIDUCIAL" device=""/>
<part name="C1" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="15pf, 100V"/>
<part name="C2" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="15pf, 100V"/>
<part name="F1" library="JMA_LBR" deviceset="GDT_2029-XX-SMLF" device="" value="2029-09-SM-RPLF"/>
<part name="F2" library="JMA_LBR" deviceset="GDT_2029-XX-SMLF" device="" value="2029-09-SM-RPLF"/>
<part name="F3" library="JMA_LBR" deviceset="GDT_2029-XX-SMLF" device="" value="2029-09-SM-RPLF"/>
<part name="F4" library="JMA_LBR" deviceset="GDT_2029-XX-SMLF" device="" value="2029-09-SM-RPLF"/>
<part name="J3" library="JMA_LBR" deviceset="JST_PA_4" device="THT_TOP" value="JST_B04B-PASK"/>
<part name="J4" library="JMA_LBR" deviceset="JST_PA_4" device="THT_TOP" value="JST_B04B-PASK"/>
<part name="D2" library="JMA_LBR" deviceset="DIODE_DO214AA-SMB" device="" value="SMBJ11CA-13-F"/>
<part name="D3" library="JMA_LBR" deviceset="DIODE_DO214AA-SMB" device="" value="SMBJ11CA-13-F"/>
</parts>
<sheets>
<sheet>
<plain>
<wire x1="66.04" y1="78.74" x2="48.26" y2="78.74" width="0.1524" layer="93"/>
<text x="48.26" y="124.46" size="1.27" layer="97" align="bottom-right">A</text>
<text x="48.26" y="134.62" size="1.27" layer="97" align="bottom-right">B</text>
<text x="48.26" y="114.3" size="1.27" layer="97" align="bottom-right">DC RETURN</text>
<text x="48.26" y="119.38" size="1.27" layer="97" align="bottom-right">10-30V</text>
<text x="48.26" y="144.78" size="1.27" layer="97" align="bottom-right">+12V</text>
<text x="48.26" y="129.54" size="1.27" layer="97" align="bottom-right">GND</text>
<text x="48.26" y="104.14" size="1.27" layer="97" align="bottom-right">PAD</text>
<text x="48.26" y="73.66" size="1.27" layer="97" align="bottom-right">A</text>
<text x="48.26" y="83.82" size="1.27" layer="97" align="bottom-right">B</text>
<text x="48.26" y="63.5" size="1.27" layer="97" align="bottom-right">DC RETURN</text>
<text x="48.26" y="68.58" size="1.27" layer="97" align="bottom-right">10-30V</text>
<text x="48.26" y="93.98" size="1.27" layer="97" align="bottom-right">+12V</text>
<text x="48.26" y="78.74" size="1.27" layer="97" align="bottom-right">GND</text>
<text x="48.26" y="53.34" size="1.27" layer="97" align="bottom-right">PAD</text>
<text x="48.26" y="149.86" size="1.27" layer="97">AISG CONNECTOR INTERFACE + SURGE PROTECTION</text>
<text x="48.26" y="139.7" size="1.27" layer="97" align="bottom-right">-48V</text>
<text x="48.26" y="109.22" size="1.27" layer="97" align="bottom-right">NC</text>
<text x="48.26" y="58.42" size="1.27" layer="97" align="bottom-right">NC</text>
<text x="48.26" y="88.9" size="1.27" layer="97" align="bottom-right">-48V</text>
</plain>
<instances>
<instance part="FRAME1" gate="G$1" x="0" y="0" smashed="yes">
<attribute name="DRAWING_NAME" x="217.17" y="15.24" size="1.27" layer="94"/>
<attribute name="LAST_DATE_TIME" x="217.17" y="10.16" size="1.27" layer="94"/>
<attribute name="SHEET" x="230.505" y="5.08" size="1.27" layer="94"/>
</instance>
<instance part="F5" gate="G$1" x="200.66" y="53.34" smashed="yes">
<attribute name="NAME" x="198.374" y="62.23" size="1.27" layer="125"/>
</instance>
<instance part="F6" gate="G$1" x="220.98" y="53.34" smashed="yes">
<attribute name="NAME" x="218.694" y="62.23" size="1.27" layer="125"/>
</instance>
<instance part="F7" gate="G$1" x="241.3" y="53.34" smashed="yes">
<attribute name="NAME" x="239.014" y="62.23" size="1.27" layer="125"/>
</instance>
<instance part="J2" gate="-1" x="53.34" y="93.98" smashed="yes" rot="MR0">
<attribute name="NAME" x="56.642" y="94.488" size="1.27" layer="95" ratio="15" rot="MR0"/>
</instance>
<instance part="J2" gate="-PAD" x="53.34" y="53.34" smashed="yes" rot="MR0">
<attribute name="NAME" x="56.642" y="53.848" size="1.27" layer="95" ratio="15" rot="MR0"/>
</instance>
<instance part="J2" gate="-3" x="53.34" y="83.82" smashed="yes" rot="MR0">
<attribute name="NAME" x="56.642" y="84.328" size="1.27" layer="95" ratio="15" rot="MR0"/>
</instance>
<instance part="J2" gate="-4" x="53.34" y="78.74" smashed="yes" rot="MR0">
<attribute name="NAME" x="56.642" y="79.248" size="1.27" layer="95" ratio="15" rot="MR0"/>
</instance>
<instance part="J2" gate="-5" x="53.34" y="73.66" smashed="yes" rot="MR0">
<attribute name="NAME" x="56.642" y="74.168" size="1.27" layer="95" ratio="15" rot="MR0"/>
</instance>
<instance part="J2" gate="-6" x="53.34" y="68.58" smashed="yes" rot="MR0">
<attribute name="NAME" x="56.642" y="69.088" size="1.27" layer="95" ratio="15" rot="MR0"/>
</instance>
<instance part="J2" gate="-7" x="53.34" y="63.5" smashed="yes" rot="MR0">
<attribute name="NAME" x="56.642" y="64.008" size="1.27" layer="95" ratio="15" rot="MR0"/>
</instance>
<instance part="J1" gate="-1" x="53.34" y="144.78" smashed="yes" rot="R180">
<attribute name="NAME" x="56.642" y="144.272" size="1.27" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="J1" gate="-PAD" x="53.34" y="104.14" smashed="yes" rot="R180">
<attribute name="NAME" x="56.642" y="103.632" size="1.27" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="J1" gate="-3" x="53.34" y="134.62" smashed="yes" rot="R180">
<attribute name="NAME" x="56.642" y="134.112" size="1.27" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="J1" gate="-4" x="53.34" y="129.54" smashed="yes" rot="R180">
<attribute name="NAME" x="56.642" y="129.032" size="1.27" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="J1" gate="-5" x="53.34" y="124.46" smashed="yes" rot="R180">
<attribute name="NAME" x="56.642" y="123.952" size="1.27" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="J1" gate="-6" x="53.34" y="119.38" smashed="yes" rot="R180">
<attribute name="NAME" x="56.642" y="118.872" size="1.27" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="J1" gate="-7" x="53.34" y="114.3" smashed="yes" rot="R180">
<attribute name="NAME" x="56.642" y="113.792" size="1.27" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="D1" gate="G$1" x="127" y="83.82" smashed="yes" rot="R90">
<attribute name="NAME" x="124.46" y="78.74" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="124.46" y="86.36" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="J1" gate="G$1" x="38.1" y="114.3" smashed="yes" rot="R90">
<attribute name="VALUE" x="38.1" y="119.38" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="J2" gate="G$1" x="38.1" y="68.58" smashed="yes" rot="R90">
<attribute name="VALUE" x="38.1" y="76.2" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="AGND6" gate="VR1" x="127" y="71.12" smashed="yes">
<attribute name="VALUE" x="124.46" y="66.04" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="AGND2" gate="VR1" x="96.52" y="71.12" smashed="yes">
<attribute name="VALUE" x="93.98" y="66.04" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="AGND3" gate="VR1" x="104.14" y="71.12" smashed="yes">
<attribute name="VALUE" x="101.6" y="66.04" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="AGND4" gate="VR1" x="111.76" y="71.12" smashed="yes">
<attribute name="VALUE" x="109.22" y="66.04" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="AGND5" gate="VR1" x="119.38" y="71.12" smashed="yes">
<attribute name="VALUE" x="116.84" y="66.04" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="SUPPLY5" gate="GND" x="177.8" y="93.98" smashed="yes">
<attribute name="VALUE" x="175.895" y="90.805" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY6" gate="GND" x="185.42" y="93.98" smashed="yes">
<attribute name="VALUE" x="183.515" y="90.805" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY1" gate="GND" x="83.82" y="45.72" smashed="yes">
<attribute name="VALUE" x="81.915" y="42.545" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY2" gate="GND" x="134.62" y="71.12" smashed="yes">
<attribute name="VALUE" x="132.715" y="67.945" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY3" gate="GND" x="142.24" y="71.12" smashed="yes">
<attribute name="VALUE" x="140.335" y="67.945" size="1.27" layer="96"/>
</instance>
<instance part="D4" gate="G$1" x="149.86" y="83.82" smashed="yes" rot="R90">
<attribute name="NAME" x="147.32" y="80.518" size="1.27" layer="95" ratio="10" rot="R270"/>
<attribute name="VALUE" x="147.32" y="86.36" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="SUPPLY4" gate="GND" x="149.86" y="71.12" smashed="yes">
<attribute name="VALUE" x="147.955" y="67.945" size="1.27" layer="96"/>
</instance>
<instance part="AGND1" gate="VR1" x="73.66" y="45.72" smashed="yes">
<attribute name="VALUE" x="71.12" y="43.18" size="1.27" layer="96"/>
</instance>
<instance part="J1" gate="-2" x="53.34" y="139.7" smashed="yes" rot="R180">
<attribute name="NAME" x="56.642" y="139.192" size="1.27" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="J1" gate="-8" x="53.34" y="109.22" smashed="yes" rot="R180">
<attribute name="NAME" x="56.642" y="108.712" size="1.27" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="J2" gate="-2" x="53.34" y="88.9" smashed="yes" rot="R180">
<attribute name="NAME" x="56.642" y="88.392" size="1.27" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="J2" gate="-8" x="53.34" y="58.42" smashed="yes" rot="R180">
<attribute name="NAME" x="56.642" y="57.912" size="1.27" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="NC1" gate="G$1" x="60.96" y="109.22" smashed="yes"/>
<instance part="NC2" gate="G$1" x="60.96" y="58.42" smashed="yes"/>
<instance part="C1" gate="G$1" x="177.8" y="106.68" smashed="yes" rot="R270">
<attribute name="NAME" x="175.26" y="101.6" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="175.26" y="109.22" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C2" gate="G$1" x="185.42" y="106.68" smashed="yes" rot="R270">
<attribute name="NAME" x="182.88" y="101.6" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="182.88" y="109.22" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="F1" gate="G$1" x="96.52" y="83.82" smashed="yes">
<attribute name="NAME" x="93.98" y="78.74" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="93.98" y="86.36" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="F2" gate="G$1" x="104.14" y="83.82" smashed="yes">
<attribute name="NAME" x="101.6" y="78.74" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="101.6" y="86.36" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="F3" gate="G$1" x="111.76" y="83.82" smashed="yes">
<attribute name="NAME" x="109.22" y="78.74" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="109.22" y="86.36" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="F4" gate="G$1" x="119.38" y="83.82" smashed="yes">
<attribute name="NAME" x="116.84" y="78.74" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="116.84" y="86.36" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="J3" gate="-1" x="170.18" y="60.96"/>
<instance part="J3" gate="-2" x="170.18" y="55.88"/>
<instance part="J3" gate="-3" x="170.18" y="50.8"/>
<instance part="J3" gate="-4" x="170.18" y="45.72"/>
<instance part="J4" gate="-1" x="170.18" y="58.42"/>
<instance part="J4" gate="-2" x="170.18" y="53.34"/>
<instance part="J4" gate="-3" x="170.18" y="48.26"/>
<instance part="J4" gate="-4" x="170.18" y="43.18"/>
<instance part="D2" gate="G$1" x="134.62" y="83.82" smashed="yes" rot="R270">
<attribute name="NAME" x="132.08" y="78.74" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="132.08" y="86.36" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="D3" gate="G$1" x="142.24" y="83.82" smashed="yes" rot="R270">
<attribute name="NAME" x="139.7" y="78.74" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="139.7" y="86.36" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="SUPPLY5" gate="GND" pin="GND"/>
<wire x1="177.8" y1="101.6" x2="177.8" y2="96.52" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="SUPPLY6" gate="GND" pin="GND"/>
<wire x1="185.42" y1="101.6" x2="185.42" y2="96.52" width="0.1524" layer="91"/>
<pinref part="C2" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="J1" gate="-7" pin="1"/>
<wire x1="58.42" y1="114.3" x2="83.82" y2="114.3" width="0.1524" layer="91"/>
<wire x1="83.82" y1="114.3" x2="83.82" y2="63.5" width="0.1524" layer="91"/>
<pinref part="J2" gate="-7" pin="1"/>
<wire x1="83.82" y1="63.5" x2="58.42" y2="63.5" width="0.1524" layer="91"/>
<wire x1="83.82" y1="63.5" x2="83.82" y2="48.26" width="0.1524" layer="91"/>
<pinref part="SUPPLY1" gate="GND" pin="GND"/>
<junction x="83.82" y="63.5"/>
<pinref part="F4" gate="G$1" pin="1"/>
<wire x1="83.82" y1="114.3" x2="119.38" y2="114.3" width="0.1524" layer="91"/>
<wire x1="119.38" y1="114.3" x2="119.38" y2="88.9" width="0.1524" layer="91"/>
<junction x="83.82" y="114.3"/>
</segment>
<segment>
<pinref part="SUPPLY2" gate="GND" pin="GND"/>
<pinref part="D2" gate="G$1" pin="-"/>
<wire x1="134.62" y1="78.74" x2="134.62" y2="73.66" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUPPLY3" gate="GND" pin="GND"/>
<pinref part="D3" gate="G$1" pin="-"/>
<wire x1="142.24" y1="78.74" x2="142.24" y2="73.66" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="D4" gate="G$1" pin="1"/>
<pinref part="SUPPLY4" gate="GND" pin="GND"/>
<wire x1="149.86" y1="76.2" x2="149.86" y2="73.66" width="0.1524" layer="91"/>
</segment>
<segment>
<label x="154.94" y="55.88" size="1.27" layer="95" rot="R180" xref="yes"/>
<wire x1="165.1" y1="55.88" x2="162.56" y2="55.88" width="0.1524" layer="91"/>
<wire x1="162.56" y1="55.88" x2="154.94" y2="55.88" width="0.1524" layer="91"/>
<wire x1="165.1" y1="53.34" x2="162.56" y2="53.34" width="0.1524" layer="91"/>
<wire x1="162.56" y1="53.34" x2="162.56" y2="55.88" width="0.1524" layer="91"/>
<junction x="162.56" y="55.88"/>
<pinref part="J3" gate="-2" pin="1"/>
<pinref part="J4" gate="-2" pin="1"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="J1" gate="-1" pin="1"/>
<wire x1="58.42" y1="144.78" x2="66.04" y2="144.78" width="0.1524" layer="91"/>
<pinref part="J2" gate="-1" pin="1"/>
<wire x1="58.42" y1="93.98" x2="66.04" y2="93.98" width="0.1524" layer="91"/>
<wire x1="66.04" y1="93.98" x2="66.04" y2="144.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="AGND" class="0">
<segment>
<pinref part="D1" gate="G$1" pin="+"/>
<pinref part="AGND6" gate="VR1" pin="AGND"/>
<wire x1="127" y1="78.74" x2="127" y2="73.66" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="AGND2" gate="VR1" pin="AGND"/>
<wire x1="96.52" y1="78.74" x2="96.52" y2="73.66" width="0.1524" layer="91"/>
<pinref part="F1" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="AGND3" gate="VR1" pin="AGND"/>
<wire x1="104.14" y1="78.74" x2="104.14" y2="73.66" width="0.1524" layer="91"/>
<pinref part="F2" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="AGND4" gate="VR1" pin="AGND"/>
<wire x1="111.76" y1="78.74" x2="111.76" y2="73.66" width="0.1524" layer="91"/>
<pinref part="F3" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="AGND5" gate="VR1" pin="AGND"/>
<wire x1="119.38" y1="78.74" x2="119.38" y2="73.66" width="0.1524" layer="91"/>
<pinref part="F4" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="J1" gate="-PAD" pin="1"/>
<wire x1="58.42" y1="104.14" x2="73.66" y2="104.14" width="0.1524" layer="91"/>
<pinref part="J2" gate="-PAD" pin="1"/>
<wire x1="58.42" y1="53.34" x2="73.66" y2="53.34" width="0.1524" layer="91"/>
<wire x1="73.66" y1="104.14" x2="73.66" y2="53.34" width="0.1524" layer="91"/>
<wire x1="73.66" y1="53.34" x2="73.66" y2="48.26" width="0.1524" layer="91"/>
<pinref part="AGND1" gate="VR1" pin="AGND"/>
<junction x="73.66" y="53.34"/>
</segment>
</net>
<net name="AISG_P" class="0">
<segment>
<pinref part="J1" gate="-3" pin="1"/>
<wire x1="58.42" y1="134.62" x2="76.2" y2="134.62" width="0.1524" layer="91"/>
<pinref part="J2" gate="-3" pin="1"/>
<wire x1="58.42" y1="83.82" x2="76.2" y2="83.82" width="0.1524" layer="91"/>
<wire x1="76.2" y1="83.82" x2="76.2" y2="134.62" width="0.1524" layer="91"/>
<junction x="76.2" y="134.62"/>
<wire x1="76.2" y1="134.62" x2="111.76" y2="134.62" width="0.1524" layer="91"/>
<wire x1="111.76" y1="134.62" x2="111.76" y2="109.22" width="0.1524" layer="91"/>
<wire x1="111.76" y1="109.22" x2="111.76" y2="88.9" width="0.1524" layer="91"/>
<wire x1="111.76" y1="134.62" x2="185.42" y2="134.62" width="0.1524" layer="91"/>
<junction x="111.76" y="134.62"/>
<wire x1="142.24" y1="88.9" x2="142.24" y2="109.22" width="0.1524" layer="91"/>
<wire x1="142.24" y1="109.22" x2="111.76" y2="109.22" width="0.1524" layer="91"/>
<junction x="111.76" y="109.22"/>
<pinref part="F3" gate="G$1" pin="1"/>
<wire x1="185.42" y1="134.62" x2="195.58" y2="134.62" width="0.1524" layer="91"/>
<wire x1="185.42" y1="111.76" x2="185.42" y2="134.62" width="0.1524" layer="91"/>
<label x="195.58" y="134.62" size="1.27" layer="95" xref="yes"/>
<junction x="185.42" y="134.62"/>
<pinref part="C2" gate="G$1" pin="1"/>
<pinref part="D3" gate="G$1" pin="+"/>
</segment>
<segment>
<label x="154.94" y="45.72" size="1.27" layer="95" rot="R180" xref="yes"/>
<wire x1="165.1" y1="45.72" x2="162.56" y2="45.72" width="0.1524" layer="91"/>
<wire x1="162.56" y1="45.72" x2="154.94" y2="45.72" width="0.1524" layer="91"/>
<wire x1="165.1" y1="43.18" x2="162.56" y2="43.18" width="0.1524" layer="91"/>
<wire x1="162.56" y1="43.18" x2="162.56" y2="45.72" width="0.1524" layer="91"/>
<junction x="162.56" y="45.72"/>
<pinref part="J3" gate="-4" pin="1"/>
<pinref part="J4" gate="-4" pin="1"/>
</segment>
</net>
<net name="AISG_N" class="0">
<segment>
<pinref part="J1" gate="-5" pin="1"/>
<wire x1="58.42" y1="124.46" x2="78.74" y2="124.46" width="0.1524" layer="91"/>
<pinref part="J2" gate="-5" pin="1"/>
<wire x1="58.42" y1="73.66" x2="78.74" y2="73.66" width="0.1524" layer="91"/>
<wire x1="78.74" y1="73.66" x2="78.74" y2="124.46" width="0.1524" layer="91"/>
<junction x="78.74" y="124.46"/>
<wire x1="78.74" y1="124.46" x2="104.14" y2="124.46" width="0.1524" layer="91"/>
<wire x1="104.14" y1="124.46" x2="104.14" y2="106.68" width="0.1524" layer="91"/>
<wire x1="104.14" y1="106.68" x2="104.14" y2="88.9" width="0.1524" layer="91"/>
<wire x1="104.14" y1="124.46" x2="177.8" y2="124.46" width="0.1524" layer="91"/>
<junction x="104.14" y="124.46"/>
<wire x1="134.62" y1="88.9" x2="134.62" y2="106.68" width="0.1524" layer="91"/>
<wire x1="134.62" y1="106.68" x2="104.14" y2="106.68" width="0.1524" layer="91"/>
<junction x="104.14" y="106.68"/>
<pinref part="F2" gate="G$1" pin="1"/>
<wire x1="177.8" y1="124.46" x2="195.58" y2="124.46" width="0.1524" layer="91"/>
<wire x1="177.8" y1="111.76" x2="177.8" y2="124.46" width="0.1524" layer="91"/>
<label x="195.58" y="124.46" size="1.27" layer="95" xref="yes"/>
<junction x="177.8" y="124.46"/>
<pinref part="C1" gate="G$1" pin="1"/>
<pinref part="D2" gate="G$1" pin="+"/>
</segment>
<segment>
<label x="154.94" y="50.8" size="1.27" layer="95" rot="R180" xref="yes"/>
<wire x1="165.1" y1="50.8" x2="162.56" y2="50.8" width="0.1524" layer="91"/>
<wire x1="162.56" y1="50.8" x2="154.94" y2="50.8" width="0.1524" layer="91"/>
<wire x1="165.1" y1="48.26" x2="162.56" y2="48.26" width="0.1524" layer="91"/>
<wire x1="162.56" y1="48.26" x2="162.56" y2="50.8" width="0.1524" layer="91"/>
<junction x="162.56" y="50.8"/>
<pinref part="J3" gate="-3" pin="1"/>
<pinref part="J4" gate="-3" pin="1"/>
</segment>
</net>
<net name="10-30VIN" class="0">
<segment>
<wire x1="165.1" y1="60.96" x2="162.56" y2="60.96" width="0.1524" layer="91"/>
<label x="154.94" y="60.96" size="1.27" layer="95" rot="R180" xref="yes"/>
<wire x1="162.56" y1="60.96" x2="154.94" y2="60.96" width="0.1524" layer="91"/>
<wire x1="165.1" y1="58.42" x2="162.56" y2="58.42" width="0.1524" layer="91"/>
<wire x1="162.56" y1="58.42" x2="162.56" y2="60.96" width="0.1524" layer="91"/>
<junction x="162.56" y="60.96"/>
<pinref part="J3" gate="-1" pin="1"/>
<pinref part="J4" gate="-1" pin="1"/>
</segment>
<segment>
<pinref part="J2" gate="-6" pin="1"/>
<wire x1="58.42" y1="68.58" x2="81.28" y2="68.58" width="0.1524" layer="91"/>
<wire x1="81.28" y1="68.58" x2="81.28" y2="119.38" width="0.1524" layer="91"/>
<pinref part="J1" gate="-6" pin="1"/>
<wire x1="58.42" y1="119.38" x2="81.28" y2="119.38" width="0.1524" layer="91"/>
<junction x="81.28" y="119.38"/>
<wire x1="81.28" y1="119.38" x2="96.52" y2="119.38" width="0.1524" layer="91"/>
<wire x1="96.52" y1="119.38" x2="96.52" y2="88.9" width="0.1524" layer="91"/>
<wire x1="96.52" y1="119.38" x2="127" y2="119.38" width="0.1524" layer="91"/>
<junction x="96.52" y="119.38"/>
<wire x1="127" y1="119.38" x2="149.86" y2="119.38" width="0.1524" layer="91"/>
<pinref part="D1" gate="G$1" pin="-"/>
<wire x1="127" y1="88.9" x2="127" y2="119.38" width="0.1524" layer="91"/>
<junction x="127" y="119.38"/>
<pinref part="F1" gate="G$1" pin="1"/>
<pinref part="D4" gate="G$1" pin="2"/>
<wire x1="149.86" y1="91.44" x2="149.86" y2="119.38" width="0.1524" layer="91"/>
<junction x="149.86" y="119.38"/>
<label x="195.58" y="119.38" size="1.27" layer="95" xref="yes"/>
<wire x1="149.86" y1="119.38" x2="195.58" y2="119.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$44" class="0">
<segment>
<pinref part="J1" gate="-4" pin="1"/>
<wire x1="58.42" y1="129.54" x2="71.12" y2="129.54" width="0.1524" layer="91"/>
<pinref part="J2" gate="-4" pin="1"/>
<wire x1="58.42" y1="78.74" x2="71.12" y2="78.74" width="0.1524" layer="91"/>
<wire x1="71.12" y1="78.74" x2="71.12" y2="129.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$45" class="0">
<segment>
<pinref part="J1" gate="-2" pin="1"/>
<wire x1="58.42" y1="139.7" x2="68.58" y2="139.7" width="0.1524" layer="91"/>
<wire x1="68.58" y1="139.7" x2="68.58" y2="88.9" width="0.1524" layer="91"/>
<pinref part="J2" gate="-2" pin="1"/>
<wire x1="68.58" y1="88.9" x2="58.42" y2="88.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="J2" gate="-8" pin="1"/>
<wire x1="58.42" y1="58.42" x2="60.96" y2="58.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="J1" gate="-8" pin="1"/>
<wire x1="58.42" y1="109.22" x2="60.96" y2="109.22" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
<errors>
<approved hash="106,1,58.42,58.42,N$3,,,,,"/>
<approved hash="106,1,58.42,109.22,N$4,,,,,"/>
</errors>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
</compatibility>
</eagle>
