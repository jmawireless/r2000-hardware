<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.5.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="yes" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="5" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="no" active="no"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="no" active="no"/>
<layer number="103" name="tMap" color="7" fill="1" visible="no" active="no"/>
<layer number="104" name="Name" color="7" fill="1" visible="no" active="no"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="no" active="no"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="no" active="no"/>
<layer number="107" name="Crop" color="7" fill="1" visible="no" active="no"/>
<layer number="108" name="fp8" color="7" fill="1" visible="yes" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="yes" active="yes"/>
<layer number="110" name="110" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="111" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="7" fill="1" visible="yes" active="yes"/>
<layer number="114" name="FRNTMAAT1" color="7" fill="1" visible="no" active="no"/>
<layer number="115" name="FRNTMAAT2" color="7" fill="1" visible="no" active="no"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="no" active="no"/>
<layer number="117" name="BACKMAAT1" color="7" fill="1" visible="no" active="no"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="yes" active="yes"/>
<layer number="119" name="KAP_TEKEN" color="7" fill="1" visible="no" active="no"/>
<layer number="120" name="KAP_MAAT1" color="7" fill="1" visible="no" active="no"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="no" active="no"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="no" active="no"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="top_silk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="130" name="SMDSTROOK" color="7" fill="1" visible="no" active="no"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="133" name="bottom_silk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="DrillLegend" color="7" fill="1" visible="no" active="no"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="14" fill="1" visible="no" active="no"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="no"/>
<layer number="201" name="201bmp" color="2" fill="1" visible="no" active="no"/>
<layer number="202" name="202bmp" color="3" fill="1" visible="no" active="no"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="no" active="no"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="no" active="no"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="no" active="no"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="no" active="no"/>
<layer number="207" name="207bmp" color="15" fill="10" visible="no" active="no"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="no" active="no"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="Eagle3D_PG1" color="7" fill="1" visible="no" active="no"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="no"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="OrgLBR" color="13" fill="1" visible="no" active="no"/>
<layer number="255" name="Accent" color="7" fill="1" visible="no" active="no"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="supply2" urn="urn:adsk.eagle:library:372">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
Please keep in mind, that these devices are necessary for the
automatic wiring of the supply signals.&lt;p&gt;
The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND" urn="urn:adsk.eagle:symbol:26990/1" library_version="2">
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="-1.27" y2="0" width="0.254" layer="94"/>
<text x="-1.905" y="-3.175" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" urn="urn:adsk.eagle:component:27037/1" prefix="SUPPLY" library_version="2">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="GND" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1" urn="urn:adsk.eagle:library:371">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="+3V3" urn="urn:adsk.eagle:symbol:26950/1" library_version="1">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+3V3" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="+3V3" urn="urn:adsk.eagle:component:26981/1" prefix="+3V3" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="+3V3" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="frames" urn="urn:adsk.eagle:library:229">
<description>&lt;b&gt;Frames for Sheet and Layout&lt;/b&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="A3L-LOC" urn="urn:adsk.eagle:symbol:13881/1" library_version="1">
<wire x1="288.29" y1="3.81" x2="342.265" y2="3.81" width="0.1016" layer="94"/>
<wire x1="342.265" y1="3.81" x2="373.38" y2="3.81" width="0.1016" layer="94"/>
<wire x1="373.38" y1="3.81" x2="383.54" y2="3.81" width="0.1016" layer="94"/>
<wire x1="383.54" y1="3.81" x2="383.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="383.54" y1="8.89" x2="383.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="383.54" y1="13.97" x2="383.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="383.54" y1="19.05" x2="383.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="288.29" y1="3.81" x2="288.29" y2="24.13" width="0.1016" layer="94"/>
<wire x1="288.29" y1="24.13" x2="342.265" y2="24.13" width="0.1016" layer="94"/>
<wire x1="342.265" y1="24.13" x2="383.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="373.38" y1="3.81" x2="373.38" y2="8.89" width="0.1016" layer="94"/>
<wire x1="373.38" y1="8.89" x2="383.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="373.38" y1="8.89" x2="342.265" y2="8.89" width="0.1016" layer="94"/>
<wire x1="342.265" y1="8.89" x2="342.265" y2="3.81" width="0.1016" layer="94"/>
<wire x1="342.265" y1="8.89" x2="342.265" y2="13.97" width="0.1016" layer="94"/>
<wire x1="342.265" y1="13.97" x2="383.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="342.265" y1="13.97" x2="342.265" y2="19.05" width="0.1016" layer="94"/>
<wire x1="342.265" y1="19.05" x2="383.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="342.265" y1="19.05" x2="342.265" y2="24.13" width="0.1016" layer="94"/>
<text x="344.17" y="15.24" size="2.54" layer="94">&gt;DRAWING_NAME</text>
<text x="344.17" y="10.16" size="2.286" layer="94">&gt;LAST_DATE_TIME</text>
<text x="357.505" y="5.08" size="2.54" layer="94">&gt;SHEET</text>
<text x="343.916" y="4.953" size="2.54" layer="94">Sheet:</text>
<frame x1="0" y1="0" x2="387.35" y2="260.35" columns="8" rows="5" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="A3L-LOC" urn="urn:adsk.eagle:component:13942/1" prefix="FRAME" uservalue="yes" library_version="1">
<description>&lt;b&gt;FRAME&lt;/b&gt;&lt;p&gt;
DIN A3, landscape with location and doc. field</description>
<gates>
<gate name="G$1" symbol="A3L-LOC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="JMA_LBR_v2" urn="urn:adsk.eagle:library:8413580">
<packages>
<package name="TAG-CONNECT_TC2050-IDC-NL" urn="urn:adsk.eagle:footprint:16170704/4" library_version="75">
<wire x1="-2.7305" y1="3.683" x2="4.0005" y2="3.683" width="0.127" layer="21"/>
<wire x1="4.0005" y1="3.683" x2="4.0005" y2="-8.763" width="0.127" layer="21"/>
<wire x1="4.0005" y1="-8.763" x2="-2.7305" y2="-8.763" width="0.127" layer="21"/>
<wire x1="-2.7305" y1="-8.763" x2="-2.7305" y2="3.683" width="0.127" layer="21"/>
<wire x1="-2.7305" y1="3.683" x2="4.0005" y2="3.683" width="0.127" layer="51"/>
<wire x1="4.0005" y1="3.683" x2="4.0005" y2="-8.763" width="0.127" layer="51"/>
<wire x1="4.0005" y1="-8.763" x2="-2.7305" y2="-8.763" width="0.127" layer="51"/>
<wire x1="-2.7305" y1="-8.763" x2="-2.7305" y2="3.683" width="0.127" layer="51"/>
<wire x1="-2.9805" y1="3.933" x2="4.2505" y2="3.933" width="0.05" layer="39"/>
<wire x1="4.2505" y1="3.933" x2="4.2505" y2="-9.013" width="0.05" layer="39"/>
<wire x1="4.2505" y1="-9.013" x2="-2.9805" y2="-9.013" width="0.05" layer="39"/>
<wire x1="-2.9805" y1="-9.013" x2="-2.9805" y2="3.933" width="0.05" layer="39"/>
<circle x="-3.365" y="0" radius="0.1" width="0.2" layer="21"/>
<circle x="-3.365" y="0" radius="0.1" width="0.2" layer="51"/>
<text x="-3.365" y="4.46" size="1.778" layer="25">&gt;NAME</text>
<text x="-3.365" y="-11.54" size="1.778" layer="27">&gt;VALUE</text>
<rectangle x1="0" y1="-5.08" x2="1.27" y2="0" layer="42"/>
<rectangle x1="0" y1="-5.08" x2="1.27" y2="0" layer="43"/>
<rectangle x1="0.508" y1="-5.08" x2="0.762" y2="0" layer="41"/>
<rectangle x1="0" y1="-0.762" x2="1.27" y2="-0.508" layer="41"/>
<rectangle x1="0" y1="-2.032" x2="1.27" y2="-1.778" layer="41"/>
<rectangle x1="0" y1="-3.302" x2="1.27" y2="-3.048" layer="41"/>
<rectangle x1="0" y1="-4.572" x2="1.27" y2="-4.318" layer="41"/>
<smd name="1" x="0" y="0" dx="0.787" dy="0.787" layer="1" roundness="100" rot="R270" cream="no"/>
<smd name="2" x="0" y="-1.27" dx="0.787" dy="0.787" layer="1" roundness="100" rot="R270" cream="no"/>
<smd name="3" x="0" y="-2.54" dx="0.787" dy="0.787" layer="1" roundness="100" rot="R270" cream="no"/>
<smd name="4" x="0" y="-3.81" dx="0.787" dy="0.787" layer="1" roundness="100" rot="R270" cream="no"/>
<smd name="5" x="0" y="-5.08" dx="0.787" dy="0.787" layer="1" roundness="100" rot="R270" cream="no"/>
<smd name="6" x="1.27" y="-5.08" dx="0.787" dy="0.787" layer="1" roundness="100" rot="R90" cream="no"/>
<smd name="7" x="1.27" y="-3.81" dx="0.787" dy="0.787" layer="1" roundness="100" rot="R90" cream="no"/>
<smd name="8" x="1.27" y="-2.54" dx="0.787" dy="0.787" layer="1" roundness="100" rot="R90" cream="no"/>
<smd name="9" x="1.27" y="-1.27" dx="0.787" dy="0.787" layer="1" roundness="100" rot="R90" cream="no"/>
<smd name="10" x="1.27" y="0" dx="0.787" dy="0.787" layer="1" roundness="100" rot="R90" cream="no"/>
<hole x="1.65" y="-6.35" drill="0.991"/>
<hole x="-0.38" y="-6.35" drill="0.991"/>
<hole x="0.635" y="1.27" drill="0.991"/>
</package>
<package name="TP_1.5MM" urn="urn:adsk.eagle:footprint:8413705/1" library_version="40" library_locally_modified="yes">
<pad name="1" x="0" y="0" drill="1" diameter="1.5" rot="R180"/>
<text x="-1.905" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<rectangle x1="-0.35" y1="-0.35" x2="0.35" y2="0.35" layer="51"/>
</package>
<package name="TP_1.5MM_PAD" urn="urn:adsk.eagle:footprint:8413706/1" library_version="40" library_locally_modified="yes">
<pad name="1" x="0" y="0" drill="1" diameter="1.5" rot="R180"/>
<text x="-1.27" y="4.445" size="1.27" layer="25">&gt;NAME</text>
<rectangle x1="-0.35" y1="-0.35" x2="0.35" y2="0.35" layer="51"/>
<rectangle x1="-2" y1="-2.5" x2="8" y2="2.5" layer="1"/>
</package>
<package name="TP_1.7MM" urn="urn:adsk.eagle:footprint:8413707/1" library_version="40" library_locally_modified="yes">
<pad name="1" x="0" y="0" drill="1" diameter="1.6764" rot="R180"/>
<text x="-1.905" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<rectangle x1="-0.35" y1="-0.35" x2="0.35" y2="0.35" layer="51"/>
</package>
<package name="C120H40" urn="urn:adsk.eagle:footprint:8413699/1" library_version="40" library_locally_modified="yes">
<pad name="1" x="0" y="0" drill="0.6" diameter="1.2" thermals="no"/>
<text x="0.9" y="-0.1" size="0.254" layer="25">&gt;NAME</text>
</package>
<package name="C131H51" urn="urn:adsk.eagle:footprint:8413700/1" library_version="40" library_locally_modified="yes">
<pad name="1" x="0" y="0" drill="0.71" diameter="1.31" thermals="no"/>
<text x="1.2" y="0.1" size="0.254" layer="25">&gt;NAME</text>
<text x="1.2" y="-0.2" size="0.254" layer="21">&gt;LABEL</text>
</package>
<package name="C406H326" urn="urn:adsk.eagle:footprint:8413701/1" library_version="40" library_locally_modified="yes">
<pad name="1" x="0" y="0" drill="3.26" diameter="4.06" thermals="no"/>
<text x="2.2" y="1.1" size="0.254" layer="25">&gt;NAME</text>
<text x="2.2" y="0.8" size="0.254" layer="21">&gt;LABEL</text>
</package>
<package name="C491H411" urn="urn:adsk.eagle:footprint:8413702/1" library_version="40" library_locally_modified="yes">
<pad name="1" x="0" y="0" drill="4.31" diameter="4.91" thermals="no"/>
<text x="3.2" y="1.1" size="0.254" layer="25">&gt;NAME</text>
<text x="3.2" y="0.8" size="0.254" layer="21">&gt;LABEL</text>
</package>
<package name="CIR_PAD_1MM" urn="urn:adsk.eagle:footprint:8413703/1" library_version="40" library_locally_modified="yes">
<text x="-1.905" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<smd name="P$1" x="0" y="0" dx="1" dy="1" layer="1" roundness="100"/>
</package>
<package name="CIR_PAD_2MM" urn="urn:adsk.eagle:footprint:8413704/1" library_version="40" library_locally_modified="yes">
<text x="-1.905" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<smd name="P$1" x="0" y="0" dx="2" dy="2" layer="1" roundness="100"/>
</package>
<package name="RESC1005X40" urn="urn:adsk.eagle:footprint:3811164/1" library_version="58">
<description>CHIP, 1 X 0.5 X 0.4 mm body
&lt;p&gt;CHIP package with body size 1 X 0.5 X 0.4 mm&lt;/p&gt;</description>
<wire x1="0.525" y1="0.614" x2="-0.525" y2="0.614" width="0.12" layer="21"/>
<wire x1="0.525" y1="-0.614" x2="-0.525" y2="-0.614" width="0.12" layer="21"/>
<wire x1="0.525" y1="-0.275" x2="-0.525" y2="-0.275" width="0.12" layer="51"/>
<wire x1="-0.525" y1="-0.275" x2="-0.525" y2="0.275" width="0.12" layer="51"/>
<wire x1="-0.525" y1="0.275" x2="0.525" y2="0.275" width="0.12" layer="51"/>
<wire x1="0.525" y1="0.275" x2="0.525" y2="-0.275" width="0.12" layer="51"/>
<smd name="1" x="-0.475" y="0" dx="0.55" dy="0.6" layer="1"/>
<smd name="2" x="0.475" y="0" dx="0.55" dy="0.6" layer="1"/>
<text x="0" y="1.249" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.249" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="RESC1608X55" urn="urn:adsk.eagle:footprint:3811167/1" library_version="58">
<description>CHIP, 1.6 X 0.85 X 0.55 mm body
&lt;p&gt;CHIP package with body size 1.6 X 0.85 X 0.55 mm&lt;/p&gt;</description>
<wire x1="0.875" y1="0.8036" x2="-0.875" y2="0.8036" width="0.12" layer="21"/>
<wire x1="0.875" y1="-0.8036" x2="-0.875" y2="-0.8036" width="0.12" layer="21"/>
<wire x1="0.875" y1="-0.475" x2="-0.875" y2="-0.475" width="0.12" layer="51"/>
<wire x1="-0.875" y1="-0.475" x2="-0.875" y2="0.475" width="0.12" layer="51"/>
<wire x1="-0.875" y1="0.475" x2="0.875" y2="0.475" width="0.12" layer="51"/>
<wire x1="0.875" y1="0.475" x2="0.875" y2="-0.475" width="0.12" layer="51"/>
<smd name="1" x="-0.7996" y="0" dx="0.8709" dy="0.9791" layer="1"/>
<smd name="2" x="0.7996" y="0" dx="0.8709" dy="0.9791" layer="1"/>
<text x="0" y="1.4386" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.4386" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="INDM5050X430" urn="urn:adsk.eagle:footprint:3890170/1" library_version="58">
<description>MOLDED BODY, 5 X 5 X 4.3 mm body
&lt;p&gt;MOLDED BODY package with body size 5 X 5 X 4.3 mm&lt;/p&gt;</description>
<wire x1="-2.51" y1="2.5202" x2="2.51" y2="2.5202" width="0.12" layer="21"/>
<wire x1="-2.51" y1="-2.5202" x2="2.51" y2="-2.5202" width="0.12" layer="21"/>
<wire x1="2.51" y1="-2.51" x2="-2.51" y2="-2.51" width="0.12" layer="51"/>
<wire x1="-2.51" y1="-2.51" x2="-2.51" y2="2.51" width="0.12" layer="51"/>
<wire x1="-2.51" y1="2.51" x2="2.51" y2="2.51" width="0.12" layer="51"/>
<wire x1="2.51" y1="2.51" x2="2.51" y2="-2.51" width="0.12" layer="51"/>
<smd name="1" x="-1.8761" y="0" dx="2.3466" dy="4.4123" layer="1"/>
<smd name="2" x="1.8761" y="0" dx="2.3466" dy="4.4123" layer="1"/>
<text x="0" y="3.1552" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-3.1552" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="INDM4040X300" urn="urn:adsk.eagle:footprint:3890468/1" library_version="58">
<description>MOLDED BODY, 4 X 4 X 3 mm body
&lt;p&gt;MOLDED BODY package with body size 4 X 4 X 3 mm&lt;/p&gt;</description>
<wire x1="-2" y1="2.1699" x2="2" y2="2.1699" width="0.12" layer="21"/>
<wire x1="-2" y1="-2.1699" x2="2" y2="-2.1699" width="0.12" layer="21"/>
<wire x1="2" y1="-2" x2="-2" y2="-2" width="0.12" layer="51"/>
<wire x1="-2" y1="-2" x2="-2" y2="2" width="0.12" layer="51"/>
<wire x1="-2" y1="2" x2="2" y2="2" width="0.12" layer="51"/>
<wire x1="2" y1="2" x2="2" y2="-2" width="0.12" layer="51"/>
<smd name="1" x="-1.5795" y="0" dx="1.9528" dy="3.7118" layer="1"/>
<smd name="2" x="1.5795" y="0" dx="1.9528" dy="3.7118" layer="1"/>
<text x="0" y="2.8049" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.8049" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="INDM4949X400" urn="urn:adsk.eagle:footprint:3904917/1" library_version="58">
<description>MOLDED BODY, 4.9 X 4.9 X 4 mm body
&lt;p&gt;MOLDED BODY package with body size 4.9 X 4.9 X 4 mm&lt;/p&gt;</description>
<wire x1="-2.45" y1="2.6199" x2="2.45" y2="2.6199" width="0.12" layer="21"/>
<wire x1="-2.45" y1="-2.6199" x2="2.45" y2="-2.6199" width="0.12" layer="21"/>
<wire x1="2.45" y1="-2.45" x2="-2.45" y2="-2.45" width="0.12" layer="51"/>
<wire x1="-2.45" y1="-2.45" x2="-2.45" y2="2.45" width="0.12" layer="51"/>
<wire x1="-2.45" y1="2.45" x2="2.45" y2="2.45" width="0.12" layer="51"/>
<wire x1="2.45" y1="2.45" x2="2.45" y2="-2.45" width="0.12" layer="51"/>
<smd name="1" x="-1.9795" y="0" dx="2.0528" dy="4.6118" layer="1"/>
<smd name="2" x="1.9795" y="0" dx="2.0528" dy="4.6118" layer="1"/>
<text x="0" y="3.2549" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-3.2549" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="RADIAL_THT" urn="urn:adsk.eagle:footprint:8413685/1" library_version="40" library_locally_modified="yes">
<pad name="1" x="1.27" y="0" drill="0.6" diameter="1" rot="R180"/>
<pad name="2" x="-1.27" y="0" drill="0.6" diameter="1" rot="R180"/>
<text x="-3.175" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="4.445" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<circle x="0" y="0" radius="2.60108125" width="0.127" layer="21"/>
</package>
<package name="QFP50P1600X1600X120-100" urn="urn:adsk.eagle:footprint:3791946/2" library_version="63">
<description>100-QFP, 0.5 mm pitch, 16 mm span, 14 X 14 X 1.2 mm body
&lt;p&gt;100-pin QFP package with 0.5 mm pitch, 16 mm lead span1 X 16 mm lead span2 with body size 14 X 14 X 1.2 mm&lt;/p&gt;</description>
<circle x="-7.7788" y="6.644" radius="0.25" width="0" layer="21"/>
<wire x1="-7.1" y1="6.394" x2="-7.1" y2="7.1" width="0.12" layer="21"/>
<wire x1="-7.1" y1="7.1" x2="-6.394" y2="7.1" width="0.12" layer="21"/>
<wire x1="7.1" y1="6.394" x2="7.1" y2="7.1" width="0.12" layer="21"/>
<wire x1="7.1" y1="7.1" x2="6.394" y2="7.1" width="0.12" layer="21"/>
<wire x1="7.1" y1="-6.394" x2="7.1" y2="-7.1" width="0.12" layer="21"/>
<wire x1="7.1" y1="-7.1" x2="6.394" y2="-7.1" width="0.12" layer="21"/>
<wire x1="-7.1" y1="-6.394" x2="-7.1" y2="-7.1" width="0.12" layer="21"/>
<wire x1="-7.1" y1="-7.1" x2="-6.394" y2="-7.1" width="0.12" layer="21"/>
<wire x1="7.1" y1="-7.1" x2="-7.1" y2="-7.1" width="0.12" layer="51"/>
<wire x1="-7.1" y1="-7.1" x2="-7.1" y2="7.1" width="0.12" layer="51"/>
<wire x1="-7.1" y1="7.1" x2="7.1" y2="7.1" width="0.12" layer="51"/>
<wire x1="7.1" y1="7.1" x2="7.1" y2="-7.1" width="0.12" layer="51"/>
<smd name="1" x="-7.6783" y="6" dx="1.5588" dy="0.28" layer="1"/>
<smd name="2" x="-7.6783" y="5.5" dx="1.5588" dy="0.28" layer="1"/>
<smd name="3" x="-7.6783" y="5" dx="1.5588" dy="0.28" layer="1"/>
<smd name="4" x="-7.6783" y="4.5" dx="1.5588" dy="0.28" layer="1"/>
<smd name="5" x="-7.6783" y="4" dx="1.5588" dy="0.28" layer="1"/>
<smd name="6" x="-7.6783" y="3.5" dx="1.5588" dy="0.28" layer="1"/>
<smd name="7" x="-7.6783" y="3" dx="1.5588" dy="0.28" layer="1"/>
<smd name="8" x="-7.6783" y="2.5" dx="1.5588" dy="0.28" layer="1"/>
<smd name="9" x="-7.6783" y="2" dx="1.5588" dy="0.28" layer="1"/>
<smd name="10" x="-7.6783" y="1.5" dx="1.5588" dy="0.28" layer="1"/>
<smd name="11" x="-7.6783" y="1" dx="1.5588" dy="0.28" layer="1"/>
<smd name="12" x="-7.6783" y="0.5" dx="1.5588" dy="0.28" layer="1"/>
<smd name="13" x="-7.6783" y="0" dx="1.5588" dy="0.28" layer="1"/>
<smd name="14" x="-7.6783" y="-0.5" dx="1.5588" dy="0.28" layer="1"/>
<smd name="15" x="-7.6783" y="-1" dx="1.5588" dy="0.28" layer="1"/>
<smd name="16" x="-7.6783" y="-1.5" dx="1.5588" dy="0.28" layer="1"/>
<smd name="17" x="-7.6783" y="-2" dx="1.5588" dy="0.28" layer="1"/>
<smd name="18" x="-7.6783" y="-2.5" dx="1.5588" dy="0.28" layer="1"/>
<smd name="19" x="-7.6783" y="-3" dx="1.5588" dy="0.28" layer="1"/>
<smd name="20" x="-7.6783" y="-3.5" dx="1.5588" dy="0.28" layer="1"/>
<smd name="21" x="-7.6783" y="-4" dx="1.5588" dy="0.28" layer="1"/>
<smd name="22" x="-7.6783" y="-4.5" dx="1.5588" dy="0.28" layer="1"/>
<smd name="23" x="-7.6783" y="-5" dx="1.5588" dy="0.28" layer="1"/>
<smd name="24" x="-7.6783" y="-5.5" dx="1.5588" dy="0.28" layer="1"/>
<smd name="25" x="-7.6783" y="-6" dx="1.5588" dy="0.28" layer="1"/>
<smd name="26" x="-6" y="-7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="27" x="-5.5" y="-7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="28" x="-5" y="-7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="29" x="-4.5" y="-7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="30" x="-4" y="-7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="31" x="-3.5" y="-7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="32" x="-3" y="-7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="33" x="-2.5" y="-7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="34" x="-2" y="-7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="35" x="-1.5" y="-7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="36" x="-1" y="-7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="37" x="-0.5" y="-7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="38" x="0" y="-7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="39" x="0.5" y="-7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="40" x="1" y="-7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="41" x="1.5" y="-7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="42" x="2" y="-7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="43" x="2.5" y="-7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="44" x="3" y="-7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="45" x="3.5" y="-7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="46" x="4" y="-7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="47" x="4.5" y="-7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="48" x="5" y="-7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="49" x="5.5" y="-7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="50" x="6" y="-7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="51" x="7.6783" y="-6" dx="1.5588" dy="0.28" layer="1"/>
<smd name="52" x="7.6783" y="-5.5" dx="1.5588" dy="0.28" layer="1"/>
<smd name="53" x="7.6783" y="-5" dx="1.5588" dy="0.28" layer="1"/>
<smd name="54" x="7.6783" y="-4.5" dx="1.5588" dy="0.28" layer="1"/>
<smd name="55" x="7.6783" y="-4" dx="1.5588" dy="0.28" layer="1"/>
<smd name="56" x="7.6783" y="-3.5" dx="1.5588" dy="0.28" layer="1"/>
<smd name="57" x="7.6783" y="-3" dx="1.5588" dy="0.28" layer="1"/>
<smd name="58" x="7.6783" y="-2.5" dx="1.5588" dy="0.28" layer="1"/>
<smd name="59" x="7.6783" y="-2" dx="1.5588" dy="0.28" layer="1"/>
<smd name="60" x="7.6783" y="-1.5" dx="1.5588" dy="0.28" layer="1"/>
<smd name="61" x="7.6783" y="-1" dx="1.5588" dy="0.28" layer="1"/>
<smd name="62" x="7.6783" y="-0.5" dx="1.5588" dy="0.28" layer="1"/>
<smd name="63" x="7.6783" y="0" dx="1.5588" dy="0.28" layer="1"/>
<smd name="64" x="7.6783" y="0.5" dx="1.5588" dy="0.28" layer="1"/>
<smd name="65" x="7.6783" y="1" dx="1.5588" dy="0.28" layer="1"/>
<smd name="66" x="7.6783" y="1.5" dx="1.5588" dy="0.28" layer="1"/>
<smd name="67" x="7.6783" y="2" dx="1.5588" dy="0.28" layer="1"/>
<smd name="68" x="7.6783" y="2.5" dx="1.5588" dy="0.28" layer="1"/>
<smd name="69" x="7.6783" y="3" dx="1.5588" dy="0.28" layer="1"/>
<smd name="70" x="7.6783" y="3.5" dx="1.5588" dy="0.28" layer="1"/>
<smd name="71" x="7.6783" y="4" dx="1.5588" dy="0.28" layer="1"/>
<smd name="72" x="7.6783" y="4.5" dx="1.5588" dy="0.28" layer="1"/>
<smd name="73" x="7.6783" y="5" dx="1.5588" dy="0.28" layer="1"/>
<smd name="74" x="7.6783" y="5.5" dx="1.5588" dy="0.28" layer="1"/>
<smd name="75" x="7.6783" y="6" dx="1.5588" dy="0.28" layer="1"/>
<smd name="76" x="6" y="7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="77" x="5.5" y="7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="78" x="5" y="7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="79" x="4.5" y="7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="80" x="4" y="7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="81" x="3.5" y="7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="82" x="3" y="7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="83" x="2.5" y="7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="84" x="2" y="7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="85" x="1.5" y="7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="86" x="1" y="7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="87" x="0.5" y="7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="88" x="0" y="7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="89" x="-0.5" y="7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="90" x="-1" y="7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="91" x="-1.5" y="7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="92" x="-2" y="7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="93" x="-2.5" y="7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="94" x="-3" y="7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="95" x="-3.5" y="7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="96" x="-4" y="7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="97" x="-4.5" y="7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="98" x="-5" y="7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="99" x="-5.5" y="7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="100" x="-6" y="7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<text x="0" y="9.0927" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-9.0927" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC1005X60" urn="urn:adsk.eagle:footprint:3793110/1" library_version="58">
<description>CHIP, 1.025 X 0.525 X 0.6 mm body
&lt;p&gt;CHIP package with body size 1.025 X 0.525 X 0.6 mm&lt;/p&gt;</description>
<wire x1="0.55" y1="0.6325" x2="-0.55" y2="0.6325" width="0.12" layer="21"/>
<wire x1="0.55" y1="-0.6325" x2="-0.55" y2="-0.6325" width="0.12" layer="21"/>
<wire x1="0.55" y1="-0.3" x2="-0.55" y2="-0.3" width="0.12" layer="51"/>
<wire x1="-0.55" y1="-0.3" x2="-0.55" y2="0.3" width="0.12" layer="51"/>
<wire x1="-0.55" y1="0.3" x2="0.55" y2="0.3" width="0.12" layer="51"/>
<wire x1="0.55" y1="0.3" x2="0.55" y2="-0.3" width="0.12" layer="51"/>
<smd name="1" x="-0.4875" y="0" dx="0.5621" dy="0.6371" layer="1"/>
<smd name="2" x="0.4875" y="0" dx="0.5621" dy="0.6371" layer="1"/>
<text x="0" y="1.2675" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.2675" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC1608X55" urn="urn:adsk.eagle:footprint:3793129/2" library_version="40" library_locally_modified="yes">
<description>CHIP, 1.6 X 0.8 X 0.55 mm body
&lt;p&gt;CHIP package with body size 1.6 X 0.8 X 0.55 mm&lt;/p&gt;</description>
<wire x1="0.85" y1="0.6516" x2="-0.85" y2="0.6516" width="0.12" layer="21"/>
<wire x1="0.85" y1="-0.6516" x2="-0.85" y2="-0.6516" width="0.12" layer="21"/>
<wire x1="0.85" y1="-0.45" x2="-0.85" y2="-0.45" width="0.12" layer="51"/>
<wire x1="-0.85" y1="-0.45" x2="-0.85" y2="0.45" width="0.12" layer="51"/>
<wire x1="-0.85" y1="0.45" x2="0.85" y2="0.45" width="0.12" layer="51"/>
<wire x1="0.85" y1="0.45" x2="0.85" y2="-0.45" width="0.12" layer="51"/>
<smd name="1" x="-0.7704" y="0" dx="0.8884" dy="0.9291" layer="1"/>
<smd name="2" x="0.7704" y="0" dx="0.8884" dy="0.9291" layer="1"/>
<text x="0" y="1.4136" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.4136" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC2012X127" urn="urn:adsk.eagle:footprint:3793140/1" library_version="58">
<description>CHIP, 2.01 X 1.25 X 1.27 mm body
&lt;p&gt;CHIP package with body size 2.01 X 1.25 X 1.27 mm&lt;/p&gt;</description>
<wire x1="1.105" y1="1.0467" x2="-1.105" y2="1.0467" width="0.12" layer="21"/>
<wire x1="1.105" y1="-1.0467" x2="-1.105" y2="-1.0467" width="0.12" layer="21"/>
<wire x1="1.105" y1="-0.725" x2="-1.105" y2="-0.725" width="0.12" layer="51"/>
<wire x1="-1.105" y1="-0.725" x2="-1.105" y2="0.725" width="0.12" layer="51"/>
<wire x1="-1.105" y1="0.725" x2="1.105" y2="0.725" width="0.12" layer="51"/>
<wire x1="1.105" y1="0.725" x2="1.105" y2="-0.725" width="0.12" layer="51"/>
<smd name="1" x="-0.8804" y="0" dx="1.1646" dy="1.4653" layer="1"/>
<smd name="2" x="0.8804" y="0" dx="1.1646" dy="1.4653" layer="1"/>
<text x="0" y="1.6817" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.6817" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CKSWITCHES_KXT3" urn="urn:adsk.eagle:footprint:8413680/1" library_version="40" library_locally_modified="yes">
<smd name="1" x="-1.625" y="0" dx="1.5" dy="0.55" layer="1" rot="R90"/>
<smd name="2" x="1.625" y="0" dx="1.5" dy="0.55" layer="1" rot="R90"/>
<wire x1="-2.1" y1="1.1" x2="-2.1" y2="-1.1" width="0.127" layer="21"/>
<wire x1="-2.1" y1="-1.1" x2="2.1" y2="-1.1" width="0.127" layer="21"/>
<wire x1="2.1" y1="-1.1" x2="2.1" y2="1.1" width="0.127" layer="21"/>
<wire x1="2.1" y1="1.1" x2="-2.1" y2="1.1" width="0.127" layer="21"/>
<wire x1="-2.1" y1="-1.1" x2="2.1" y2="-1.1" width="0.127" layer="51"/>
<wire x1="-2.1" y1="1.1" x2="-2.1" y2="-1.1" width="0.127" layer="51"/>
<wire x1="2.1" y1="1.1" x2="-2.1" y2="1.1" width="0.127" layer="51"/>
<wire x1="2.1" y1="-1.1" x2="2.1" y2="1.1" width="0.127" layer="51"/>
<text x="-2" y="1.5" size="0.4064" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="-2" y="-2" size="0.4064" layer="27" font="vector" ratio="15">&gt;VALUE</text>
</package>
<package name="CRYSTAL_CYLINDRICAL_RADIAL" urn="urn:adsk.eagle:footprint:8413679/1" library_version="40" library_locally_modified="yes">
<smd name="1" x="-0.45" y="2.45" dx="1.1" dy="0.6" layer="1" rot="R90"/>
<smd name="2" x="0.45" y="2.45" dx="1.1" dy="0.6" layer="1" rot="R90"/>
<smd name="3" x="0" y="-3.1" dx="2.4" dy="1.5" layer="1" rot="R90"/>
<wire x1="-1" y1="2" x2="-1" y2="-4.5" width="0.127" layer="21"/>
<wire x1="1" y1="2" x2="1" y2="-4.5" width="0.127" layer="21"/>
<wire x1="1" y1="-4.5" x2="-1" y2="-4.5" width="0.127" layer="21"/>
<wire x1="-1" y1="2" x2="-1" y2="-4.5" width="0.127" layer="51"/>
<wire x1="-1" y1="-4.5" x2="1" y2="-4.5" width="0.127" layer="51"/>
<wire x1="1" y1="-4.5" x2="1" y2="2" width="0.127" layer="51"/>
<wire x1="-1" y1="2.75" x2="-1" y2="3.25" width="0.127" layer="51"/>
<wire x1="-1" y1="3.25" x2="1" y2="3.25" width="0.127" layer="51"/>
<wire x1="1" y1="3.25" x2="1" y2="2.75" width="0.127" layer="51"/>
<wire x1="-1" y1="2.75" x2="-1" y2="3.25" width="0.127" layer="21"/>
<wire x1="-1" y1="3.25" x2="1" y2="3.25" width="0.127" layer="21"/>
<wire x1="1" y1="3.25" x2="1" y2="2.75" width="0.127" layer="21"/>
<text x="2" y="2" size="0.4064" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="2" y="1" size="0.4064" layer="27" font="vector" ratio="15">&gt;VALUE</text>
</package>
<package name="OSCCC500X320X110" urn="urn:adsk.eagle:footprint:3849468/1" library_version="58">
<description>OSCILLATOR CORNERCONCAVE, 5 X 3.2 X 1.1 mm body
&lt;p&gt;OSCILLATOR CORNERCONCAVE package with body size 5 X 3.2 X 1.1 mm&lt;/p&gt;</description>
<circle x="-3.2349" y="-1.253" radius="0.25" width="0" layer="21"/>
<wire x1="-2.7909" y1="0.421" x2="-2.7909" y2="-0.421" width="0.12" layer="21"/>
<wire x1="2.7909" y1="0.421" x2="2.7909" y2="-0.421" width="0.12" layer="21"/>
<wire x1="-0.971" y1="1.8909" x2="0.971" y2="1.8909" width="0.12" layer="21"/>
<wire x1="-0.971" y1="-1.8909" x2="0.971" y2="-1.8909" width="0.12" layer="21"/>
<wire x1="2.575" y1="-1.675" x2="-2.575" y2="-1.675" width="0.12" layer="51"/>
<wire x1="-2.575" y1="-1.675" x2="-2.575" y2="1.675" width="0.12" layer="51"/>
<wire x1="-2.575" y1="1.675" x2="2.575" y2="1.675" width="0.12" layer="51"/>
<wire x1="2.575" y1="1.675" x2="2.575" y2="-1.675" width="0.12" layer="51"/>
<smd name="1" x="-1.978" y="-1.253" dx="1.5059" dy="1.1559" layer="1"/>
<smd name="2" x="1.978" y="-1.253" dx="1.5059" dy="1.1559" layer="1"/>
<smd name="3" x="1.978" y="1.253" dx="1.5059" dy="1.1559" layer="1"/>
<smd name="4" x="-1.978" y="1.253" dx="1.5059" dy="1.1559" layer="1"/>
<text x="0" y="2.5259" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.5259" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="LED_CUSTOM_0402/RADIAL" urn="urn:adsk.eagle:footprint:8413687/1" library_version="41" library_locally_modified="yes">
<smd name="A_2" x="0" y="0.45" dx="0.5" dy="0.7" layer="1" roundness="25" rot="R270"/>
<smd name="K_2" x="0" y="-0.45" dx="0.5" dy="0.7" layer="1" roundness="25" rot="R270"/>
<smd name="A_3" x="0" y="0.45" dx="0.5" dy="0.7" layer="16" roundness="25" rot="R270"/>
<smd name="K_3" x="0" y="-0.45" dx="0.5" dy="0.7" layer="16" roundness="25" rot="R270"/>
<pad name="A_1" x="0" y="1.27" drill="0.6" diameter="1.143" shape="square" rot="R270"/>
<pad name="K_1" x="0" y="-1.27" drill="0.6" diameter="1.143" shape="octagon" rot="R270"/>
<text x="1.27" y="-3.81" size="1.27" layer="25" rot="R180">&gt;NAME</text>
<text x="1.27" y="5.08" size="1.27" layer="27" rot="R180">&gt;VALUE</text>
<wire x1="-1.905" y1="0.385" x2="-0.635" y2="0.385" width="0.127" layer="21"/>
<wire x1="-0.635" y1="0.385" x2="-1.27" y2="-0.385" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-0.385" x2="-1.905" y2="0.385" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-0.385" x2="-1.905" y2="-0.385" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-0.385" x2="-0.635" y2="-0.385" width="0.127" layer="21"/>
<wire x1="-1.25" y1="1" x2="-1.25" y2="0.375" width="0.127" layer="21"/>
<wire x1="-1.25" y1="-0.375" x2="-1.25" y2="-1" width="0.127" layer="21"/>
<wire x1="-1.5" y1="-2.125" x2="1.5" y2="-2.125" width="0.127" layer="21"/>
<circle x="0" y="0" radius="2.60108125" width="0.127" layer="21"/>
</package>
<package name="LED_RADIAL" urn="urn:adsk.eagle:footprint:8413692/1" library_version="41" library_locally_modified="yes">
<pad name="A" x="1.27" y="0" drill="0.6" diameter="1" shape="square" rot="R180"/>
<pad name="K" x="-1.27" y="0" drill="0.6" diameter="1" rot="R180"/>
<text x="-2.54" y="2.794" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-4.064" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="0.385" y1="1.905" x2="0.385" y2="0.635" width="0.127" layer="21"/>
<wire x1="0.385" y1="0.635" x2="-0.385" y2="1.27" width="0.127" layer="21"/>
<wire x1="-0.385" y1="1.27" x2="0.385" y2="1.905" width="0.127" layer="21"/>
<wire x1="-0.385" y1="1.27" x2="-0.385" y2="1.905" width="0.127" layer="21"/>
<wire x1="-0.385" y1="1.27" x2="-0.385" y2="0.635" width="0.127" layer="21"/>
<wire x1="1" y1="1.25" x2="0.375" y2="1.25" width="0.127" layer="21"/>
<wire x1="-0.375" y1="1.25" x2="-1" y2="1.25" width="0.127" layer="21"/>
<wire x1="-2.125" y1="1.5" x2="-2.125" y2="-1.5" width="0.127" layer="21"/>
<circle x="0" y="0" radius="2.60108125" width="0.127" layer="21"/>
</package>
<package name="LEDC1005X60N" urn="urn:adsk.eagle:footprint:15620583/1" library_version="41" library_locally_modified="yes">
<wire x1="-0.52" y1="-0.93" x2="-0.52" y2="0.93" width="0.127" layer="21"/>
<wire x1="-0.52" y1="0.93" x2="0.52" y2="0.93" width="0.127" layer="21"/>
<wire x1="0.52" y1="0.93" x2="0.52" y2="-0.93" width="0.127" layer="21"/>
<wire x1="0.52" y1="-0.93" x2="-0.52" y2="-0.93" width="0.127" layer="21" style="shortdash"/>
<wire x1="-0.52" y1="-0.93" x2="-0.52" y2="0.93" width="0.127" layer="51"/>
<wire x1="-0.52" y1="0.93" x2="0.52" y2="0.93" width="0.127" layer="51"/>
<wire x1="0.52" y1="0.93" x2="0.52" y2="-0.93" width="0.127" layer="51"/>
<wire x1="0.52" y1="-0.93" x2="-0.52" y2="-0.93" width="0.127" layer="51" style="shortdash"/>
<wire x1="-0.5" y1="-1.15" x2="0.5" y2="-1.15" width="0.127" layer="21"/>
<wire x1="-0.5" y1="-1.15" x2="0.5" y2="-1.15" width="0.127" layer="51"/>
<smd name="1" x="0" y="0.45" dx="0.5" dy="0.7" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-0.45" dx="0.5" dy="0.7" layer="1" roundness="25" rot="R270"/>
<text x="0.635" y="1.27" size="0.889" layer="25" ratio="11" rot="R270">&gt;NAME</text>
<text x="-1.524" y="1.397" size="0.635" layer="27" font="vector" ratio="10" rot="R270">&gt;VALUE</text>
</package>
<package name="DO214AC_SMA" urn="urn:adsk.eagle:footprint:15671032/4" library_version="70">
<wire x1="3.43" y1="-2.02" x2="-3.43" y2="-2.02" width="0.127" layer="21"/>
<wire x1="-3.43" y1="-2.02" x2="-3.43" y2="2.02" width="0.127" layer="21"/>
<wire x1="-3.43" y1="2.02" x2="3.43" y2="2.02" width="0.127" layer="21"/>
<wire x1="3.43" y1="2.02" x2="3.43" y2="-2.02" width="0.127" layer="21" style="shortdash"/>
<wire x1="3.43" y1="-2.02" x2="-3.43" y2="-2.02" width="0.127" layer="21"/>
<wire x1="-3.43" y1="-2.02" x2="-3.43" y2="2.02" width="0.127" layer="21"/>
<wire x1="-3.43" y1="2.02" x2="3.43" y2="2.02" width="0.127" layer="21"/>
<wire x1="3.43" y1="2.02" x2="3.43" y2="-2.02" width="0.127" layer="21" style="shortdash"/>
<wire x1="3.65" y1="-2" x2="3.65" y2="2" width="0.127" layer="21"/>
<wire x1="3.65" y1="-2" x2="3.65" y2="2" width="0.127" layer="21"/>
<smd name="1" x="-2.25" y="0" dx="1.7" dy="2.5" layer="1" roundness="25"/>
<smd name="2" x="2.25" y="0" dx="1.7" dy="2.5" layer="1" roundness="25"/>
<text x="2" y="3" size="0.8128" layer="25" font="vector" ratio="15" rot="R180">&gt;NAME</text>
<text x="2" y="-2.25" size="0.635" layer="27" font="vector" ratio="10" rot="R180">&gt;VALUE</text>
</package>
<package name="SOIC127P600X175-9" urn="urn:adsk.eagle:footprint:3985888/2" library_version="76">
<description>8-SOIC, 1.27 mm pitch, 6 mm span, 4.9 X 3.9 X 1.75 mm body
&lt;p&gt;8-pin SOIC package with 1.27 mm pitch, 6 mm span with body size 4.9 X 3.9 X 1.75 mm&lt;/p&gt;</description>
<circle x="-2.7288" y="2.7086" radius="0.25" width="0" layer="21"/>
<wire x1="-2" y1="2.5186" x2="2" y2="2.5186" width="0.12" layer="21"/>
<wire x1="-2" y1="-2.5186" x2="2" y2="-2.5186" width="0.12" layer="21"/>
<wire x1="2" y1="-2.5" x2="-2" y2="-2.5" width="0.12" layer="51"/>
<wire x1="-2" y1="-2.5" x2="-2" y2="2.5" width="0.12" layer="51"/>
<wire x1="-2" y1="2.5" x2="2" y2="2.5" width="0.12" layer="51"/>
<wire x1="2" y1="2.5" x2="2" y2="-2.5" width="0.12" layer="51"/>
<smd name="1" x="-2.4734" y="1.905" dx="1.9685" dy="0.5991" layer="1"/>
<smd name="2" x="-2.4734" y="0.635" dx="1.9685" dy="0.5991" layer="1"/>
<smd name="3" x="-2.4734" y="-0.635" dx="1.9685" dy="0.5991" layer="1"/>
<smd name="4" x="-2.4734" y="-1.905" dx="1.9685" dy="0.5991" layer="1"/>
<smd name="5" x="2.4734" y="-1.905" dx="1.9685" dy="0.5991" layer="1"/>
<smd name="6" x="2.4734" y="-0.635" dx="1.9685" dy="0.5991" layer="1"/>
<smd name="7" x="2.4734" y="0.635" dx="1.9685" dy="0.5991" layer="1"/>
<smd name="8" x="2.4734" y="1.905" dx="1.9685" dy="0.5991" layer="1"/>
<smd name="9" x="0" y="0" dx="2.413" dy="3.0988" layer="1" cream="no"/>
<text x="0" y="3.5936" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-3.1536" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
<polygon width="0.0254" layer="31">
<vertex x="-0.9795" y="1.3224"/>
<vertex x="-0.9795" y="0.227"/>
<vertex x="0.9795" y="0.227"/>
<vertex x="0.9795" y="1.3224"/>
</polygon>
<polygon width="0.0254" layer="31">
<vertex x="-0.9795" y="-0.227"/>
<vertex x="-0.9795" y="-1.3224"/>
<vertex x="0.9795" y="-1.3224"/>
<vertex x="0.9795" y="-0.227"/>
</polygon>
</package>
<package name="SOP50P310X90-8" urn="urn:adsk.eagle:footprint:3986205/1" library_version="58">
<description>8-SOP, 0.5 mm pitch, 3.1 mm span, 2 X 2.3 X 0.9 mm body
&lt;p&gt;8-pin SOP package with 0.5 mm pitch, 3.1 mm span with body size 2 X 2.3 X 0.9 mm&lt;/p&gt;</description>
<circle x="-1.704" y="1.3877" radius="0.25" width="0" layer="21"/>
<wire x1="-1.2" y1="1.1977" x2="1.2" y2="1.1977" width="0.12" layer="21"/>
<wire x1="-1.2" y1="-1.1977" x2="1.2" y2="-1.1977" width="0.12" layer="21"/>
<wire x1="1.2" y1="-1.05" x2="-1.2" y2="-1.05" width="0.12" layer="51"/>
<wire x1="-1.2" y1="-1.05" x2="-1.2" y2="1.05" width="0.12" layer="51"/>
<wire x1="-1.2" y1="1.05" x2="1.2" y2="1.05" width="0.12" layer="51"/>
<wire x1="1.2" y1="1.05" x2="1.2" y2="-1.05" width="0.12" layer="51"/>
<smd name="1" x="-1.5032" y="0.75" dx="0.9228" dy="0.2675" layer="1"/>
<smd name="2" x="-1.5032" y="0.25" dx="0.9228" dy="0.2675" layer="1"/>
<smd name="3" x="-1.5032" y="-0.25" dx="0.9228" dy="0.2675" layer="1"/>
<smd name="4" x="-1.5032" y="-0.75" dx="0.9228" dy="0.2675" layer="1"/>
<smd name="5" x="1.5032" y="-0.75" dx="0.9228" dy="0.2675" layer="1"/>
<smd name="6" x="1.5032" y="-0.25" dx="0.9228" dy="0.2675" layer="1"/>
<smd name="7" x="1.5032" y="0.25" dx="0.9228" dy="0.2675" layer="1"/>
<smd name="8" x="1.5032" y="0.75" dx="0.9228" dy="0.2675" layer="1"/>
<text x="0" y="2.2727" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.8327" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="DIOM7959X262N" urn="urn:adsk.eagle:footprint:15726459/3" library_version="75" library_locally_modified="yes">
<description>Molded Body, 7.94 X 5.91 X 2.62 mm body
&lt;p&gt;Molded Body package with body size 7.94 X 5.91 X 2.62 mm&lt;/p&gt;</description>
<wire x1="4.065" y1="3.11" x2="-4.8871" y2="3.11" width="0.12" layer="21"/>
<wire x1="-4.8871" y1="3.11" x2="-4.8871" y2="-3.11" width="0.12" layer="21"/>
<wire x1="-4.8871" y1="-3.11" x2="4.065" y2="-3.11" width="0.12" layer="21"/>
<wire x1="4.065" y1="-3.11" x2="-4.065" y2="-3.11" width="0.12" layer="51"/>
<wire x1="-4.065" y1="-3.11" x2="-4.065" y2="3.11" width="0.12" layer="51"/>
<wire x1="-4.065" y1="3.11" x2="4.065" y2="3.11" width="0.12" layer="51"/>
<wire x1="4.065" y1="3.11" x2="4.065" y2="-3.11" width="0.12" layer="51"/>
<wire x1="-5.2578" y1="3.0226" x2="-5.2578" y2="-2.9972" width="0.1524" layer="21"/>
<smd name="1" x="-3.4827" y="0" dx="2.1808" dy="3.1202" layer="1"/>
<smd name="2" x="3.4827" y="0" dx="2.1808" dy="3.1202" layer="1"/>
<text x="0" y="3.745" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-3.745" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="RESC2012X70" urn="urn:adsk.eagle:footprint:3811170/1" library_version="58">
<description>CHIP, 2 X 1.25 X 0.7 mm body
&lt;p&gt;CHIP package with body size 2 X 1.25 X 0.7 mm&lt;/p&gt;</description>
<wire x1="1.1" y1="1.0036" x2="-1.1" y2="1.0036" width="0.12" layer="21"/>
<wire x1="1.1" y1="-1.0036" x2="-1.1" y2="-1.0036" width="0.12" layer="21"/>
<wire x1="1.1" y1="-0.675" x2="-1.1" y2="-0.675" width="0.12" layer="51"/>
<wire x1="-1.1" y1="-0.675" x2="-1.1" y2="0.675" width="0.12" layer="51"/>
<wire x1="-1.1" y1="0.675" x2="1.1" y2="0.675" width="0.12" layer="51"/>
<wire x1="1.1" y1="0.675" x2="1.1" y2="-0.675" width="0.12" layer="51"/>
<smd name="1" x="-0.94" y="0" dx="1.0354" dy="1.3791" layer="1"/>
<smd name="2" x="0.94" y="0" dx="1.0354" dy="1.3791" layer="1"/>
<text x="0" y="1.6386" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.6386" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="MOLEX_5031590400_SD" urn="urn:adsk.eagle:footprint:8413689/1" library_version="47">
<pad name="P$1" x="0" y="0" drill="0.7"/>
<pad name="P$2" x="1.5" y="2" drill="0.7"/>
<pad name="P$3" x="3" y="0" drill="0.7"/>
<pad name="P$4" x="4.5" y="2" drill="0.7"/>
<wire x1="-2.75" y1="-1.4" x2="-2.75" y2="5.6" width="0.127" layer="21"/>
<wire x1="-2.75" y1="5.6" x2="7.25" y2="5.6" width="0.127" layer="21"/>
<wire x1="7.25" y1="5.6" x2="7.25" y2="-1.4" width="0.127" layer="21"/>
<wire x1="7.25" y1="-1.4" x2="-2.75" y2="-1.4" width="0.127" layer="21"/>
<wire x1="-2.75" y1="-1.4" x2="7.25" y2="-1.4" width="0.127" layer="51"/>
<wire x1="7.25" y1="-1.4" x2="7.25" y2="5.6" width="0.127" layer="51"/>
<wire x1="7.25" y1="5.6" x2="-2.75" y2="5.6" width="0.127" layer="51"/>
<wire x1="-2.75" y1="5.6" x2="-2.75" y2="-1.4" width="0.127" layer="51"/>
<text x="9.8" y="0.7" size="0.6096" layer="25" font="vector">&gt;NAME</text>
<text x="9.8" y="0" size="0.6096" layer="27" font="vector">&gt;VALUE</text>
<hole x="-1.65" y="3.8" drill="0.8"/>
</package>
<package name="SAMTEC_TSW-104-XX-X-S" urn="urn:adsk.eagle:footprint:8413711/1" library_version="47">
<wire x1="-5.209" y1="1.155" x2="5.209" y2="1.155" width="0.2032" layer="21"/>
<wire x1="5.209" y1="1.155" x2="5.209" y2="-1.155" width="0.2032" layer="21"/>
<wire x1="5.209" y1="-1.155" x2="-5.209" y2="-1.155" width="0.2032" layer="21"/>
<wire x1="-5.209" y1="-1.155" x2="-5.209" y2="1.155" width="0.2032" layer="21"/>
<wire x1="5.715" y1="0.9525" x2="5.715" y2="-0.9525" width="0.127" layer="21"/>
<wire x1="3.81" y1="1.5875" x2="5.715" y2="1.5875" width="0.127" layer="51"/>
<wire x1="5.715" y1="1.5875" x2="5.715" y2="0" width="0.127" layer="51"/>
<wire x1="-5.209" y1="1.155" x2="5.209" y2="1.155" width="0.2032" layer="51"/>
<wire x1="5.209" y1="1.155" x2="5.209" y2="-1.155" width="0.2032" layer="51"/>
<wire x1="5.209" y1="-1.155" x2="-5.209" y2="-1.155" width="0.2032" layer="51"/>
<wire x1="-5.209" y1="-1.155" x2="-5.209" y2="1.155" width="0.2032" layer="51"/>
<pad name="1" x="3.81" y="0" drill="1" diameter="1.5" shape="square" rot="R180"/>
<pad name="2" x="1.27" y="0" drill="1" diameter="1.5" rot="R180"/>
<pad name="3" x="-1.27" y="0" drill="1" diameter="1.5" rot="R180"/>
<pad name="4" x="-3.81" y="0" drill="1" diameter="1.5" rot="R180"/>
<text x="-5.715" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="8.255" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-4.16" y1="-0.35" x2="-3.46" y2="0.35" layer="51"/>
<rectangle x1="-1.62" y1="-0.35" x2="-0.92" y2="0.35" layer="51"/>
<rectangle x1="0.92" y1="-0.35" x2="1.62" y2="0.35" layer="51"/>
<rectangle x1="3.46" y1="-0.35" x2="4.16" y2="0.35" layer="51"/>
</package>
<package name="SAMTEC_TSW-104-XX-X-S_NO_OUTLINE" urn="urn:adsk.eagle:footprint:8413712/1" library_version="47">
<pad name="1" x="3.81" y="0" drill="1" diameter="1.5" rot="R180"/>
<pad name="2" x="1.27" y="0" drill="1" diameter="1.5" rot="R180"/>
<pad name="3" x="-1.27" y="0" drill="1" diameter="1.5" rot="R180"/>
<pad name="4" x="-3.81" y="0" drill="1" diameter="1.5" rot="R180"/>
<text x="-5.715" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="8.255" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-4.16" y1="-0.35" x2="-3.46" y2="0.35" layer="51"/>
<rectangle x1="-1.62" y1="-0.35" x2="-0.92" y2="0.35" layer="51"/>
<rectangle x1="0.92" y1="-0.35" x2="1.62" y2="0.35" layer="51"/>
<rectangle x1="3.46" y1="-0.35" x2="4.16" y2="0.35" layer="51"/>
<wire x1="5.08" y1="-0.3175" x2="5.08" y2="0.3175" width="0.254" layer="21"/>
<wire x1="5.08" y1="-0.3175" x2="5.08" y2="0.3175" width="0.254" layer="51"/>
</package>
<package name="SAMTEC_TSW-104-XX-X-S_FLEXSMD_PAD" urn="urn:adsk.eagle:footprint:8413716/1" library_version="47">
<pad name="1" x="3.81" y="0" drill="1" diameter="1.5" rot="R180"/>
<pad name="2" x="1.27" y="0" drill="1" diameter="1.5" rot="R180"/>
<pad name="3" x="-1.27" y="0" drill="1" diameter="1.5" rot="R180"/>
<pad name="4" x="-3.81" y="0" drill="1" diameter="1.5" rot="R180"/>
<text x="-5.715" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="8.255" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-4.16" y1="-0.35" x2="-3.46" y2="0.35" layer="51"/>
<rectangle x1="-1.62" y1="-0.35" x2="-0.92" y2="0.35" layer="51"/>
<rectangle x1="0.92" y1="-0.35" x2="1.62" y2="0.35" layer="51"/>
<rectangle x1="3.46" y1="-0.35" x2="4.16" y2="0.35" layer="51"/>
<smd name="P$1" x="3.81" y="1.27" dx="1.27" dy="0.635" layer="1" rot="R90"/>
<smd name="P$2" x="1.27" y="1.27" dx="1.27" dy="0.635" layer="1" rot="R90"/>
<smd name="P$3" x="-1.27" y="1.27" dx="1.27" dy="0.635" layer="1" rot="R90"/>
<smd name="P$4" x="-3.81" y="1.27" dx="1.27" dy="0.635" layer="1" rot="R90"/>
<wire x1="-3.81" y1="0" x2="-3.81" y2="1.27" width="0.6096" layer="1"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="1.27" width="0.6096" layer="1"/>
<wire x1="1.27" y1="0" x2="1.27" y2="1.27" width="0.6096" layer="1"/>
<wire x1="3.81" y1="0" x2="3.81" y2="1.27" width="0.6096" layer="1"/>
</package>
<package name="JST_B4P-SHF-1AA" urn="urn:adsk.eagle:footprint:8413723/1" library_version="47">
<pad name="1" x="3.75" y="-0.7" drill="0.9"/>
<pad name="2" x="1.25" y="-0.7" drill="0.9"/>
<pad name="3" x="-1.25" y="-0.7" drill="0.9"/>
<pad name="4" x="-3.75" y="-0.7" drill="0.9"/>
<wire x1="-5.25" y1="2.8" x2="5.25" y2="2.8" width="0.127" layer="21"/>
<wire x1="-5.25" y1="-2.8" x2="5.25" y2="-2.8" width="0.127" layer="21"/>
<wire x1="-5.25" y1="-2.8" x2="-5.25" y2="-2.1" width="0.127" layer="21"/>
<wire x1="-5.25" y1="-2.1" x2="-5.25" y2="2.8" width="0.127" layer="21"/>
<wire x1="5.25" y1="-2.8" x2="5.25" y2="-2.1" width="0.127" layer="21"/>
<wire x1="5.25" y1="-2.1" x2="5.25" y2="2.8" width="0.127" layer="21"/>
<wire x1="-5.25" y1="-2.1" x2="5.25" y2="-2.1" width="0.127" layer="21"/>
<wire x1="-5.25" y1="2.8" x2="5.25" y2="2.8" width="0.127" layer="51"/>
<wire x1="-5.25" y1="-2.8" x2="5.25" y2="-2.8" width="0.127" layer="51"/>
<wire x1="-5.25" y1="-2.8" x2="-5.25" y2="-2.1" width="0.127" layer="51"/>
<wire x1="-5.25" y1="-2.1" x2="-5.25" y2="2.8" width="0.127" layer="51"/>
<wire x1="5.25" y1="-2.8" x2="5.25" y2="-2.1" width="0.127" layer="51"/>
<wire x1="5.25" y1="-2.1" x2="5.25" y2="2.8" width="0.127" layer="51"/>
<wire x1="-5.25" y1="-2.1" x2="5.25" y2="-2.1" width="0.127" layer="51"/>
<text x="-0.55" y="-4.3" size="1.27" layer="25">&gt;NAME</text>
<text x="-7.75" y="-4.3" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="JST_BS4P-SHF-1AA" urn="urn:adsk.eagle:footprint:8413724/1" library_version="47">
<pad name="1" x="3.75" y="0" drill="0.9"/>
<pad name="2" x="1.25" y="0" drill="0.9"/>
<pad name="3" x="-1.25" y="0" drill="0.9"/>
<pad name="4" x="-3.75" y="0" drill="0.9"/>
<text x="-0.55" y="-4.5" size="1.27" layer="25">&gt;NAME</text>
<text x="-7.75" y="-4.5" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-5.21" y1="3" x2="5.25" y2="3" width="0.127" layer="21"/>
<wire x1="-5.21" y1="-3" x2="5.25" y2="-3" width="0.127" layer="21"/>
<wire x1="-5.21" y1="3" x2="-5.21" y2="-3" width="0.127" layer="21"/>
<wire x1="5.25" y1="3" x2="5.25" y2="-3" width="0.127" layer="21"/>
<wire x1="-5.21" y1="3" x2="5.25" y2="3" width="0.127" layer="51"/>
<wire x1="-5.21" y1="-3" x2="5.25" y2="-3" width="0.127" layer="51"/>
<wire x1="-5.21" y1="3" x2="-5.21" y2="-3" width="0.127" layer="51"/>
<wire x1="5.25" y1="3" x2="5.25" y2="-3" width="0.127" layer="51"/>
</package>
<package name="JST_F4P-HVQ" urn="urn:adsk.eagle:footprint:8413725/1" library_version="47">
<pad name="1" x="3.75" y="-0.7" drill="1"/>
<pad name="2" x="1.25" y="-0.7" drill="1"/>
<pad name="3" x="-1.25" y="-0.7" drill="1"/>
<pad name="4" x="-3.75" y="-0.7" drill="1"/>
<wire x1="-7.25" y1="-5.3" x2="-7.25" y2="2.8" width="0.127" layer="51"/>
<wire x1="7.25" y1="-5.3" x2="7.25" y2="2.8" width="0.127" layer="51"/>
<wire x1="-7.25" y1="2.8" x2="7.25" y2="2.8" width="0.127" layer="51"/>
<wire x1="-5.75" y1="-1.9" x2="5.75" y2="-1.9" width="0.127" layer="51"/>
<wire x1="-7.25" y1="-5.3" x2="-5.75" y2="-5.3" width="0.127" layer="51"/>
<wire x1="5.75" y1="-5.3" x2="7.25" y2="-5.3" width="0.127" layer="51"/>
<wire x1="-5.75" y1="-1.9" x2="-5.75" y2="-5.3" width="0.127" layer="51"/>
<wire x1="5.75" y1="-1.9" x2="5.75" y2="-5.3" width="0.127" layer="51"/>
<wire x1="-7.25" y1="-1.9" x2="-7.25" y2="2.8" width="0.127" layer="21"/>
<wire x1="7.25" y1="-1.9" x2="7.25" y2="2.8" width="0.127" layer="21"/>
<wire x1="-7.25" y1="2.8" x2="7.25" y2="2.8" width="0.127" layer="21"/>
<wire x1="-7.25" y1="-1.9" x2="7.25" y2="-1.9" width="0.127" layer="21"/>
<text x="-0.55" y="-6.95" size="1.27" layer="25">&gt;NAME</text>
<text x="-7.75" y="-6.95" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="JST_F4P-SHVQ" urn="urn:adsk.eagle:footprint:8413726/1" library_version="47">
<pad name="1" x="3.82" y="-3.35" drill="1"/>
<pad name="2" x="1.32" y="-3.35" drill="1"/>
<pad name="3" x="-1.18" y="-3.35" drill="1"/>
<pad name="4" x="-3.68" y="-3.35" drill="1"/>
<text x="1.3" y="-5.95" size="1.27" layer="25">&gt;NAME</text>
<text x="-6.2" y="-5.95" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-7.2" y1="3.05" x2="7.3" y2="3.05" width="0.127" layer="21"/>
<wire x1="-7.2" y1="-4.45" x2="-7.2" y2="3.05" width="0.127" layer="21"/>
<wire x1="7.3" y1="-4.45" x2="7.3" y2="3.05" width="0.127" layer="21"/>
<wire x1="7.3" y1="-4.45" x2="-7.2" y2="-4.45" width="0.127" layer="21"/>
<wire x1="-5.7" y1="3.05" x2="5.8" y2="3.05" width="0.127" layer="51"/>
<wire x1="-7.2" y1="-4.45" x2="-7.2" y2="4.85" width="0.127" layer="51"/>
<wire x1="7.3" y1="-4.45" x2="7.3" y2="4.85" width="0.127" layer="51"/>
<wire x1="7.3" y1="-4.45" x2="-7.2" y2="-4.45" width="0.127" layer="51"/>
<wire x1="-7.2" y1="4.85" x2="-5.7" y2="4.85" width="0.127" layer="51"/>
<wire x1="5.8" y1="4.85" x2="7.3" y2="4.85" width="0.127" layer="51"/>
<wire x1="-5.7" y1="4.85" x2="-5.7" y2="3.05" width="0.127" layer="51"/>
<wire x1="5.8" y1="4.85" x2="5.8" y2="3.05" width="0.127" layer="51"/>
</package>
<package name="JST_04JQ-BT" urn="urn:adsk.eagle:footprint:8413851/1" library_version="47">
<pad name="1" x="3.75" y="0" drill="0.9"/>
<pad name="2" x="1.25" y="0" drill="0.9"/>
<pad name="3" x="-1.25" y="0" drill="0.9"/>
<pad name="4" x="-3.75" y="0" drill="0.9"/>
<wire x1="-6.25" y1="2.7" x2="6.25" y2="2.7" width="0.127" layer="21"/>
<wire x1="6.25" y1="-3.3" x2="-6.25" y2="-3.3" width="0.127" layer="21"/>
<wire x1="-6.25" y1="2.7" x2="-6.25" y2="-3.3" width="0.127" layer="21"/>
<wire x1="6.25" y1="2.7" x2="6.25" y2="-3.3" width="0.127" layer="21"/>
<wire x1="-6.25" y1="2.7" x2="6.25" y2="2.7" width="0.127" layer="51"/>
<wire x1="6.25" y1="-3.3" x2="-6.25" y2="-3.3" width="0.127" layer="51"/>
<wire x1="-6.25" y1="2.7" x2="-6.25" y2="-3.3" width="0.127" layer="51"/>
<wire x1="6.25" y1="2.7" x2="6.25" y2="-3.3" width="0.127" layer="51"/>
<text x="0.5" y="2.93" size="1.27" layer="25">&gt;NAME</text>
<text x="-7.75" y="2.93" size="1.27" layer="27">&gt;VALUE</text>
<circle x="3.75" y="-3.85" radius="0.25" width="0.127" layer="21"/>
<circle x="3.75" y="-3.85" radius="0.13" width="0.127" layer="21"/>
<circle x="3.75" y="-3.85" radius="0.014140625" width="0.127" layer="21"/>
<circle x="3.75" y="-3.85" radius="0.25" width="0.127" layer="51"/>
<circle x="3.75" y="-3.85" radius="0.13" width="0.127" layer="51"/>
<circle x="3.75" y="-3.85" radius="0.014140625" width="0.127" layer="51"/>
</package>
<package name="JST_04JQ-ST" urn="urn:adsk.eagle:footprint:8413852/1" library_version="47">
<pad name="1" x="3.75" y="0" drill="0.9"/>
<pad name="2" x="1.25" y="0" drill="0.9"/>
<pad name="3" x="-1.25" y="0" drill="0.9"/>
<pad name="4" x="-3.75" y="0" drill="0.9"/>
<wire x1="-6.25" y1="-2.2" x2="6.25" y2="-2.2" width="0.127" layer="51"/>
<wire x1="-6.25" y1="2.6" x2="-5.35" y2="2.6" width="0.127" layer="51"/>
<wire x1="-5.35" y1="2.6" x2="5.35" y2="2.6" width="0.127" layer="51"/>
<wire x1="5.35" y1="2.6" x2="6.25" y2="2.6" width="0.127" layer="51"/>
<wire x1="6.25" y1="-2.2" x2="6.25" y2="2.6" width="0.127" layer="51"/>
<wire x1="-6.25" y1="-2.2" x2="-6.25" y2="2.6" width="0.127" layer="51"/>
<wire x1="-6.25" y1="-2.2" x2="6.25" y2="-2.2" width="0.127" layer="21"/>
<wire x1="-6.25" y1="2.6" x2="6.25" y2="2.6" width="0.127" layer="21"/>
<wire x1="6.25" y1="-2.2" x2="6.25" y2="2.6" width="0.127" layer="21"/>
<wire x1="-6.25" y1="-2.2" x2="-6.25" y2="2.6" width="0.127" layer="21"/>
<wire x1="-5.35" y1="2.6" x2="-5.35" y2="7.3" width="0.127" layer="51"/>
<wire x1="5.35" y1="2.6" x2="5.35" y2="7.3" width="0.127" layer="51"/>
<wire x1="-5.35" y1="7.3" x2="5.35" y2="7.3" width="0.127" layer="51"/>
<text x="-4.15" y="-3.82" size="1.27" layer="25">&gt;NAME</text>
<text x="-12.4" y="-3.82" size="1.27" layer="27">&gt;VALUE</text>
<circle x="3.75" y="-2.75" radius="0.25" width="0.127" layer="21"/>
<circle x="3.75" y="-2.75" radius="0.13" width="0.127" layer="21"/>
<circle x="3.75" y="-2.75" radius="0.014140625" width="0.127" layer="21"/>
<circle x="3.75" y="-2.75" radius="0.25" width="0.127" layer="51"/>
<circle x="3.75" y="-2.75" radius="0.13" width="0.127" layer="51"/>
<circle x="3.75" y="-2.75" radius="0.014140625" width="0.127" layer="51"/>
</package>
<package name="JST_B4B-XH-A" urn="urn:adsk.eagle:footprint:8413853/1" library_version="47">
<pad name="1" x="3.73" y="0" drill="0.9"/>
<pad name="2" x="1.23" y="0" drill="0.9"/>
<pad name="3" x="-1.27" y="0" drill="0.9"/>
<pad name="4" x="-3.77" y="0" drill="0.9"/>
<wire x1="-6.22" y1="2.35" x2="6.18" y2="2.35" width="0.127" layer="51"/>
<wire x1="-6.22" y1="-3.4" x2="6.18" y2="-3.4" width="0.127" layer="51"/>
<wire x1="-6.22" y1="2.35" x2="-6.22" y2="-3.4" width="0.127" layer="51"/>
<wire x1="6.18" y1="2.35" x2="6.18" y2="-3.4" width="0.127" layer="51"/>
<wire x1="-6.22" y1="2.35" x2="6.18" y2="2.35" width="0.127" layer="21"/>
<wire x1="-6.22" y1="-3.4" x2="6.18" y2="-3.4" width="0.127" layer="21"/>
<wire x1="-6.22" y1="2.35" x2="-6.22" y2="-3.4" width="0.127" layer="21"/>
<wire x1="6.18" y1="2.35" x2="6.18" y2="-3.4" width="0.127" layer="21"/>
<text x="-2.75" y="-4.94" size="1.27" layer="25">&gt;NAME</text>
<text x="-10" y="-4.94" size="1.27" layer="27">&gt;VALUE</text>
<circle x="3.73" y="-4" radius="0.25" width="0.127" layer="21"/>
<circle x="3.73" y="-4" radius="0.13" width="0.127" layer="21"/>
<circle x="3.73" y="-4" radius="0.014140625" width="0.127" layer="21"/>
<circle x="3.73" y="-4" radius="0.25" width="0.127" layer="51"/>
<circle x="3.73" y="-4" radius="0.13" width="0.127" layer="51"/>
<circle x="3.73" y="-4" radius="0.014140625" width="0.127" layer="51"/>
</package>
<package name="JST_S4B-XH-A-1" urn="urn:adsk.eagle:footprint:8413854/1" library_version="47">
<pad name="1" x="3.75" y="0" drill="0.9"/>
<pad name="2" x="1.25" y="0" drill="0.9"/>
<pad name="3" x="-1.25" y="0" drill="0.9"/>
<pad name="4" x="-3.75" y="0" drill="0.9"/>
<wire x1="-6.2" y1="7.6" x2="6.2" y2="7.6" width="0.127" layer="21"/>
<wire x1="-6.2" y1="-3.9" x2="6.2" y2="-3.9" width="0.127" layer="21"/>
<wire x1="-6.2" y1="-3.9" x2="-6.2" y2="7.6" width="0.127" layer="21"/>
<wire x1="6.2" y1="-3.9" x2="6.2" y2="7.6" width="0.127" layer="21"/>
<wire x1="-6.2" y1="7.6" x2="6.2" y2="7.6" width="0.127" layer="51"/>
<wire x1="-6.2" y1="-3.9" x2="6.2" y2="-3.9" width="0.127" layer="51"/>
<wire x1="-6.2" y1="-3.9" x2="-6.2" y2="7.6" width="0.127" layer="51"/>
<wire x1="6.2" y1="-3.9" x2="6.2" y2="7.6" width="0.127" layer="51"/>
<text x="0.2" y="-5.395" size="1.27" layer="25">&gt;NAME</text>
<text x="-7.3" y="-5.395" size="1.27" layer="27">&gt;VALUE</text>
<circle x="3.75" y="-2.125" radius="0.25" width="0.127" layer="21"/>
<circle x="3.75" y="-2.125" radius="0.13" width="0.127" layer="21"/>
<circle x="3.75" y="-2.125" radius="0.014140625" width="0.127" layer="21"/>
<circle x="3.75" y="-2.125" radius="0.25" width="0.127" layer="51"/>
<circle x="3.75" y="-2.125" radius="0.13" width="0.127" layer="51"/>
<circle x="3.75" y="-2.125" radius="0.014140625" width="0.127" layer="51"/>
</package>
<package name="JST_B04B-PASK" urn="urn:adsk.eagle:footprint:8413901/1" library_version="47">
<pad name="P$1" x="3" y="0" drill="0.7" rot="R90"/>
<pad name="P$2" x="1" y="0" drill="0.7" rot="R90"/>
<pad name="P$3" x="-1" y="0" drill="0.7" rot="R90"/>
<pad name="P$4" x="-3" y="0" drill="0.7" rot="R90"/>
<hole x="4.5" y="1.7" drill="1.1"/>
<wire x1="-5" y1="-3.05" x2="5" y2="-3.05" width="0.127" layer="21"/>
<wire x1="-5" y1="2.25" x2="4.03" y2="2.25" width="0.127" layer="21"/>
<wire x1="-5" y1="2.25" x2="-5" y2="-3.05" width="0.127" layer="21"/>
<wire x1="5" y1="-3.05" x2="5" y2="1.17" width="0.127" layer="21"/>
<wire x1="-5" y1="-3.05" x2="5" y2="-3.05" width="0.127" layer="51"/>
<wire x1="-5" y1="2.25" x2="4.03" y2="2.25" width="0.127" layer="51"/>
<wire x1="-5" y1="2.25" x2="-5" y2="-3.05" width="0.127" layer="51"/>
<wire x1="5" y1="-3.05" x2="5" y2="1.17" width="0.127" layer="51"/>
<text x="-2.5" y="2.48" size="1.27" layer="25">&gt;NAME</text>
<text x="-10.75" y="2.48" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="JST_S04B-PASK-2" urn="urn:adsk.eagle:footprint:8413902/1" library_version="47">
<pad name="1" x="3" y="-2.35" drill="0.7"/>
<pad name="2" x="1" y="-2.35" drill="0.7"/>
<pad name="3" x="-1" y="-2.35" drill="0.7"/>
<pad name="4" x="-3" y="-2.35" drill="0.7"/>
<hole x="4.5" y="-0.25" drill="1.1"/>
<hole x="-4.5" y="-0.25" drill="1.1"/>
<wire x1="-5" y1="5.85" x2="5" y2="5.85" width="0.127" layer="21"/>
<wire x1="-5" y1="-5.85" x2="5" y2="-5.85" width="0.127" layer="21"/>
<wire x1="-5" y1="5.85" x2="-5" y2="0.27" width="0.127" layer="21"/>
<wire x1="5" y1="5.85" x2="5" y2="0.27" width="0.127" layer="21"/>
<wire x1="-5" y1="-5.85" x2="-5" y2="-0.77" width="0.127" layer="21"/>
<wire x1="5" y1="-5.85" x2="5" y2="-0.77" width="0.127" layer="21"/>
<wire x1="-5" y1="5.85" x2="5" y2="5.85" width="0.127" layer="51"/>
<wire x1="-5" y1="-5.85" x2="5" y2="-5.85" width="0.127" layer="51"/>
<wire x1="-5" y1="5.85" x2="-5" y2="0.27" width="0.127" layer="51"/>
<wire x1="5" y1="5.85" x2="5" y2="0.27" width="0.127" layer="51"/>
<wire x1="-5" y1="-5.85" x2="-5" y2="-0.77" width="0.127" layer="51"/>
<wire x1="5" y1="-5.85" x2="5" y2="-0.77" width="0.127" layer="51"/>
<text x="-0.95" y="-7.35" size="1.27" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="-9.2" y="-7.35" size="1.27" layer="27" font="vector" ratio="15">&gt;VALUE</text>
<circle x="3" y="-4" radius="0.25" width="0.127" layer="21"/>
<circle x="3" y="-4" radius="0.13" width="0.127" layer="21"/>
<circle x="3" y="-4" radius="0.014140625" width="0.127" layer="21"/>
<circle x="3" y="-4" radius="0.25" width="0.127" layer="51"/>
<circle x="3" y="-4" radius="0.13" width="0.127" layer="51"/>
<circle x="3" y="-4" radius="0.014140625" width="0.127" layer="51"/>
</package>
<package name="JST_B4B-PH-K-S" urn="urn:adsk.eagle:footprint:8413934/1" library_version="47">
<pad name="1" x="3" y="-0.55" drill="0.7"/>
<pad name="2" x="1" y="-0.55" drill="0.7"/>
<pad name="3" x="-1" y="-0.55" drill="0.7"/>
<pad name="4" x="-3" y="-0.55" drill="0.7"/>
<wire x1="-4.95" y1="-2.25" x2="4.95" y2="-2.25" width="0.127" layer="21"/>
<wire x1="-4.95" y1="2.25" x2="4.95" y2="2.25" width="0.127" layer="21"/>
<wire x1="-4.95" y1="2.25" x2="-4.95" y2="-2.25" width="0.127" layer="21"/>
<wire x1="4.95" y1="2.25" x2="4.95" y2="-2.25" width="0.127" layer="21"/>
<wire x1="-4.95" y1="-2.25" x2="4.95" y2="-2.25" width="0.127" layer="51"/>
<wire x1="-4.95" y1="2.25" x2="4.95" y2="2.25" width="0.127" layer="51"/>
<wire x1="-4.95" y1="2.25" x2="-4.95" y2="-2.25" width="0.127" layer="51"/>
<wire x1="4.95" y1="2.25" x2="4.95" y2="-2.25" width="0.127" layer="51"/>
<text x="-4.55" y="-4.07" size="1.27" layer="25">&gt;NAME</text>
<text x="-12.8" y="-4.07" size="1.27" layer="27">&gt;VALUE</text>
<circle x="3" y="-2.8" radius="0.25" width="0.127" layer="21"/>
<circle x="3" y="-2.8" radius="0.13" width="0.127" layer="21"/>
<circle x="3" y="-2.8" radius="0.014140625" width="0.127" layer="21"/>
<circle x="3" y="-2.8" radius="0.25" width="0.127" layer="51"/>
<circle x="3" y="-2.8" radius="0.13" width="0.127" layer="51"/>
<circle x="3" y="-2.8" radius="0.014140625" width="0.127" layer="51"/>
</package>
<package name="JST_S4B-PH-K-S" urn="urn:adsk.eagle:footprint:8413935/1" library_version="47">
<pad name="1" x="3" y="-2.875" drill="0.7"/>
<pad name="2" x="1" y="-2.875" drill="0.7"/>
<pad name="3" x="-1" y="-2.875" drill="0.7"/>
<pad name="4" x="-3" y="-2.875" drill="0.7"/>
<text x="-3.3" y="-5.395" size="1.27" layer="25">&gt;NAME</text>
<text x="-10.55" y="-5.395" size="1.27" layer="27">&gt;VALUE</text>
<circle x="3" y="-4.375" radius="0.25" width="0.127" layer="21"/>
<circle x="3" y="-4.375" radius="0.13" width="0.127" layer="21"/>
<circle x="3" y="-4.375" radius="0.014140625" width="0.127" layer="21"/>
<circle x="3" y="-4.375" radius="0.25" width="0.127" layer="51"/>
<circle x="3" y="-4.375" radius="0.13" width="0.127" layer="51"/>
<circle x="3" y="-4.375" radius="0.014140625" width="0.127" layer="51"/>
<wire x1="-4.95" y1="-3.925" x2="4.95" y2="-3.925" width="0.127" layer="51"/>
<wire x1="-4.95" y1="3.925" x2="4.95" y2="3.925" width="0.127" layer="51"/>
<wire x1="-4.95" y1="3.925" x2="-4.95" y2="-3.925" width="0.127" layer="51"/>
<wire x1="4.95" y1="3.925" x2="4.95" y2="-3.925" width="0.127" layer="51"/>
<wire x1="-4.95" y1="-3.925" x2="4.95" y2="-3.925" width="0.127" layer="21"/>
<wire x1="-4.95" y1="3.925" x2="4.95" y2="3.925" width="0.127" layer="21"/>
<wire x1="-4.95" y1="3.925" x2="-4.95" y2="-3.925" width="0.127" layer="21"/>
<wire x1="4.95" y1="3.925" x2="4.95" y2="-3.925" width="0.127" layer="21"/>
</package>
<package name="JST_B04B-PSILE-A1_B04B-PSIY-B1_B04B-PSIR-C1" urn="urn:adsk.eagle:footprint:8413966/1" library_version="47">
<pad name="P$1" x="6" y="1" drill="1.65"/>
<pad name="P$2" x="2" y="1" drill="1.65"/>
<pad name="P$3" x="-2" y="1" drill="1.65"/>
<pad name="P$4" x="-6" y="1" drill="1.65"/>
<hole x="7.65" y="-3.5" drill="1.3"/>
<wire x1="-8.35" y1="4.55" x2="8.35" y2="4.55" width="0.127" layer="51"/>
<wire x1="-8.35" y1="-4.55" x2="8.35" y2="-4.55" width="0.127" layer="51"/>
<wire x1="-8.35" y1="4.55" x2="-8.35" y2="-4.55" width="0.127" layer="51"/>
<wire x1="8.35" y1="-3.07" x2="8.35" y2="4.55" width="0.127" layer="51"/>
<wire x1="8.35" y1="-4.55" x2="8.35" y2="-3.93" width="0.127" layer="51"/>
<wire x1="-8.35" y1="4.55" x2="8.35" y2="4.55" width="0.127" layer="21"/>
<wire x1="-8.35" y1="-4.55" x2="8.35" y2="-4.55" width="0.127" layer="21"/>
<wire x1="-8.35" y1="4.55" x2="-8.35" y2="-4.55" width="0.127" layer="21"/>
<wire x1="8.35" y1="-3.07" x2="8.35" y2="4.55" width="0.127" layer="21"/>
<wire x1="8.35" y1="-4.55" x2="8.35" y2="-3.93" width="0.127" layer="21"/>
<text x="2.45" y="-6.07" size="1.27" layer="25">&gt;NAME</text>
<text x="-4.8" y="-6.07" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="JST_B4PS-VH" urn="urn:adsk.eagle:footprint:8413982/1" library_version="47">
<pad name="P$1" x="-5.94" y="0" drill="1.65"/>
<pad name="P$2" x="-1.98" y="0" drill="1.65"/>
<pad name="P$3" x="1.98" y="0" drill="1.65"/>
<pad name="P$4" x="5.94" y="0" drill="1.65"/>
<text x="-7.96" y="3" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.96" y="3" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-7.89" y1="-7.2" x2="-7.89" y2="1.72" width="0.127" layer="51"/>
<wire x1="-7.36" y1="2.25" x2="7.92" y2="2.25" width="0.127" layer="51"/>
<wire x1="-7.89" y1="-7.2" x2="7.92" y2="-7.2" width="0.127" layer="51"/>
<wire x1="-7.89" y1="1.72" x2="-7.36" y2="2.25" width="0.127" layer="51"/>
<wire x1="7.92" y1="2.25" x2="7.92" y2="-7.2" width="0.127" layer="51"/>
<wire x1="-7.89" y1="-7.2" x2="-7.89" y2="1.72" width="0.127" layer="21"/>
<wire x1="-7.36" y1="2.25" x2="7.92" y2="2.25" width="0.127" layer="21"/>
<wire x1="-7.89" y1="-7.2" x2="7.92" y2="-7.2" width="0.127" layer="21"/>
<wire x1="-7.89" y1="1.72" x2="-7.36" y2="2.25" width="0.127" layer="21"/>
<wire x1="7.92" y1="2.25" x2="7.92" y2="-7.2" width="0.127" layer="21"/>
</package>
<package name="JST_B4P-VH" urn="urn:adsk.eagle:footprint:8413983/1" library_version="47">
<pad name="P$1" x="-5.94" y="0" drill="1.65"/>
<pad name="P$2" x="-1.98" y="0" drill="1.65"/>
<pad name="P$3" x="1.98" y="0" drill="1.65"/>
<pad name="P$4" x="5.94" y="0" drill="1.65"/>
<text x="-7.96" y="-7" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.96" y="-7" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-7.91" y1="-5.25" x2="-7.91" y2="2.5" width="0.127" layer="51"/>
<wire x1="-7.91" y1="2.5" x2="-7.91" y2="3.25" width="0.127" layer="51"/>
<wire x1="7.89" y1="3.25" x2="7.89" y2="2.5" width="0.127" layer="51"/>
<wire x1="7.89" y1="2.5" x2="7.9" y2="-5.25" width="0.127" layer="51"/>
<wire x1="-7.91" y1="3.25" x2="7.89" y2="3.25" width="0.127" layer="51"/>
<wire x1="-7.91" y1="-5.25" x2="7.9" y2="-5.25" width="0.127" layer="51"/>
<wire x1="-7.91" y1="2.5" x2="7.89" y2="2.5" width="0.127" layer="51"/>
<wire x1="-7.91" y1="-5.25" x2="-7.91" y2="2.5" width="0.127" layer="21"/>
<wire x1="-7.91" y1="2.5" x2="-7.91" y2="3.25" width="0.127" layer="21"/>
<wire x1="7.89" y1="3.25" x2="7.89" y2="2.5" width="0.127" layer="21"/>
<wire x1="7.89" y1="2.5" x2="7.9" y2="-5.25" width="0.127" layer="21"/>
<wire x1="-7.91" y1="3.25" x2="7.89" y2="3.25" width="0.127" layer="21"/>
<wire x1="-7.91" y1="-5.25" x2="7.9" y2="-5.25" width="0.127" layer="21"/>
<wire x1="-7.91" y1="2.5" x2="7.89" y2="2.5" width="0.127" layer="21"/>
</package>
<package name="JST_B4P-VH-FB-B" urn="urn:adsk.eagle:footprint:8413984/1" library_version="47">
<pad name="P$1" x="-5.94" y="0" drill="1.65"/>
<pad name="P$2" x="-1.98" y="0" drill="1.65"/>
<pad name="P$3" x="1.98" y="0" drill="1.65"/>
<pad name="P$4" x="5.94" y="0" drill="1.65"/>
<hole x="-7.44" y="-3.4" drill="1.4"/>
<wire x1="-8.86" y1="4.85" x2="8.86" y2="4.85" width="0.127" layer="21"/>
<wire x1="-8.86" y1="-4.85" x2="8.86" y2="-4.85" width="0.127" layer="21"/>
<wire x1="-8.86" y1="4.85" x2="-8.86" y2="-4.85" width="0.127" layer="21"/>
<wire x1="8.86" y1="4.85" x2="8.86" y2="-4.85" width="0.127" layer="21"/>
<wire x1="-8.86" y1="4.85" x2="8.86" y2="4.85" width="0.127" layer="51"/>
<wire x1="-8.86" y1="-4.85" x2="8.86" y2="-4.85" width="0.127" layer="51"/>
<wire x1="-8.86" y1="4.85" x2="-8.86" y2="-4.85" width="0.127" layer="51"/>
<wire x1="8.86" y1="4.85" x2="8.86" y2="-4.85" width="0.127" layer="51"/>
<text x="-8.96" y="5" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.96" y="5" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="JST_B04B-ZESK-1D" urn="urn:adsk.eagle:footprint:8414007/1" library_version="47">
<pad name="P$1" x="2.25" y="1.98" drill="0.7" rot="R180"/>
<pad name="P$2" x="0.75" y="-0.02" drill="0.7" rot="R180"/>
<pad name="P$3" x="-0.75" y="1.98" drill="0.7" rot="R180"/>
<pad name="P$4" x="-2.25" y="-0.02" drill="0.7" rot="R180"/>
<hole x="3.9" y="-1.82" drill="0.8"/>
<wire x1="-4.505" y1="3.115" x2="-4.505" y2="-3.105" width="0.127" layer="51"/>
<wire x1="4.505" y1="3.115" x2="4.505" y2="-3.105" width="0.127" layer="51"/>
<wire x1="-4.505" y1="3.115" x2="4.505" y2="3.115" width="0.127" layer="51"/>
<wire x1="-4.505" y1="-3.105" x2="4.505" y2="-3.105" width="0.127" layer="51"/>
<wire x1="-4.505" y1="3.115" x2="-4.505" y2="-3.105" width="0.127" layer="21"/>
<wire x1="4.505" y1="3.115" x2="4.505" y2="-3.105" width="0.127" layer="21"/>
<wire x1="-4.505" y1="3.115" x2="4.505" y2="3.115" width="0.127" layer="21"/>
<wire x1="-4.505" y1="-3.105" x2="4.505" y2="-3.105" width="0.127" layer="21"/>
<text x="-1.5" y="3.48" size="1.27" layer="25">&gt;NAME</text>
<text x="-9.75" y="3.48" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="JST_S04B-ZESK-2D" urn="urn:adsk.eagle:footprint:8414008/1" library_version="47">
<pad name="P$1" x="2.25" y="-7.5" drill="0.7"/>
<pad name="P$2" x="0.75" y="-3.8" drill="0.7"/>
<pad name="P$3" x="-0.75" y="-7.5" drill="0.7"/>
<pad name="P$4" x="-2.25" y="-3.8" drill="0.7"/>
<hole x="3.8" y="-5.65" drill="1.1"/>
<hole x="-3.8" y="-5.65" drill="1.1"/>
<wire x1="4.5" y1="7.22" x2="4.5" y2="6.47" width="0.127" layer="21"/>
<wire x1="4.5" y1="6.47" x2="4.5" y2="-8.38" width="0.127" layer="21"/>
<wire x1="-4.5" y1="7.22" x2="-4.5" y2="6.47" width="0.127" layer="21"/>
<wire x1="-4.5" y1="6.47" x2="-4.5" y2="-8.38" width="0.127" layer="21"/>
<wire x1="-4.5" y1="-8.38" x2="4.5" y2="-8.38" width="0.127" layer="21"/>
<wire x1="-4.5" y1="7.22" x2="4.5" y2="7.22" width="0.127" layer="21"/>
<wire x1="4.5" y1="7.22" x2="4.5" y2="6.47" width="0.127" layer="51"/>
<wire x1="4.5" y1="6.47" x2="4.5" y2="-8.38" width="0.127" layer="51"/>
<wire x1="-4.5" y1="7.22" x2="-4.5" y2="6.47" width="0.127" layer="51"/>
<wire x1="-4.5" y1="6.47" x2="-4.5" y2="-8.38" width="0.127" layer="51"/>
<wire x1="-4.5" y1="-8.38" x2="4.5" y2="-8.38" width="0.127" layer="51"/>
<wire x1="-4.5" y1="7.22" x2="4.5" y2="7.22" width="0.127" layer="51"/>
<wire x1="-4.5" y1="6.47" x2="4.5" y2="6.47" width="0.127" layer="21"/>
<wire x1="-4.5" y1="6.47" x2="4.5" y2="6.47" width="0.127" layer="51"/>
<text x="-1.2" y="-10" size="1.27" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="-9.45" y="-10" size="1.27" layer="27" font="vector" ratio="15">&gt;VALUE</text>
</package>
<package name="TO457P1000X238-3" urn="urn:adsk.eagle:footprint:3920003/3" library_version="76">
<description>3-TO, DPAK, 4.57 mm pitch, 10 mm span, 6.6 X 6.1 X 2.38 mm body
&lt;p&gt;3-pin TO, DPAK package with 4.57 mm pitch, 10 mm span with body size 6.6 X 6.1 X 2.38 mm&lt;/p&gt;</description>
<circle x="-4.75" y="3.3836" radius="0.25" width="0" layer="21"/>
<wire x1="6.221" y1="3.0207" x2="6.221" y2="3.365" width="0.12" layer="21"/>
<wire x1="6.221" y1="3.365" x2="-1.015" y2="3.365" width="0.12" layer="21"/>
<wire x1="-1.015" y1="3.365" x2="-1.015" y2="-3.365" width="0.12" layer="21"/>
<wire x1="-1.015" y1="-3.365" x2="6.221" y2="-3.365" width="0.12" layer="21"/>
<wire x1="6.221" y1="-3.365" x2="6.221" y2="-3.0207" width="0.12" layer="21"/>
<wire x1="6.221" y1="-3.365" x2="-1.015" y2="-3.365" width="0.12" layer="51"/>
<wire x1="-1.015" y1="-3.365" x2="-1.015" y2="3.365" width="0.12" layer="51"/>
<wire x1="-1.015" y1="3.365" x2="6.221" y2="3.365" width="0.12" layer="51"/>
<wire x1="6.221" y1="3.365" x2="6.221" y2="-3.365" width="0.12" layer="51"/>
<smd name="1" x="-4.75" y="2.285" dx="1.6078" dy="1.1891" layer="1"/>
<smd name="2" x="-4.75" y="-2.285" dx="1.6078" dy="1.1891" layer="1"/>
<smd name="3" x="2.545" y="0" dx="6.0178" dy="5.5334" layer="1"/>
<text x="0" y="4.2686" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-4" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="POWERPAK_SO-8" urn="urn:adsk.eagle:footprint:8413717/1" library_version="40">
<smd name="1" x="-2.67" y="1.905" dx="1.27" dy="0.61" layer="1"/>
<smd name="2" x="-2.67" y="0.635" dx="1.27" dy="0.61" layer="1"/>
<smd name="3" x="-2.67" y="-0.635" dx="1.27" dy="0.61" layer="1"/>
<smd name="4" x="-2.67" y="-1.905" dx="1.27" dy="0.61" layer="1"/>
<smd name="5" x="2.795" y="-1.905" dx="1.02" dy="0.61" layer="1"/>
<smd name="6" x="2.795" y="-0.635" dx="1.02" dy="0.61" layer="1"/>
<smd name="7" x="2.795" y="0.635" dx="1.02" dy="0.61" layer="1"/>
<smd name="8" x="2.795" y="1.905" dx="1.02" dy="0.61" layer="1"/>
<smd name="9" x="0.69" y="0" dx="3.81" dy="3.91" layer="1"/>
<wire x1="-3.075" y1="2.575" x2="3.075" y2="2.575" width="0.127" layer="21"/>
<wire x1="-3.075" y1="-2.575" x2="3.075" y2="-2.575" width="0.127" layer="21"/>
<wire x1="-3.075" y1="2.575" x2="3.075" y2="2.575" width="0.127" layer="51"/>
<wire x1="-3.075" y1="-2.575" x2="3.075" y2="-2.575" width="0.127" layer="51"/>
<text x="-3.81" y="3.175" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.81" y="-4.445" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="DIOC5226X110N" urn="urn:adsk.eagle:footprint:16113287/2" library_version="70">
<description>Chip, 5.20 X 2.60 X 1.10 mm body
&lt;p&gt;Chip package with body size 5.20 X 2.60 X 1.10 mm&lt;/p&gt;</description>
<wire x1="2.8" y1="1.7934" x2="-3.4079" y2="1.7934" width="0.12" layer="21"/>
<wire x1="-3.4079" y1="1.7934" x2="-3.4079" y2="-1.7934" width="0.12" layer="21"/>
<wire x1="-3.4079" y1="-1.7934" x2="2.8" y2="-1.7934" width="0.12" layer="21"/>
<wire x1="2.8" y1="-1.475" x2="-2.8" y2="-1.475" width="0.12" layer="51"/>
<wire x1="-2.8" y1="-1.475" x2="-2.8" y2="1.475" width="0.12" layer="51"/>
<wire x1="-2.8" y1="1.475" x2="2.8" y2="1.475" width="0.12" layer="51"/>
<wire x1="2.8" y1="1.475" x2="2.8" y2="-1.475" width="0.12" layer="51"/>
<wire x1="3.4544" y1="1.4986" x2="3.4544" y2="-1.4986" width="0.1524" layer="21"/>
<smd name="1" x="-2.1729" y="0" dx="1.962" dy="2.9589" layer="1"/>
<smd name="2" x="2.1729" y="0" dx="1.962" dy="2.9589" layer="1"/>
<text x="0" y="2.4284" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.4284" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="SOP50P490X110-10" urn="urn:adsk.eagle:footprint:3921299/1" library_version="58">
<description>10-SOP, 0.5 mm pitch, 4.9 mm span, 3 X 3 X 1.1 mm body
&lt;p&gt;10-pin SOP package with 0.5 mm pitch, 4.9 mm span with body size 3 X 3 X 1.1 mm&lt;/p&gt;</description>
<circle x="-2.2175" y="1.659" radius="0.25" width="0" layer="21"/>
<wire x1="-1.55" y1="1.409" x2="-1.55" y2="1.55" width="0.12" layer="21"/>
<wire x1="-1.55" y1="1.55" x2="1.55" y2="1.55" width="0.12" layer="21"/>
<wire x1="1.55" y1="1.55" x2="1.55" y2="1.409" width="0.12" layer="21"/>
<wire x1="-1.55" y1="-1.409" x2="-1.55" y2="-1.55" width="0.12" layer="21"/>
<wire x1="-1.55" y1="-1.55" x2="1.55" y2="-1.55" width="0.12" layer="21"/>
<wire x1="1.55" y1="-1.55" x2="1.55" y2="-1.409" width="0.12" layer="21"/>
<wire x1="1.55" y1="-1.55" x2="-1.55" y2="-1.55" width="0.12" layer="51"/>
<wire x1="-1.55" y1="-1.55" x2="-1.55" y2="1.55" width="0.12" layer="51"/>
<wire x1="-1.55" y1="1.55" x2="1.55" y2="1.55" width="0.12" layer="51"/>
<wire x1="1.55" y1="1.55" x2="1.55" y2="-1.55" width="0.12" layer="51"/>
<smd name="1" x="-2.1496" y="1" dx="1.4709" dy="0.31" layer="1"/>
<smd name="2" x="-2.1496" y="0.5" dx="1.4709" dy="0.31" layer="1"/>
<smd name="3" x="-2.1496" y="0" dx="1.4709" dy="0.31" layer="1"/>
<smd name="4" x="-2.1496" y="-0.5" dx="1.4709" dy="0.31" layer="1"/>
<smd name="5" x="-2.1496" y="-1" dx="1.4709" dy="0.31" layer="1"/>
<smd name="6" x="2.1496" y="-1" dx="1.4709" dy="0.31" layer="1"/>
<smd name="7" x="2.1496" y="-0.5" dx="1.4709" dy="0.31" layer="1"/>
<smd name="8" x="2.1496" y="0" dx="1.4709" dy="0.31" layer="1"/>
<smd name="9" x="2.1496" y="0.5" dx="1.4709" dy="0.31" layer="1"/>
<smd name="10" x="2.1496" y="1" dx="1.4709" dy="0.31" layer="1"/>
<text x="0" y="2.544" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.185" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="RESC3115X70" urn="urn:adsk.eagle:footprint:3811174/1" library_version="58">
<description>CHIP, 3.125 X 1.55 X 0.7 mm body
&lt;p&gt;CHIP package with body size 3.125 X 1.55 X 0.7 mm&lt;/p&gt;</description>
<wire x1="1.625" y1="1.1536" x2="-1.625" y2="1.1536" width="0.12" layer="21"/>
<wire x1="1.625" y1="-1.1536" x2="-1.625" y2="-1.1536" width="0.12" layer="21"/>
<wire x1="1.625" y1="-0.825" x2="-1.625" y2="-0.825" width="0.12" layer="51"/>
<wire x1="-1.625" y1="-0.825" x2="-1.625" y2="0.825" width="0.12" layer="51"/>
<wire x1="-1.625" y1="0.825" x2="1.625" y2="0.825" width="0.12" layer="51"/>
<wire x1="1.625" y1="0.825" x2="1.625" y2="-0.825" width="0.12" layer="51"/>
<smd name="1" x="-1.4449" y="0" dx="1.0841" dy="1.6791" layer="1"/>
<smd name="2" x="1.4449" y="0" dx="1.0841" dy="1.6791" layer="1"/>
<text x="0" y="1.7886" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.7886" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="JST_B8B-PHDSS" urn="urn:adsk.eagle:footprint:8413718/1" library_version="52">
<pad name="1" x="3" y="-1" drill="0.7"/>
<pad name="2" x="3" y="1" drill="0.7"/>
<pad name="3" x="1" y="-1" drill="0.7"/>
<pad name="4" x="1" y="1" drill="0.7"/>
<pad name="5" x="-1" y="-1" drill="0.7"/>
<pad name="6" x="-1" y="1" drill="0.7"/>
<pad name="7" x="-3" y="-1" drill="0.7"/>
<pad name="8" x="-3" y="1" drill="0.7"/>
<wire x1="-5.5" y1="3.5" x2="5.5" y2="3.5" width="0.127" layer="21"/>
<wire x1="5.5" y1="3.5" x2="5.5" y2="-3.5" width="0.127" layer="21"/>
<wire x1="5.5" y1="-3.5" x2="-5.5" y2="-3.5" width="0.127" layer="21"/>
<wire x1="-5.5" y1="-3.5" x2="-5.5" y2="3.5" width="0.127" layer="21"/>
<wire x1="-5.5" y1="3.5" x2="5.5" y2="3.5" width="0.127" layer="51"/>
<wire x1="5.5" y1="3.5" x2="5.5" y2="-3.5" width="0.127" layer="51"/>
<wire x1="5.5" y1="-3.5" x2="-5.5" y2="-3.5" width="0.127" layer="51"/>
<wire x1="-5.5" y1="-3.5" x2="-5.5" y2="3.5" width="0.127" layer="51"/>
<wire x1="4" y1="-4" x2="6" y2="-4" width="0.127" layer="51"/>
<wire x1="6" y1="-4" x2="6" y2="-2" width="0.127" layer="51"/>
<wire x1="4" y1="-4" x2="6" y2="-4" width="0.127" layer="21"/>
<wire x1="6" y1="-4" x2="6" y2="-2" width="0.127" layer="21"/>
<text x="6.35" y="2.54" size="0.8128" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="6.35" y="1.27" size="0.8128" layer="27" font="vector" ratio="15">&gt;VALUE</text>
</package>
<package name="JST_8P-HVQ" urn="urn:adsk.eagle:footprint:8413742/1" library_version="52">
<pad name="1" x="8.75" y="-0.7" drill="1"/>
<pad name="2" x="6.25" y="-0.7" drill="1"/>
<pad name="3" x="3.75" y="-0.7" drill="1"/>
<pad name="4" x="1.25" y="-0.7" drill="1"/>
<pad name="5" x="-1.25" y="-0.7" drill="1"/>
<pad name="6" x="-3.75" y="-0.7" drill="1"/>
<pad name="7" x="-6.25" y="-0.7" drill="1"/>
<pad name="8" x="-8.75" y="-0.7" drill="1"/>
<wire x1="-12.25" y1="-5.3" x2="-12.25" y2="2.8" width="0.127" layer="51"/>
<wire x1="12.25" y1="-5.3" x2="12.25" y2="2.8" width="0.127" layer="51"/>
<wire x1="-12.25" y1="2.8" x2="-10.65" y2="2.8" width="0.127" layer="51"/>
<wire x1="-10.65" y1="2.8" x2="10.65" y2="2.8" width="0.127" layer="51"/>
<wire x1="10.65" y1="2.8" x2="12.25" y2="2.8" width="0.127" layer="51"/>
<wire x1="-10.75" y1="-1.9" x2="10.75" y2="-1.9" width="0.127" layer="51"/>
<wire x1="-12.25" y1="-5.3" x2="-10.75" y2="-5.3" width="0.127" layer="51"/>
<wire x1="10.75" y1="-5.3" x2="12.25" y2="-5.3" width="0.127" layer="51"/>
<wire x1="-10.75" y1="-1.9" x2="-10.75" y2="-5.3" width="0.127" layer="51"/>
<wire x1="10.75" y1="-1.9" x2="10.75" y2="-5.3" width="0.127" layer="51"/>
<wire x1="-12.25" y1="-1.9" x2="-12.25" y2="2.8" width="0.127" layer="21"/>
<wire x1="12.25" y1="-1.9" x2="12.25" y2="2.8" width="0.127" layer="21"/>
<wire x1="-12.25" y1="2.8" x2="12.25" y2="2.8" width="0.127" layer="21"/>
<wire x1="-12.25" y1="-1.9" x2="12.25" y2="-1.9" width="0.127" layer="21"/>
<wire x1="-10.65" y1="4.5" x2="10.65" y2="4.5" width="0.127" layer="51"/>
<wire x1="-10.65" y1="2.8" x2="-10.65" y2="4.5" width="0.127" layer="51"/>
<wire x1="10.65" y1="2.8" x2="10.65" y2="4.5" width="0.127" layer="51"/>
<text x="4.45" y="-5.2" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.75" y="-5.2" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="JST_B8P-SHF-1AA" urn="urn:adsk.eagle:footprint:8413743/1" library_version="52">
<pad name="1" x="8.75" y="-0.7" drill="0.9"/>
<pad name="2" x="6.25" y="-0.7" drill="0.9"/>
<pad name="3" x="3.75" y="-0.7" drill="0.9"/>
<pad name="4" x="1.25" y="-0.7" drill="0.9"/>
<pad name="5" x="-1.25" y="-0.7" drill="0.9"/>
<pad name="6" x="-3.75" y="-0.7" drill="0.9"/>
<pad name="7" x="-6.25" y="-0.7" drill="0.9"/>
<pad name="8" x="-8.75" y="-0.7" drill="0.9"/>
<wire x1="-10.25" y1="2.8" x2="10.25" y2="2.8" width="0.127" layer="21"/>
<wire x1="-10.25" y1="-2.8" x2="10.25" y2="-2.8" width="0.127" layer="21"/>
<wire x1="-10.25" y1="-2.8" x2="-10.25" y2="-2.1" width="0.127" layer="21"/>
<wire x1="-10.25" y1="-2.1" x2="-10.25" y2="2.8" width="0.127" layer="21"/>
<wire x1="10.25" y1="-2.8" x2="10.25" y2="-2.1" width="0.127" layer="21"/>
<wire x1="10.25" y1="-2.1" x2="10.25" y2="2.8" width="0.127" layer="21"/>
<wire x1="-10.25" y1="-2.1" x2="10.25" y2="-2.1" width="0.127" layer="21"/>
<wire x1="-10.25" y1="2.8" x2="10.25" y2="2.8" width="0.127" layer="51"/>
<wire x1="-10.25" y1="-2.8" x2="10.25" y2="-2.8" width="0.127" layer="51"/>
<wire x1="-10.25" y1="-2.8" x2="-10.25" y2="-2.1" width="0.127" layer="51"/>
<wire x1="-10.25" y1="-2.1" x2="-10.25" y2="2.8" width="0.127" layer="51"/>
<wire x1="10.25" y1="-2.8" x2="10.25" y2="-2.1" width="0.127" layer="51"/>
<wire x1="10.25" y1="-2.1" x2="10.25" y2="2.8" width="0.127" layer="51"/>
<wire x1="-10.25" y1="-2.1" x2="10.25" y2="-2.1" width="0.127" layer="51"/>
<text x="4.45" y="-4.3" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.75" y="-4.3" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="JST_BS8P-SHF-1AA" urn="urn:adsk.eagle:footprint:8413744/1" library_version="52">
<pad name="1" x="8.75" y="-1.27" drill="0.9"/>
<pad name="2" x="6.25" y="-1.27" drill="0.9"/>
<pad name="3" x="3.75" y="-1.27" drill="0.9"/>
<pad name="4" x="1.25" y="-1.27" drill="0.9"/>
<pad name="5" x="-1.25" y="-1.27" drill="0.9"/>
<pad name="6" x="-3.75" y="-1.27" drill="0.9"/>
<pad name="7" x="-6.25" y="-1.27" drill="0.9"/>
<pad name="8" x="-8.75" y="-1.27" drill="0.9"/>
<text x="4.45" y="-5.77" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.75" y="-5.77" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-10.21" y1="1.73" x2="10.25" y2="1.73" width="0.127" layer="21"/>
<wire x1="-10.21" y1="-4.27" x2="10.25" y2="-4.27" width="0.127" layer="21"/>
<wire x1="-10.21" y1="1.73" x2="-10.21" y2="-4.27" width="0.127" layer="21"/>
<wire x1="10.25" y1="1.73" x2="10.25" y2="-4.27" width="0.127" layer="21"/>
<wire x1="-10.21" y1="1.73" x2="10.25" y2="1.73" width="0.127" layer="51"/>
<wire x1="-10.21" y1="-4.27" x2="10.25" y2="-4.27" width="0.127" layer="51"/>
<wire x1="-10.21" y1="1.73" x2="-10.21" y2="-4.27" width="0.127" layer="51"/>
<wire x1="10.25" y1="1.73" x2="10.25" y2="-4.27" width="0.127" layer="51"/>
</package>
<package name="JST_F8P-HVQ" urn="urn:adsk.eagle:footprint:8413745/1" library_version="52">
<pad name="1" x="8.75" y="-0.7" drill="1"/>
<pad name="2" x="6.25" y="-0.7" drill="1"/>
<pad name="3" x="3.75" y="-0.7" drill="1"/>
<pad name="4" x="1.25" y="-0.7" drill="1"/>
<pad name="5" x="-1.25" y="-0.7" drill="1"/>
<pad name="6" x="-3.75" y="-0.7" drill="1"/>
<pad name="7" x="-6.25" y="-0.7" drill="1"/>
<pad name="8" x="-8.75" y="-0.7" drill="1"/>
<wire x1="-12.25" y1="-5.3" x2="-12.25" y2="2.8" width="0.127" layer="51"/>
<wire x1="12.25" y1="-5.3" x2="12.25" y2="2.8" width="0.127" layer="51"/>
<wire x1="-12.25" y1="2.8" x2="12.25" y2="2.8" width="0.127" layer="51"/>
<wire x1="-10.75" y1="-1.9" x2="10.75" y2="-1.9" width="0.127" layer="51"/>
<wire x1="-12.25" y1="-5.3" x2="-10.75" y2="-5.3" width="0.127" layer="51"/>
<wire x1="10.75" y1="-5.3" x2="12.25" y2="-5.3" width="0.127" layer="51"/>
<wire x1="-10.75" y1="-1.9" x2="-10.75" y2="-5.3" width="0.127" layer="51"/>
<wire x1="10.75" y1="-1.9" x2="10.75" y2="-5.3" width="0.127" layer="51"/>
<wire x1="-12.25" y1="-1.9" x2="-12.25" y2="2.8" width="0.127" layer="21"/>
<wire x1="12.25" y1="-1.9" x2="12.25" y2="2.8" width="0.127" layer="21"/>
<wire x1="-12.25" y1="2.8" x2="12.25" y2="2.8" width="0.127" layer="21"/>
<wire x1="-12.25" y1="-1.9" x2="12.25" y2="-1.9" width="0.127" layer="21"/>
<text x="4.45" y="-5.2" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.75" y="-5.2" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="JST_F8P-SHVQ" urn="urn:adsk.eagle:footprint:8413746/1" library_version="52">
<pad name="1" x="8.82" y="-3.35" drill="1"/>
<pad name="2" x="6.32" y="-3.35" drill="1"/>
<pad name="3" x="3.82" y="-3.35" drill="1"/>
<pad name="4" x="1.32" y="-3.35" drill="1"/>
<pad name="5" x="-1.18" y="-3.35" drill="1"/>
<pad name="6" x="-3.68" y="-3.35" drill="1"/>
<pad name="7" x="-6.18" y="-3.35" drill="1"/>
<pad name="8" x="-8.68" y="-3.35" drill="1"/>
<text x="6.3" y="-5.95" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.2" y="-5.95" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-12.2" y1="3.05" x2="12.3" y2="3.05" width="0.127" layer="21"/>
<wire x1="-12.2" y1="-4.45" x2="-12.2" y2="3.05" width="0.127" layer="21"/>
<wire x1="12.3" y1="-4.45" x2="12.3" y2="3.05" width="0.127" layer="21"/>
<wire x1="12.3" y1="-4.45" x2="-12.2" y2="-4.45" width="0.127" layer="21"/>
<wire x1="-10.7" y1="3.05" x2="10.8" y2="3.05" width="0.127" layer="51"/>
<wire x1="-12.2" y1="-4.45" x2="-12.2" y2="4.85" width="0.127" layer="51"/>
<wire x1="12.3" y1="-4.45" x2="12.3" y2="4.85" width="0.127" layer="51"/>
<wire x1="12.3" y1="-4.45" x2="-12.2" y2="-4.45" width="0.127" layer="51"/>
<wire x1="-12.2" y1="4.85" x2="-10.7" y2="4.85" width="0.127" layer="51"/>
<wire x1="10.8" y1="4.85" x2="12.3" y2="4.85" width="0.127" layer="51"/>
<wire x1="-10.7" y1="4.85" x2="-10.7" y2="3.05" width="0.127" layer="51"/>
<wire x1="10.8" y1="4.85" x2="10.8" y2="3.05" width="0.127" layer="51"/>
</package>
<package name="JST_08JQ-BT" urn="urn:adsk.eagle:footprint:8413867/1" library_version="52">
<pad name="1" x="8.75" y="0" drill="0.9"/>
<pad name="2" x="6.25" y="0" drill="0.9"/>
<pad name="3" x="3.75" y="0" drill="0.9"/>
<pad name="4" x="1.25" y="0" drill="0.9"/>
<pad name="5" x="-1.25" y="0" drill="0.9"/>
<pad name="6" x="-3.75" y="0" drill="0.9"/>
<pad name="7" x="-6.25" y="0" drill="0.9"/>
<pad name="8" x="-8.75" y="0" drill="0.9"/>
<wire x1="-11.25" y1="2.7" x2="11.25" y2="2.7" width="0.127" layer="21"/>
<wire x1="11.25" y1="-3.3" x2="-11.25" y2="-3.3" width="0.127" layer="21"/>
<wire x1="-11.25" y1="2.7" x2="-11.25" y2="-3.3" width="0.127" layer="21"/>
<wire x1="11.25" y1="2.7" x2="11.25" y2="-3.3" width="0.127" layer="21"/>
<wire x1="-11.25" y1="2.7" x2="11.25" y2="2.7" width="0.127" layer="51"/>
<wire x1="11.25" y1="-3.3" x2="-11.25" y2="-3.3" width="0.127" layer="51"/>
<wire x1="-11.25" y1="2.7" x2="-11.25" y2="-3.3" width="0.127" layer="51"/>
<wire x1="11.25" y1="2.7" x2="11.25" y2="-3.3" width="0.127" layer="51"/>
<text x="5.5" y="2.93" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.75" y="2.93" size="1.27" layer="27">&gt;VALUE</text>
<circle x="8.75" y="-3.85" radius="0.25" width="0.127" layer="21"/>
<circle x="8.75" y="-3.85" radius="0.13" width="0.127" layer="21"/>
<circle x="8.75" y="-3.85" radius="0.014140625" width="0.127" layer="21"/>
<circle x="8.75" y="-3.85" radius="0.25" width="0.127" layer="51"/>
<circle x="8.75" y="-3.85" radius="0.13" width="0.127" layer="51"/>
<circle x="8.75" y="-3.85" radius="0.014140625" width="0.127" layer="51"/>
</package>
<package name="JST_08JQ-ST" urn="urn:adsk.eagle:footprint:8413868/1" library_version="52">
<pad name="1" x="8.75" y="0" drill="0.9"/>
<pad name="2" x="6.25" y="0" drill="0.9"/>
<pad name="3" x="3.75" y="0" drill="0.9"/>
<pad name="4" x="1.25" y="0" drill="0.9"/>
<pad name="5" x="-1.25" y="0" drill="0.9"/>
<pad name="6" x="-3.75" y="0" drill="0.9"/>
<pad name="7" x="-6.25" y="0" drill="0.9"/>
<pad name="8" x="-8.75" y="0" drill="0.9"/>
<wire x1="-11.25" y1="-2.2" x2="11.25" y2="-2.2" width="0.127" layer="51"/>
<wire x1="-11.25" y1="2.6" x2="-10.35" y2="2.6" width="0.127" layer="51"/>
<wire x1="-10.35" y1="2.6" x2="10.35" y2="2.6" width="0.127" layer="51"/>
<wire x1="10.35" y1="2.6" x2="11.25" y2="2.6" width="0.127" layer="51"/>
<wire x1="11.25" y1="-2.2" x2="11.25" y2="2.6" width="0.127" layer="51"/>
<wire x1="-11.25" y1="-2.2" x2="-11.25" y2="2.6" width="0.127" layer="51"/>
<wire x1="-11.25" y1="-2.2" x2="11.25" y2="-2.2" width="0.127" layer="21"/>
<wire x1="-11.25" y1="2.6" x2="11.25" y2="2.6" width="0.127" layer="21"/>
<wire x1="11.25" y1="-2.2" x2="11.25" y2="2.6" width="0.127" layer="21"/>
<wire x1="-11.25" y1="-2.2" x2="-11.25" y2="2.6" width="0.127" layer="21"/>
<wire x1="-10.35" y1="2.6" x2="-10.35" y2="7.3" width="0.127" layer="51"/>
<wire x1="10.35" y1="2.6" x2="10.35" y2="7.3" width="0.127" layer="51"/>
<wire x1="-10.35" y1="7.3" x2="10.35" y2="7.3" width="0.127" layer="51"/>
<text x="0.85" y="-3.82" size="1.27" layer="25">&gt;NAME</text>
<text x="-7.4" y="-3.82" size="1.27" layer="27">&gt;VALUE</text>
<circle x="8.75" y="-2.75" radius="0.25" width="0.127" layer="21"/>
<circle x="8.75" y="-2.75" radius="0.13" width="0.127" layer="21"/>
<circle x="8.75" y="-2.75" radius="0.014140625" width="0.127" layer="21"/>
<circle x="8.75" y="-2.75" radius="0.25" width="0.127" layer="51"/>
<circle x="8.75" y="-2.75" radius="0.13" width="0.127" layer="51"/>
<circle x="8.75" y="-2.75" radius="0.014140625" width="0.127" layer="51"/>
</package>
<package name="JST_B8B-XH-A" urn="urn:adsk.eagle:footprint:8413869/1" library_version="52">
<pad name="1" x="8.75" y="0" drill="0.9"/>
<pad name="2" x="6.25" y="0" drill="0.9"/>
<pad name="3" x="3.75" y="0" drill="0.9"/>
<pad name="4" x="1.25" y="0" drill="0.9"/>
<pad name="5" x="-1.25" y="0" drill="0.9"/>
<pad name="6" x="-3.75" y="0" drill="0.9"/>
<pad name="7" x="-6.25" y="0" drill="0.9"/>
<pad name="8" x="-8.75" y="0" drill="0.9"/>
<wire x1="-11.2" y1="2.35" x2="11.2" y2="2.35" width="0.127" layer="51"/>
<wire x1="-11.2" y1="-3.4" x2="11.2" y2="-3.4" width="0.127" layer="51"/>
<wire x1="-11.2" y1="2.35" x2="-11.2" y2="-3.4" width="0.127" layer="51"/>
<wire x1="11.2" y1="2.35" x2="11.2" y2="-3.4" width="0.127" layer="51"/>
<wire x1="-11.2" y1="2.35" x2="11.2" y2="2.35" width="0.127" layer="21"/>
<wire x1="-11.2" y1="-3.4" x2="11.2" y2="-3.4" width="0.127" layer="21"/>
<wire x1="-11.2" y1="2.35" x2="-11.2" y2="-3.4" width="0.127" layer="21"/>
<wire x1="11.2" y1="2.35" x2="11.2" y2="-3.4" width="0.127" layer="21"/>
<text x="2.27" y="-4.94" size="1.27" layer="25">&gt;NAME</text>
<text x="-4.98" y="-4.94" size="1.27" layer="27">&gt;VALUE</text>
<circle x="8.75" y="-4" radius="0.25" width="0.127" layer="21"/>
<circle x="8.75" y="-4" radius="0.13" width="0.127" layer="21"/>
<circle x="8.75" y="-4" radius="0.014140625" width="0.127" layer="21"/>
<circle x="8.75" y="-4" radius="0.25" width="0.127" layer="51"/>
<circle x="8.75" y="-4" radius="0.13" width="0.127" layer="51"/>
<circle x="8.75" y="-4" radius="0.014140625" width="0.127" layer="51"/>
</package>
<package name="JST_S8B-XH-A-1" urn="urn:adsk.eagle:footprint:8413870/1" library_version="52">
<pad name="1" x="8.75" y="0" drill="0.9"/>
<pad name="2" x="6.25" y="0" drill="0.9"/>
<pad name="3" x="3.75" y="0" drill="0.9"/>
<pad name="4" x="1.25" y="0" drill="0.9"/>
<pad name="5" x="-1.25" y="0" drill="0.9"/>
<pad name="6" x="-3.75" y="0" drill="0.9"/>
<pad name="7" x="-6.25" y="0" drill="0.9"/>
<pad name="8" x="-8.75" y="0" drill="0.9"/>
<wire x1="-11.2" y1="7.6" x2="11.2" y2="7.6" width="0.127" layer="21"/>
<wire x1="-11.2" y1="-3.9" x2="11.2" y2="-3.9" width="0.127" layer="21"/>
<wire x1="-11.2" y1="-3.9" x2="-11.2" y2="7.6" width="0.127" layer="21"/>
<wire x1="11.2" y1="-3.9" x2="11.2" y2="7.6" width="0.127" layer="21"/>
<wire x1="-11.2" y1="7.6" x2="11.2" y2="7.6" width="0.127" layer="51"/>
<wire x1="-11.2" y1="-3.9" x2="11.2" y2="-3.9" width="0.127" layer="51"/>
<wire x1="-11.2" y1="-3.9" x2="-11.2" y2="7.6" width="0.127" layer="51"/>
<wire x1="11.2" y1="-3.9" x2="11.2" y2="7.6" width="0.127" layer="51"/>
<text x="5.2" y="-5.395" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.3" y="-5.395" size="1.27" layer="27">&gt;VALUE</text>
<circle x="8.75" y="-2.125" radius="0.25" width="0.127" layer="21"/>
<circle x="8.75" y="-2.125" radius="0.13" width="0.127" layer="21"/>
<circle x="8.75" y="-2.125" radius="0.014140625" width="0.127" layer="21"/>
<circle x="8.75" y="-2.125" radius="0.25" width="0.127" layer="51"/>
<circle x="8.75" y="-2.125" radius="0.13" width="0.127" layer="51"/>
<circle x="8.75" y="-2.125" radius="0.014140625" width="0.127" layer="51"/>
</package>
<package name="JST_B08B-PASK" urn="urn:adsk.eagle:footprint:8413909/1" library_version="52">
<pad name="P$1" x="7" y="0" drill="0.7" rot="R90"/>
<pad name="P$2" x="5" y="0" drill="0.7" rot="R90"/>
<pad name="P$3" x="3" y="0" drill="0.7" rot="R90"/>
<pad name="P$4" x="1" y="0" drill="0.7" rot="R90"/>
<pad name="P$5" x="-1" y="0" drill="0.7" rot="R90"/>
<pad name="P$6" x="-3" y="0" drill="0.7" rot="R90"/>
<pad name="P$7" x="-5" y="0" drill="0.7" rot="R90"/>
<pad name="P$8" x="-7" y="0" drill="0.7" rot="R90"/>
<hole x="8.5" y="1.7" drill="1.1"/>
<wire x1="-9" y1="-3.05" x2="9" y2="-3.05" width="0.127" layer="21"/>
<wire x1="-9" y1="2.25" x2="8.03" y2="2.25" width="0.127" layer="21"/>
<wire x1="-9" y1="2.25" x2="-9" y2="-3.05" width="0.127" layer="21"/>
<wire x1="9" y1="-3.05" x2="9" y2="1.17" width="0.127" layer="21"/>
<wire x1="-9" y1="-3.05" x2="9" y2="-3.05" width="0.127" layer="51"/>
<wire x1="-9" y1="2.25" x2="8.03" y2="2.25" width="0.127" layer="51"/>
<wire x1="-9" y1="2.25" x2="-9" y2="-3.05" width="0.127" layer="51"/>
<wire x1="9" y1="-3.05" x2="9" y2="1.17" width="0.127" layer="51"/>
<text x="1.5" y="2.48" size="1.27" layer="25">&gt;NAME</text>
<text x="-6.75" y="2.48" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="JST_S08B-PASK-2" urn="urn:adsk.eagle:footprint:8413910/1" library_version="52">
<pad name="1" x="7" y="-2.35" drill="0.7"/>
<pad name="2" x="5" y="-2.35" drill="0.7"/>
<pad name="3" x="3" y="-2.35" drill="0.7"/>
<pad name="4" x="1" y="-2.35" drill="0.7"/>
<pad name="5" x="-1" y="-2.35" drill="0.7"/>
<pad name="6" x="-3" y="-2.35" drill="0.7"/>
<pad name="7" x="-5" y="-2.35" drill="0.7"/>
<pad name="8" x="-7" y="-2.35" drill="0.7"/>
<hole x="8.5" y="-0.25" drill="1.1"/>
<hole x="-8.5" y="-0.25" drill="1.1"/>
<wire x1="-9" y1="5.85" x2="9" y2="5.85" width="0.127" layer="21"/>
<wire x1="-9" y1="-5.85" x2="9" y2="-5.85" width="0.127" layer="21"/>
<wire x1="-9" y1="5.85" x2="-9" y2="0.27" width="0.127" layer="21"/>
<wire x1="9" y1="5.85" x2="9" y2="0.27" width="0.127" layer="21"/>
<wire x1="-9" y1="-5.85" x2="-9" y2="-0.77" width="0.127" layer="21"/>
<wire x1="9" y1="-5.85" x2="9" y2="-0.77" width="0.127" layer="21"/>
<wire x1="-9" y1="5.85" x2="9" y2="5.85" width="0.127" layer="51"/>
<wire x1="-9" y1="-5.85" x2="9" y2="-5.85" width="0.127" layer="51"/>
<wire x1="-9" y1="5.85" x2="-9" y2="0.27" width="0.127" layer="51"/>
<wire x1="9" y1="5.85" x2="9" y2="0.27" width="0.127" layer="51"/>
<wire x1="-9" y1="-5.85" x2="-9" y2="-0.77" width="0.127" layer="51"/>
<wire x1="9" y1="-5.85" x2="9" y2="-0.77" width="0.127" layer="51"/>
<text x="3.05" y="-7.35" size="1.27" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="-5.2" y="-7.35" size="1.27" layer="27" font="vector" ratio="15">&gt;VALUE</text>
<circle x="7" y="-4" radius="0.25" width="0.127" layer="21"/>
<circle x="7" y="-4" radius="0.13" width="0.127" layer="21"/>
<circle x="7" y="-4" radius="0.014140625" width="0.127" layer="21"/>
<circle x="7" y="-4" radius="0.25" width="0.127" layer="51"/>
<circle x="7" y="-4" radius="0.13" width="0.127" layer="51"/>
<circle x="7" y="-4" radius="0.014140625" width="0.127" layer="51"/>
</package>
<package name="JST_PHD_4PIN_2MM" urn="urn:adsk.eagle:footprint:8413928/1" library_version="52">
<pad name="P$1" x="-3" y="-1" drill="0.7"/>
<pad name="P$2" x="-3" y="1" drill="0.7"/>
<pad name="P$3" x="-1" y="-1" drill="0.7"/>
<pad name="P$4" x="-1" y="1" drill="0.7"/>
<pad name="P$5" x="1" y="-1" drill="0.7"/>
<pad name="P$6" x="1" y="1" drill="0.7"/>
<pad name="P$7" x="3" y="-1" drill="0.7"/>
<pad name="P$8" x="3" y="1" drill="0.7"/>
<wire x1="-4.92" y1="-3" x2="-4.92" y2="8.5" width="0.127" layer="21"/>
<wire x1="-4.92" y1="8.5" x2="5.08" y2="8.5" width="0.127" layer="21"/>
<wire x1="5.08" y1="8.5" x2="5.08" y2="-3" width="0.127" layer="21"/>
<wire x1="5.08" y1="-3" x2="-4.92" y2="-3" width="0.127" layer="21"/>
<wire x1="-4.92" y1="0" x2="-4.92" y2="11" width="0.127" layer="27"/>
<wire x1="-4.92" y1="11" x2="5.08" y2="11" width="0.127" layer="27"/>
<wire x1="5.08" y1="11" x2="5.08" y2="8" width="0.127" layer="27"/>
<text x="6.08" y="7" size="1.27" layer="21" font="vector" ratio="15">&gt;NAME</text>
<text x="6.08" y="5" size="1.27" layer="27" font="vector" ratio="15">&gt;VALUE</text>
<text x="-2.056" y="8.89" size="1.27" layer="27" font="vector" ratio="15">MATE </text>
<text x="-3.42" y="7" size="1.27" layer="27" font="vector" ratio="15">HEADER</text>
</package>
<package name="JST_B8B-PH-K-S" urn="urn:adsk.eagle:footprint:8413942/1" library_version="52">
<pad name="1" x="7" y="-0.55" drill="0.7"/>
<pad name="2" x="5" y="-0.55" drill="0.7"/>
<pad name="3" x="3" y="-0.55" drill="0.7"/>
<pad name="4" x="1" y="-0.55" drill="0.7"/>
<pad name="5" x="-1" y="-0.55" drill="0.7"/>
<pad name="6" x="-3" y="-0.55" drill="0.7"/>
<pad name="7" x="-5" y="-0.55" drill="0.7"/>
<pad name="8" x="-7" y="-0.55" drill="0.7"/>
<wire x1="-8.95" y1="-2.25" x2="8.95" y2="-2.25" width="0.127" layer="21"/>
<wire x1="-8.95" y1="2.25" x2="8.95" y2="2.25" width="0.127" layer="21"/>
<wire x1="-8.95" y1="2.25" x2="-8.95" y2="-2.25" width="0.127" layer="21"/>
<wire x1="8.95" y1="2.25" x2="8.95" y2="-2.25" width="0.127" layer="21"/>
<wire x1="-8.95" y1="-2.25" x2="8.95" y2="-2.25" width="0.127" layer="51"/>
<wire x1="-8.95" y1="2.25" x2="8.95" y2="2.25" width="0.127" layer="51"/>
<wire x1="-8.95" y1="2.25" x2="-8.95" y2="-2.25" width="0.127" layer="51"/>
<wire x1="8.95" y1="2.25" x2="8.95" y2="-2.25" width="0.127" layer="51"/>
<text x="-0.55" y="-4.07" size="1.27" layer="25">&gt;NAME</text>
<text x="-8.8" y="-4.07" size="1.27" layer="27">&gt;VALUE</text>
<circle x="7" y="-2.8" radius="0.25" width="0.127" layer="21"/>
<circle x="7" y="-2.8" radius="0.13" width="0.127" layer="21"/>
<circle x="7" y="-2.8" radius="0.014140625" width="0.127" layer="21"/>
<circle x="7" y="-2.8" radius="0.25" width="0.127" layer="51"/>
<circle x="7" y="-2.8" radius="0.13" width="0.127" layer="51"/>
<circle x="7" y="-2.8" radius="0.014140625" width="0.127" layer="51"/>
</package>
<package name="JST_S8B-PH-K-S" urn="urn:adsk.eagle:footprint:8413943/1" library_version="52">
<pad name="1" x="7" y="-2.875" drill="0.7"/>
<pad name="2" x="5" y="-2.875" drill="0.7"/>
<pad name="3" x="3" y="-2.875" drill="0.7"/>
<pad name="4" x="1" y="-2.875" drill="0.7"/>
<pad name="5" x="-1" y="-2.875" drill="0.7"/>
<pad name="6" x="-3" y="-2.875" drill="0.7"/>
<pad name="7" x="-5" y="-2.875" drill="0.7"/>
<pad name="8" x="-7" y="-2.875" drill="0.7"/>
<text x="0.7" y="-5.395" size="1.27" layer="25">&gt;NAME</text>
<text x="-6.55" y="-5.395" size="1.27" layer="27">&gt;VALUE</text>
<circle x="7" y="-4.375" radius="0.25" width="0.127" layer="21"/>
<circle x="7" y="-4.375" radius="0.13" width="0.127" layer="21"/>
<circle x="7" y="-4.375" radius="0.014140625" width="0.127" layer="21"/>
<circle x="7" y="-4.375" radius="0.25" width="0.127" layer="51"/>
<circle x="7" y="-4.375" radius="0.13" width="0.127" layer="51"/>
<circle x="7" y="-4.375" radius="0.014140625" width="0.127" layer="51"/>
<wire x1="-8.95" y1="-3.925" x2="8.95" y2="-3.925" width="0.127" layer="51"/>
<wire x1="-8.95" y1="3.925" x2="8.95" y2="3.925" width="0.127" layer="51"/>
<wire x1="-8.95" y1="3.925" x2="-8.95" y2="-3.925" width="0.127" layer="51"/>
<wire x1="8.95" y1="3.925" x2="8.95" y2="-3.925" width="0.127" layer="51"/>
<wire x1="-8.95" y1="-3.925" x2="8.95" y2="-3.925" width="0.127" layer="21"/>
<wire x1="-8.95" y1="3.925" x2="8.95" y2="3.925" width="0.127" layer="21"/>
<wire x1="-8.95" y1="3.925" x2="-8.95" y2="-3.925" width="0.127" layer="21"/>
<wire x1="8.95" y1="3.925" x2="8.95" y2="-3.925" width="0.127" layer="21"/>
</package>
<package name="JST_B08B-PSILE-1" urn="urn:adsk.eagle:footprint:8413969/1" library_version="52">
<pad name="P$1" x="6" y="-1.5" drill="1.65"/>
<pad name="P$2" x="6" y="4.45" drill="1.65"/>
<pad name="P$3" x="2" y="-1.5" drill="1.65"/>
<pad name="P$4" x="2" y="4.45" drill="1.65"/>
<pad name="P$5" x="-2" y="-1.5" drill="1.65"/>
<pad name="P$6" x="-2" y="4.45" drill="1.65"/>
<pad name="P$7" x="-6" y="-1.5" drill="1.65"/>
<pad name="P$8" x="-6" y="4.45" drill="1.65"/>
<hole x="7.65" y="-6" drill="1.3"/>
<wire x1="-8.35" y1="7.525" x2="-8.35" y2="-7.525" width="0.127" layer="21"/>
<wire x1="-8.35" y1="7.525" x2="8.35" y2="7.525" width="0.127" layer="21"/>
<wire x1="-8.35" y1="-7.525" x2="8.35" y2="-7.525" width="0.127" layer="21"/>
<wire x1="8.35" y1="7.525" x2="8.35" y2="-5.57" width="0.127" layer="21"/>
<wire x1="8.35" y1="-7.525" x2="8.35" y2="-6.44" width="0.127" layer="21"/>
<wire x1="-8.35" y1="7.525" x2="-8.35" y2="-7.525" width="0.127" layer="51"/>
<wire x1="-8.35" y1="7.525" x2="8.35" y2="7.525" width="0.127" layer="51"/>
<wire x1="-8.35" y1="-7.525" x2="8.35" y2="-7.525" width="0.127" layer="51"/>
<wire x1="8.35" y1="7.525" x2="8.35" y2="-5.57" width="0.127" layer="51"/>
<wire x1="8.35" y1="-7.525" x2="8.35" y2="-6.44" width="0.127" layer="51"/>
<text x="2.45" y="-9.07" size="1.27" layer="25">&gt;NAME</text>
<text x="-4.8" y="-9.07" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="JST_B8PS-VH" urn="urn:adsk.eagle:footprint:8413994/1" library_version="52">
<pad name="P$1" x="-13.86" y="0" drill="1.65"/>
<pad name="P$2" x="-9.9" y="0" drill="1.65"/>
<pad name="P$3" x="-5.94" y="0" drill="1.65"/>
<pad name="P$4" x="-1.98" y="0" drill="1.65"/>
<pad name="P$5" x="1.98" y="0" drill="1.65"/>
<pad name="P$6" x="5.94" y="0" drill="1.65"/>
<pad name="P$7" x="9.9" y="0" drill="1.65"/>
<pad name="P$8" x="13.86" y="0" drill="1.65"/>
<text x="-15.88" y="3" size="1.27" layer="25">&gt;NAME</text>
<text x="-8.88" y="3" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-15.81" y1="-7.2" x2="-15.81" y2="1.72" width="0.127" layer="51"/>
<wire x1="-15.28" y1="2.25" x2="15.84" y2="2.25" width="0.127" layer="51"/>
<wire x1="-15.81" y1="-7.2" x2="15.84" y2="-7.2" width="0.127" layer="51"/>
<wire x1="-15.81" y1="1.72" x2="-15.28" y2="2.25" width="0.127" layer="51"/>
<wire x1="15.84" y1="2.25" x2="15.84" y2="-7.2" width="0.127" layer="51"/>
<wire x1="-15.81" y1="-7.2" x2="-15.81" y2="1.72" width="0.127" layer="21"/>
<wire x1="-15.28" y1="2.25" x2="15.84" y2="2.25" width="0.127" layer="21"/>
<wire x1="-15.81" y1="-7.2" x2="15.84" y2="-7.2" width="0.127" layer="21"/>
<wire x1="-15.81" y1="1.72" x2="-15.28" y2="2.25" width="0.127" layer="21"/>
<wire x1="15.84" y1="2.25" x2="15.84" y2="-7.2" width="0.127" layer="21"/>
</package>
<package name="JST_B8P-VH" urn="urn:adsk.eagle:footprint:8413995/1" library_version="52">
<pad name="P$1" x="-13.86" y="0" drill="1.65"/>
<pad name="P$2" x="-9.9" y="0" drill="1.65"/>
<pad name="P$3" x="-5.94" y="0" drill="1.65"/>
<pad name="P$4" x="-1.98" y="0" drill="1.65"/>
<pad name="P$5" x="1.98" y="0" drill="1.65"/>
<pad name="P$6" x="5.94" y="0" drill="1.65"/>
<pad name="P$7" x="9.9" y="0" drill="1.65"/>
<pad name="P$8" x="13.86" y="0" drill="1.65"/>
<text x="-15.88" y="-7" size="1.27" layer="25">&gt;NAME</text>
<text x="3" y="-7" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-15.83" y1="-5.25" x2="-15.83" y2="2.5" width="0.127" layer="51"/>
<wire x1="-15.83" y1="2.5" x2="-15.83" y2="3.25" width="0.127" layer="51"/>
<wire x1="15.81" y1="3.25" x2="15.81" y2="2.5" width="0.127" layer="51"/>
<wire x1="15.81" y1="2.5" x2="15.81" y2="-5.25" width="0.127" layer="51"/>
<wire x1="-15.83" y1="3.25" x2="15.81" y2="3.25" width="0.127" layer="51"/>
<wire x1="-15.83" y1="-5.25" x2="15.81" y2="-5.25" width="0.127" layer="51"/>
<wire x1="-15.83" y1="2.5" x2="15.81" y2="2.5" width="0.127" layer="51"/>
<wire x1="-15.83" y1="-5.25" x2="-15.83" y2="2.5" width="0.127" layer="21"/>
<wire x1="-15.83" y1="2.5" x2="-15.83" y2="3.25" width="0.127" layer="21"/>
<wire x1="15.81" y1="3.25" x2="15.81" y2="2.5" width="0.127" layer="21"/>
<wire x1="15.81" y1="2.5" x2="15.81" y2="-5.25" width="0.127" layer="21"/>
<wire x1="-15.83" y1="3.25" x2="15.81" y2="3.25" width="0.127" layer="21"/>
<wire x1="-15.83" y1="-5.25" x2="15.81" y2="-5.25" width="0.127" layer="21"/>
<wire x1="-15.83" y1="2.5" x2="15.81" y2="2.5" width="0.127" layer="21"/>
</package>
<package name="JST_B8P-VH-FB-B" urn="urn:adsk.eagle:footprint:8413996/1" library_version="52">
<pad name="P$1" x="-13.86" y="0" drill="1.65"/>
<pad name="P$2" x="-9.9" y="0" drill="1.65"/>
<pad name="P$3" x="-5.94" y="0" drill="1.65"/>
<pad name="P$4" x="-1.98" y="0" drill="1.65"/>
<pad name="P$5" x="1.98" y="0" drill="1.65"/>
<pad name="P$6" x="5.94" y="0" drill="1.65"/>
<pad name="P$7" x="9.9" y="0" drill="1.65"/>
<pad name="P$8" x="13.86" y="0" drill="1.65"/>
<hole x="-15.36" y="-3.4" drill="1.4"/>
<wire x1="-16.78" y1="4.85" x2="16.78" y2="4.85" width="0.127" layer="21"/>
<wire x1="-16.78" y1="-4.85" x2="16.78" y2="-4.85" width="0.127" layer="21"/>
<wire x1="-16.78" y1="4.85" x2="-16.78" y2="-4.85" width="0.127" layer="21"/>
<wire x1="16.78" y1="4.85" x2="16.78" y2="-4.85" width="0.127" layer="21"/>
<wire x1="-16.78" y1="4.85" x2="16.78" y2="4.85" width="0.127" layer="51"/>
<wire x1="-16.78" y1="-4.85" x2="16.78" y2="-4.85" width="0.127" layer="51"/>
<wire x1="-16.78" y1="4.85" x2="-16.78" y2="-4.85" width="0.127" layer="51"/>
<wire x1="16.78" y1="4.85" x2="16.78" y2="-4.85" width="0.127" layer="51"/>
<text x="-16.88" y="5" size="1.27" layer="25">&gt;NAME</text>
<text x="-9.88" y="5" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="JST_B08B-ZESK-1D" urn="urn:adsk.eagle:footprint:8414015/1" library_version="52">
<pad name="P$1" x="5.25" y="1.98" drill="0.7" rot="R180"/>
<pad name="P$2" x="3.75" y="-0.02" drill="0.7" rot="R180"/>
<pad name="P$3" x="2.25" y="1.98" drill="0.7" rot="R180"/>
<pad name="P$4" x="0.75" y="-0.02" drill="0.7" rot="R180"/>
<pad name="P$5" x="-0.75" y="1.98" drill="0.7" rot="R180"/>
<pad name="P$6" x="-2.25" y="-0.02" drill="0.7" rot="R180"/>
<pad name="P$7" x="-3.75" y="1.98" drill="0.7" rot="R180"/>
<pad name="P$8" x="-5.25" y="-0.02" drill="0.7" rot="R180"/>
<hole x="6.9" y="-1.82" drill="0.8"/>
<wire x1="-7.505" y1="3.115" x2="-7.505" y2="-3.105" width="0.127" layer="51"/>
<wire x1="7.505" y1="3.115" x2="7.505" y2="-3.105" width="0.127" layer="51"/>
<wire x1="-7.505" y1="3.115" x2="7.505" y2="3.115" width="0.127" layer="51"/>
<wire x1="-7.505" y1="-3.105" x2="7.505" y2="-3.105" width="0.127" layer="51"/>
<wire x1="-7.505" y1="3.115" x2="-7.505" y2="-3.105" width="0.127" layer="21"/>
<wire x1="7.505" y1="3.115" x2="7.505" y2="-3.105" width="0.127" layer="21"/>
<wire x1="-7.505" y1="3.115" x2="7.505" y2="3.115" width="0.127" layer="21"/>
<wire x1="-7.505" y1="-3.105" x2="7.505" y2="-3.105" width="0.127" layer="21"/>
<text x="1.5" y="3.48" size="1.27" layer="25">&gt;NAME</text>
<text x="-6.75" y="3.48" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="JST_S08B-ZESK-2D" urn="urn:adsk.eagle:footprint:8414016/1" library_version="52">
<pad name="P$1" x="5.25" y="-7.5" drill="0.7"/>
<pad name="P$2" x="3.75" y="-3.8" drill="0.7"/>
<pad name="P$3" x="2.25" y="-7.5" drill="0.7"/>
<pad name="P$4" x="0.75" y="-3.8" drill="0.7"/>
<pad name="P$5" x="-0.75" y="-7.5" drill="0.7"/>
<pad name="P$6" x="-2.25" y="-3.8" drill="0.7"/>
<pad name="P$7" x="-3.75" y="-7.5" drill="0.7"/>
<pad name="P$8" x="-5.25" y="-3.8" drill="0.7"/>
<hole x="6.8" y="-5.65" drill="1.1"/>
<hole x="-6.8" y="-5.65" drill="1.1"/>
<wire x1="7.5" y1="7.22" x2="7.5" y2="6.47" width="0.127" layer="21"/>
<wire x1="7.5" y1="6.47" x2="7.5" y2="-8.38" width="0.127" layer="21"/>
<wire x1="-7.5" y1="7.22" x2="-7.5" y2="6.47" width="0.127" layer="21"/>
<wire x1="-7.5" y1="6.47" x2="-7.5" y2="-8.38" width="0.127" layer="21"/>
<wire x1="-7.5" y1="-8.38" x2="7.5" y2="-8.38" width="0.127" layer="21"/>
<wire x1="-7.5" y1="7.22" x2="7.5" y2="7.22" width="0.127" layer="21"/>
<wire x1="7.5" y1="7.22" x2="7.5" y2="6.47" width="0.127" layer="51"/>
<wire x1="7.5" y1="6.47" x2="7.5" y2="-8.38" width="0.127" layer="51"/>
<wire x1="-7.5" y1="7.22" x2="-7.5" y2="6.47" width="0.127" layer="51"/>
<wire x1="-7.5" y1="6.47" x2="-7.5" y2="-8.38" width="0.127" layer="51"/>
<wire x1="-7.5" y1="-8.38" x2="7.5" y2="-8.38" width="0.127" layer="51"/>
<wire x1="-7.5" y1="7.22" x2="7.5" y2="7.22" width="0.127" layer="51"/>
<wire x1="-7.5" y1="6.47" x2="7.5" y2="6.47" width="0.127" layer="21"/>
<wire x1="-7.5" y1="6.47" x2="7.5" y2="6.47" width="0.127" layer="51"/>
<text x="1.8" y="-10" size="1.27" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="-6.45" y="-10" size="1.27" layer="27" font="vector" ratio="15">&gt;VALUE</text>
</package>
<package name="SOT65P210X110-3" urn="urn:adsk.eagle:footprint:3809952/2" library_version="76">
<description>3-SOT23, 0.65 mm pitch, 2.1 mm span, 2 X 1.25 X 1.1 mm body
&lt;p&gt;3-pin SOT23 package with 0.65 mm pitch, 2.1 mm span with body size 2 X 1.25 X 1.1 mm&lt;/p&gt;</description>
<circle x="-1.0698" y="1.409" radius="0.25" width="0" layer="21"/>
<wire x1="-0.675" y1="1.219" x2="0.675" y2="1.219" width="0.12" layer="21"/>
<wire x1="0.675" y1="1.219" x2="0.675" y2="0.509" width="0.12" layer="21"/>
<wire x1="-0.675" y1="-1.219" x2="0.675" y2="-1.219" width="0.12" layer="21"/>
<wire x1="0.675" y1="-1.219" x2="0.675" y2="-0.509" width="0.12" layer="21"/>
<wire x1="0.675" y1="-1.05" x2="-0.675" y2="-1.05" width="0.12" layer="51"/>
<wire x1="-0.675" y1="-1.05" x2="-0.675" y2="1.05" width="0.12" layer="51"/>
<wire x1="-0.675" y1="1.05" x2="0.675" y2="1.05" width="0.12" layer="51"/>
<wire x1="0.675" y1="1.05" x2="0.675" y2="-1.05" width="0.12" layer="51"/>
<smd name="A" x="-0.9704" y="0.65" dx="0.9884" dy="0.51" layer="1"/>
<smd name="NC" x="-0.9704" y="-0.65" dx="0.9884" dy="0.51" layer="1"/>
<smd name="K" x="0.9704" y="0" dx="0.9884" dy="0.51" layer="1"/>
<text x="0" y="2.294" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.854" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="RESC0603X30" urn="urn:adsk.eagle:footprint:3811152/1" library_version="58">
<description>CHIP, 0.6 X 0.3 X 0.3 mm body
&lt;p&gt;CHIP package with body size 0.6 X 0.3 X 0.3 mm&lt;/p&gt;</description>
<wire x1="0.315" y1="0.5124" x2="-0.315" y2="0.5124" width="0.12" layer="21"/>
<wire x1="0.315" y1="-0.5124" x2="-0.315" y2="-0.5124" width="0.12" layer="21"/>
<wire x1="0.315" y1="-0.165" x2="-0.315" y2="-0.165" width="0.12" layer="51"/>
<wire x1="-0.315" y1="-0.165" x2="-0.315" y2="0.165" width="0.12" layer="51"/>
<wire x1="-0.315" y1="0.165" x2="0.315" y2="0.165" width="0.12" layer="51"/>
<wire x1="0.315" y1="0.165" x2="0.315" y2="-0.165" width="0.12" layer="51"/>
<smd name="1" x="-0.325" y="0" dx="0.4469" dy="0.3969" layer="1"/>
<smd name="2" x="0.325" y="0" dx="0.4469" dy="0.3969" layer="1"/>
<text x="0" y="1.1474" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.1474" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="RESC3225X70" urn="urn:adsk.eagle:footprint:3811185/1" library_version="58">
<description>CHIP, 3.2 X 2.5 X 0.7 mm body
&lt;p&gt;CHIP package with body size 3.2 X 2.5 X 0.7 mm&lt;/p&gt;</description>
<wire x1="1.7" y1="1.6717" x2="-1.7" y2="1.6717" width="0.12" layer="21"/>
<wire x1="1.7" y1="-1.6717" x2="-1.7" y2="-1.6717" width="0.12" layer="21"/>
<wire x1="1.7" y1="-1.35" x2="-1.7" y2="-1.35" width="0.12" layer="51"/>
<wire x1="-1.7" y1="-1.35" x2="-1.7" y2="1.35" width="0.12" layer="51"/>
<wire x1="-1.7" y1="1.35" x2="1.7" y2="1.35" width="0.12" layer="51"/>
<wire x1="1.7" y1="1.35" x2="1.7" y2="-1.35" width="0.12" layer="51"/>
<smd name="1" x="-1.49" y="0" dx="1.1354" dy="2.7153" layer="1"/>
<smd name="2" x="1.49" y="0" dx="1.1354" dy="2.7153" layer="1"/>
<text x="0" y="2.3067" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.3067" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="RESC4532X70" urn="urn:adsk.eagle:footprint:3811192/1" library_version="58">
<description>CHIP, 4.5 X 3.2 X 0.7 mm body
&lt;p&gt;CHIP package with body size 4.5 X 3.2 X 0.7 mm&lt;/p&gt;</description>
<wire x1="2.35" y1="2.0217" x2="-2.35" y2="2.0217" width="0.12" layer="21"/>
<wire x1="2.35" y1="-2.0217" x2="-2.35" y2="-2.0217" width="0.12" layer="21"/>
<wire x1="2.35" y1="-1.7" x2="-2.35" y2="-1.7" width="0.12" layer="51"/>
<wire x1="-2.35" y1="-1.7" x2="-2.35" y2="1.7" width="0.12" layer="51"/>
<wire x1="-2.35" y1="1.7" x2="2.35" y2="1.7" width="0.12" layer="51"/>
<wire x1="2.35" y1="1.7" x2="2.35" y2="-1.7" width="0.12" layer="51"/>
<smd name="1" x="-2.14" y="0" dx="1.1354" dy="3.4153" layer="1"/>
<smd name="2" x="2.14" y="0" dx="1.1354" dy="3.4153" layer="1"/>
<text x="0" y="2.6567" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.6567" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="RESC5025X70" urn="urn:adsk.eagle:footprint:3811199/1" library_version="58">
<description>CHIP, 5 X 2.5 X 0.7 mm body
&lt;p&gt;CHIP package with body size 5 X 2.5 X 0.7 mm&lt;/p&gt;</description>
<wire x1="2.6" y1="1.6717" x2="-2.6" y2="1.6717" width="0.12" layer="21"/>
<wire x1="2.6" y1="-1.6717" x2="-2.6" y2="-1.6717" width="0.12" layer="21"/>
<wire x1="2.6" y1="-1.35" x2="-2.6" y2="-1.35" width="0.12" layer="51"/>
<wire x1="-2.6" y1="-1.35" x2="-2.6" y2="1.35" width="0.12" layer="51"/>
<wire x1="-2.6" y1="1.35" x2="2.6" y2="1.35" width="0.12" layer="51"/>
<wire x1="2.6" y1="1.35" x2="2.6" y2="-1.35" width="0.12" layer="51"/>
<smd name="1" x="-2.34" y="0" dx="1.2354" dy="2.7153" layer="1"/>
<smd name="2" x="2.34" y="0" dx="1.2354" dy="2.7153" layer="1"/>
<text x="0" y="2.3067" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.3067" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="RESC5750X229" urn="urn:adsk.eagle:footprint:3811213/1" library_version="58">
<description>CHIP, 5.7 X 5 X 2.29 mm body
&lt;p&gt;CHIP package with body size 5.7 X 5 X 2.29 mm&lt;/p&gt;</description>
<wire x1="3.05" y1="3.0179" x2="-3.05" y2="3.0179" width="0.12" layer="21"/>
<wire x1="3.05" y1="-3.0179" x2="-3.05" y2="-3.0179" width="0.12" layer="21"/>
<wire x1="3.05" y1="-2.7" x2="-3.05" y2="-2.7" width="0.12" layer="51"/>
<wire x1="-3.05" y1="-2.7" x2="-3.05" y2="2.7" width="0.12" layer="51"/>
<wire x1="-3.05" y1="2.7" x2="3.05" y2="2.7" width="0.12" layer="51"/>
<wire x1="3.05" y1="2.7" x2="3.05" y2="-2.7" width="0.12" layer="51"/>
<smd name="1" x="-2.6355" y="0" dx="1.5368" dy="5.4078" layer="1"/>
<smd name="2" x="2.6355" y="0" dx="1.5368" dy="5.4078" layer="1"/>
<text x="0" y="3.6529" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-3.6529" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="RESC6432X70" urn="urn:adsk.eagle:footprint:3811285/1" library_version="58">
<description>CHIP, 6.4 X 3.2 X 0.7 mm body
&lt;p&gt;CHIP package with body size 6.4 X 3.2 X 0.7 mm&lt;/p&gt;</description>
<wire x1="3.3" y1="2.0217" x2="-3.3" y2="2.0217" width="0.12" layer="21"/>
<wire x1="3.3" y1="-2.0217" x2="-3.3" y2="-2.0217" width="0.12" layer="21"/>
<wire x1="3.3" y1="-1.7" x2="-3.3" y2="-1.7" width="0.12" layer="51"/>
<wire x1="-3.3" y1="-1.7" x2="-3.3" y2="1.7" width="0.12" layer="51"/>
<wire x1="-3.3" y1="1.7" x2="3.3" y2="1.7" width="0.12" layer="51"/>
<wire x1="3.3" y1="1.7" x2="3.3" y2="-1.7" width="0.12" layer="51"/>
<smd name="1" x="-3.04" y="0" dx="1.2354" dy="3.4153" layer="1"/>
<smd name="2" x="3.04" y="0" dx="1.2354" dy="3.4153" layer="1"/>
<text x="0" y="2.6567" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.6567" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="RESC2037X52" urn="urn:adsk.eagle:footprint:3950970/1" library_version="58">
<description>CHIP, 2 X 3.75 X 0.52 mm body
&lt;p&gt;CHIP package with body size 2 X 3.75 X 0.52 mm&lt;/p&gt;</description>
<wire x1="1.1" y1="2.3442" x2="-1.1" y2="2.3442" width="0.12" layer="21"/>
<wire x1="1.1" y1="-2.3442" x2="-1.1" y2="-2.3442" width="0.12" layer="21"/>
<wire x1="1.1" y1="-2.025" x2="-1.1" y2="-2.025" width="0.12" layer="51"/>
<wire x1="-1.1" y1="-2.025" x2="-1.1" y2="2.025" width="0.12" layer="51"/>
<wire x1="-1.1" y1="2.025" x2="1.1" y2="2.025" width="0.12" layer="51"/>
<wire x1="1.1" y1="2.025" x2="1.1" y2="-2.025" width="0.12" layer="51"/>
<smd name="1" x="-0.94" y="0" dx="1.0354" dy="4.0603" layer="1"/>
<smd name="2" x="0.94" y="0" dx="1.0354" dy="4.0603" layer="1"/>
<text x="0" y="2.9792" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.9792" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="INDC1005X60" urn="urn:adsk.eagle:footprint:3810111/1" library_version="58">
<description>CHIP, 1 X 0.5 X 0.6 mm body
&lt;p&gt;CHIP package with body size 1 X 0.5 X 0.6 mm&lt;/p&gt;</description>
<wire x1="0.55" y1="0.6717" x2="-0.55" y2="0.6717" width="0.12" layer="21"/>
<wire x1="0.55" y1="-0.6717" x2="-0.55" y2="-0.6717" width="0.12" layer="21"/>
<wire x1="0.55" y1="-0.35" x2="-0.55" y2="-0.35" width="0.12" layer="51"/>
<wire x1="-0.55" y1="-0.35" x2="-0.55" y2="0.35" width="0.12" layer="51"/>
<wire x1="-0.55" y1="0.35" x2="0.55" y2="0.35" width="0.12" layer="51"/>
<wire x1="0.55" y1="0.35" x2="0.55" y2="-0.35" width="0.12" layer="51"/>
<smd name="1" x="-0.475" y="0" dx="0.5791" dy="0.7153" layer="1"/>
<smd name="2" x="0.475" y="0" dx="0.5791" dy="0.7153" layer="1"/>
<text x="0" y="1.3067" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.3067" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="INDC1608X95" urn="urn:adsk.eagle:footprint:3810120/1" library_version="58">
<description>CHIP, 1.6 X 0.8 X 0.95 mm body
&lt;p&gt;CHIP package with body size 1.6 X 0.8 X 0.95 mm&lt;/p&gt;</description>
<wire x1="0.875" y1="0.7991" x2="-0.875" y2="0.7991" width="0.12" layer="21"/>
<wire x1="0.875" y1="-0.7991" x2="-0.875" y2="-0.7991" width="0.12" layer="21"/>
<wire x1="0.875" y1="-0.475" x2="-0.875" y2="-0.475" width="0.12" layer="51"/>
<wire x1="-0.875" y1="-0.475" x2="-0.875" y2="0.475" width="0.12" layer="51"/>
<wire x1="-0.875" y1="0.475" x2="0.875" y2="0.475" width="0.12" layer="51"/>
<wire x1="0.875" y1="0.475" x2="0.875" y2="-0.475" width="0.12" layer="51"/>
<smd name="1" x="-0.7746" y="0" dx="0.9209" dy="0.9702" layer="1"/>
<smd name="2" x="0.7746" y="0" dx="0.9209" dy="0.9702" layer="1"/>
<text x="0" y="1.4341" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.4341" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="INDC2520X220" urn="urn:adsk.eagle:footprint:3810124/1" library_version="58">
<description>CHIP, 2.5 X 2 X 2.2 mm body
&lt;p&gt;CHIP package with body size 2.5 X 2 X 2.2 mm&lt;/p&gt;</description>
<wire x1="1.35" y1="1.4692" x2="-1.35" y2="1.4692" width="0.12" layer="21"/>
<wire x1="1.35" y1="-1.4692" x2="-1.35" y2="-1.4692" width="0.12" layer="21"/>
<wire x1="1.35" y1="-1.15" x2="-1.35" y2="-1.15" width="0.12" layer="51"/>
<wire x1="-1.35" y1="-1.15" x2="-1.35" y2="1.15" width="0.12" layer="51"/>
<wire x1="-1.35" y1="1.15" x2="1.35" y2="1.15" width="0.12" layer="51"/>
<wire x1="1.35" y1="1.15" x2="1.35" y2="-1.15" width="0.12" layer="51"/>
<smd name="1" x="-1.2283" y="0" dx="0.9588" dy="2.3103" layer="1"/>
<smd name="2" x="1.2283" y="0" dx="0.9588" dy="2.3103" layer="1"/>
<text x="0" y="2.1042" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.1042" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="INDC3225X135" urn="urn:adsk.eagle:footprint:3810699/1" library_version="58">
<description>CHIP, 3.2 X 2.5 X 1.35 mm body
&lt;p&gt;CHIP package with body size 3.2 X 2.5 X 1.35 mm&lt;/p&gt;</description>
<wire x1="1.7" y1="1.6717" x2="-1.7" y2="1.6717" width="0.12" layer="21"/>
<wire x1="1.7" y1="-1.6717" x2="-1.7" y2="-1.6717" width="0.12" layer="21"/>
<wire x1="1.7" y1="-1.35" x2="-1.7" y2="-1.35" width="0.12" layer="51"/>
<wire x1="-1.7" y1="-1.35" x2="-1.7" y2="1.35" width="0.12" layer="51"/>
<wire x1="-1.7" y1="1.35" x2="1.7" y2="1.35" width="0.12" layer="51"/>
<wire x1="1.7" y1="1.35" x2="1.7" y2="-1.35" width="0.12" layer="51"/>
<smd name="1" x="-1.4754" y="0" dx="1.1646" dy="2.7153" layer="1"/>
<smd name="2" x="1.4754" y="0" dx="1.1646" dy="2.7153" layer="1"/>
<text x="0" y="2.3067" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.3067" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="INDC4509X190" urn="urn:adsk.eagle:footprint:3810713/1" library_version="58">
<description>CHIP, 4.5 X 0.9 X 1.9 mm body
&lt;p&gt;CHIP package with body size 4.5 X 0.9 X 1.9 mm&lt;/p&gt;</description>
<wire x1="2.4" y1="0.9192" x2="-2.4" y2="0.9192" width="0.12" layer="21"/>
<wire x1="2.4" y1="-0.9192" x2="-2.4" y2="-0.9192" width="0.12" layer="21"/>
<wire x1="2.4" y1="-0.6" x2="-2.4" y2="-0.6" width="0.12" layer="51"/>
<wire x1="-2.4" y1="-0.6" x2="-2.4" y2="0.6" width="0.12" layer="51"/>
<wire x1="-2.4" y1="0.6" x2="2.4" y2="0.6" width="0.12" layer="51"/>
<wire x1="2.4" y1="0.6" x2="2.4" y2="-0.6" width="0.12" layer="51"/>
<smd name="1" x="-2.11" y="0" dx="1.2904" dy="1.2103" layer="1"/>
<smd name="2" x="2.11" y="0" dx="1.2904" dy="1.2103" layer="1"/>
<text x="0" y="1.5542" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.5542" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="INDC4532X175" urn="urn:adsk.eagle:footprint:3810720/1" library_version="58">
<description>CHIP, 4.5 X 3.2 X 1.75 mm body
&lt;p&gt;CHIP package with body size 4.5 X 3.2 X 1.75 mm&lt;/p&gt;</description>
<wire x1="2.4" y1="2.0217" x2="-2.4" y2="2.0217" width="0.12" layer="21"/>
<wire x1="2.4" y1="-2.0217" x2="-2.4" y2="-2.0217" width="0.12" layer="21"/>
<wire x1="2.4" y1="-1.7" x2="-2.4" y2="-1.7" width="0.12" layer="51"/>
<wire x1="-2.4" y1="-1.7" x2="-2.4" y2="1.7" width="0.12" layer="51"/>
<wire x1="-2.4" y1="1.7" x2="2.4" y2="1.7" width="0.12" layer="51"/>
<wire x1="2.4" y1="1.7" x2="2.4" y2="-1.7" width="0.12" layer="51"/>
<smd name="1" x="-2.0565" y="0" dx="1.3973" dy="3.4153" layer="1"/>
<smd name="2" x="2.0565" y="0" dx="1.3973" dy="3.4153" layer="1"/>
<text x="0" y="2.6567" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.6567" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="INDC5750X180" urn="urn:adsk.eagle:footprint:3810723/1" library_version="58">
<description>CHIP, 5.7 X 5 X 1.8 mm body
&lt;p&gt;CHIP package with body size 5.7 X 5 X 1.8 mm&lt;/p&gt;</description>
<wire x1="2.95" y1="2.9692" x2="-2.95" y2="2.9692" width="0.12" layer="21"/>
<wire x1="2.95" y1="-2.9692" x2="-2.95" y2="-2.9692" width="0.12" layer="21"/>
<wire x1="2.95" y1="-2.65" x2="-2.95" y2="-2.65" width="0.12" layer="51"/>
<wire x1="-2.95" y1="-2.65" x2="-2.95" y2="2.65" width="0.12" layer="51"/>
<wire x1="-2.95" y1="2.65" x2="2.95" y2="2.65" width="0.12" layer="51"/>
<wire x1="2.95" y1="2.65" x2="2.95" y2="-2.65" width="0.12" layer="51"/>
<smd name="1" x="-2.7004" y="0" dx="1.2146" dy="5.3103" layer="1"/>
<smd name="2" x="2.7004" y="0" dx="1.2146" dy="5.3103" layer="1"/>
<text x="0" y="3.6042" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-3.6042" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="INDC6350X200" urn="urn:adsk.eagle:footprint:3811097/1" library_version="58">
<description>CHIP, 6.3 X 5 X 2 mm body
&lt;p&gt;CHIP package with body size 6.3 X 5 X 2 mm&lt;/p&gt;</description>
<wire x1="3.25" y1="2.9692" x2="-3.25" y2="2.9692" width="0.12" layer="21"/>
<wire x1="3.25" y1="-2.9692" x2="-3.25" y2="-2.9692" width="0.12" layer="21"/>
<wire x1="3.25" y1="-2.65" x2="-3.25" y2="-2.65" width="0.12" layer="51"/>
<wire x1="-3.25" y1="-2.65" x2="-3.25" y2="2.65" width="0.12" layer="51"/>
<wire x1="-3.25" y1="2.65" x2="3.25" y2="2.65" width="0.12" layer="51"/>
<wire x1="3.25" y1="2.65" x2="3.25" y2="-2.65" width="0.12" layer="51"/>
<smd name="1" x="-3.0004" y="0" dx="1.2146" dy="5.3103" layer="1"/>
<smd name="2" x="3.0004" y="0" dx="1.2146" dy="5.3103" layer="1"/>
<text x="0" y="3.6042" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-3.6042" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="INDM4848X300" urn="urn:adsk.eagle:footprint:3837794/1" library_version="58">
<description>MOLDED BODY, 4.8 X 4.8 X 3 mm body
&lt;p&gt;MOLDED BODY package with body size 4.8 X 4.8 X 3 mm&lt;/p&gt;</description>
<wire x1="-2.5" y1="2.5" x2="2.5" y2="2.5" width="0.12" layer="21"/>
<wire x1="-2.5" y1="-2.5" x2="2.5" y2="-2.5" width="0.12" layer="21"/>
<wire x1="2.5" y1="-2.5" x2="-2.5" y2="-2.5" width="0.12" layer="51"/>
<wire x1="-2.5" y1="-2.5" x2="-2.5" y2="2.5" width="0.12" layer="51"/>
<wire x1="-2.5" y1="2.5" x2="2.5" y2="2.5" width="0.12" layer="51"/>
<wire x1="2.5" y1="2.5" x2="2.5" y2="-2.5" width="0.12" layer="51"/>
<smd name="1" x="-1.74" y="0" dx="2.5354" dy="4.2118" layer="1"/>
<smd name="2" x="1.74" y="0" dx="2.5354" dy="4.2118" layer="1"/>
<text x="0" y="3.135" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-3.135" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="INDM4440X300" urn="urn:adsk.eagle:footprint:3890490/1" library_version="58">
<description>MOLDED BODY, 4.45 X 4.06 X 3 mm body
&lt;p&gt;MOLDED BODY package with body size 4.45 X 4.06 X 3 mm&lt;/p&gt;</description>
<wire x1="-2.35" y1="2.3349" x2="2.35" y2="2.3349" width="0.12" layer="21"/>
<wire x1="-2.35" y1="-2.3349" x2="2.35" y2="-2.3349" width="0.12" layer="21"/>
<wire x1="2.35" y1="-2.155" x2="-2.35" y2="-2.155" width="0.12" layer="51"/>
<wire x1="-2.35" y1="-2.155" x2="-2.35" y2="2.155" width="0.12" layer="51"/>
<wire x1="-2.35" y1="2.155" x2="2.35" y2="2.155" width="0.12" layer="51"/>
<wire x1="2.35" y1="2.155" x2="2.35" y2="-2.155" width="0.12" layer="51"/>
<smd name="1" x="-2.0191" y="0" dx="1.674" dy="4.0418" layer="1"/>
<smd name="2" x="2.0191" y="0" dx="1.674" dy="4.0418" layer="1"/>
<text x="0" y="2.9699" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.9699" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="INDM7070X450" urn="urn:adsk.eagle:footprint:3890495/1" library_version="58">
<description>MOLDED BODY, 7 X 7 X 4.5 mm body
&lt;p&gt;MOLDED BODY package with body size 7 X 7 X 4.5 mm&lt;/p&gt;</description>
<wire x1="-3.5" y1="3.6699" x2="3.5" y2="3.6699" width="0.12" layer="21"/>
<wire x1="-3.5" y1="-3.6699" x2="3.5" y2="-3.6699" width="0.12" layer="21"/>
<wire x1="3.5" y1="-3.5" x2="-3.5" y2="-3.5" width="0.12" layer="51"/>
<wire x1="-3.5" y1="-3.5" x2="-3.5" y2="3.5" width="0.12" layer="51"/>
<wire x1="-3.5" y1="3.5" x2="3.5" y2="3.5" width="0.12" layer="51"/>
<wire x1="3.5" y1="3.5" x2="3.5" y2="-3.5" width="0.12" layer="51"/>
<smd name="1" x="-3.175" y="0" dx="1.7618" dy="6.7118" layer="1"/>
<smd name="2" x="3.175" y="0" dx="1.7618" dy="6.7118" layer="1"/>
<text x="0" y="4.3049" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-4.3049" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="INDM6363X500" urn="urn:adsk.eagle:footprint:3920874/1" library_version="58">
<description>MOLDED BODY, 6.3 X 6.3 X 5 mm body
&lt;p&gt;MOLDED BODY package with body size 6.3 X 6.3 X 5 mm&lt;/p&gt;</description>
<wire x1="-3.15" y1="3.3199" x2="3.15" y2="3.3199" width="0.12" layer="21"/>
<wire x1="-3.15" y1="-3.3199" x2="3.15" y2="-3.3199" width="0.12" layer="21"/>
<wire x1="3.15" y1="-3.15" x2="-3.15" y2="-3.15" width="0.12" layer="51"/>
<wire x1="-3.15" y1="-3.15" x2="-3.15" y2="3.15" width="0.12" layer="51"/>
<wire x1="-3.15" y1="3.15" x2="3.15" y2="3.15" width="0.12" layer="51"/>
<wire x1="3.15" y1="3.15" x2="3.15" y2="-3.15" width="0.12" layer="51"/>
<smd name="1" x="-2.475" y="0" dx="2.4618" dy="6.0118" layer="1"/>
<smd name="2" x="2.475" y="0" dx="2.4618" dy="6.0118" layer="1"/>
<text x="0" y="3.9549" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-3.9549" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="INDM238238X1016" urn="urn:adsk.eagle:footprint:3949007/1" library_version="58">
<description>MOLDED BODY, 23.88 X 23.88 X 10.16 mm body
&lt;p&gt;MOLDED BODY package with body size 23.88 X 23.88 X 10.16 mm&lt;/p&gt;</description>
<wire x1="-11.94" y1="12.1099" x2="11.94" y2="12.1099" width="0.12" layer="21"/>
<wire x1="-11.94" y1="-12.1099" x2="11.94" y2="-12.1099" width="0.12" layer="21"/>
<wire x1="11.94" y1="-11.94" x2="-11.94" y2="-11.94" width="0.12" layer="51"/>
<wire x1="-11.94" y1="-11.94" x2="-11.94" y2="11.94" width="0.12" layer="51"/>
<wire x1="-11.94" y1="11.94" x2="11.94" y2="11.94" width="0.12" layer="51"/>
<wire x1="11.94" y1="11.94" x2="11.94" y2="-11.94" width="0.12" layer="51"/>
<smd name="1" x="-11.035" y="0" dx="2.9218" dy="23.5918" layer="1"/>
<smd name="2" x="11.035" y="0" dx="2.9218" dy="23.5918" layer="1"/>
<text x="0" y="12.7449" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-12.7449" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="INDM8080X400" urn="urn:adsk.eagle:footprint:3950983/1" library_version="58">
<description>MOLDED BODY, 8 X 8 X 4 mm body
&lt;p&gt;MOLDED BODY package with body size 8 X 8 X 4 mm&lt;/p&gt;</description>
<wire x1="-4" y1="4.1699" x2="4" y2="4.1699" width="0.12" layer="21"/>
<wire x1="-4" y1="-4.1699" x2="4" y2="-4.1699" width="0.12" layer="21"/>
<wire x1="4" y1="-4" x2="-4" y2="-4" width="0.12" layer="51"/>
<wire x1="-4" y1="-4" x2="-4" y2="4" width="0.12" layer="51"/>
<wire x1="-4" y1="4" x2="4" y2="4" width="0.12" layer="51"/>
<wire x1="4" y1="4" x2="4" y2="-4" width="0.12" layer="51"/>
<smd name="1" x="-3.175" y="0" dx="2.7618" dy="7.7118" layer="1"/>
<smd name="2" x="3.175" y="0" dx="2.7618" dy="7.7118" layer="1"/>
<text x="0" y="4.8049" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-4.8049" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="INDM125125X650" urn="urn:adsk.eagle:footprint:3950989/1" library_version="58">
<description>MOLDED BODY, 12.5 X 12.5 X 6.5 mm body
&lt;p&gt;MOLDED BODY package with body size 12.5 X 12.5 X 6.5 mm&lt;/p&gt;</description>
<wire x1="-6.25" y1="6.4199" x2="6.25" y2="6.4199" width="0.12" layer="21"/>
<wire x1="-6.25" y1="-6.4199" x2="6.25" y2="-6.4199" width="0.12" layer="21"/>
<wire x1="6.25" y1="-6.25" x2="-6.25" y2="-6.25" width="0.12" layer="51"/>
<wire x1="-6.25" y1="-6.25" x2="-6.25" y2="6.25" width="0.12" layer="51"/>
<wire x1="-6.25" y1="6.25" x2="6.25" y2="6.25" width="0.12" layer="51"/>
<wire x1="6.25" y1="6.25" x2="6.25" y2="-6.25" width="0.12" layer="51"/>
<smd name="1" x="-5.125" y="0" dx="3.3618" dy="12.2118" layer="1"/>
<smd name="2" x="5.125" y="0" dx="3.3618" dy="12.2118" layer="1"/>
<text x="0" y="7.0549" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-7.0549" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC1220X107" urn="urn:adsk.eagle:footprint:3793123/1" library_version="58">
<description>CHIP, 1.25 X 2 X 1.07 mm body
&lt;p&gt;CHIP package with body size 1.25 X 2 X 1.07 mm&lt;/p&gt;</description>
<wire x1="0.725" y1="1.4217" x2="-0.725" y2="1.4217" width="0.12" layer="21"/>
<wire x1="0.725" y1="-1.4217" x2="-0.725" y2="-1.4217" width="0.12" layer="21"/>
<wire x1="0.725" y1="-1.1" x2="-0.725" y2="-1.1" width="0.12" layer="51"/>
<wire x1="-0.725" y1="-1.1" x2="-0.725" y2="1.1" width="0.12" layer="51"/>
<wire x1="-0.725" y1="1.1" x2="0.725" y2="1.1" width="0.12" layer="51"/>
<wire x1="0.725" y1="1.1" x2="0.725" y2="-1.1" width="0.12" layer="51"/>
<smd name="1" x="-0.552" y="0" dx="0.7614" dy="2.2153" layer="1"/>
<smd name="2" x="0.552" y="0" dx="0.7614" dy="2.2153" layer="1"/>
<text x="0" y="2.0567" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.0567" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC1632X168" urn="urn:adsk.eagle:footprint:3793133/1" library_version="58">
<description>CHIP, 1.6 X 3.2 X 1.68 mm body
&lt;p&gt;CHIP package with body size 1.6 X 3.2 X 1.68 mm&lt;/p&gt;</description>
<wire x1="0.9" y1="2.0217" x2="-0.9" y2="2.0217" width="0.12" layer="21"/>
<wire x1="0.9" y1="-2.0217" x2="-0.9" y2="-2.0217" width="0.12" layer="21"/>
<wire x1="0.9" y1="-1.7" x2="-0.9" y2="-1.7" width="0.12" layer="51"/>
<wire x1="-0.9" y1="-1.7" x2="-0.9" y2="1.7" width="0.12" layer="51"/>
<wire x1="-0.9" y1="1.7" x2="0.9" y2="1.7" width="0.12" layer="51"/>
<wire x1="0.9" y1="1.7" x2="0.9" y2="-1.7" width="0.12" layer="51"/>
<smd name="1" x="-0.786" y="0" dx="0.9434" dy="3.4153" layer="1"/>
<smd name="2" x="0.786" y="0" dx="0.9434" dy="3.4153" layer="1"/>
<text x="0" y="2.6567" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.6567" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC3215X168" urn="urn:adsk.eagle:footprint:3793146/1" library_version="58">
<description>CHIP, 3.2 X 1.5 X 1.68 mm body
&lt;p&gt;CHIP package with body size 3.2 X 1.5 X 1.68 mm&lt;/p&gt;</description>
<wire x1="1.7" y1="1.1286" x2="-1.7" y2="1.1286" width="0.12" layer="21"/>
<wire x1="1.7" y1="-1.1286" x2="-1.7" y2="-1.1286" width="0.12" layer="21"/>
<wire x1="1.7" y1="-0.8" x2="-1.7" y2="-0.8" width="0.12" layer="51"/>
<wire x1="-1.7" y1="-0.8" x2="-1.7" y2="0.8" width="0.12" layer="51"/>
<wire x1="-1.7" y1="0.8" x2="1.7" y2="0.8" width="0.12" layer="51"/>
<wire x1="1.7" y1="0.8" x2="1.7" y2="-0.8" width="0.12" layer="51"/>
<smd name="1" x="-1.4913" y="0" dx="1.1327" dy="1.6291" layer="1"/>
<smd name="2" x="1.4913" y="0" dx="1.1327" dy="1.6291" layer="1"/>
<text x="0" y="1.7636" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.7636" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC3225X168" urn="urn:adsk.eagle:footprint:3793150/1" library_version="58">
<description>CHIP, 3.2 X 2.5 X 1.68 mm body
&lt;p&gt;CHIP package with body size 3.2 X 2.5 X 1.68 mm&lt;/p&gt;</description>
<wire x1="1.7" y1="1.6717" x2="-1.7" y2="1.6717" width="0.12" layer="21"/>
<wire x1="1.7" y1="-1.6717" x2="-1.7" y2="-1.6717" width="0.12" layer="21"/>
<wire x1="1.7" y1="-1.35" x2="-1.7" y2="-1.35" width="0.12" layer="51"/>
<wire x1="-1.7" y1="-1.35" x2="-1.7" y2="1.35" width="0.12" layer="51"/>
<wire x1="-1.7" y1="1.35" x2="1.7" y2="1.35" width="0.12" layer="51"/>
<wire x1="1.7" y1="1.35" x2="1.7" y2="-1.35" width="0.12" layer="51"/>
<smd name="1" x="-1.4913" y="0" dx="1.1327" dy="2.7153" layer="1"/>
<smd name="2" x="1.4913" y="0" dx="1.1327" dy="2.7153" layer="1"/>
<text x="0" y="2.3067" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.3067" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC4520X168" urn="urn:adsk.eagle:footprint:3793156/1" library_version="58">
<description>CHIP, 4.5 X 2.03 X 1.68 mm body
&lt;p&gt;CHIP package with body size 4.5 X 2.03 X 1.68 mm&lt;/p&gt;</description>
<wire x1="2.375" y1="1.4602" x2="-2.375" y2="1.4602" width="0.12" layer="21"/>
<wire x1="2.375" y1="-1.4602" x2="-2.375" y2="-1.4602" width="0.12" layer="21"/>
<wire x1="2.375" y1="-1.14" x2="-2.375" y2="-1.14" width="0.12" layer="51"/>
<wire x1="-2.375" y1="-1.14" x2="-2.375" y2="1.14" width="0.12" layer="51"/>
<wire x1="-2.375" y1="1.14" x2="2.375" y2="1.14" width="0.12" layer="51"/>
<wire x1="2.375" y1="1.14" x2="2.375" y2="-1.14" width="0.12" layer="51"/>
<smd name="1" x="-2.1266" y="0" dx="1.2091" dy="2.2923" layer="1"/>
<smd name="2" x="2.1266" y="0" dx="1.2091" dy="2.2923" layer="1"/>
<text x="0" y="2.0952" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.0952" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC4532X218" urn="urn:adsk.eagle:footprint:3793162/1" library_version="58">
<description>CHIP, 4.5 X 3.2 X 2.18 mm body
&lt;p&gt;CHIP package with body size 4.5 X 3.2 X 2.18 mm&lt;/p&gt;</description>
<wire x1="2.375" y1="2.0217" x2="-2.375" y2="2.0217" width="0.12" layer="21"/>
<wire x1="2.375" y1="-2.0217" x2="-2.375" y2="-2.0217" width="0.12" layer="21"/>
<wire x1="2.375" y1="-1.7" x2="-2.375" y2="-1.7" width="0.12" layer="51"/>
<wire x1="-2.375" y1="-1.7" x2="-2.375" y2="1.7" width="0.12" layer="51"/>
<wire x1="-2.375" y1="1.7" x2="2.375" y2="1.7" width="0.12" layer="51"/>
<wire x1="2.375" y1="1.7" x2="2.375" y2="-1.7" width="0.12" layer="51"/>
<smd name="1" x="-2.1266" y="0" dx="1.2091" dy="3.4153" layer="1"/>
<smd name="2" x="2.1266" y="0" dx="1.2091" dy="3.4153" layer="1"/>
<text x="0" y="2.6567" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.6567" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC4564X218" urn="urn:adsk.eagle:footprint:3793169/1" library_version="58">
<description>CHIP, 4.5 X 6.4 X 2.18 mm body
&lt;p&gt;CHIP package with body size 4.5 X 6.4 X 2.18 mm&lt;/p&gt;</description>
<wire x1="2.375" y1="3.6452" x2="-2.375" y2="3.6452" width="0.12" layer="21"/>
<wire x1="2.375" y1="-3.6452" x2="-2.375" y2="-3.6452" width="0.12" layer="21"/>
<wire x1="2.375" y1="-3.325" x2="-2.375" y2="-3.325" width="0.12" layer="51"/>
<wire x1="-2.375" y1="-3.325" x2="-2.375" y2="3.325" width="0.12" layer="51"/>
<wire x1="-2.375" y1="3.325" x2="2.375" y2="3.325" width="0.12" layer="51"/>
<wire x1="2.375" y1="3.325" x2="2.375" y2="-3.325" width="0.12" layer="51"/>
<smd name="1" x="-2.1266" y="0" dx="1.2091" dy="6.6623" layer="1"/>
<smd name="2" x="2.1266" y="0" dx="1.2091" dy="6.6623" layer="1"/>
<text x="0" y="4.2802" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-4.2802" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC5650X218" urn="urn:adsk.eagle:footprint:3793172/1" library_version="58">
<description>CHIP, 5.64 X 5.08 X 2.18 mm body
&lt;p&gt;CHIP package with body size 5.64 X 5.08 X 2.18 mm&lt;/p&gt;</description>
<wire x1="2.895" y1="2.9852" x2="-2.895" y2="2.9852" width="0.12" layer="21"/>
<wire x1="2.895" y1="-2.9852" x2="-2.895" y2="-2.9852" width="0.12" layer="21"/>
<wire x1="2.895" y1="-2.665" x2="-2.895" y2="-2.665" width="0.12" layer="51"/>
<wire x1="-2.895" y1="-2.665" x2="-2.895" y2="2.665" width="0.12" layer="51"/>
<wire x1="-2.895" y1="2.665" x2="2.895" y2="2.665" width="0.12" layer="51"/>
<wire x1="2.895" y1="2.665" x2="2.895" y2="-2.665" width="0.12" layer="51"/>
<smd name="1" x="-2.6854" y="0" dx="1.1393" dy="5.3423" layer="1"/>
<smd name="2" x="2.6854" y="0" dx="1.1393" dy="5.3423" layer="1"/>
<text x="0" y="3.6202" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-3.6202" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC5563X218" urn="urn:adsk.eagle:footprint:3793175/1" library_version="58">
<description>CHIP, 5.59 X 6.35 X 2.18 mm body
&lt;p&gt;CHIP package with body size 5.59 X 6.35 X 2.18 mm&lt;/p&gt;</description>
<wire x1="2.92" y1="3.6202" x2="-2.92" y2="3.6202" width="0.12" layer="21"/>
<wire x1="2.92" y1="-3.6202" x2="-2.92" y2="-3.6202" width="0.12" layer="21"/>
<wire x1="2.92" y1="-3.3" x2="-2.92" y2="-3.3" width="0.12" layer="51"/>
<wire x1="-2.92" y1="-3.3" x2="-2.92" y2="3.3" width="0.12" layer="51"/>
<wire x1="-2.92" y1="3.3" x2="2.92" y2="3.3" width="0.12" layer="51"/>
<wire x1="2.92" y1="3.3" x2="2.92" y2="-3.3" width="0.12" layer="51"/>
<smd name="1" x="-2.6716" y="0" dx="1.2091" dy="6.6123" layer="1"/>
<smd name="2" x="2.6716" y="0" dx="1.2091" dy="6.6123" layer="1"/>
<text x="0" y="4.2552" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-4.2552" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC9198X218" urn="urn:adsk.eagle:footprint:3793179/1" library_version="58">
<description>CHIP, 9.14 X 9.82 X 2.18 mm body
&lt;p&gt;CHIP package with body size 9.14 X 9.82 X 2.18 mm&lt;/p&gt;</description>
<wire x1="4.76" y1="5.2799" x2="-4.76" y2="5.2799" width="0.12" layer="21"/>
<wire x1="4.76" y1="-5.2799" x2="-4.76" y2="-5.2799" width="0.12" layer="21"/>
<wire x1="4.76" y1="-4.91" x2="-4.76" y2="-4.91" width="0.12" layer="51"/>
<wire x1="-4.76" y1="-4.91" x2="-4.76" y2="4.91" width="0.12" layer="51"/>
<wire x1="-4.76" y1="4.91" x2="4.76" y2="4.91" width="0.12" layer="51"/>
<wire x1="4.76" y1="4.91" x2="4.76" y2="-4.91" width="0.12" layer="51"/>
<smd name="1" x="-4.4571" y="0" dx="1.314" dy="9.9318" layer="1"/>
<smd name="2" x="4.4571" y="0" dx="1.314" dy="9.9318" layer="1"/>
<text x="0" y="5.9149" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-5.9149" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="SOIC127P800X203-8" urn="urn:adsk.eagle:footprint:3852773/1" library_version="58">
<description>8-SOIC, 1.27 mm pitch, 8.005 mm span, 5.23 X 5.255 X 2.03 mm body
&lt;p&gt;8-pin SOIC package with 1.27 mm pitch, 8.005 mm span with body size 5.23 X 5.255 X 2.03 mm&lt;/p&gt;</description>
<circle x="-3.588" y="2.701" radius="0.25" width="0" layer="21"/>
<wire x1="-2.69" y1="2.451" x2="-2.69" y2="2.665" width="0.12" layer="21"/>
<wire x1="-2.69" y1="2.665" x2="2.69" y2="2.665" width="0.12" layer="21"/>
<wire x1="2.69" y1="2.665" x2="2.69" y2="2.451" width="0.12" layer="21"/>
<wire x1="-2.69" y1="-2.451" x2="-2.69" y2="-2.665" width="0.12" layer="21"/>
<wire x1="-2.69" y1="-2.665" x2="2.69" y2="-2.665" width="0.12" layer="21"/>
<wire x1="2.69" y1="-2.665" x2="2.69" y2="-2.451" width="0.12" layer="21"/>
<wire x1="2.69" y1="-2.665" x2="-2.69" y2="-2.665" width="0.12" layer="51"/>
<wire x1="-2.69" y1="-2.665" x2="-2.69" y2="2.665" width="0.12" layer="51"/>
<wire x1="-2.69" y1="2.665" x2="2.69" y2="2.665" width="0.12" layer="51"/>
<wire x1="2.69" y1="2.665" x2="2.69" y2="-2.665" width="0.12" layer="51"/>
<smd name="1" x="-3.6717" y="1.905" dx="1.6287" dy="0.584" layer="1"/>
<smd name="2" x="-3.6717" y="0.635" dx="1.6287" dy="0.584" layer="1"/>
<smd name="3" x="-3.6717" y="-0.635" dx="1.6287" dy="0.584" layer="1"/>
<smd name="4" x="-3.6717" y="-1.905" dx="1.6287" dy="0.584" layer="1"/>
<smd name="5" x="3.6717" y="-1.905" dx="1.6287" dy="0.584" layer="1"/>
<smd name="6" x="3.6717" y="-0.635" dx="1.6287" dy="0.584" layer="1"/>
<smd name="7" x="3.6717" y="0.635" dx="1.6287" dy="0.584" layer="1"/>
<smd name="8" x="3.6717" y="1.905" dx="1.6287" dy="0.584" layer="1"/>
<text x="0" y="3.586" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-3.3" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="OSCCC320X250X80" urn="urn:adsk.eagle:footprint:3849547/1" library_version="58">
<description>OSCILLATOR CORNERCONCAVE, 3.2 X 2.5 X 0.8 mm body
&lt;p&gt;OSCILLATOR CORNERCONCAVE package with body size 3.2 X 2.5 X 0.8 mm&lt;/p&gt;</description>
<circle x="-2.3599" y="-0.9" radius="0.25" width="0" layer="21"/>
<wire x1="-1.9159" y1="0.0401" x2="-1.9159" y2="-0.0401" width="0.12" layer="21"/>
<wire x1="1.9159" y1="0.0401" x2="1.9159" y2="-0.0401" width="0.12" layer="21"/>
<wire x1="-0.2901" y1="1.5659" x2="0.2901" y2="1.5659" width="0.12" layer="21"/>
<wire x1="-0.2901" y1="-1.5659" x2="0.2901" y2="-1.5659" width="0.12" layer="21"/>
<wire x1="1.65" y1="-1.3" x2="-1.65" y2="-1.3" width="0.12" layer="51"/>
<wire x1="-1.65" y1="-1.3" x2="-1.65" y2="1.3" width="0.12" layer="51"/>
<wire x1="-1.65" y1="1.3" x2="1.65" y2="1.3" width="0.12" layer="51"/>
<wire x1="1.65" y1="1.3" x2="1.65" y2="-1.3" width="0.12" layer="51"/>
<smd name="1" x="-1.2" y="-0.9" dx="1.3118" dy="1.2118" layer="1"/>
<smd name="2" x="1.2" y="-0.9" dx="1.3118" dy="1.2118" layer="1"/>
<smd name="3" x="1.2" y="0.9" dx="1.3118" dy="1.2118" layer="1"/>
<smd name="4" x="-1.2" y="0.9" dx="1.3118" dy="1.2118" layer="1"/>
<text x="0" y="2.2009" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.2009" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="SOT230P700X180-4" urn="urn:adsk.eagle:footprint:3834047/1" library_version="58">
<description>4-SOT223, 2.3 mm pitch, 7 mm span, 6.5 X 3.5 X 1.8 mm body
&lt;p&gt;4-pin SOT223 package with 2.3 mm pitch, 7 mm span with body size 6.5 X 3.5 X 1.8 mm&lt;/p&gt;</description>
<circle x="-2.9276" y="3.2847" radius="0.25" width="0" layer="21"/>
<wire x1="-1.85" y1="3.0347" x2="-1.85" y2="3.35" width="0.12" layer="21"/>
<wire x1="-1.85" y1="3.35" x2="1.85" y2="3.35" width="0.12" layer="21"/>
<wire x1="1.85" y1="3.35" x2="1.85" y2="1.8847" width="0.12" layer="21"/>
<wire x1="-1.85" y1="-3.0347" x2="-1.85" y2="-3.35" width="0.12" layer="21"/>
<wire x1="-1.85" y1="-3.35" x2="1.85" y2="-3.35" width="0.12" layer="21"/>
<wire x1="1.85" y1="-3.35" x2="1.85" y2="-1.8847" width="0.12" layer="21"/>
<wire x1="1.85" y1="-3.35" x2="-1.85" y2="-3.35" width="0.12" layer="51"/>
<wire x1="-1.85" y1="-3.35" x2="-1.85" y2="3.35" width="0.12" layer="51"/>
<wire x1="-1.85" y1="3.35" x2="1.85" y2="3.35" width="0.12" layer="51"/>
<wire x1="1.85" y1="3.35" x2="1.85" y2="-3.35" width="0.12" layer="51"/>
<smd name="1" x="-2.9226" y="2.3" dx="2.1651" dy="0.9615" layer="1"/>
<smd name="2" x="-2.9226" y="0" dx="2.1651" dy="0.9615" layer="1"/>
<smd name="3" x="-2.9226" y="-2.3" dx="2.1651" dy="0.9615" layer="1"/>
<smd name="4" x="2.9226" y="0" dx="2.1651" dy="3.2615" layer="1"/>
<text x="0" y="4.1697" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-3.985" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="TO508P1530X456-3" urn="urn:adsk.eagle:footprint:3921296/1" library_version="58">
<description>3-TO, DPAK, 5.08 mm pitch, 15.3 mm span, 10.35 X 8.8 X 4.56 mm body
&lt;p&gt;3-pin TO, DPAK package with 5.08 mm pitch, 15.3 mm span with body size 10.35 X 8.8 X 4.56 mm&lt;/p&gt;</description>
<circle x="-6.6562" y="3.7736" radius="0.25" width="0" layer="21"/>
<wire x1="6.165" y1="4.6392" x2="6.165" y2="5.275" width="0.12" layer="21"/>
<wire x1="6.165" y1="5.275" x2="-2.835" y2="5.275" width="0.12" layer="21"/>
<wire x1="-2.835" y1="5.275" x2="-2.835" y2="-5.275" width="0.12" layer="21"/>
<wire x1="-2.835" y1="-5.275" x2="6.165" y2="-5.275" width="0.12" layer="21"/>
<wire x1="6.165" y1="-5.275" x2="6.165" y2="-4.6392" width="0.12" layer="21"/>
<wire x1="6.165" y1="-5.275" x2="-2.835" y2="-5.275" width="0.12" layer="51"/>
<wire x1="-2.835" y1="-5.275" x2="-2.835" y2="5.275" width="0.12" layer="51"/>
<wire x1="-2.835" y1="5.275" x2="6.165" y2="5.275" width="0.12" layer="51"/>
<wire x1="6.165" y1="5.275" x2="6.165" y2="-5.275" width="0.12" layer="51"/>
<smd name="1" x="-6.6562" y="2.54" dx="2.998" dy="1.4591" layer="1"/>
<smd name="2" x="-6.6562" y="-2.54" dx="2.998" dy="1.4591" layer="1"/>
<smd name="3" x="4.1562" y="0" dx="7.998" dy="8.7703" layer="1"/>
<text x="0" y="5.91" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-5.91" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="SOP65P400X130-8" urn="urn:adsk.eagle:footprint:3986193/1" library_version="58">
<description>8-SOP, 0.65 mm pitch, 4 mm span, 2.95 X 2.8 X 1.3 mm body
&lt;p&gt;8-pin SOP package with 0.65 mm pitch, 4 mm span with body size 2.95 X 2.8 X 1.3 mm&lt;/p&gt;</description>
<circle x="-1.9656" y="1.6775" radius="0.25" width="0" layer="21"/>
<wire x1="-1.45" y1="1.4275" x2="-1.45" y2="1.575" width="0.12" layer="21"/>
<wire x1="-1.45" y1="1.575" x2="1.45" y2="1.575" width="0.12" layer="21"/>
<wire x1="1.45" y1="1.575" x2="1.45" y2="1.4275" width="0.12" layer="21"/>
<wire x1="-1.45" y1="-1.4275" x2="-1.45" y2="-1.575" width="0.12" layer="21"/>
<wire x1="-1.45" y1="-1.575" x2="1.45" y2="-1.575" width="0.12" layer="21"/>
<wire x1="1.45" y1="-1.575" x2="1.45" y2="-1.4275" width="0.12" layer="21"/>
<wire x1="1.45" y1="-1.575" x2="-1.45" y2="-1.575" width="0.12" layer="51"/>
<wire x1="-1.45" y1="-1.575" x2="-1.45" y2="1.575" width="0.12" layer="51"/>
<wire x1="-1.45" y1="1.575" x2="1.45" y2="1.575" width="0.12" layer="51"/>
<wire x1="1.45" y1="1.575" x2="1.45" y2="-1.575" width="0.12" layer="51"/>
<smd name="1" x="-1.8692" y="0.975" dx="1.224" dy="0.3971" layer="1"/>
<smd name="2" x="-1.8692" y="0.325" dx="1.224" dy="0.3971" layer="1"/>
<smd name="3" x="-1.8692" y="-0.325" dx="1.224" dy="0.3971" layer="1"/>
<smd name="4" x="-1.8692" y="-0.975" dx="1.224" dy="0.3971" layer="1"/>
<smd name="5" x="1.8692" y="-0.975" dx="1.224" dy="0.3971" layer="1"/>
<smd name="6" x="1.8692" y="-0.325" dx="1.224" dy="0.3971" layer="1"/>
<smd name="7" x="1.8692" y="0.325" dx="1.224" dy="0.3971" layer="1"/>
<smd name="8" x="1.8692" y="0.975" dx="1.224" dy="0.3971" layer="1"/>
<text x="0" y="2.5625" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.21" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="HDRRA4W64P254_1X4_1016X254X254B" urn="urn:adsk.eagle:footprint:15597976/1" library_version="58">
<description>Single-row, 4-pin Pin Header (Male) Right Angle, 2.54 mm (0.10 in) col pitch, 5.84 mm mating length, 10.16 X 2.54 X 2.54 mm body
&lt;p&gt;Single-row (1X4), 4-pin Pin Header (Male) Right Angle package with 2.54 mm (0.10 in) col pitch, 0.64 mm lead width, 2.29 mm tail length and 5.84 mm mating length with body size 10.16 X 2.54 X 2.54 mm, pin pattern - clockwise from top left&lt;/p&gt;</description>
<circle x="-1.3565" y="0" radius="0.25" width="0" layer="21"/>
<wire x1="8.89" y1="-4.06" x2="-1.27" y2="-4.06" width="0.12" layer="21"/>
<wire x1="-1.27" y1="-4.06" x2="-1.27" y2="-1.52" width="0.12" layer="21"/>
<wire x1="-1.27" y1="-1.52" x2="8.89" y2="-1.52" width="0.12" layer="21"/>
<wire x1="8.89" y1="-1.52" x2="8.89" y2="-4.06" width="0.12" layer="21"/>
<wire x1="0" y1="-4.12" x2="0" y2="-9.96" width="0.12" layer="21"/>
<wire x1="2.54" y1="-4.12" x2="2.54" y2="-9.96" width="0.12" layer="21"/>
<wire x1="5.08" y1="-4.12" x2="5.08" y2="-9.96" width="0.12" layer="21"/>
<wire x1="7.62" y1="-4.12" x2="7.62" y2="-9.96" width="0.12" layer="21"/>
<wire x1="8.89" y1="-4.06" x2="-1.27" y2="-4.06" width="0.12" layer="51"/>
<wire x1="-1.27" y1="-4.06" x2="-1.27" y2="-1.52" width="0.12" layer="51"/>
<wire x1="-1.27" y1="-1.52" x2="8.89" y2="-1.52" width="0.12" layer="51"/>
<wire x1="8.89" y1="-1.52" x2="8.89" y2="-4.06" width="0.12" layer="51"/>
<pad name="1" x="0" y="0" drill="1.1051" diameter="1.7051"/>
<pad name="2" x="2.54" y="0" drill="1.1051" diameter="1.7051"/>
<pad name="3" x="5.08" y="0" drill="1.1051" diameter="1.7051"/>
<pad name="4" x="7.62" y="0" drill="1.1051" diameter="1.7051"/>
<text x="0" y="1.4875" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-10.595" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="HDRV4W64P254_1X4_1016X508X1829B" urn="urn:adsk.eagle:footprint:15737104/1" library_version="58">
<description>Single-row, 4-pin Pin Header (Male) Straight, 2.54 mm (0.10 in) col pitch, 15.75 mm mating length, 10.16 X 5.08 X 18.29 mm body
&lt;p&gt;Single-row (1X4), 4-pin Pin Header (Male) Straight package with 2.54 mm (0.10 in) col pitch, 0.64 mm lead width, 2.79 mm tail length and 15.75 mm mating length with overall size 10.16 X 5.08 X 18.29 mm, pin pattern - clockwise from top left&lt;/p&gt;</description>
<circle x="0" y="3.044" radius="0.25" width="0" layer="21"/>
<wire x1="8.89" y1="-2.54" x2="-1.27" y2="-2.54" width="0.12" layer="21"/>
<wire x1="-1.27" y1="-2.54" x2="-1.27" y2="2.54" width="0.12" layer="21"/>
<wire x1="-1.27" y1="2.54" x2="8.89" y2="2.54" width="0.12" layer="21"/>
<wire x1="8.89" y1="2.54" x2="8.89" y2="-2.54" width="0.12" layer="21"/>
<wire x1="8.89" y1="-2.54" x2="-1.27" y2="-2.54" width="0.12" layer="51"/>
<wire x1="-1.27" y1="-2.54" x2="-1.27" y2="2.54" width="0.12" layer="51"/>
<wire x1="-1.27" y1="2.54" x2="8.89" y2="2.54" width="0.12" layer="51"/>
<wire x1="8.89" y1="2.54" x2="8.89" y2="-2.54" width="0.12" layer="51"/>
<pad name="1" x="0" y="0" drill="1.1051" diameter="1.7051"/>
<pad name="2" x="2.54" y="0" drill="1.1051" diameter="1.7051"/>
<pad name="3" x="5.08" y="0" drill="1.1051" diameter="1.7051"/>
<pad name="4" x="7.62" y="0" drill="1.1051" diameter="1.7051"/>
<text x="0" y="3.929" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-3.175" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="QFN65P700X700X80-33" urn="urn:adsk.eagle:footprint:3837102/2" library_version="76">
<description>32-QFN, 0.65 mm pitch, 7 X 7 X 0.8 mm body
&lt;p&gt;32-pin QFN package with 0.65 mm pitch with body size 7 X 7 X 0.8 mm&lt;/p&gt;</description>
<circle x="-4.054" y="2.936" radius="0.25" width="0" layer="21"/>
<wire x1="-3.55" y1="2.686" x2="-3.55" y2="3.55" width="0.12" layer="21"/>
<wire x1="-3.55" y1="3.55" x2="-2.686" y2="3.55" width="0.12" layer="21"/>
<wire x1="3.55" y1="2.686" x2="3.55" y2="3.55" width="0.12" layer="21"/>
<wire x1="3.55" y1="3.55" x2="2.686" y2="3.55" width="0.12" layer="21"/>
<wire x1="3.55" y1="-2.686" x2="3.55" y2="-3.55" width="0.12" layer="21"/>
<wire x1="3.55" y1="-3.55" x2="2.686" y2="-3.55" width="0.12" layer="21"/>
<wire x1="-3.55" y1="-2.686" x2="-3.55" y2="-3.55" width="0.12" layer="21"/>
<wire x1="-3.55" y1="-3.55" x2="-2.686" y2="-3.55" width="0.12" layer="21"/>
<wire x1="3.55" y1="-3.55" x2="-3.55" y2="-3.55" width="0.12" layer="51"/>
<wire x1="-3.55" y1="-3.55" x2="-3.55" y2="3.55" width="0.12" layer="51"/>
<wire x1="-3.55" y1="3.55" x2="3.55" y2="3.55" width="0.12" layer="51"/>
<wire x1="3.55" y1="3.55" x2="3.55" y2="-3.55" width="0.12" layer="51"/>
<smd name="1" x="-3.3596" y="2.275" dx="1.01" dy="0.314" layer="1" roundness="100"/>
<smd name="2" x="-3.3596" y="1.625" dx="1.01" dy="0.314" layer="1" roundness="100"/>
<smd name="3" x="-3.3596" y="0.975" dx="1.01" dy="0.314" layer="1" roundness="100"/>
<smd name="4" x="-3.3596" y="0.325" dx="1.01" dy="0.314" layer="1" roundness="100"/>
<smd name="5" x="-3.3596" y="-0.325" dx="1.01" dy="0.314" layer="1" roundness="100"/>
<smd name="6" x="-3.3596" y="-0.975" dx="1.01" dy="0.314" layer="1" roundness="100"/>
<smd name="7" x="-3.3596" y="-1.625" dx="1.01" dy="0.314" layer="1" roundness="100"/>
<smd name="8" x="-3.3596" y="-2.275" dx="1.01" dy="0.314" layer="1" roundness="100"/>
<smd name="9" x="-2.275" y="-3.3596" dx="1.01" dy="0.314" layer="1" roundness="100" rot="R90"/>
<smd name="10" x="-1.625" y="-3.3596" dx="1.01" dy="0.314" layer="1" roundness="100" rot="R90"/>
<smd name="11" x="-0.975" y="-3.3596" dx="1.01" dy="0.314" layer="1" roundness="100" rot="R90"/>
<smd name="12" x="-0.325" y="-3.3596" dx="1.01" dy="0.314" layer="1" roundness="100" rot="R90"/>
<smd name="13" x="0.325" y="-3.3596" dx="1.01" dy="0.314" layer="1" roundness="100" rot="R90"/>
<smd name="14" x="0.975" y="-3.3596" dx="1.01" dy="0.314" layer="1" roundness="100" rot="R90"/>
<smd name="15" x="1.625" y="-3.3596" dx="1.01" dy="0.314" layer="1" roundness="100" rot="R90"/>
<smd name="16" x="2.275" y="-3.3596" dx="1.01" dy="0.314" layer="1" roundness="100" rot="R90"/>
<smd name="17" x="3.3596" y="-2.275" dx="1.01" dy="0.314" layer="1" roundness="100"/>
<smd name="18" x="3.3596" y="-1.625" dx="1.01" dy="0.314" layer="1" roundness="100"/>
<smd name="19" x="3.3596" y="-0.975" dx="1.01" dy="0.314" layer="1" roundness="100"/>
<smd name="20" x="3.3596" y="-0.325" dx="1.01" dy="0.314" layer="1" roundness="100"/>
<smd name="21" x="3.3596" y="0.325" dx="1.01" dy="0.314" layer="1" roundness="100"/>
<smd name="22" x="3.3596" y="0.975" dx="1.01" dy="0.314" layer="1" roundness="100"/>
<smd name="23" x="3.3596" y="1.625" dx="1.01" dy="0.314" layer="1" roundness="100"/>
<smd name="24" x="3.3596" y="2.275" dx="1.01" dy="0.314" layer="1" roundness="100"/>
<smd name="25" x="2.275" y="3.3596" dx="1.01" dy="0.314" layer="1" roundness="100" rot="R90"/>
<smd name="26" x="1.625" y="3.3596" dx="1.01" dy="0.314" layer="1" roundness="100" rot="R90"/>
<smd name="27" x="0.975" y="3.3596" dx="1.01" dy="0.314" layer="1" roundness="100" rot="R90"/>
<smd name="28" x="0.325" y="3.3596" dx="1.01" dy="0.314" layer="1" roundness="100" rot="R90"/>
<smd name="29" x="-0.325" y="3.3596" dx="1.01" dy="0.314" layer="1" roundness="100" rot="R90"/>
<smd name="30" x="-0.975" y="3.3596" dx="1.01" dy="0.314" layer="1" roundness="100" rot="R90"/>
<smd name="31" x="-1.625" y="3.3596" dx="1.01" dy="0.314" layer="1" roundness="100" rot="R90"/>
<smd name="32" x="-2.275" y="3.3596" dx="1.01" dy="0.314" layer="1" roundness="100" rot="R90"/>
<smd name="33" x="0" y="0" dx="4.85" dy="4.85" layer="1" cream="no"/>
<text x="0" y="4.4996" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-4.4996" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
<rectangle x1="-2.2" y1="0.2" x2="-0.2" y2="2.2" layer="31"/>
<rectangle x1="0.225" y1="0.2" x2="2.225" y2="2.2" layer="31"/>
<rectangle x1="0.225" y1="-2.225" x2="2.225" y2="-0.225" layer="31"/>
<rectangle x1="-2.2" y1="-2.225" x2="-0.2" y2="-0.225" layer="31"/>
</package>
<package name="SOT95P280X135-3" urn="urn:adsk.eagle:footprint:3809968/2" library_version="76">
<description>3-SOT23, 0.95 mm pitch, 2.8 mm span, 2.9 X 1.6 X 1.35 mm body
&lt;p&gt;3-pin SOT23 package with 0.95 mm pitch, 2.8 mm span with body size 2.9 X 1.6 X 1.35 mm&lt;/p&gt;</description>
<circle x="-1.3538" y="1.7786" radius="0.25" width="0" layer="21"/>
<wire x1="-0.85" y1="1.5886" x2="0.85" y2="1.5886" width="0.12" layer="21"/>
<wire x1="0.85" y1="1.5886" x2="0.85" y2="0.5786" width="0.12" layer="21"/>
<wire x1="-0.85" y1="-1.5886" x2="0.85" y2="-1.5886" width="0.12" layer="21"/>
<wire x1="0.85" y1="-1.5886" x2="0.85" y2="-0.5786" width="0.12" layer="21"/>
<wire x1="0.85" y1="-1.5" x2="-0.85" y2="-1.5" width="0.12" layer="51"/>
<wire x1="-0.85" y1="-1.5" x2="-0.85" y2="1.5" width="0.12" layer="51"/>
<wire x1="-0.85" y1="1.5" x2="0.85" y2="1.5" width="0.12" layer="51"/>
<wire x1="0.85" y1="1.5" x2="0.85" y2="-1.5" width="0.12" layer="51"/>
<smd name="A" x="-1.2644" y="0.95" dx="1.1864" dy="0.6492" layer="1"/>
<smd name="NC" x="-1.2644" y="-0.95" dx="1.1864" dy="0.6492" layer="1"/>
<smd name="K" x="1.2644" y="0" dx="1.1864" dy="0.6492" layer="1"/>
<text x="0" y="2.6636" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.2236" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="SOIC127P600X175-8" urn="urn:adsk.eagle:footprint:3833981/3" library_version="63">
<description>8-SOIC, 1.27 mm pitch, 6 mm span, 4.9 X 3.9 X 1.75 mm body
&lt;p&gt;8-pin SOIC package with 1.27 mm pitch, 6 mm span with body size 4.9 X 3.9 X 1.75 mm&lt;/p&gt;</description>
<circle x="-2.7288" y="2.7086" radius="0.25" width="0" layer="21"/>
<wire x1="-2" y1="2.5186" x2="2" y2="2.5186" width="0.12" layer="21"/>
<wire x1="-2" y1="-2.5186" x2="2" y2="-2.5186" width="0.12" layer="21"/>
<wire x1="2" y1="-2.5" x2="-2" y2="-2.5" width="0.12" layer="51"/>
<wire x1="-2" y1="-2.5" x2="-2" y2="2.5" width="0.12" layer="51"/>
<wire x1="-2" y1="2.5" x2="2" y2="2.5" width="0.12" layer="51"/>
<wire x1="2" y1="2.5" x2="2" y2="-2.5" width="0.12" layer="51"/>
<smd name="1" x="-2.4734" y="1.905" dx="1.9685" dy="0.5991" layer="1"/>
<smd name="2" x="-2.4734" y="0.635" dx="1.9685" dy="0.5991" layer="1"/>
<smd name="3" x="-2.4734" y="-0.635" dx="1.9685" dy="0.5991" layer="1"/>
<smd name="4" x="-2.4734" y="-1.905" dx="1.9685" dy="0.5991" layer="1"/>
<smd name="5" x="2.4734" y="-1.905" dx="1.9685" dy="0.5991" layer="1"/>
<smd name="6" x="2.4734" y="-0.635" dx="1.9685" dy="0.5991" layer="1"/>
<smd name="7" x="2.4734" y="0.635" dx="1.9685" dy="0.5991" layer="1"/>
<smd name="8" x="2.4734" y="1.905" dx="1.9685" dy="0.5991" layer="1"/>
<text x="0" y="3.5936" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-3.1536" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CONNECTOR_SMD_4PIN" urn="urn:adsk.eagle:footprint:21311884/1" library_version="65" library_locally_modified="yes">
<wire x1="-5.209" y1="1.155" x2="5.209" y2="1.155" width="0.2032" layer="21"/>
<wire x1="5.209" y1="1.155" x2="5.209" y2="-1.155" width="0.2032" layer="21"/>
<wire x1="5.209" y1="-1.155" x2="-5.209" y2="-1.155" width="0.2032" layer="21"/>
<wire x1="-5.209" y1="-1.155" x2="-5.209" y2="1.155" width="0.2032" layer="21"/>
<wire x1="5.715" y1="0.9525" x2="5.715" y2="-0.9525" width="0.127" layer="21"/>
<wire x1="3.81" y1="1.5875" x2="5.715" y2="1.5875" width="0.127" layer="51"/>
<wire x1="5.715" y1="1.5875" x2="5.715" y2="0" width="0.127" layer="51"/>
<wire x1="-5.209" y1="1.155" x2="5.209" y2="1.155" width="0.2032" layer="51"/>
<wire x1="5.209" y1="1.155" x2="5.209" y2="-1.155" width="0.2032" layer="51"/>
<wire x1="5.209" y1="-1.155" x2="-5.209" y2="-1.155" width="0.2032" layer="51"/>
<wire x1="-5.209" y1="-1.155" x2="-5.209" y2="1.155" width="0.2032" layer="51"/>
<rectangle x1="-4.16" y1="-0.35" x2="-3.46" y2="0.35" layer="51"/>
<rectangle x1="-1.62" y1="-0.35" x2="-0.92" y2="0.35" layer="51"/>
<rectangle x1="0.92" y1="-0.35" x2="1.62" y2="0.35" layer="51"/>
<rectangle x1="3.46" y1="-0.35" x2="4.16" y2="0.35" layer="51"/>
<smd name="P$1" x="3.81" y="0" dx="1.27" dy="1.27" layer="1" roundness="100" rot="R270" cream="no"/>
<smd name="P$2" x="1.27" y="0" dx="1.27" dy="1.27" layer="1" roundness="100" rot="R270" cream="no"/>
<smd name="P$3" x="-1.27" y="0" dx="1.27" dy="1.27" layer="1" roundness="100" rot="R270" cream="no"/>
<smd name="P$4" x="-3.81" y="0" dx="1.27" dy="1.27" layer="1" roundness="100" rot="R270" cream="no"/>
</package>
<package name="MOLEX_5031590800_SD" urn="urn:adsk.eagle:footprint:21515988/1" library_version="67">
<pad name="P$1" x="0" y="0" drill="0.7"/>
<pad name="P$2" x="1.5" y="2" drill="0.7"/>
<pad name="P$3" x="3" y="0" drill="0.7"/>
<pad name="P$4" x="4.5" y="2" drill="0.7"/>
<pad name="P$5" x="6" y="0" drill="0.7"/>
<pad name="P$6" x="7.2" y="2" drill="0.7"/>
<pad name="P$7" x="9" y="0" drill="0.7"/>
<pad name="P$8" x="10.5" y="2" drill="0.7"/>
<wire x1="-2.75" y1="-1.4" x2="-2.75" y2="5.6" width="0.127" layer="21"/>
<wire x1="-2.75" y1="5.6" x2="12.9" y2="5.6" width="0.127" layer="21"/>
<wire x1="12.9" y1="5.6" x2="12.9" y2="-1.4" width="0.127" layer="21"/>
<wire x1="12.9" y1="-1.4" x2="-2.75" y2="-1.4" width="0.127" layer="21"/>
<wire x1="-2.75" y1="-1.4" x2="12.9" y2="-1.4" width="0.127" layer="51"/>
<wire x1="12.9" y1="-1.4" x2="12.9" y2="5.6" width="0.127" layer="51"/>
<wire x1="12.9" y1="5.6" x2="-2.75" y2="5.6" width="0.127" layer="51"/>
<wire x1="-2.75" y1="5.6" x2="-2.75" y2="-1.4" width="0.127" layer="51"/>
<text x="-2.54" y="-2.54" size="0.6096" layer="25" font="vector">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="0.6096" layer="27" font="vector">&gt;VALUE</text>
<hole x="-1.65" y="3.8" drill="0.8"/>
</package>
<package name="DO214AA_SMB" urn="urn:adsk.eagle:footprint:15617778/3" library_version="70">
<description>&lt;b&gt;DIODE&lt;/b&gt;</description>
<smd name="A" x="-2.15" y="0" dx="2.5" dy="2.3" layer="1" rot="R180"/>
<smd name="K" x="2.15" y="0" dx="2.5" dy="2.3" layer="1" rot="R180"/>
<wire x1="-2.29" y1="1.97" x2="2.29" y2="1.97" width="0.127" layer="21"/>
<wire x1="-2.29" y1="-1.97" x2="2.29" y2="-1.97" width="0.127" layer="21"/>
<wire x1="-2.29" y1="1.97" x2="-2.29" y2="1.32" width="0.127" layer="21"/>
<wire x1="-2.29" y1="-1.97" x2="-2.29" y2="-1.32" width="0.127" layer="21"/>
<wire x1="2.29" y1="1.97" x2="2.29" y2="1.32" width="0.127" layer="21"/>
<wire x1="2.29" y1="-1.97" x2="2.29" y2="-1.32" width="0.127" layer="21"/>
<wire x1="2.7" y1="1.6" x2="2.8" y2="1.6" width="0.127" layer="21"/>
<wire x1="-2.29" y1="1.97" x2="2.29" y2="1.97" width="0.127" layer="51"/>
<wire x1="-2.29" y1="-1.97" x2="2.29" y2="-1.97" width="0.127" layer="51"/>
<wire x1="-2.29" y1="1.97" x2="-2.29" y2="1.32" width="0.127" layer="51"/>
<wire x1="-2.29" y1="-1.97" x2="-2.29" y2="-1.32" width="0.127" layer="51"/>
<wire x1="2.29" y1="1.97" x2="2.29" y2="1.32" width="0.127" layer="51"/>
<wire x1="2.29" y1="-1.97" x2="2.29" y2="-1.32" width="0.127" layer="51"/>
<wire x1="2.7" y1="1.6" x2="2.8" y2="1.6" width="0.127" layer="51"/>
<circle x="2.7" y="1.6" radius="0.1" width="0.127" layer="21"/>
<circle x="2.7" y="1.6" radius="0.14141875" width="0.127" layer="21"/>
<circle x="2.7" y="1.6" radius="0.1" width="0.127" layer="51"/>
<circle x="2.7" y="1.6" radius="0.14141875" width="0.127" layer="51"/>
<text x="-3" y="2.2" size="1.27" layer="25">&gt;NAME</text>
<text x="-3" y="-3.5" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="FIDUCIAL_0.5MM" urn="urn:adsk.eagle:footprint:8413676/1" library_version="74">
<wire x1="0" y1="1" x2="0" y2="-1" width="0.0508" layer="51"/>
<wire x1="-1" y1="0" x2="1" y2="0" width="0.0508" layer="51"/>
<circle x="0" y="0" radius="0.5" width="0" layer="1"/>
<circle x="0" y="0" radius="0.889" width="0" layer="29"/>
<circle x="0" y="0" radius="1" width="0.0508" layer="51"/>
<circle x="0" y="0" radius="1.0307" width="0" layer="41"/>
<circle x="0" y="0" radius="0.5" width="0.0508" layer="51"/>
</package>
<package name="LEDC1608X55" urn="urn:adsk.eagle:footprint:3905181/2" library_version="76">
<description>CHIP, 1.6 X 0.85 X 0.55 mm body
&lt;p&gt;CHIP package with body size 1.6 X 0.85 X 0.55 mm&lt;/p&gt;</description>
<wire x1="1.4319" y1="0.5496" x2="1.4319" y2="-0.5496" width="0.12" layer="21"/>
<wire x1="0.875" y1="-0.475" x2="-0.875" y2="-0.475" width="0.12" layer="51"/>
<wire x1="-0.875" y1="-0.475" x2="-0.875" y2="0.475" width="0.12" layer="51"/>
<wire x1="-0.875" y1="0.475" x2="0.875" y2="0.475" width="0.12" layer="51"/>
<wire x1="0.875" y1="0.475" x2="0.875" y2="-0.475" width="0.12" layer="51"/>
<smd name="A" x="-0.7996" y="0" dx="0.8709" dy="0.9791" layer="1"/>
<smd name="K" x="0.7996" y="0" dx="0.8709" dy="0.9791" layer="1"/>
<text x="0" y="1.4386" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.4386" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="LEDC2012X70" urn="urn:adsk.eagle:footprint:3905185/2" library_version="76">
<description>CHIP, 2 X 1.25 X 0.7 mm body
&lt;p&gt;CHIP package with body size 2 X 1.25 X 0.7 mm&lt;/p&gt;</description>
<wire x1="1.7173" y1="0.7496" x2="1.7173" y2="-0.7496" width="0.12" layer="21"/>
<wire x1="1.1" y1="-0.675" x2="-1.1" y2="-0.675" width="0.12" layer="51"/>
<wire x1="-1.1" y1="-0.675" x2="-1.1" y2="0.675" width="0.12" layer="51"/>
<wire x1="-1.1" y1="0.675" x2="1.1" y2="0.675" width="0.12" layer="51"/>
<wire x1="1.1" y1="0.675" x2="1.1" y2="-0.675" width="0.12" layer="51"/>
<smd name="A" x="-0.94" y="0" dx="1.0354" dy="1.3791" layer="1"/>
<smd name="K" x="0.94" y="0" dx="1.0354" dy="1.3791" layer="1"/>
<text x="0" y="1.6386" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.6386" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="DIOM5226X292" urn="urn:adsk.eagle:footprint:3793195/2" library_version="76">
<description>MOLDED BODY, 5.265 X 2.6 X 2.92 mm body
&lt;p&gt;MOLDED BODY package with body size 5.265 X 2.6 X 2.92 mm&lt;/p&gt;</description>
<wire x1="2.8" y1="1.45" x2="-2.8" y2="1.45" width="0.12" layer="21"/>
<wire x1="-3.6186" y1="1.4614" x2="-3.6186" y2="-1.4614" width="0.12" layer="21"/>
<wire x1="-2.8" y1="-1.45" x2="2.8" y2="-1.45" width="0.12" layer="21"/>
<wire x1="2.8" y1="-1.45" x2="-2.8" y2="-1.45" width="0.12" layer="51"/>
<wire x1="-2.8" y1="-1.45" x2="-2.8" y2="1.45" width="0.12" layer="51"/>
<wire x1="-2.8" y1="1.45" x2="2.8" y2="1.45" width="0.12" layer="51"/>
<wire x1="2.8" y1="1.45" x2="2.8" y2="-1.45" width="0.12" layer="51"/>
<smd name="K" x="-2.164" y="0" dx="2.2812" dy="1.5153" layer="1"/>
<smd name="A" x="2.164" y="0" dx="2.2812" dy="1.5153" layer="1"/>
<text x="0" y="2.085" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.085" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="DIOM5436X265" urn="urn:adsk.eagle:footprint:3793203/2" library_version="76">
<description>MOLDED BODY, 5.4 X 3.6 X 2.65 mm body
&lt;p&gt;MOLDED BODY package with body size 5.4 X 3.6 X 2.65 mm&lt;/p&gt;</description>
<wire x1="-2.795" y1="1.95" x2="2.795" y2="1.95" width="0.12" layer="21"/>
<wire x1="-3.6171" y1="1.95" x2="-3.6171" y2="-1.95" width="0.12" layer="21"/>
<wire x1="2.795" y1="-1.95" x2="-2.795" y2="-1.95" width="0.12" layer="21"/>
<wire x1="2.795" y1="-1.95" x2="-2.795" y2="-1.95" width="0.12" layer="51"/>
<wire x1="-2.795" y1="-1.95" x2="-2.795" y2="1.95" width="0.12" layer="51"/>
<wire x1="-2.795" y1="1.95" x2="2.795" y2="1.95" width="0.12" layer="51"/>
<wire x1="2.795" y1="1.95" x2="2.795" y2="-1.95" width="0.12" layer="51"/>
<smd name="K" x="-2.2127" y="0" dx="2.1808" dy="2.0291" layer="1"/>
<smd name="A" x="2.2127" y="0" dx="2.1808" dy="2.0291" layer="1"/>
<text x="0" y="2.585" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.585" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="DIOM7959X265" urn="urn:adsk.eagle:footprint:3793209/2" library_version="76">
<description>MOLDED BODY, 7.94 X 5.95 X 2.65 mm body
&lt;p&gt;MOLDED BODY package with body size 7.94 X 5.95 X 2.65 mm&lt;/p&gt;</description>
<wire x1="4.065" y1="3.15" x2="-4.07" y2="3.15" width="0.12" layer="21"/>
<wire x1="-4.8871" y1="3.15" x2="-4.8871" y2="-3.15" width="0.12" layer="21"/>
<wire x1="-4.07" y1="-3.15" x2="4.065" y2="-3.15" width="0.12" layer="21"/>
<wire x1="4.065" y1="-3.15" x2="-4.065" y2="-3.15" width="0.12" layer="51"/>
<wire x1="-4.065" y1="-3.15" x2="-4.065" y2="3.15" width="0.12" layer="51"/>
<wire x1="-4.065" y1="3.15" x2="4.065" y2="3.15" width="0.12" layer="51"/>
<wire x1="4.065" y1="3.15" x2="4.065" y2="-3.15" width="0.12" layer="51"/>
<smd name="K" x="-3.4827" y="0" dx="2.1808" dy="3.1202" layer="1"/>
<smd name="A" x="3.4827" y="0" dx="2.1808" dy="3.1202" layer="1"/>
<text x="0" y="3.785" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-3.785" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="SOD2512X100" urn="urn:adsk.eagle:footprint:3793220/2" library_version="76">
<description>SOD, 2.5 mm span, 1.7 X 1.25 X 1 mm body
&lt;p&gt;SOD package with 2.5 mm span with body size 1.7 X 1.25 X 1 mm&lt;/p&gt;</description>
<wire x1="-0.9" y1="0.675" x2="0.9" y2="0.675" width="0.12" layer="21"/>
<wire x1="-2.0217" y1="0.675" x2="-2.0217" y2="-0.675" width="0.12" layer="21"/>
<wire x1="0.9" y1="-0.675" x2="-0.9" y2="-0.675" width="0.12" layer="21"/>
<wire x1="0.9" y1="-0.675" x2="-0.9" y2="-0.675" width="0.12" layer="51"/>
<wire x1="-0.9" y1="-0.675" x2="-0.9" y2="0.675" width="0.12" layer="51"/>
<wire x1="-0.9" y1="0.675" x2="0.9" y2="0.675" width="0.12" layer="51"/>
<wire x1="0.9" y1="0.675" x2="0.9" y2="-0.675" width="0.12" layer="51"/>
<smd name="K" x="-1.1968" y="0" dx="1.0218" dy="0.4971" layer="1"/>
<smd name="A" x="1.1968" y="0" dx="1.0218" dy="0.4971" layer="1"/>
<text x="0" y="1.31" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.31" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="SOD3716X135" urn="urn:adsk.eagle:footprint:3793234/2" library_version="76">
<description>SOD, 3.7 mm span, 2.7 X 1.6 X 1.35 mm body
&lt;p&gt;SOD package with 3.7 mm span with body size 2.7 X 1.6 X 1.35 mm&lt;/p&gt;</description>
<wire x1="-1.425" y1="0.9" x2="1.425" y2="0.9" width="0.12" layer="21"/>
<wire x1="-2.5991" y1="0.9" x2="-2.5991" y2="-0.9" width="0.12" layer="21"/>
<wire x1="1.425" y1="-0.9" x2="-1.425" y2="-0.9" width="0.12" layer="21"/>
<wire x1="1.425" y1="-0.9" x2="-1.425" y2="-0.9" width="0.12" layer="51"/>
<wire x1="-1.425" y1="-0.9" x2="-1.425" y2="0.9" width="0.12" layer="51"/>
<wire x1="-1.425" y1="0.9" x2="1.425" y2="0.9" width="0.12" layer="51"/>
<wire x1="1.425" y1="0.9" x2="1.425" y2="-0.9" width="0.12" layer="51"/>
<smd name="K" x="-1.7116" y="0" dx="1.147" dy="0.7891" layer="1"/>
<smd name="A" x="1.7116" y="0" dx="1.147" dy="0.7891" layer="1"/>
<text x="0" y="1.535" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.535" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="SODFL4725X110" urn="urn:adsk.eagle:footprint:3921258/2" library_version="76">
<description>SODFL, 4.7 mm span, 3.8 X 2.5 X 1.1 mm body
&lt;p&gt;SODFL package with 4.7 mm span with body size 3.8 X 2.5 X 1.1 mm&lt;/p&gt;</description>
<wire x1="-3.0192" y1="1.35" x2="-3.0192" y2="-1.35" width="0.12" layer="21"/>
<wire x1="2" y1="-1.35" x2="-2" y2="-1.35" width="0.12" layer="51"/>
<wire x1="-2" y1="-1.35" x2="-2" y2="1.35" width="0.12" layer="51"/>
<wire x1="-2" y1="1.35" x2="2" y2="1.35" width="0.12" layer="51"/>
<wire x1="2" y1="1.35" x2="2" y2="-1.35" width="0.12" layer="51"/>
<smd name="K" x="-1.9923" y="0" dx="1.4257" dy="1.9202" layer="1"/>
<smd name="A" x="1.9923" y="0" dx="1.4257" dy="1.9202" layer="1"/>
<text x="0" y="1.985" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.985" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="DIOC0502X50" urn="urn:adsk.eagle:footprint:3948091/2" library_version="76">
<description>CHIP, 0.5 X 0.25 X 0.5 mm body
&lt;p&gt;CHIP package with body size 0.5 X 0.25 X 0.5 mm&lt;/p&gt;</description>
<wire x1="-0.754" y1="0.235" x2="-0.754" y2="-0.235" width="0.12" layer="21"/>
<wire x1="0.275" y1="-0.15" x2="-0.275" y2="-0.15" width="0.12" layer="51"/>
<wire x1="-0.275" y1="-0.15" x2="-0.275" y2="0.15" width="0.12" layer="51"/>
<wire x1="-0.275" y1="0.15" x2="0.275" y2="0.15" width="0.12" layer="51"/>
<wire x1="0.275" y1="0.15" x2="0.275" y2="-0.15" width="0.12" layer="51"/>
<smd name="K" x="-0.275" y="0" dx="0.45" dy="0.35" layer="1"/>
<smd name="A" x="0.275" y="0" dx="0.45" dy="0.35" layer="1"/>
<text x="0" y="1.124" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.124" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="DIOC1005X84" urn="urn:adsk.eagle:footprint:3948102/2" library_version="76">
<description>CHIP, 1.07 X 0.56 X 0.84 mm body
&lt;p&gt;CHIP package with body size 1.07 X 0.56 X 0.84 mm&lt;/p&gt;</description>
<wire x1="-1.0741" y1="0.4165" x2="-1.0741" y2="-0.4165" width="0.12" layer="21"/>
<wire x1="0.61" y1="-0.345" x2="-0.61" y2="-0.345" width="0.12" layer="51"/>
<wire x1="-0.61" y1="-0.345" x2="-0.61" y2="0.345" width="0.12" layer="51"/>
<wire x1="-0.61" y1="0.345" x2="0.61" y2="0.345" width="0.12" layer="51"/>
<wire x1="0.61" y1="0.345" x2="0.61" y2="-0.345" width="0.12" layer="51"/>
<smd name="K" x="-0.51" y="0" dx="0.6202" dy="0.713" layer="1"/>
<smd name="A" x="0.51" y="0" dx="0.6202" dy="0.713" layer="1"/>
<text x="0" y="1.3055" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.3055" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="DIOC1206X63" urn="urn:adsk.eagle:footprint:3948142/2" library_version="76">
<description>CHIP, 1.27 X 0.6 X 0.63 mm body
&lt;p&gt;CHIP package with body size 1.27 X 0.6 X 0.63 mm&lt;/p&gt;</description>
<wire x1="-1.1741" y1="0.4365" x2="-1.1741" y2="-0.4365" width="0.12" layer="21"/>
<wire x1="0.71" y1="-0.365" x2="-0.71" y2="-0.365" width="0.12" layer="51"/>
<wire x1="-0.71" y1="-0.365" x2="-0.71" y2="0.365" width="0.12" layer="51"/>
<wire x1="-0.71" y1="0.365" x2="0.71" y2="0.365" width="0.12" layer="51"/>
<wire x1="0.71" y1="0.365" x2="0.71" y2="-0.365" width="0.12" layer="51"/>
<smd name="K" x="-0.545" y="0" dx="0.7502" dy="0.753" layer="1"/>
<smd name="A" x="0.545" y="0" dx="0.7502" dy="0.753" layer="1"/>
<text x="0" y="1.3255" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.3255" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="DIOC1608X84" urn="urn:adsk.eagle:footprint:3948148/2" library_version="76">
<description>CHIP, 1.63 X 0.81 X 0.84 mm body
&lt;p&gt;CHIP package with body size 1.63 X 0.81 X 0.84 mm&lt;/p&gt;</description>
<wire x1="-1.5041" y1="0.5415" x2="-1.5041" y2="-0.5415" width="0.12" layer="21"/>
<wire x1="0.89" y1="-0.47" x2="-0.89" y2="-0.47" width="0.12" layer="51"/>
<wire x1="-0.89" y1="-0.47" x2="-0.89" y2="0.47" width="0.12" layer="51"/>
<wire x1="-0.89" y1="0.47" x2="0.89" y2="0.47" width="0.12" layer="51"/>
<wire x1="0.89" y1="0.47" x2="0.89" y2="-0.47" width="0.12" layer="51"/>
<smd name="K" x="-0.845" y="0" dx="0.8102" dy="0.963" layer="1"/>
<smd name="A" x="0.845" y="0" dx="0.8102" dy="0.963" layer="1"/>
<text x="0" y="1.4305" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.4305" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="DIOC2012X70" urn="urn:adsk.eagle:footprint:3948151/2" library_version="76">
<description>CHIP, 2 X 1.25 X 0.7 mm body
&lt;p&gt;CHIP package with body size 2 X 1.25 X 0.7 mm&lt;/p&gt;</description>
<wire x1="-1.7117" y1="0.7496" x2="-1.7117" y2="-0.7496" width="0.12" layer="21"/>
<wire x1="1.1" y1="-0.675" x2="-1.1" y2="-0.675" width="0.12" layer="51"/>
<wire x1="-1.1" y1="-0.675" x2="-1.1" y2="0.675" width="0.12" layer="51"/>
<wire x1="-1.1" y1="0.675" x2="1.1" y2="0.675" width="0.12" layer="51"/>
<wire x1="1.1" y1="0.675" x2="1.1" y2="-0.675" width="0.12" layer="51"/>
<smd name="K" x="-0.94" y="0" dx="1.0354" dy="1.3791" layer="1"/>
<smd name="A" x="0.94" y="0" dx="1.0354" dy="1.3791" layer="1"/>
<text x="0" y="1.6386" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.6386" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="DIOC3216X84" urn="urn:adsk.eagle:footprint:3948169/2" library_version="76">
<description>CHIP, 3.2 X 1.6 X 0.84 mm body
&lt;p&gt;CHIP package with body size 3.2 X 1.6 X 0.84 mm&lt;/p&gt;</description>
<wire x1="-2.2891" y1="0.9365" x2="-2.2891" y2="-0.9365" width="0.12" layer="21"/>
<wire x1="1.675" y1="-0.865" x2="-1.675" y2="-0.865" width="0.12" layer="51"/>
<wire x1="-1.675" y1="-0.865" x2="-1.675" y2="0.865" width="0.12" layer="51"/>
<wire x1="-1.675" y1="0.865" x2="1.675" y2="0.865" width="0.12" layer="51"/>
<wire x1="1.675" y1="0.865" x2="1.675" y2="-0.865" width="0.12" layer="51"/>
<smd name="K" x="-1.5131" y="0" dx="1.044" dy="1.753" layer="1"/>
<smd name="A" x="1.5131" y="0" dx="1.044" dy="1.753" layer="1"/>
<text x="0" y="1.8255" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.8255" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="DIOC5324X84" urn="urn:adsk.eagle:footprint:3948172/2" library_version="76">
<description>CHIP, 5.31 X 2.49 X 0.84 mm body
&lt;p&gt;CHIP package with body size 5.31 X 2.49 X 0.84 mm&lt;/p&gt;</description>
<wire x1="-3.3441" y1="1.3815" x2="-3.3441" y2="-1.3815" width="0.12" layer="21"/>
<wire x1="2.73" y1="-1.31" x2="-2.73" y2="-1.31" width="0.12" layer="51"/>
<wire x1="-2.73" y1="-1.31" x2="-2.73" y2="1.31" width="0.12" layer="51"/>
<wire x1="-2.73" y1="1.31" x2="2.73" y2="1.31" width="0.12" layer="51"/>
<wire x1="2.73" y1="1.31" x2="2.73" y2="-1.31" width="0.12" layer="51"/>
<smd name="K" x="-2.555" y="0" dx="1.0702" dy="2.643" layer="1"/>
<smd name="A" x="2.555" y="0" dx="1.0702" dy="2.643" layer="1"/>
<text x="0" y="2.2705" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.2705" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="TAG-CONNECT_TC2050-IDC-NL" urn="urn:adsk.eagle:package:16170706/4" type="box" library_version="75">
<packageinstances>
<packageinstance name="TAG-CONNECT_TC2050-IDC-NL"/>
</packageinstances>
</package3d>
<package3d name="TP_1.5MM" urn="urn:adsk.eagle:package:8414069/2" type="empty" library_version="40" library_locally_modified="yes">
<packageinstances>
<packageinstance name="TP_1.5MM"/>
</packageinstances>
</package3d>
<package3d name="TP_1.5MM_PAD" urn="urn:adsk.eagle:package:8414070/2" type="empty" library_version="40" library_locally_modified="yes">
<packageinstances>
<packageinstance name="TP_1.5MM_PAD"/>
</packageinstances>
</package3d>
<package3d name="TP_1.7MM" urn="urn:adsk.eagle:package:8414071/2" type="empty" library_version="40" library_locally_modified="yes">
<packageinstances>
<packageinstance name="TP_1.7MM"/>
</packageinstances>
</package3d>
<package3d name="C120H40" urn="urn:adsk.eagle:package:8414063/2" type="empty" library_version="40" library_locally_modified="yes">
<packageinstances>
<packageinstance name="C120H40"/>
</packageinstances>
</package3d>
<package3d name="C131H51" urn="urn:adsk.eagle:package:8414064/2" type="empty" library_version="40" library_locally_modified="yes">
<packageinstances>
<packageinstance name="C131H51"/>
</packageinstances>
</package3d>
<package3d name="C406H326" urn="urn:adsk.eagle:package:8414065/2" type="empty" library_version="40" library_locally_modified="yes">
<packageinstances>
<packageinstance name="C406H326"/>
</packageinstances>
</package3d>
<package3d name="C491H411" urn="urn:adsk.eagle:package:8414066/2" type="empty" library_version="40" library_locally_modified="yes">
<packageinstances>
<packageinstance name="C491H411"/>
</packageinstances>
</package3d>
<package3d name="CIR_PAD_1MM" urn="urn:adsk.eagle:package:8414067/2" type="empty" library_version="40" library_locally_modified="yes">
<packageinstances>
<packageinstance name="CIR_PAD_1MM"/>
</packageinstances>
</package3d>
<package3d name="CIR_PAD_2MM" urn="urn:adsk.eagle:package:8414068/2" type="empty" library_version="40" library_locally_modified="yes">
<packageinstances>
<packageinstance name="CIR_PAD_2MM"/>
</packageinstances>
</package3d>
<package3d name="RESC1005X40" urn="urn:adsk.eagle:package:3811154/1" type="model" library_version="58">
<description>CHIP, 1 X 0.5 X 0.4 mm body
&lt;p&gt;CHIP package with body size 1 X 0.5 X 0.4 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESC1005X40"/>
</packageinstances>
</package3d>
<package3d name="RESC1608X55" urn="urn:adsk.eagle:package:3811166/1" type="model" library_version="58">
<description>CHIP, 1.6 X 0.85 X 0.55 mm body
&lt;p&gt;CHIP package with body size 1.6 X 0.85 X 0.55 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESC1608X55"/>
</packageinstances>
</package3d>
<package3d name="INDM5050X430" urn="urn:adsk.eagle:package:3890169/1" type="model" library_version="58">
<description>MOLDED BODY, 5 X 5 X 4.3 mm body
&lt;p&gt;MOLDED BODY package with body size 5 X 5 X 4.3 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="INDM5050X430"/>
</packageinstances>
</package3d>
<package3d name="INDM4040X300" urn="urn:adsk.eagle:package:3890461/1" type="model" library_version="58">
<description>MOLDED BODY, 4 X 4 X 3 mm body
&lt;p&gt;MOLDED BODY package with body size 4 X 4 X 3 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="INDM4040X300"/>
</packageinstances>
</package3d>
<package3d name="INDM4949X400" urn="urn:adsk.eagle:package:3904911/1" type="model" library_version="58">
<description>MOLDED BODY, 4.9 X 4.9 X 4 mm body
&lt;p&gt;MOLDED BODY package with body size 4.9 X 4.9 X 4 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="INDM4949X400"/>
</packageinstances>
</package3d>
<package3d name="RADIAL_THT" urn="urn:adsk.eagle:package:8414048/1" type="box" library_version="40" library_locally_modified="yes">
<packageinstances>
<packageinstance name="RADIAL_THT"/>
</packageinstances>
</package3d>
<package3d name="QFP50P1600X1600X120-100" urn="urn:adsk.eagle:package:3791942/2" type="model" library_version="63">
<description>100-QFP, 0.5 mm pitch, 16 mm span, 14 X 14 X 1.2 mm body
&lt;p&gt;100-pin QFP package with 0.5 mm pitch, 16 mm lead span1 X 16 mm lead span2 with body size 14 X 14 X 1.2 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="QFP50P1600X1600X120-100"/>
</packageinstances>
</package3d>
<package3d name="CAPC1005X60" urn="urn:adsk.eagle:package:3793108/1" type="model" library_version="58">
<description>CHIP, 1.025 X 0.525 X 0.6 mm body
&lt;p&gt;CHIP package with body size 1.025 X 0.525 X 0.6 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC1005X60"/>
</packageinstances>
</package3d>
<package3d name="CAPC1608X55" urn="urn:adsk.eagle:package:3793126/2" type="model" library_version="40" library_locally_modified="yes">
<description>CHIP, 1.6 X 0.8 X 0.55 mm body
&lt;p&gt;CHIP package with body size 1.6 X 0.8 X 0.55 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC1608X55"/>
</packageinstances>
</package3d>
<package3d name="CAPC2012X127" urn="urn:adsk.eagle:package:3793139/1" type="model" library_version="58">
<description>CHIP, 2.01 X 1.25 X 1.27 mm body
&lt;p&gt;CHIP package with body size 2.01 X 1.25 X 1.27 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC2012X127"/>
</packageinstances>
</package3d>
<package3d name="CKSWITCHES_KXT3" urn="urn:adsk.eagle:package:8414043/1" type="box" library_version="40" library_locally_modified="yes">
<packageinstances>
<packageinstance name="CKSWITCHES_KXT3"/>
</packageinstances>
</package3d>
<package3d name="CRYSTAL_CYLINDRICAL_RADIAL" urn="urn:adsk.eagle:package:8414042/1" type="box" library_version="40" library_locally_modified="yes">
<packageinstances>
<packageinstance name="CRYSTAL_CYLINDRICAL_RADIAL"/>
</packageinstances>
</package3d>
<package3d name="OSCCC500X320X110" urn="urn:adsk.eagle:package:3849466/1" type="model" library_version="58">
<description>OSCILLATOR CORNERCONCAVE, 5 X 3.2 X 1.1 mm body
&lt;p&gt;OSCILLATOR CORNERCONCAVE package with body size 5 X 3.2 X 1.1 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="OSCCC500X320X110"/>
</packageinstances>
</package3d>
<package3d name="LED_CUSTOM_0402/RADIAL" urn="urn:adsk.eagle:package:8414050/2" type="model" library_version="41" library_locally_modified="yes">
<packageinstances>
<packageinstance name="LED_CUSTOM_0402/RADIAL"/>
</packageinstances>
</package3d>
<package3d name="LED_RADIAL" urn="urn:adsk.eagle:package:8414056/2" type="model" library_version="41" library_locally_modified="yes">
<packageinstances>
<packageinstance name="LED_RADIAL"/>
</packageinstances>
</package3d>
<package3d name="LEDC1005X60N" urn="urn:adsk.eagle:package:15620674/1" type="box" library_version="41" library_locally_modified="yes">
<packageinstances>
<packageinstance name="LEDC1005X60N"/>
</packageinstances>
</package3d>
<package3d name="DO214AC_SMA" urn="urn:adsk.eagle:package:15671034/5" type="model" library_version="70">
<packageinstances>
<packageinstance name="DO214AC_SMA"/>
</packageinstances>
</package3d>
<package3d name="SOIC127P600X175-8" urn="urn:adsk.eagle:package:3985885/2" type="model" library_version="76">
<description>8-SOIC, 1.27 mm pitch, 6 mm span, 4.9 X 3.9 X 1.75 mm body
&lt;p&gt;8-pin SOIC package with 1.27 mm pitch, 6 mm span with body size 4.9 X 3.9 X 1.75 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="SOIC127P600X175-9"/>
</packageinstances>
</package3d>
<package3d name="SOP50P310X90-8" urn="urn:adsk.eagle:package:3986204/1" type="model" library_version="58">
<description>8-SOP, 0.5 mm pitch, 3.1 mm span, 2 X 2.3 X 0.9 mm body
&lt;p&gt;8-pin SOP package with 0.5 mm pitch, 3.1 mm span with body size 2 X 2.3 X 0.9 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="SOP50P310X90-8"/>
</packageinstances>
</package3d>
<package3d name="DIOM7959X262N" urn="urn:adsk.eagle:package:15726434/3" type="model" library_version="75" library_locally_modified="yes">
<description>Molded Body, 7.94 X 5.91 X 2.62 mm body
&lt;p&gt;Molded Body package with body size 7.94 X 5.91 X 2.62 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="DIOM7959X262N"/>
</packageinstances>
</package3d>
<package3d name="RESC2012X70" urn="urn:adsk.eagle:package:3811169/1" type="model" library_version="58">
<description>CHIP, 2 X 1.25 X 0.7 mm body
&lt;p&gt;CHIP package with body size 2 X 1.25 X 0.7 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESC2012X70"/>
</packageinstances>
</package3d>
<package3d name="SAMTEC_TSW-104-XX-X-S" urn="urn:adsk.eagle:package:8414075/1" type="box" library_version="47">
<packageinstances>
<packageinstance name="SAMTEC_TSW-104-XX-X-S"/>
</packageinstances>
</package3d>
<package3d name="SAMTEC_TSW-104-XX-X-S_NO_OUTLINE" urn="urn:adsk.eagle:package:8414076/1" type="box" library_version="47">
<packageinstances>
<packageinstance name="SAMTEC_TSW-104-XX-X-S_NO_OUTLINE"/>
</packageinstances>
</package3d>
<package3d name="SAMTEC_TSW-104-XX-X-S_FLEXSMD_PAD" urn="urn:adsk.eagle:package:8414081/1" type="box" library_version="47">
<packageinstances>
<packageinstance name="SAMTEC_TSW-104-XX-X-S_FLEXSMD_PAD"/>
</packageinstances>
</package3d>
<package3d name="JST_B4P-SHF-1AA" urn="urn:adsk.eagle:package:8414089/2" type="model" library_version="47">
<packageinstances>
<packageinstance name="JST_B4P-SHF-1AA"/>
</packageinstances>
</package3d>
<package3d name="JST_BS4P-SHF-1AA" urn="urn:adsk.eagle:package:8414090/1" type="box" library_version="47">
<packageinstances>
<packageinstance name="JST_BS4P-SHF-1AA"/>
</packageinstances>
</package3d>
<package3d name="JST_F4P-HVQ" urn="urn:adsk.eagle:package:8414091/1" type="box" library_version="47">
<packageinstances>
<packageinstance name="JST_F4P-HVQ"/>
</packageinstances>
</package3d>
<package3d name="JST_F4P-SHVQ" urn="urn:adsk.eagle:package:8414092/1" type="box" library_version="47">
<packageinstances>
<packageinstance name="JST_F4P-SHVQ"/>
</packageinstances>
</package3d>
<package3d name="JST_04JQ-BT" urn="urn:adsk.eagle:package:8414218/2" type="model" library_version="47">
<packageinstances>
<packageinstance name="JST_04JQ-BT"/>
</packageinstances>
</package3d>
<package3d name="JST_04JQ-ST" urn="urn:adsk.eagle:package:8414219/2" type="model" library_version="47">
<packageinstances>
<packageinstance name="JST_04JQ-ST"/>
</packageinstances>
</package3d>
<package3d name="JST_B4B-XH-A" urn="urn:adsk.eagle:package:8414220/2" type="model" library_version="47">
<packageinstances>
<packageinstance name="JST_B4B-XH-A"/>
</packageinstances>
</package3d>
<package3d name="JST_S4B-XH-A-1" urn="urn:adsk.eagle:package:8414221/1" type="box" library_version="47">
<packageinstances>
<packageinstance name="JST_S4B-XH-A-1"/>
</packageinstances>
</package3d>
<package3d name="JST_B04B-PASK" urn="urn:adsk.eagle:package:8414269/2" type="model" library_version="47">
<packageinstances>
<packageinstance name="JST_B04B-PASK"/>
</packageinstances>
</package3d>
<package3d name="JST_S04B-PASK-2" urn="urn:adsk.eagle:package:8414270/1" type="box" library_version="47">
<packageinstances>
<packageinstance name="JST_S04B-PASK-2"/>
</packageinstances>
</package3d>
<package3d name="JST_B4B-PH-K-S" urn="urn:adsk.eagle:package:8414302/2" type="model" library_version="47">
<packageinstances>
<packageinstance name="JST_B4B-PH-K-S"/>
</packageinstances>
</package3d>
<package3d name="JST_S4B-PH-K-S" urn="urn:adsk.eagle:package:8414303/1" type="box" library_version="47">
<packageinstances>
<packageinstance name="JST_S4B-PH-K-S"/>
</packageinstances>
</package3d>
<package3d name="JST_B04B-PSILE-A1_B04B-PSIY-B1_B04B-PSIR-C1" urn="urn:adsk.eagle:package:8414334/2" type="model" library_version="47">
<packageinstances>
<packageinstance name="JST_B04B-PSILE-A1_B04B-PSIY-B1_B04B-PSIR-C1"/>
</packageinstances>
</package3d>
<package3d name="JST_B4PS-VH" urn="urn:adsk.eagle:package:8414350/2" type="model" library_version="47">
<packageinstances>
<packageinstance name="JST_B4PS-VH"/>
</packageinstances>
</package3d>
<package3d name="JST_B4P-VH" urn="urn:adsk.eagle:package:8414351/2" type="model" library_version="47">
<packageinstances>
<packageinstance name="JST_B4P-VH"/>
</packageinstances>
</package3d>
<package3d name="JST_B4P-VH-FB-B" urn="urn:adsk.eagle:package:8414352/2" type="model" library_version="47">
<packageinstances>
<packageinstance name="JST_B4P-VH-FB-B"/>
</packageinstances>
</package3d>
<package3d name="JST_B04B-ZESK-1D" urn="urn:adsk.eagle:package:8414376/2" type="model" library_version="47">
<packageinstances>
<packageinstance name="JST_B04B-ZESK-1D"/>
</packageinstances>
</package3d>
<package3d name="JST_S04B-ZESK-2D" urn="urn:adsk.eagle:package:8414377/1" type="box" library_version="47">
<packageinstances>
<packageinstance name="JST_S04B-ZESK-2D"/>
</packageinstances>
</package3d>
<package3d name="TO457P1000X238-3" urn="urn:adsk.eagle:package:3920001/3" type="model" library_version="76">
<description>3-TO, DPAK, 4.57 mm pitch, 10 mm span, 6.6 X 6.1 X 2.38 mm body
&lt;p&gt;3-pin TO, DPAK package with 4.57 mm pitch, 10 mm span with body size 6.6 X 6.1 X 2.38 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="TO457P1000X238-3"/>
</packageinstances>
</package3d>
<package3d name="POWERPAK_SO-8" urn="urn:adsk.eagle:package:8414082/1" type="box" library_version="40">
<packageinstances>
<packageinstance name="POWERPAK_SO-8"/>
</packageinstances>
</package3d>
<package3d name="DIOC5226X110N" urn="urn:adsk.eagle:package:16113232/2" type="model" library_version="70">
<description>Chip, 5.20 X 2.60 X 1.10 mm body
&lt;p&gt;Chip package with body size 5.20 X 2.60 X 1.10 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="DIOC5226X110N"/>
</packageinstances>
</package3d>
<package3d name="SOP50P490X110-10" urn="urn:adsk.eagle:package:3921298/1" type="model" library_version="58">
<description>10-SOP, 0.5 mm pitch, 4.9 mm span, 3 X 3 X 1.1 mm body
&lt;p&gt;10-pin SOP package with 0.5 mm pitch, 4.9 mm span with body size 3 X 3 X 1.1 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="SOP50P490X110-10"/>
</packageinstances>
</package3d>
<package3d name="RESC3115X70" urn="urn:adsk.eagle:package:3811173/1" type="model" library_version="58">
<description>CHIP, 3.125 X 1.55 X 0.7 mm body
&lt;p&gt;CHIP package with body size 3.125 X 1.55 X 0.7 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESC3115X70"/>
</packageinstances>
</package3d>
<package3d name="JST_B8B-PHDSS" urn="urn:adsk.eagle:package:8414083/1" type="box" library_version="52">
<packageinstances>
<packageinstance name="JST_B8B-PHDSS"/>
</packageinstances>
</package3d>
<package3d name="JST_8P-HVQ" urn="urn:adsk.eagle:package:8414108/2" type="model" library_version="52">
<packageinstances>
<packageinstance name="JST_8P-HVQ"/>
</packageinstances>
</package3d>
<package3d name="JST_B8P-SHF-1AA" urn="urn:adsk.eagle:package:8414109/1" type="box" library_version="52">
<packageinstances>
<packageinstance name="JST_B8P-SHF-1AA"/>
</packageinstances>
</package3d>
<package3d name="JST_BS8P-SHF-1AA" urn="urn:adsk.eagle:package:8414110/1" type="box" library_version="52">
<packageinstances>
<packageinstance name="JST_BS8P-SHF-1AA"/>
</packageinstances>
</package3d>
<package3d name="JST_F8P-HVQ" urn="urn:adsk.eagle:package:8414111/1" type="box" library_version="52">
<packageinstances>
<packageinstance name="JST_F8P-HVQ"/>
</packageinstances>
</package3d>
<package3d name="JST_F8P-SHVQ" urn="urn:adsk.eagle:package:8414112/1" type="box" library_version="52">
<packageinstances>
<packageinstance name="JST_F8P-SHVQ"/>
</packageinstances>
</package3d>
<package3d name="JST_08JQ-BT" urn="urn:adsk.eagle:package:8414234/2" type="model" library_version="52">
<packageinstances>
<packageinstance name="JST_08JQ-BT"/>
</packageinstances>
</package3d>
<package3d name="JST_08JQ-ST" urn="urn:adsk.eagle:package:8414235/2" type="model" library_version="52">
<packageinstances>
<packageinstance name="JST_08JQ-ST"/>
</packageinstances>
</package3d>
<package3d name="JST_B8B-XH-A" urn="urn:adsk.eagle:package:8414236/1" type="box" library_version="52">
<packageinstances>
<packageinstance name="JST_B8B-XH-A"/>
</packageinstances>
</package3d>
<package3d name="JST_S8B-XH-A-1" urn="urn:adsk.eagle:package:8414237/1" type="box" library_version="52">
<packageinstances>
<packageinstance name="JST_S8B-XH-A-1"/>
</packageinstances>
</package3d>
<package3d name="JST_B08B-PASK" urn="urn:adsk.eagle:package:8414277/1" type="box" library_version="52">
<packageinstances>
<packageinstance name="JST_B08B-PASK"/>
</packageinstances>
</package3d>
<package3d name="JST_S08B-PASK-2" urn="urn:adsk.eagle:package:8414278/1" type="box" library_version="52">
<packageinstances>
<packageinstance name="JST_S08B-PASK-2"/>
</packageinstances>
</package3d>
<package3d name="JST_PHD_4PIN_2MM" urn="urn:adsk.eagle:package:8414296/1" type="box" library_version="52">
<packageinstances>
<packageinstance name="JST_PHD_4PIN_2MM"/>
</packageinstances>
</package3d>
<package3d name="JST_B8B-PH-K-S" urn="urn:adsk.eagle:package:8414310/1" type="box" library_version="52">
<packageinstances>
<packageinstance name="JST_B8B-PH-K-S"/>
</packageinstances>
</package3d>
<package3d name="JST_S8B-PH-K-S" urn="urn:adsk.eagle:package:8414311/1" type="box" library_version="52">
<packageinstances>
<packageinstance name="JST_S8B-PH-K-S"/>
</packageinstances>
</package3d>
<package3d name="JST_B08B-PSILE-1" urn="urn:adsk.eagle:package:8414337/1" type="box" library_version="52">
<packageinstances>
<packageinstance name="JST_B08B-PSILE-1"/>
</packageinstances>
</package3d>
<package3d name="JST_B8PS-VH" urn="urn:adsk.eagle:package:8414363/1" type="box" library_version="52">
<packageinstances>
<packageinstance name="JST_B8PS-VH"/>
</packageinstances>
</package3d>
<package3d name="JST_B8P-VH" urn="urn:adsk.eagle:package:8414364/1" type="box" library_version="52">
<packageinstances>
<packageinstance name="JST_B8P-VH"/>
</packageinstances>
</package3d>
<package3d name="JST_B8P-VH-FB-B" urn="urn:adsk.eagle:package:8414365/1" type="box" library_version="52">
<packageinstances>
<packageinstance name="JST_B8P-VH-FB-B"/>
</packageinstances>
</package3d>
<package3d name="JST_B08B-ZESK-1D" urn="urn:adsk.eagle:package:8414384/1" type="box" library_version="52">
<packageinstances>
<packageinstance name="JST_B08B-ZESK-1D"/>
</packageinstances>
</package3d>
<package3d name="JST_S08B-ZESK-2D" urn="urn:adsk.eagle:package:8414385/1" type="box" library_version="52">
<packageinstances>
<packageinstance name="JST_S08B-ZESK-2D"/>
</packageinstances>
</package3d>
<package3d name="SOT65P210X110-3" urn="urn:adsk.eagle:package:3809951/2" type="model" library_version="76">
<description>3-SOT23, 0.65 mm pitch, 2.1 mm span, 2 X 1.25 X 1.1 mm body
&lt;p&gt;3-pin SOT23 package with 0.65 mm pitch, 2.1 mm span with body size 2 X 1.25 X 1.1 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="SOT65P210X110-3"/>
</packageinstances>
</package3d>
<package3d name="RESC0603X30" urn="urn:adsk.eagle:package:3811151/1" type="model" library_version="58">
<description>CHIP, 0.6 X 0.3 X 0.3 mm body
&lt;p&gt;CHIP package with body size 0.6 X 0.3 X 0.3 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESC0603X30"/>
</packageinstances>
</package3d>
<package3d name="RESC3225X70" urn="urn:adsk.eagle:package:3811180/1" type="model" library_version="58">
<description>CHIP, 3.2 X 2.5 X 0.7 mm body
&lt;p&gt;CHIP package with body size 3.2 X 2.5 X 0.7 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESC3225X70"/>
</packageinstances>
</package3d>
<package3d name="RESC4532X70" urn="urn:adsk.eagle:package:3811188/1" type="model" library_version="58">
<description>CHIP, 4.5 X 3.2 X 0.7 mm body
&lt;p&gt;CHIP package with body size 4.5 X 3.2 X 0.7 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESC4532X70"/>
</packageinstances>
</package3d>
<package3d name="RESC5025X70" urn="urn:adsk.eagle:package:3811197/1" type="model" library_version="58">
<description>CHIP, 5 X 2.5 X 0.7 mm body
&lt;p&gt;CHIP package with body size 5 X 2.5 X 0.7 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESC5025X70"/>
</packageinstances>
</package3d>
<package3d name="RESC5750X229" urn="urn:adsk.eagle:package:3811212/1" type="model" library_version="58">
<description>CHIP, 5.7 X 5 X 2.29 mm body
&lt;p&gt;CHIP package with body size 5.7 X 5 X 2.29 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESC5750X229"/>
</packageinstances>
</package3d>
<package3d name="RESC6432X70" urn="urn:adsk.eagle:package:3811284/1" type="model" library_version="58">
<description>CHIP, 6.4 X 3.2 X 0.7 mm body
&lt;p&gt;CHIP package with body size 6.4 X 3.2 X 0.7 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESC6432X70"/>
</packageinstances>
</package3d>
<package3d name="RESC2037X52" urn="urn:adsk.eagle:package:3950966/1" type="model" library_version="58">
<description>CHIP, 2 X 3.75 X 0.52 mm body
&lt;p&gt;CHIP package with body size 2 X 3.75 X 0.52 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESC2037X52"/>
</packageinstances>
</package3d>
<package3d name="INDC1005X60" urn="urn:adsk.eagle:package:3810108/1" type="model" library_version="58">
<description>CHIP, 1 X 0.5 X 0.6 mm body
&lt;p&gt;CHIP package with body size 1 X 0.5 X 0.6 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="INDC1005X60"/>
</packageinstances>
</package3d>
<package3d name="INDC1608X95" urn="urn:adsk.eagle:package:3810118/1" type="model" library_version="58">
<description>CHIP, 1.6 X 0.8 X 0.95 mm body
&lt;p&gt;CHIP package with body size 1.6 X 0.8 X 0.95 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="INDC1608X95"/>
</packageinstances>
</package3d>
<package3d name="INDC2520X220" urn="urn:adsk.eagle:package:3810123/1" type="model" library_version="58">
<description>CHIP, 2.5 X 2 X 2.2 mm body
&lt;p&gt;CHIP package with body size 2.5 X 2 X 2.2 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="INDC2520X220"/>
</packageinstances>
</package3d>
<package3d name="INDC3225X135" urn="urn:adsk.eagle:package:3810128/1" type="model" library_version="58">
<description>CHIP, 3.2 X 2.5 X 1.35 mm body
&lt;p&gt;CHIP package with body size 3.2 X 2.5 X 1.35 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="INDC3225X135"/>
</packageinstances>
</package3d>
<package3d name="INDC4509X190" urn="urn:adsk.eagle:package:3810712/1" type="model" library_version="58">
<description>CHIP, 4.5 X 0.9 X 1.9 mm body
&lt;p&gt;CHIP package with body size 4.5 X 0.9 X 1.9 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="INDC4509X190"/>
</packageinstances>
</package3d>
<package3d name="INDC4532X175" urn="urn:adsk.eagle:package:3810715/1" type="model" library_version="58">
<description>CHIP, 4.5 X 3.2 X 1.75 mm body
&lt;p&gt;CHIP package with body size 4.5 X 3.2 X 1.75 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="INDC4532X175"/>
</packageinstances>
</package3d>
<package3d name="INDC5750X180" urn="urn:adsk.eagle:package:3810722/1" type="model" library_version="58">
<description>CHIP, 5.7 X 5 X 1.8 mm body
&lt;p&gt;CHIP package with body size 5.7 X 5 X 1.8 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="INDC5750X180"/>
</packageinstances>
</package3d>
<package3d name="INDC6350X200" urn="urn:adsk.eagle:package:3811095/1" type="model" library_version="58">
<description>CHIP, 6.3 X 5 X 2 mm body
&lt;p&gt;CHIP package with body size 6.3 X 5 X 2 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="INDC6350X200"/>
</packageinstances>
</package3d>
<package3d name="INDM4848X300" urn="urn:adsk.eagle:package:3837793/1" type="model" library_version="58">
<description>MOLDED BODY, 4.8 X 4.8 X 3 mm body
&lt;p&gt;MOLDED BODY package with body size 4.8 X 4.8 X 3 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="INDM4848X300"/>
</packageinstances>
</package3d>
<package3d name="INDM4440X300" urn="urn:adsk.eagle:package:3890484/1" type="model" library_version="58">
<description>MOLDED BODY, 4.45 X 4.06 X 3 mm body
&lt;p&gt;MOLDED BODY package with body size 4.45 X 4.06 X 3 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="INDM4440X300"/>
</packageinstances>
</package3d>
<package3d name="INDM7070X450" urn="urn:adsk.eagle:package:3890494/1" type="model" library_version="58">
<description>MOLDED BODY, 7 X 7 X 4.5 mm body
&lt;p&gt;MOLDED BODY package with body size 7 X 7 X 4.5 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="INDM7070X450"/>
</packageinstances>
</package3d>
<package3d name="INDM6363X500" urn="urn:adsk.eagle:package:3920872/1" type="model" library_version="58">
<description>MOLDED BODY, 6.3 X 6.3 X 5 mm body
&lt;p&gt;MOLDED BODY package with body size 6.3 X 6.3 X 5 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="INDM6363X500"/>
</packageinstances>
</package3d>
<package3d name="INDM238238X1016" urn="urn:adsk.eagle:package:3949005/1" type="model" library_version="58">
<description>MOLDED BODY, 23.88 X 23.88 X 10.16 mm body
&lt;p&gt;MOLDED BODY package with body size 23.88 X 23.88 X 10.16 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="INDM238238X1016"/>
</packageinstances>
</package3d>
<package3d name="INDM8080X400" urn="urn:adsk.eagle:package:3950981/1" type="model" library_version="58">
<description>MOLDED BODY, 8 X 8 X 4 mm body
&lt;p&gt;MOLDED BODY package with body size 8 X 8 X 4 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="INDM8080X400"/>
</packageinstances>
</package3d>
<package3d name="INDM125125X650" urn="urn:adsk.eagle:package:3950988/1" type="model" library_version="58">
<description>MOLDED BODY, 12.5 X 12.5 X 6.5 mm body
&lt;p&gt;MOLDED BODY package with body size 12.5 X 12.5 X 6.5 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="INDM125125X650"/>
</packageinstances>
</package3d>
<package3d name="CAPC1220X107" urn="urn:adsk.eagle:package:3793115/1" type="model" library_version="58">
<description>CHIP, 1.25 X 2 X 1.07 mm body
&lt;p&gt;CHIP package with body size 1.25 X 2 X 1.07 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC1220X107"/>
</packageinstances>
</package3d>
<package3d name="CAPC1632X168" urn="urn:adsk.eagle:package:3793131/1" type="model" library_version="58">
<description>CHIP, 1.6 X 3.2 X 1.68 mm body
&lt;p&gt;CHIP package with body size 1.6 X 3.2 X 1.68 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC1632X168"/>
</packageinstances>
</package3d>
<package3d name="CAPC3215X168" urn="urn:adsk.eagle:package:3793145/1" type="model" library_version="58">
<description>CHIP, 3.2 X 1.5 X 1.68 mm body
&lt;p&gt;CHIP package with body size 3.2 X 1.5 X 1.68 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC3215X168"/>
</packageinstances>
</package3d>
<package3d name="CAPC3225X168" urn="urn:adsk.eagle:package:3793149/1" type="model" library_version="58">
<description>CHIP, 3.2 X 2.5 X 1.68 mm body
&lt;p&gt;CHIP package with body size 3.2 X 2.5 X 1.68 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC3225X168"/>
</packageinstances>
</package3d>
<package3d name="CAPC4520X168" urn="urn:adsk.eagle:package:3793154/1" type="model" library_version="58">
<description>CHIP, 4.5 X 2.03 X 1.68 mm body
&lt;p&gt;CHIP package with body size 4.5 X 2.03 X 1.68 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC4520X168"/>
</packageinstances>
</package3d>
<package3d name="CAPC4532X218" urn="urn:adsk.eagle:package:3793159/1" type="model" library_version="58">
<description>CHIP, 4.5 X 3.2 X 2.18 mm body
&lt;p&gt;CHIP package with body size 4.5 X 3.2 X 2.18 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC4532X218"/>
</packageinstances>
</package3d>
<package3d name="CAPC4564X218" urn="urn:adsk.eagle:package:3793166/1" type="model" library_version="58">
<description>CHIP, 4.5 X 6.4 X 2.18 mm body
&lt;p&gt;CHIP package with body size 4.5 X 6.4 X 2.18 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC4564X218"/>
</packageinstances>
</package3d>
<package3d name="CAPC5650X218" urn="urn:adsk.eagle:package:3793171/1" type="model" library_version="58">
<description>CHIP, 5.64 X 5.08 X 2.18 mm body
&lt;p&gt;CHIP package with body size 5.64 X 5.08 X 2.18 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC5650X218"/>
</packageinstances>
</package3d>
<package3d name="CAPC5563X218" urn="urn:adsk.eagle:package:3793174/1" type="model" library_version="58">
<description>CHIP, 5.59 X 6.35 X 2.18 mm body
&lt;p&gt;CHIP package with body size 5.59 X 6.35 X 2.18 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC5563X218"/>
</packageinstances>
</package3d>
<package3d name="CAPC9198X218" urn="urn:adsk.eagle:package:3793178/1" type="model" library_version="58">
<description>CHIP, 9.14 X 9.82 X 2.18 mm body
&lt;p&gt;CHIP package with body size 9.14 X 9.82 X 2.18 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC9198X218"/>
</packageinstances>
</package3d>
<package3d name="SOIC127P800X203-8" urn="urn:adsk.eagle:package:3852771/1" type="model" library_version="58">
<description>8-SOIC, 1.27 mm pitch, 8.005 mm span, 5.23 X 5.255 X 2.03 mm body
&lt;p&gt;8-pin SOIC package with 1.27 mm pitch, 8.005 mm span with body size 5.23 X 5.255 X 2.03 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="SOIC127P800X203-8"/>
</packageinstances>
</package3d>
<package3d name="OSCCC320X250X80" urn="urn:adsk.eagle:package:3849546/1" type="model" library_version="58">
<description>OSCILLATOR CORNERCONCAVE, 3.2 X 2.5 X 0.8 mm body
&lt;p&gt;OSCILLATOR CORNERCONCAVE package with body size 3.2 X 2.5 X 0.8 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="OSCCC320X250X80"/>
</packageinstances>
</package3d>
<package3d name="SOT230P700X180-4" urn="urn:adsk.eagle:package:3834044/1" type="model" library_version="58">
<description>4-SOT223, 2.3 mm pitch, 7 mm span, 6.5 X 3.5 X 1.8 mm body
&lt;p&gt;4-pin SOT223 package with 2.3 mm pitch, 7 mm span with body size 6.5 X 3.5 X 1.8 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="SOT230P700X180-4"/>
</packageinstances>
</package3d>
<package3d name="TO508P1530X456-3" urn="urn:adsk.eagle:package:3921285/1" type="model" library_version="58">
<description>3-TO, DPAK, 5.08 mm pitch, 15.3 mm span, 10.35 X 8.8 X 4.56 mm body
&lt;p&gt;3-pin TO, DPAK package with 5.08 mm pitch, 15.3 mm span with body size 10.35 X 8.8 X 4.56 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="TO508P1530X456-3"/>
</packageinstances>
</package3d>
<package3d name="SOP65P400X130-8" urn="urn:adsk.eagle:package:3986190/1" type="model" library_version="58">
<description>8-SOP, 0.65 mm pitch, 4 mm span, 2.95 X 2.8 X 1.3 mm body
&lt;p&gt;8-pin SOP package with 0.65 mm pitch, 4 mm span with body size 2.95 X 2.8 X 1.3 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="SOP65P400X130-8"/>
</packageinstances>
</package3d>
<package3d name="HDRRA4W64P254_1X4_1016X254X254B" urn="urn:adsk.eagle:package:15597948/1" type="model" library_version="58">
<description>Single-row, 4-pin Pin Header (Male) Right Angle, 2.54 mm (0.10 in) col pitch, 5.84 mm mating length, 10.16 X 2.54 X 2.54 mm body
&lt;p&gt;Single-row (1X4), 4-pin Pin Header (Male) Right Angle package with 2.54 mm (0.10 in) col pitch, 0.64 mm lead width, 2.29 mm tail length and 5.84 mm mating length with body size 10.16 X 2.54 X 2.54 mm, pin pattern - clockwise from top left&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="HDRRA4W64P254_1X4_1016X254X254B"/>
</packageinstances>
</package3d>
<package3d name="HDRV4W64P254_1X4_1016X508X1829B" urn="urn:adsk.eagle:package:15735943/2" type="model" library_version="58">
<description>Single-row, 4-pin Pin Header (Male) Straight, 2.54 mm (0.10 in) col pitch, 15.75 mm mating length, 10.16 X 5.08 X 18.29 mm body
&lt;p&gt;Single-row (1X4), 4-pin Pin Header (Male) Straight package with 2.54 mm (0.10 in) col pitch, 0.64 mm lead width, 2.79 mm tail length and 15.75 mm mating length with overall size 10.16 X 5.08 X 18.29 mm, pin pattern - clockwise from top left&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="HDRV4W64P254_1X4_1016X508X1829B"/>
</packageinstances>
</package3d>
<package3d name="QFN65P700X700X80-32" urn="urn:adsk.eagle:package:3837100/3" type="model" library_version="76">
<description>32-QFN, 0.65 mm pitch, 7 X 7 X 0.8 mm body
&lt;p&gt;32-pin QFN package with 0.65 mm pitch with body size 7 X 7 X 0.8 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="QFN65P700X700X80-33"/>
</packageinstances>
</package3d>
<package3d name="SOT95P280X135-3" urn="urn:adsk.eagle:package:3809961/2" type="model" library_version="76">
<description>3-SOT23, 0.95 mm pitch, 2.8 mm span, 2.9 X 1.6 X 1.35 mm body
&lt;p&gt;3-pin SOT23 package with 0.95 mm pitch, 2.8 mm span with body size 2.9 X 1.6 X 1.35 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="SOT95P280X135-3"/>
</packageinstances>
</package3d>
<package3d name="SOIC127P600X175-8" urn="urn:adsk.eagle:package:3833977/3" type="model" library_version="63">
<description>8-SOIC, 1.27 mm pitch, 6 mm span, 4.9 X 3.9 X 1.75 mm body
&lt;p&gt;8-pin SOIC package with 1.27 mm pitch, 6 mm span with body size 4.9 X 3.9 X 1.75 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="SOIC127P600X175-8"/>
</packageinstances>
</package3d>
<package3d name="MOLEX_5031590400_SD" urn="urn:adsk.eagle:package:8414052/2" type="model" library_version="64">
<packageinstances>
<packageinstance name="MOLEX_5031590400_SD"/>
</packageinstances>
</package3d>
<package3d name="CONNECTOR_SMD_4PIN" urn="urn:adsk.eagle:package:21311885/1" type="box" library_version="65" library_locally_modified="yes">
<packageinstances>
<packageinstance name="CONNECTOR_SMD_4PIN"/>
</packageinstances>
</package3d>
<package3d name="MOLEX_5031590800_SD" urn="urn:adsk.eagle:package:21515996/2" type="model" library_version="67">
<packageinstances>
<packageinstance name="MOLEX_5031590800_SD"/>
</packageinstances>
</package3d>
<package3d name="DO214AA" urn="urn:adsk.eagle:package:15617780/3" type="model" library_version="70">
<description>&lt;b&gt;DIODE&lt;/b&gt;</description>
<packageinstances>
<packageinstance name="DO214AA_SMB"/>
</packageinstances>
</package3d>
<package3d name="FIDUCIAL_0.5MM" urn="urn:adsk.eagle:package:8414039/1" type="box" library_version="74">
<packageinstances>
<packageinstance name="FIDUCIAL_0.5MM"/>
</packageinstances>
</package3d>
<package3d name="LEDC1608X55" urn="urn:adsk.eagle:package:3905179/3" type="model" library_version="76">
<description>CHIP, 1.6 X 0.85 X 0.55 mm body
&lt;p&gt;CHIP package with body size 1.6 X 0.85 X 0.55 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="LEDC1608X55"/>
</packageinstances>
</package3d>
<package3d name="LEDC2012X70" urn="urn:adsk.eagle:package:3905183/4" type="box" library_version="76">
<description>CHIP, 2 X 1.25 X 0.7 mm body
&lt;p&gt;CHIP package with body size 2 X 1.25 X 0.7 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="LEDC2012X70"/>
</packageinstances>
</package3d>
<package3d name="DIOM5226X292" urn="urn:adsk.eagle:package:3793189/2" type="model" library_version="76">
<description>MOLDED BODY, 5.265 X 2.6 X 2.92 mm body
&lt;p&gt;MOLDED BODY package with body size 5.265 X 2.6 X 2.92 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="DIOM5226X292"/>
</packageinstances>
</package3d>
<package3d name="DIOM5436X265" urn="urn:adsk.eagle:package:3793201/2" type="model" library_version="76">
<description>MOLDED BODY, 5.4 X 3.6 X 2.65 mm body
&lt;p&gt;MOLDED BODY package with body size 5.4 X 3.6 X 2.65 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="DIOM5436X265"/>
</packageinstances>
</package3d>
<package3d name="DIOM7959X265" urn="urn:adsk.eagle:package:3793207/2" type="model" library_version="76">
<description>MOLDED BODY, 7.94 X 5.95 X 2.65 mm body
&lt;p&gt;MOLDED BODY package with body size 7.94 X 5.95 X 2.65 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="DIOM7959X265"/>
</packageinstances>
</package3d>
<package3d name="SOD2512X100" urn="urn:adsk.eagle:package:3793216/2" type="model" library_version="76">
<description>SOD, 2.5 mm span, 1.7 X 1.25 X 1 mm body
&lt;p&gt;SOD package with 2.5 mm span with body size 1.7 X 1.25 X 1 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="SOD2512X100"/>
</packageinstances>
</package3d>
<package3d name="SOD3716X135" urn="urn:adsk.eagle:package:3793231/2" type="model" library_version="76">
<description>SOD, 3.7 mm span, 2.7 X 1.6 X 1.35 mm body
&lt;p&gt;SOD package with 3.7 mm span with body size 2.7 X 1.6 X 1.35 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="SOD3716X135"/>
</packageinstances>
</package3d>
<package3d name="SODFL4725X110" urn="urn:adsk.eagle:package:3921253/2" type="model" library_version="76">
<description>SODFL, 4.7 mm span, 3.8 X 2.5 X 1.1 mm body
&lt;p&gt;SODFL package with 4.7 mm span with body size 3.8 X 2.5 X 1.1 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="SODFL4725X110"/>
</packageinstances>
</package3d>
<package3d name="DIOC0502X50" urn="urn:adsk.eagle:package:3948086/2" type="model" library_version="76">
<description>CHIP, 0.5 X 0.25 X 0.5 mm body
&lt;p&gt;CHIP package with body size 0.5 X 0.25 X 0.5 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="DIOC0502X50"/>
</packageinstances>
</package3d>
<package3d name="DIOC1005X84" urn="urn:adsk.eagle:package:3948100/2" type="model" library_version="76">
<description>CHIP, 1.07 X 0.56 X 0.84 mm body
&lt;p&gt;CHIP package with body size 1.07 X 0.56 X 0.84 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="DIOC1005X84"/>
</packageinstances>
</package3d>
<package3d name="DIOC1206X63" urn="urn:adsk.eagle:package:3948141/2" type="model" library_version="76">
<description>CHIP, 1.27 X 0.6 X 0.63 mm body
&lt;p&gt;CHIP package with body size 1.27 X 0.6 X 0.63 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="DIOC1206X63"/>
</packageinstances>
</package3d>
<package3d name="DIOC1608X84" urn="urn:adsk.eagle:package:3948147/2" type="model" library_version="76">
<description>CHIP, 1.63 X 0.81 X 0.84 mm body
&lt;p&gt;CHIP package with body size 1.63 X 0.81 X 0.84 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="DIOC1608X84"/>
</packageinstances>
</package3d>
<package3d name="DIOC2012X70" urn="urn:adsk.eagle:package:3948150/2" type="model" library_version="76">
<description>CHIP, 2 X 1.25 X 0.7 mm body
&lt;p&gt;CHIP package with body size 2 X 1.25 X 0.7 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="DIOC2012X70"/>
</packageinstances>
</package3d>
<package3d name="DIOC3216X84" urn="urn:adsk.eagle:package:3948168/2" type="model" library_version="76">
<description>CHIP, 3.2 X 1.6 X 0.84 mm body
&lt;p&gt;CHIP package with body size 3.2 X 1.6 X 0.84 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="DIOC3216X84"/>
</packageinstances>
</package3d>
<package3d name="DIOC5324X84" urn="urn:adsk.eagle:package:3948171/2" type="model" library_version="76">
<description>CHIP, 5.31 X 2.49 X 0.84 mm body
&lt;p&gt;CHIP package with body size 5.31 X 2.49 X 0.84 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="DIOC5324X84"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="PIN" urn="urn:adsk.eagle:symbol:8413622/1" library_version="40" library_locally_modified="yes">
<pin name="1" x="-5.08" y="0" visible="off"/>
<wire x1="-3.302" y1="0" x2="-1.27" y2="0" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="0" x2="2.54" y2="0" width="0.8128" layer="94"/>
<text x="-3.302" y="0.508" size="1.27" layer="95" ratio="15">&gt;NAME</text>
</symbol>
<symbol name="TP" urn="urn:adsk.eagle:symbol:8413657/1" library_version="40" library_locally_modified="yes">
<wire x1="0.762" y1="1.778" x2="0" y2="0.254" width="0.254" layer="94"/>
<wire x1="0" y1="0.254" x2="-0.762" y2="1.778" width="0.254" layer="94"/>
<wire x1="-0.762" y1="1.778" x2="0.762" y2="1.778" width="0.254" layer="94" curve="-180"/>
<text x="-1.27" y="3.81" size="1.778" layer="95">&gt;NAME</text>
<pin name="PP" x="0" y="0" visible="off" length="short" direction="in" rot="R90"/>
</symbol>
<symbol name="RESISTOR" urn="urn:adsk.eagle:symbol:8413611/1" library_version="40" library_locally_modified="yes">
<text x="-2.54" y="1.524" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.2032" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.2032" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.2032" layer="94"/>
</symbol>
<symbol name="INDUCTOR" urn="urn:adsk.eagle:symbol:8413612/1" library_version="40" library_locally_modified="yes">
<text x="-2.54" y="1.524" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="-1.27" y1="0" x2="0" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="0" y1="0" x2="1.27" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.254" layer="94" curve="-180"/>
</symbol>
<symbol name="ATMEL_ATSAMD51J-100" urn="urn:adsk.eagle:symbol:8413619/2" library_version="63">
<pin name="1" x="-45.72" y="30.48" visible="pad" length="middle"/>
<pin name="2" x="-45.72" y="27.94" visible="pad" length="middle"/>
<pin name="3" x="-45.72" y="25.4" visible="pad" length="middle"/>
<pin name="4" x="-45.72" y="22.86" visible="pad" length="middle"/>
<pin name="5" x="-45.72" y="20.32" visible="pad" length="middle"/>
<pin name="6" x="-45.72" y="17.78" visible="pad" length="middle"/>
<pin name="7" x="-45.72" y="15.24" visible="pad" length="middle"/>
<pin name="8" x="-45.72" y="12.7" visible="pad" length="middle"/>
<pin name="9" x="-45.72" y="10.16" visible="pad" length="middle"/>
<pin name="10" x="-45.72" y="7.62" visible="pad" length="middle"/>
<pin name="11" x="-45.72" y="5.08" visible="pad" length="middle"/>
<pin name="12" x="-45.72" y="2.54" visible="pad" length="middle"/>
<pin name="13" x="-45.72" y="0" visible="pad" length="middle"/>
<pin name="14" x="-45.72" y="-2.54" visible="pad" length="middle"/>
<pin name="15" x="-45.72" y="-5.08" visible="pad" length="middle"/>
<pin name="16" x="-45.72" y="-7.62" visible="pad" length="middle"/>
<pin name="17" x="-45.72" y="-10.16" visible="pad" length="middle"/>
<pin name="18" x="-45.72" y="-12.7" visible="pad" length="middle"/>
<pin name="19" x="-45.72" y="-15.24" visible="pad" length="middle"/>
<pin name="20" x="-45.72" y="-17.78" visible="pad" length="middle"/>
<pin name="21" x="-45.72" y="-20.32" visible="pad" length="middle"/>
<pin name="22" x="-45.72" y="-22.86" visible="pad" length="middle"/>
<pin name="23" x="-45.72" y="-25.4" visible="pad" length="middle"/>
<pin name="24" x="-45.72" y="-27.94" visible="pad" length="middle"/>
<pin name="25" x="-45.72" y="-30.48" visible="pad" length="middle"/>
<pin name="26" x="-30.48" y="-45.72" visible="pad" length="middle" rot="R90"/>
<pin name="27" x="-27.94" y="-45.72" visible="pad" length="middle" rot="R90"/>
<pin name="28" x="-25.4" y="-45.72" visible="pad" length="middle" rot="R90"/>
<pin name="29" x="-22.86" y="-45.72" visible="pad" length="middle" rot="R90"/>
<pin name="30" x="-20.32" y="-45.72" visible="pad" length="middle" rot="R90"/>
<pin name="31" x="-17.78" y="-45.72" visible="pad" length="middle" rot="R90"/>
<pin name="32" x="-15.24" y="-45.72" visible="pad" length="middle" rot="R90"/>
<pin name="33" x="-12.7" y="-45.72" visible="pad" length="middle" rot="R90"/>
<pin name="34" x="-10.16" y="-45.72" visible="pad" length="middle" rot="R90"/>
<pin name="35" x="-7.62" y="-45.72" visible="pad" length="middle" rot="R90"/>
<pin name="36" x="-5.08" y="-45.72" visible="pad" length="middle" rot="R90"/>
<pin name="37" x="-2.54" y="-45.72" visible="pad" length="middle" rot="R90"/>
<pin name="38" x="0" y="-45.72" visible="pad" length="middle" rot="R90"/>
<pin name="39" x="2.54" y="-45.72" visible="pad" length="middle" rot="R90"/>
<pin name="40" x="5.08" y="-45.72" visible="pad" length="middle" rot="R90"/>
<pin name="41" x="7.62" y="-45.72" visible="pad" length="middle" rot="R90"/>
<pin name="42" x="10.16" y="-45.72" visible="pad" length="middle" rot="R90"/>
<pin name="43" x="12.7" y="-45.72" visible="pad" length="middle" rot="R90"/>
<pin name="44" x="15.24" y="-45.72" visible="pad" length="middle" rot="R90"/>
<pin name="45" x="17.78" y="-45.72" visible="pad" length="middle" rot="R90"/>
<pin name="46" x="20.32" y="-45.72" visible="pad" length="middle" rot="R90"/>
<pin name="47" x="22.86" y="-45.72" visible="pad" length="middle" rot="R90"/>
<pin name="48" x="25.4" y="-45.72" visible="pad" length="middle" rot="R90"/>
<pin name="49" x="27.94" y="-45.72" visible="pad" length="middle" rot="R90"/>
<pin name="50" x="30.48" y="-45.72" visible="pad" length="middle" rot="R90"/>
<pin name="51" x="45.72" y="-30.48" visible="pad" length="middle" rot="R180"/>
<pin name="52" x="45.72" y="-27.94" visible="pad" length="middle" rot="R180"/>
<pin name="53" x="45.72" y="-25.4" visible="pad" length="middle" rot="R180"/>
<pin name="54" x="45.72" y="-22.86" visible="pad" length="middle" rot="R180"/>
<pin name="55" x="45.72" y="-20.32" visible="pad" length="middle" rot="R180"/>
<pin name="56" x="45.72" y="-17.78" visible="pad" length="middle" rot="R180"/>
<pin name="57" x="45.72" y="-15.24" visible="pad" length="middle" rot="R180"/>
<pin name="58" x="45.72" y="-12.7" visible="pad" length="middle" rot="R180"/>
<pin name="59" x="45.72" y="-10.16" visible="pad" length="middle" rot="R180"/>
<pin name="60" x="45.72" y="-7.62" visible="pad" length="middle" rot="R180"/>
<pin name="61" x="45.72" y="-5.08" visible="pad" length="middle" rot="R180"/>
<pin name="62" x="45.72" y="-2.54" visible="pad" length="middle" rot="R180"/>
<pin name="63" x="45.72" y="0" visible="pad" length="middle" rot="R180"/>
<pin name="64" x="45.72" y="2.54" visible="pad" length="middle" rot="R180"/>
<pin name="65" x="45.72" y="5.08" visible="pad" length="middle" rot="R180"/>
<pin name="66" x="45.72" y="7.62" visible="pad" length="middle" rot="R180"/>
<pin name="67" x="45.72" y="10.16" visible="pad" length="middle" rot="R180"/>
<pin name="68" x="45.72" y="12.7" visible="pad" length="middle" rot="R180"/>
<pin name="69" x="45.72" y="15.24" visible="pad" length="middle" rot="R180"/>
<pin name="70" x="45.72" y="17.78" visible="pad" length="middle" rot="R180"/>
<pin name="71" x="45.72" y="20.32" visible="pad" length="middle" rot="R180"/>
<pin name="72" x="45.72" y="22.86" visible="pad" length="middle" rot="R180"/>
<pin name="73" x="45.72" y="25.4" visible="pad" length="middle" rot="R180"/>
<pin name="74" x="45.72" y="27.94" visible="pad" length="middle" rot="R180"/>
<pin name="75" x="45.72" y="30.48" visible="pad" length="middle" rot="R180"/>
<pin name="76" x="30.48" y="45.72" visible="pad" length="middle" rot="R270"/>
<pin name="77" x="27.94" y="45.72" visible="pad" length="middle" rot="R270"/>
<pin name="78" x="25.4" y="45.72" visible="pad" length="middle" rot="R270"/>
<pin name="79" x="22.86" y="45.72" visible="pad" length="middle" rot="R270"/>
<pin name="80" x="20.32" y="45.72" visible="pad" length="middle" rot="R270"/>
<pin name="81" x="17.78" y="45.72" visible="pad" length="middle" rot="R270"/>
<pin name="82" x="15.24" y="45.72" visible="pad" length="middle" rot="R270"/>
<pin name="83" x="12.7" y="45.72" visible="pad" length="middle" rot="R270"/>
<pin name="84" x="10.16" y="45.72" visible="pad" length="middle" rot="R270"/>
<pin name="85" x="7.62" y="45.72" visible="pad" length="middle" rot="R270"/>
<pin name="86" x="5.08" y="45.72" visible="pad" length="middle" rot="R270"/>
<pin name="87" x="2.54" y="45.72" visible="pad" length="middle" rot="R270"/>
<pin name="88" x="0" y="45.72" visible="pad" length="middle" rot="R270"/>
<pin name="89" x="-2.54" y="45.72" visible="pad" length="middle" rot="R270"/>
<pin name="90" x="-5.08" y="45.72" visible="pad" length="middle" rot="R270"/>
<pin name="91" x="-7.62" y="45.72" visible="pad" length="middle" rot="R270"/>
<pin name="92" x="-10.16" y="45.72" visible="pad" length="middle" rot="R270"/>
<pin name="93" x="-12.7" y="45.72" visible="pad" length="middle" rot="R270"/>
<pin name="94" x="-15.24" y="45.72" visible="pad" length="middle" rot="R270"/>
<pin name="95" x="-17.78" y="45.72" visible="pad" length="middle" rot="R270"/>
<pin name="96" x="-20.32" y="45.72" visible="pad" length="middle" rot="R270"/>
<pin name="97" x="-22.86" y="45.72" visible="pad" length="middle" rot="R270"/>
<pin name="98" x="-25.4" y="45.72" visible="pad" length="middle" rot="R270"/>
<pin name="99" x="-27.94" y="45.72" visible="pad" length="middle" rot="R270"/>
<pin name="100" x="-30.48" y="45.72" visible="pad" length="middle" rot="R270"/>
<text x="-39.624" y="30.48" size="1.016" layer="94" ratio="15">PA00</text>
<text x="-39.624" y="27.686" size="1.016" layer="94" ratio="15">PA01</text>
<text x="-39.624" y="25.4" size="1.016" layer="94" ratio="15">PC00</text>
<text x="-39.624" y="22.86" size="1.016" layer="94" ratio="15">PC01</text>
<text x="-39.624" y="20.32" size="1.016" layer="94" ratio="15">PC02</text>
<text x="-39.624" y="17.78" size="1.016" layer="94" ratio="15">PC03</text>
<text x="-39.624" y="15.24" size="1.016" layer="94" ratio="15">PA02</text>
<text x="-39.624" y="12.7" size="1.016" layer="94" ratio="15">PA03</text>
<text x="-39.624" y="10.16" size="1.016" layer="94" ratio="15">PB04</text>
<text x="-39.624" y="7.62" size="1.016" layer="94" ratio="15">PB05</text>
<text x="-39.624" y="5.08" size="1.016" layer="94" ratio="15">GNDANA</text>
<text x="-39.624" y="2.54" size="1.016" layer="94" ratio="15">VDDANA</text>
<text x="-39.624" y="0" size="1.016" layer="94" ratio="15">PB06</text>
<text x="-39.624" y="-2.54" size="1.016" layer="94" ratio="15">PB07</text>
<text x="-39.624" y="-5.08" size="1.016" layer="94" ratio="15">PB08</text>
<text x="-39.624" y="-7.62" size="1.016" layer="94" ratio="15">PB09</text>
<text x="-39.624" y="-10.16" size="1.016" layer="94" ratio="15">PA04</text>
<text x="-39.624" y="-12.7" size="1.016" layer="94" ratio="15">PA05</text>
<text x="-39.624" y="-15.24" size="1.016" layer="94" ratio="15">PA06</text>
<text x="-39.624" y="-17.78" size="1.016" layer="94" ratio="15">PA07</text>
<text x="-39.624" y="-20.32" size="1.016" layer="94" ratio="15">PC05</text>
<text x="-39.624" y="-22.86" size="1.016" layer="94" ratio="15">PC06</text>
<text x="-39.624" y="-25.4" size="1.016" layer="94" ratio="15">PC07</text>
<text x="-39.624" y="-27.94" size="1.016" layer="94" ratio="15">GND</text>
<text x="-39.624" y="-30.48" size="1.016" layer="94" ratio="15">VDDIOB</text>
<text x="-30.48" y="-39.624" size="1.016" layer="94" ratio="15" rot="R90">PA08</text>
<text x="-27.686" y="-39.624" size="1.016" layer="94" ratio="15" rot="R90">PA09</text>
<text x="-25.4" y="-39.624" size="1.016" layer="94" ratio="15" rot="R90">PA10</text>
<text x="-22.86" y="-39.624" size="1.016" layer="94" ratio="15" rot="R90">PA11</text>
<text x="-20.32" y="-39.624" size="1.016" layer="94" ratio="15" rot="R90">VDDIOB</text>
<text x="-17.78" y="-39.624" size="1.016" layer="94" ratio="15" rot="R90">GND</text>
<text x="-15.24" y="-39.624" size="1.016" layer="94" ratio="15" rot="R90">PB10</text>
<text x="-12.7" y="-39.624" size="1.016" layer="94" ratio="15" rot="R90">PB11</text>
<text x="-10.16" y="-39.624" size="1.016" layer="94" ratio="15" rot="R90">PB12</text>
<text x="-7.62" y="-39.624" size="1.016" layer="94" ratio="15" rot="R90">PB13</text>
<text x="-5.08" y="-39.624" size="1.016" layer="94" ratio="15" rot="R90">PB14</text>
<text x="-2.54" y="-39.624" size="1.016" layer="94" ratio="15" rot="R90">PB15</text>
<text x="0" y="-39.624" size="1.016" layer="94" ratio="15" rot="R90">GND</text>
<text x="2.54" y="-39.624" size="1.016" layer="94" ratio="15" rot="R90">VDDIO</text>
<text x="5.08" y="-39.624" size="1.016" layer="94" ratio="15" rot="R90">PC10</text>
<text x="7.62" y="-39.624" size="1.016" layer="94" ratio="15" rot="R90">PC11</text>
<text x="10.16" y="-39.624" size="1.016" layer="94" ratio="15" rot="R90">PC12</text>
<text x="12.7" y="-39.624" size="1.016" layer="94" ratio="15" rot="R90">PC13</text>
<text x="15.24" y="-39.624" size="1.016" layer="94" ratio="15" rot="R90">PC14</text>
<text x="17.78" y="-39.624" size="1.016" layer="94" ratio="15" rot="R90">PC15</text>
<text x="20.32" y="-39.624" size="1.016" layer="94" ratio="15" rot="R90">PA12</text>
<text x="22.86" y="-39.624" size="1.016" layer="94" ratio="15" rot="R90">PA13</text>
<text x="25.4" y="-39.624" size="1.016" layer="94" ratio="15" rot="R90">PA14</text>
<text x="27.94" y="-39.624" size="1.016" layer="94" ratio="15" rot="R90">PA15</text>
<text x="30.48" y="-39.624" size="1.016" layer="94" ratio="15" rot="R90">GND</text>
<text x="39.624" y="-30.48" size="1.016" layer="94" ratio="15" rot="R180">VDDIO</text>
<text x="39.624" y="-27.686" size="1.016" layer="94" ratio="15" rot="R180">PA16</text>
<text x="39.624" y="-25.4" size="1.016" layer="94" ratio="15" rot="R180">PA17</text>
<text x="39.624" y="-22.86" size="1.016" layer="94" ratio="15" rot="R180">PA18</text>
<text x="39.624" y="-20.32" size="1.016" layer="94" ratio="15" rot="R180">PA19</text>
<text x="39.624" y="-17.78" size="1.016" layer="94" ratio="15" rot="R180">PC16</text>
<text x="39.624" y="-15.24" size="1.016" layer="94" ratio="15" rot="R180">PC17</text>
<text x="39.624" y="-12.7" size="1.016" layer="94" ratio="15" rot="R180">PC18</text>
<text x="39.624" y="-10.16" size="1.016" layer="94" ratio="15" rot="R180">PC19</text>
<text x="39.624" y="-7.62" size="1.016" layer="94" ratio="15" rot="R180">PC20</text>
<text x="39.624" y="-5.08" size="1.016" layer="94" ratio="15" rot="R180">PC21</text>
<text x="39.624" y="-2.54" size="1.016" layer="94" ratio="15" rot="R180">GND</text>
<text x="39.624" y="0" size="1.016" layer="94" ratio="15" rot="R180">VDDIO</text>
<text x="39.624" y="2.54" size="1.016" layer="94" ratio="15" rot="R180">PB16</text>
<text x="39.624" y="5.08" size="1.016" layer="94" ratio="15" rot="R180">PB17</text>
<text x="39.624" y="7.62" size="1.016" layer="94" ratio="15" rot="R180">PB18</text>
<text x="39.624" y="10.16" size="1.016" layer="94" ratio="15" rot="R180">PB19</text>
<text x="39.624" y="12.7" size="1.016" layer="94" ratio="15" rot="R180">PB20</text>
<text x="39.624" y="15.24" size="1.016" layer="94" ratio="15" rot="R180">PB21</text>
<text x="39.624" y="17.78" size="1.016" layer="94" ratio="15" rot="R180">PA20</text>
<text x="39.624" y="20.32" size="1.016" layer="94" ratio="15" rot="R180">PA21</text>
<text x="39.624" y="22.86" size="1.016" layer="94" ratio="15" rot="R180">PA22</text>
<text x="39.624" y="25.4" size="1.016" layer="94" ratio="15" rot="R180">PA23</text>
<text x="39.624" y="27.94" size="1.016" layer="94" ratio="15" rot="R180">PA24</text>
<text x="39.624" y="30.48" size="1.016" layer="94" ratio="15" rot="R180">PA25</text>
<text x="30.48" y="39.624" size="1.016" layer="94" ratio="15" rot="R270">GND</text>
<text x="27.686" y="39.624" size="1.016" layer="94" ratio="15" rot="R270">VDDIO</text>
<text x="25.4" y="39.624" size="1.016" layer="94" ratio="15" rot="R270">PB22</text>
<text x="22.86" y="39.624" size="1.016" layer="94" ratio="15" rot="R270">PB23</text>
<text x="20.32" y="39.624" size="1.016" layer="94" ratio="15" rot="R270">PB24</text>
<text x="17.78" y="39.624" size="1.016" layer="94" ratio="15" rot="R270">PB25</text>
<text x="15.24" y="39.624" size="1.016" layer="94" ratio="15" rot="R270">PC24</text>
<text x="12.7" y="39.624" size="1.016" layer="94" ratio="15" rot="R270">PC25</text>
<text x="10.16" y="39.624" size="1.016" layer="94" ratio="15" rot="R270">PC26</text>
<text x="7.62" y="39.624" size="1.016" layer="94" ratio="15" rot="R270">PC27</text>
<text x="5.08" y="39.624" size="1.016" layer="94" ratio="15" rot="R270">PC28</text>
<text x="2.54" y="39.624" size="1.016" layer="94" ratio="15" rot="R270">PA27</text>
<text x="0" y="39.624" size="1.016" layer="94" ratio="15" rot="R270">RESETN</text>
<text x="-2.54" y="39.624" size="1.016" layer="94" ratio="15" rot="R270">VDDCORE</text>
<text x="-5.08" y="39.624" size="1.016" layer="94" ratio="15" rot="R270">GND</text>
<text x="-7.62" y="39.624" size="1.016" layer="94" ratio="15" rot="R270">VSW</text>
<text x="-10.16" y="39.624" size="1.016" layer="94" ratio="15" rot="R270">VDDIO</text>
<text x="-12.7" y="39.624" size="1.016" layer="94" ratio="15" rot="R270">PA30</text>
<text x="-15.24" y="39.624" size="1.016" layer="94" ratio="15" rot="R270">PA31</text>
<text x="-17.78" y="39.624" size="1.016" layer="94" ratio="15" rot="R270">PB30</text>
<text x="-20.32" y="39.624" size="1.016" layer="94" ratio="15" rot="R270">PB31</text>
<text x="-22.86" y="39.624" size="1.016" layer="94" ratio="15" rot="R270">PB00</text>
<text x="-25.4" y="39.624" size="1.016" layer="94" ratio="15" rot="R270">PB01</text>
<text x="-27.94" y="39.624" size="1.016" layer="94" ratio="15" rot="R270">PB02</text>
<text x="-30.48" y="39.624" size="1.016" layer="94" ratio="15" rot="R270">PB03</text>
<text x="-40.64" y="55.88" size="1.778" layer="95">&gt;NAME</text>
<text x="-40.64" y="53.34" size="1.778" layer="96">&gt;VALUE</text>
<wire x1="-40.64" y1="35.56" x2="-40.64" y2="-40.64" width="0.254" layer="94"/>
<wire x1="-40.64" y1="-40.64" x2="40.64" y2="-40.64" width="0.254" layer="94"/>
<wire x1="40.64" y1="-40.64" x2="40.64" y2="40.64" width="0.254" layer="94"/>
<wire x1="40.64" y1="40.64" x2="-35.56" y2="40.64" width="0.254" layer="94"/>
<wire x1="-35.56" y1="40.64" x2="-40.64" y2="35.56" width="0.254" layer="94"/>
</symbol>
<symbol name="CAPACITOR" urn="urn:adsk.eagle:symbol:8413617/1" library_version="40" library_locally_modified="yes">
<wire x1="-2.54" y1="0" x2="-0.762" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="0.762" y2="0" width="0.1524" layer="94"/>
<text x="-2.54" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-4.318" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-1.524" y1="-0.254" x2="2.54" y2="0.254" layer="94" rot="R90"/>
<rectangle x1="-2.54" y1="-0.254" x2="1.524" y2="0.254" layer="94" rot="R90"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="EEPROM_8PIN" urn="urn:adsk.eagle:symbol:8413595/1" library_version="40" library_locally_modified="yes">
<text x="0.508" y="-5.08" size="1.27" layer="94">!CS</text>
<text x="0.508" y="-7.62" size="1.27" layer="94">SO</text>
<text x="0.508" y="-10.16" size="1.27" layer="94">!WP</text>
<text x="0.508" y="-12.7" size="1.27" layer="94">VSS</text>
<text x="12.192" y="-12.7" size="1.27" layer="94" align="bottom-right">SI</text>
<text x="12.192" y="-10.16" size="1.27" layer="94" align="bottom-right">SCK</text>
<text x="12.192" y="-7.62" size="1.27" layer="94" align="bottom-right">!HOLD</text>
<text x="12.192" y="-5.08" size="1.27" layer="94" align="bottom-right">VCC</text>
<text x="-2.54" y="2.54" size="1.778" layer="95" ratio="15">&gt;NAME</text>
<text x="-2.54" y="0.508" size="1.778" layer="96" ratio="15">&gt;VALUE</text>
<wire x1="0" y1="-2.54" x2="0" y2="-15.24" width="0.254" layer="94"/>
<wire x1="0" y1="-15.24" x2="12.7" y2="-15.24" width="0.254" layer="94"/>
<wire x1="12.7" y1="-15.24" x2="12.7" y2="0" width="0.254" layer="94"/>
<wire x1="12.7" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="0" y2="-2.54" width="0.254" layer="94"/>
<pin name="1" x="-2.54" y="-5.08" visible="pad" length="short"/>
<pin name="2" x="-2.54" y="-7.62" visible="pad" length="short"/>
<pin name="3" x="-2.54" y="-10.16" visible="pad" length="short"/>
<pin name="4" x="-2.54" y="-12.7" visible="pad" length="short"/>
<pin name="5" x="15.24" y="-12.7" visible="pad" length="short" rot="R180"/>
<pin name="6" x="15.24" y="-10.16" visible="pad" length="short" rot="R180"/>
<pin name="7" x="15.24" y="-7.62" visible="pad" length="short" rot="R180"/>
<pin name="8" x="15.24" y="-5.08" visible="pad" length="short" rot="R180"/>
</symbol>
<symbol name="SWITCH" urn="urn:adsk.eagle:symbol:8413599/1" library_version="40" library_locally_modified="yes">
<pin name="1" x="-5.08" y="0" visible="pad" length="short"/>
<pin name="2" x="5.08" y="0" visible="pad" length="short" rot="R180"/>
<wire x1="-2.54" y1="0" x2="-1.016" y2="0" width="0.254" layer="94"/>
<wire x1="-1.016" y1="0" x2="0.508" y2="1.524" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="1.016" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="2.54" size="1.016" layer="95" font="vector" ratio="15">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.016" layer="96" font="vector" ratio="15">&gt;VALUE</text>
</symbol>
<symbol name="CRYSTAL_3-POS" urn="urn:adsk.eagle:symbol:8413601/1" library_version="40" library_locally_modified="yes">
<wire x1="1.016" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.016" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.381" y1="1.524" x2="-0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="-0.381" y1="-1.524" x2="0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="-1.524" x2="0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="1.524" x2="-0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="1.016" y1="1.778" x2="1.016" y2="0" width="0.254" layer="94"/>
<wire x1="1.016" y1="0" x2="1.016" y2="-1.778" width="0.254" layer="94"/>
<wire x1="-1.016" y1="1.778" x2="-1.016" y2="0" width="0.254" layer="94"/>
<wire x1="-1.016" y1="0" x2="-1.016" y2="-1.778" width="0.254" layer="94"/>
<wire x1="-1.778" y1="-1.524" x2="-1.778" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-1.778" y1="-2.54" x2="1.778" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.778" y1="-2.54" x2="1.778" y2="-1.524" width="0.254" layer="94"/>
<wire x1="-1.778" y1="2.54" x2="1.778" y2="2.54" width="0.254" layer="94"/>
<wire x1="1.778" y1="2.54" x2="1.778" y2="1.524" width="0.254" layer="94"/>
<wire x1="-1.778" y1="2.54" x2="-1.778" y2="1.524" width="0.254" layer="94"/>
<text x="2.54" y="2.54" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="2.54" y="4.064" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="1"/>
<pin name="3" x="0" y="-5.08" visible="pad" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
<symbol name="LED" urn="urn:adsk.eagle:symbol:8413629/1" library_version="41" library_locally_modified="yes">
<wire x1="-1.016" y1="-1.27" x2="1.524" y2="0" width="0.254" layer="94"/>
<wire x1="1.524" y1="0" x2="-1.016" y2="1.27" width="0.254" layer="94"/>
<wire x1="-1.016" y1="1.27" x2="-1.016" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.524" y1="1.27" x2="1.524" y2="0" width="0.254" layer="94"/>
<wire x1="1.524" y1="0" x2="1.524" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-0.635" y1="1.143" x2="0.635" y2="2.413" width="0.1524" layer="94"/>
<wire x1="0.635" y1="2.413" x2="-0.127" y2="2.286" width="0.1524" layer="94"/>
<wire x1="0.635" y1="2.413" x2="0.508" y2="1.651" width="0.1524" layer="94"/>
<wire x1="0.254" y1="0.762" x2="1.27" y2="1.778" width="0.1524" layer="94"/>
<wire x1="1.27" y1="1.778" x2="0.508" y2="1.651" width="0.1524" layer="94"/>
<wire x1="1.27" y1="1.778" x2="1.143" y2="1.016" width="0.1524" layer="94"/>
<pin name="A" x="-3.556" y="0" visible="off" length="short" direction="pas"/>
<pin name="K" x="4.064" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<text x="-3.556" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.556" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="DIODE_SCHOTTKY" urn="urn:adsk.eagle:symbol:8413616/1" library_version="43">
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.905" y1="1.27" x2="1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.905" y1="1.27" x2="1.905" y2="1.016" width="0.254" layer="94"/>
<wire x1="1.27" y1="-1.27" x2="0.635" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0.635" y1="-1.016" x2="0.635" y2="-1.27" width="0.254" layer="94"/>
<text x="-2.54" y="1.905" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.778" layer="96">&gt;VALUE</text>
<pin name="A" x="-3.81" y="0" visible="off" length="short" direction="pas"/>
<pin name="K" x="3.81" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="TI_LM22672MRX-ADJ" urn="urn:adsk.eagle:symbol:8413651/1" library_version="43">
<pin name="BOOT" x="25.4" y="-5.08" visible="pad" length="middle" rot="R180"/>
<pin name="SS" x="-5.08" y="-17.78" visible="pad" length="middle"/>
<pin name="RT/SYNC" x="-5.08" y="-12.7" visible="pad" length="middle"/>
<pin name="FB" x="25.4" y="-10.16" visible="pad" length="middle" rot="R180"/>
<pin name="EN" x="-5.08" y="-7.62" visible="pad" length="middle"/>
<pin name="GND" x="25.4" y="-15.24" visible="pad" length="middle" rot="R180"/>
<pin name="VIN" x="-5.08" y="-2.54" visible="pad" length="middle"/>
<pin name="SW" x="25.4" y="-2.54" visible="pad" length="middle" rot="R180"/>
<pin name="PAD" x="25.4" y="-17.78" visible="pad" length="middle" rot="R180"/>
<wire x1="0" y1="0" x2="0" y2="-20.32" width="0.1524" layer="94"/>
<wire x1="0" y1="-20.32" x2="20.32" y2="-20.32" width="0.1524" layer="94"/>
<wire x1="20.32" y1="-20.32" x2="20.32" y2="0" width="0.1524" layer="94"/>
<wire x1="20.32" y1="0" x2="0" y2="0" width="0.1524" layer="94"/>
<text x="5.08" y="2.54" size="2.0828" layer="95" ratio="6" rot="SR0">&gt;Name</text>
<text x="5.08" y="0.254" size="2.0828" layer="96" ratio="6" rot="SR0">&gt;Value</text>
<text x="2.54" y="-2.54" size="1.778" layer="94">VIN</text>
<text x="2.54" y="-7.62" size="1.778" layer="94">EN</text>
<text x="2.54" y="-12.7" size="1.778" layer="94">RT/SYNC</text>
<text x="2.54" y="-17.78" size="1.778" layer="94">SS</text>
<text x="17.78" y="-0.762" size="1.778" layer="94" rot="R180">SW</text>
<text x="17.78" y="-3.302" size="1.778" layer="94" rot="R180">BOOT</text>
<text x="17.78" y="-8.382" size="1.778" layer="94" rot="R180">FB</text>
<text x="17.78" y="-13.716" size="1.778" layer="94" rot="R180">GND</text>
<text x="17.78" y="-16.002" size="1.778" layer="94" rot="R180">PAD</text>
</symbol>
<symbol name="TRANSCEIVER_RS485" urn="urn:adsk.eagle:symbol:8413649/1" library_version="44">
<text x="1.27" y="-7.62" size="1.778" layer="94" ratio="15">R0</text>
<text x="1.27" y="-12.7" size="1.778" layer="94" ratio="15">!RE</text>
<text x="1.27" y="-17.78" size="1.778" layer="94" ratio="15">DE</text>
<text x="1.27" y="-22.86" size="1.778" layer="94" ratio="15">DI</text>
<text x="19.05" y="-22.86" size="1.778" layer="94" ratio="15" align="bottom-right">GND</text>
<text x="19.05" y="-17.78" size="1.778" layer="94" ratio="15" align="bottom-right">A(D0/RI)</text>
<text x="19.05" y="-12.7" size="1.778" layer="94" ratio="15" align="bottom-right">B(!D0!/!RI!)</text>
<text x="19.05" y="-7.62" size="1.778" layer="94" ratio="15" align="bottom-right">VCC</text>
<text x="6.35" y="-2.54" size="1.778" layer="94" ratio="15">RS485</text>
<text x="0" y="2.54" size="1.778" layer="95" ratio="15">&gt;NAME</text>
<text x="0" y="-27.94" size="1.778" layer="96" ratio="15">&gt;VALUE</text>
<wire x1="0" y1="-2.54" x2="0" y2="-25.4" width="0.254" layer="94"/>
<wire x1="0" y1="-25.4" x2="20.32" y2="-25.4" width="0.254" layer="94"/>
<wire x1="20.32" y1="-25.4" x2="20.32" y2="0" width="0.254" layer="94"/>
<wire x1="20.32" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="0" y2="-2.54" width="0.254" layer="94"/>
<pin name="1" x="-2.54" y="-7.62" visible="pad" length="short"/>
<pin name="2" x="-2.54" y="-12.7" visible="pad" length="short"/>
<pin name="3" x="-2.54" y="-17.78" visible="pad" length="short"/>
<pin name="4" x="-2.54" y="-22.86" visible="pad" length="short"/>
<pin name="5" x="22.86" y="-22.86" visible="pad" length="short" rot="R180"/>
<pin name="6" x="22.86" y="-17.78" visible="pad" length="short" rot="R180"/>
<pin name="7" x="22.86" y="-12.7" visible="pad" length="short" rot="R180"/>
<pin name="8" x="22.86" y="-7.62" visible="pad" length="short" rot="R180"/>
</symbol>
<symbol name="TI_TCA9406" urn="urn:adsk.eagle:symbol:8413654/1" library_version="44">
<pin name="1" x="-5.08" y="-5.08" visible="pad" length="middle"/>
<pin name="2" x="-5.08" y="-10.16" visible="pad" length="middle"/>
<pin name="3" x="-5.08" y="-15.24" visible="pad" length="middle"/>
<pin name="4" x="-5.08" y="-20.32" visible="pad" length="middle"/>
<pin name="5" x="20.32" y="-20.32" visible="pad" length="middle" rot="R180"/>
<pin name="6" x="20.32" y="-15.24" visible="pad" length="middle" rot="R180"/>
<pin name="7" x="20.32" y="-10.16" visible="pad" length="middle" rot="R180"/>
<pin name="8" x="20.32" y="-5.08" visible="pad" length="middle" rot="R180"/>
<wire x1="0" y1="-2.54" x2="0" y2="-22.86" width="0.254" layer="94"/>
<wire x1="0" y1="-22.86" x2="15.24" y2="-22.86" width="0.254" layer="94"/>
<wire x1="15.24" y1="-22.86" x2="15.24" y2="0" width="0.254" layer="94"/>
<wire x1="15.24" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="0" y2="-2.54" width="0.254" layer="94"/>
<text x="1.27" y="-5.08" size="1.27" layer="94" ratio="15">SDA_B</text>
<text x="1.27" y="-10.16" size="1.27" layer="94" ratio="15">GND</text>
<text x="1.27" y="-15.24" size="1.27" layer="94" ratio="15">VCCA</text>
<text x="1.27" y="-20.32" size="1.27" layer="94" ratio="15">SDA_A</text>
<text x="13.97" y="-20.32" size="1.27" layer="94" ratio="15" align="bottom-right">SCL_A</text>
<text x="13.97" y="-15.24" size="1.27" layer="94" ratio="15" align="bottom-right">OE</text>
<text x="13.97" y="-10.16" size="1.27" layer="94" ratio="15" align="bottom-right">VCCB</text>
<text x="13.97" y="-5.08" size="1.27" layer="94" ratio="15" align="bottom-right">SCL_B</text>
<text x="0" y="2.54" size="1.27" layer="95" ratio="15">&gt;NAME</text>
<text x="0" y="5.08" size="1.27" layer="95" ratio="15">&gt;VALUE</text>
<text x="3.81" y="-2.54" size="1.27" layer="94" ratio="15">TCA9406</text>
</symbol>
<symbol name="TRANSISTOR_PNP" urn="urn:adsk.eagle:symbol:8413604/1" library_version="42">
<wire x1="2.0861" y1="1.6779" x2="1.5781" y2="2.5941" width="0.1524" layer="94"/>
<wire x1="1.5781" y1="2.5941" x2="0.5159" y2="1.478" width="0.1524" layer="94"/>
<wire x1="0.5159" y1="1.478" x2="2.0861" y2="1.6779" width="0.1524" layer="94"/>
<wire x1="2.54" y1="2.54" x2="1.808" y2="2.1239" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="0.508" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="1.905" y1="1.778" x2="1.524" y2="2.413" width="0.254" layer="94"/>
<wire x1="1.524" y1="2.413" x2="0.762" y2="1.651" width="0.254" layer="94"/>
<wire x1="0.762" y1="1.651" x2="1.778" y2="1.778" width="0.254" layer="94"/>
<wire x1="1.778" y1="1.778" x2="1.524" y2="2.159" width="0.254" layer="94"/>
<wire x1="1.524" y1="2.159" x2="1.143" y2="1.905" width="0.254" layer="94"/>
<wire x1="1.143" y1="1.905" x2="1.524" y2="1.905" width="0.254" layer="94"/>
<text x="-10.16" y="7.62" size="1.778" layer="95">&gt;NAME</text>
<text x="-10.16" y="5.08" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-2.54" x2="0.508" y2="2.54" layer="94"/>
<pin name="B" x="-2.54" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="E" x="2.54" y="5.08" visible="off" length="short" direction="pas" swaplevel="2" rot="R270"/>
<pin name="C" x="2.54" y="-5.08" visible="off" length="short" direction="pas" swaplevel="3" rot="R90"/>
</symbol>
<symbol name="LM317_REGULATOR" urn="urn:adsk.eagle:symbol:8413642/1" library_version="42">
<text x="6.604" y="0" size="1.778" layer="94" ratio="15" rot="R180" align="center-left">VOUT</text>
<text x="-6.604" y="0" size="1.778" layer="94" ratio="15" align="center-left">VIN</text>
<text x="-2.032" y="-3.81" size="1.778" layer="94" ratio="15" align="center-left">ADJ</text>
<text x="-3.81" y="3.556" size="1.778" layer="94" ratio="15" align="center-left">LM317</text>
<text x="-7.62" y="7.62" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.62" y="10.16" size="1.778" layer="96">&gt;VALUE</text>
<wire x1="-7.62" y1="-5.08" x2="7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="7.62" y2="5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="5.08" x2="-7.62" y2="5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="5.08" x2="-7.62" y2="-5.08" width="0.254" layer="94"/>
<pin name="VOUT" x="10.16" y="0" visible="pad" length="short" rot="R180"/>
<pin name="ADJ" x="0" y="-7.62" visible="pad" length="short" rot="R90"/>
<pin name="VIN" x="-10.16" y="0" visible="pad" length="short"/>
</symbol>
<symbol name="DIODE_ZENER" urn="urn:adsk.eagle:symbol:8413614/1" library_version="47">
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="0.635" y1="1.778" x2="1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="-1.27" x2="1.905" y2="-1.778" width="0.254" layer="94"/>
<text x="-2.54" y="1.905" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.778" layer="96">&gt;VALUE</text>
<pin name="A" x="-3.81" y="0" visible="off" length="short" direction="pas"/>
<pin name="K" x="3.81" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="MOSFET-PCH" urn="urn:adsk.eagle:symbol:8413607/1" library_version="63">
<wire x1="-2.54" y1="-2.54" x2="-1.2192" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="0.762" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-0.762" width="0.254" layer="94"/>
<wire x1="0" y1="3.683" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="1.397" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.397" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-3.683" width="0.254" layer="94"/>
<wire x1="-1.143" y1="2.54" x2="-1.143" y2="-2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-0.508" x2="5.08" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="5.08" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="2.54" x2="5.08" y2="2.54" width="0.1524" layer="94"/>
<wire x1="4.572" y1="-0.508" x2="5.08" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-0.508" x2="5.588" y2="-0.508" width="0.1524" layer="94"/>
<circle x="2.54" y="-2.54" radius="0.3592" width="0" layer="94"/>
<circle x="2.54" y="2.54" radius="0.3592" width="0" layer="94"/>
<text x="-11.43" y="0" size="1.778" layer="96">&gt;VALUE</text>
<text x="-11.43" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<pin name="S" x="2.54" y="-7.62" visible="off" length="middle" direction="pas" rot="R90"/>
<pin name="G" x="-5.08" y="-2.54" visible="off" length="short" direction="pas"/>
<pin name="D" x="2.54" y="7.62" visible="off" length="middle" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="5.08" y="-0.508"/>
<vertex x="5.588" y="0.254"/>
<vertex x="4.572" y="0.254"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="2.286" y="0"/>
<vertex x="1.27" y="0.762"/>
<vertex x="1.27" y="-0.762"/>
</polygon>
</symbol>
<symbol name="MOSFET-NCH" urn="urn:adsk.eagle:symbol:8413608/1" library_version="40">
<wire x1="-2.54" y1="-2.54" x2="-1.2192" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="0.762" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-0.762" width="0.254" layer="94"/>
<wire x1="0" y1="3.683" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="1.397" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.397" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-3.683" width="0.254" layer="94"/>
<wire x1="-1.143" y1="2.54" x2="-1.143" y2="-2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="0.508" width="0.1524" layer="94"/>
<wire x1="5.08" y1="0.508" x2="5.08" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="5.08" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="2.54" x2="5.08" y2="2.54" width="0.1524" layer="94"/>
<wire x1="5.588" y1="0.508" x2="5.08" y2="0.508" width="0.1524" layer="94"/>
<wire x1="5.08" y1="0.508" x2="4.572" y2="0.508" width="0.1524" layer="94"/>
<circle x="2.54" y="-2.54" radius="0.3592" width="0" layer="94"/>
<circle x="2.54" y="2.54" radius="0.3592" width="0" layer="94"/>
<text x="-11.43" y="0" size="1.778" layer="96">&gt;VALUE</text>
<text x="-11.43" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<pin name="S" x="2.54" y="-7.62" visible="off" length="middle" direction="pas" rot="R90"/>
<pin name="G" x="-5.08" y="-2.54" visible="off" length="short" direction="pas"/>
<pin name="D" x="2.54" y="7.62" visible="off" length="middle" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="5.08" y="0.508"/>
<vertex x="4.572" y="-0.254"/>
<vertex x="5.588" y="-0.254"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="0.254" y="0"/>
<vertex x="1.27" y="0.762"/>
<vertex x="1.27" y="-0.762"/>
</polygon>
</symbol>
<symbol name="DIODE" urn="urn:adsk.eagle:symbol:8413615/1" library_version="40" library_locally_modified="yes">
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<text x="-2.54" y="1.905" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.778" layer="96">&gt;VALUE</text>
<pin name="A" x="-3.81" y="0" visible="off" length="short" direction="pas"/>
<pin name="K" x="3.81" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="TI_TPS2491DGSR" urn="urn:adsk.eagle:symbol:8413655/1" library_version="43">
<wire x1="20.32" y1="0" x2="20.32" y2="-10.16" width="0.254" layer="94"/>
<wire x1="20.32" y1="-10.16" x2="0" y2="-10.16" width="0.254" layer="94"/>
<wire x1="0" y1="-10.16" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="20.32" y2="0" width="0.254" layer="94"/>
<pin name="10_VCC" x="-2.54" y="-2.54" visible="pad" length="short"/>
<pin name="1_EN" x="-2.54" y="-5.08" visible="pad" length="short"/>
<pin name="2_VREF" x="-2.54" y="-7.62" visible="pad" length="short"/>
<pin name="3_PROG" x="5.08" y="-12.7" visible="pad" length="short" rot="R90"/>
<pin name="4_TIMER" x="10.16" y="-12.7" visible="pad" length="short" rot="R90"/>
<pin name="5_GND" x="15.24" y="-12.7" visible="pad" length="short" rot="R90"/>
<pin name="6_PG" x="22.86" y="-2.54" visible="pad" length="short" rot="R180"/>
<pin name="9_SENSE" x="5.08" y="2.54" visible="pad" length="short" rot="R270"/>
<pin name="8_GATE" x="10.16" y="2.54" visible="pad" length="short" rot="R270"/>
<pin name="7_OUT" x="15.24" y="2.54" visible="pad" length="short" rot="R270"/>
<text x="7.112" y="-5.08" size="1.27" layer="94">TPS2491</text>
<text x="3.556" y="-1.27" size="0.8128" layer="94">SENSE</text>
<text x="8.89" y="-1.27" size="0.8128" layer="94">GATE</text>
<text x="14.224" y="-1.27" size="0.8128" layer="94">OUT</text>
<text x="18.542" y="-2.794" size="0.8128" layer="94">PG</text>
<text x="14.224" y="-9.652" size="0.8128" layer="94">GND</text>
<text x="8.89" y="-9.652" size="0.8128" layer="94">TIMER</text>
<text x="3.81" y="-9.652" size="0.8128" layer="94">PROG</text>
<text x="0.508" y="-8.128" size="0.8128" layer="94">VREF</text>
<text x="0.508" y="-5.588" size="0.8128" layer="94">EN</text>
<text x="0.508" y="-3.048" size="0.8128" layer="94">VCC</text>
<text x="-2.54" y="2.54" size="1.27" layer="95" ratio="15">&gt;NAME</text>
<text x="-2.54" y="5.08" size="1.27" layer="95" ratio="15">&gt;VALUE</text>
</symbol>
<symbol name="NC" urn="urn:adsk.eagle:symbol:8413641/1" library_version="49">
<wire x1="-0.635" y1="0.635" x2="0.635" y2="-0.635" width="0.254" layer="94"/>
<wire x1="0.635" y1="0.635" x2="-0.635" y2="-0.635" width="0.254" layer="94"/>
</symbol>
<symbol name="AMIS_30624" urn="urn:adsk.eagle:symbol:20425763/1" library_version="62">
<pin name="1" x="-5.08" y="-5.08" visible="pad" length="middle"/>
<pin name="2" x="-5.08" y="-7.62" visible="pad" length="middle"/>
<pin name="3" x="-5.08" y="-10.16" visible="pad" length="middle"/>
<pin name="4" x="-5.08" y="-12.7" visible="pad" length="middle"/>
<pin name="5" x="-5.08" y="-15.24" visible="pad" length="middle"/>
<pin name="6" x="-5.08" y="-17.78" visible="pad" length="middle"/>
<pin name="7" x="-5.08" y="-20.32" visible="pad" length="middle"/>
<pin name="8" x="-5.08" y="-22.86" visible="pad" length="middle"/>
<pin name="9" x="-5.08" y="-25.4" visible="pad" length="middle"/>
<pin name="10" x="-5.08" y="-27.94" visible="pad" length="middle"/>
<pin name="11" x="-5.08" y="-30.48" visible="pad" length="middle"/>
<pin name="12" x="-5.08" y="-33.02" visible="pad" length="middle"/>
<pin name="13" x="-5.08" y="-35.56" visible="pad" length="middle"/>
<pin name="14" x="-5.08" y="-38.1" visible="pad" length="middle"/>
<pin name="15" x="-5.08" y="-40.64" visible="pad" length="middle"/>
<pin name="16" x="-5.08" y="-43.18" visible="pad" length="middle"/>
<pin name="17" x="30.48" y="-43.18" visible="pad" length="middle" rot="R180"/>
<pin name="18" x="30.48" y="-40.64" visible="pad" length="middle" rot="R180"/>
<pin name="19" x="30.48" y="-38.1" visible="pad" length="middle" rot="R180"/>
<pin name="20" x="30.48" y="-35.56" visible="pad" length="middle" rot="R180"/>
<pin name="21" x="30.48" y="-33.02" visible="pad" length="middle" rot="R180"/>
<pin name="22" x="30.48" y="-30.48" visible="pad" length="middle" rot="R180"/>
<pin name="23" x="30.48" y="-27.94" visible="pad" length="middle" rot="R180"/>
<pin name="24" x="30.48" y="-25.4" visible="pad" length="middle" rot="R180"/>
<pin name="25" x="30.48" y="-22.86" visible="pad" length="middle" rot="R180"/>
<pin name="26" x="30.48" y="-20.32" visible="pad" length="middle" rot="R180"/>
<pin name="27" x="30.48" y="-17.78" visible="pad" length="middle" rot="R180"/>
<pin name="28" x="30.48" y="-15.24" visible="pad" length="middle" rot="R180"/>
<pin name="29" x="30.48" y="-12.7" visible="pad" length="middle" rot="R180"/>
<pin name="30" x="30.48" y="-10.16" visible="pad" length="middle" rot="R180"/>
<pin name="31" x="30.48" y="-7.62" visible="pad" length="middle" rot="R180"/>
<pin name="32" x="30.48" y="-5.08" visible="pad" length="middle" rot="R180"/>
<pin name="33" x="30.48" y="-2.54" visible="pad" length="middle" rot="R180"/>
<text x="1.27" y="-5.08" size="1.778" layer="94">XP</text>
<text x="1.27" y="-7.62" size="1.778" layer="94">XP</text>
<text x="1.27" y="-10.16" size="1.778" layer="94">VBB</text>
<text x="1.27" y="-12.7" size="1.778" layer="94">VBB</text>
<text x="1.27" y="-15.24" size="1.778" layer="94">VBB</text>
<text x="1.27" y="-17.78" size="1.778" layer="94">SWI</text>
<text x="1.27" y="-20.32" size="1.778" layer="94">NC</text>
<text x="1.27" y="-22.86" size="1.778" layer="94">SDA</text>
<text x="1.27" y="-25.4" size="1.778" layer="94">SCK</text>
<text x="1.27" y="-27.94" size="1.778" layer="94">VDD</text>
<text x="1.27" y="-30.48" size="1.778" layer="94">GND</text>
<text x="1.27" y="-33.02" size="1.778" layer="94">TST1</text>
<text x="1.27" y="-35.56" size="1.778" layer="94">TST2</text>
<text x="1.27" y="-38.1" size="1.778" layer="94">GND</text>
<text x="1.27" y="-40.64" size="1.778" layer="94">HW</text>
<text x="1.27" y="-43.18" size="1.778" layer="94">NC</text>
<text x="24.13" y="-43.18" size="1.778" layer="94" align="bottom-right">CPN</text>
<text x="24.13" y="-40.64" size="1.778" layer="94" align="bottom-right">CPP</text>
<text x="24.13" y="-38.1" size="1.778" layer="94" align="bottom-right">VCP</text>
<text x="24.13" y="-35.56" size="1.778" layer="94" align="bottom-right">VBB</text>
<text x="24.13" y="-33.02" size="1.778" layer="94" align="bottom-right">VBB</text>
<text x="24.13" y="-30.48" size="1.778" layer="94" align="bottom-right">VBB</text>
<text x="24.13" y="-27.94" size="1.778" layer="94" align="bottom-right">YN</text>
<text x="24.13" y="-25.4" size="1.778" layer="94" align="bottom-right">YN</text>
<text x="24.13" y="-22.86" size="1.778" layer="94" align="bottom-right">GND</text>
<text x="24.13" y="-20.32" size="1.778" layer="94" align="bottom-right">GND</text>
<text x="24.13" y="-17.78" size="1.778" layer="94" align="bottom-right">YP</text>
<text x="24.13" y="-15.24" size="1.778" layer="94" align="bottom-right">YP</text>
<text x="24.13" y="-12.7" size="1.778" layer="94" align="bottom-right">XN</text>
<text x="24.13" y="-10.16" size="1.778" layer="94" align="bottom-right">XN</text>
<text x="24.13" y="-7.62" size="1.778" layer="94" align="bottom-right">GND</text>
<text x="24.13" y="-5.08" size="1.778" layer="94" align="bottom-right">GND</text>
<text x="3.81" y="0" size="1.778" layer="94">DC MOTOR DR</text>
<text x="0" y="5.08" size="1.778" layer="95">&gt;NAME</text>
<text x="0" y="7.62" size="1.778" layer="96">&gt;VALUE</text>
<text x="24.13" y="-2.54" size="1.778" layer="94" align="bottom-right">PAD</text>
<wire x1="0" y1="0" x2="0" y2="-45.72" width="0.254" layer="94"/>
<wire x1="0" y1="-45.72" x2="25.4" y2="-45.72" width="0.254" layer="94"/>
<wire x1="25.4" y1="-45.72" x2="25.4" y2="2.54" width="0.254" layer="94"/>
<wire x1="25.4" y1="2.54" x2="2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="2.54" x2="0" y2="0" width="0.254" layer="94"/>
</symbol>
<symbol name="FIDUCIAL" urn="urn:adsk.eagle:symbol:8413613/1" library_version="74">
<circle x="0" y="0" radius="3.81" width="0" layer="94"/>
<circle x="0" y="0" radius="6.35" width="0.254" layer="94"/>
<text x="-4.826" y="8.89" size="1.778" layer="95">&gt;NAME</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="TC2050-IDC-NL" urn="urn:adsk.eagle:component:16170707/6" prefix="J" uservalue="yes" library_version="76">
<description>Cbl Plug-Of-Nails 10-Pin</description>
<gates>
<gate name="-1" symbol="PIN" x="5.08" y="12.7"/>
<gate name="-2" symbol="PIN" x="5.08" y="10.16"/>
<gate name="-3" symbol="PIN" x="5.08" y="7.62"/>
<gate name="-4" symbol="PIN" x="5.08" y="5.08"/>
<gate name="-5" symbol="PIN" x="5.08" y="2.54"/>
<gate name="-6" symbol="PIN" x="5.08" y="0"/>
<gate name="-7" symbol="PIN" x="5.08" y="-2.54"/>
<gate name="-8" symbol="PIN" x="5.08" y="-5.08"/>
<gate name="-9" symbol="PIN" x="5.08" y="-7.62"/>
<gate name="-10" symbol="PIN" x="5.08" y="-10.16"/>
</gates>
<devices>
<device name="10PIN" package="TAG-CONNECT_TC2050-IDC-NL">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-10" pin="1" pad="10"/>
<connect gate="-2" pin="1" pad="2"/>
<connect gate="-3" pin="1" pad="3"/>
<connect gate="-4" pin="1" pad="4"/>
<connect gate="-5" pin="1" pad="5"/>
<connect gate="-6" pin="1" pad="6"/>
<connect gate="-7" pin="1" pad="7"/>
<connect gate="-8" pin="1" pad="8"/>
<connect gate="-9" pin="1" pad="9"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:16170706/4"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="AVAILABILITY" value="Unavailable"/>
<attribute name="DESCRIPTION" value="  Plug-Of-Nails™ Adapter Cable "/>
<attribute name="MF" value="Tag-Connect LLC"/>
<attribute name="MP" value="TC2050-IDC-NL"/>
<attribute name="PACKAGE" value="None"/>
<attribute name="PRICE" value="None"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TP_CLASS_B" urn="urn:adsk.eagle:component:8414518/4" prefix="TP" uservalue="yes" library_version="40" library_locally_modified="yes">
<gates>
<gate name="G$1" symbol="TP" x="0" y="0"/>
</gates>
<devices>
<device name="TP_1.5MM" package="TP_1.5MM">
<connects>
<connect gate="G$1" pin="PP" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414069/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TP_1.5MM_PAD" package="TP_1.5MM_PAD">
<connects>
<connect gate="G$1" pin="PP" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414070/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TP_1.7MM" package="TP_1.7MM">
<connects>
<connect gate="G$1" pin="PP" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414071/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C120H40" package="C120H40">
<connects>
<connect gate="G$1" pin="PP" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414063/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C131H51" package="C131H51">
<connects>
<connect gate="G$1" pin="PP" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414064/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C406H326" package="C406H326">
<connects>
<connect gate="G$1" pin="PP" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414065/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C491H411" package="C491H411">
<connects>
<connect gate="G$1" pin="PP" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414066/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CIR_PAD_1MM" package="CIR_PAD_1MM">
<connects>
<connect gate="G$1" pin="PP" pad="P$1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414067/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CIR_PAD_2MM" package="CIR_PAD_2MM">
<connects>
<connect gate="G$1" pin="PP" pad="P$1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414068/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RESISTOR" urn="urn:adsk.eagle:component:8414463/1" prefix="R" uservalue="yes" library_version="58">
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="0201" package="RESC0603X30">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3811151/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402" package="RESC1005X40">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3811154/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="RESC1608X55">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3811166/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="RESC2012X70">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3811169/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="RESC3115X70">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3811173/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1210" package="RESC3225X70">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3811180/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1812" package="RESC4532X70">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3811188/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2010" package="RESC5025X70">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3811197/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2220" package="RESC5750X229">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3811212/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2512" package="RESC6432X70">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3811284/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1508_WIDE" package="RESC2037X52">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3950966/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="INDUCTOR" urn="urn:adsk.eagle:component:8414465/1" prefix="L" uservalue="yes" library_version="58">
<gates>
<gate name="G$1" symbol="INDUCTOR" x="0" y="0"/>
</gates>
<devices>
<device name="0402" package="INDC1005X60">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3810108/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="INDC1608X95">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3810118/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1008" package="INDC2520X220">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3810123/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1210" package="INDC3225X135">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3810128/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1806" package="INDC4509X190">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3810712/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1812" package="INDC4532X175">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3810715/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2220" package="INDC5750X180">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3810722/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2520" package="INDC6350X200">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3811095/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="4.80MM_X_4.80MM_SMT" package="INDM4848X300">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3837793/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5.00MM_X_5.00MM_SMT" package="INDM5050X430">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3890169/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="4.00MM_X_4.00MM_SMT" package="INDM4040X300">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3890461/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="4.45MM_X_4.06MM_SMT" package="INDM4440X300">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3890484/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="7.00MM_X_7.00MM_SMT" package="INDM7070X450">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3890494/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="4.90MM_X_4.90MM_SMT" package="INDM4949X400">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3904911/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="RADIAL_THT" package="RADIAL_THT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414048/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="6.30MM_X_6.30MM_SMT" package="INDM6363X500">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3920872/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="23.88MM_X_23.88MM_SMT" package="INDM238238X1016">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3949005/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="8.00MM_X_8.00MM_SMT" package="INDM8080X400">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3950981/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="12.5MM_X_12.5MM_SMT" package="INDM125125X650">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3950988/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ATMEL_ATSAMD51N19" urn="urn:adsk.eagle:component:8414472/3" prefix="U" uservalue="yes" library_version="63">
<gates>
<gate name="G$1" symbol="ATMEL_ATSAMD51J-100" x="0" y="0"/>
</gates>
<devices>
<device name="TQFP-100" package="QFP50P1600X1600X120-100">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="100" pad="100"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="13" pad="13"/>
<connect gate="G$1" pin="14" pad="14"/>
<connect gate="G$1" pin="15" pad="15"/>
<connect gate="G$1" pin="16" pad="16"/>
<connect gate="G$1" pin="17" pad="17"/>
<connect gate="G$1" pin="18" pad="18"/>
<connect gate="G$1" pin="19" pad="19"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="20" pad="20"/>
<connect gate="G$1" pin="21" pad="21"/>
<connect gate="G$1" pin="22" pad="22"/>
<connect gate="G$1" pin="23" pad="23"/>
<connect gate="G$1" pin="24" pad="24"/>
<connect gate="G$1" pin="25" pad="25"/>
<connect gate="G$1" pin="26" pad="26"/>
<connect gate="G$1" pin="27" pad="27"/>
<connect gate="G$1" pin="28" pad="28"/>
<connect gate="G$1" pin="29" pad="29"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="30" pad="30"/>
<connect gate="G$1" pin="31" pad="31"/>
<connect gate="G$1" pin="32" pad="32"/>
<connect gate="G$1" pin="33" pad="33"/>
<connect gate="G$1" pin="34" pad="34"/>
<connect gate="G$1" pin="35" pad="35"/>
<connect gate="G$1" pin="36" pad="36"/>
<connect gate="G$1" pin="37" pad="37"/>
<connect gate="G$1" pin="38" pad="38"/>
<connect gate="G$1" pin="39" pad="39"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="40" pad="40"/>
<connect gate="G$1" pin="41" pad="41"/>
<connect gate="G$1" pin="42" pad="42"/>
<connect gate="G$1" pin="43" pad="43"/>
<connect gate="G$1" pin="44" pad="44"/>
<connect gate="G$1" pin="45" pad="45"/>
<connect gate="G$1" pin="46" pad="46"/>
<connect gate="G$1" pin="47" pad="47"/>
<connect gate="G$1" pin="48" pad="48"/>
<connect gate="G$1" pin="49" pad="49"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="50" pad="50"/>
<connect gate="G$1" pin="51" pad="51"/>
<connect gate="G$1" pin="52" pad="52"/>
<connect gate="G$1" pin="53" pad="53"/>
<connect gate="G$1" pin="54" pad="54"/>
<connect gate="G$1" pin="55" pad="55"/>
<connect gate="G$1" pin="56" pad="56"/>
<connect gate="G$1" pin="57" pad="57"/>
<connect gate="G$1" pin="58" pad="58"/>
<connect gate="G$1" pin="59" pad="59"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="60" pad="60"/>
<connect gate="G$1" pin="61" pad="61"/>
<connect gate="G$1" pin="62" pad="62"/>
<connect gate="G$1" pin="63" pad="63"/>
<connect gate="G$1" pin="64" pad="64"/>
<connect gate="G$1" pin="65" pad="65"/>
<connect gate="G$1" pin="66" pad="66"/>
<connect gate="G$1" pin="67" pad="67"/>
<connect gate="G$1" pin="68" pad="68"/>
<connect gate="G$1" pin="69" pad="69"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="70" pad="70"/>
<connect gate="G$1" pin="71" pad="71"/>
<connect gate="G$1" pin="72" pad="72"/>
<connect gate="G$1" pin="73" pad="73"/>
<connect gate="G$1" pin="74" pad="74"/>
<connect gate="G$1" pin="75" pad="75"/>
<connect gate="G$1" pin="76" pad="76"/>
<connect gate="G$1" pin="77" pad="77"/>
<connect gate="G$1" pin="78" pad="78"/>
<connect gate="G$1" pin="79" pad="79"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="80" pad="80"/>
<connect gate="G$1" pin="81" pad="81"/>
<connect gate="G$1" pin="82" pad="82"/>
<connect gate="G$1" pin="83" pad="83"/>
<connect gate="G$1" pin="84" pad="84"/>
<connect gate="G$1" pin="85" pad="85"/>
<connect gate="G$1" pin="86" pad="86"/>
<connect gate="G$1" pin="87" pad="87"/>
<connect gate="G$1" pin="88" pad="88"/>
<connect gate="G$1" pin="89" pad="89"/>
<connect gate="G$1" pin="9" pad="9"/>
<connect gate="G$1" pin="90" pad="90"/>
<connect gate="G$1" pin="91" pad="91"/>
<connect gate="G$1" pin="92" pad="92"/>
<connect gate="G$1" pin="93" pad="93"/>
<connect gate="G$1" pin="94" pad="94"/>
<connect gate="G$1" pin="95" pad="95"/>
<connect gate="G$1" pin="96" pad="96"/>
<connect gate="G$1" pin="97" pad="97"/>
<connect gate="G$1" pin="98" pad="98"/>
<connect gate="G$1" pin="99" pad="99"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3791942/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAPACITOR" urn="urn:adsk.eagle:component:8414470/2" prefix="C" uservalue="yes" library_version="58">
<gates>
<gate name="G$1" symbol="CAPACITOR" x="0" y="0"/>
</gates>
<devices>
<device name="0402" package="CAPC1005X60">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793108/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0508" package="CAPC1220X107">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793115/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="CAPC1608X55">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793126/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0612" package="CAPC1632X168">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793131/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="CAPC2012X127">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793139/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="CAPC3215X168">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793145/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1210" package="CAPC3225X168">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793149/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1808" package="CAPC4520X168">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793154/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1812" package="CAPC4532X218">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793159/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1825" package="CAPC4564X218">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793166/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2220" package="CAPC5650X218">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793171/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2225" package="CAPC5563X218">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793174/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3640" package="CAPC9198X218">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793178/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="EEPROM_8PIN" urn="urn:adsk.eagle:component:8414432/2" prefix="U" uservalue="yes" library_version="63">
<gates>
<gate name="G$1" symbol="EEPROM_8PIN" x="0" y="0"/>
</gates>
<devices>
<device name="150-MIL" package="SOIC127P600X175-8">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3833977/3"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="208-MIL" package="SOIC127P800X203-8">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3852771/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CKSWITCHES_KXT3" urn="urn:adsk.eagle:component:8414442/1" prefix="SW" uservalue="yes" library_version="40" library_locally_modified="yes">
<gates>
<gate name="G$1" symbol="SWITCH" x="0" y="0"/>
</gates>
<devices>
<device name="KXT3" package="CKSWITCHES_KXT3">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414043/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CRYSTAL_3/4-POS" urn="urn:adsk.eagle:component:8414446/1" prefix="Y" uservalue="yes" library_version="58">
<gates>
<gate name="G$1" symbol="CRYSTAL_3-POS" x="0" y="0"/>
</gates>
<devices>
<device name="CYLINDRICAL_CAN_RADIAL" package="CRYSTAL_CYLINDRICAL_RADIAL">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414042/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="4-SMD_5.00MM_X_3.20MM" package="OSCCC500X320X110">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="3"/>
<connect gate="G$1" pin="3" pad="2 4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3849466/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="4-SMD_3.20MM_X_2.50MM" package="OSCCC320X250X80">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="3"/>
<connect gate="G$1" pin="3" pad="2 4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3849546/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LED" urn="urn:adsk.eagle:component:8414483/4" prefix="D" uservalue="yes" library_version="76">
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="LED_CUSTOM_0402/RADIAL" package="LED_CUSTOM_0402/RADIAL">
<connects>
<connect gate="G$1" pin="A" pad="A_1 A_2 A_3"/>
<connect gate="G$1" pin="K" pad="K_1 K_2 K_3"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414050/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="LEDC1608X55">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3905179/3"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="LEDC2012X70">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3905183/4"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LED_RADIAL" package="LED_RADIAL">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414056/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402" package="LEDC1005X60N">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="K" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15620674/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DIODE_SCHOTTKY" urn="urn:adsk.eagle:component:8414469/9" prefix="D" uservalue="yes" library_version="76">
<gates>
<gate name="G$1" symbol="DIODE_SCHOTTKY" x="0" y="0"/>
</gates>
<devices>
<device name="SMA" package="DIOM5226X292">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793189/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMB" package="DIOM5436X265">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793201/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMC" package="DIOM7959X265">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793207/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOD-323" package="SOD2512X100">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793216/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOD-123" package="SOD3716X135">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793231/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOT-323" package="SOT65P210X110-3">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3809951/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOT-23" package="SOT95P280X135-3">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3809961/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOD-128" package="SODFL4725X110">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3921253/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0201" package="DIOC0502X50">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3948086/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402" package="DIOC1005X84">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3948100/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0502" package="DIOC1206X63">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3948141/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="DIOC1608X84">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3948147/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="DIOC2012X70">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3948150/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="DIOC3216X84">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3948168/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2010" package="DIOC5324X84">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3948171/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="DO214AC_SMA" package="DO214AC_SMA">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="K" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15671034/5"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TI_SIMPLE_SWITCHER_REGULATOR" urn="urn:adsk.eagle:component:8414512/1" prefix="U" uservalue="yes" library_version="76">
<gates>
<gate name="G$1" symbol="TI_LM22672MRX-ADJ" x="0" y="0"/>
</gates>
<devices>
<device name="TI_LM22672MRX-ADJ" package="SOIC127P600X175-9">
<connects>
<connect gate="G$1" pin="BOOT" pad="1"/>
<connect gate="G$1" pin="EN" pad="5"/>
<connect gate="G$1" pin="FB" pad="4"/>
<connect gate="G$1" pin="GND" pad="6"/>
<connect gate="G$1" pin="PAD" pad="9"/>
<connect gate="G$1" pin="RT/SYNC" pad="3"/>
<connect gate="G$1" pin="SS" pad="2"/>
<connect gate="G$1" pin="SW" pad="8"/>
<connect gate="G$1" pin="VIN" pad="7"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3985885/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TI_LMR14020" package="SOIC127P600X175-9">
<connects>
<connect gate="G$1" pin="BOOT" pad="1"/>
<connect gate="G$1" pin="EN" pad="3"/>
<connect gate="G$1" pin="FB" pad="5"/>
<connect gate="G$1" pin="GND" pad="7"/>
<connect gate="G$1" pin="PAD" pad="9"/>
<connect gate="G$1" pin="RT/SYNC" pad="4"/>
<connect gate="G$1" pin="SS" pad="6"/>
<connect gate="G$1" pin="SW" pad="8"/>
<connect gate="G$1" pin="VIN" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3985885/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TRANSCEIVER_RS485" urn="urn:adsk.eagle:component:8414510/2" prefix="U" uservalue="yes" library_version="63">
<gates>
<gate name="G$1" symbol="TRANSCEIVER_RS485" x="0" y="0"/>
</gates>
<devices>
<device name="8-SOIC" package="SOIC127P600X175-8">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3833977/3"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TI_TCA9406" urn="urn:adsk.eagle:component:8414515/1" prefix="U" uservalue="yes" library_version="58">
<gates>
<gate name="G$1" symbol="TI_TCA9406" x="0" y="0"/>
</gates>
<devices>
<device name="8-MSOP_DCT" package="SOP65P400X130-8">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3986190/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="8-VFSOP_DCU" package="SOP50P310X90-8">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3986204/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TRANSISTOR_PNP" urn="urn:adsk.eagle:component:8414453/3" prefix="Q" uservalue="yes" library_version="76">
<gates>
<gate name="G$1" symbol="TRANSISTOR_PNP" x="0" y="0"/>
</gates>
<devices>
<device name="SOT-223" package="SOT230P700X180-4">
<connects>
<connect gate="G$1" pin="B" pad="1"/>
<connect gate="G$1" pin="C" pad="2 4"/>
<connect gate="G$1" pin="E" pad="3"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3834044/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOT-323" package="SOT65P210X110-3">
<connects>
<connect gate="G$1" pin="B" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
<connect gate="G$1" pin="E" pad="NC"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3809951/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOT-23" package="SOT95P280X135-3">
<connects>
<connect gate="G$1" pin="B" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
<connect gate="G$1" pin="E" pad="NC"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3809961/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LM317_REGULATOR" urn="urn:adsk.eagle:component:8414498/1" prefix="U" uservalue="yes" library_version="58">
<gates>
<gate name="G$1" symbol="LM317_REGULATOR" x="0" y="0"/>
</gates>
<devices>
<device name="TO-263-3" package="TO508P1530X456-3">
<connects>
<connect gate="G$1" pin="ADJ" pad="1"/>
<connect gate="G$1" pin="VIN" pad="3"/>
<connect gate="G$1" pin="VOUT" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3921285/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TO-261AA" package="SOT230P700X180-4">
<connects>
<connect gate="G$1" pin="ADJ" pad="1"/>
<connect gate="G$1" pin="VIN" pad="3"/>
<connect gate="G$1" pin="VOUT" pad="2 4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3834044/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DIODE_TVS_UNI-DIRECTIONAL" urn="urn:adsk.eagle:component:15726467/4" prefix="D" library_version="75" library_locally_modified="yes">
<gates>
<gate name="G$1" symbol="DIODE_ZENER" x="0" y="0"/>
</gates>
<devices>
<device name="DIO214AB_SMC_TVS" package="DIOM7959X262N">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="K" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15726434/3"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CONNECTOR_4POS" urn="urn:adsk.eagle:component:8414493/8" prefix="J" uservalue="yes" library_version="65" library_locally_modified="yes">
<gates>
<gate name="-1" symbol="PIN" x="5.08" y="0"/>
<gate name="-2" symbol="PIN" x="5.08" y="-2.54"/>
<gate name="-3" symbol="PIN" x="5.08" y="-5.08"/>
<gate name="-4" symbol="PIN" x="5.08" y="-7.62"/>
</gates>
<devices>
<device name="MOLEX_5031590400" package="MOLEX_5031590400_SD">
<connects>
<connect gate="-1" pin="1" pad="P$1"/>
<connect gate="-2" pin="1" pad="P$2"/>
<connect gate="-3" pin="1" pad="P$3"/>
<connect gate="-4" pin="1" pad="P$4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414052/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SAMTEC_TSW-104-XX-X-S" package="SAMTEC_TSW-104-XX-X-S">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-2" pin="1" pad="2"/>
<connect gate="-3" pin="1" pad="3"/>
<connect gate="-4" pin="1" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414075/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SAMTEC_TSW-104-XX-X-S_NO_OUTLINE" package="SAMTEC_TSW-104-XX-X-S_NO_OUTLINE">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-2" pin="1" pad="2"/>
<connect gate="-3" pin="1" pad="3"/>
<connect gate="-4" pin="1" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414076/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SAMTEC_TSW-104-XX-X-S_FLEXSMD_PAD" package="SAMTEC_TSW-104-XX-X-S_FLEXSMD_PAD">
<connects>
<connect gate="-1" pin="1" pad="1 P$1"/>
<connect gate="-2" pin="1" pad="2 P$2"/>
<connect gate="-3" pin="1" pad="3 P$3"/>
<connect gate="-4" pin="1" pad="4 P$4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414081/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST_B4P-SHF-1AA" package="JST_B4P-SHF-1AA">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-2" pin="1" pad="2"/>
<connect gate="-3" pin="1" pad="3"/>
<connect gate="-4" pin="1" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414089/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST_BS4P-SHF-1AA" package="JST_BS4P-SHF-1AA">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-2" pin="1" pad="2"/>
<connect gate="-3" pin="1" pad="3"/>
<connect gate="-4" pin="1" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414090/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST_F4P-HVQ" package="JST_F4P-HVQ">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-2" pin="1" pad="2"/>
<connect gate="-3" pin="1" pad="3"/>
<connect gate="-4" pin="1" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414091/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST_F4P-SHVQ" package="JST_F4P-SHVQ">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-2" pin="1" pad="2"/>
<connect gate="-3" pin="1" pad="3"/>
<connect gate="-4" pin="1" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414092/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST_04JQ-BT" package="JST_04JQ-BT">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-2" pin="1" pad="2"/>
<connect gate="-3" pin="1" pad="3"/>
<connect gate="-4" pin="1" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414218/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST_04JQ-ST" package="JST_04JQ-ST">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-2" pin="1" pad="2"/>
<connect gate="-3" pin="1" pad="3"/>
<connect gate="-4" pin="1" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414219/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST_B4B-XH-A" package="JST_B4B-XH-A">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-2" pin="1" pad="2"/>
<connect gate="-3" pin="1" pad="3"/>
<connect gate="-4" pin="1" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414220/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST_S4B-XH-A-1" package="JST_S4B-XH-A-1">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-2" pin="1" pad="2"/>
<connect gate="-3" pin="1" pad="3"/>
<connect gate="-4" pin="1" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414221/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST_B04B-PASK" package="JST_B04B-PASK">
<connects>
<connect gate="-1" pin="1" pad="P$1"/>
<connect gate="-2" pin="1" pad="P$2"/>
<connect gate="-3" pin="1" pad="P$3"/>
<connect gate="-4" pin="1" pad="P$4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414269/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST_S04B-PASK-2" package="JST_S04B-PASK-2">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-2" pin="1" pad="2"/>
<connect gate="-3" pin="1" pad="3"/>
<connect gate="-4" pin="1" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414270/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST_B4B-PK-K-S" package="JST_B4B-PH-K-S">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-2" pin="1" pad="2"/>
<connect gate="-3" pin="1" pad="3"/>
<connect gate="-4" pin="1" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414302/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST_S4B-PH-K-S" package="JST_S4B-PH-K-S">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-2" pin="1" pad="2"/>
<connect gate="-3" pin="1" pad="3"/>
<connect gate="-4" pin="1" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414303/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST_B04B-PSILE-A1_B04B-PSIY-B1_B04B-PSIR-C1" package="JST_B04B-PSILE-A1_B04B-PSIY-B1_B04B-PSIR-C1">
<connects>
<connect gate="-1" pin="1" pad="P$1"/>
<connect gate="-2" pin="1" pad="P$2"/>
<connect gate="-3" pin="1" pad="P$3"/>
<connect gate="-4" pin="1" pad="P$4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414334/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST_B4PS-VH" package="JST_B4PS-VH">
<connects>
<connect gate="-1" pin="1" pad="P$1"/>
<connect gate="-2" pin="1" pad="P$2"/>
<connect gate="-3" pin="1" pad="P$3"/>
<connect gate="-4" pin="1" pad="P$4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414350/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST_B4P-VH" package="JST_B4P-VH">
<connects>
<connect gate="-1" pin="1" pad="P$1"/>
<connect gate="-2" pin="1" pad="P$2"/>
<connect gate="-3" pin="1" pad="P$3"/>
<connect gate="-4" pin="1" pad="P$4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414351/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST_B4P-VH-FB-B" package="JST_B4P-VH-FB-B">
<connects>
<connect gate="-1" pin="1" pad="P$1"/>
<connect gate="-2" pin="1" pad="P$2"/>
<connect gate="-3" pin="1" pad="P$3"/>
<connect gate="-4" pin="1" pad="P$4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414352/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST_B04B-ESK-1D" package="JST_B04B-ZESK-1D">
<connects>
<connect gate="-1" pin="1" pad="P$1"/>
<connect gate="-2" pin="1" pad="P$2"/>
<connect gate="-3" pin="1" pad="P$3"/>
<connect gate="-4" pin="1" pad="P$4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414376/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST_S04B-ZESK-2D" package="JST_S04B-ZESK-2D">
<connects>
<connect gate="-1" pin="1" pad="P$1"/>
<connect gate="-2" pin="1" pad="P$2"/>
<connect gate="-3" pin="1" pad="P$3"/>
<connect gate="-4" pin="1" pad="P$4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414377/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TSW-104-08-F-S-RA" package="HDRRA4W64P254_1X4_1016X254X254B">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-2" pin="1" pad="2"/>
<connect gate="-3" pin="1" pad="3"/>
<connect gate="-4" pin="1" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15597948/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="DW-04-09-F-S-512" package="HDRV4W64P254_1X4_1016X508X1829B">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-2" pin="1" pad="2"/>
<connect gate="-3" pin="1" pad="3"/>
<connect gate="-4" pin="1" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15735943/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="CONNECTOR_SMD_4PIN">
<connects>
<connect gate="-1" pin="1" pad="P$1"/>
<connect gate="-2" pin="1" pad="P$2"/>
<connect gate="-3" pin="1" pad="P$3"/>
<connect gate="-4" pin="1" pad="P$4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:21311885/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MOSFET_PCH" urn="urn:adsk.eagle:component:8414459/4" prefix="Q" uservalue="yes" library_version="76">
<gates>
<gate name="G$1" symbol="MOSFET-PCH" x="0" y="0"/>
</gates>
<devices>
<device name="SOIC-8" package="SOIC127P600X175-8">
<connects>
<connect gate="G$1" pin="D" pad="5 6 7 8"/>
<connect gate="G$1" pin="G" pad="4"/>
<connect gate="G$1" pin="S" pad="1 2 3"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3833977/3"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOT-223" package="SOT230P700X180-4">
<connects>
<connect gate="G$1" pin="D" pad="2 4"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="3"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3834044/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOT-23" package="SOT95P280X135-3">
<connects>
<connect gate="G$1" pin="D" pad="K"/>
<connect gate="G$1" pin="G" pad="A"/>
<connect gate="G$1" pin="S" pad="NC"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3809961/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TO-252-3_DPAK" package="TO457P1000X238-3">
<connects>
<connect gate="G$1" pin="D" pad="3"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3920001/3"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MOSFET_NCH" urn="urn:adsk.eagle:component:8414460/5" prefix="Q" uservalue="yes" library_version="76">
<gates>
<gate name="G$1" symbol="MOSFET-NCH" x="0" y="0"/>
</gates>
<devices>
<device name="SOIC-8" package="SOIC127P600X175-8">
<connects>
<connect gate="G$1" pin="D" pad="5 6 7 8"/>
<connect gate="G$1" pin="G" pad="4"/>
<connect gate="G$1" pin="S" pad="1 2 3"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3833977/3"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOT-223" package="SOT230P700X180-4">
<connects>
<connect gate="G$1" pin="D" pad="2 4"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="3"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3834044/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOT-23" package="SOT95P280X135-3">
<connects>
<connect gate="G$1" pin="D" pad="K"/>
<connect gate="G$1" pin="G" pad="A"/>
<connect gate="G$1" pin="S" pad="NC"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3809961/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TO-252-3_DPAK" package="TO457P1000X238-3">
<connects>
<connect gate="G$1" pin="D" pad="3"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3920001/3"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POWERPAK_SO-8" package="POWERPAK_SO-8">
<connects>
<connect gate="G$1" pin="D" pad="5 6 7 8 9"/>
<connect gate="G$1" pin="G" pad="4"/>
<connect gate="G$1" pin="S" pad="1 2 3"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414082/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DIODE" urn="urn:adsk.eagle:component:8414468/5" prefix="D" uservalue="yes" library_version="76">
<gates>
<gate name="G$1" symbol="DIODE" x="0" y="0"/>
</gates>
<devices>
<device name="SMA" package="DIOM5226X292">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793189/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMB" package="DIOM5436X265">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793201/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMC" package="DIOM7959X265">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793207/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOD-323" package="SOD2512X100">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793216/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOD-123" package="SOD3716X135">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793231/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOT-323" package="SOT65P210X110-3">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3809951/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOT-23" package="SOT95P280X135-3">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3809961/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOD-128" package="SODFL4725X110">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3921253/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0201" package="DIOC0502X50">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3948086/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402" package="DIOC1005X84">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3948100/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0502" package="DIOC1206X63">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3948141/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="DIOC1608X84">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3948147/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="DIOC2012X70">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3948150/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="DIOC3216X84">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3948168/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2010" package="DIOC5324X84">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3948171/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="DO_221AC_SMAF" package="DIOC5226X110N">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="K" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:16113232/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="DO-214AA_SMB_UNI" package="DO214AA_SMB">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15617780/3"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TI_TPS2491DGSR" urn="urn:adsk.eagle:component:8414516/1" prefix="U" uservalue="yes" library_version="58">
<gates>
<gate name="G$1" symbol="TI_TPS2491DGSR" x="0" y="0"/>
</gates>
<devices>
<device name="10-TFSOP" package="SOP50P490X110-10">
<connects>
<connect gate="G$1" pin="10_VCC" pad="10"/>
<connect gate="G$1" pin="1_EN" pad="1"/>
<connect gate="G$1" pin="2_VREF" pad="2"/>
<connect gate="G$1" pin="3_PROG" pad="3"/>
<connect gate="G$1" pin="4_TIMER" pad="4"/>
<connect gate="G$1" pin="5_GND" pad="5"/>
<connect gate="G$1" pin="6_PG" pad="6"/>
<connect gate="G$1" pin="7_OUT" pad="7"/>
<connect gate="G$1" pin="8_GATE" pad="8"/>
<connect gate="G$1" pin="9_SENSE" pad="9"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3921298/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="NC" urn="urn:adsk.eagle:component:8414497/2" prefix="NC" library_version="58">
<gates>
<gate name="G$1" symbol="NC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CONNECTOR_8POS" urn="urn:adsk.eagle:component:8414524/4" prefix="J" uservalue="yes" library_version="67">
<gates>
<gate name="G$1" symbol="PIN" x="5.08" y="0"/>
<gate name="G$2" symbol="PIN" x="5.08" y="-2.54"/>
<gate name="G$3" symbol="PIN" x="5.08" y="-5.08"/>
<gate name="G$4" symbol="PIN" x="5.08" y="-7.62"/>
<gate name="G$5" symbol="PIN" x="5.08" y="-10.16"/>
<gate name="G$6" symbol="PIN" x="5.08" y="-12.7"/>
<gate name="G$7" symbol="PIN" x="5.08" y="-15.24"/>
<gate name="G$8" symbol="PIN" x="5.08" y="-17.78"/>
</gates>
<devices>
<device name="JST_B8B-PHDSS" package="JST_B8B-PHDSS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$2" pin="1" pad="2"/>
<connect gate="G$3" pin="1" pad="3"/>
<connect gate="G$4" pin="1" pad="4"/>
<connect gate="G$5" pin="1" pad="5"/>
<connect gate="G$6" pin="1" pad="6"/>
<connect gate="G$7" pin="1" pad="7"/>
<connect gate="G$8" pin="1" pad="8"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414083/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST_8P-HVQ" package="JST_8P-HVQ">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$2" pin="1" pad="2"/>
<connect gate="G$3" pin="1" pad="3"/>
<connect gate="G$4" pin="1" pad="4"/>
<connect gate="G$5" pin="1" pad="5"/>
<connect gate="G$6" pin="1" pad="6"/>
<connect gate="G$7" pin="1" pad="7"/>
<connect gate="G$8" pin="1" pad="8"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414108/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JAT_B8P-SHF-1AA" package="JST_B8P-SHF-1AA">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$2" pin="1" pad="2"/>
<connect gate="G$3" pin="1" pad="3"/>
<connect gate="G$4" pin="1" pad="4"/>
<connect gate="G$5" pin="1" pad="5"/>
<connect gate="G$6" pin="1" pad="6"/>
<connect gate="G$7" pin="1" pad="7"/>
<connect gate="G$8" pin="1" pad="8"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414109/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST_BS8P-SHF-1AA" package="JST_BS8P-SHF-1AA">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$2" pin="1" pad="2"/>
<connect gate="G$3" pin="1" pad="3"/>
<connect gate="G$4" pin="1" pad="4"/>
<connect gate="G$5" pin="1" pad="5"/>
<connect gate="G$6" pin="1" pad="6"/>
<connect gate="G$7" pin="1" pad="7"/>
<connect gate="G$8" pin="1" pad="8"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414110/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST_F8P-HVQ" package="JST_F8P-HVQ">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$2" pin="1" pad="2"/>
<connect gate="G$3" pin="1" pad="3"/>
<connect gate="G$4" pin="1" pad="4"/>
<connect gate="G$5" pin="1" pad="5"/>
<connect gate="G$6" pin="1" pad="6"/>
<connect gate="G$7" pin="1" pad="7"/>
<connect gate="G$8" pin="1" pad="8"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414111/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST_F8P-SHVQ" package="JST_F8P-SHVQ">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$2" pin="1" pad="2"/>
<connect gate="G$3" pin="1" pad="3"/>
<connect gate="G$4" pin="1" pad="4"/>
<connect gate="G$5" pin="1" pad="5"/>
<connect gate="G$6" pin="1" pad="6"/>
<connect gate="G$7" pin="1" pad="7"/>
<connect gate="G$8" pin="1" pad="8"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414112/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST_08JQ-BT" package="JST_08JQ-BT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$2" pin="1" pad="2"/>
<connect gate="G$3" pin="1" pad="3"/>
<connect gate="G$4" pin="1" pad="4"/>
<connect gate="G$5" pin="1" pad="5"/>
<connect gate="G$6" pin="1" pad="6"/>
<connect gate="G$7" pin="1" pad="7"/>
<connect gate="G$8" pin="1" pad="8"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414234/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST_08JQ-ST" package="JST_08JQ-ST">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$2" pin="1" pad="2"/>
<connect gate="G$3" pin="1" pad="3"/>
<connect gate="G$4" pin="1" pad="4"/>
<connect gate="G$5" pin="1" pad="5"/>
<connect gate="G$6" pin="1" pad="6"/>
<connect gate="G$7" pin="1" pad="7"/>
<connect gate="G$8" pin="1" pad="8"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414235/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST_B8B-XH-A" package="JST_B8B-XH-A">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$2" pin="1" pad="2"/>
<connect gate="G$3" pin="1" pad="3"/>
<connect gate="G$4" pin="1" pad="4"/>
<connect gate="G$5" pin="1" pad="5"/>
<connect gate="G$6" pin="1" pad="6"/>
<connect gate="G$7" pin="1" pad="7"/>
<connect gate="G$8" pin="1" pad="8"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414236/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST_S8B-XH-A-1" package="JST_S8B-XH-A-1">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$2" pin="1" pad="2"/>
<connect gate="G$3" pin="1" pad="3"/>
<connect gate="G$4" pin="1" pad="4"/>
<connect gate="G$5" pin="1" pad="5"/>
<connect gate="G$6" pin="1" pad="6"/>
<connect gate="G$7" pin="1" pad="7"/>
<connect gate="G$8" pin="1" pad="8"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414237/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST_B08B-PASK" package="JST_B08B-PASK">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$2" pin="1" pad="P$2"/>
<connect gate="G$3" pin="1" pad="P$3"/>
<connect gate="G$4" pin="1" pad="P$4"/>
<connect gate="G$5" pin="1" pad="P$5"/>
<connect gate="G$6" pin="1" pad="P$6"/>
<connect gate="G$7" pin="1" pad="P$7"/>
<connect gate="G$8" pin="1" pad="P$8"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414277/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST_S08B-PASK-2" package="JST_S08B-PASK-2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$2" pin="1" pad="2"/>
<connect gate="G$3" pin="1" pad="3"/>
<connect gate="G$4" pin="1" pad="4"/>
<connect gate="G$5" pin="1" pad="5"/>
<connect gate="G$6" pin="1" pad="6"/>
<connect gate="G$7" pin="1" pad="7"/>
<connect gate="G$8" pin="1" pad="8"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414278/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST_PHD_4PIN_2MM" package="JST_PHD_4PIN_2MM">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$2" pin="1" pad="P$2"/>
<connect gate="G$3" pin="1" pad="P$3"/>
<connect gate="G$4" pin="1" pad="P$4"/>
<connect gate="G$5" pin="1" pad="P$5"/>
<connect gate="G$6" pin="1" pad="P$6"/>
<connect gate="G$7" pin="1" pad="P$7"/>
<connect gate="G$8" pin="1" pad="P$8"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414296/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST_B8B-PH-K-S" package="JST_B8B-PH-K-S">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$2" pin="1" pad="2"/>
<connect gate="G$3" pin="1" pad="3"/>
<connect gate="G$4" pin="1" pad="4"/>
<connect gate="G$5" pin="1" pad="5"/>
<connect gate="G$6" pin="1" pad="6"/>
<connect gate="G$7" pin="1" pad="7"/>
<connect gate="G$8" pin="1" pad="8"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414310/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST_S8B-PH-K-S" package="JST_S8B-PH-K-S">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$2" pin="1" pad="2"/>
<connect gate="G$3" pin="1" pad="3"/>
<connect gate="G$4" pin="1" pad="4"/>
<connect gate="G$5" pin="1" pad="5"/>
<connect gate="G$6" pin="1" pad="6"/>
<connect gate="G$7" pin="1" pad="7"/>
<connect gate="G$8" pin="1" pad="8"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414311/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST_B08B-PSILE-1" package="JST_B08B-PSILE-1">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$2" pin="1" pad="P$2"/>
<connect gate="G$3" pin="1" pad="P$3"/>
<connect gate="G$4" pin="1" pad="P$4"/>
<connect gate="G$5" pin="1" pad="P$5"/>
<connect gate="G$6" pin="1" pad="P$6"/>
<connect gate="G$7" pin="1" pad="P$7"/>
<connect gate="G$8" pin="1" pad="P$8"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414337/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST_B8PS-VH" package="JST_B8PS-VH">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$2" pin="1" pad="P$2"/>
<connect gate="G$3" pin="1" pad="P$3"/>
<connect gate="G$4" pin="1" pad="P$4"/>
<connect gate="G$5" pin="1" pad="P$5"/>
<connect gate="G$6" pin="1" pad="P$6"/>
<connect gate="G$7" pin="1" pad="P$7"/>
<connect gate="G$8" pin="1" pad="P$8"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414363/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST_B8P-VH" package="JST_B8P-VH">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$2" pin="1" pad="P$2"/>
<connect gate="G$3" pin="1" pad="P$3"/>
<connect gate="G$4" pin="1" pad="P$4"/>
<connect gate="G$5" pin="1" pad="P$5"/>
<connect gate="G$6" pin="1" pad="P$6"/>
<connect gate="G$7" pin="1" pad="P$7"/>
<connect gate="G$8" pin="1" pad="P$8"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414364/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST_B8P-VH-FB-B" package="JST_B8P-VH-FB-B">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$2" pin="1" pad="P$2"/>
<connect gate="G$3" pin="1" pad="P$3"/>
<connect gate="G$4" pin="1" pad="P$4"/>
<connect gate="G$5" pin="1" pad="P$5"/>
<connect gate="G$6" pin="1" pad="P$6"/>
<connect gate="G$7" pin="1" pad="P$7"/>
<connect gate="G$8" pin="1" pad="P$8"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414365/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST_B08B-ZESK-1D" package="JST_B08B-ZESK-1D">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$2" pin="1" pad="P$2"/>
<connect gate="G$3" pin="1" pad="P$3"/>
<connect gate="G$4" pin="1" pad="P$4"/>
<connect gate="G$5" pin="1" pad="P$5"/>
<connect gate="G$6" pin="1" pad="P$6"/>
<connect gate="G$7" pin="1" pad="P$7"/>
<connect gate="G$8" pin="1" pad="P$8"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414384/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST_S08B-ZESK-2D" package="JST_S08B-ZESK-2D">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$2" pin="1" pad="P$2"/>
<connect gate="G$3" pin="1" pad="P$3"/>
<connect gate="G$4" pin="1" pad="P$4"/>
<connect gate="G$5" pin="1" pad="P$5"/>
<connect gate="G$6" pin="1" pad="P$6"/>
<connect gate="G$7" pin="1" pad="P$7"/>
<connect gate="G$8" pin="1" pad="P$8"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414385/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5031590800" package="MOLEX_5031590800_SD">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$2" pin="1" pad="P$2"/>
<connect gate="G$3" pin="1" pad="P$3"/>
<connect gate="G$4" pin="1" pad="P$4"/>
<connect gate="G$5" pin="1" pad="P$5"/>
<connect gate="G$6" pin="1" pad="P$6"/>
<connect gate="G$7" pin="1" pad="P$7"/>
<connect gate="G$8" pin="1" pad="P$8"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:21515996/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="AMIS_30624" urn="urn:adsk.eagle:component:20425770/2" prefix="U" uservalue="yes" library_version="76">
<gates>
<gate name="G$1" symbol="AMIS_30624" x="0" y="0"/>
</gates>
<devices>
<device name="NQFP" package="QFN65P700X700X80-33">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="13" pad="13"/>
<connect gate="G$1" pin="14" pad="14"/>
<connect gate="G$1" pin="15" pad="15"/>
<connect gate="G$1" pin="16" pad="16"/>
<connect gate="G$1" pin="17" pad="17"/>
<connect gate="G$1" pin="18" pad="18"/>
<connect gate="G$1" pin="19" pad="19"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="20" pad="20"/>
<connect gate="G$1" pin="21" pad="21"/>
<connect gate="G$1" pin="22" pad="22"/>
<connect gate="G$1" pin="23" pad="23"/>
<connect gate="G$1" pin="24" pad="24"/>
<connect gate="G$1" pin="25" pad="25"/>
<connect gate="G$1" pin="26" pad="26"/>
<connect gate="G$1" pin="27" pad="27"/>
<connect gate="G$1" pin="28" pad="28"/>
<connect gate="G$1" pin="29" pad="29"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="30" pad="30"/>
<connect gate="G$1" pin="31" pad="31"/>
<connect gate="G$1" pin="32" pad="32"/>
<connect gate="G$1" pin="33" pad="33"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3837100/3"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="FIDUCIAL" urn="urn:adsk.eagle:component:8414466/1" prefix="F" uservalue="yes" library_version="74">
<gates>
<gate name="G$1" symbol="FIDUCIAL" x="0" y="0"/>
</gates>
<devices>
<device name="0.5MM" package="FIDUCIAL_0.5MM">
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414039/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply2">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
Please keep in mind, that these devices are necessary for the
automatic wiring of the supply signals.&lt;p&gt;
The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND">
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="-1.27" y2="0" width="0.254" layer="94"/>
<text x="-1.905" y="-3.175" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="SUPPLY">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="GND" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
<class number="1" name="RF" width="0.237490625" drill="0.254">
<clearance class="0" value="0.508"/>
<clearance class="1" value="0.508"/>
</class>
</classes>
<groups>
<schematic_group name="PROGRAMMING_HEADER1"/>
<schematic_group name="CRYSTAL_OSC1"/>
<schematic_group name="PROCESSOR1"/>
<schematic_group name="BIASING_CAPACITORS1"/>
<schematic_group name="RESET1"/>
<schematic_group name="CORE_BIASING_CAPS1"/>
<schematic_group name="MEMORY1"/>
</groups>
<parts>
<part name="SUPPLY11" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY12" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="C97" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="0.1uF"/>
<part name="C96" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="10nF"/>
<part name="C89" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="0.1uF"/>
<part name="C100" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="0.1uF"/>
<part name="C101" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="10nF"/>
<part name="C102" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="0.1uF"/>
<part name="C103" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="10nF"/>
<part name="C83" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="0.1uF"/>
<part name="C81" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="10nF"/>
<part name="C91" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0603" package3d_urn="urn:adsk.eagle:package:3793126/2" value="DNP"/>
<part name="SUPPLY13" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY14" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="R120" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="100K"/>
<part name="R119" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="100"/>
<part name="C87" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="0.1uF"/>
<part name="SW1" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CKSWITCHES_KXT3" device="KXT3" package3d_urn="urn:adsk.eagle:package:8414043/1" value="KXT331LHS"/>
<part name="+3V2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="SUPPLY15" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="R126" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="100K"/>
<part name="+3V6" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="SUPPLY16" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY17" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="C99" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="0.1uF"/>
<part name="C82" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="10nF"/>
<part name="C95" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="0.1uF"/>
<part name="C98" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="10nF"/>
<part name="C84" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="0.1uF"/>
<part name="C92" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0603" package3d_urn="urn:adsk.eagle:package:3793126/2" value="4.7uF"/>
<part name="U13" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="ATMEL_ATSAMD51N19" device="TQFP-100" package3d_urn="urn:adsk.eagle:package:3791942/2" value="SAM51N19/SAMD51N20"/>
<part name="Y1" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CRYSTAL_3/4-POS" device="4-SMD_5.00MM_X_3.20MM" package3d_urn="urn:adsk.eagle:package:3849466/1" value="ECS_ECX-53B-DU"/>
<part name="C88" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="20pF"/>
<part name="C79" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="20pF"/>
<part name="SUPPLY18" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="C80" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="0.1uF"/>
<part name="R153" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="0"/>
<part name="TP32" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TP_CLASS_B" device="CIR_PAD_1MM" package3d_urn="urn:adsk.eagle:package:8414067/2" value="3V3"/>
<part name="C85" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="10nF"/>
<part name="C86" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="0.1uF"/>
<part name="TP31" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TP_CLASS_B" device="CIR_PAD_1MM" package3d_urn="urn:adsk.eagle:package:8414067/2" value="VDDCORE"/>
<part name="+3V8" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="TP28" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TP_CLASS_B" device="CIR_PAD_1MM" package3d_urn="urn:adsk.eagle:package:8414067/2" value="GND"/>
<part name="C94" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="10nF"/>
<part name="C90" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="10nF"/>
<part name="R127" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="100K"/>
<part name="R125" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="DNP"/>
<part name="R121" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="100"/>
<part name="R142" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="DNP"/>
<part name="R143" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="DNP"/>
<part name="R150" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="DNP"/>
<part name="R152" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="DNP"/>
<part name="R141" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="DNP"/>
<part name="R144" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="DNP"/>
<part name="R149" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="DNP"/>
<part name="R151" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="DNP"/>
<part name="SUPPLY19" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="+3V10" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="TP15" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TP_CLASS_B" device="CIR_PAD_1MM" package3d_urn="urn:adsk.eagle:package:8414067/2" value="PA03"/>
<part name="TP8" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TP_CLASS_B" device="CIR_PAD_1MM" package3d_urn="urn:adsk.eagle:package:8414067/2" value="HB3_DIR"/>
<part name="TP19" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TP_CLASS_B" device="CIR_PAD_1MM" package3d_urn="urn:adsk.eagle:package:8414067/2" value="HB3_SPEED"/>
<part name="R148" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="510"/>
<part name="R147" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="510"/>
<part name="R146" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="510"/>
<part name="SUPPLY20" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="R132" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="DNP"/>
<part name="R134" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="DNP"/>
<part name="R138" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="DNP"/>
<part name="R139" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="DNP"/>
<part name="R133" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="DNP"/>
<part name="R131" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="DNP"/>
<part name="R136" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="DNP"/>
<part name="R140" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="DNP"/>
<part name="SUPPLY21" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="+3V11" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="U14" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="EEPROM_8PIN" device="150-MIL" package3d_urn="urn:adsk.eagle:package:3833977/3" value="W25x40CL/MX25V8035FM1I "/>
<part name="R128" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="10K"/>
<part name="R129" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="DNP"/>
<part name="R122" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="10K"/>
<part name="R123" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="10K"/>
<part name="R124" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="DNP"/>
<part name="R130" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="10K"/>
<part name="SUPPLY22" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY23" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="C93" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="0.1uF"/>
<part name="+3V12" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="TP33" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TP_CLASS_B" device="CIR_PAD_1MM" package3d_urn="urn:adsk.eagle:package:8414067/2" value="VDD_A"/>
<part name="D28" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="LED" device="0402" package3d_urn="urn:adsk.eagle:package:15620674/1" value="1830-1056-1-ND"/>
<part name="D29" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="LED" device="0402" package3d_urn="urn:adsk.eagle:package:15620674/1" value="1830-1056-1-ND"/>
<part name="D30" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="LED" device="0402" package3d_urn="urn:adsk.eagle:package:15620674/1" value="1830-1056-1-ND"/>
<part name="C41" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0805" package3d_urn="urn:adsk.eagle:package:3793139/1" value="4.7uF"/>
<part name="C22" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="0.1uF"/>
<part name="L3" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="INDUCTOR" device="5.00MM_X_5.00MM_SMT" package3d_urn="urn:adsk.eagle:package:3890169/1" value="10uH"/>
<part name="R47" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0603" package3d_urn="urn:adsk.eagle:package:3811166/1" value="DNP"/>
<part name="C31" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0603" package3d_urn="urn:adsk.eagle:package:3793126/2" value="0.1uF"/>
<part name="C30" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="10nF"/>
<part name="U4" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TI_SIMPLE_SWITCHER_REGULATOR" device="TI_LM22672MRX-ADJ" package3d_urn="urn:adsk.eagle:package:3985885/2" value="LM22672MRX-ADJ"/>
<part name="R40" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="DNP"/>
<part name="SUPPLY26" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY27" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="R51" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0603" package3d_urn="urn:adsk.eagle:package:3811166/1" value="2.94K"/>
<part name="R38" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0603" package3d_urn="urn:adsk.eagle:package:3811166/1" value="1K"/>
<part name="L5" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="INDUCTOR" device="5.00MM_X_5.00MM_SMT" package3d_urn="urn:adsk.eagle:package:3890169/1" value="22uH"/>
<part name="C27" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="10nF"/>
<part name="SUPPLY28" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY29" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY30" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="C32" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0805" package3d_urn="urn:adsk.eagle:package:3793139/1" value="4.7uF"/>
<part name="R42" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="3"/>
<part name="R48" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="5.1"/>
<part name="C33" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="330pF"/>
<part name="C40" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0603" package3d_urn="urn:adsk.eagle:package:3793126/2" value="0.1uF"/>
<part name="C38" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="10nF"/>
<part name="SUPPLY31" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="C44" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0805" package3d_urn="urn:adsk.eagle:package:3793139/1" value="4.7uF"/>
<part name="C42" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0805" package3d_urn="urn:adsk.eagle:package:3793139/1" value="4.7uF"/>
<part name="C43" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0805" package3d_urn="urn:adsk.eagle:package:3793139/1" value="4.7uF"/>
<part name="D18" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="DIODE_SCHOTTKY" device="DO214AC_SMA" package3d_urn="urn:adsk.eagle:package:15671034/5" value="DNP"/>
<part name="D19" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="DIODE_SCHOTTKY" device="DO214AC_SMA" package3d_urn="urn:adsk.eagle:package:15671034/5" value="B320A-13-F/B220A-13-F "/>
<part name="C36" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0805" package3d_urn="urn:adsk.eagle:package:3793139/1" value="4.7uF"/>
<part name="C23" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="0.1uF"/>
<part name="L2" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="INDUCTOR" device="5.00MM_X_5.00MM_SMT" package3d_urn="urn:adsk.eagle:package:3890169/1" value="DNP"/>
<part name="R34" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0603" package3d_urn="urn:adsk.eagle:package:3811166/1" value="0"/>
<part name="C21" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0603" package3d_urn="urn:adsk.eagle:package:3793126/2" value="0.1uF"/>
<part name="C24" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="10nF"/>
<part name="U3" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TI_SIMPLE_SWITCHER_REGULATOR" device="TI_LM22672MRX-ADJ" package3d_urn="urn:adsk.eagle:package:3985885/2" value="LM22672MRX-ADJ"/>
<part name="R39" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="DNP"/>
<part name="SUPPLY84" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY85" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="R49" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0603" package3d_urn="urn:adsk.eagle:package:3811166/1" value="6.34K"/>
<part name="R37" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0603" package3d_urn="urn:adsk.eagle:package:3811166/1" value="1K"/>
<part name="L6" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="INDUCTOR" device="5.00MM_X_5.00MM_SMT" package3d_urn="urn:adsk.eagle:package:3890169/1" value="22uH"/>
<part name="C25" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="10nF"/>
<part name="SUPPLY86" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY87" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY88" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="C19" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0805" package3d_urn="urn:adsk.eagle:package:3793139/1" value="4.7uF"/>
<part name="R43" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="3"/>
<part name="R44" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="5.1"/>
<part name="C26" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="330pF"/>
<part name="C34" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0603" package3d_urn="urn:adsk.eagle:package:3793126/2" value="0.1uF"/>
<part name="C45" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="10nF"/>
<part name="SUPPLY53" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="C37" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0805" package3d_urn="urn:adsk.eagle:package:3793139/1" value="4.7uF"/>
<part name="C39" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0805" package3d_urn="urn:adsk.eagle:package:3793139/1" value="4.7uF"/>
<part name="C35" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0805" package3d_urn="urn:adsk.eagle:package:3793139/1" value="4.7uF"/>
<part name="D20" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="DIODE_SCHOTTKY" device="DO214AC_SMA" package3d_urn="urn:adsk.eagle:package:15671034/5" value="B320A-13-F/B220A-13-F "/>
<part name="U11" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TRANSCEIVER_RS485" device="8-SOIC" package3d_urn="urn:adsk.eagle:package:3833977/3" value="ADM3061EBRZ"/>
<part name="R109" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="0"/>
<part name="R103" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="0"/>
<part name="R113" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="0"/>
<part name="R98" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="0"/>
<part name="C71" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="15pF"/>
<part name="C74" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="15pF"/>
<part name="C78" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0603" package3d_urn="urn:adsk.eagle:package:3793126/2" value="1uF"/>
<part name="SUPPLY1" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY2" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY3" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="U12" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TRANSCEIVER_RS485" device="8-SOIC" package3d_urn="urn:adsk.eagle:package:3833977/3" value="ADM3061EBRZ"/>
<part name="R111" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="0"/>
<part name="R104" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="0"/>
<part name="R114" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="0"/>
<part name="R99" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="0"/>
<part name="C72" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="15pF"/>
<part name="C75" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="15pF"/>
<part name="C77" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0603" package3d_urn="urn:adsk.eagle:package:3793126/2" value="1uF"/>
<part name="SUPPLY7" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY8" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY9" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="U10" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TRANSCEIVER_RS485" device="8-SOIC" package3d_urn="urn:adsk.eagle:package:3833977/3" value="ADM3061EBRZ"/>
<part name="R106" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="0"/>
<part name="R101" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="0"/>
<part name="R112" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="0"/>
<part name="R97" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="0"/>
<part name="C70" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="15pF"/>
<part name="C73" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="15pF"/>
<part name="C76" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0603" package3d_urn="urn:adsk.eagle:package:3793126/2" value="1uF"/>
<part name="SUPPLY4" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY5" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY6" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="C50" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="0.1uF"/>
<part name="C55" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="0.1uF"/>
<part name="C49" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0603" package3d_urn="urn:adsk.eagle:package:3793126/2" value="1uF"/>
<part name="C57" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0603" package3d_urn="urn:adsk.eagle:package:3793126/2" value="1uF"/>
<part name="R62" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="4.7K"/>
<part name="R56" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="4.7K"/>
<part name="R69" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="4.7K"/>
<part name="R64" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="4.7K"/>
<part name="SUPPLY35" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY36" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="R68" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="4.7K"/>
<part name="SUPPLY37" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="TP14" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TP_CLASS_B" device="CIR_PAD_1MM" package3d_urn="urn:adsk.eagle:package:8414067/2" value="SDA1_5V"/>
<part name="TP11" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TP_CLASS_B" device="CIR_PAD_1MM" package3d_urn="urn:adsk.eagle:package:8414067/2" value="SCL1_5V"/>
<part name="TP16" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TP_CLASS_B" device="CIR_PAD_1MM" package3d_urn="urn:adsk.eagle:package:8414067/2" value="GND"/>
<part name="SUPPLY38" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="TP10" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TP_CLASS_B" device="CIR_PAD_1MM" package3d_urn="urn:adsk.eagle:package:8414067/2" value="OE"/>
<part name="U6" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TI_TCA9406" device="8-VFSOP_DCU" package3d_urn="urn:adsk.eagle:package:3986204/1" value="TCA9406DCUR"/>
<part name="C29" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0603" package3d_urn="urn:adsk.eagle:package:3793126/2" value="1uF"/>
<part name="C28" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="0.1uF"/>
<part name="C15" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="0.1uF"/>
<part name="C16" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0603" package3d_urn="urn:adsk.eagle:package:3793126/2" value="1uF"/>
<part name="L1" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="INDUCTOR" device="4.90MM_X_4.90MM_SMT" package3d_urn="urn:adsk.eagle:package:3904911/1" value="10uH"/>
<part name="L4" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="INDUCTOR" device="4.90MM_X_4.90MM_SMT" package3d_urn="urn:adsk.eagle:package:3904911/1" value="10uH"/>
<part name="R45" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0603" package3d_urn="urn:adsk.eagle:package:3811166/1" value="931"/>
<part name="R46" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0603" package3d_urn="urn:adsk.eagle:package:3811166/1" value="1.58K"/>
<part name="SUPPLY32" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY33" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY34" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY43" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY44" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="R50" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0603" package3d_urn="urn:adsk.eagle:package:3811166/1" value="DNP"/>
<part name="R36" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0603" package3d_urn="urn:adsk.eagle:package:3811166/1" value="DNP"/>
<part name="R41" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="49.9K"/>
<part name="Q11" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TRANSISTOR_PNP" device="SOT-323" package3d_urn="urn:adsk.eagle:package:3809951/2" value="CMST2907A TR PBFREE"/>
<part name="C20" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="0.1uF"/>
<part name="SUPPLY45" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="U2" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="LM317_REGULATOR" device="TO-261AA" package3d_urn="urn:adsk.eagle:package:3834044/1" value="LM317AEMP/NOPB"/>
<part name="D17" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="DIODE_SCHOTTKY" device="DO214AC_SMA" package3d_urn="urn:adsk.eagle:package:15671034/5" value="CGRA4001-G"/>
<part name="R108" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="0"/>
<part name="R102" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="0"/>
<part name="D23" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="DIODE_TVS_UNI-DIRECTIONAL" device="DIO214AB_SMC_TVS" package3d_urn="urn:adsk.eagle:package:15726434/3" value="1SMC33AT3G"/>
<part name="R110" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="0"/>
<part name="R105" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="0"/>
<part name="D24" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="DIODE_TVS_UNI-DIRECTIONAL" device="DIO214AB_SMC_TVS" package3d_urn="urn:adsk.eagle:package:15726434/3" value="1SMC33AT3G"/>
<part name="C1" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0603" package3d_urn="urn:adsk.eagle:package:3793126/2" value="DNP"/>
<part name="C6" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0603" package3d_urn="urn:adsk.eagle:package:3793126/2" value="10nF"/>
<part name="C8" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0603" package3d_urn="urn:adsk.eagle:package:3793126/2" value="DNP"/>
<part name="R12" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0603" package3d_urn="urn:adsk.eagle:package:3811166/1" value="442K"/>
<part name="R18" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0603" package3d_urn="urn:adsk.eagle:package:3811166/1" value="30K"/>
<part name="Q9" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="MOSFET_PCH" device="SOT-23" package3d_urn="urn:adsk.eagle:package:3809961/2" value="SSM3J356R,LF "/>
<part name="Q1" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="MOSFET_PCH" device="SOT-23" package3d_urn="urn:adsk.eagle:package:3809961/2" value="SSM3J356R,LF "/>
<part name="Q7" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="MOSFET_NCH" device="SOT-23" package3d_urn="urn:adsk.eagle:package:3809961/2" value="SSM3K2615R,LF "/>
<part name="R15" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="10K"/>
<part name="SUPPLY47" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="R4" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0603" package3d_urn="urn:adsk.eagle:package:3811166/1" value="DNP"/>
<part name="SUPPLY48" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="R9" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0603" package3d_urn="urn:adsk.eagle:package:3811166/1" value="0"/>
<part name="C11" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0603" package3d_urn="urn:adsk.eagle:package:3793126/2" value="DNP"/>
<part name="R1" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0603" package3d_urn="urn:adsk.eagle:package:3811166/1" value="DNP"/>
<part name="D4" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="DIODE_SCHOTTKY" device="DO214AC_SMA" package3d_urn="urn:adsk.eagle:package:15671034/5" value="DNP"/>
<part name="D9" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="DIODE_SCHOTTKY" device="DO214AC_SMA" package3d_urn="urn:adsk.eagle:package:15671034/5" value="3SMAJ5925B-TP"/>
<part name="D12" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="DIODE" device="DO-214AA_SMB_UNI" package3d_urn="urn:adsk.eagle:package:15617780/3" value="ES2D-13-F"/>
<part name="C2" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0603" package3d_urn="urn:adsk.eagle:package:3793126/2" value="DNP"/>
<part name="C5" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0603" package3d_urn="urn:adsk.eagle:package:3793126/2" value="10nF"/>
<part name="C9" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0603" package3d_urn="urn:adsk.eagle:package:3793126/2" value="DNP"/>
<part name="R7" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0603" package3d_urn="urn:adsk.eagle:package:3811166/1" value="442K"/>
<part name="R16" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0603" package3d_urn="urn:adsk.eagle:package:3811166/1" value="30K"/>
<part name="Q8" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="MOSFET_PCH" device="SOT-23" package3d_urn="urn:adsk.eagle:package:3809961/2" value="SSM3J356R,LF "/>
<part name="Q2" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="MOSFET_PCH" device="SOT-23" package3d_urn="urn:adsk.eagle:package:3809961/2" value="SSM3J356R,LF "/>
<part name="Q4" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="MOSFET_NCH" device="SOT-23" package3d_urn="urn:adsk.eagle:package:3809961/2" value="SSM3K2615R,LF "/>
<part name="R13" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="10K"/>
<part name="SUPPLY51" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="R5" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0603" package3d_urn="urn:adsk.eagle:package:3811166/1" value="DNP"/>
<part name="SUPPLY52" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="R10" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0603" package3d_urn="urn:adsk.eagle:package:3811166/1" value="0"/>
<part name="C12" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0603" package3d_urn="urn:adsk.eagle:package:3793126/2" value="DNP"/>
<part name="R2" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0603" package3d_urn="urn:adsk.eagle:package:3811166/1" value="DNP"/>
<part name="D5" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="DIODE_SCHOTTKY" device="DO214AC_SMA" package3d_urn="urn:adsk.eagle:package:15671034/5" value="DNP"/>
<part name="D7" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="DIODE_SCHOTTKY" device="DO214AC_SMA" package3d_urn="urn:adsk.eagle:package:15671034/5" value="3SMAJ5925B-TP"/>
<part name="D11" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="DIODE" device="DO-214AA_SMB_UNI" package3d_urn="urn:adsk.eagle:package:15617780/3" value="ES2D-13-F"/>
<part name="C3" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0603" package3d_urn="urn:adsk.eagle:package:3793126/2" value="DNP"/>
<part name="C4" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0603" package3d_urn="urn:adsk.eagle:package:3793126/2" value="10nF"/>
<part name="C7" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0603" package3d_urn="urn:adsk.eagle:package:3793126/2" value="DNP"/>
<part name="R8" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0603" package3d_urn="urn:adsk.eagle:package:3811166/1" value="442K"/>
<part name="R17" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0603" package3d_urn="urn:adsk.eagle:package:3811166/1" value="30K"/>
<part name="Q6" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="MOSFET_PCH" device="SOT-23" package3d_urn="urn:adsk.eagle:package:3809961/2" value="SSM3J356R,LF "/>
<part name="Q3" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="MOSFET_PCH" device="SOT-23" package3d_urn="urn:adsk.eagle:package:3809961/2" value="SSM3J356R,LF "/>
<part name="Q5" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="MOSFET_NCH" device="SOT-23" package3d_urn="urn:adsk.eagle:package:3809961/2" value="SSM3K2615R,LF "/>
<part name="R14" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="10K"/>
<part name="SUPPLY49" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="R6" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0603" package3d_urn="urn:adsk.eagle:package:3811166/1" value="DNP"/>
<part name="SUPPLY50" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="R11" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0603" package3d_urn="urn:adsk.eagle:package:3811166/1" value="0"/>
<part name="C10" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0603" package3d_urn="urn:adsk.eagle:package:3793126/2" value="DNP"/>
<part name="R3" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0603" package3d_urn="urn:adsk.eagle:package:3811166/1" value="DNP"/>
<part name="D6" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="DIODE_SCHOTTKY" device="DO214AC_SMA" package3d_urn="urn:adsk.eagle:package:15671034/5" value="DNP"/>
<part name="D8" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="DIODE_SCHOTTKY" device="DO214AC_SMA" package3d_urn="urn:adsk.eagle:package:15671034/5" value="3SMAJ5925B-TP"/>
<part name="D10" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="DIODE" device="DO-214AA_SMB_UNI" package3d_urn="urn:adsk.eagle:package:15617780/3" value="ES2D-13-F"/>
<part name="SUPPLY56" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="R28" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="10K"/>
<part name="R27" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="56K"/>
<part name="R29" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="10K"/>
<part name="R32" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="8.87K"/>
<part name="C14" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0603" package3d_urn="urn:adsk.eagle:package:3793126/2" value="6.8nF"/>
<part name="SUPPLY57" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="R33" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="470K"/>
<part name="R31" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="10"/>
<part name="C17" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0805" package3d_urn="urn:adsk.eagle:package:3793139/1" value="DNP"/>
<part name="C13" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0603" package3d_urn="urn:adsk.eagle:package:3793126/2" value="22nF"/>
<part name="R30" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="1K"/>
<part name="U1" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TI_TPS2491DGSR" device="10-TFSOP" package3d_urn="urn:adsk.eagle:package:3921298/1" value="TPS2491DGSR"/>
<part name="R24" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:3811169/1" value="0.20"/>
<part name="Q10" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="MOSFET_NCH" device="TO-252-3_DPAK" package3d_urn="urn:adsk.eagle:package:3920001/3" value="BUK72150-55A,118"/>
<part name="R26" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:3811169/1" value="DNP"/>
<part name="R25" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:3811169/1" value="0"/>
<part name="SUPPLY58" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="C18" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0603" package3d_urn="urn:adsk.eagle:package:3793126/2" value="DNP"/>
<part name="R35" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="0"/>
<part name="D16" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="DIODE_SCHOTTKY" device="DO214AC_SMA" package3d_urn="urn:adsk.eagle:package:15671034/5" value="CDBA140-G "/>
<part name="TP7" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TP_CLASS_B" device="CIR_PAD_1MM" package3d_urn="urn:adsk.eagle:package:8414067/2" value="PG_OUT"/>
<part name="R22" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="1206" package3d_urn="urn:adsk.eagle:package:3811173/1" value="0"/>
<part name="R107" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="0"/>
<part name="R100" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="0"/>
<part name="D22" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="DIODE_TVS_UNI-DIRECTIONAL" device="DIO214AB_SMC_TVS" package3d_urn="urn:adsk.eagle:package:15726434/3" value="1SMC33AT3G"/>
<part name="SUPPLY59" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="R23" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="1206" package3d_urn="urn:adsk.eagle:package:3811173/1" value="0"/>
<part name="TP5" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TP_CLASS_B" device="CIR_PAD_1MM" package3d_urn="urn:adsk.eagle:package:8414067/2" value="AISG_2_VOUT"/>
<part name="TP4" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TP_CLASS_B" device="CIR_PAD_1MM" package3d_urn="urn:adsk.eagle:package:8414067/2" value="AISG_3_VOUT"/>
<part name="D1" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="DIODE" device="DO-214AA_SMB_UNI" package3d_urn="urn:adsk.eagle:package:15617780/3" value="ES2D-13-F"/>
<part name="D2" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="DIODE" device="DO-214AA_SMB_UNI" package3d_urn="urn:adsk.eagle:package:15617780/3" value="ES2D-13-F"/>
<part name="D3" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="DIODE" device="DO-214AA_SMB_UNI" package3d_urn="urn:adsk.eagle:package:15617780/3" value="ES2D-13-F"/>
<part name="TP1" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TP_CLASS_B" device="CIR_PAD_1MM" package3d_urn="urn:adsk.eagle:package:8414067/2" value="INTERFACE_3"/>
<part name="TP3" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TP_CLASS_B" device="CIR_PAD_1MM" package3d_urn="urn:adsk.eagle:package:8414067/2" value="INTERFACE_2"/>
<part name="TP2" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TP_CLASS_B" device="CIR_PAD_1MM" package3d_urn="urn:adsk.eagle:package:8414067/2" value="INTERFACE_1"/>
<part name="TP21" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TP_CLASS_B" device="CIR_PAD_1MM" package3d_urn="urn:adsk.eagle:package:8414067/2" value="PA02"/>
<part name="TP13" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TP_CLASS_B" device="CIR_PAD_1MM" package3d_urn="urn:adsk.eagle:package:8414067/2" value="OE"/>
<part name="TP12" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TP_CLASS_B" device="CIR_PAD_1MM" package3d_urn="urn:adsk.eagle:package:8414067/2" value="OE"/>
<part name="TP30" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TP_CLASS_B" device="CIR_PAD_1MM" package3d_urn="urn:adsk.eagle:package:8414067/2" value="COM_7.3"/>
<part name="J9" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CONNECTOR_4POS" device="JST_B04B-PASK" package3d_urn="urn:adsk.eagle:package:8414269/2" value="B04B-PASK(LF)(SN)"/>
<part name="TP29" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TP_CLASS_B" device="CIR_PAD_1MM" package3d_urn="urn:adsk.eagle:package:8414067/2" value="COM_7.3"/>
<part name="L7" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="INDUCTOR" device="4.90MM_X_4.90MM_SMT" package3d_urn="urn:adsk.eagle:package:3904911/1" value="10uH"/>
<part name="TP18" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TP_CLASS_B" device="CIR_PAD_1MM" package3d_urn="urn:adsk.eagle:package:8414067/2" value="HB3_SPEED"/>
<part name="TP17" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TP_CLASS_B" device="CIR_PAD_1MM" package3d_urn="urn:adsk.eagle:package:8414067/2" value="HB3_SPEED"/>
<part name="TP20" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TP_CLASS_B" device="CIR_PAD_1MM" package3d_urn="urn:adsk.eagle:package:8414067/2" value="PA02"/>
<part name="TP9" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TP_CLASS_B" device="CIR_PAD_1MM" package3d_urn="urn:adsk.eagle:package:8414067/2" value="PA02"/>
<part name="D27" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="LED" device="0402" package3d_urn="urn:adsk.eagle:package:15620674/1" value="1830-1056-1-ND"/>
<part name="R145" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="510"/>
<part name="U7" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="AMIS_30624" device="NQFP" package3d_urn="urn:adsk.eagle:package:3837100/3" value="AMIS 30624"/>
<part name="C59" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="220nF"/>
<part name="C58" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="220nF"/>
<part name="SUPPLY92" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY93" library="supply2" deviceset="GND" device=""/>
<part name="NC18" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="NC" device=""/>
<part name="NC19" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="NC" device=""/>
<part name="NC20" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="NC" device=""/>
<part name="C54" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0603" package3d_urn="urn:adsk.eagle:package:3793126/2" value="3.3uF"/>
<part name="R74" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="1K"/>
<part name="SUPPLY94" library="supply2" deviceset="GND" device=""/>
<part name="R58" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="1K">
<attribute name="SPICEPREFIX" value="R"/>
</part>
<part name="C48" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0603" package3d_urn="urn:adsk.eagle:package:3793126/2" value="3.3uF">
<attribute name="SPICEPREFIX" value="C"/>
</part>
<part name="SUPPLY95" library="supply2" deviceset="GND" device="">
<attribute name="SPICEPREFIX" value="G"/>
</part>
<part name="R57" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="DNP">
<attribute name="SPICEPREFIX" value="R"/>
</part>
<part name="R75" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="DNP"/>
<part name="R52" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="0">
<attribute name="SPICEPREFIX" value="R"/>
</part>
<part name="R70" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="0"/>
<part name="SUPPLY96" library="supply2" deviceset="GND" device="">
<attribute name="SPICEPREFIX" value="G"/>
</part>
<part name="SUPPLY97" library="supply2" deviceset="GND" device=""/>
<part name="C53" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="0.1uF"/>
<part name="R55" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="0">
<attribute name="SPICEPREFIX" value="R"/>
</part>
<part name="R59" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="DNP">
<attribute name="SPICEPREFIX" value="R"/>
</part>
<part name="R61" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="0">
<attribute name="SPICEPREFIX" value="R"/>
</part>
<part name="R63" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="DNP">
<attribute name="SPICEPREFIX" value="R"/>
</part>
<part name="U9" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="AMIS_30624" device="NQFP" package3d_urn="urn:adsk.eagle:package:3837100/3" value="AMIS 30624"/>
<part name="C67" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="220nF"/>
<part name="C69" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="220nF"/>
<part name="SUPPLY104" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY105" library="supply2" deviceset="GND" device=""/>
<part name="NC24" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="NC" device=""/>
<part name="NC25" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="NC" device=""/>
<part name="NC26" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="NC" device=""/>
<part name="C61" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0603" package3d_urn="urn:adsk.eagle:package:3793126/2" value="3.3uF"/>
<part name="R83" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="1K"/>
<part name="SUPPLY106" library="supply2" deviceset="GND" device=""/>
<part name="R94" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="1K">
<attribute name="SPICEPREFIX" value="R"/>
</part>
<part name="C65" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0603" package3d_urn="urn:adsk.eagle:package:3793126/2" value="3.3uF">
<attribute name="SPICEPREFIX" value="C"/>
</part>
<part name="SUPPLY107" library="supply2" deviceset="GND" device="">
<attribute name="SPICEPREFIX" value="G"/>
</part>
<part name="R96" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="DNP">
<attribute name="SPICEPREFIX" value="R"/>
</part>
<part name="R84" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="0"/>
<part name="R92" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="0">
<attribute name="SPICEPREFIX" value="R"/>
</part>
<part name="R90" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="DNP"/>
<part name="SUPPLY108" library="supply2" deviceset="GND" device="">
<attribute name="SPICEPREFIX" value="G"/>
</part>
<part name="SUPPLY109" library="supply2" deviceset="GND" device=""/>
<part name="C63" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="0.1uF"/>
<part name="R85" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="0">
<attribute name="SPICEPREFIX" value="R"/>
</part>
<part name="R86" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="DNP">
<attribute name="SPICEPREFIX" value="R"/>
</part>
<part name="R87" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="0">
<attribute name="SPICEPREFIX" value="R"/>
</part>
<part name="R88" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="DNP">
<attribute name="SPICEPREFIX" value="R"/>
</part>
<part name="U8" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="AMIS_30624" device="NQFP" package3d_urn="urn:adsk.eagle:package:3837100/3" value="AMIS 30624"/>
<part name="C66" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="220nF"/>
<part name="C68" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="220nF"/>
<part name="SUPPLY116" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY117" library="supply2" deviceset="GND" device=""/>
<part name="NC30" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="NC" device=""/>
<part name="NC31" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="NC" device=""/>
<part name="NC32" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="NC" device=""/>
<part name="C60" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0603" package3d_urn="urn:adsk.eagle:package:3793126/2" value="3.3uF"/>
<part name="R81" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="1K"/>
<part name="SUPPLY118" library="supply2" deviceset="GND" device=""/>
<part name="R93" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="1K">
<attribute name="SPICEPREFIX" value="R"/>
</part>
<part name="C64" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0603" package3d_urn="urn:adsk.eagle:package:3793126/2" value="3.3uF">
<attribute name="SPICEPREFIX" value="C"/>
</part>
<part name="SUPPLY119" library="supply2" deviceset="GND" device="">
<attribute name="SPICEPREFIX" value="G"/>
</part>
<part name="R95" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="DNP">
<attribute name="SPICEPREFIX" value="R"/>
</part>
<part name="R82" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="DNP"/>
<part name="R91" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="0">
<attribute name="SPICEPREFIX" value="R"/>
</part>
<part name="R89" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="0"/>
<part name="SUPPLY120" library="supply2" deviceset="GND" device="">
<attribute name="SPICEPREFIX" value="G"/>
</part>
<part name="SUPPLY121" library="supply2" deviceset="GND" device=""/>
<part name="C62" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="0.1uF"/>
<part name="R77" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="DNP">
<attribute name="SPICEPREFIX" value="R"/>
</part>
<part name="R78" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="0">
<attribute name="SPICEPREFIX" value="R"/>
</part>
<part name="R79" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="DNP">
<attribute name="SPICEPREFIX" value="R"/>
</part>
<part name="R80" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="0">
<attribute name="SPICEPREFIX" value="R"/>
</part>
<part name="U5" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="AMIS_30624" device="NQFP" package3d_urn="urn:adsk.eagle:package:3837100/3" value="AMIS 30624"/>
<part name="C46" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="220nF"/>
<part name="C47" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="220nF"/>
<part name="SUPPLY128" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY129" library="supply2" deviceset="GND" device=""/>
<part name="NC36" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="NC" device=""/>
<part name="NC37" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="NC" device=""/>
<part name="NC38" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="NC" device=""/>
<part name="C51" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0603" package3d_urn="urn:adsk.eagle:package:3793126/2" value="3.3uF"/>
<part name="R54" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="1K"/>
<part name="SUPPLY130" library="supply2" deviceset="GND" device=""/>
<part name="R71" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="1K">
<attribute name="SPICEPREFIX" value="R"/>
</part>
<part name="C56" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0603" package3d_urn="urn:adsk.eagle:package:3793126/2" value="3.3uF">
<attribute name="SPICEPREFIX" value="C"/>
</part>
<part name="SUPPLY131" library="supply2" deviceset="GND" device="">
<attribute name="SPICEPREFIX" value="G"/>
</part>
<part name="R72" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="DNP">
<attribute name="SPICEPREFIX" value="R"/>
</part>
<part name="R53" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="0"/>
<part name="R76" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="0">
<attribute name="SPICEPREFIX" value="R"/>
</part>
<part name="R60" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="DNP"/>
<part name="SUPPLY132" library="supply2" deviceset="GND" device="">
<attribute name="SPICEPREFIX" value="G"/>
</part>
<part name="SUPPLY133" library="supply2" deviceset="GND" device=""/>
<part name="C52" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="0.1uF"/>
<part name="R73" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="DNP">
<attribute name="SPICEPREFIX" value="R"/>
</part>
<part name="R67" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="0">
<attribute name="SPICEPREFIX" value="R"/>
</part>
<part name="R66" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="DNP">
<attribute name="SPICEPREFIX" value="R"/>
</part>
<part name="R65" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="0">
<attribute name="SPICEPREFIX" value="R"/>
</part>
<part name="J5" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CONNECTOR_8POS" device="5031590800" package3d_urn="urn:adsk.eagle:package:21515996/2" value="5031590800"/>
<part name="J6" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CONNECTOR_8POS" device="5031590800" package3d_urn="urn:adsk.eagle:package:21515996/2" value="5031590800"/>
<part name="J7" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CONNECTOR_8POS" device="5031590800" package3d_urn="urn:adsk.eagle:package:21515996/2" value="5031590800"/>
<part name="J4" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CONNECTOR_8POS" device="5031590800" package3d_urn="urn:adsk.eagle:package:21515996/2" value="5031590800"/>
<part name="C106" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="0.1uF"/>
<part name="C105" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="0.1uF"/>
<part name="C107" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0603" package3d_urn="urn:adsk.eagle:package:3793126/2" value="1uF"/>
<part name="C104" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0603" package3d_urn="urn:adsk.eagle:package:3793126/2" value="1uF"/>
<part name="R157" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="4.7K"/>
<part name="R158" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="4.7K"/>
<part name="R154" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="4.7K"/>
<part name="R156" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="4.7K"/>
<part name="SUPPLY40" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY41" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="R155" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="4.7K"/>
<part name="SUPPLY42" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="TP35" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TP_CLASS_B" device="CIR_PAD_1MM" package3d_urn="urn:adsk.eagle:package:8414067/2" value="SDA2_5V"/>
<part name="TP36" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TP_CLASS_B" device="CIR_PAD_1MM" package3d_urn="urn:adsk.eagle:package:8414067/2" value="SCL_2"/>
<part name="TP34" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TP_CLASS_B" device="CIR_PAD_1MM" package3d_urn="urn:adsk.eagle:package:8414067/2" value="GND"/>
<part name="SUPPLY63" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="TP37" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TP_CLASS_B" device="CIR_PAD_1MM" package3d_urn="urn:adsk.eagle:package:8414067/2" value="OE"/>
<part name="U15" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TI_TCA9406" device="8-VFSOP_DCU" package3d_urn="urn:adsk.eagle:package:3986204/1" value="TCA9406DCUR"/>
<part name="R115" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="DNP"/>
<part name="R116" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="DNP"/>
<part name="R117" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="DNP"/>
<part name="R118" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="DNP"/>
<part name="TP6" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TP_CLASS_B" device="CIR_PAD_1MM" package3d_urn="urn:adsk.eagle:package:8414067/2" value="AISG_1_VOUT"/>
<part name="D26" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="LED" device="0402" package3d_urn="urn:adsk.eagle:package:15620674/1" value="1830-1056-1-ND"/>
<part name="D25" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="LED" device="0402" package3d_urn="urn:adsk.eagle:package:15620674/1" value="1830-1056-1-ND"/>
<part name="R137" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="510"/>
<part name="R135" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="510"/>
<part name="FRAME2" library="frames" library_urn="urn:adsk.eagle:library:229" deviceset="A3L-LOC" device=""/>
<part name="FRAME3" library="frames" library_urn="urn:adsk.eagle:library:229" deviceset="A3L-LOC" device=""/>
<part name="FRAME4" library="frames" library_urn="urn:adsk.eagle:library:229" deviceset="A3L-LOC" device=""/>
<part name="FRAME5" library="frames" library_urn="urn:adsk.eagle:library:229" deviceset="A3L-LOC" device=""/>
<part name="FRAME7" library="frames" library_urn="urn:adsk.eagle:library:229" deviceset="A3L-LOC" device=""/>
<part name="J1" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CONNECTOR_4POS" device="MOLEX_5031590400" package3d_urn="urn:adsk.eagle:package:8414052/2" value="5031590400"/>
<part name="J2" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CONNECTOR_4POS" device="MOLEX_5031590400" package3d_urn="urn:adsk.eagle:package:8414052/2" value="5031590400"/>
<part name="J3" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CONNECTOR_4POS" device="MOLEX_5031590400" package3d_urn="urn:adsk.eagle:package:8414052/2" value="5031590400"/>
<part name="R20" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:3811169/1" value="DNP"/>
<part name="TP26" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TP_CLASS_B" device="CIR_PAD_1MM" package3d_urn="urn:adsk.eagle:package:8414067/2" value="INTERFACE_1"/>
<part name="TP23" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TP_CLASS_B" device="CIR_PAD_1MM" package3d_urn="urn:adsk.eagle:package:8414067/2" value="INTERFACE_1"/>
<part name="TP24" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TP_CLASS_B" device="CIR_PAD_1MM" package3d_urn="urn:adsk.eagle:package:8414067/2" value="INTERFACE_1"/>
<part name="TP27" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TP_CLASS_B" device="CIR_PAD_1MM" package3d_urn="urn:adsk.eagle:package:8414067/2" value="INTERFACE_1"/>
<part name="TP22" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TP_CLASS_B" device="CIR_PAD_1MM" package3d_urn="urn:adsk.eagle:package:8414067/2" value="INTERFACE_1"/>
<part name="TP25" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TP_CLASS_B" device="CIR_PAD_1MM" package3d_urn="urn:adsk.eagle:package:8414067/2" value="INTERFACE_1"/>
<part name="R19" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:3811169/1" value="DNP"/>
<part name="R21" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:3811169/1" value="DNP"/>
<part name="SUPPLY10" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="GND" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TP_CLASS_B" device="CIR_PAD_1MM" package3d_urn="urn:adsk.eagle:package:8414067/2" value="GND"/>
<part name="R162" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:3811169/1" value="0"/>
<part name="R163" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="470"/>
<part name="R160" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="470"/>
<part name="R161" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="470"/>
<part name="R159" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:3811169/1" value="0"/>
<part name="R164" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:3811169/1" value="0"/>
<part name="R165" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:3811169/1" value="0"/>
<part name="R166" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:3811169/1" value="0"/>
<part name="FRAME6" library="frames" library_urn="urn:adsk.eagle:library:229" deviceset="A3L-LOC" device="" value="1"/>
<part name="FRAME1" library="frames" library_urn="urn:adsk.eagle:library:229" deviceset="A3L-LOC" device=""/>
<part name="TP38" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TP_CLASS_B" device="CIR_PAD_1MM" package3d_urn="urn:adsk.eagle:package:8414067/2"/>
<part name="TP39" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TP_CLASS_B" device="CIR_PAD_1MM" package3d_urn="urn:adsk.eagle:package:8414067/2"/>
<part name="TP40" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TP_CLASS_B" device="CIR_PAD_1MM" package3d_urn="urn:adsk.eagle:package:8414067/2"/>
<part name="TP41" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TP_CLASS_B" device="CIR_PAD_1MM" package3d_urn="urn:adsk.eagle:package:8414067/2"/>
<part name="TP42" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TP_CLASS_B" device="CIR_PAD_1MM" package3d_urn="urn:adsk.eagle:package:8414067/2"/>
<part name="D21" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="DIODE_SCHOTTKY" device="DO214AC_SMA" package3d_urn="urn:adsk.eagle:package:15671034/5" value="DNP"/>
<part name="F1" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="FIDUCIAL" device="0.5MM" package3d_urn="urn:adsk.eagle:package:8414039/1"/>
<part name="F2" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="FIDUCIAL" device="0.5MM" package3d_urn="urn:adsk.eagle:package:8414039/1"/>
<part name="F3" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="FIDUCIAL" device="0.5MM" package3d_urn="urn:adsk.eagle:package:8414039/1"/>
<part name="F4" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="FIDUCIAL" device="0.5MM" package3d_urn="urn:adsk.eagle:package:8414039/1"/>
<part name="F5" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="FIDUCIAL" device="0.5MM" package3d_urn="urn:adsk.eagle:package:8414039/1"/>
<part name="F6" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="FIDUCIAL" device="0.5MM" package3d_urn="urn:adsk.eagle:package:8414039/1"/>
<part name="J8" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TC2050-IDC-NL" device="10PIN" package3d_urn="urn:adsk.eagle:package:16170706/4" value="TC2050-IDC-NL"/>
<part name="NC1" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="NC" device=""/>
<part name="NC3" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="NC" device=""/>
<part name="NC2" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="NC" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<text x="370.84" y="10.16" size="1.27" layer="94">0</text>
<text x="130.556" y="154.94" size="1.27" layer="97" rot="R90">Replace Inductor with 0 ohm
if Linear regualtors used.</text>
<text x="124.46" y="182.88" size="1.27" layer="97">LQM18PN1R0MFHD </text>
<text x="325.12" y="78.74" size="1.27" layer="97">Optional #LTMM-105-02-F-D</text>
<text x="238.76" y="60.96" size="1.27" layer="97" rot="R90">AISG_3</text>
<text x="238.76" y="91.44" size="1.27" layer="97" rot="R90">AISG_1</text>
<text x="238.76" y="210.82" size="1.27" layer="97">VDDIO</text>
<text x="119.38" y="10.16" size="1.27" layer="97">External Memory</text>
<text x="238.76" y="73.66" size="1.27" layer="97" rot="R90">AISG_2</text>
<text x="144.78" y="10.16" size="1.27" layer="97">Debug</text>
<text x="299.72" y="165.1" size="1.27" layer="97">PROCESSOR-&gt;EEPROM </text>
<text x="101.6" y="10.16" size="1.27" layer="97">MOTOR DRIVER 1</text>
<text x="154.94" y="10.16" size="1.27" layer="97">MOTOR DRIVER 2</text>
</plain>
<instances>
<instance part="SUPPLY11" gate="GND" x="83.82" y="35.56" smashed="yes">
<attribute name="VALUE" x="81.915" y="32.385" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY12" gate="GND" x="170.18" y="137.16" smashed="yes">
<attribute name="VALUE" x="168.275" y="133.985" size="1.27" layer="96"/>
</instance>
<instance part="C97" gate="G$1" x="304.8" y="198.12" smashed="yes" rot="R90">
<attribute name="NAME" x="304.292" y="193.04" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="304.292" y="199.39" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="C96" gate="G$1" x="309.88" y="198.12" smashed="yes" rot="R90">
<attribute name="NAME" x="309.372" y="193.04" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="309.372" y="199.39" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="C89" gate="G$1" x="220.98" y="27.94" smashed="yes" rot="R90">
<attribute name="NAME" x="220.472" y="22.86" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="220.472" y="29.21" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="C100" gate="G$1" x="320.04" y="198.12" smashed="yes" rot="R90">
<attribute name="NAME" x="319.532" y="193.04" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="319.532" y="199.39" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="C101" gate="G$1" x="325.12" y="198.12" smashed="yes" rot="R90">
<attribute name="NAME" x="324.612" y="193.04" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="324.612" y="199.39" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="C102" gate="G$1" x="335.28" y="198.12" smashed="yes" rot="R90">
<attribute name="NAME" x="334.772" y="193.04" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="334.772" y="199.39" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="C103" gate="G$1" x="340.36" y="198.12" smashed="yes" rot="R90">
<attribute name="NAME" x="339.852" y="193.04" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="339.852" y="199.39" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="C83" gate="G$1" x="350.52" y="198.12" smashed="yes" rot="R90">
<attribute name="NAME" x="350.012" y="193.04" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="350.012" y="199.39" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="C81" gate="G$1" x="355.6" y="198.12" smashed="yes" rot="R90">
<attribute name="NAME" x="355.092" y="193.04" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="355.092" y="199.39" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="C91" gate="G$1" x="226.06" y="27.94" smashed="yes" rot="R90">
<attribute name="NAME" x="225.552" y="22.86" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="225.552" y="29.21" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="SUPPLY13" gate="GND" x="299.72" y="185.42" smashed="yes">
<attribute name="VALUE" x="297.815" y="182.245" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY14" gate="GND" x="220.98" y="12.7" smashed="yes">
<attribute name="VALUE" x="226.441" y="13.335" size="1.27" layer="96" rot="R180"/>
</instance>
<instance part="R120" gate="G$1" x="271.78" y="81.28" smashed="yes" rot="MR90">
<attribute name="NAME" x="271.2974" y="78.74" size="1.27" layer="95" rot="MR270"/>
<attribute name="VALUE" x="271.272" y="88.138" size="1.27" layer="96" rot="MR270"/>
</instance>
<instance part="R119" gate="G$1" x="279.4" y="71.12" smashed="yes" rot="MR180">
<attribute name="NAME" x="276.86" y="71.6026" size="1.27" layer="95" rot="MR0"/>
<attribute name="VALUE" x="285.496" y="71.628" size="1.27" layer="96" rot="MR0"/>
</instance>
<instance part="C87" gate="G$1" x="271.78" y="63.5" smashed="yes" rot="MR90">
<attribute name="NAME" x="272.288" y="58.42" size="1.27" layer="95" rot="MR90"/>
<attribute name="VALUE" x="272.288" y="64.77" size="1.27" layer="96" rot="MR90"/>
</instance>
<instance part="SW1" gate="G$1" x="287.02" y="63.5" smashed="yes" rot="MR90">
<attribute name="NAME" x="287.528" y="58.42" size="1.27" layer="95" rot="MR90"/>
<attribute name="VALUE" x="289.56" y="58.42" size="1.27" layer="96" rot="MR90"/>
</instance>
<instance part="+3V2" gate="G$1" x="271.78" y="91.44" smashed="yes" rot="MR0">
<attribute name="VALUE" x="274.574" y="91.694" size="1.27" layer="96" rot="MR0"/>
</instance>
<instance part="SUPPLY15" gate="GND" x="287.02" y="50.8" smashed="yes" rot="MR0">
<attribute name="VALUE" x="292.481" y="52.197" size="1.27" layer="96" rot="MR0"/>
</instance>
<instance part="R126" gate="G$1" x="309.88" y="81.28" smashed="yes" rot="R180">
<attribute name="NAME" x="303.784" y="81.7626" size="1.27" layer="95"/>
<attribute name="VALUE" x="312.42" y="81.788" size="1.27" layer="96"/>
</instance>
<instance part="+3V6" gate="G$1" x="302.26" y="91.44" smashed="yes">
<attribute name="VALUE" x="299.72" y="86.36" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="SUPPLY16" gate="GND" x="307.34" y="53.34" smashed="yes">
<attribute name="VALUE" x="305.435" y="50.165" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY17" gate="GND" x="185.42" y="35.56" smashed="yes">
<attribute name="VALUE" x="183.515" y="32.385" size="1.27" layer="96"/>
</instance>
<instance part="C99" gate="G$1" x="289.56" y="198.12" smashed="yes" rot="R90">
<attribute name="NAME" x="289.052" y="193.04" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="289.052" y="199.39" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="C82" gate="G$1" x="294.64" y="198.12" smashed="yes" rot="R90">
<attribute name="NAME" x="294.132" y="193.04" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="294.132" y="199.39" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="C95" gate="G$1" x="274.32" y="198.12" smashed="yes" rot="R90">
<attribute name="NAME" x="273.812" y="193.04" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="273.812" y="199.39" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="C98" gate="G$1" x="279.4" y="198.12" smashed="yes" rot="R90">
<attribute name="NAME" x="278.892" y="193.04" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="278.892" y="199.39" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="C84" gate="G$1" x="259.08" y="198.12" smashed="yes" rot="R90">
<attribute name="NAME" x="258.572" y="193.04" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="258.572" y="199.39" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="C92" gate="G$1" x="264.16" y="198.12" smashed="yes" rot="R90">
<attribute name="NAME" x="263.652" y="193.04" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="263.652" y="199.39" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="U13" gate="G$1" x="134.62" y="91.44" smashed="yes">
<attribute name="NAME" x="134.62" y="93.98" size="1.27" layer="95"/>
<attribute name="VALUE" x="121.92" y="96.52" size="1.27" layer="96"/>
</instance>
<instance part="Y1" gate="G$1" x="208.28" y="185.42" smashed="yes">
<attribute name="NAME" x="207.264" y="190.246" size="1.27" layer="95"/>
<attribute name="VALUE" x="201.676" y="188.468" size="1.27" layer="96"/>
</instance>
<instance part="C88" gate="G$1" x="200.66" y="177.8" smashed="yes" rot="R90">
<attribute name="NAME" x="200.152" y="172.72" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="200.152" y="179.07" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="C79" gate="G$1" x="215.9" y="177.8" smashed="yes" rot="R90">
<attribute name="NAME" x="215.392" y="172.72" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="215.392" y="179.07" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="SUPPLY18" gate="GND" x="208.28" y="162.56" smashed="yes" rot="MR0">
<attribute name="VALUE" x="213.741" y="163.957" size="1.27" layer="96" rot="MR0"/>
</instance>
<instance part="C80" gate="G$1" x="302.26" y="66.04" smashed="yes" rot="R90">
<attribute name="NAME" x="301.752" y="60.96" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="301.752" y="67.31" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R153" gate="G$1" x="71.12" y="63.5" smashed="yes" rot="MR0">
<attribute name="NAME" x="65.024" y="63.0174" size="1.27" layer="95" rot="MR180"/>
<attribute name="VALUE" x="73.66" y="62.992" size="1.27" layer="96" rot="MR180"/>
</instance>
<instance part="TP32" gate="G$1" x="243.84" y="205.74" smashed="yes" rot="R90">
<attribute name="NAME" x="240.03" y="204.47" size="1.27" layer="95" rot="R90"/>
</instance>
<instance part="C85" gate="G$1" x="370.84" y="198.12" smashed="yes" rot="R90">
<attribute name="NAME" x="370.332" y="193.04" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="370.332" y="199.39" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="C86" gate="G$1" x="365.76" y="198.12" smashed="yes" rot="R90">
<attribute name="NAME" x="365.252" y="193.04" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="365.252" y="199.39" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="TP31" gate="G$1" x="210.82" y="35.56" smashed="yes" rot="R90">
<attribute name="NAME" x="209.55" y="34.29" size="1.27" layer="95" rot="R180"/>
</instance>
<instance part="+3V8" gate="G$1" x="246.38" y="215.9" smashed="yes">
<attribute name="VALUE" x="243.078" y="216.408" size="1.27" layer="96"/>
</instance>
<instance part="TP28" gate="G$1" x="243.84" y="190.5" smashed="yes" rot="R90">
<attribute name="NAME" x="240.03" y="189.23" size="1.27" layer="95" rot="R90"/>
</instance>
<instance part="C94" gate="G$1" x="254" y="198.12" smashed="yes" rot="R90">
<attribute name="NAME" x="253.492" y="193.04" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="253.492" y="199.39" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="C90" gate="G$1" x="215.9" y="27.94" smashed="yes" rot="R90">
<attribute name="NAME" x="215.392" y="22.86" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="215.392" y="29.21" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R127" gate="G$1" x="309.88" y="86.36" smashed="yes" rot="R180">
<attribute name="NAME" x="303.784" y="86.8426" size="1.27" layer="95"/>
<attribute name="VALUE" x="312.42" y="86.868" size="1.27" layer="96"/>
</instance>
<instance part="R125" gate="G$1" x="314.96" y="60.96" smashed="yes" rot="R180">
<attribute name="NAME" x="308.61" y="61.4426" size="1.27" layer="95"/>
<attribute name="VALUE" x="317.5" y="61.468" size="1.27" layer="96"/>
</instance>
<instance part="R121" gate="G$1" x="134.62" y="152.4" smashed="yes" rot="R270">
<attribute name="NAME" x="134.1374" y="146.05" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="134.112" y="154.94" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R142" gate="G$1" x="30.48" y="119.38" smashed="yes" rot="R270">
<attribute name="NAME" x="29.9974" y="113.03" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="29.972" y="121.92" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R143" gate="G$1" x="35.56" y="119.38" smashed="yes" rot="R270">
<attribute name="NAME" x="35.0774" y="113.03" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="35.052" y="121.92" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R150" gate="G$1" x="40.64" y="119.38" smashed="yes" rot="R270">
<attribute name="NAME" x="40.1574" y="113.03" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="40.132" y="121.92" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R152" gate="G$1" x="45.72" y="119.38" smashed="yes" rot="R270">
<attribute name="NAME" x="45.2374" y="113.03" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="45.212" y="121.92" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R141" gate="G$1" x="30.48" y="147.32" smashed="yes" rot="R270">
<attribute name="NAME" x="29.9974" y="140.97" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="29.972" y="149.86" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R144" gate="G$1" x="35.56" y="147.32" smashed="yes" rot="R270">
<attribute name="NAME" x="35.0774" y="140.97" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="35.052" y="149.86" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R149" gate="G$1" x="40.64" y="147.32" smashed="yes" rot="R270">
<attribute name="NAME" x="40.1574" y="140.97" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="40.132" y="149.86" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R151" gate="G$1" x="45.72" y="147.32" smashed="yes" rot="R270">
<attribute name="NAME" x="45.2374" y="140.97" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="45.212" y="149.86" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="SUPPLY19" gate="GND" x="30.48" y="104.14" smashed="yes">
<attribute name="VALUE" x="28.575" y="100.965" size="1.27" layer="96"/>
</instance>
<instance part="+3V10" gate="G$1" x="45.72" y="160.02" smashed="yes">
<attribute name="VALUE" x="42.418" y="160.528" size="1.27" layer="96"/>
</instance>
<instance part="TP15" gate="G$1" x="66.04" y="83.82" smashed="yes" rot="R90">
<attribute name="NAME" x="63.246" y="84.582" size="1.27" layer="95" rot="R180"/>
</instance>
<instance part="TP8" gate="G$1" x="66.04" y="81.28" smashed="yes" rot="R90">
<attribute name="NAME" x="63.246" y="82.042" size="1.27" layer="95" rot="R180"/>
</instance>
<instance part="TP19" gate="G$1" x="66.04" y="78.74" smashed="yes" rot="R90">
<attribute name="NAME" x="63.246" y="79.502" size="1.27" layer="95" rot="R180"/>
</instance>
<instance part="R148" gate="G$1" x="33.02" y="73.66" smashed="yes" rot="MR0">
<attribute name="NAME" x="26.924" y="73.1774" size="1.27" layer="95" rot="MR180"/>
<attribute name="VALUE" x="35.56" y="73.152" size="1.27" layer="96" rot="MR180"/>
</instance>
<instance part="R147" gate="G$1" x="33.02" y="81.28" smashed="yes" rot="MR0">
<attribute name="NAME" x="26.924" y="80.7974" size="1.27" layer="95" rot="MR180"/>
<attribute name="VALUE" x="35.56" y="80.772" size="1.27" layer="96" rot="MR180"/>
</instance>
<instance part="R146" gate="G$1" x="33.02" y="88.9" smashed="yes" rot="MR0">
<attribute name="NAME" x="26.924" y="88.4174" size="1.27" layer="95" rot="MR180"/>
<attribute name="VALUE" x="35.56" y="88.392" size="1.27" layer="96" rot="MR180"/>
</instance>
<instance part="SUPPLY20" gate="GND" x="10.16" y="48.26" smashed="yes">
<attribute name="VALUE" x="8.255" y="45.085" size="1.27" layer="96"/>
</instance>
<instance part="R132" gate="G$1" x="10.16" y="154.94" smashed="yes" rot="R270">
<attribute name="NAME" x="9.6774" y="148.59" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="9.652" y="157.48" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R134" gate="G$1" x="15.24" y="154.94" smashed="yes" rot="R270">
<attribute name="NAME" x="14.7574" y="148.59" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="14.732" y="157.48" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R138" gate="G$1" x="20.32" y="154.94" smashed="yes" rot="R270">
<attribute name="NAME" x="19.8374" y="148.59" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="19.812" y="157.48" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R139" gate="G$1" x="25.4" y="154.94" smashed="yes" rot="R270">
<attribute name="NAME" x="24.9174" y="148.59" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="24.892" y="157.48" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R133" gate="G$1" x="10.16" y="182.88" smashed="yes" rot="R270">
<attribute name="NAME" x="9.6774" y="176.53" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="9.652" y="185.42" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R131" gate="G$1" x="15.24" y="182.88" smashed="yes" rot="R270">
<attribute name="NAME" x="14.7574" y="176.53" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="14.732" y="185.42" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R136" gate="G$1" x="20.32" y="182.88" smashed="yes" rot="R270">
<attribute name="NAME" x="19.8374" y="176.53" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="19.812" y="185.42" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R140" gate="G$1" x="25.4" y="182.88" smashed="yes" rot="R270">
<attribute name="NAME" x="24.9174" y="176.53" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="24.892" y="185.42" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="SUPPLY21" gate="GND" x="10.16" y="139.7" smashed="yes">
<attribute name="VALUE" x="8.255" y="136.525" size="1.27" layer="96"/>
</instance>
<instance part="+3V11" gate="G$1" x="25.4" y="195.58" smashed="yes">
<attribute name="VALUE" x="22.098" y="196.088" size="1.27" layer="96"/>
</instance>
<instance part="U14" gate="G$1" x="299.72" y="137.16" smashed="yes">
<attribute name="NAME" x="299.72" y="142.24" size="1.27" layer="95"/>
<attribute name="VALUE" x="299.72" y="139.7" size="1.27" layer="96"/>
</instance>
<instance part="R128" gate="G$1" x="289.56" y="149.86" smashed="yes" rot="R90">
<attribute name="NAME" x="289.56" y="142.24" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="289.56" y="154.94" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R129" gate="G$1" x="284.48" y="149.86" smashed="yes" rot="R90">
<attribute name="NAME" x="284.48" y="142.24" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="284.48" y="154.94" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R122" gate="G$1" x="340.36" y="149.86" smashed="yes" rot="R90">
<attribute name="NAME" x="340.36" y="142.24" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="340.36" y="154.94" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R123" gate="G$1" x="345.44" y="149.86" smashed="yes" rot="R90">
<attribute name="NAME" x="345.44" y="142.24" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="345.44" y="154.94" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R124" gate="G$1" x="350.52" y="149.86" smashed="yes" rot="R90">
<attribute name="NAME" x="350.52" y="142.24" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="350.52" y="154.94" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R130" gate="G$1" x="279.4" y="149.86" smashed="yes" rot="R90">
<attribute name="NAME" x="279.4" y="142.24" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="279.4" y="154.94" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="SUPPLY22" gate="GND" x="289.56" y="119.38" smashed="yes">
<attribute name="VALUE" x="287.655" y="116.205" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY23" gate="GND" x="317.5" y="149.86" smashed="yes">
<attribute name="VALUE" x="315.595" y="146.685" size="1.27" layer="96"/>
</instance>
<instance part="C93" gate="G$1" x="325.12" y="157.48" smashed="yes">
<attribute name="NAME" x="322.58" y="160.02" size="1.27" layer="95"/>
<attribute name="VALUE" x="322.58" y="152.4" size="1.27" layer="96"/>
</instance>
<instance part="+3V12" gate="G$1" x="363.22" y="167.64" smashed="yes">
<attribute name="VALUE" x="360.68" y="162.56" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="TP33" gate="G$1" x="60.96" y="63.5" smashed="yes" rot="R90">
<attribute name="NAME" x="58.166" y="64.262" size="1.27" layer="95" rot="R180"/>
</instance>
<instance part="D28" gate="G$1" x="17.78" y="88.9" smashed="yes" rot="R180">
<attribute name="NAME" x="19.304" y="86.36" size="1.27" layer="95"/>
<attribute name="VALUE" x="12.7" y="91.44" size="1.27" layer="96"/>
</instance>
<instance part="D29" gate="G$1" x="17.78" y="81.28" smashed="yes" rot="R180">
<attribute name="NAME" x="19.304" y="78.74" size="1.27" layer="95"/>
<attribute name="VALUE" x="12.7" y="83.82" size="1.27" layer="96"/>
</instance>
<instance part="D30" gate="G$1" x="17.78" y="73.66" smashed="yes" rot="R180">
<attribute name="NAME" x="19.304" y="71.12" size="1.27" layer="95"/>
<attribute name="VALUE" x="12.7" y="76.2" size="1.27" layer="96"/>
</instance>
<instance part="TP21" gate="G$1" x="66.04" y="86.36" smashed="yes" rot="R90">
<attribute name="NAME" x="63.246" y="87.122" size="1.27" layer="95" rot="R180"/>
</instance>
<instance part="TP30" gate="G$1" x="266.7" y="78.74" smashed="yes">
<attribute name="NAME" x="267.716" y="81.534" size="1.27" layer="95" rot="R90"/>
</instance>
<instance part="J9" gate="-1" x="241.3" y="35.56" smashed="yes" rot="R180">
<attribute name="NAME" x="244.602" y="35.052" size="1.27" layer="95" rot="R180"/>
</instance>
<instance part="J9" gate="-2" x="241.3" y="33.02" smashed="yes" rot="R180">
<attribute name="NAME" x="244.602" y="32.512" size="1.27" layer="95" rot="R180"/>
</instance>
<instance part="J9" gate="-3" x="241.3" y="30.48" smashed="yes" rot="R180">
<attribute name="NAME" x="244.602" y="29.972" size="1.27" layer="95" rot="R180"/>
</instance>
<instance part="J9" gate="-4" x="241.3" y="27.94" smashed="yes" rot="R180">
<attribute name="NAME" x="244.602" y="27.432" size="1.27" layer="95" rot="R180"/>
</instance>
<instance part="TP29" gate="G$1" x="152.4" y="30.48" smashed="yes" rot="R180">
<attribute name="NAME" x="151.384" y="27.686" size="1.27" layer="95" rot="R270"/>
</instance>
<instance part="L7" gate="G$1" x="127" y="144.78" smashed="yes" rot="R90">
<attribute name="NAME" x="126.492" y="138.684" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="126.492" y="147.574" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="TP18" gate="G$1" x="66.04" y="76.2" smashed="yes" rot="R90">
<attribute name="NAME" x="63.246" y="76.962" size="1.27" layer="95" rot="R180"/>
</instance>
<instance part="TP17" gate="G$1" x="66.04" y="73.66" smashed="yes" rot="R90">
<attribute name="NAME" x="63.246" y="74.422" size="1.27" layer="95" rot="R180"/>
</instance>
<instance part="TP20" gate="G$1" x="66.04" y="88.9" smashed="yes" rot="R90">
<attribute name="NAME" x="63.246" y="89.662" size="1.27" layer="95" rot="R180"/>
</instance>
<instance part="TP9" gate="G$1" x="66.04" y="91.44" smashed="yes" rot="R90">
<attribute name="NAME" x="63.246" y="92.202" size="1.27" layer="95" rot="R180"/>
</instance>
<instance part="D27" gate="G$1" x="17.78" y="96.52" smashed="yes" rot="R180">
<attribute name="NAME" x="19.304" y="93.98" size="1.27" layer="95"/>
<attribute name="VALUE" x="12.7" y="99.06" size="1.27" layer="96"/>
</instance>
<instance part="R145" gate="G$1" x="33.02" y="96.52" smashed="yes" rot="MR0">
<attribute name="NAME" x="26.924" y="96.0374" size="1.27" layer="95" rot="MR180"/>
<attribute name="VALUE" x="35.56" y="96.012" size="1.27" layer="96" rot="MR180"/>
</instance>
<instance part="D26" gate="G$1" x="17.78" y="66.04" smashed="yes" rot="R180">
<attribute name="NAME" x="19.304" y="63.5" size="1.27" layer="95"/>
<attribute name="VALUE" x="12.7" y="68.58" size="1.27" layer="96"/>
</instance>
<instance part="D25" gate="G$1" x="17.78" y="58.42" smashed="yes" rot="R180">
<attribute name="NAME" x="19.304" y="55.88" size="1.27" layer="95"/>
<attribute name="VALUE" x="12.7" y="60.96" size="1.27" layer="96"/>
</instance>
<instance part="R137" gate="G$1" x="33.02" y="66.04" smashed="yes" rot="MR0">
<attribute name="NAME" x="26.924" y="65.5574" size="1.27" layer="95" rot="MR180"/>
<attribute name="VALUE" x="35.56" y="65.532" size="1.27" layer="96" rot="MR180"/>
</instance>
<instance part="R135" gate="G$1" x="33.02" y="58.42" smashed="yes" rot="MR0">
<attribute name="NAME" x="26.924" y="57.9374" size="1.27" layer="95" rot="MR180"/>
<attribute name="VALUE" x="35.56" y="57.912" size="1.27" layer="96" rot="MR180"/>
</instance>
<instance part="FRAME7" gate="G$1" x="0" y="0" smashed="yes">
<attribute name="DRAWING_NAME" x="344.17" y="15.24" size="2.54" layer="94"/>
<attribute name="LAST_DATE_TIME" x="344.17" y="10.16" size="2.286" layer="94"/>
<attribute name="SHEET" x="357.505" y="5.08" size="2.54" layer="94"/>
</instance>
<instance part="SUPPLY10" gate="GND" x="248.92" y="7.62" smashed="yes">
<attribute name="VALUE" x="254.381" y="8.255" size="1.27" layer="96" rot="R180"/>
</instance>
<instance part="GND" gate="G$1" x="251.46" y="25.4" smashed="yes" rot="R270">
<attribute name="NAME" x="256.54" y="25.4" size="1.27" layer="95"/>
</instance>
<instance part="TP38" gate="G$1" x="251.46" y="22.86" smashed="yes" rot="R270">
<attribute name="NAME" x="256.54" y="22.86" size="1.778" layer="95"/>
</instance>
<instance part="TP39" gate="G$1" x="251.46" y="20.32" smashed="yes" rot="R270">
<attribute name="NAME" x="256.54" y="20.32" size="1.778" layer="95"/>
</instance>
<instance part="TP40" gate="G$1" x="251.46" y="17.78" smashed="yes" rot="R270">
<attribute name="NAME" x="256.54" y="17.78" size="1.778" layer="95"/>
</instance>
<instance part="TP41" gate="G$1" x="251.46" y="15.24" smashed="yes" rot="R270">
<attribute name="NAME" x="256.54" y="15.24" size="1.778" layer="95"/>
</instance>
<instance part="TP42" gate="G$1" x="251.46" y="12.7" smashed="yes" rot="R270">
<attribute name="NAME" x="256.54" y="12.7" size="1.778" layer="95"/>
</instance>
<instance part="J8" gate="-1" x="335.28" y="73.66" smashed="yes">
<attribute name="NAME" x="331.978" y="74.168" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J8" gate="-2" x="335.28" y="71.12" smashed="yes">
<attribute name="NAME" x="331.978" y="71.628" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J8" gate="-3" x="335.28" y="68.58" smashed="yes">
<attribute name="NAME" x="331.978" y="69.088" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J8" gate="-4" x="335.28" y="66.04" smashed="yes">
<attribute name="NAME" x="331.978" y="66.548" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J8" gate="-5" x="335.28" y="63.5" smashed="yes">
<attribute name="NAME" x="331.978" y="64.008" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J8" gate="-6" x="347.98" y="63.5" smashed="yes" rot="R180">
<attribute name="NAME" x="351.282" y="62.992" size="1.27" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="J8" gate="-7" x="347.98" y="66.04" smashed="yes" rot="R180">
<attribute name="NAME" x="351.282" y="65.532" size="1.27" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="J8" gate="-8" x="347.98" y="68.58" smashed="yes" rot="R180">
<attribute name="NAME" x="351.282" y="68.072" size="1.27" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="J8" gate="-9" x="347.98" y="71.12" smashed="yes" rot="R180">
<attribute name="NAME" x="351.282" y="70.612" size="1.27" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="J8" gate="-10" x="347.98" y="73.66" smashed="yes" rot="R180">
<attribute name="NAME" x="351.282" y="73.152" size="1.27" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="NC1" gate="G$1" x="353.314" y="68.58" smashed="yes"/>
<instance part="NC3" gate="G$1" x="353.314" y="71.12" smashed="yes"/>
<instance part="NC2" gate="G$1" x="353.314" y="66.04" smashed="yes"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="SUPPLY12" gate="GND" pin="GND"/>
<wire x1="129.54" y1="142.24" x2="165.1" y2="142.24" width="0.1524" layer="91"/>
<wire x1="165.1" y1="142.24" x2="170.18" y2="142.24" width="0.1524" layer="91"/>
<wire x1="170.18" y1="142.24" x2="170.18" y2="139.7" width="0.1524" layer="91"/>
<wire x1="129.54" y1="137.16" x2="129.54" y2="142.24" width="0.1524" layer="91"/>
<wire x1="165.1" y1="137.16" x2="165.1" y2="142.24" width="0.1524" layer="91"/>
<junction x="165.1" y="142.24"/>
<pinref part="U13" gate="G$1" pin="76"/>
<pinref part="U13" gate="G$1" pin="90"/>
</segment>
<segment>
<wire x1="88.9" y1="96.52" x2="83.82" y2="96.52" width="0.1524" layer="91"/>
<pinref part="SUPPLY11" gate="GND" pin="GND"/>
<wire x1="83.82" y1="96.52" x2="83.82" y2="63.5" width="0.1524" layer="91"/>
<wire x1="83.82" y1="63.5" x2="83.82" y2="40.64" width="0.1524" layer="91"/>
<wire x1="83.82" y1="40.64" x2="83.82" y2="38.1" width="0.1524" layer="91"/>
<wire x1="88.9" y1="63.5" x2="83.82" y2="63.5" width="0.1524" layer="91"/>
<junction x="83.82" y="63.5"/>
<wire x1="116.84" y1="45.72" x2="116.84" y2="40.64" width="0.1524" layer="91"/>
<wire x1="116.84" y1="40.64" x2="83.82" y2="40.64" width="0.1524" layer="91"/>
<junction x="83.82" y="40.64"/>
<pinref part="U13" gate="G$1" pin="11"/>
<pinref part="U13" gate="G$1" pin="24"/>
<pinref part="U13" gate="G$1" pin="31"/>
</segment>
<segment>
<pinref part="C89" gate="G$1" pin="1"/>
<wire x1="220.98" y1="20.32" x2="220.98" y2="22.86" width="0.1524" layer="91"/>
<pinref part="C91" gate="G$1" pin="1"/>
<wire x1="226.06" y1="22.86" x2="226.06" y2="20.32" width="0.1524" layer="91"/>
<wire x1="226.06" y1="20.32" x2="220.98" y2="20.32" width="0.1524" layer="91"/>
<pinref part="SUPPLY14" gate="GND" pin="GND"/>
<wire x1="220.98" y1="15.24" x2="220.98" y2="17.78" width="0.1524" layer="91"/>
<junction x="220.98" y="20.32"/>
<pinref part="C90" gate="G$1" pin="1"/>
<wire x1="220.98" y1="17.78" x2="220.98" y2="20.32" width="0.1524" layer="91"/>
<wire x1="215.9" y1="22.86" x2="215.9" y2="17.78" width="0.1524" layer="91"/>
<wire x1="215.9" y1="17.78" x2="220.98" y2="17.78" width="0.1524" layer="91"/>
<junction x="220.98" y="17.78"/>
</segment>
<segment>
<pinref part="SW1" gate="G$1" pin="1"/>
<wire x1="287.02" y1="58.42" x2="287.02" y2="55.88" width="0.1524" layer="91"/>
<wire x1="287.02" y1="55.88" x2="271.78" y2="55.88" width="0.1524" layer="91"/>
<pinref part="C87" gate="G$1" pin="1"/>
<wire x1="271.78" y1="55.88" x2="271.78" y2="58.42" width="0.1524" layer="91"/>
<pinref part="SUPPLY15" gate="GND" pin="GND"/>
<wire x1="287.02" y1="53.34" x2="287.02" y2="55.88" width="0.1524" layer="91"/>
<junction x="287.02" y="55.88"/>
</segment>
<segment>
<wire x1="307.34" y1="60.96" x2="307.34" y2="58.42" width="0.1524" layer="91"/>
<wire x1="307.34" y1="58.42" x2="307.34" y2="55.88" width="0.1524" layer="91"/>
<wire x1="330.2" y1="68.58" x2="307.34" y2="68.58" width="0.1524" layer="91"/>
<wire x1="307.34" y1="68.58" x2="307.34" y2="60.96" width="0.1524" layer="91"/>
<junction x="307.34" y="60.96"/>
<pinref part="SUPPLY16" gate="GND" pin="GND"/>
<pinref part="C80" gate="G$1" pin="1"/>
<wire x1="309.88" y1="60.96" x2="307.34" y2="60.96" width="0.1524" layer="91"/>
<wire x1="307.34" y1="58.42" x2="302.26" y2="58.42" width="0.1524" layer="91"/>
<wire x1="302.26" y1="58.42" x2="302.26" y2="60.96" width="0.1524" layer="91"/>
<junction x="307.34" y="58.42"/>
<pinref part="R125" gate="G$1" pin="2"/>
<pinref part="J8" gate="-3" pin="1"/>
</segment>
<segment>
<wire x1="180.34" y1="88.9" x2="185.42" y2="88.9" width="0.1524" layer="91"/>
<pinref part="SUPPLY17" gate="GND" pin="GND"/>
<wire x1="185.42" y1="88.9" x2="185.42" y2="40.64" width="0.1524" layer="91"/>
<wire x1="185.42" y1="40.64" x2="185.42" y2="38.1" width="0.1524" layer="91"/>
<wire x1="134.62" y1="45.72" x2="134.62" y2="40.64" width="0.1524" layer="91"/>
<wire x1="134.62" y1="40.64" x2="165.1" y2="40.64" width="0.1524" layer="91"/>
<junction x="185.42" y="40.64"/>
<pinref part="U13" gate="G$1" pin="38"/>
<pinref part="U13" gate="G$1" pin="62"/>
<pinref part="U13" gate="G$1" pin="50"/>
<wire x1="165.1" y1="40.64" x2="185.42" y2="40.64" width="0.1524" layer="91"/>
<wire x1="165.1" y1="45.72" x2="165.1" y2="40.64" width="0.1524" layer="91"/>
<junction x="165.1" y="40.64"/>
</segment>
<segment>
<pinref part="Y1" gate="G$1" pin="3"/>
<pinref part="SUPPLY18" gate="GND" pin="GND"/>
<wire x1="208.28" y1="180.34" x2="208.28" y2="170.18" width="0.1524" layer="91"/>
<pinref part="C88" gate="G$1" pin="1"/>
<wire x1="208.28" y1="170.18" x2="208.28" y2="167.64" width="0.1524" layer="91"/>
<wire x1="208.28" y1="167.64" x2="208.28" y2="165.1" width="0.1524" layer="91"/>
<wire x1="200.66" y1="172.72" x2="200.66" y2="170.18" width="0.1524" layer="91"/>
<wire x1="200.66" y1="170.18" x2="208.28" y2="170.18" width="0.1524" layer="91"/>
<pinref part="C79" gate="G$1" pin="1"/>
<wire x1="215.9" y1="172.72" x2="215.9" y2="167.64" width="0.1524" layer="91"/>
<wire x1="215.9" y1="167.64" x2="208.28" y2="167.64" width="0.1524" layer="91"/>
<junction x="208.28" y="170.18"/>
<junction x="208.28" y="167.64"/>
</segment>
<segment>
<pinref part="C96" gate="G$1" pin="1"/>
<wire x1="304.8" y1="190.5" x2="309.88" y2="190.5" width="0.1524" layer="91"/>
<wire x1="309.88" y1="190.5" x2="309.88" y2="193.04" width="0.1524" layer="91"/>
<pinref part="C97" gate="G$1" pin="1"/>
<wire x1="304.8" y1="190.5" x2="304.8" y2="193.04" width="0.1524" layer="91"/>
<pinref part="C101" gate="G$1" pin="1"/>
<wire x1="320.04" y1="190.5" x2="325.12" y2="190.5" width="0.1524" layer="91"/>
<wire x1="325.12" y1="190.5" x2="325.12" y2="193.04" width="0.1524" layer="91"/>
<pinref part="C100" gate="G$1" pin="1"/>
<wire x1="320.04" y1="190.5" x2="320.04" y2="193.04" width="0.1524" layer="91"/>
<wire x1="309.88" y1="190.5" x2="320.04" y2="190.5" width="0.1524" layer="91"/>
<junction x="309.88" y="190.5"/>
<junction x="320.04" y="190.5"/>
<pinref part="C103" gate="G$1" pin="1"/>
<wire x1="335.28" y1="190.5" x2="340.36" y2="190.5" width="0.1524" layer="91"/>
<wire x1="340.36" y1="190.5" x2="340.36" y2="193.04" width="0.1524" layer="91"/>
<pinref part="C102" gate="G$1" pin="1"/>
<wire x1="335.28" y1="190.5" x2="335.28" y2="193.04" width="0.1524" layer="91"/>
<wire x1="325.12" y1="190.5" x2="335.28" y2="190.5" width="0.1524" layer="91"/>
<junction x="335.28" y="190.5"/>
<junction x="325.12" y="190.5"/>
<pinref part="C81" gate="G$1" pin="1"/>
<wire x1="350.52" y1="190.5" x2="355.6" y2="190.5" width="0.1524" layer="91"/>
<wire x1="355.6" y1="190.5" x2="355.6" y2="193.04" width="0.1524" layer="91"/>
<pinref part="C83" gate="G$1" pin="1"/>
<wire x1="350.52" y1="190.5" x2="350.52" y2="193.04" width="0.1524" layer="91"/>
<wire x1="337.82" y1="190.5" x2="340.36" y2="190.5" width="0.1524" layer="91"/>
<junction x="350.52" y="190.5"/>
<junction x="340.36" y="190.5"/>
<wire x1="340.36" y1="190.5" x2="350.52" y2="190.5" width="0.1524" layer="91"/>
<junction x="304.8" y="190.5"/>
<pinref part="C82" gate="G$1" pin="1"/>
<wire x1="289.56" y1="190.5" x2="294.64" y2="190.5" width="0.1524" layer="91"/>
<wire x1="294.64" y1="190.5" x2="294.64" y2="193.04" width="0.1524" layer="91"/>
<pinref part="C99" gate="G$1" pin="1"/>
<wire x1="289.56" y1="190.5" x2="289.56" y2="193.04" width="0.1524" layer="91"/>
<wire x1="279.4" y1="190.5" x2="289.56" y2="190.5" width="0.1524" layer="91"/>
<junction x="289.56" y="190.5"/>
<pinref part="C98" gate="G$1" pin="1"/>
<wire x1="274.32" y1="190.5" x2="279.4" y2="190.5" width="0.1524" layer="91"/>
<wire x1="279.4" y1="190.5" x2="279.4" y2="193.04" width="0.1524" layer="91"/>
<pinref part="C95" gate="G$1" pin="1"/>
<wire x1="274.32" y1="190.5" x2="274.32" y2="193.04" width="0.1524" layer="91"/>
<wire x1="264.16" y1="190.5" x2="274.32" y2="190.5" width="0.1524" layer="91"/>
<junction x="274.32" y="190.5"/>
<junction x="279.4" y="190.5"/>
<pinref part="C92" gate="G$1" pin="1"/>
<wire x1="259.08" y1="190.5" x2="264.16" y2="190.5" width="0.1524" layer="91"/>
<wire x1="264.16" y1="190.5" x2="264.16" y2="193.04" width="0.1524" layer="91"/>
<pinref part="C84" gate="G$1" pin="1"/>
<wire x1="259.08" y1="190.5" x2="259.08" y2="193.04" width="0.1524" layer="91"/>
<junction x="264.16" y="190.5"/>
<wire x1="294.64" y1="190.5" x2="299.72" y2="190.5" width="0.1524" layer="91"/>
<junction x="294.64" y="190.5"/>
<wire x1="299.72" y1="190.5" x2="304.8" y2="190.5" width="0.1524" layer="91"/>
<wire x1="355.6" y1="190.5" x2="365.76" y2="190.5" width="0.1524" layer="91"/>
<junction x="355.6" y="190.5"/>
<pinref part="C86" gate="G$1" pin="1"/>
<wire x1="365.76" y1="190.5" x2="365.76" y2="193.04" width="0.1524" layer="91"/>
<pinref part="C85" gate="G$1" pin="1"/>
<wire x1="365.76" y1="190.5" x2="370.84" y2="190.5" width="0.1524" layer="91"/>
<wire x1="370.84" y1="190.5" x2="370.84" y2="193.04" width="0.1524" layer="91"/>
<junction x="365.76" y="190.5"/>
<pinref part="SUPPLY13" gate="GND" pin="GND"/>
<wire x1="299.72" y1="187.96" x2="299.72" y2="190.5" width="0.1524" layer="91"/>
<junction x="299.72" y="190.5"/>
<pinref part="TP28" gate="G$1" pin="PP"/>
<wire x1="243.84" y1="190.5" x2="254" y2="190.5" width="0.1524" layer="91"/>
<junction x="259.08" y="190.5"/>
<pinref part="C94" gate="G$1" pin="1"/>
<wire x1="254" y1="190.5" x2="259.08" y2="190.5" width="0.1524" layer="91"/>
<wire x1="254" y1="193.04" x2="254" y2="190.5" width="0.1524" layer="91"/>
<junction x="254" y="190.5"/>
</segment>
<segment>
<pinref part="R142" gate="G$1" pin="2"/>
<wire x1="30.48" y1="114.3" x2="30.48" y2="109.22" width="0.1524" layer="91"/>
<wire x1="30.48" y1="109.22" x2="35.56" y2="109.22" width="0.1524" layer="91"/>
<pinref part="R152" gate="G$1" pin="2"/>
<wire x1="35.56" y1="109.22" x2="40.64" y2="109.22" width="0.1524" layer="91"/>
<wire x1="40.64" y1="109.22" x2="45.72" y2="109.22" width="0.1524" layer="91"/>
<wire x1="45.72" y1="109.22" x2="45.72" y2="114.3" width="0.1524" layer="91"/>
<pinref part="R150" gate="G$1" pin="2"/>
<wire x1="40.64" y1="114.3" x2="40.64" y2="109.22" width="0.1524" layer="91"/>
<junction x="40.64" y="109.22"/>
<pinref part="R143" gate="G$1" pin="2"/>
<wire x1="35.56" y1="114.3" x2="35.56" y2="109.22" width="0.1524" layer="91"/>
<junction x="35.56" y="109.22"/>
<pinref part="SUPPLY19" gate="GND" pin="GND"/>
<wire x1="30.48" y1="106.68" x2="30.48" y2="109.22" width="0.1524" layer="91"/>
<junction x="30.48" y="109.22"/>
</segment>
<segment>
<wire x1="10.16" y1="88.9" x2="10.16" y2="81.28" width="0.1524" layer="91"/>
<wire x1="10.16" y1="81.28" x2="10.16" y2="73.66" width="0.1524" layer="91"/>
<wire x1="10.16" y1="73.66" x2="13.716" y2="73.66" width="0.1524" layer="91"/>
<wire x1="13.716" y1="81.28" x2="10.16" y2="81.28" width="0.1524" layer="91"/>
<junction x="10.16" y="81.28"/>
<wire x1="13.716" y1="88.9" x2="10.16" y2="88.9" width="0.1524" layer="91"/>
<pinref part="SUPPLY20" gate="GND" pin="GND"/>
<wire x1="10.16" y1="50.8" x2="10.16" y2="58.42" width="0.1524" layer="91"/>
<junction x="10.16" y="73.66"/>
<pinref part="D28" gate="G$1" pin="K"/>
<pinref part="D29" gate="G$1" pin="K"/>
<pinref part="D30" gate="G$1" pin="K"/>
<pinref part="D27" gate="G$1" pin="K"/>
<wire x1="10.16" y1="58.42" x2="10.16" y2="66.04" width="0.1524" layer="91"/>
<wire x1="10.16" y1="66.04" x2="10.16" y2="73.66" width="0.1524" layer="91"/>
<wire x1="13.716" y1="96.52" x2="10.16" y2="96.52" width="0.1524" layer="91"/>
<wire x1="10.16" y1="96.52" x2="10.16" y2="88.9" width="0.1524" layer="91"/>
<junction x="10.16" y="88.9"/>
<pinref part="D26" gate="G$1" pin="K"/>
<wire x1="13.716" y1="66.04" x2="10.16" y2="66.04" width="0.1524" layer="91"/>
<junction x="10.16" y="66.04"/>
<pinref part="D25" gate="G$1" pin="K"/>
<wire x1="13.716" y1="58.42" x2="10.16" y2="58.42" width="0.1524" layer="91"/>
<junction x="10.16" y="58.42"/>
</segment>
<segment>
<pinref part="R132" gate="G$1" pin="2"/>
<wire x1="10.16" y1="149.86" x2="10.16" y2="144.78" width="0.1524" layer="91"/>
<wire x1="10.16" y1="144.78" x2="15.24" y2="144.78" width="0.1524" layer="91"/>
<pinref part="R139" gate="G$1" pin="2"/>
<wire x1="15.24" y1="144.78" x2="20.32" y2="144.78" width="0.1524" layer="91"/>
<wire x1="20.32" y1="144.78" x2="25.4" y2="144.78" width="0.1524" layer="91"/>
<wire x1="25.4" y1="144.78" x2="25.4" y2="149.86" width="0.1524" layer="91"/>
<pinref part="R138" gate="G$1" pin="2"/>
<wire x1="20.32" y1="149.86" x2="20.32" y2="144.78" width="0.1524" layer="91"/>
<junction x="20.32" y="144.78"/>
<pinref part="R134" gate="G$1" pin="2"/>
<wire x1="15.24" y1="149.86" x2="15.24" y2="144.78" width="0.1524" layer="91"/>
<junction x="15.24" y="144.78"/>
<pinref part="SUPPLY21" gate="GND" pin="GND"/>
<wire x1="10.16" y1="142.24" x2="10.16" y2="144.78" width="0.1524" layer="91"/>
<junction x="10.16" y="144.78"/>
</segment>
<segment>
<pinref part="U14" gate="G$1" pin="4"/>
<wire x1="297.18" y1="124.46" x2="289.56" y2="124.46" width="0.1524" layer="91"/>
<wire x1="289.56" y1="124.46" x2="289.56" y2="121.92" width="0.1524" layer="91"/>
<pinref part="SUPPLY22" gate="GND" pin="GND"/>
</segment>
<segment>
<wire x1="320.04" y1="157.48" x2="317.5" y2="157.48" width="0.1524" layer="91"/>
<wire x1="317.5" y1="157.48" x2="317.5" y2="152.4" width="0.1524" layer="91"/>
<pinref part="SUPPLY23" gate="GND" pin="GND"/>
<pinref part="C93" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="J9" gate="-4" pin="1"/>
<pinref part="SUPPLY10" gate="GND" pin="GND"/>
<wire x1="246.38" y1="27.94" x2="248.92" y2="27.94" width="0.1524" layer="91"/>
<wire x1="248.92" y1="27.94" x2="248.92" y2="25.4" width="0.1524" layer="91"/>
<pinref part="GND" gate="G$1" pin="PP"/>
<wire x1="248.92" y1="25.4" x2="248.92" y2="22.86" width="0.1524" layer="91"/>
<wire x1="248.92" y1="22.86" x2="248.92" y2="20.32" width="0.1524" layer="91"/>
<wire x1="248.92" y1="20.32" x2="248.92" y2="17.78" width="0.1524" layer="91"/>
<wire x1="248.92" y1="17.78" x2="248.92" y2="15.24" width="0.1524" layer="91"/>
<wire x1="248.92" y1="15.24" x2="248.92" y2="12.7" width="0.1524" layer="91"/>
<wire x1="248.92" y1="12.7" x2="248.92" y2="10.16" width="0.1524" layer="91"/>
<wire x1="251.46" y1="25.4" x2="248.92" y2="25.4" width="0.1524" layer="91"/>
<junction x="248.92" y="25.4"/>
<pinref part="TP38" gate="G$1" pin="PP"/>
<wire x1="251.46" y1="22.86" x2="248.92" y2="22.86" width="0.1524" layer="91"/>
<junction x="248.92" y="22.86"/>
<pinref part="TP39" gate="G$1" pin="PP"/>
<wire x1="251.46" y1="20.32" x2="248.92" y2="20.32" width="0.1524" layer="91"/>
<junction x="248.92" y="20.32"/>
<pinref part="TP40" gate="G$1" pin="PP"/>
<wire x1="251.46" y1="17.78" x2="248.92" y2="17.78" width="0.1524" layer="91"/>
<junction x="248.92" y="17.78"/>
<pinref part="TP41" gate="G$1" pin="PP"/>
<wire x1="251.46" y1="15.24" x2="248.92" y2="15.24" width="0.1524" layer="91"/>
<junction x="248.92" y="15.24"/>
<pinref part="TP42" gate="G$1" pin="PP"/>
<wire x1="251.46" y1="12.7" x2="248.92" y2="12.7" width="0.1524" layer="91"/>
<junction x="248.92" y="12.7"/>
</segment>
</net>
<net name="RSTN" class="0">
<segment>
<pinref part="R119" gate="G$1" pin="1"/>
<wire x1="274.32" y1="71.12" x2="271.78" y2="71.12" width="0.1524" layer="91"/>
<pinref part="C87" gate="G$1" pin="2"/>
<wire x1="271.78" y1="71.12" x2="271.78" y2="68.58" width="0.1524" layer="91"/>
<pinref part="R120" gate="G$1" pin="1"/>
<wire x1="271.78" y1="76.2" x2="271.78" y2="73.66" width="0.1524" layer="91"/>
<junction x="271.78" y="71.12"/>
<wire x1="271.78" y1="73.66" x2="271.78" y2="71.12" width="0.1524" layer="91"/>
<wire x1="271.78" y1="73.66" x2="266.7" y2="73.66" width="0.1524" layer="91"/>
<junction x="271.78" y="73.66"/>
<label x="264.16" y="73.66" size="1.27" layer="95" rot="MR0" xref="yes"/>
<pinref part="TP30" gate="G$1" pin="PP"/>
<wire x1="266.7" y1="73.66" x2="264.16" y2="73.66" width="0.1524" layer="91"/>
<wire x1="266.7" y1="78.74" x2="266.7" y2="73.66" width="0.1524" layer="91"/>
<junction x="266.7" y="73.66"/>
</segment>
<segment>
<pinref part="R121" gate="G$1" pin="1"/>
<wire x1="134.62" y1="157.48" x2="134.62" y2="167.64" width="0.1524" layer="91"/>
<label x="134.62" y="160.02" size="1.27" layer="95" rot="R90"/>
<junction x="134.62" y="167.64"/>
</segment>
<segment>
<pinref part="J8" gate="-10" pin="1"/>
<wire x1="353.06" y1="73.66" x2="358.14" y2="73.66" width="0.1524" layer="91"/>
<label x="358.14" y="73.66" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="VDD_A" class="0">
<segment>
<wire x1="88.9" y1="93.98" x2="81.28" y2="93.98" width="0.1524" layer="91"/>
<pinref part="U13" gate="G$1" pin="12"/>
<pinref part="R153" gate="G$1" pin="2"/>
<wire x1="81.28" y1="93.98" x2="81.28" y2="66.04" width="0.1524" layer="91"/>
<wire x1="81.28" y1="66.04" x2="63.5" y2="66.04" width="0.1524" layer="91"/>
<wire x1="63.5" y1="66.04" x2="63.5" y2="63.5" width="0.1524" layer="91"/>
<wire x1="63.5" y1="63.5" x2="66.04" y2="63.5" width="0.1524" layer="91"/>
<wire x1="60.96" y1="63.5" x2="63.5" y2="63.5" width="0.1524" layer="91"/>
<junction x="63.5" y="63.5"/>
<pinref part="TP33" gate="G$1" pin="PP"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<wire x1="127" y1="137.16" x2="127" y2="139.7" width="0.1524" layer="91"/>
<pinref part="U13" gate="G$1" pin="91"/>
<pinref part="L7" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="SW1" gate="G$1" pin="2"/>
<wire x1="287.02" y1="68.58" x2="287.02" y2="71.12" width="0.1524" layer="91"/>
<pinref part="R119" gate="G$1" pin="2"/>
<wire x1="287.02" y1="71.12" x2="284.48" y2="71.12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<pinref part="R120" gate="G$1" pin="2"/>
<wire x1="271.78" y1="86.36" x2="271.78" y2="88.9" width="0.1524" layer="91"/>
<pinref part="+3V2" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<wire x1="330.2" y1="73.66" x2="302.26" y2="73.66" width="0.1524" layer="91"/>
<wire x1="302.26" y1="73.66" x2="302.26" y2="81.28" width="0.1524" layer="91"/>
<pinref part="+3V6" gate="G$1" pin="+3V3"/>
<wire x1="302.26" y1="81.28" x2="302.26" y2="86.36" width="0.1524" layer="91"/>
<wire x1="302.26" y1="86.36" x2="302.26" y2="88.9" width="0.1524" layer="91"/>
<junction x="302.26" y="73.66"/>
<pinref part="C80" gate="G$1" pin="2"/>
<wire x1="302.26" y1="73.66" x2="302.26" y2="71.12" width="0.1524" layer="91"/>
<pinref part="R127" gate="G$1" pin="2"/>
<wire x1="304.8" y1="86.36" x2="302.26" y2="86.36" width="0.1524" layer="91"/>
<junction x="302.26" y="86.36"/>
<pinref part="R126" gate="G$1" pin="2"/>
<wire x1="304.8" y1="81.28" x2="302.26" y2="81.28" width="0.1524" layer="91"/>
<junction x="302.26" y="81.28"/>
<pinref part="J8" gate="-1" pin="1"/>
</segment>
<segment>
<pinref part="C96" gate="G$1" pin="2"/>
<wire x1="304.8" y1="205.74" x2="309.88" y2="205.74" width="0.1524" layer="91"/>
<wire x1="309.88" y1="205.74" x2="309.88" y2="203.2" width="0.1524" layer="91"/>
<pinref part="C97" gate="G$1" pin="2"/>
<wire x1="304.8" y1="203.2" x2="304.8" y2="205.74" width="0.1524" layer="91"/>
<pinref part="C101" gate="G$1" pin="2"/>
<wire x1="309.88" y1="205.74" x2="320.04" y2="205.74" width="0.1524" layer="91"/>
<wire x1="320.04" y1="205.74" x2="325.12" y2="205.74" width="0.1524" layer="91"/>
<wire x1="325.12" y1="205.74" x2="325.12" y2="203.2" width="0.1524" layer="91"/>
<pinref part="C100" gate="G$1" pin="2"/>
<wire x1="320.04" y1="203.2" x2="320.04" y2="205.74" width="0.1524" layer="91"/>
<junction x="320.04" y="205.74"/>
<junction x="309.88" y="205.74"/>
<pinref part="C103" gate="G$1" pin="2"/>
<wire x1="325.12" y1="205.74" x2="335.28" y2="205.74" width="0.1524" layer="91"/>
<wire x1="335.28" y1="205.74" x2="340.36" y2="205.74" width="0.1524" layer="91"/>
<wire x1="340.36" y1="205.74" x2="340.36" y2="203.2" width="0.1524" layer="91"/>
<pinref part="C102" gate="G$1" pin="2"/>
<wire x1="335.28" y1="203.2" x2="335.28" y2="205.74" width="0.1524" layer="91"/>
<junction x="335.28" y="205.74"/>
<junction x="325.12" y="205.74"/>
<pinref part="C81" gate="G$1" pin="2"/>
<wire x1="337.82" y1="205.74" x2="340.36" y2="205.74" width="0.1524" layer="91"/>
<wire x1="340.36" y1="205.74" x2="350.52" y2="205.74" width="0.1524" layer="91"/>
<wire x1="350.52" y1="205.74" x2="355.6" y2="205.74" width="0.1524" layer="91"/>
<wire x1="355.6" y1="205.74" x2="355.6" y2="203.2" width="0.1524" layer="91"/>
<pinref part="C83" gate="G$1" pin="2"/>
<wire x1="350.52" y1="203.2" x2="350.52" y2="205.74" width="0.1524" layer="91"/>
<junction x="350.52" y="205.74"/>
<junction x="340.36" y="205.74"/>
<pinref part="C82" gate="G$1" pin="2"/>
<wire x1="279.4" y1="205.74" x2="289.56" y2="205.74" width="0.1524" layer="91"/>
<wire x1="289.56" y1="205.74" x2="294.64" y2="205.74" width="0.1524" layer="91"/>
<wire x1="294.64" y1="205.74" x2="294.64" y2="203.2" width="0.1524" layer="91"/>
<pinref part="C99" gate="G$1" pin="2"/>
<wire x1="289.56" y1="203.2" x2="289.56" y2="205.74" width="0.1524" layer="91"/>
<junction x="289.56" y="205.74"/>
<pinref part="C98" gate="G$1" pin="2"/>
<wire x1="264.16" y1="205.74" x2="274.32" y2="205.74" width="0.1524" layer="91"/>
<wire x1="274.32" y1="205.74" x2="279.4" y2="205.74" width="0.1524" layer="91"/>
<wire x1="279.4" y1="205.74" x2="279.4" y2="203.2" width="0.1524" layer="91"/>
<pinref part="C95" gate="G$1" pin="2"/>
<wire x1="274.32" y1="203.2" x2="274.32" y2="205.74" width="0.1524" layer="91"/>
<junction x="274.32" y="205.74"/>
<junction x="279.4" y="205.74"/>
<pinref part="C92" gate="G$1" pin="2"/>
<wire x1="259.08" y1="205.74" x2="264.16" y2="205.74" width="0.1524" layer="91"/>
<wire x1="264.16" y1="205.74" x2="264.16" y2="203.2" width="0.1524" layer="91"/>
<pinref part="C84" gate="G$1" pin="2"/>
<wire x1="259.08" y1="203.2" x2="259.08" y2="205.74" width="0.1524" layer="91"/>
<junction x="264.16" y="205.74"/>
<wire x1="294.64" y1="205.74" x2="304.8" y2="205.74" width="0.1524" layer="91"/>
<junction x="294.64" y="205.74"/>
<junction x="304.8" y="205.74"/>
<junction x="259.08" y="205.74"/>
<pinref part="TP32" gate="G$1" pin="PP"/>
<wire x1="243.84" y1="205.74" x2="246.38" y2="205.74" width="0.1524" layer="91"/>
<wire x1="246.38" y1="205.74" x2="254" y2="205.74" width="0.1524" layer="91"/>
<wire x1="254" y1="205.74" x2="259.08" y2="205.74" width="0.1524" layer="91"/>
<wire x1="355.6" y1="205.74" x2="365.76" y2="205.74" width="0.1524" layer="91"/>
<junction x="355.6" y="205.74"/>
<pinref part="C86" gate="G$1" pin="2"/>
<wire x1="365.76" y1="203.2" x2="365.76" y2="205.74" width="0.1524" layer="91"/>
<pinref part="C85" gate="G$1" pin="2"/>
<wire x1="365.76" y1="205.74" x2="370.84" y2="205.74" width="0.1524" layer="91"/>
<wire x1="370.84" y1="205.74" x2="370.84" y2="203.2" width="0.1524" layer="91"/>
<junction x="365.76" y="205.74"/>
<pinref part="+3V8" gate="G$1" pin="+3V3"/>
<wire x1="246.38" y1="213.36" x2="246.38" y2="205.74" width="0.1524" layer="91"/>
<junction x="246.38" y="205.74"/>
<pinref part="C94" gate="G$1" pin="2"/>
<wire x1="254" y1="203.2" x2="254" y2="205.74" width="0.1524" layer="91"/>
<junction x="254" y="205.74"/>
</segment>
<segment>
<wire x1="180.34" y1="91.44" x2="210.82" y2="91.44" width="0.1524" layer="91"/>
<label x="210.82" y="91.44" size="1.27" layer="95" xref="yes"/>
<pinref part="U13" gate="G$1" pin="63"/>
</segment>
<segment>
<pinref part="U13" gate="G$1" pin="51"/>
<wire x1="180.34" y1="60.96" x2="210.82" y2="60.96" width="0.1524" layer="91"/>
<label x="210.82" y="60.96" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="137.16" y1="45.72" x2="137.16" y2="30.48" width="0.1524" layer="91"/>
<label x="137.16" y="30.48" size="1.27" layer="95" rot="R270" xref="yes"/>
<pinref part="U13" gate="G$1" pin="39"/>
</segment>
<segment>
<wire x1="114.3" y1="45.72" x2="114.3" y2="30.48" width="0.1524" layer="91"/>
<label x="114.3" y="30.48" size="1.27" layer="95" rot="R270" xref="yes"/>
<pinref part="U13" gate="G$1" pin="30"/>
</segment>
<segment>
<wire x1="88.9" y1="60.96" x2="78.74" y2="60.96" width="0.1524" layer="91"/>
<label x="60.96" y="60.96" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="U13" gate="G$1" pin="25"/>
<pinref part="R153" gate="G$1" pin="1"/>
<wire x1="78.74" y1="60.96" x2="60.96" y2="60.96" width="0.1524" layer="91"/>
<wire x1="76.2" y1="63.5" x2="78.74" y2="63.5" width="0.1524" layer="91"/>
<wire x1="78.74" y1="63.5" x2="78.74" y2="60.96" width="0.1524" layer="91"/>
<junction x="78.74" y="60.96"/>
</segment>
<segment>
<wire x1="124.46" y1="137.16" x2="124.46" y2="167.64" width="0.1524" layer="91"/>
<label x="124.46" y="167.64" size="1.27" layer="95" rot="R90" xref="yes"/>
<pinref part="U13" gate="G$1" pin="92"/>
</segment>
<segment>
<wire x1="162.56" y1="137.16" x2="162.56" y2="167.64" width="0.1524" layer="91"/>
<label x="162.56" y="167.64" size="1.27" layer="95" rot="R90" xref="yes"/>
<pinref part="U13" gate="G$1" pin="77"/>
</segment>
<segment>
<pinref part="R141" gate="G$1" pin="1"/>
<wire x1="30.48" y1="152.4" x2="30.48" y2="154.94" width="0.1524" layer="91"/>
<wire x1="30.48" y1="154.94" x2="35.56" y2="154.94" width="0.1524" layer="91"/>
<pinref part="R151" gate="G$1" pin="1"/>
<wire x1="35.56" y1="154.94" x2="40.64" y2="154.94" width="0.1524" layer="91"/>
<wire x1="40.64" y1="154.94" x2="45.72" y2="154.94" width="0.1524" layer="91"/>
<wire x1="45.72" y1="154.94" x2="45.72" y2="152.4" width="0.1524" layer="91"/>
<pinref part="R144" gate="G$1" pin="1"/>
<wire x1="35.56" y1="152.4" x2="35.56" y2="154.94" width="0.1524" layer="91"/>
<junction x="35.56" y="154.94"/>
<pinref part="R149" gate="G$1" pin="1"/>
<wire x1="40.64" y1="152.4" x2="40.64" y2="154.94" width="0.1524" layer="91"/>
<junction x="40.64" y="154.94"/>
<wire x1="45.72" y1="154.94" x2="45.72" y2="157.48" width="0.1524" layer="91"/>
<junction x="45.72" y="154.94"/>
<pinref part="+3V10" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="R133" gate="G$1" pin="1"/>
<wire x1="10.16" y1="187.96" x2="10.16" y2="190.5" width="0.1524" layer="91"/>
<wire x1="10.16" y1="190.5" x2="15.24" y2="190.5" width="0.1524" layer="91"/>
<pinref part="R140" gate="G$1" pin="1"/>
<wire x1="15.24" y1="190.5" x2="20.32" y2="190.5" width="0.1524" layer="91"/>
<wire x1="20.32" y1="190.5" x2="25.4" y2="190.5" width="0.1524" layer="91"/>
<wire x1="25.4" y1="190.5" x2="25.4" y2="187.96" width="0.1524" layer="91"/>
<pinref part="R131" gate="G$1" pin="1"/>
<wire x1="15.24" y1="187.96" x2="15.24" y2="190.5" width="0.1524" layer="91"/>
<junction x="15.24" y="190.5"/>
<pinref part="R136" gate="G$1" pin="1"/>
<wire x1="20.32" y1="187.96" x2="20.32" y2="190.5" width="0.1524" layer="91"/>
<junction x="20.32" y="190.5"/>
<wire x1="25.4" y1="190.5" x2="25.4" y2="193.04" width="0.1524" layer="91"/>
<junction x="25.4" y="190.5"/>
<pinref part="+3V11" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="R130" gate="G$1" pin="2"/>
<wire x1="279.4" y1="154.94" x2="279.4" y2="162.56" width="0.1524" layer="91"/>
<pinref part="R124" gate="G$1" pin="2"/>
<wire x1="279.4" y1="162.56" x2="284.48" y2="162.56" width="0.1524" layer="91"/>
<wire x1="284.48" y1="162.56" x2="289.56" y2="162.56" width="0.1524" layer="91"/>
<wire x1="289.56" y1="162.56" x2="335.28" y2="162.56" width="0.1524" layer="91"/>
<wire x1="335.28" y1="162.56" x2="340.36" y2="162.56" width="0.1524" layer="91"/>
<wire x1="340.36" y1="162.56" x2="345.44" y2="162.56" width="0.1524" layer="91"/>
<wire x1="345.44" y1="162.56" x2="350.52" y2="162.56" width="0.1524" layer="91"/>
<wire x1="350.52" y1="162.56" x2="350.52" y2="154.94" width="0.1524" layer="91"/>
<pinref part="R123" gate="G$1" pin="2"/>
<wire x1="345.44" y1="154.94" x2="345.44" y2="162.56" width="0.1524" layer="91"/>
<junction x="345.44" y="162.56"/>
<pinref part="R122" gate="G$1" pin="2"/>
<wire x1="340.36" y1="154.94" x2="340.36" y2="162.56" width="0.1524" layer="91"/>
<junction x="340.36" y="162.56"/>
<pinref part="R128" gate="G$1" pin="2"/>
<wire x1="289.56" y1="154.94" x2="289.56" y2="162.56" width="0.1524" layer="91"/>
<junction x="289.56" y="162.56"/>
<pinref part="R129" gate="G$1" pin="2"/>
<wire x1="284.48" y1="154.94" x2="284.48" y2="162.56" width="0.1524" layer="91"/>
<junction x="284.48" y="162.56"/>
<pinref part="U14" gate="G$1" pin="8"/>
<wire x1="314.96" y1="132.08" x2="335.28" y2="132.08" width="0.1524" layer="91"/>
<wire x1="335.28" y1="132.08" x2="335.28" y2="157.48" width="0.1524" layer="91"/>
<junction x="335.28" y="162.56"/>
<pinref part="C93" gate="G$1" pin="2"/>
<wire x1="335.28" y1="157.48" x2="335.28" y2="162.56" width="0.1524" layer="91"/>
<wire x1="330.2" y1="157.48" x2="335.28" y2="157.48" width="0.1524" layer="91"/>
<junction x="335.28" y="157.48"/>
<wire x1="350.52" y1="162.56" x2="363.22" y2="162.56" width="0.1524" layer="91"/>
<junction x="350.52" y="162.56"/>
<pinref part="+3V12" gate="G$1" pin="+3V3"/>
<wire x1="363.22" y1="162.56" x2="363.22" y2="165.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SWCLK" class="0">
<segment>
<wire x1="121.92" y1="137.16" x2="121.92" y2="167.64" width="0.1524" layer="91"/>
<pinref part="U13" gate="G$1" pin="93"/>
<label x="121.92" y="157.48" size="1.27" layer="95" rot="R90"/>
<junction x="121.92" y="167.64"/>
</segment>
<segment>
<pinref part="R126" gate="G$1" pin="1"/>
<wire x1="314.96" y1="81.28" x2="325.12" y2="81.28" width="0.1524" layer="91"/>
<wire x1="325.12" y1="81.28" x2="325.12" y2="66.04" width="0.1524" layer="91"/>
<pinref part="J8" gate="-4" pin="1"/>
<wire x1="325.12" y1="66.04" x2="330.2" y2="66.04" width="0.1524" layer="91"/>
<wire x1="325.12" y1="66.04" x2="322.58" y2="66.04" width="0.1524" layer="91"/>
<junction x="325.12" y="66.04"/>
<label x="322.58" y="66.04" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SWO" class="0">
<segment>
<wire x1="116.84" y1="137.16" x2="116.84" y2="167.64" width="0.1524" layer="91"/>
<pinref part="U13" gate="G$1" pin="95"/>
<label x="116.84" y="157.48" size="1.27" layer="95" rot="R90"/>
<junction x="116.84" y="167.64"/>
</segment>
<segment>
<pinref part="J8" gate="-6" pin="1"/>
<wire x1="353.06" y1="63.5" x2="358.14" y2="63.5" width="0.1524" layer="91"/>
<label x="358.14" y="63.5" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="SWDIO" class="0">
<segment>
<pinref part="R127" gate="G$1" pin="1"/>
<wire x1="314.96" y1="86.36" x2="327.66" y2="86.36" width="0.1524" layer="91"/>
<pinref part="J8" gate="-2" pin="1"/>
<wire x1="330.2" y1="71.12" x2="327.66" y2="71.12" width="0.1524" layer="91"/>
<wire x1="327.66" y1="71.12" x2="327.66" y2="86.36" width="0.1524" layer="91"/>
<wire x1="327.66" y1="71.12" x2="322.58" y2="71.12" width="0.1524" layer="91"/>
<junction x="327.66" y="71.12"/>
<label x="322.58" y="71.12" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="119.38" y1="137.16" x2="119.38" y2="167.64" width="0.1524" layer="91"/>
<pinref part="U13" gate="G$1" pin="94"/>
<label x="119.38" y="157.48" size="1.27" layer="95" rot="R90"/>
<junction x="119.38" y="167.64"/>
</segment>
</net>
<net name="COM4_MISO" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="2"/>
<pinref part="R129" gate="G$1" pin="1"/>
<wire x1="297.18" y1="129.54" x2="284.48" y2="129.54" width="0.1524" layer="91"/>
<wire x1="284.48" y1="129.54" x2="284.48" y2="144.78" width="0.1524" layer="91"/>
<wire x1="284.48" y1="129.54" x2="274.32" y2="129.54" width="0.1524" layer="91"/>
<junction x="284.48" y="129.54"/>
<label x="274.32" y="129.54" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U13" gate="G$1" pin="37"/>
<wire x1="132.08" y1="45.72" x2="132.08" y2="30.48" width="0.1524" layer="91"/>
<label x="132.08" y="30.48" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
</net>
<net name="COM4_SCK" class="0">
<segment>
<pinref part="U13" gate="G$1" pin="35"/>
<wire x1="127" y1="45.72" x2="127" y2="30.48" width="0.1524" layer="91"/>
<label x="127" y="30.48" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="U14" gate="G$1" pin="6"/>
<pinref part="R123" gate="G$1" pin="1"/>
<wire x1="314.96" y1="127" x2="345.44" y2="127" width="0.1524" layer="91"/>
<wire x1="345.44" y1="127" x2="345.44" y2="144.78" width="0.1524" layer="91"/>
<wire x1="345.44" y1="127" x2="355.6" y2="127" width="0.1524" layer="91"/>
<junction x="345.44" y="127"/>
<label x="355.6" y="127" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="COM4_CSN" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="1"/>
<pinref part="R128" gate="G$1" pin="1"/>
<wire x1="297.18" y1="132.08" x2="289.56" y2="132.08" width="0.1524" layer="91"/>
<wire x1="289.56" y1="132.08" x2="289.56" y2="144.78" width="0.1524" layer="91"/>
<wire x1="289.56" y1="132.08" x2="274.32" y2="132.08" width="0.1524" layer="91"/>
<junction x="289.56" y="132.08"/>
<label x="274.32" y="132.08" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<label x="129.54" y="30.48" size="1.27" layer="95" rot="R270" xref="yes"/>
<pinref part="U13" gate="G$1" pin="36"/>
<wire x1="129.54" y1="45.72" x2="129.54" y2="30.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="COM5_TXD_OUT" class="0">
<segment>
<pinref part="U13" gate="G$1" pin="64"/>
<wire x1="180.34" y1="93.98" x2="210.82" y2="93.98" width="0.1524" layer="91"/>
<label x="210.82" y="93.98" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="COM5_RXD_IN" class="0">
<segment>
<pinref part="U13" gate="G$1" pin="65"/>
<wire x1="180.34" y1="96.52" x2="210.82" y2="96.52" width="0.1524" layer="91"/>
<label x="210.82" y="96.52" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="XOUT1" class="0">
<segment>
<pinref part="Y1" gate="G$1" pin="1"/>
<wire x1="203.2" y1="185.42" x2="200.66" y2="185.42" width="0.1524" layer="91"/>
<pinref part="C88" gate="G$1" pin="2"/>
<wire x1="200.66" y1="185.42" x2="195.58" y2="185.42" width="0.1524" layer="91"/>
<wire x1="200.66" y1="182.88" x2="200.66" y2="185.42" width="0.1524" layer="91"/>
<junction x="200.66" y="185.42"/>
<label x="195.58" y="185.42" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U13" gate="G$1" pin="79"/>
<wire x1="157.48" y1="137.16" x2="157.48" y2="167.64" width="0.1524" layer="91"/>
<label x="157.48" y="157.48" size="1.27" layer="95" rot="R90"/>
<junction x="157.48" y="167.64"/>
</segment>
</net>
<net name="XIN1" class="0">
<segment>
<pinref part="Y1" gate="G$1" pin="2"/>
<wire x1="213.36" y1="185.42" x2="215.9" y2="185.42" width="0.1524" layer="91"/>
<pinref part="C79" gate="G$1" pin="2"/>
<wire x1="215.9" y1="185.42" x2="220.98" y2="185.42" width="0.1524" layer="91"/>
<wire x1="215.9" y1="182.88" x2="215.9" y2="185.42" width="0.1524" layer="91"/>
<junction x="215.9" y="185.42"/>
<label x="220.98" y="185.42" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U13" gate="G$1" pin="78"/>
<wire x1="160.02" y1="137.16" x2="160.02" y2="167.64" width="0.1524" layer="91"/>
<label x="160.02" y="157.48" size="1.27" layer="95" rot="R90"/>
<junction x="160.02" y="167.64"/>
</segment>
</net>
<net name="COM5_EN" class="0">
<segment>
<pinref part="U13" gate="G$1" pin="66"/>
<wire x1="180.34" y1="99.06" x2="210.82" y2="99.06" width="0.1524" layer="91"/>
<label x="210.82" y="99.06" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="COM4_MOSI" class="0">
<segment>
<label x="124.46" y="30.48" size="1.27" layer="95" rot="R270" xref="yes"/>
<pinref part="U13" gate="G$1" pin="34"/>
<wire x1="124.46" y1="45.72" x2="124.46" y2="30.48" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U14" gate="G$1" pin="5"/>
<pinref part="R124" gate="G$1" pin="1"/>
<wire x1="314.96" y1="124.46" x2="350.52" y2="124.46" width="0.1524" layer="91"/>
<wire x1="350.52" y1="124.46" x2="350.52" y2="144.78" width="0.1524" layer="91"/>
<wire x1="350.52" y1="124.46" x2="355.6" y2="124.46" width="0.1524" layer="91"/>
<junction x="350.52" y="124.46"/>
<label x="355.6" y="124.46" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<wire x1="330.2" y1="63.5" x2="325.12" y2="63.5" width="0.1524" layer="91"/>
<wire x1="325.12" y1="63.5" x2="325.12" y2="60.96" width="0.1524" layer="91"/>
<wire x1="325.12" y1="60.96" x2="320.04" y2="60.96" width="0.1524" layer="91"/>
<pinref part="R125" gate="G$1" pin="1"/>
<pinref part="J8" gate="-5" pin="1"/>
</segment>
</net>
<net name="CTRL2" class="0">
<segment>
<pinref part="U13" gate="G$1" pin="86"/>
<wire x1="139.7" y1="137.16" x2="139.7" y2="167.64" width="0.1524" layer="91"/>
<label x="139.7" y="167.64" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="CTRL3" class="0">
<segment>
<label x="137.16" y="167.64" size="1.27" layer="95" rot="R90" xref="yes"/>
<pinref part="U13" gate="G$1" pin="87"/>
<wire x1="137.16" y1="137.16" x2="137.16" y2="167.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SPEED_4" class="0">
<segment>
<pinref part="U13" gate="G$1" pin="20"/>
<wire x1="88.9" y1="73.66" x2="66.04" y2="73.66" width="0.1524" layer="91"/>
<pinref part="TP17" gate="G$1" pin="PP"/>
<label x="68.58" y="73.66" size="1.27" layer="95"/>
</segment>
</net>
<net name="SPEED_3" class="0">
<segment>
<pinref part="U13" gate="G$1" pin="19"/>
<wire x1="88.9" y1="76.2" x2="66.04" y2="76.2" width="0.1524" layer="91"/>
<pinref part="TP18" gate="G$1" pin="PP"/>
<label x="68.58" y="76.2" size="1.27" layer="95"/>
</segment>
</net>
<net name="COM6_RXD_IN" class="0">
<segment>
<pinref part="U13" gate="G$1" pin="57"/>
<wire x1="180.34" y1="76.2" x2="210.82" y2="76.2" width="0.1524" layer="91"/>
<label x="210.82" y="76.2" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="COM6_EN" class="0">
<segment>
<pinref part="U13" gate="G$1" pin="58"/>
<wire x1="180.34" y1="78.74" x2="210.82" y2="78.74" width="0.1524" layer="91"/>
<label x="210.82" y="78.74" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="U13" gate="G$1" pin="88"/>
<pinref part="R121" gate="G$1" pin="2"/>
<wire x1="134.62" y1="137.16" x2="134.62" y2="147.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="COM6_TXD_OUT" class="0">
<segment>
<pinref part="U13" gate="G$1" pin="56"/>
<wire x1="180.34" y1="73.66" x2="210.82" y2="73.66" width="0.1524" layer="91"/>
<label x="210.82" y="73.66" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="COM1_EN" class="0">
<segment>
<pinref part="U13" gate="G$1" pin="54"/>
<wire x1="180.34" y1="68.58" x2="210.82" y2="68.58" width="0.1524" layer="91"/>
<label x="210.82" y="68.58" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="COM1_RXD_IN" class="0">
<segment>
<pinref part="U13" gate="G$1" pin="53"/>
<wire x1="180.34" y1="66.04" x2="210.82" y2="66.04" width="0.1524" layer="91"/>
<label x="210.82" y="66.04" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="COM1_TXD_OUT" class="0">
<segment>
<pinref part="U13" gate="G$1" pin="52"/>
<wire x1="180.34" y1="63.5" x2="210.82" y2="63.5" width="0.1524" layer="91"/>
<label x="210.82" y="63.5" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="!MEM_WP" class="0">
<segment>
<pinref part="U13" gate="G$1" pin="32"/>
<wire x1="119.38" y1="45.72" x2="119.38" y2="30.48" width="0.1524" layer="91"/>
<label x="119.38" y="30.48" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="U14" gate="G$1" pin="3"/>
<wire x1="297.18" y1="127" x2="279.4" y2="127" width="0.1524" layer="91"/>
<pinref part="R130" gate="G$1" pin="1"/>
<wire x1="279.4" y1="127" x2="279.4" y2="144.78" width="0.1524" layer="91"/>
<wire x1="279.4" y1="127" x2="274.32" y2="127" width="0.1524" layer="91"/>
<junction x="279.4" y="127"/>
<label x="274.32" y="127" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="!MEM_HOLD" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="7"/>
<pinref part="R122" gate="G$1" pin="1"/>
<wire x1="314.96" y1="129.54" x2="340.36" y2="129.54" width="0.1524" layer="91"/>
<wire x1="340.36" y1="129.54" x2="340.36" y2="144.78" width="0.1524" layer="91"/>
<wire x1="340.36" y1="129.54" x2="355.6" y2="129.54" width="0.1524" layer="91"/>
<junction x="340.36" y="129.54"/>
<label x="355.6" y="129.54" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<label x="121.92" y="30.48" size="1.27" layer="95" rot="R270" xref="yes"/>
<pinref part="U13" gate="G$1" pin="33"/>
<wire x1="121.92" y1="45.72" x2="121.92" y2="30.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="R141" gate="G$1" pin="2"/>
<pinref part="R142" gate="G$1" pin="1"/>
<wire x1="30.48" y1="142.24" x2="30.48" y2="137.16" width="0.1524" layer="91"/>
<wire x1="30.48" y1="137.16" x2="30.48" y2="124.46" width="0.1524" layer="91"/>
<wire x1="76.2" y1="137.16" x2="30.48" y2="137.16" width="0.1524" layer="91"/>
<junction x="30.48" y="137.16"/>
<pinref part="U13" gate="G$1" pin="1"/>
<wire x1="88.9" y1="121.92" x2="76.2" y2="121.92" width="0.1524" layer="91"/>
<wire x1="76.2" y1="121.92" x2="76.2" y2="137.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="R144" gate="G$1" pin="2"/>
<pinref part="R143" gate="G$1" pin="1"/>
<wire x1="35.56" y1="142.24" x2="35.56" y2="134.62" width="0.1524" layer="91"/>
<wire x1="35.56" y1="134.62" x2="35.56" y2="124.46" width="0.1524" layer="91"/>
<wire x1="73.66" y1="134.62" x2="35.56" y2="134.62" width="0.1524" layer="91"/>
<junction x="35.56" y="134.62"/>
<pinref part="U13" gate="G$1" pin="2"/>
<wire x1="88.9" y1="119.38" x2="73.66" y2="119.38" width="0.1524" layer="91"/>
<wire x1="73.66" y1="119.38" x2="73.66" y2="134.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="R146" gate="G$1" pin="1"/>
<wire x1="38.1" y1="88.9" x2="53.34" y2="88.9" width="0.1524" layer="91"/>
<wire x1="53.34" y1="88.9" x2="53.34" y2="114.3" width="0.1524" layer="91"/>
<pinref part="U13" gate="G$1" pin="4"/>
<wire x1="88.9" y1="114.3" x2="53.34" y2="114.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="R147" gate="G$1" pin="1"/>
<wire x1="38.1" y1="81.28" x2="55.88" y2="81.28" width="0.1524" layer="91"/>
<wire x1="55.88" y1="81.28" x2="55.88" y2="111.76" width="0.1524" layer="91"/>
<pinref part="U13" gate="G$1" pin="5"/>
<wire x1="55.88" y1="111.76" x2="88.9" y2="111.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="R146" gate="G$1" pin="2"/>
<wire x1="27.94" y1="88.9" x2="21.336" y2="88.9" width="0.1524" layer="91"/>
<pinref part="D28" gate="G$1" pin="A"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="R147" gate="G$1" pin="2"/>
<wire x1="27.94" y1="81.28" x2="21.336" y2="81.28" width="0.1524" layer="91"/>
<pinref part="D29" gate="G$1" pin="A"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="R148" gate="G$1" pin="2"/>
<wire x1="27.94" y1="73.66" x2="21.336" y2="73.66" width="0.1524" layer="91"/>
<pinref part="D30" gate="G$1" pin="A"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<pinref part="R133" gate="G$1" pin="2"/>
<pinref part="R132" gate="G$1" pin="1"/>
<wire x1="10.16" y1="177.8" x2="10.16" y2="172.72" width="0.1524" layer="91"/>
<wire x1="10.16" y1="172.72" x2="10.16" y2="160.02" width="0.1524" layer="91"/>
<wire x1="111.76" y1="172.72" x2="10.16" y2="172.72" width="0.1524" layer="91"/>
<junction x="10.16" y="172.72"/>
<pinref part="U13" gate="G$1" pin="97"/>
<wire x1="111.76" y1="137.16" x2="111.76" y2="172.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<pinref part="R131" gate="G$1" pin="2"/>
<pinref part="R134" gate="G$1" pin="1"/>
<wire x1="15.24" y1="177.8" x2="15.24" y2="170.18" width="0.1524" layer="91"/>
<wire x1="15.24" y1="170.18" x2="15.24" y2="160.02" width="0.1524" layer="91"/>
<junction x="15.24" y="170.18"/>
<pinref part="U13" gate="G$1" pin="98"/>
<wire x1="109.22" y1="137.16" x2="109.22" y2="170.18" width="0.1524" layer="91"/>
<wire x1="109.22" y1="170.18" x2="15.24" y2="170.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<pinref part="R136" gate="G$1" pin="2"/>
<pinref part="R138" gate="G$1" pin="1"/>
<wire x1="20.32" y1="177.8" x2="20.32" y2="167.64" width="0.1524" layer="91"/>
<wire x1="20.32" y1="167.64" x2="20.32" y2="160.02" width="0.1524" layer="91"/>
<junction x="20.32" y="167.64"/>
<pinref part="U13" gate="G$1" pin="99"/>
<wire x1="106.68" y1="137.16" x2="106.68" y2="167.64" width="0.1524" layer="91"/>
<wire x1="106.68" y1="167.64" x2="20.32" y2="167.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<pinref part="R140" gate="G$1" pin="2"/>
<pinref part="R139" gate="G$1" pin="1"/>
<wire x1="25.4" y1="177.8" x2="25.4" y2="165.1" width="0.1524" layer="91"/>
<wire x1="25.4" y1="165.1" x2="25.4" y2="160.02" width="0.1524" layer="91"/>
<junction x="25.4" y="165.1"/>
<pinref part="U13" gate="G$1" pin="100"/>
<wire x1="104.14" y1="137.16" x2="104.14" y2="165.1" width="0.1524" layer="91"/>
<wire x1="104.14" y1="165.1" x2="25.4" y2="165.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VDDCORE" class="0">
<segment>
<pinref part="U13" gate="G$1" pin="89"/>
<wire x1="132.08" y1="137.16" x2="132.08" y2="152.4" width="0.1524" layer="91"/>
<wire x1="132.08" y1="152.4" x2="127" y2="152.4" width="0.1524" layer="91"/>
<wire x1="127" y1="152.4" x2="127" y2="149.86" width="0.1524" layer="91"/>
<wire x1="132.08" y1="152.4" x2="132.08" y2="167.64" width="0.1524" layer="91"/>
<label x="132.08" y="167.64" size="1.27" layer="95" rot="R90" xref="yes"/>
<junction x="132.08" y="152.4"/>
<pinref part="L7" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="TP31" gate="G$1" pin="PP"/>
<pinref part="C91" gate="G$1" pin="2"/>
<wire x1="210.82" y1="35.56" x2="215.9" y2="35.56" width="0.1524" layer="91"/>
<wire x1="215.9" y1="35.56" x2="220.98" y2="35.56" width="0.1524" layer="91"/>
<wire x1="220.98" y1="35.56" x2="226.06" y2="35.56" width="0.1524" layer="91"/>
<wire x1="226.06" y1="35.56" x2="226.06" y2="33.02" width="0.1524" layer="91"/>
<pinref part="C89" gate="G$1" pin="2"/>
<wire x1="220.98" y1="33.02" x2="220.98" y2="35.56" width="0.1524" layer="91"/>
<pinref part="C90" gate="G$1" pin="2"/>
<wire x1="215.9" y1="33.02" x2="215.9" y2="35.56" width="0.1524" layer="91"/>
<junction x="215.9" y="35.56"/>
<junction x="220.98" y="35.56"/>
<wire x1="226.06" y1="35.56" x2="226.06" y2="38.1" width="0.1524" layer="91"/>
<wire x1="226.06" y1="38.1" x2="210.82" y2="38.1" width="0.1524" layer="91"/>
<label x="210.82" y="38.1" size="1.27" layer="95" rot="R180" xref="yes"/>
<junction x="226.06" y="35.56"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<pinref part="R152" gate="G$1" pin="1"/>
<pinref part="R151" gate="G$1" pin="2"/>
<wire x1="45.72" y1="142.24" x2="45.72" y2="129.54" width="0.1524" layer="91"/>
<wire x1="45.72" y1="129.54" x2="45.72" y2="124.46" width="0.1524" layer="91"/>
<wire x1="66.04" y1="104.14" x2="66.04" y2="129.54" width="0.1524" layer="91"/>
<wire x1="66.04" y1="129.54" x2="45.72" y2="129.54" width="0.1524" layer="91"/>
<junction x="45.72" y="129.54"/>
<pinref part="U13" gate="G$1" pin="8"/>
<wire x1="88.9" y1="104.14" x2="66.04" y2="104.14" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<pinref part="R150" gate="G$1" pin="1"/>
<pinref part="R149" gate="G$1" pin="2"/>
<wire x1="40.64" y1="142.24" x2="40.64" y2="132.08" width="0.1524" layer="91"/>
<wire x1="40.64" y1="132.08" x2="40.64" y2="124.46" width="0.1524" layer="91"/>
<wire x1="68.58" y1="106.68" x2="68.58" y2="132.08" width="0.1524" layer="91"/>
<wire x1="68.58" y1="132.08" x2="40.64" y2="132.08" width="0.1524" layer="91"/>
<junction x="40.64" y="132.08"/>
<pinref part="U13" gate="G$1" pin="7"/>
<wire x1="88.9" y1="106.68" x2="68.58" y2="106.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SDA_A" class="0">
<segment>
<pinref part="U13" gate="G$1" pin="26"/>
<wire x1="104.14" y1="45.72" x2="104.14" y2="30.48" width="0.1524" layer="91"/>
<label x="104.14" y="30.48" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
</net>
<net name="SCL_A" class="0">
<segment>
<pinref part="U13" gate="G$1" pin="27"/>
<wire x1="106.68" y1="45.72" x2="106.68" y2="30.48" width="0.1524" layer="91"/>
<label x="106.68" y="30.48" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
</net>
<net name="COM7_TXD_OUT" class="0">
<segment>
<pinref part="U13" gate="G$1" pin="42"/>
<wire x1="144.78" y1="45.72" x2="144.78" y2="30.48" width="0.1524" layer="91"/>
<label x="144.78" y="30.48" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="J9" gate="-1" pin="1"/>
<wire x1="246.38" y1="35.56" x2="269.24" y2="35.56" width="0.1524" layer="91"/>
<junction x="269.24" y="35.56"/>
<label x="246.38" y="35.56" size="1.27" layer="95"/>
</segment>
</net>
<net name="COM7_RXD_IN" class="0">
<segment>
<pinref part="U13" gate="G$1" pin="43"/>
<wire x1="147.32" y1="45.72" x2="147.32" y2="30.48" width="0.1524" layer="91"/>
<label x="147.32" y="30.48" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="J9" gate="-2" pin="1"/>
<wire x1="246.38" y1="33.02" x2="269.24" y2="33.02" width="0.1524" layer="91"/>
<junction x="269.24" y="33.02"/>
<label x="246.38" y="33.02" size="1.27" layer="95"/>
</segment>
</net>
<net name="COM7_EN" class="0">
<segment>
<pinref part="U13" gate="G$1" pin="44"/>
<wire x1="149.86" y1="45.72" x2="149.86" y2="30.48" width="0.1524" layer="91"/>
<label x="149.86" y="30.48" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="J9" gate="-3" pin="1"/>
<wire x1="246.38" y1="30.48" x2="269.24" y2="30.48" width="0.1524" layer="91"/>
<junction x="269.24" y="30.48"/>
<label x="246.38" y="30.48" size="1.27" layer="95"/>
</segment>
</net>
<net name="COM_7" class="0">
<segment>
<pinref part="U13" gate="G$1" pin="45"/>
<wire x1="152.4" y1="45.72" x2="152.4" y2="30.48" width="0.1524" layer="91"/>
<pinref part="TP29" gate="G$1" pin="PP"/>
<label x="152.4" y="33.02" size="1.27" layer="95" rot="R90"/>
</segment>
</net>
<net name="HW1" class="0">
<segment>
<pinref part="U13" gate="G$1" pin="75"/>
<wire x1="180.34" y1="121.92" x2="210.82" y2="121.92" width="0.1524" layer="91"/>
<label x="210.82" y="121.92" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="HW2" class="0">
<segment>
<pinref part="U13" gate="G$1" pin="74"/>
<wire x1="180.34" y1="119.38" x2="210.82" y2="119.38" width="0.1524" layer="91"/>
<label x="210.82" y="119.38" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="HW3" class="0">
<segment>
<pinref part="U13" gate="G$1" pin="73"/>
<wire x1="180.34" y1="116.84" x2="210.82" y2="116.84" width="0.1524" layer="91"/>
<label x="210.82" y="116.84" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="HW4" class="0">
<segment>
<pinref part="U13" gate="G$1" pin="72"/>
<wire x1="180.34" y1="114.3" x2="210.82" y2="114.3" width="0.1524" layer="91"/>
<label x="210.82" y="114.3" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="SPEED_2" class="0">
<segment>
<pinref part="U13" gate="G$1" pin="18"/>
<pinref part="TP19" gate="G$1" pin="PP"/>
<wire x1="88.9" y1="78.74" x2="66.04" y2="78.74" width="0.1524" layer="91"/>
<label x="68.58" y="78.74" size="1.27" layer="95"/>
</segment>
</net>
<net name="SPEED_1" class="0">
<segment>
<pinref part="U13" gate="G$1" pin="17"/>
<pinref part="TP8" gate="G$1" pin="PP"/>
<wire x1="88.9" y1="81.28" x2="66.04" y2="81.28" width="0.1524" layer="91"/>
<label x="68.58" y="81.28" size="1.27" layer="95"/>
</segment>
</net>
<net name="DIR_1" class="0">
<segment>
<pinref part="TP9" gate="G$1" pin="PP"/>
<pinref part="U13" gate="G$1" pin="13"/>
<wire x1="66.04" y1="91.44" x2="88.9" y2="91.44" width="0.1524" layer="91"/>
<label x="68.58" y="91.44" size="1.27" layer="95"/>
</segment>
</net>
<net name="DIR_2" class="0">
<segment>
<pinref part="TP20" gate="G$1" pin="PP"/>
<pinref part="U13" gate="G$1" pin="14"/>
<wire x1="66.04" y1="88.9" x2="88.9" y2="88.9" width="0.1524" layer="91"/>
<label x="68.58" y="88.9" size="1.27" layer="95"/>
</segment>
</net>
<net name="DIR_3" class="0">
<segment>
<pinref part="TP21" gate="G$1" pin="PP"/>
<pinref part="U13" gate="G$1" pin="15"/>
<wire x1="66.04" y1="86.36" x2="88.9" y2="86.36" width="0.1524" layer="91"/>
<label x="68.58" y="86.36" size="1.27" layer="95"/>
</segment>
</net>
<net name="DIR_4" class="0">
<segment>
<pinref part="TP15" gate="G$1" pin="PP"/>
<pinref part="U13" gate="G$1" pin="16"/>
<wire x1="66.04" y1="83.82" x2="88.9" y2="83.82" width="0.1524" layer="91"/>
<label x="68.58" y="83.82" size="1.27" layer="95"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="R148" gate="G$1" pin="1"/>
<wire x1="38.1" y1="73.66" x2="58.42" y2="73.66" width="0.1524" layer="91"/>
<wire x1="58.42" y1="73.66" x2="58.42" y2="109.22" width="0.1524" layer="91"/>
<pinref part="U13" gate="G$1" pin="6"/>
<wire x1="58.42" y1="109.22" x2="88.9" y2="109.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="D27" gate="G$1" pin="A"/>
<pinref part="R145" gate="G$1" pin="2"/>
<wire x1="21.336" y1="96.52" x2="27.94" y2="96.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="R145" gate="G$1" pin="1"/>
<wire x1="38.1" y1="96.52" x2="50.8" y2="96.52" width="0.1524" layer="91"/>
<wire x1="50.8" y1="96.52" x2="50.8" y2="116.84" width="0.1524" layer="91"/>
<pinref part="U13" gate="G$1" pin="3"/>
<wire x1="88.9" y1="116.84" x2="50.8" y2="116.84" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SDA_B" class="0">
<segment>
<pinref part="U13" gate="G$1" pin="46"/>
<wire x1="154.94" y1="45.72" x2="154.94" y2="30.48" width="0.1524" layer="91"/>
<label x="154.94" y="30.48" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
</net>
<net name="SCL_B" class="0">
<segment>
<pinref part="U13" gate="G$1" pin="47"/>
<wire x1="157.48" y1="45.72" x2="157.48" y2="30.48" width="0.1524" layer="91"/>
<label x="157.48" y="30.48" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
</net>
<net name="N$82" class="0">
<segment>
<pinref part="U13" gate="G$1" pin="21"/>
<wire x1="88.9" y1="71.12" x2="43.18" y2="71.12" width="0.1524" layer="91"/>
<pinref part="R137" gate="G$1" pin="1"/>
<wire x1="38.1" y1="66.04" x2="43.18" y2="66.04" width="0.1524" layer="91"/>
<wire x1="43.18" y1="66.04" x2="43.18" y2="71.12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$83" class="0">
<segment>
<pinref part="D25" gate="G$1" pin="A"/>
<pinref part="R135" gate="G$1" pin="2"/>
<wire x1="21.336" y1="58.42" x2="27.94" y2="58.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$84" class="0">
<segment>
<pinref part="D26" gate="G$1" pin="A"/>
<pinref part="R137" gate="G$1" pin="2"/>
<wire x1="21.336" y1="66.04" x2="27.94" y2="66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$86" class="0">
<segment>
<pinref part="U13" gate="G$1" pin="22"/>
<wire x1="88.9" y1="68.58" x2="48.26" y2="68.58" width="0.1524" layer="91"/>
<pinref part="R135" gate="G$1" pin="1"/>
<wire x1="48.26" y1="68.58" x2="48.26" y2="58.42" width="0.1524" layer="91"/>
<wire x1="48.26" y1="58.42" x2="38.1" y2="58.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="CTRL1" class="0">
<segment>
<pinref part="U13" gate="G$1" pin="85"/>
<wire x1="142.24" y1="137.16" x2="142.24" y2="167.64" width="0.1524" layer="91"/>
<label x="142.24" y="167.64" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
<text x="73.66" y="190.5" size="1.27" layer="91">10-30V</text>
</plain>
<instances>
<instance part="C41" gate="G$1" x="256.54" y="177.8" smashed="yes" rot="R270">
<attribute name="NAME" x="256.032" y="172.72" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="256.032" y="179.07" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C22" gate="G$1" x="160.02" y="165.1" smashed="yes" rot="R270">
<attribute name="NAME" x="160.02" y="160.02" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="160.02" y="167.64" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="L3" gate="G$1" x="119.38" y="187.96" smashed="yes">
<attribute name="NAME" x="113.792" y="188.468" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="122.428" y="188.468" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="R47" gate="G$1" x="119.38" y="195.58" smashed="yes">
<attribute name="NAME" x="111.76" y="195.58" size="1.27" layer="95"/>
<attribute name="VALUE" x="124.46" y="195.58" size="1.27" layer="96"/>
</instance>
<instance part="C31" gate="G$1" x="139.7" y="180.34" smashed="yes" rot="R90">
<attribute name="NAME" x="139.192" y="174.498" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="139.192" y="181.61" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C30" gate="G$1" x="147.32" y="180.34" smashed="yes" rot="R90">
<attribute name="NAME" x="146.812" y="174.498" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="146.812" y="181.61" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="U4" gate="G$1" x="170.18" y="190.5" smashed="yes">
<attribute name="NAME" x="170.18" y="193.548" size="1.27" layer="95" ratio="6" rot="SR0"/>
<attribute name="VALUE" x="170.18" y="196.088" size="1.27" layer="96" ratio="6" rot="SR0"/>
</instance>
<instance part="R40" gate="G$1" x="152.4" y="167.64" smashed="yes" rot="R90">
<attribute name="NAME" x="152.4" y="160.02" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="152.4" y="172.72" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="SUPPLY26" gate="GND" x="132.08" y="167.64" smashed="yes">
<attribute name="VALUE" x="130.175" y="164.465" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY27" gate="GND" x="152.4" y="152.4" smashed="yes">
<attribute name="VALUE" x="150.495" y="149.225" size="1.27" layer="96"/>
</instance>
<instance part="R51" gate="G$1" x="276.86" y="177.8" smashed="yes" rot="R90">
<attribute name="NAME" x="276.3774" y="170.942" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="276.352" y="180.34" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R38" gate="G$1" x="284.48" y="149.86" smashed="yes" rot="R180">
<attribute name="NAME" x="277.622" y="150.3426" size="1.27" layer="95"/>
<attribute name="VALUE" x="287.02" y="150.368" size="1.27" layer="96"/>
</instance>
<instance part="L5" gate="G$1" x="248.92" y="187.96" smashed="yes">
<attribute name="NAME" x="242.824" y="188.468" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="251.968" y="188.468" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="C27" gate="G$1" x="218.44" y="182.88" smashed="yes">
<attribute name="NAME" x="213.36" y="183.388" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="219.71" y="183.388" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="SUPPLY28" gate="GND" x="292.1" y="144.78" smashed="yes">
<attribute name="VALUE" x="293.497" y="144.907" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY29" gate="GND" x="200.66" y="167.64" smashed="yes">
<attribute name="VALUE" x="198.755" y="164.465" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY30" gate="GND" x="256.54" y="157.48" smashed="yes">
<attribute name="VALUE" x="254.635" y="154.305" size="1.27" layer="96"/>
</instance>
<instance part="C32" gate="G$1" x="132.08" y="180.34" smashed="yes" rot="R90">
<attribute name="NAME" x="131.572" y="174.498" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="131.572" y="181.61" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="R42" gate="G$1" x="205.74" y="182.88" smashed="yes">
<attribute name="NAME" x="199.39" y="183.3626" size="1.27" layer="95"/>
<attribute name="VALUE" x="208.28" y="183.388" size="1.27" layer="96"/>
</instance>
<instance part="R48" gate="G$1" x="226.06" y="175.26" smashed="yes" rot="R90">
<attribute name="NAME" x="225.552" y="169.672" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="225.552" y="179.324" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="C33" gate="G$1" x="233.68" y="162.56" smashed="yes" rot="R180">
<attribute name="NAME" x="238.76" y="162.052" size="1.27" layer="95" ratio="10" rot="R180"/>
<attribute name="VALUE" x="232.41" y="162.052" size="1.27" layer="96" ratio="10" rot="R180"/>
</instance>
<instance part="C40" gate="G$1" x="281.94" y="177.8" smashed="yes" rot="R90">
<attribute name="NAME" x="281.432" y="172.72" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="281.432" y="179.07" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C38" gate="G$1" x="287.02" y="177.8" smashed="yes" rot="R90">
<attribute name="NAME" x="286.512" y="172.72" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="286.512" y="179.07" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="SUPPLY31" gate="GND" x="287.02" y="165.1" smashed="yes">
<attribute name="VALUE" x="285.115" y="161.925" size="1.27" layer="96"/>
</instance>
<instance part="C44" gate="G$1" x="261.62" y="177.8" smashed="yes" rot="R270">
<attribute name="NAME" x="261.112" y="172.72" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="261.112" y="179.07" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C42" gate="G$1" x="266.7" y="177.8" smashed="yes" rot="R270">
<attribute name="NAME" x="266.192" y="172.72" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="266.192" y="179.07" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C43" gate="G$1" x="271.78" y="177.8" smashed="yes" rot="R270">
<attribute name="NAME" x="271.272" y="172.72" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="271.272" y="179.07" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="D18" gate="G$1" x="101.6" y="187.96" smashed="yes">
<attribute name="NAME" x="99.06" y="190.5" size="1.27" layer="95"/>
<attribute name="VALUE" x="99.06" y="182.88" size="1.27" layer="96"/>
</instance>
<instance part="D19" gate="G$1" x="241.3" y="177.8" smashed="yes" rot="R90">
<attribute name="NAME" x="238.76" y="175.26" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="236.22" y="175.26" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="FRAME5" gate="G$1" x="0" y="0" smashed="yes">
<attribute name="DRAWING_NAME" x="344.17" y="15.24" size="2.54" layer="94"/>
<attribute name="LAST_DATE_TIME" x="344.17" y="10.16" size="2.286" layer="94"/>
<attribute name="SHEET" x="357.505" y="5.08" size="2.54" layer="94"/>
</instance>
<instance part="R165" gate="G$1" x="101.6" y="193.04" smashed="yes">
<attribute name="NAME" x="99.06" y="195.58" size="1.778" layer="95"/>
<attribute name="VALUE" x="99.06" y="198.12" size="1.778" layer="96"/>
</instance>
<instance part="R166" gate="G$1" x="297.18" y="193.04" smashed="yes">
<attribute name="NAME" x="294.64" y="195.58" size="1.778" layer="95"/>
<attribute name="VALUE" x="294.64" y="198.12" size="1.778" layer="96"/>
</instance>
<instance part="D21" gate="G$1" x="297.18" y="187.96" smashed="yes">
<attribute name="NAME" x="294.64" y="190.5" size="1.27" layer="95"/>
<attribute name="VALUE" x="294.64" y="182.88" size="1.27" layer="96"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="C30" gate="G$1" pin="1"/>
<wire x1="147.32" y1="175.26" x2="147.32" y2="172.72" width="0.1524" layer="91"/>
<wire x1="147.32" y1="172.72" x2="139.7" y2="172.72" width="0.1524" layer="91"/>
<wire x1="139.7" y1="172.72" x2="132.08" y2="172.72" width="0.1524" layer="91"/>
<wire x1="132.08" y1="172.72" x2="132.08" y2="175.26" width="0.1524" layer="91"/>
<pinref part="C31" gate="G$1" pin="1"/>
<wire x1="139.7" y1="175.26" x2="139.7" y2="172.72" width="0.1524" layer="91"/>
<junction x="139.7" y="172.72"/>
<wire x1="132.08" y1="172.72" x2="132.08" y2="170.18" width="0.1524" layer="91"/>
<junction x="132.08" y="172.72"/>
<pinref part="SUPPLY26" gate="GND" pin="GND"/>
<pinref part="C32" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="R40" gate="G$1" pin="1"/>
<wire x1="152.4" y1="162.56" x2="152.4" y2="157.48" width="0.1524" layer="91"/>
<wire x1="152.4" y1="157.48" x2="160.02" y2="157.48" width="0.1524" layer="91"/>
<pinref part="C22" gate="G$1" pin="2"/>
<wire x1="160.02" y1="157.48" x2="160.02" y2="160.02" width="0.1524" layer="91"/>
<pinref part="SUPPLY27" gate="GND" pin="GND"/>
<wire x1="152.4" y1="157.48" x2="152.4" y2="154.94" width="0.1524" layer="91"/>
<junction x="152.4" y="157.48"/>
</segment>
<segment>
<pinref part="R38" gate="G$1" pin="1"/>
<pinref part="SUPPLY28" gate="GND" pin="GND"/>
<wire x1="289.56" y1="149.86" x2="292.1" y2="149.86" width="0.1524" layer="91"/>
<wire x1="292.1" y1="149.86" x2="292.1" y2="147.32" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C41" gate="G$1" pin="2"/>
<pinref part="SUPPLY30" gate="GND" pin="GND"/>
<wire x1="256.54" y1="172.72" x2="256.54" y2="170.18" width="0.1524" layer="91"/>
<pinref part="C33" gate="G$1" pin="1"/>
<wire x1="256.54" y1="170.18" x2="256.54" y2="162.56" width="0.1524" layer="91"/>
<wire x1="256.54" y1="162.56" x2="256.54" y2="160.02" width="0.1524" layer="91"/>
<wire x1="238.76" y1="162.56" x2="241.3" y2="162.56" width="0.1524" layer="91"/>
<wire x1="241.3" y1="162.56" x2="241.3" y2="173.99" width="0.1524" layer="91"/>
<wire x1="241.3" y1="162.56" x2="256.54" y2="162.56" width="0.1524" layer="91"/>
<junction x="241.3" y="162.56"/>
<junction x="256.54" y="162.56"/>
<pinref part="C44" gate="G$1" pin="2"/>
<wire x1="261.62" y1="172.72" x2="261.62" y2="170.18" width="0.1524" layer="91"/>
<wire x1="261.62" y1="170.18" x2="256.54" y2="170.18" width="0.1524" layer="91"/>
<pinref part="C42" gate="G$1" pin="2"/>
<wire x1="261.62" y1="170.18" x2="266.7" y2="170.18" width="0.1524" layer="91"/>
<wire x1="266.7" y1="170.18" x2="266.7" y2="172.72" width="0.1524" layer="91"/>
<wire x1="266.7" y1="170.18" x2="271.78" y2="170.18" width="0.1524" layer="91"/>
<pinref part="C43" gate="G$1" pin="2"/>
<wire x1="271.78" y1="170.18" x2="271.78" y2="172.72" width="0.1524" layer="91"/>
<junction x="261.62" y="170.18"/>
<junction x="266.7" y="170.18"/>
<junction x="256.54" y="170.18"/>
<pinref part="D19" gate="G$1" pin="A"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="GND"/>
<wire x1="195.58" y1="175.26" x2="200.66" y2="175.26" width="0.1524" layer="91"/>
<wire x1="200.66" y1="175.26" x2="200.66" y2="172.72" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="PAD"/>
<wire x1="200.66" y1="172.72" x2="200.66" y2="170.18" width="0.1524" layer="91"/>
<wire x1="195.58" y1="172.72" x2="200.66" y2="172.72" width="0.1524" layer="91"/>
<junction x="200.66" y="172.72"/>
<pinref part="SUPPLY29" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C40" gate="G$1" pin="1"/>
<wire x1="281.94" y1="172.72" x2="281.94" y2="170.18" width="0.1524" layer="91"/>
<wire x1="281.94" y1="170.18" x2="287.02" y2="170.18" width="0.1524" layer="91"/>
<pinref part="C38" gate="G$1" pin="1"/>
<wire x1="287.02" y1="170.18" x2="287.02" y2="172.72" width="0.1524" layer="91"/>
<wire x1="287.02" y1="170.18" x2="287.02" y2="167.64" width="0.1524" layer="91"/>
<junction x="287.02" y="170.18"/>
<pinref part="SUPPLY31" gate="GND" pin="GND"/>
</segment>
</net>
<net name="N$62" class="0">
<segment>
<pinref part="R47" gate="G$1" pin="2"/>
<wire x1="124.46" y1="195.58" x2="127" y2="195.58" width="0.1524" layer="91"/>
<wire x1="127" y1="195.58" x2="127" y2="187.96" width="0.1524" layer="91"/>
<pinref part="L3" gate="G$1" pin="2"/>
<wire x1="127" y1="187.96" x2="124.46" y2="187.96" width="0.1524" layer="91"/>
<wire x1="127" y1="187.96" x2="132.08" y2="187.96" width="0.1524" layer="91"/>
<junction x="127" y="187.96"/>
<pinref part="C30" gate="G$1" pin="2"/>
<wire x1="132.08" y1="187.96" x2="139.7" y2="187.96" width="0.1524" layer="91"/>
<wire x1="139.7" y1="187.96" x2="147.32" y2="187.96" width="0.1524" layer="91"/>
<wire x1="147.32" y1="187.96" x2="147.32" y2="185.42" width="0.1524" layer="91"/>
<pinref part="C31" gate="G$1" pin="2"/>
<wire x1="139.7" y1="185.42" x2="139.7" y2="187.96" width="0.1524" layer="91"/>
<junction x="139.7" y="187.96"/>
<wire x1="132.08" y1="185.42" x2="132.08" y2="187.96" width="0.1524" layer="91"/>
<junction x="132.08" y="187.96"/>
<pinref part="U4" gate="G$1" pin="VIN"/>
<wire x1="147.32" y1="187.96" x2="165.1" y2="187.96" width="0.1524" layer="91"/>
<junction x="147.32" y="187.96"/>
<pinref part="C32" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$63" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="RT/SYNC"/>
<pinref part="R40" gate="G$1" pin="2"/>
<wire x1="165.1" y1="177.8" x2="152.4" y2="177.8" width="0.1524" layer="91"/>
<wire x1="152.4" y1="177.8" x2="152.4" y2="172.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$64" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="SS"/>
<pinref part="C22" gate="G$1" pin="1"/>
<wire x1="165.1" y1="172.72" x2="160.02" y2="172.72" width="0.1524" layer="91"/>
<wire x1="160.02" y1="172.72" x2="160.02" y2="170.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$65" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="BOOT"/>
<wire x1="195.58" y1="185.42" x2="198.12" y2="185.42" width="0.1524" layer="91"/>
<wire x1="198.12" y1="185.42" x2="198.12" y2="182.88" width="0.1524" layer="91"/>
<wire x1="198.12" y1="182.88" x2="200.66" y2="182.88" width="0.1524" layer="91"/>
<pinref part="R42" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$66" class="0">
<segment>
<pinref part="C27" gate="G$1" pin="2"/>
<wire x1="223.52" y1="182.88" x2="226.06" y2="182.88" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="SW"/>
<wire x1="226.06" y1="182.88" x2="226.06" y2="187.96" width="0.1524" layer="91"/>
<wire x1="226.06" y1="187.96" x2="195.58" y2="187.96" width="0.1524" layer="91"/>
<pinref part="L5" gate="G$1" pin="1"/>
<wire x1="226.06" y1="187.96" x2="241.3" y2="187.96" width="0.1524" layer="91"/>
<junction x="226.06" y="187.96"/>
<wire x1="241.3" y1="187.96" x2="243.84" y2="187.96" width="0.1524" layer="91"/>
<wire x1="241.3" y1="181.61" x2="241.3" y2="187.96" width="0.1524" layer="91"/>
<junction x="241.3" y="187.96"/>
<pinref part="R48" gate="G$1" pin="2"/>
<wire x1="226.06" y1="180.34" x2="226.06" y2="182.88" width="0.1524" layer="91"/>
<junction x="226.06" y="182.88"/>
<pinref part="D19" gate="G$1" pin="K"/>
</segment>
</net>
<net name="N$67" class="0">
<segment>
<pinref part="R51" gate="G$1" pin="1"/>
<pinref part="R38" gate="G$1" pin="2"/>
<wire x1="276.86" y1="172.72" x2="276.86" y2="149.86" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="FB"/>
<wire x1="276.86" y1="149.86" x2="279.4" y2="149.86" width="0.1524" layer="91"/>
<wire x1="195.58" y1="180.34" x2="208.28" y2="180.34" width="0.1524" layer="91"/>
<wire x1="208.28" y1="180.34" x2="208.28" y2="149.86" width="0.1524" layer="91"/>
<junction x="276.86" y="149.86"/>
<wire x1="208.28" y1="149.86" x2="276.86" y2="149.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$68" class="0">
<segment>
<pinref part="C27" gate="G$1" pin="1"/>
<pinref part="R42" gate="G$1" pin="2"/>
<wire x1="210.82" y1="182.88" x2="213.36" y2="182.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$69" class="0">
<segment>
<pinref part="R48" gate="G$1" pin="1"/>
<pinref part="C33" gate="G$1" pin="2"/>
<wire x1="226.06" y1="170.18" x2="226.06" y2="162.56" width="0.1524" layer="91"/>
<wire x1="226.06" y1="162.56" x2="228.6" y2="162.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$70" class="0">
<segment>
<pinref part="R47" gate="G$1" pin="1"/>
<wire x1="114.3" y1="195.58" x2="111.76" y2="195.58" width="0.1524" layer="91"/>
<wire x1="111.76" y1="195.58" x2="111.76" y2="187.96" width="0.1524" layer="91"/>
<pinref part="L3" gate="G$1" pin="1"/>
<wire x1="114.3" y1="187.96" x2="111.76" y2="187.96" width="0.1524" layer="91"/>
<junction x="111.76" y="187.96"/>
<wire x1="105.41" y1="187.96" x2="109.22" y2="187.96" width="0.1524" layer="91"/>
<pinref part="D18" gate="G$1" pin="K"/>
<pinref part="R165" gate="G$1" pin="2"/>
<wire x1="109.22" y1="187.96" x2="111.76" y2="187.96" width="0.1524" layer="91"/>
<wire x1="106.68" y1="193.04" x2="109.22" y2="193.04" width="0.1524" layer="91"/>
<wire x1="109.22" y1="193.04" x2="109.22" y2="187.96" width="0.1524" layer="91"/>
<junction x="109.22" y="187.96"/>
</segment>
</net>
<net name="N$71" class="0">
<segment>
<pinref part="L5" gate="G$1" pin="2"/>
<wire x1="254" y1="187.96" x2="256.54" y2="187.96" width="0.1524" layer="91"/>
<pinref part="C41" gate="G$1" pin="1"/>
<wire x1="256.54" y1="187.96" x2="261.62" y2="187.96" width="0.1524" layer="91"/>
<wire x1="261.62" y1="187.96" x2="266.7" y2="187.96" width="0.1524" layer="91"/>
<wire x1="266.7" y1="187.96" x2="271.78" y2="187.96" width="0.1524" layer="91"/>
<wire x1="271.78" y1="187.96" x2="276.86" y2="187.96" width="0.1524" layer="91"/>
<wire x1="276.86" y1="187.96" x2="281.94" y2="187.96" width="0.1524" layer="91"/>
<wire x1="281.94" y1="187.96" x2="287.02" y2="187.96" width="0.1524" layer="91"/>
<wire x1="256.54" y1="182.88" x2="256.54" y2="187.96" width="0.1524" layer="91"/>
<junction x="256.54" y="187.96"/>
<pinref part="C44" gate="G$1" pin="1"/>
<wire x1="261.62" y1="182.88" x2="261.62" y2="187.96" width="0.1524" layer="91"/>
<junction x="261.62" y="187.96"/>
<pinref part="C42" gate="G$1" pin="1"/>
<wire x1="266.7" y1="182.88" x2="266.7" y2="187.96" width="0.1524" layer="91"/>
<junction x="266.7" y="187.96"/>
<pinref part="C43" gate="G$1" pin="1"/>
<wire x1="271.78" y1="182.88" x2="271.78" y2="187.96" width="0.1524" layer="91"/>
<junction x="271.78" y="187.96"/>
<pinref part="R51" gate="G$1" pin="2"/>
<wire x1="276.86" y1="182.88" x2="276.86" y2="187.96" width="0.1524" layer="91"/>
<junction x="276.86" y="187.96"/>
<pinref part="C40" gate="G$1" pin="2"/>
<wire x1="281.94" y1="182.88" x2="281.94" y2="187.96" width="0.1524" layer="91"/>
<junction x="281.94" y="187.96"/>
<pinref part="C38" gate="G$1" pin="2"/>
<wire x1="287.02" y1="182.88" x2="287.02" y2="187.96" width="0.1524" layer="91"/>
<pinref part="D21" gate="G$1" pin="A"/>
<wire x1="293.37" y1="187.96" x2="289.56" y2="187.96" width="0.1524" layer="91"/>
<pinref part="R166" gate="G$1" pin="1"/>
<wire x1="289.56" y1="187.96" x2="287.02" y2="187.96" width="0.1524" layer="91"/>
<wire x1="292.1" y1="193.04" x2="289.56" y2="193.04" width="0.1524" layer="91"/>
<wire x1="289.56" y1="193.04" x2="289.56" y2="187.96" width="0.1524" layer="91"/>
<junction x="289.56" y="187.96"/>
<junction x="287.02" y="187.96"/>
</segment>
</net>
<net name="VIN" class="0">
<segment>
<wire x1="97.79" y1="187.96" x2="91.44" y2="187.96" width="0.1524" layer="91"/>
<pinref part="D18" gate="G$1" pin="A"/>
<label x="83.82" y="187.96" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="R165" gate="G$1" pin="1"/>
<wire x1="91.44" y1="187.96" x2="83.82" y2="187.96" width="0.1524" layer="91"/>
<wire x1="96.52" y1="193.04" x2="91.44" y2="193.04" width="0.1524" layer="91"/>
<wire x1="91.44" y1="193.04" x2="91.44" y2="187.96" width="0.1524" layer="91"/>
<junction x="91.44" y="187.96"/>
</segment>
</net>
<net name="+5V0" class="0">
<segment>
<label x="309.88" y="187.96" size="1.27" layer="95" xref="yes"/>
<pinref part="D21" gate="G$1" pin="K"/>
<wire x1="300.99" y1="187.96" x2="304.8" y2="187.96" width="0.1524" layer="91"/>
<pinref part="R166" gate="G$1" pin="2"/>
<wire x1="304.8" y1="187.96" x2="309.88" y2="187.96" width="0.1524" layer="91"/>
<wire x1="302.26" y1="193.04" x2="304.8" y2="193.04" width="0.1524" layer="91"/>
<wire x1="304.8" y1="193.04" x2="304.8" y2="187.96" width="0.1524" layer="91"/>
<junction x="304.8" y="187.96"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
<text x="302.26" y="190.5" size="1.27" layer="91">+9V0</text>
</plain>
<instances>
<instance part="C36" gate="G$1" x="256.54" y="177.8" smashed="yes" rot="R270">
<attribute name="NAME" x="256.032" y="172.72" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="256.032" y="179.07" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C23" gate="G$1" x="160.02" y="165.1" smashed="yes" rot="R270">
<attribute name="NAME" x="159.512" y="159.004" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="159.512" y="166.37" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="L2" gate="G$1" x="119.38" y="187.96" smashed="yes">
<attribute name="NAME" x="113.792" y="188.468" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="122.428" y="188.468" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="R34" gate="G$1" x="119.38" y="195.58" smashed="yes">
<attribute name="NAME" x="112.268" y="196.0626" size="1.27" layer="95"/>
<attribute name="VALUE" x="122.174" y="196.088" size="1.27" layer="96"/>
</instance>
<instance part="C21" gate="G$1" x="139.7" y="180.34" smashed="yes" rot="R90">
<attribute name="NAME" x="139.192" y="174.498" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="139.192" y="181.61" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C24" gate="G$1" x="147.32" y="180.34" smashed="yes" rot="R90">
<attribute name="NAME" x="146.812" y="174.498" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="146.812" y="181.61" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="U3" gate="G$1" x="170.18" y="190.5" smashed="yes">
<attribute name="NAME" x="170.18" y="193.04" size="1.27" layer="95" ratio="6" rot="SR0"/>
<attribute name="VALUE" x="170.18" y="195.58" size="1.27" layer="96" ratio="6" rot="SR0"/>
</instance>
<instance part="R39" gate="G$1" x="152.4" y="167.64" smashed="yes" rot="R90">
<attribute name="NAME" x="151.9174" y="160.528" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="151.892" y="170.18" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="SUPPLY84" gate="GND" x="132.08" y="167.64" smashed="yes">
<attribute name="VALUE" x="130.175" y="164.465" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY85" gate="GND" x="152.4" y="152.4" smashed="yes">
<attribute name="VALUE" x="150.495" y="149.225" size="1.27" layer="96"/>
</instance>
<instance part="R49" gate="G$1" x="276.86" y="177.8" smashed="yes" rot="R90">
<attribute name="NAME" x="276.3774" y="170.942" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="276.352" y="180.34" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R37" gate="G$1" x="284.48" y="149.86" smashed="yes" rot="R180">
<attribute name="NAME" x="277.622" y="150.3426" size="1.27" layer="95"/>
<attribute name="VALUE" x="287.02" y="150.368" size="1.27" layer="96"/>
</instance>
<instance part="L6" gate="G$1" x="248.92" y="187.96" smashed="yes">
<attribute name="NAME" x="242.824" y="188.468" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="251.968" y="188.468" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="C25" gate="G$1" x="218.44" y="182.88" smashed="yes">
<attribute name="NAME" x="213.36" y="183.388" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="219.71" y="183.388" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="SUPPLY86" gate="GND" x="292.1" y="144.78" smashed="yes">
<attribute name="VALUE" x="293.497" y="144.907" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY87" gate="GND" x="200.66" y="167.64" smashed="yes">
<attribute name="VALUE" x="198.755" y="164.465" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY88" gate="GND" x="256.54" y="157.48" smashed="yes">
<attribute name="VALUE" x="254.635" y="154.305" size="1.27" layer="96"/>
</instance>
<instance part="C19" gate="G$1" x="132.08" y="180.34" smashed="yes" rot="R90">
<attribute name="NAME" x="131.572" y="174.498" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="131.572" y="181.61" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="R43" gate="G$1" x="205.74" y="182.88" smashed="yes">
<attribute name="NAME" x="199.39" y="183.3626" size="1.27" layer="95"/>
<attribute name="VALUE" x="208.28" y="183.388" size="1.27" layer="96"/>
</instance>
<instance part="R44" gate="G$1" x="226.06" y="175.26" smashed="yes" rot="R90">
<attribute name="NAME" x="225.552" y="169.672" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="225.552" y="179.324" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="C26" gate="G$1" x="233.68" y="162.56" smashed="yes" rot="R180">
<attribute name="NAME" x="238.76" y="162.052" size="1.27" layer="95" ratio="10" rot="R180"/>
<attribute name="VALUE" x="232.41" y="162.052" size="1.27" layer="96" ratio="10" rot="R180"/>
</instance>
<instance part="C34" gate="G$1" x="281.94" y="177.8" smashed="yes" rot="R90">
<attribute name="NAME" x="281.432" y="172.72" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="281.432" y="179.07" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C45" gate="G$1" x="287.02" y="177.8" smashed="yes" rot="R90">
<attribute name="NAME" x="286.512" y="172.72" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="286.512" y="179.07" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="SUPPLY53" gate="GND" x="287.02" y="165.1" smashed="yes">
<attribute name="VALUE" x="285.115" y="161.925" size="1.27" layer="96"/>
</instance>
<instance part="C37" gate="G$1" x="261.62" y="177.8" smashed="yes" rot="R270">
<attribute name="NAME" x="261.112" y="172.72" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="261.112" y="179.07" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C39" gate="G$1" x="266.7" y="177.8" smashed="yes" rot="R270">
<attribute name="NAME" x="266.192" y="172.72" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="266.192" y="179.07" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C35" gate="G$1" x="271.78" y="177.8" smashed="yes" rot="R270">
<attribute name="NAME" x="271.272" y="172.72" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="271.272" y="179.07" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="D20" gate="G$1" x="241.3" y="177.8" smashed="yes" rot="R90">
<attribute name="NAME" x="238.76" y="172.72" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="236.22" y="172.72" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="FRAME4" gate="G$1" x="0" y="0" smashed="yes">
<attribute name="DRAWING_NAME" x="344.17" y="15.24" size="2.54" layer="94"/>
<attribute name="LAST_DATE_TIME" x="344.17" y="10.16" size="2.286" layer="94"/>
<attribute name="SHEET" x="357.505" y="5.08" size="2.54" layer="94"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="C24" gate="G$1" pin="1"/>
<wire x1="147.32" y1="175.26" x2="147.32" y2="172.72" width="0.1524" layer="91"/>
<wire x1="147.32" y1="172.72" x2="139.7" y2="172.72" width="0.1524" layer="91"/>
<wire x1="139.7" y1="172.72" x2="132.08" y2="172.72" width="0.1524" layer="91"/>
<wire x1="132.08" y1="172.72" x2="132.08" y2="175.26" width="0.1524" layer="91"/>
<pinref part="C21" gate="G$1" pin="1"/>
<wire x1="139.7" y1="175.26" x2="139.7" y2="172.72" width="0.1524" layer="91"/>
<junction x="139.7" y="172.72"/>
<wire x1="132.08" y1="172.72" x2="132.08" y2="170.18" width="0.1524" layer="91"/>
<junction x="132.08" y="172.72"/>
<pinref part="SUPPLY84" gate="GND" pin="GND"/>
<pinref part="C19" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="R39" gate="G$1" pin="1"/>
<wire x1="152.4" y1="162.56" x2="152.4" y2="157.48" width="0.1524" layer="91"/>
<wire x1="152.4" y1="157.48" x2="160.02" y2="157.48" width="0.1524" layer="91"/>
<pinref part="C23" gate="G$1" pin="2"/>
<wire x1="160.02" y1="157.48" x2="160.02" y2="160.02" width="0.1524" layer="91"/>
<pinref part="SUPPLY85" gate="GND" pin="GND"/>
<wire x1="152.4" y1="157.48" x2="152.4" y2="154.94" width="0.1524" layer="91"/>
<junction x="152.4" y="157.48"/>
</segment>
<segment>
<pinref part="R37" gate="G$1" pin="1"/>
<pinref part="SUPPLY86" gate="GND" pin="GND"/>
<wire x1="289.56" y1="149.86" x2="292.1" y2="149.86" width="0.1524" layer="91"/>
<wire x1="292.1" y1="149.86" x2="292.1" y2="147.32" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C36" gate="G$1" pin="2"/>
<pinref part="SUPPLY88" gate="GND" pin="GND"/>
<wire x1="256.54" y1="172.72" x2="256.54" y2="170.18" width="0.1524" layer="91"/>
<pinref part="C26" gate="G$1" pin="1"/>
<wire x1="256.54" y1="170.18" x2="256.54" y2="162.56" width="0.1524" layer="91"/>
<wire x1="256.54" y1="162.56" x2="256.54" y2="160.02" width="0.1524" layer="91"/>
<wire x1="238.76" y1="162.56" x2="241.3" y2="162.56" width="0.1524" layer="91"/>
<wire x1="241.3" y1="162.56" x2="241.3" y2="173.99" width="0.1524" layer="91"/>
<wire x1="241.3" y1="162.56" x2="256.54" y2="162.56" width="0.1524" layer="91"/>
<junction x="241.3" y="162.56"/>
<junction x="256.54" y="162.56"/>
<pinref part="C37" gate="G$1" pin="2"/>
<wire x1="261.62" y1="172.72" x2="261.62" y2="170.18" width="0.1524" layer="91"/>
<wire x1="261.62" y1="170.18" x2="256.54" y2="170.18" width="0.1524" layer="91"/>
<pinref part="C39" gate="G$1" pin="2"/>
<wire x1="261.62" y1="170.18" x2="266.7" y2="170.18" width="0.1524" layer="91"/>
<wire x1="266.7" y1="170.18" x2="266.7" y2="172.72" width="0.1524" layer="91"/>
<wire x1="266.7" y1="170.18" x2="271.78" y2="170.18" width="0.1524" layer="91"/>
<pinref part="C35" gate="G$1" pin="2"/>
<wire x1="271.78" y1="170.18" x2="271.78" y2="172.72" width="0.1524" layer="91"/>
<junction x="261.62" y="170.18"/>
<junction x="266.7" y="170.18"/>
<junction x="256.54" y="170.18"/>
<pinref part="D20" gate="G$1" pin="A"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="GND"/>
<wire x1="195.58" y1="175.26" x2="200.66" y2="175.26" width="0.1524" layer="91"/>
<wire x1="200.66" y1="175.26" x2="200.66" y2="172.72" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="PAD"/>
<wire x1="200.66" y1="172.72" x2="200.66" y2="170.18" width="0.1524" layer="91"/>
<wire x1="195.58" y1="172.72" x2="200.66" y2="172.72" width="0.1524" layer="91"/>
<junction x="200.66" y="172.72"/>
<pinref part="SUPPLY87" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C34" gate="G$1" pin="1"/>
<wire x1="281.94" y1="172.72" x2="281.94" y2="170.18" width="0.1524" layer="91"/>
<wire x1="281.94" y1="170.18" x2="287.02" y2="170.18" width="0.1524" layer="91"/>
<pinref part="C45" gate="G$1" pin="1"/>
<wire x1="287.02" y1="170.18" x2="287.02" y2="172.72" width="0.1524" layer="91"/>
<wire x1="287.02" y1="170.18" x2="287.02" y2="167.64" width="0.1524" layer="91"/>
<junction x="287.02" y="170.18"/>
<pinref part="SUPPLY53" gate="GND" pin="GND"/>
</segment>
</net>
<net name="N$158" class="0">
<segment>
<pinref part="R34" gate="G$1" pin="2"/>
<wire x1="124.46" y1="195.58" x2="127" y2="195.58" width="0.1524" layer="91"/>
<wire x1="127" y1="195.58" x2="127" y2="187.96" width="0.1524" layer="91"/>
<pinref part="L2" gate="G$1" pin="2"/>
<wire x1="127" y1="187.96" x2="124.46" y2="187.96" width="0.1524" layer="91"/>
<wire x1="127" y1="187.96" x2="132.08" y2="187.96" width="0.1524" layer="91"/>
<junction x="127" y="187.96"/>
<pinref part="C24" gate="G$1" pin="2"/>
<wire x1="132.08" y1="187.96" x2="139.7" y2="187.96" width="0.1524" layer="91"/>
<wire x1="139.7" y1="187.96" x2="147.32" y2="187.96" width="0.1524" layer="91"/>
<wire x1="147.32" y1="187.96" x2="147.32" y2="185.42" width="0.1524" layer="91"/>
<pinref part="C21" gate="G$1" pin="2"/>
<wire x1="139.7" y1="185.42" x2="139.7" y2="187.96" width="0.1524" layer="91"/>
<junction x="139.7" y="187.96"/>
<wire x1="132.08" y1="185.42" x2="132.08" y2="187.96" width="0.1524" layer="91"/>
<junction x="132.08" y="187.96"/>
<pinref part="U3" gate="G$1" pin="VIN"/>
<wire x1="147.32" y1="187.96" x2="165.1" y2="187.96" width="0.1524" layer="91"/>
<junction x="147.32" y="187.96"/>
<pinref part="C19" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$167" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="RT/SYNC"/>
<pinref part="R39" gate="G$1" pin="2"/>
<wire x1="165.1" y1="177.8" x2="152.4" y2="177.8" width="0.1524" layer="91"/>
<wire x1="152.4" y1="177.8" x2="152.4" y2="172.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$172" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="SS"/>
<pinref part="C23" gate="G$1" pin="1"/>
<wire x1="165.1" y1="172.72" x2="160.02" y2="172.72" width="0.1524" layer="91"/>
<wire x1="160.02" y1="172.72" x2="160.02" y2="170.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$177" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="BOOT"/>
<wire x1="195.58" y1="185.42" x2="198.12" y2="185.42" width="0.1524" layer="91"/>
<wire x1="198.12" y1="185.42" x2="198.12" y2="182.88" width="0.1524" layer="91"/>
<wire x1="198.12" y1="182.88" x2="200.66" y2="182.88" width="0.1524" layer="91"/>
<pinref part="R43" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$179" class="0">
<segment>
<pinref part="C25" gate="G$1" pin="2"/>
<wire x1="223.52" y1="182.88" x2="226.06" y2="182.88" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="SW"/>
<wire x1="226.06" y1="182.88" x2="226.06" y2="187.96" width="0.1524" layer="91"/>
<wire x1="226.06" y1="187.96" x2="195.58" y2="187.96" width="0.1524" layer="91"/>
<pinref part="L6" gate="G$1" pin="1"/>
<wire x1="226.06" y1="187.96" x2="241.3" y2="187.96" width="0.1524" layer="91"/>
<junction x="226.06" y="187.96"/>
<wire x1="241.3" y1="187.96" x2="243.84" y2="187.96" width="0.1524" layer="91"/>
<wire x1="241.3" y1="181.61" x2="241.3" y2="187.96" width="0.1524" layer="91"/>
<junction x="241.3" y="187.96"/>
<pinref part="R44" gate="G$1" pin="2"/>
<wire x1="226.06" y1="180.34" x2="226.06" y2="182.88" width="0.1524" layer="91"/>
<junction x="226.06" y="182.88"/>
<pinref part="D20" gate="G$1" pin="K"/>
</segment>
</net>
<net name="N$181" class="0">
<segment>
<pinref part="R49" gate="G$1" pin="1"/>
<pinref part="R37" gate="G$1" pin="2"/>
<wire x1="276.86" y1="172.72" x2="276.86" y2="149.86" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="FB"/>
<wire x1="276.86" y1="149.86" x2="279.4" y2="149.86" width="0.1524" layer="91"/>
<wire x1="195.58" y1="180.34" x2="208.28" y2="180.34" width="0.1524" layer="91"/>
<wire x1="208.28" y1="180.34" x2="208.28" y2="149.86" width="0.1524" layer="91"/>
<junction x="276.86" y="149.86"/>
<wire x1="208.28" y1="149.86" x2="276.86" y2="149.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="MOTOR_VGE" class="0">
<segment>
<label x="302.26" y="187.96" size="1.27" layer="95" xref="yes"/>
<wire x1="298.45" y1="187.96" x2="299.72" y2="187.96" width="0.1524" layer="91"/>
<pinref part="L6" gate="G$1" pin="2"/>
<wire x1="299.72" y1="187.96" x2="302.26" y2="187.96" width="0.1524" layer="91"/>
<wire x1="254" y1="187.96" x2="256.54" y2="187.96" width="0.1524" layer="91"/>
<pinref part="C36" gate="G$1" pin="1"/>
<wire x1="256.54" y1="187.96" x2="261.62" y2="187.96" width="0.1524" layer="91"/>
<wire x1="261.62" y1="187.96" x2="266.7" y2="187.96" width="0.1524" layer="91"/>
<wire x1="266.7" y1="187.96" x2="271.78" y2="187.96" width="0.1524" layer="91"/>
<wire x1="271.78" y1="187.96" x2="276.86" y2="187.96" width="0.1524" layer="91"/>
<wire x1="276.86" y1="187.96" x2="281.94" y2="187.96" width="0.1524" layer="91"/>
<wire x1="281.94" y1="187.96" x2="287.02" y2="187.96" width="0.1524" layer="91"/>
<wire x1="287.02" y1="187.96" x2="289.56" y2="187.96" width="0.1524" layer="91"/>
<wire x1="289.56" y1="187.96" x2="290.83" y2="187.96" width="0.1524" layer="91"/>
<wire x1="256.54" y1="182.88" x2="256.54" y2="187.96" width="0.1524" layer="91"/>
<junction x="256.54" y="187.96"/>
<pinref part="C37" gate="G$1" pin="1"/>
<wire x1="261.62" y1="182.88" x2="261.62" y2="187.96" width="0.1524" layer="91"/>
<junction x="261.62" y="187.96"/>
<pinref part="C39" gate="G$1" pin="1"/>
<wire x1="266.7" y1="182.88" x2="266.7" y2="187.96" width="0.1524" layer="91"/>
<junction x="266.7" y="187.96"/>
<pinref part="C35" gate="G$1" pin="1"/>
<wire x1="271.78" y1="182.88" x2="271.78" y2="187.96" width="0.1524" layer="91"/>
<junction x="271.78" y="187.96"/>
<pinref part="R49" gate="G$1" pin="2"/>
<wire x1="276.86" y1="182.88" x2="276.86" y2="187.96" width="0.1524" layer="91"/>
<junction x="276.86" y="187.96"/>
<pinref part="C34" gate="G$1" pin="2"/>
<wire x1="281.94" y1="182.88" x2="281.94" y2="187.96" width="0.1524" layer="91"/>
<junction x="281.94" y="187.96"/>
<pinref part="C45" gate="G$1" pin="2"/>
<wire x1="287.02" y1="182.88" x2="287.02" y2="187.96" width="0.1524" layer="91"/>
<junction x="287.02" y="187.96"/>
<wire x1="290.83" y1="187.96" x2="298.45" y2="187.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$76" class="0">
<segment>
<pinref part="C25" gate="G$1" pin="1"/>
<pinref part="R43" gate="G$1" pin="2"/>
<wire x1="210.82" y1="182.88" x2="213.36" y2="182.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$78" class="0">
<segment>
<pinref part="R44" gate="G$1" pin="1"/>
<pinref part="C26" gate="G$1" pin="2"/>
<wire x1="226.06" y1="170.18" x2="226.06" y2="162.56" width="0.1524" layer="91"/>
<wire x1="226.06" y1="162.56" x2="228.6" y2="162.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="MOTOR_VIN" class="0">
<segment>
<wire x1="97.79" y1="187.96" x2="96.52" y2="187.96" width="0.1524" layer="91"/>
<label x="83.82" y="187.96" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="R34" gate="G$1" pin="1"/>
<wire x1="96.52" y1="187.96" x2="83.82" y2="187.96" width="0.1524" layer="91"/>
<wire x1="114.3" y1="195.58" x2="111.76" y2="195.58" width="0.1524" layer="91"/>
<wire x1="111.76" y1="195.58" x2="111.76" y2="187.96" width="0.1524" layer="91"/>
<pinref part="L2" gate="G$1" pin="1"/>
<wire x1="114.3" y1="187.96" x2="111.76" y2="187.96" width="0.1524" layer="91"/>
<junction x="111.76" y="187.96"/>
<wire x1="105.41" y1="187.96" x2="106.68" y2="187.96" width="0.1524" layer="91"/>
<wire x1="106.68" y1="187.96" x2="111.76" y2="187.96" width="0.1524" layer="91"/>
<wire x1="97.79" y1="187.96" x2="105.41" y2="187.96" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
<text x="139.7" y="187.96" size="1.27" layer="97">DRIVE OUT -&gt; MP_RXD</text>
<text x="139.7" y="165.1" size="1.27" layer="97">DRIVE IN &lt;- MP_TXD</text>
<text x="139.7" y="137.16" size="1.27" layer="97">DRIVE OUT -&gt; MP_RXD</text>
<text x="139.7" y="114.3" size="1.27" layer="97">DRIVE IN &lt;- MP_TXD</text>
<text x="139.7" y="81.28" size="1.27" layer="97">DRIVE OUT -&gt; MP_RXD</text>
<text x="139.7" y="58.42" size="1.27" layer="97">DRIVE IN &lt;- MP_TXD</text>
<text x="213.36" y="193.04" size="1.27" layer="97">I2C level translator</text>
<text x="299.72" y="193.04" size="1.27" layer="97" rot="MR180">Stepper Motor Driver Side</text>
<text x="238.76" y="190.5" size="1.27" layer="97" rot="MR180">Processor Side</text>
<text x="213.36" y="127" size="1.27" layer="97">I2C level translator</text>
<text x="299.72" y="127" size="1.27" layer="97" rot="MR180">Stepper Motor Driver Side</text>
<text x="238.76" y="124.46" size="1.27" layer="97" rot="MR180">Processor Side</text>
</plain>
<instances>
<instance part="U11" gate="G$1" x="119.38" y="193.04" smashed="yes" rot="MR0">
<attribute name="NAME" x="119.38" y="195.58" size="1.27" layer="95" ratio="15" rot="MR0"/>
<attribute name="VALUE" x="119.38" y="193.548" size="1.27" layer="96" ratio="15" rot="MR0"/>
</instance>
<instance part="R109" gate="G$1" x="63.5" y="182.88" smashed="yes">
<attribute name="NAME" x="56.642" y="183.3626" size="1.27" layer="95"/>
<attribute name="VALUE" x="66.04" y="183.388" size="1.27" layer="96"/>
</instance>
<instance part="R103" gate="G$1" x="63.5" y="175.26" smashed="yes">
<attribute name="NAME" x="56.642" y="175.7426" size="1.27" layer="95"/>
<attribute name="VALUE" x="66.04" y="175.768" size="1.27" layer="96"/>
</instance>
<instance part="R113" gate="G$1" x="132.08" y="185.42" smashed="yes">
<attribute name="NAME" x="124.968" y="185.9026" size="1.27" layer="95"/>
<attribute name="VALUE" x="134.62" y="185.928" size="1.27" layer="96"/>
</instance>
<instance part="R98" gate="G$1" x="132.08" y="170.18" smashed="yes">
<attribute name="NAME" x="124.968" y="170.6626" size="1.27" layer="95"/>
<attribute name="VALUE" x="134.62" y="170.688" size="1.27" layer="96"/>
</instance>
<instance part="C71" gate="G$1" x="45.72" y="165.1" smashed="yes" rot="R90">
<attribute name="NAME" x="44.196" y="163.83" size="1.27" layer="95" ratio="10" rot="R270"/>
<attribute name="VALUE" x="45.212" y="166.37" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C74" gate="G$1" x="50.8" y="165.1" smashed="yes" rot="R90">
<attribute name="NAME" x="49.276" y="163.83" size="1.27" layer="95" ratio="10" rot="R270"/>
<attribute name="VALUE" x="50.292" y="166.37" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C78" gate="G$1" x="83.82" y="195.58" smashed="yes" rot="R180">
<attribute name="NAME" x="78.74" y="196.088" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="85.09" y="196.088" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="SUPPLY1" gate="GND" x="76.2" y="190.5" smashed="yes">
<attribute name="VALUE" x="74.295" y="187.325" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY2" gate="GND" x="43.18" y="154.94" smashed="yes">
<attribute name="VALUE" x="41.275" y="155.575" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="SUPPLY3" gate="GND" x="93.98" y="165.1" smashed="yes">
<attribute name="VALUE" x="97.155" y="164.465" size="1.27" layer="96"/>
</instance>
<instance part="U12" gate="G$1" x="119.38" y="142.24" smashed="yes" rot="MR0">
<attribute name="NAME" x="119.38" y="144.78" size="1.27" layer="95" ratio="15" rot="MR0"/>
<attribute name="VALUE" x="119.38" y="142.748" size="1.27" layer="96" ratio="15" rot="MR0"/>
</instance>
<instance part="R111" gate="G$1" x="63.5" y="132.08" smashed="yes">
<attribute name="NAME" x="56.642" y="132.5626" size="1.27" layer="95"/>
<attribute name="VALUE" x="66.04" y="132.588" size="1.27" layer="96"/>
</instance>
<instance part="R104" gate="G$1" x="63.5" y="124.46" smashed="yes">
<attribute name="NAME" x="56.642" y="124.9426" size="1.27" layer="95"/>
<attribute name="VALUE" x="66.04" y="124.968" size="1.27" layer="96"/>
</instance>
<instance part="R114" gate="G$1" x="132.08" y="134.62" smashed="yes">
<attribute name="NAME" x="124.968" y="135.1026" size="1.27" layer="95"/>
<attribute name="VALUE" x="134.62" y="135.128" size="1.27" layer="96"/>
</instance>
<instance part="R99" gate="G$1" x="132.08" y="119.38" smashed="yes">
<attribute name="NAME" x="124.968" y="119.8626" size="1.27" layer="95"/>
<attribute name="VALUE" x="134.62" y="119.888" size="1.27" layer="96"/>
</instance>
<instance part="C72" gate="G$1" x="45.72" y="114.3" smashed="yes" rot="R90">
<attribute name="NAME" x="44.196" y="113.03" size="1.27" layer="95" ratio="10" rot="R270"/>
<attribute name="VALUE" x="45.212" y="115.57" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C75" gate="G$1" x="50.8" y="114.3" smashed="yes" rot="R90">
<attribute name="NAME" x="49.276" y="113.03" size="1.27" layer="95" ratio="10" rot="R270"/>
<attribute name="VALUE" x="50.292" y="115.57" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C77" gate="G$1" x="83.82" y="144.78" smashed="yes" rot="R180">
<attribute name="NAME" x="78.74" y="145.288" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="85.09" y="145.288" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="SUPPLY7" gate="GND" x="76.2" y="139.7" smashed="yes">
<attribute name="VALUE" x="74.295" y="136.525" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY8" gate="GND" x="43.18" y="104.14" smashed="yes">
<attribute name="VALUE" x="41.275" y="104.775" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="SUPPLY9" gate="GND" x="93.98" y="114.3" smashed="yes">
<attribute name="VALUE" x="97.155" y="113.665" size="1.27" layer="96"/>
</instance>
<instance part="U10" gate="G$1" x="119.38" y="86.36" smashed="yes" rot="MR0">
<attribute name="NAME" x="119.38" y="88.9" size="1.27" layer="95" ratio="15" rot="MR0"/>
<attribute name="VALUE" x="119.38" y="86.868" size="1.27" layer="96" ratio="15" rot="MR0"/>
</instance>
<instance part="R106" gate="G$1" x="63.5" y="76.2" smashed="yes">
<attribute name="NAME" x="56.642" y="76.6826" size="1.27" layer="95"/>
<attribute name="VALUE" x="66.04" y="76.708" size="1.27" layer="96"/>
</instance>
<instance part="R101" gate="G$1" x="63.5" y="68.58" smashed="yes">
<attribute name="NAME" x="56.642" y="69.0626" size="1.27" layer="95"/>
<attribute name="VALUE" x="66.04" y="69.088" size="1.27" layer="96"/>
</instance>
<instance part="R112" gate="G$1" x="132.08" y="78.74" smashed="yes">
<attribute name="NAME" x="124.968" y="79.2226" size="1.27" layer="95"/>
<attribute name="VALUE" x="134.62" y="79.248" size="1.27" layer="96"/>
</instance>
<instance part="R97" gate="G$1" x="132.08" y="63.5" smashed="yes">
<attribute name="NAME" x="124.968" y="63.9826" size="1.27" layer="95"/>
<attribute name="VALUE" x="134.62" y="64.008" size="1.27" layer="96"/>
</instance>
<instance part="C70" gate="G$1" x="45.72" y="58.42" smashed="yes" rot="R90">
<attribute name="NAME" x="44.196" y="57.15" size="1.27" layer="95" ratio="10" rot="R270"/>
<attribute name="VALUE" x="45.212" y="59.69" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C73" gate="G$1" x="50.8" y="58.42" smashed="yes" rot="R90">
<attribute name="NAME" x="49.276" y="57.15" size="1.27" layer="95" ratio="10" rot="R270"/>
<attribute name="VALUE" x="50.292" y="59.69" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C76" gate="G$1" x="83.82" y="88.9" smashed="yes" rot="R180">
<attribute name="NAME" x="78.74" y="89.408" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="85.09" y="89.408" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="SUPPLY4" gate="GND" x="76.2" y="83.82" smashed="yes">
<attribute name="VALUE" x="74.295" y="80.645" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY5" gate="GND" x="43.18" y="48.26" smashed="yes">
<attribute name="VALUE" x="41.275" y="48.895" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="SUPPLY6" gate="GND" x="93.98" y="58.42" smashed="yes">
<attribute name="VALUE" x="97.155" y="57.785" size="1.27" layer="96"/>
</instance>
<instance part="C50" gate="G$1" x="304.8" y="165.1" smashed="yes" rot="MR270">
<attribute name="NAME" x="305.308" y="166.37" size="1.27" layer="95" ratio="10" rot="MR90"/>
<attribute name="VALUE" x="306.324" y="163.83" size="1.27" layer="96" ratio="10" rot="MR270"/>
</instance>
<instance part="C55" gate="G$1" x="259.08" y="180.34" smashed="yes" rot="MR90">
<attribute name="NAME" x="257.556" y="181.356" size="1.27" layer="95" ratio="10" rot="MR90"/>
<attribute name="VALUE" x="258.572" y="179.07" size="1.27" layer="96" ratio="10" rot="MR270"/>
</instance>
<instance part="C49" gate="G$1" x="309.88" y="165.1" smashed="yes" rot="MR270">
<attribute name="NAME" x="310.388" y="166.37" size="1.27" layer="95" ratio="10" rot="MR90"/>
<attribute name="VALUE" x="311.404" y="163.83" size="1.27" layer="96" ratio="10" rot="MR270"/>
</instance>
<instance part="C57" gate="G$1" x="251.46" y="180.34" smashed="yes" rot="MR90">
<attribute name="NAME" x="250.952" y="179.07" size="1.27" layer="95" ratio="10" rot="MR270"/>
<attribute name="VALUE" x="249.936" y="181.356" size="1.27" layer="96" ratio="10" rot="MR90"/>
</instance>
<instance part="R62" gate="G$1" x="243.84" y="175.26" smashed="yes" rot="MR90">
<attribute name="NAME" x="243.6114" y="172.72" size="1.27" layer="95" rot="MR270"/>
<attribute name="VALUE" x="242.316" y="178.054" size="1.27" layer="96" rot="MR90"/>
</instance>
<instance part="R56" gate="G$1" x="236.22" y="175.26" smashed="yes" rot="MR90">
<attribute name="NAME" x="235.9914" y="172.72" size="1.27" layer="95" rot="MR270"/>
<attribute name="VALUE" x="234.696" y="178.054" size="1.27" layer="96" rot="MR90"/>
</instance>
<instance part="R69" gate="G$1" x="320.04" y="165.1" smashed="yes" rot="MR270">
<attribute name="NAME" x="320.5226" y="167.64" size="1.27" layer="95" rot="MR90"/>
<attribute name="VALUE" x="321.564" y="162.052" size="1.27" layer="96" rot="MR270"/>
</instance>
<instance part="R64" gate="G$1" x="327.66" y="165.1" smashed="yes" rot="MR270">
<attribute name="NAME" x="328.1426" y="167.64" size="1.27" layer="95" rot="MR90"/>
<attribute name="VALUE" x="329.184" y="162.052" size="1.27" layer="96" rot="MR270"/>
</instance>
<instance part="SUPPLY35" gate="GND" x="314.96" y="172.72" smashed="yes">
<attribute name="VALUE" x="313.309" y="170.561" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="SUPPLY36" gate="GND" x="248.92" y="167.64" smashed="yes">
<attribute name="VALUE" x="247.015" y="164.465" size="1.27" layer="96"/>
</instance>
<instance part="R68" gate="G$1" x="228.6" y="175.26" smashed="yes" rot="MR90">
<attribute name="NAME" x="228.3714" y="172.72" size="1.27" layer="95" rot="MR270"/>
<attribute name="VALUE" x="227.076" y="178.054" size="1.27" layer="96" rot="MR90"/>
</instance>
<instance part="SUPPLY37" gate="GND" x="266.7" y="170.18" smashed="yes">
<attribute name="VALUE" x="266.065" y="171.069" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="TP14" gate="G$1" x="330.2" y="195.58" smashed="yes">
<attribute name="NAME" x="326.39" y="199.39" size="1.27" layer="95"/>
</instance>
<instance part="TP11" gate="G$1" x="332.74" y="195.58" smashed="yes">
<attribute name="NAME" x="331.47" y="199.39" size="1.27" layer="95"/>
</instance>
<instance part="TP16" gate="G$1" x="335.28" y="195.58" smashed="yes">
<attribute name="NAME" x="336.55" y="199.39" size="1.27" layer="95"/>
</instance>
<instance part="SUPPLY38" gate="GND" x="335.28" y="190.5" smashed="yes">
<attribute name="VALUE" x="333.375" y="187.325" size="1.27" layer="96"/>
</instance>
<instance part="TP10" gate="G$1" x="218.44" y="152.4" smashed="yes" rot="R90">
<attribute name="NAME" x="215.646" y="153.162" size="1.27" layer="95" rot="R180"/>
</instance>
<instance part="U6" gate="G$1" x="274.32" y="182.88" smashed="yes">
<attribute name="NAME" x="274.32" y="187.96" size="1.27" layer="95" ratio="15"/>
<attribute name="VALUE" x="274.32" y="190.5" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="TP13" gate="G$1" x="220.98" y="170.18" smashed="yes">
<attribute name="NAME" x="221.742" y="172.974" size="1.27" layer="95" rot="R90"/>
</instance>
<instance part="TP12" gate="G$1" x="223.52" y="170.18" smashed="yes">
<attribute name="NAME" x="224.282" y="172.974" size="1.27" layer="95" rot="R90"/>
</instance>
<instance part="C106" gate="G$1" x="304.8" y="99.06" smashed="yes" rot="MR270">
<attribute name="NAME" x="305.308" y="100.33" size="1.27" layer="95" ratio="10" rot="MR90"/>
<attribute name="VALUE" x="306.324" y="97.79" size="1.27" layer="96" ratio="10" rot="MR270"/>
</instance>
<instance part="C105" gate="G$1" x="259.08" y="114.3" smashed="yes" rot="MR90">
<attribute name="NAME" x="257.556" y="115.316" size="1.27" layer="95" ratio="10" rot="MR90"/>
<attribute name="VALUE" x="258.572" y="113.03" size="1.27" layer="96" ratio="10" rot="MR270"/>
</instance>
<instance part="C107" gate="G$1" x="309.88" y="99.06" smashed="yes" rot="MR270">
<attribute name="NAME" x="310.388" y="100.33" size="1.27" layer="95" ratio="10" rot="MR90"/>
<attribute name="VALUE" x="311.404" y="97.79" size="1.27" layer="96" ratio="10" rot="MR270"/>
</instance>
<instance part="C104" gate="G$1" x="251.46" y="114.3" smashed="yes" rot="MR90">
<attribute name="NAME" x="250.952" y="113.03" size="1.27" layer="95" ratio="10" rot="MR270"/>
<attribute name="VALUE" x="249.936" y="115.316" size="1.27" layer="96" ratio="10" rot="MR90"/>
</instance>
<instance part="R157" gate="G$1" x="243.84" y="109.22" smashed="yes" rot="MR90">
<attribute name="NAME" x="243.6114" y="106.68" size="1.27" layer="95" rot="MR270"/>
<attribute name="VALUE" x="242.316" y="112.014" size="1.27" layer="96" rot="MR90"/>
</instance>
<instance part="R158" gate="G$1" x="236.22" y="109.22" smashed="yes" rot="MR90">
<attribute name="NAME" x="235.9914" y="106.68" size="1.27" layer="95" rot="MR270"/>
<attribute name="VALUE" x="234.696" y="112.014" size="1.27" layer="96" rot="MR90"/>
</instance>
<instance part="R154" gate="G$1" x="320.04" y="99.06" smashed="yes" rot="MR270">
<attribute name="NAME" x="320.5226" y="101.6" size="1.27" layer="95" rot="MR90"/>
<attribute name="VALUE" x="321.564" y="96.012" size="1.27" layer="96" rot="MR270"/>
</instance>
<instance part="R156" gate="G$1" x="327.66" y="99.06" smashed="yes" rot="MR270">
<attribute name="NAME" x="328.1426" y="101.6" size="1.27" layer="95" rot="MR90"/>
<attribute name="VALUE" x="329.184" y="96.012" size="1.27" layer="96" rot="MR270"/>
</instance>
<instance part="SUPPLY40" gate="GND" x="314.96" y="106.68" smashed="yes">
<attribute name="VALUE" x="313.309" y="104.521" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="SUPPLY41" gate="GND" x="248.92" y="101.6" smashed="yes">
<attribute name="VALUE" x="247.015" y="98.425" size="1.27" layer="96"/>
</instance>
<instance part="R155" gate="G$1" x="228.6" y="109.22" smashed="yes" rot="MR90">
<attribute name="NAME" x="228.3714" y="106.68" size="1.27" layer="95" rot="MR270"/>
<attribute name="VALUE" x="227.076" y="112.014" size="1.27" layer="96" rot="MR90"/>
</instance>
<instance part="SUPPLY42" gate="GND" x="266.7" y="104.14" smashed="yes">
<attribute name="VALUE" x="266.065" y="105.029" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="TP35" gate="G$1" x="330.2" y="129.54" smashed="yes">
<attribute name="NAME" x="326.39" y="133.35" size="1.27" layer="95"/>
</instance>
<instance part="TP36" gate="G$1" x="332.74" y="129.54" smashed="yes">
<attribute name="NAME" x="331.47" y="133.35" size="1.27" layer="95"/>
</instance>
<instance part="TP34" gate="G$1" x="335.28" y="129.54" smashed="yes">
<attribute name="NAME" x="336.55" y="133.35" size="1.27" layer="95"/>
</instance>
<instance part="SUPPLY63" gate="GND" x="335.28" y="124.46" smashed="yes">
<attribute name="VALUE" x="333.375" y="121.285" size="1.27" layer="96"/>
</instance>
<instance part="TP37" gate="G$1" x="218.44" y="86.36" smashed="yes" rot="R90">
<attribute name="NAME" x="215.646" y="87.122" size="1.27" layer="95" rot="R180"/>
</instance>
<instance part="U15" gate="G$1" x="274.32" y="116.84" smashed="yes">
<attribute name="NAME" x="274.32" y="121.92" size="1.27" layer="95" ratio="15"/>
<attribute name="VALUE" x="274.32" y="124.46" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="FRAME3" gate="G$1" x="0" y="0" smashed="yes">
<attribute name="DRAWING_NAME" x="344.17" y="15.24" size="2.54" layer="94"/>
<attribute name="LAST_DATE_TIME" x="344.17" y="10.16" size="2.286" layer="94"/>
<attribute name="SHEET" x="357.505" y="5.08" size="2.54" layer="94"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="C78" gate="G$1" pin="2"/>
<wire x1="78.74" y1="195.58" x2="76.2" y2="195.58" width="0.1524" layer="91"/>
<pinref part="SUPPLY1" gate="GND" pin="GND"/>
<wire x1="76.2" y1="195.58" x2="76.2" y2="193.04" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C71" gate="G$1" pin="1"/>
<pinref part="SUPPLY2" gate="GND" pin="GND"/>
<wire x1="45.72" y1="160.02" x2="45.72" y2="157.48" width="0.1524" layer="91"/>
<pinref part="C74" gate="G$1" pin="1"/>
<wire x1="45.72" y1="157.48" x2="43.18" y2="157.48" width="0.1524" layer="91"/>
<wire x1="50.8" y1="160.02" x2="50.8" y2="157.48" width="0.1524" layer="91"/>
<wire x1="50.8" y1="157.48" x2="45.72" y2="157.48" width="0.1524" layer="91"/>
<junction x="45.72" y="157.48"/>
</segment>
<segment>
<pinref part="U11" gate="G$1" pin="5"/>
<pinref part="SUPPLY3" gate="GND" pin="GND"/>
<wire x1="96.52" y1="170.18" x2="93.98" y2="170.18" width="0.1524" layer="91"/>
<wire x1="93.98" y1="170.18" x2="93.98" y2="167.64" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C77" gate="G$1" pin="2"/>
<wire x1="78.74" y1="144.78" x2="76.2" y2="144.78" width="0.1524" layer="91"/>
<pinref part="SUPPLY7" gate="GND" pin="GND"/>
<wire x1="76.2" y1="144.78" x2="76.2" y2="142.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C72" gate="G$1" pin="1"/>
<pinref part="SUPPLY8" gate="GND" pin="GND"/>
<wire x1="45.72" y1="109.22" x2="45.72" y2="106.68" width="0.1524" layer="91"/>
<pinref part="C75" gate="G$1" pin="1"/>
<wire x1="45.72" y1="106.68" x2="43.18" y2="106.68" width="0.1524" layer="91"/>
<wire x1="50.8" y1="109.22" x2="50.8" y2="106.68" width="0.1524" layer="91"/>
<wire x1="50.8" y1="106.68" x2="45.72" y2="106.68" width="0.1524" layer="91"/>
<junction x="45.72" y="106.68"/>
</segment>
<segment>
<pinref part="U12" gate="G$1" pin="5"/>
<pinref part="SUPPLY9" gate="GND" pin="GND"/>
<wire x1="96.52" y1="119.38" x2="93.98" y2="119.38" width="0.1524" layer="91"/>
<wire x1="93.98" y1="119.38" x2="93.98" y2="116.84" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C76" gate="G$1" pin="2"/>
<wire x1="78.74" y1="88.9" x2="76.2" y2="88.9" width="0.1524" layer="91"/>
<pinref part="SUPPLY4" gate="GND" pin="GND"/>
<wire x1="76.2" y1="88.9" x2="76.2" y2="86.36" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C70" gate="G$1" pin="1"/>
<pinref part="SUPPLY5" gate="GND" pin="GND"/>
<wire x1="45.72" y1="53.34" x2="45.72" y2="50.8" width="0.1524" layer="91"/>
<pinref part="C73" gate="G$1" pin="1"/>
<wire x1="45.72" y1="50.8" x2="43.18" y2="50.8" width="0.1524" layer="91"/>
<wire x1="50.8" y1="53.34" x2="50.8" y2="50.8" width="0.1524" layer="91"/>
<wire x1="50.8" y1="50.8" x2="45.72" y2="50.8" width="0.1524" layer="91"/>
<junction x="45.72" y="50.8"/>
</segment>
<segment>
<pinref part="U10" gate="G$1" pin="5"/>
<pinref part="SUPPLY6" gate="GND" pin="GND"/>
<wire x1="96.52" y1="63.5" x2="93.98" y2="63.5" width="0.1524" layer="91"/>
<wire x1="93.98" y1="63.5" x2="93.98" y2="60.96" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C50" gate="G$1" pin="1"/>
<wire x1="304.8" y1="170.18" x2="304.8" y2="175.26" width="0.1524" layer="91"/>
<wire x1="304.8" y1="175.26" x2="309.88" y2="175.26" width="0.1524" layer="91"/>
<wire x1="309.88" y1="175.26" x2="314.96" y2="175.26" width="0.1524" layer="91"/>
<pinref part="C49" gate="G$1" pin="1"/>
<wire x1="309.88" y1="170.18" x2="309.88" y2="175.26" width="0.1524" layer="91"/>
<junction x="309.88" y="175.26"/>
<pinref part="SUPPLY35" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C55" gate="G$1" pin="1"/>
<wire x1="259.08" y1="175.26" x2="259.08" y2="172.72" width="0.1524" layer="91"/>
<wire x1="259.08" y1="172.72" x2="251.46" y2="172.72" width="0.1524" layer="91"/>
<wire x1="251.46" y1="172.72" x2="248.92" y2="172.72" width="0.1524" layer="91"/>
<wire x1="248.92" y1="172.72" x2="248.92" y2="170.18" width="0.1524" layer="91"/>
<pinref part="C57" gate="G$1" pin="1"/>
<wire x1="251.46" y1="175.26" x2="251.46" y2="172.72" width="0.1524" layer="91"/>
<junction x="251.46" y="172.72"/>
<pinref part="SUPPLY36" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="TP16" gate="G$1" pin="PP"/>
<pinref part="SUPPLY38" gate="GND" pin="GND"/>
<wire x1="335.28" y1="195.58" x2="335.28" y2="193.04" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUPPLY37" gate="GND" pin="GND"/>
<pinref part="U6" gate="G$1" pin="2"/>
<wire x1="266.7" y1="172.72" x2="269.24" y2="172.72" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C106" gate="G$1" pin="1"/>
<wire x1="304.8" y1="104.14" x2="304.8" y2="109.22" width="0.1524" layer="91"/>
<wire x1="304.8" y1="109.22" x2="309.88" y2="109.22" width="0.1524" layer="91"/>
<wire x1="309.88" y1="109.22" x2="314.96" y2="109.22" width="0.1524" layer="91"/>
<pinref part="C107" gate="G$1" pin="1"/>
<wire x1="309.88" y1="104.14" x2="309.88" y2="109.22" width="0.1524" layer="91"/>
<junction x="309.88" y="109.22"/>
<pinref part="SUPPLY40" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C105" gate="G$1" pin="1"/>
<wire x1="259.08" y1="109.22" x2="259.08" y2="106.68" width="0.1524" layer="91"/>
<wire x1="259.08" y1="106.68" x2="251.46" y2="106.68" width="0.1524" layer="91"/>
<wire x1="251.46" y1="106.68" x2="248.92" y2="106.68" width="0.1524" layer="91"/>
<wire x1="248.92" y1="106.68" x2="248.92" y2="104.14" width="0.1524" layer="91"/>
<pinref part="C104" gate="G$1" pin="1"/>
<wire x1="251.46" y1="109.22" x2="251.46" y2="106.68" width="0.1524" layer="91"/>
<junction x="251.46" y="106.68"/>
<pinref part="SUPPLY41" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="TP34" gate="G$1" pin="PP"/>
<pinref part="SUPPLY63" gate="GND" pin="GND"/>
<wire x1="335.28" y1="129.54" x2="335.28" y2="127" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUPPLY42" gate="GND" pin="GND"/>
<pinref part="U15" gate="G$1" pin="2"/>
<wire x1="266.7" y1="106.68" x2="269.24" y2="106.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="R109" gate="G$1" pin="2"/>
<wire x1="68.58" y1="182.88" x2="71.12" y2="182.88" width="0.1524" layer="91"/>
<wire x1="71.12" y1="182.88" x2="71.12" y2="180.34" width="0.1524" layer="91"/>
<pinref part="U11" gate="G$1" pin="7"/>
<wire x1="71.12" y1="180.34" x2="96.52" y2="180.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="6"/>
<wire x1="68.58" y1="175.26" x2="96.52" y2="175.26" width="0.1524" layer="91"/>
<pinref part="R103" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="1"/>
<pinref part="R113" gate="G$1" pin="1"/>
<wire x1="121.92" y1="185.42" x2="127" y2="185.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="4"/>
<pinref part="R98" gate="G$1" pin="1"/>
<wire x1="121.92" y1="170.18" x2="127" y2="170.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$32" class="0">
<segment>
<pinref part="R111" gate="G$1" pin="2"/>
<wire x1="68.58" y1="132.08" x2="71.12" y2="132.08" width="0.1524" layer="91"/>
<wire x1="71.12" y1="132.08" x2="71.12" y2="129.54" width="0.1524" layer="91"/>
<pinref part="U12" gate="G$1" pin="7"/>
<wire x1="71.12" y1="129.54" x2="96.52" y2="129.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$33" class="0">
<segment>
<pinref part="U12" gate="G$1" pin="6"/>
<wire x1="68.58" y1="124.46" x2="96.52" y2="124.46" width="0.1524" layer="91"/>
<pinref part="R104" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$34" class="0">
<segment>
<pinref part="U12" gate="G$1" pin="1"/>
<pinref part="R114" gate="G$1" pin="1"/>
<wire x1="121.92" y1="134.62" x2="127" y2="134.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$35" class="0">
<segment>
<pinref part="U12" gate="G$1" pin="4"/>
<pinref part="R99" gate="G$1" pin="1"/>
<wire x1="121.92" y1="119.38" x2="127" y2="119.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$28" class="0">
<segment>
<pinref part="R106" gate="G$1" pin="2"/>
<wire x1="68.58" y1="76.2" x2="71.12" y2="76.2" width="0.1524" layer="91"/>
<wire x1="71.12" y1="76.2" x2="71.12" y2="73.66" width="0.1524" layer="91"/>
<pinref part="U10" gate="G$1" pin="7"/>
<wire x1="71.12" y1="73.66" x2="96.52" y2="73.66" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$29" class="0">
<segment>
<pinref part="U10" gate="G$1" pin="6"/>
<wire x1="68.58" y1="68.58" x2="96.52" y2="68.58" width="0.1524" layer="91"/>
<pinref part="R101" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$30" class="0">
<segment>
<pinref part="U10" gate="G$1" pin="1"/>
<pinref part="R112" gate="G$1" pin="1"/>
<wire x1="121.92" y1="78.74" x2="127" y2="78.74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$31" class="0">
<segment>
<pinref part="U10" gate="G$1" pin="4"/>
<pinref part="R97" gate="G$1" pin="1"/>
<wire x1="121.92" y1="63.5" x2="127" y2="63.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SCL2_5V" class="0">
<segment>
<pinref part="R156" gate="G$1" pin="1"/>
<wire x1="327.66" y1="111.76" x2="332.74" y2="111.76" width="0.1524" layer="91"/>
<wire x1="327.66" y1="104.14" x2="327.66" y2="111.76" width="0.1524" layer="91"/>
<label x="340.36" y="111.76" size="1.27" layer="95" xref="yes"/>
<pinref part="TP36" gate="G$1" pin="PP"/>
<wire x1="332.74" y1="129.54" x2="332.74" y2="111.76" width="0.1524" layer="91"/>
<wire x1="327.66" y1="111.76" x2="294.64" y2="111.76" width="0.1524" layer="91"/>
<junction x="327.66" y="111.76"/>
<wire x1="332.74" y1="111.76" x2="340.36" y2="111.76" width="0.1524" layer="91"/>
<junction x="332.74" y="111.76"/>
<pinref part="U15" gate="G$1" pin="8"/>
</segment>
</net>
<net name="SDA2_5V" class="0">
<segment>
<pinref part="R154" gate="G$1" pin="1"/>
<wire x1="320.04" y1="119.38" x2="330.2" y2="119.38" width="0.1524" layer="91"/>
<wire x1="320.04" y1="104.14" x2="320.04" y2="119.38" width="0.1524" layer="91"/>
<label x="340.36" y="119.38" size="1.27" layer="95" xref="yes"/>
<pinref part="TP35" gate="G$1" pin="PP"/>
<wire x1="330.2" y1="129.54" x2="330.2" y2="119.38" width="0.1524" layer="91"/>
<wire x1="330.2" y1="119.38" x2="340.36" y2="119.38" width="0.1524" layer="91"/>
<junction x="330.2" y="119.38"/>
<junction x="320.04" y="119.38"/>
<wire x1="266.7" y1="119.38" x2="320.04" y2="119.38" width="0.1524" layer="91"/>
<pinref part="U15" gate="G$1" pin="1"/>
<wire x1="269.24" y1="111.76" x2="266.7" y2="111.76" width="0.1524" layer="91"/>
<wire x1="266.7" y1="111.76" x2="266.7" y2="119.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$41" class="0">
<segment>
<pinref part="R56" gate="G$1" pin="1"/>
<wire x1="236.22" y1="152.4" x2="218.44" y2="152.4" width="0.1524" layer="91"/>
<wire x1="236.22" y1="170.18" x2="236.22" y2="152.4" width="0.1524" layer="91"/>
<wire x1="297.18" y1="152.4" x2="236.22" y2="152.4" width="0.1524" layer="91"/>
<junction x="236.22" y="152.4"/>
<pinref part="TP10" gate="G$1" pin="PP"/>
<pinref part="U6" gate="G$1" pin="6"/>
<wire x1="294.64" y1="167.64" x2="297.18" y2="167.64" width="0.1524" layer="91"/>
<wire x1="297.18" y1="167.64" x2="297.18" y2="152.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SCL_A" class="0">
<segment>
<pinref part="R62" gate="G$1" pin="1"/>
<wire x1="243.84" y1="170.18" x2="243.84" y2="157.48" width="0.1524" layer="91"/>
<wire x1="218.44" y1="157.48" x2="223.52" y2="157.48" width="0.1524" layer="91"/>
<label x="218.44" y="157.48" size="1.27" layer="95" rot="R180" xref="yes"/>
<wire x1="223.52" y1="157.48" x2="243.84" y2="157.48" width="0.1524" layer="91"/>
<wire x1="243.84" y1="157.48" x2="294.64" y2="157.48" width="0.1524" layer="91"/>
<junction x="243.84" y="157.48"/>
<pinref part="U6" gate="G$1" pin="5"/>
<wire x1="294.64" y1="157.48" x2="294.64" y2="162.56" width="0.1524" layer="91"/>
<pinref part="TP12" gate="G$1" pin="PP"/>
<wire x1="223.52" y1="170.18" x2="223.52" y2="157.48" width="0.1524" layer="91"/>
<junction x="223.52" y="157.48"/>
</segment>
</net>
<net name="SDA_A" class="0">
<segment>
<pinref part="R68" gate="G$1" pin="1"/>
<wire x1="228.6" y1="170.18" x2="228.6" y2="162.56" width="0.1524" layer="91"/>
<wire x1="228.6" y1="162.56" x2="220.98" y2="162.56" width="0.1524" layer="91"/>
<label x="218.44" y="162.56" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="U6" gate="G$1" pin="4"/>
<wire x1="220.98" y1="162.56" x2="218.44" y2="162.56" width="0.1524" layer="91"/>
<wire x1="228.6" y1="162.56" x2="269.24" y2="162.56" width="0.1524" layer="91"/>
<junction x="228.6" y="162.56"/>
<pinref part="TP13" gate="G$1" pin="PP"/>
<wire x1="220.98" y1="170.18" x2="220.98" y2="162.56" width="0.1524" layer="91"/>
<junction x="220.98" y="162.56"/>
</segment>
</net>
<net name="INTERFACE_1_RS485_N" class="0">
<segment>
<pinref part="R109" gate="G$1" pin="1"/>
<wire x1="58.42" y1="182.88" x2="53.34" y2="182.88" width="0.1524" layer="91"/>
<wire x1="53.34" y1="182.88" x2="53.34" y2="177.8" width="0.1524" layer="91"/>
<wire x1="53.34" y1="177.8" x2="50.8" y2="177.8" width="0.1524" layer="91"/>
<pinref part="C74" gate="G$1" pin="2"/>
<wire x1="50.8" y1="177.8" x2="43.18" y2="177.8" width="0.1524" layer="91"/>
<wire x1="50.8" y1="170.18" x2="50.8" y2="177.8" width="0.1524" layer="91"/>
<junction x="50.8" y="177.8"/>
<label x="43.18" y="177.8" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="INTERFACE_1_RS485_P" class="0">
<segment>
<pinref part="R103" gate="G$1" pin="1"/>
<wire x1="58.42" y1="175.26" x2="45.72" y2="175.26" width="0.1524" layer="91"/>
<pinref part="C71" gate="G$1" pin="2"/>
<wire x1="45.72" y1="175.26" x2="43.18" y2="175.26" width="0.1524" layer="91"/>
<wire x1="45.72" y1="170.18" x2="45.72" y2="175.26" width="0.1524" layer="91"/>
<junction x="45.72" y="175.26"/>
<label x="43.18" y="175.26" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="INTERFACE_2_RS485_P" class="0">
<segment>
<pinref part="R104" gate="G$1" pin="1"/>
<wire x1="58.42" y1="124.46" x2="45.72" y2="124.46" width="0.1524" layer="91"/>
<pinref part="C72" gate="G$1" pin="2"/>
<wire x1="45.72" y1="124.46" x2="43.18" y2="124.46" width="0.1524" layer="91"/>
<wire x1="45.72" y1="119.38" x2="45.72" y2="124.46" width="0.1524" layer="91"/>
<junction x="45.72" y="124.46"/>
<label x="43.18" y="124.46" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="INTERFACE_2_RS485_N" class="0">
<segment>
<pinref part="R111" gate="G$1" pin="1"/>
<wire x1="58.42" y1="132.08" x2="53.34" y2="132.08" width="0.1524" layer="91"/>
<wire x1="53.34" y1="132.08" x2="53.34" y2="127" width="0.1524" layer="91"/>
<wire x1="53.34" y1="127" x2="50.8" y2="127" width="0.1524" layer="91"/>
<pinref part="C75" gate="G$1" pin="2"/>
<wire x1="50.8" y1="127" x2="43.18" y2="127" width="0.1524" layer="91"/>
<wire x1="50.8" y1="119.38" x2="50.8" y2="127" width="0.1524" layer="91"/>
<junction x="50.8" y="127"/>
<label x="43.18" y="127" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="INTERFACE_3_RS485_N" class="0">
<segment>
<pinref part="R106" gate="G$1" pin="1"/>
<wire x1="58.42" y1="76.2" x2="53.34" y2="76.2" width="0.1524" layer="91"/>
<wire x1="53.34" y1="76.2" x2="53.34" y2="71.12" width="0.1524" layer="91"/>
<wire x1="53.34" y1="71.12" x2="50.8" y2="71.12" width="0.1524" layer="91"/>
<pinref part="C73" gate="G$1" pin="2"/>
<wire x1="50.8" y1="71.12" x2="43.18" y2="71.12" width="0.1524" layer="91"/>
<wire x1="50.8" y1="63.5" x2="50.8" y2="71.12" width="0.1524" layer="91"/>
<junction x="50.8" y="71.12"/>
<label x="43.18" y="71.12" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="INTERFACE_3_RS485_P" class="0">
<segment>
<pinref part="R101" gate="G$1" pin="1"/>
<wire x1="58.42" y1="68.58" x2="45.72" y2="68.58" width="0.1524" layer="91"/>
<pinref part="C70" gate="G$1" pin="2"/>
<wire x1="45.72" y1="68.58" x2="43.18" y2="68.58" width="0.1524" layer="91"/>
<wire x1="45.72" y1="63.5" x2="45.72" y2="68.58" width="0.1524" layer="91"/>
<junction x="45.72" y="68.58"/>
<label x="43.18" y="68.58" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="COM1_RXD_IN" class="0">
<segment>
<pinref part="R112" gate="G$1" pin="2"/>
<wire x1="137.16" y1="78.74" x2="144.78" y2="78.74" width="0.1524" layer="91"/>
<label x="144.78" y="78.74" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="COM1_EN" class="0">
<segment>
<pinref part="U10" gate="G$1" pin="3"/>
<wire x1="121.92" y1="68.58" x2="127" y2="68.58" width="0.1524" layer="91"/>
<pinref part="U10" gate="G$1" pin="2"/>
<wire x1="127" y1="68.58" x2="144.78" y2="68.58" width="0.1524" layer="91"/>
<wire x1="121.92" y1="73.66" x2="127" y2="73.66" width="0.1524" layer="91"/>
<wire x1="127" y1="73.66" x2="127" y2="68.58" width="0.1524" layer="91"/>
<junction x="127" y="68.58"/>
<label x="144.78" y="68.58" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="COM1_TXD_OUT" class="0">
<segment>
<pinref part="R97" gate="G$1" pin="2"/>
<wire x1="137.16" y1="63.5" x2="144.78" y2="63.5" width="0.1524" layer="91"/>
<label x="144.78" y="63.5" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="8"/>
<wire x1="96.52" y1="185.42" x2="91.44" y2="185.42" width="0.1524" layer="91"/>
<wire x1="91.44" y1="185.42" x2="91.44" y2="195.58" width="0.1524" layer="91"/>
<pinref part="C78" gate="G$1" pin="1"/>
<wire x1="88.9" y1="195.58" x2="91.44" y2="195.58" width="0.1524" layer="91"/>
<label x="88.9" y="203.2" size="1.27" layer="95" rot="R180" xref="yes"/>
<wire x1="88.9" y1="203.2" x2="91.44" y2="203.2" width="0.1524" layer="91"/>
<wire x1="91.44" y1="203.2" x2="91.44" y2="195.58" width="0.1524" layer="91"/>
<junction x="91.44" y="195.58"/>
</segment>
<segment>
<pinref part="U12" gate="G$1" pin="8"/>
<wire x1="96.52" y1="134.62" x2="91.44" y2="134.62" width="0.1524" layer="91"/>
<wire x1="91.44" y1="134.62" x2="91.44" y2="144.78" width="0.1524" layer="91"/>
<pinref part="C77" gate="G$1" pin="1"/>
<wire x1="88.9" y1="144.78" x2="91.44" y2="144.78" width="0.1524" layer="91"/>
<label x="88.9" y="149.86" size="1.27" layer="95" rot="R180" xref="yes"/>
<wire x1="88.9" y1="149.86" x2="91.44" y2="149.86" width="0.1524" layer="91"/>
<wire x1="91.44" y1="149.86" x2="91.44" y2="144.78" width="0.1524" layer="91"/>
<junction x="91.44" y="144.78"/>
</segment>
<segment>
<pinref part="U10" gate="G$1" pin="8"/>
<wire x1="96.52" y1="78.74" x2="91.44" y2="78.74" width="0.1524" layer="91"/>
<wire x1="91.44" y1="78.74" x2="91.44" y2="88.9" width="0.1524" layer="91"/>
<pinref part="C76" gate="G$1" pin="1"/>
<wire x1="88.9" y1="88.9" x2="91.44" y2="88.9" width="0.1524" layer="91"/>
<label x="88.9" y="93.98" size="1.27" layer="95" rot="R180" xref="yes"/>
<wire x1="88.9" y1="93.98" x2="91.44" y2="93.98" width="0.1524" layer="91"/>
<wire x1="91.44" y1="93.98" x2="91.44" y2="88.9" width="0.1524" layer="91"/>
<junction x="91.44" y="88.9"/>
</segment>
<segment>
<pinref part="C55" gate="G$1" pin="2"/>
<wire x1="264.16" y1="187.96" x2="259.08" y2="187.96" width="0.1524" layer="91"/>
<wire x1="259.08" y1="187.96" x2="251.46" y2="187.96" width="0.1524" layer="91"/>
<wire x1="251.46" y1="187.96" x2="243.84" y2="187.96" width="0.1524" layer="91"/>
<wire x1="243.84" y1="187.96" x2="236.22" y2="187.96" width="0.1524" layer="91"/>
<wire x1="236.22" y1="187.96" x2="228.6" y2="187.96" width="0.1524" layer="91"/>
<wire x1="228.6" y1="187.96" x2="218.44" y2="187.96" width="0.1524" layer="91"/>
<wire x1="259.08" y1="185.42" x2="259.08" y2="187.96" width="0.1524" layer="91"/>
<pinref part="C57" gate="G$1" pin="2"/>
<wire x1="251.46" y1="185.42" x2="251.46" y2="187.96" width="0.1524" layer="91"/>
<pinref part="R62" gate="G$1" pin="2"/>
<wire x1="243.84" y1="180.34" x2="243.84" y2="187.96" width="0.1524" layer="91"/>
<pinref part="R56" gate="G$1" pin="2"/>
<wire x1="236.22" y1="180.34" x2="236.22" y2="187.96" width="0.1524" layer="91"/>
<junction x="259.08" y="187.96"/>
<junction x="251.46" y="187.96"/>
<junction x="243.84" y="187.96"/>
<junction x="236.22" y="187.96"/>
<label x="218.44" y="187.96" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="R68" gate="G$1" pin="2"/>
<wire x1="228.6" y1="180.34" x2="228.6" y2="187.96" width="0.1524" layer="91"/>
<junction x="228.6" y="187.96"/>
<pinref part="U6" gate="G$1" pin="3"/>
<wire x1="269.24" y1="167.64" x2="264.16" y2="167.64" width="0.1524" layer="91"/>
<wire x1="264.16" y1="167.64" x2="264.16" y2="187.96" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C105" gate="G$1" pin="2"/>
<wire x1="264.16" y1="121.92" x2="259.08" y2="121.92" width="0.1524" layer="91"/>
<wire x1="259.08" y1="121.92" x2="251.46" y2="121.92" width="0.1524" layer="91"/>
<wire x1="251.46" y1="121.92" x2="243.84" y2="121.92" width="0.1524" layer="91"/>
<wire x1="243.84" y1="121.92" x2="236.22" y2="121.92" width="0.1524" layer="91"/>
<wire x1="236.22" y1="121.92" x2="228.6" y2="121.92" width="0.1524" layer="91"/>
<wire x1="228.6" y1="121.92" x2="218.44" y2="121.92" width="0.1524" layer="91"/>
<wire x1="259.08" y1="119.38" x2="259.08" y2="121.92" width="0.1524" layer="91"/>
<pinref part="C104" gate="G$1" pin="2"/>
<wire x1="251.46" y1="119.38" x2="251.46" y2="121.92" width="0.1524" layer="91"/>
<pinref part="R157" gate="G$1" pin="2"/>
<wire x1="243.84" y1="114.3" x2="243.84" y2="121.92" width="0.1524" layer="91"/>
<pinref part="R158" gate="G$1" pin="2"/>
<wire x1="236.22" y1="114.3" x2="236.22" y2="121.92" width="0.1524" layer="91"/>
<junction x="259.08" y="121.92"/>
<junction x="251.46" y="121.92"/>
<junction x="243.84" y="121.92"/>
<junction x="236.22" y="121.92"/>
<label x="218.44" y="121.92" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="R155" gate="G$1" pin="2"/>
<wire x1="228.6" y1="114.3" x2="228.6" y2="121.92" width="0.1524" layer="91"/>
<junction x="228.6" y="121.92"/>
<pinref part="U15" gate="G$1" pin="3"/>
<wire x1="269.24" y1="101.6" x2="264.16" y2="101.6" width="0.1524" layer="91"/>
<wire x1="264.16" y1="101.6" x2="264.16" y2="121.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+5V0" class="0">
<segment>
<pinref part="C50" gate="G$1" pin="2"/>
<wire x1="304.8" y1="157.48" x2="309.88" y2="157.48" width="0.1524" layer="91"/>
<wire x1="309.88" y1="157.48" x2="320.04" y2="157.48" width="0.1524" layer="91"/>
<wire x1="320.04" y1="157.48" x2="327.66" y2="157.48" width="0.1524" layer="91"/>
<wire x1="327.66" y1="157.48" x2="340.36" y2="157.48" width="0.1524" layer="91"/>
<wire x1="304.8" y1="160.02" x2="304.8" y2="157.48" width="0.1524" layer="91"/>
<pinref part="C49" gate="G$1" pin="2"/>
<wire x1="309.88" y1="160.02" x2="309.88" y2="157.48" width="0.1524" layer="91"/>
<pinref part="R69" gate="G$1" pin="2"/>
<wire x1="320.04" y1="160.02" x2="320.04" y2="157.48" width="0.1524" layer="91"/>
<pinref part="R64" gate="G$1" pin="2"/>
<wire x1="327.66" y1="160.02" x2="327.66" y2="157.48" width="0.1524" layer="91"/>
<junction x="327.66" y="157.48"/>
<junction x="320.04" y="157.48"/>
<junction x="309.88" y="157.48"/>
<label x="340.36" y="157.48" size="1.27" layer="95" xref="yes"/>
<pinref part="U6" gate="G$1" pin="7"/>
<wire x1="294.64" y1="172.72" x2="299.72" y2="172.72" width="0.1524" layer="91"/>
<wire x1="299.72" y1="172.72" x2="299.72" y2="157.48" width="0.1524" layer="91"/>
<wire x1="299.72" y1="157.48" x2="304.8" y2="157.48" width="0.1524" layer="91"/>
<junction x="304.8" y="157.48"/>
</segment>
<segment>
<pinref part="C106" gate="G$1" pin="2"/>
<wire x1="304.8" y1="91.44" x2="309.88" y2="91.44" width="0.1524" layer="91"/>
<wire x1="309.88" y1="91.44" x2="320.04" y2="91.44" width="0.1524" layer="91"/>
<wire x1="320.04" y1="91.44" x2="327.66" y2="91.44" width="0.1524" layer="91"/>
<wire x1="327.66" y1="91.44" x2="340.36" y2="91.44" width="0.1524" layer="91"/>
<wire x1="304.8" y1="93.98" x2="304.8" y2="91.44" width="0.1524" layer="91"/>
<pinref part="C107" gate="G$1" pin="2"/>
<wire x1="309.88" y1="93.98" x2="309.88" y2="91.44" width="0.1524" layer="91"/>
<pinref part="R154" gate="G$1" pin="2"/>
<wire x1="320.04" y1="93.98" x2="320.04" y2="91.44" width="0.1524" layer="91"/>
<pinref part="R156" gate="G$1" pin="2"/>
<wire x1="327.66" y1="93.98" x2="327.66" y2="91.44" width="0.1524" layer="91"/>
<junction x="327.66" y="91.44"/>
<junction x="320.04" y="91.44"/>
<junction x="309.88" y="91.44"/>
<label x="340.36" y="91.44" size="1.27" layer="95" xref="yes"/>
<pinref part="U15" gate="G$1" pin="7"/>
<wire x1="294.64" y1="106.68" x2="299.72" y2="106.68" width="0.1524" layer="91"/>
<wire x1="299.72" y1="106.68" x2="299.72" y2="91.44" width="0.1524" layer="91"/>
<wire x1="299.72" y1="91.44" x2="304.8" y2="91.44" width="0.1524" layer="91"/>
<junction x="304.8" y="91.44"/>
</segment>
</net>
<net name="N$205" class="0">
<segment>
<pinref part="R158" gate="G$1" pin="1"/>
<wire x1="236.22" y1="86.36" x2="218.44" y2="86.36" width="0.1524" layer="91"/>
<wire x1="236.22" y1="104.14" x2="236.22" y2="86.36" width="0.1524" layer="91"/>
<wire x1="297.18" y1="86.36" x2="236.22" y2="86.36" width="0.1524" layer="91"/>
<junction x="236.22" y="86.36"/>
<pinref part="TP37" gate="G$1" pin="PP"/>
<pinref part="U15" gate="G$1" pin="6"/>
<wire x1="294.64" y1="101.6" x2="297.18" y2="101.6" width="0.1524" layer="91"/>
<wire x1="297.18" y1="101.6" x2="297.18" y2="86.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SDA1_5V" class="0">
<segment>
<pinref part="R69" gate="G$1" pin="1"/>
<wire x1="320.04" y1="185.42" x2="330.2" y2="185.42" width="0.1524" layer="91"/>
<wire x1="320.04" y1="170.18" x2="320.04" y2="185.42" width="0.1524" layer="91"/>
<label x="340.36" y="185.42" size="1.27" layer="95" xref="yes"/>
<pinref part="TP14" gate="G$1" pin="PP"/>
<wire x1="330.2" y1="195.58" x2="330.2" y2="185.42" width="0.1524" layer="91"/>
<wire x1="330.2" y1="185.42" x2="340.36" y2="185.42" width="0.1524" layer="91"/>
<junction x="330.2" y="185.42"/>
<junction x="320.04" y="185.42"/>
<wire x1="266.7" y1="185.42" x2="320.04" y2="185.42" width="0.1524" layer="91"/>
<pinref part="U6" gate="G$1" pin="1"/>
<wire x1="269.24" y1="177.8" x2="266.7" y2="177.8" width="0.1524" layer="91"/>
<wire x1="266.7" y1="177.8" x2="266.7" y2="185.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SCL1_5V" class="0">
<segment>
<pinref part="R64" gate="G$1" pin="1"/>
<wire x1="327.66" y1="177.8" x2="332.74" y2="177.8" width="0.1524" layer="91"/>
<wire x1="327.66" y1="170.18" x2="327.66" y2="177.8" width="0.1524" layer="91"/>
<label x="340.36" y="177.8" size="1.27" layer="95" xref="yes"/>
<pinref part="TP11" gate="G$1" pin="PP"/>
<wire x1="332.74" y1="195.58" x2="332.74" y2="177.8" width="0.1524" layer="91"/>
<wire x1="327.66" y1="177.8" x2="294.64" y2="177.8" width="0.1524" layer="91"/>
<junction x="327.66" y="177.8"/>
<wire x1="332.74" y1="177.8" x2="340.36" y2="177.8" width="0.1524" layer="91"/>
<junction x="332.74" y="177.8"/>
<pinref part="U6" gate="G$1" pin="8"/>
</segment>
</net>
<net name="SDA_B" class="0">
<segment>
<pinref part="R155" gate="G$1" pin="1"/>
<wire x1="228.6" y1="104.14" x2="228.6" y2="96.52" width="0.1524" layer="91"/>
<wire x1="228.6" y1="96.52" x2="218.44" y2="96.52" width="0.1524" layer="91"/>
<label x="218.44" y="96.52" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="U15" gate="G$1" pin="4"/>
<wire x1="228.6" y1="96.52" x2="269.24" y2="96.52" width="0.1524" layer="91"/>
<junction x="228.6" y="96.52"/>
</segment>
</net>
<net name="SCL_B" class="0">
<segment>
<pinref part="R157" gate="G$1" pin="1"/>
<wire x1="243.84" y1="104.14" x2="243.84" y2="91.44" width="0.1524" layer="91"/>
<wire x1="218.44" y1="91.44" x2="243.84" y2="91.44" width="0.1524" layer="91"/>
<label x="218.44" y="91.44" size="1.27" layer="95" rot="R180" xref="yes"/>
<wire x1="243.84" y1="91.44" x2="294.64" y2="91.44" width="0.1524" layer="91"/>
<junction x="243.84" y="91.44"/>
<pinref part="U15" gate="G$1" pin="5"/>
<wire x1="294.64" y1="91.44" x2="294.64" y2="96.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="COM5_RXD_IN" class="0">
<segment>
<pinref part="R113" gate="G$1" pin="2"/>
<wire x1="137.16" y1="185.42" x2="144.78" y2="185.42" width="0.1524" layer="91"/>
<label x="144.78" y="185.42" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="COM5_EN" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="3"/>
<wire x1="121.92" y1="175.26" x2="127" y2="175.26" width="0.1524" layer="91"/>
<pinref part="U11" gate="G$1" pin="2"/>
<wire x1="127" y1="175.26" x2="144.78" y2="175.26" width="0.1524" layer="91"/>
<wire x1="121.92" y1="180.34" x2="127" y2="180.34" width="0.1524" layer="91"/>
<wire x1="127" y1="180.34" x2="127" y2="175.26" width="0.1524" layer="91"/>
<junction x="127" y="175.26"/>
<label x="144.78" y="175.26" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="COM5_TXD_OUT" class="0">
<segment>
<pinref part="R98" gate="G$1" pin="2"/>
<wire x1="137.16" y1="170.18" x2="144.78" y2="170.18" width="0.1524" layer="91"/>
<label x="144.78" y="170.18" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="COM6_RXD_IN" class="0">
<segment>
<pinref part="R114" gate="G$1" pin="2"/>
<wire x1="137.16" y1="134.62" x2="144.78" y2="134.62" width="0.1524" layer="91"/>
<label x="144.78" y="134.62" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="COM6_EN" class="0">
<segment>
<pinref part="U12" gate="G$1" pin="3"/>
<wire x1="121.92" y1="124.46" x2="127" y2="124.46" width="0.1524" layer="91"/>
<pinref part="U12" gate="G$1" pin="2"/>
<wire x1="127" y1="124.46" x2="144.78" y2="124.46" width="0.1524" layer="91"/>
<wire x1="121.92" y1="129.54" x2="127" y2="129.54" width="0.1524" layer="91"/>
<wire x1="127" y1="129.54" x2="127" y2="124.46" width="0.1524" layer="91"/>
<junction x="127" y="124.46"/>
<label x="144.78" y="124.46" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="COM6_TXD_OUT" class="0">
<segment>
<pinref part="R99" gate="G$1" pin="2"/>
<wire x1="137.16" y1="119.38" x2="144.78" y2="119.38" width="0.1524" layer="91"/>
<label x="144.78" y="119.38" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
<text x="218.44" y="220.98" size="1.27" layer="97">SBRT3U45SAF-13/SBRT3U45SA-13</text>
<text x="12.7" y="226.06" size="1.27" layer="91">AISG_1</text>
<text x="12.7" y="154.94" size="1.27" layer="91">AISG_2</text>
<text x="12.7" y="88.9" size="1.27" layer="91">AISG_3</text>
</plain>
<instances>
<instance part="C29" gate="G$1" x="220.98" y="109.22" smashed="yes" rot="R90">
<attribute name="NAME" x="220.472" y="104.14" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="220.472" y="110.49" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C28" gate="G$1" x="228.6" y="109.22" smashed="yes" rot="R90">
<attribute name="NAME" x="228.092" y="104.14" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="228.092" y="110.49" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C15" gate="G$1" x="307.34" y="109.22" smashed="yes" rot="R90">
<attribute name="NAME" x="306.832" y="104.14" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="306.832" y="110.49" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C16" gate="G$1" x="314.96" y="109.22" smashed="yes" rot="R90">
<attribute name="NAME" x="314.452" y="104.14" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="314.452" y="110.49" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="L1" gate="G$1" x="294.64" y="116.84" smashed="yes">
<attribute name="NAME" x="288.798" y="117.348" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="297.434" y="117.348" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="L4" gate="G$1" x="210.82" y="116.84" smashed="yes">
<attribute name="NAME" x="204.724" y="117.348" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="213.614" y="117.348" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="R45" gate="G$1" x="266.7" y="109.22" smashed="yes" rot="R90">
<attribute name="NAME" x="266.2174" y="102.87" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="266.192" y="111.76" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R46" gate="G$1" x="238.76" y="91.44" smashed="yes" rot="R180">
<attribute name="NAME" x="231.648" y="91.9226" size="1.27" layer="95"/>
<attribute name="VALUE" x="241.3" y="91.948" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY32" gate="GND" x="231.14" y="86.36" smashed="yes">
<attribute name="VALUE" x="229.235" y="83.185" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY33" gate="GND" x="220.98" y="99.06" smashed="yes">
<attribute name="VALUE" x="219.075" y="95.885" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY34" gate="GND" x="228.6" y="99.06" smashed="yes">
<attribute name="VALUE" x="226.695" y="95.885" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY43" gate="GND" x="307.34" y="99.06" smashed="yes">
<attribute name="VALUE" x="305.435" y="95.885" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY44" gate="GND" x="314.96" y="99.06" smashed="yes">
<attribute name="VALUE" x="313.055" y="95.885" size="1.27" layer="96"/>
</instance>
<instance part="R50" gate="G$1" x="210.82" y="121.92" smashed="yes" rot="R180">
<attribute name="NAME" x="203.708" y="122.4026" size="1.27" layer="95"/>
<attribute name="VALUE" x="213.36" y="122.428" size="1.27" layer="96"/>
</instance>
<instance part="R36" gate="G$1" x="294.64" y="121.92" smashed="yes" rot="R180">
<attribute name="NAME" x="287.528" y="122.4026" size="1.27" layer="95"/>
<attribute name="VALUE" x="297.18" y="122.428" size="1.27" layer="96"/>
</instance>
<instance part="R41" gate="G$1" x="274.32" y="101.6" smashed="yes" rot="R180">
<attribute name="NAME" x="267.462" y="102.0826" size="1.27" layer="95"/>
<attribute name="VALUE" x="272.034" y="99.06" size="1.27" layer="96"/>
</instance>
<instance part="Q11" gate="G$1" x="281.94" y="93.98" smashed="yes" rot="MR270">
<attribute name="NAME" x="276.86" y="88.9" size="1.27" layer="95" rot="MR180"/>
<attribute name="VALUE" x="276.86" y="86.36" size="1.27" layer="96" rot="MR180"/>
</instance>
<instance part="C20" gate="G$1" x="289.56" y="99.06" smashed="yes" rot="R180">
<attribute name="NAME" x="284.226" y="99.568" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="290.83" y="99.568" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="SUPPLY45" gate="GND" x="297.18" y="86.36" smashed="yes">
<attribute name="VALUE" x="295.275" y="83.185" size="1.27" layer="96"/>
</instance>
<instance part="U2" gate="G$1" x="246.38" y="116.84" smashed="yes">
<attribute name="NAME" x="238.76" y="124.46" size="1.27" layer="95"/>
<attribute name="VALUE" x="238.76" y="127" size="1.27" layer="96"/>
</instance>
<instance part="D17" gate="G$1" x="281.94" y="109.22" smashed="yes" rot="R90">
<attribute name="NAME" x="280.035" y="104.14" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="285.75" y="104.14" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R108" gate="G$1" x="48.26" y="220.98" smashed="yes">
<attribute name="NAME" x="40.64" y="220.98" size="1.27" layer="95"/>
<attribute name="VALUE" x="53.34" y="220.98" size="1.27" layer="96"/>
</instance>
<instance part="R102" gate="G$1" x="48.26" y="218.44" smashed="yes">
<attribute name="NAME" x="40.64" y="218.44" size="1.27" layer="95"/>
<attribute name="VALUE" x="53.34" y="218.44" size="1.27" layer="96"/>
</instance>
<instance part="D23" gate="G$1" x="33.02" y="208.28" smashed="yes" rot="R90">
<attribute name="NAME" x="30.48" y="203.2" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="38.1" y="203.2" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R110" gate="G$1" x="48.26" y="149.86" smashed="yes">
<attribute name="NAME" x="40.64" y="149.86" size="1.27" layer="95"/>
<attribute name="VALUE" x="53.34" y="149.86" size="1.27" layer="96"/>
</instance>
<instance part="R105" gate="G$1" x="48.26" y="147.32" smashed="yes">
<attribute name="NAME" x="40.64" y="147.32" size="1.27" layer="95"/>
<attribute name="VALUE" x="53.34" y="147.32" size="1.27" layer="96"/>
</instance>
<instance part="D24" gate="G$1" x="33.02" y="137.16" smashed="yes" rot="R90">
<attribute name="NAME" x="30.48" y="132.08" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="38.1" y="132.08" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="SUPPLY56" gate="GND" x="246.38" y="152.4" smashed="yes">
<attribute name="VALUE" x="244.475" y="149.225" size="1.27" layer="96"/>
</instance>
<instance part="R28" gate="G$1" x="246.38" y="167.64" smashed="yes" rot="R270">
<attribute name="NAME" x="245.8974" y="160.528" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="245.872" y="170.18" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R27" gate="G$1" x="246.38" y="195.58" smashed="yes" rot="R270">
<attribute name="NAME" x="245.8974" y="188.468" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="245.872" y="198.12" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R29" gate="G$1" x="266.7" y="170.18" smashed="yes" rot="R270">
<attribute name="NAME" x="266.2174" y="163.068" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="266.192" y="172.72" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R32" gate="G$1" x="274.32" y="157.48" smashed="yes">
<attribute name="NAME" x="281.432" y="156.9974" size="1.27" layer="95" rot="R180"/>
<attribute name="VALUE" x="271.78" y="156.972" size="1.27" layer="96" rot="R180"/>
</instance>
<instance part="C14" gate="G$1" x="281.94" y="165.1" smashed="yes" rot="R90">
<attribute name="NAME" x="281.432" y="159.258" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="281.432" y="166.37" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="SUPPLY57" gate="GND" x="287.02" y="147.32" smashed="yes">
<attribute name="VALUE" x="285.115" y="144.145" size="1.27" layer="96"/>
</instance>
<instance part="R33" gate="G$1" x="307.34" y="200.66" smashed="yes" rot="R270">
<attribute name="NAME" x="306.8574" y="193.548" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="306.832" y="203.2" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R31" gate="G$1" x="289.56" y="200.66" smashed="yes" rot="R180">
<attribute name="NAME" x="282.448" y="201.1426" size="1.27" layer="95"/>
<attribute name="VALUE" x="292.1" y="201.168" size="1.27" layer="96"/>
</instance>
<instance part="C17" gate="G$1" x="314.96" y="203.2" smashed="yes" rot="R90">
<attribute name="NAME" x="312.42" y="201.93" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="318.516" y="201.93" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C13" gate="G$1" x="297.18" y="162.56" smashed="yes" rot="R90">
<attribute name="NAME" x="294.894" y="160.782" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="300.736" y="160.782" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="R30" gate="G$1" x="297.18" y="175.26" smashed="yes" rot="R270">
<attribute name="NAME" x="296.6974" y="168.148" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="296.672" y="177.8" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="U1" gate="G$1" x="271.78" y="185.42" smashed="yes"/>
<instance part="R24" gate="G$1" x="266.7" y="210.82" smashed="yes">
<attribute name="NAME" x="259.334" y="211.3026" size="1.27" layer="95"/>
<attribute name="VALUE" x="269.24" y="211.328" size="1.27" layer="96"/>
</instance>
<instance part="Q10" gate="G$1" x="294.64" y="208.28" smashed="yes" rot="R90">
<attribute name="NAME" x="289.56" y="213.36" size="1.27" layer="95" ratio="15"/>
<attribute name="VALUE" x="297.18" y="213.36" size="1.27" layer="96" ratio="15"/>
</instance>
<instance part="R26" gate="G$1" x="266.7" y="195.58" smashed="yes">
<attribute name="NAME" x="259.842" y="196.0626" size="1.27" layer="95"/>
<attribute name="VALUE" x="269.24" y="196.088" size="1.27" layer="96"/>
</instance>
<instance part="R25" gate="G$1" x="276.86" y="203.2" smashed="yes" rot="R90">
<attribute name="NAME" x="276.3774" y="196.342" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="276.352" y="205.74" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="SUPPLY58" gate="GND" x="314.96" y="190.5" smashed="yes">
<attribute name="VALUE" x="313.055" y="187.325" size="1.27" layer="96"/>
</instance>
<instance part="C18" gate="G$1" x="322.58" y="203.2" smashed="yes" rot="R90">
<attribute name="NAME" x="322.072" y="197.358" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="322.072" y="204.47" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="R35" gate="G$1" x="317.5" y="182.88" smashed="yes">
<attribute name="NAME" x="324.612" y="182.3974" size="1.27" layer="95" rot="R180"/>
<attribute name="VALUE" x="314.96" y="182.372" size="1.27" layer="96" rot="R180"/>
</instance>
<instance part="D16" gate="G$1" x="332.74" y="203.2" smashed="yes" rot="R90">
<attribute name="NAME" x="330.835" y="200.66" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="336.55" y="200.66" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="TP7" gate="G$1" x="325.12" y="185.42" smashed="yes">
<attribute name="NAME" x="323.85" y="189.23" size="1.27" layer="95"/>
</instance>
<instance part="R107" gate="G$1" x="48.26" y="83.82" smashed="yes">
<attribute name="NAME" x="40.64" y="83.82" size="1.27" layer="95"/>
<attribute name="VALUE" x="53.34" y="83.82" size="1.27" layer="96"/>
</instance>
<instance part="R100" gate="G$1" x="48.26" y="81.28" smashed="yes">
<attribute name="NAME" x="40.64" y="81.28" size="1.27" layer="95"/>
<attribute name="VALUE" x="53.34" y="81.28" size="1.27" layer="96"/>
</instance>
<instance part="D22" gate="G$1" x="33.02" y="66.04" smashed="yes" rot="R90">
<attribute name="NAME" x="30.48" y="60.96" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="38.1" y="60.96" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="SUPPLY59" gate="GND" x="27.94" y="48.26" smashed="yes">
<attribute name="VALUE" x="26.035" y="45.085" size="1.27" layer="96"/>
</instance>
<instance part="D1" gate="G$1" x="58.42" y="223.52" smashed="yes">
<attribute name="NAME" x="53.34" y="226.06" size="1.27" layer="95"/>
<attribute name="VALUE" x="53.34" y="228.6" size="1.27" layer="96"/>
</instance>
<instance part="D2" gate="G$1" x="58.42" y="152.4" smashed="yes">
<attribute name="NAME" x="53.34" y="154.94" size="1.27" layer="95"/>
<attribute name="VALUE" x="53.34" y="157.48" size="1.27" layer="96"/>
</instance>
<instance part="D3" gate="G$1" x="58.42" y="86.36" smashed="yes">
<attribute name="NAME" x="53.34" y="88.9" size="1.27" layer="95"/>
<attribute name="VALUE" x="53.34" y="91.44" size="1.27" layer="96"/>
</instance>
<instance part="TP1" gate="G$1" x="104.14" y="88.9" smashed="yes">
<attribute name="NAME" x="104.14" y="93.98" size="1.27" layer="95" rot="R90"/>
</instance>
<instance part="TP3" gate="G$1" x="104.14" y="154.94" smashed="yes">
<attribute name="NAME" x="104.14" y="160.02" size="1.27" layer="95" rot="R90"/>
</instance>
<instance part="TP2" gate="G$1" x="104.14" y="226.06" smashed="yes">
<attribute name="NAME" x="104.14" y="231.14" size="1.27" layer="95" rot="R90"/>
</instance>
<instance part="FRAME2" gate="G$1" x="0" y="0" smashed="yes">
<attribute name="DRAWING_NAME" x="344.17" y="15.24" size="2.54" layer="94"/>
<attribute name="LAST_DATE_TIME" x="344.17" y="10.16" size="2.286" layer="94"/>
<attribute name="SHEET" x="357.505" y="5.08" size="2.54" layer="94"/>
</instance>
<instance part="J1" gate="-1" x="17.78" y="223.52" smashed="yes" rot="R180">
<attribute name="NAME" x="21.082" y="223.012" size="1.27" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="J1" gate="-2" x="17.78" y="220.98" smashed="yes" rot="R180">
<attribute name="NAME" x="21.082" y="220.472" size="1.27" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="J1" gate="-3" x="17.78" y="218.44" smashed="yes" rot="R180">
<attribute name="NAME" x="21.082" y="217.932" size="1.27" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="J1" gate="-4" x="17.78" y="215.9" smashed="yes" rot="R180">
<attribute name="NAME" x="21.082" y="215.392" size="1.27" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="J2" gate="-1" x="17.78" y="152.4" smashed="yes" rot="R180">
<attribute name="NAME" x="21.082" y="151.892" size="1.27" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="J2" gate="-2" x="17.78" y="149.86" smashed="yes" rot="R180">
<attribute name="NAME" x="21.082" y="149.352" size="1.27" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="J2" gate="-3" x="17.78" y="147.32" smashed="yes" rot="R180">
<attribute name="NAME" x="21.082" y="146.812" size="1.27" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="J2" gate="-4" x="17.78" y="144.78" smashed="yes" rot="R180">
<attribute name="NAME" x="21.082" y="144.272" size="1.27" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="J3" gate="-1" x="17.78" y="86.36" smashed="yes" rot="R180">
<attribute name="NAME" x="21.082" y="85.852" size="1.27" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="J3" gate="-2" x="17.78" y="83.82" smashed="yes" rot="R180">
<attribute name="NAME" x="21.082" y="83.312" size="1.27" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="J3" gate="-3" x="17.78" y="81.28" smashed="yes" rot="R180">
<attribute name="NAME" x="21.082" y="80.772" size="1.27" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="J3" gate="-4" x="17.78" y="78.74" smashed="yes" rot="R180">
<attribute name="NAME" x="21.082" y="78.232" size="1.27" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="TP26" gate="G$1" x="76.2" y="213.36" smashed="yes" rot="R180">
<attribute name="NAME" x="76.2" y="208.28" size="1.27" layer="95" rot="R270"/>
</instance>
<instance part="TP23" gate="G$1" x="78.74" y="210.82" smashed="yes" rot="R180">
<attribute name="NAME" x="78.74" y="205.74" size="1.27" layer="95" rot="R270"/>
</instance>
<instance part="TP24" gate="G$1" x="78.74" y="139.7" smashed="yes" rot="R180">
<attribute name="NAME" x="78.74" y="134.62" size="1.27" layer="95" rot="R270"/>
</instance>
<instance part="TP27" gate="G$1" x="76.2" y="142.24" smashed="yes" rot="R180">
<attribute name="NAME" x="76.2" y="137.16" size="1.27" layer="95" rot="R270"/>
</instance>
<instance part="TP22" gate="G$1" x="78.74" y="73.66" smashed="yes" rot="R180">
<attribute name="NAME" x="78.74" y="68.58" size="1.27" layer="95" rot="R270"/>
</instance>
<instance part="TP25" gate="G$1" x="76.2" y="76.2" smashed="yes" rot="R180">
<attribute name="NAME" x="76.2" y="71.12" size="1.27" layer="95" rot="R270"/>
</instance>
<instance part="R162" gate="G$1" x="220.98" y="210.82" smashed="yes">
<attribute name="NAME" x="218.44" y="212.344" size="1.778" layer="95"/>
<attribute name="VALUE" x="218.44" y="207.518" size="1.778" layer="96"/>
</instance>
<instance part="R159" gate="G$1" x="220.98" y="200.66" smashed="yes">
<attribute name="NAME" x="218.44" y="202.184" size="1.778" layer="95"/>
<attribute name="VALUE" x="218.44" y="197.358" size="1.778" layer="96"/>
</instance>
<instance part="R164" gate="G$1" x="220.98" y="190.5" smashed="yes">
<attribute name="NAME" x="218.44" y="192.024" size="1.778" layer="95"/>
<attribute name="VALUE" x="218.44" y="187.198" size="1.778" layer="96"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="N$43" class="0">
<segment>
<pinref part="L4" gate="G$1" pin="2"/>
<wire x1="215.9" y1="116.84" x2="218.44" y2="116.84" width="0.1524" layer="91"/>
<pinref part="C28" gate="G$1" pin="2"/>
<wire x1="218.44" y1="116.84" x2="220.98" y2="116.84" width="0.1524" layer="91"/>
<wire x1="220.98" y1="116.84" x2="228.6" y2="116.84" width="0.1524" layer="91"/>
<wire x1="228.6" y1="114.3" x2="228.6" y2="116.84" width="0.1524" layer="91"/>
<pinref part="C29" gate="G$1" pin="2"/>
<wire x1="220.98" y1="114.3" x2="220.98" y2="116.84" width="0.1524" layer="91"/>
<junction x="220.98" y="116.84"/>
<pinref part="R50" gate="G$1" pin="1"/>
<wire x1="215.9" y1="121.92" x2="218.44" y2="121.92" width="0.1524" layer="91"/>
<wire x1="218.44" y1="121.92" x2="218.44" y2="116.84" width="0.1524" layer="91"/>
<junction x="218.44" y="116.84"/>
<wire x1="236.22" y1="116.84" x2="228.6" y2="116.84" width="0.1524" layer="91"/>
<junction x="228.6" y="116.84"/>
<pinref part="U2" gate="G$1" pin="VIN"/>
</segment>
</net>
<net name="N$44" class="0">
<segment>
<pinref part="R46" gate="G$1" pin="1"/>
<pinref part="R45" gate="G$1" pin="1"/>
<wire x1="266.7" y1="104.14" x2="266.7" y2="101.6" width="0.1524" layer="91"/>
<wire x1="266.7" y1="101.6" x2="266.7" y2="91.44" width="0.1524" layer="91"/>
<wire x1="266.7" y1="91.44" x2="246.38" y2="91.44" width="0.1524" layer="91"/>
<wire x1="246.38" y1="91.44" x2="243.84" y2="91.44" width="0.1524" layer="91"/>
<junction x="246.38" y="91.44"/>
<wire x1="246.38" y1="109.22" x2="246.38" y2="91.44" width="0.1524" layer="91"/>
<pinref part="R41" gate="G$1" pin="2"/>
<wire x1="269.24" y1="101.6" x2="266.7" y2="101.6" width="0.1524" layer="91"/>
<junction x="266.7" y="101.6"/>
<pinref part="Q11" gate="G$1" pin="E"/>
<wire x1="276.86" y1="91.44" x2="266.7" y2="91.44" width="0.1524" layer="91"/>
<junction x="266.7" y="91.44"/>
<pinref part="U2" gate="G$1" pin="ADJ"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="R46" gate="G$1" pin="2"/>
<wire x1="233.68" y1="91.44" x2="231.14" y2="91.44" width="0.1524" layer="91"/>
<wire x1="231.14" y1="91.44" x2="231.14" y2="88.9" width="0.1524" layer="91"/>
<pinref part="SUPPLY32" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C29" gate="G$1" pin="1"/>
<pinref part="SUPPLY33" gate="GND" pin="GND"/>
<wire x1="220.98" y1="104.14" x2="220.98" y2="101.6" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUPPLY34" gate="GND" pin="GND"/>
<pinref part="C28" gate="G$1" pin="1"/>
<wire x1="228.6" y1="101.6" x2="228.6" y2="104.14" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C15" gate="G$1" pin="1"/>
<pinref part="SUPPLY43" gate="GND" pin="GND"/>
<wire x1="307.34" y1="104.14" x2="307.34" y2="101.6" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C16" gate="G$1" pin="1"/>
<pinref part="SUPPLY44" gate="GND" pin="GND"/>
<wire x1="314.96" y1="104.14" x2="314.96" y2="101.6" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="297.18" y1="88.9" x2="297.18" y2="91.44" width="0.1524" layer="91"/>
<pinref part="C20" gate="G$1" pin="1"/>
<wire x1="297.18" y1="91.44" x2="297.18" y2="99.06" width="0.1524" layer="91"/>
<wire x1="297.18" y1="99.06" x2="294.64" y2="99.06" width="0.1524" layer="91"/>
<pinref part="SUPPLY45" gate="GND" pin="GND"/>
<pinref part="Q11" gate="G$1" pin="C"/>
<wire x1="287.02" y1="91.44" x2="297.18" y2="91.44" width="0.1524" layer="91"/>
<junction x="297.18" y="91.44"/>
</segment>
<segment>
<pinref part="R28" gate="G$1" pin="2"/>
<pinref part="SUPPLY56" gate="GND" pin="GND"/>
<wire x1="246.38" y1="162.56" x2="246.38" y2="154.94" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="5_GND"/>
<pinref part="SUPPLY57" gate="GND" pin="GND"/>
<wire x1="287.02" y1="172.72" x2="287.02" y2="157.48" width="0.1524" layer="91"/>
<pinref part="C14" gate="G$1" pin="1"/>
<wire x1="287.02" y1="157.48" x2="287.02" y2="154.94" width="0.1524" layer="91"/>
<wire x1="287.02" y1="154.94" x2="287.02" y2="149.86" width="0.1524" layer="91"/>
<wire x1="281.94" y1="160.02" x2="281.94" y2="157.48" width="0.1524" layer="91"/>
<wire x1="281.94" y1="157.48" x2="287.02" y2="157.48" width="0.1524" layer="91"/>
<junction x="287.02" y="157.48"/>
<pinref part="R32" gate="G$1" pin="2"/>
<wire x1="279.4" y1="157.48" x2="281.94" y2="157.48" width="0.1524" layer="91"/>
<junction x="281.94" y="157.48"/>
<pinref part="C13" gate="G$1" pin="1"/>
<wire x1="297.18" y1="157.48" x2="297.18" y2="154.94" width="0.1524" layer="91"/>
<wire x1="297.18" y1="154.94" x2="287.02" y2="154.94" width="0.1524" layer="91"/>
<junction x="287.02" y="154.94"/>
</segment>
<segment>
<pinref part="C17" gate="G$1" pin="1"/>
<pinref part="SUPPLY58" gate="GND" pin="GND"/>
<wire x1="314.96" y1="198.12" x2="314.96" y2="195.58" width="0.1524" layer="91"/>
<pinref part="C18" gate="G$1" pin="1"/>
<wire x1="314.96" y1="195.58" x2="314.96" y2="193.04" width="0.1524" layer="91"/>
<wire x1="322.58" y1="198.12" x2="322.58" y2="195.58" width="0.1524" layer="91"/>
<wire x1="322.58" y1="195.58" x2="314.96" y2="195.58" width="0.1524" layer="91"/>
<junction x="314.96" y="195.58"/>
<wire x1="332.74" y1="199.39" x2="332.74" y2="195.58" width="0.1524" layer="91"/>
<wire x1="332.74" y1="195.58" x2="322.58" y2="195.58" width="0.1524" layer="91"/>
<junction x="322.58" y="195.58"/>
<pinref part="D16" gate="G$1" pin="A"/>
</segment>
<segment>
<wire x1="27.94" y1="55.88" x2="27.94" y2="50.8" width="0.1524" layer="91"/>
<pinref part="SUPPLY59" gate="GND" pin="GND"/>
<wire x1="27.94" y1="78.74" x2="27.94" y2="55.88" width="0.1524" layer="91"/>
<pinref part="D22" gate="G$1" pin="A"/>
<wire x1="33.02" y1="62.23" x2="33.02" y2="55.88" width="0.1524" layer="91"/>
<wire x1="33.02" y1="55.88" x2="27.94" y2="55.88" width="0.1524" layer="91"/>
<wire x1="27.94" y1="78.74" x2="22.86" y2="78.74" width="0.1524" layer="91"/>
<junction x="27.94" y="78.74"/>
<pinref part="D23" gate="G$1" pin="A"/>
<wire x1="33.02" y1="204.47" x2="33.02" y2="198.12" width="0.1524" layer="91"/>
<wire x1="33.02" y1="198.12" x2="25.4" y2="198.12" width="0.1524" layer="91"/>
<wire x1="27.94" y1="127" x2="27.94" y2="78.74" width="0.1524" layer="91"/>
<wire x1="25.4" y1="144.78" x2="27.94" y2="144.78" width="0.1524" layer="91"/>
<wire x1="27.94" y1="144.78" x2="27.94" y2="127" width="0.1524" layer="91"/>
<pinref part="D24" gate="G$1" pin="A"/>
<wire x1="33.02" y1="133.35" x2="33.02" y2="127" width="0.1524" layer="91"/>
<wire x1="33.02" y1="127" x2="27.94" y2="127" width="0.1524" layer="91"/>
<junction x="27.94" y="127"/>
<wire x1="25.4" y1="198.12" x2="25.4" y2="144.78" width="0.1524" layer="91"/>
<wire x1="25.4" y1="198.12" x2="25.4" y2="215.9" width="0.1524" layer="91"/>
<junction x="25.4" y="198.12"/>
<wire x1="25.4" y1="215.9" x2="22.86" y2="215.9" width="0.1524" layer="91"/>
<wire x1="25.4" y1="144.78" x2="22.86" y2="144.78" width="0.1524" layer="91"/>
<junction x="25.4" y="144.78"/>
<junction x="27.94" y="55.88"/>
<pinref part="J1" gate="-4" pin="1"/>
<pinref part="J2" gate="-4" pin="1"/>
<pinref part="J3" gate="-4" pin="1"/>
</segment>
</net>
<net name="+5V0" class="0">
<segment>
<pinref part="L4" gate="G$1" pin="1"/>
<wire x1="205.74" y1="116.84" x2="203.2" y2="116.84" width="0.1524" layer="91"/>
<pinref part="R50" gate="G$1" pin="2"/>
<wire x1="203.2" y1="116.84" x2="198.12" y2="116.84" width="0.1524" layer="91"/>
<wire x1="205.74" y1="121.92" x2="203.2" y2="121.92" width="0.1524" layer="91"/>
<wire x1="203.2" y1="121.92" x2="203.2" y2="116.84" width="0.1524" layer="91"/>
<junction x="203.2" y="116.84"/>
<label x="198.12" y="116.84" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$45" class="0">
<segment>
<wire x1="281.94" y1="105.41" x2="281.94" y2="101.6" width="0.1524" layer="91"/>
<pinref part="R41" gate="G$1" pin="1"/>
<wire x1="281.94" y1="101.6" x2="279.4" y2="101.6" width="0.1524" layer="91"/>
<pinref part="Q11" gate="G$1" pin="B"/>
<wire x1="281.94" y1="101.6" x2="281.94" y2="99.06" width="0.1524" layer="91"/>
<junction x="281.94" y="101.6"/>
<pinref part="C20" gate="G$1" pin="2"/>
<wire x1="281.94" y1="99.06" x2="281.94" y2="96.52" width="0.1524" layer="91"/>
<wire x1="284.48" y1="99.06" x2="281.94" y2="99.06" width="0.1524" layer="91"/>
<junction x="281.94" y="99.06"/>
<pinref part="D17" gate="G$1" pin="A"/>
</segment>
</net>
<net name="N$46" class="0">
<segment>
<pinref part="R36" gate="G$1" pin="2"/>
<wire x1="289.56" y1="121.92" x2="287.02" y2="121.92" width="0.1524" layer="91"/>
<wire x1="266.7" y1="116.84" x2="281.94" y2="116.84" width="0.1524" layer="91"/>
<pinref part="R45" gate="G$1" pin="2"/>
<wire x1="266.7" y1="114.3" x2="266.7" y2="116.84" width="0.1524" layer="91"/>
<wire x1="256.54" y1="116.84" x2="266.7" y2="116.84" width="0.1524" layer="91"/>
<junction x="266.7" y="116.84"/>
<wire x1="281.94" y1="113.03" x2="281.94" y2="116.84" width="0.1524" layer="91"/>
<pinref part="L1" gate="G$1" pin="1"/>
<wire x1="289.56" y1="116.84" x2="287.02" y2="116.84" width="0.1524" layer="91"/>
<wire x1="287.02" y1="116.84" x2="281.94" y2="116.84" width="0.1524" layer="91"/>
<wire x1="287.02" y1="121.92" x2="287.02" y2="116.84" width="0.1524" layer="91"/>
<junction x="281.94" y="116.84"/>
<junction x="287.02" y="116.84"/>
<pinref part="U2" gate="G$1" pin="VOUT"/>
<pinref part="D17" gate="G$1" pin="K"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<pinref part="R36" gate="G$1" pin="1"/>
<wire x1="299.72" y1="121.92" x2="302.26" y2="121.92" width="0.1524" layer="91"/>
<pinref part="C16" gate="G$1" pin="2"/>
<wire x1="314.96" y1="114.3" x2="314.96" y2="116.84" width="0.1524" layer="91"/>
<junction x="314.96" y="116.84"/>
<wire x1="307.34" y1="116.84" x2="314.96" y2="116.84" width="0.1524" layer="91"/>
<pinref part="C15" gate="G$1" pin="2"/>
<wire x1="307.34" y1="114.3" x2="307.34" y2="116.84" width="0.1524" layer="91"/>
<pinref part="L1" gate="G$1" pin="2"/>
<wire x1="299.72" y1="116.84" x2="302.26" y2="116.84" width="0.1524" layer="91"/>
<wire x1="302.26" y1="116.84" x2="307.34" y2="116.84" width="0.1524" layer="91"/>
<wire x1="302.26" y1="121.92" x2="302.26" y2="116.84" width="0.1524" layer="91"/>
<junction x="302.26" y="116.84"/>
<junction x="307.34" y="116.84"/>
<label x="325.12" y="116.84" size="1.27" layer="95" xref="yes"/>
<wire x1="314.96" y1="116.84" x2="325.12" y2="116.84" width="0.1524" layer="91"/>
</segment>
</net>
<net name="AISG_1_N" class="0">
<segment>
<pinref part="R108" gate="G$1" pin="1"/>
<wire x1="22.86" y1="220.98" x2="43.18" y2="220.98" width="0.1524" layer="91"/>
<pinref part="J1" gate="-2" pin="1"/>
<label x="27.94" y="220.98" size="1.778" layer="95"/>
</segment>
</net>
<net name="AISG_1_P" class="0">
<segment>
<pinref part="R102" gate="G$1" pin="1"/>
<wire x1="22.86" y1="218.44" x2="43.18" y2="218.44" width="0.1524" layer="91"/>
<pinref part="J1" gate="-3" pin="1"/>
<label x="27.94" y="218.44" size="1.778" layer="95"/>
</segment>
</net>
<net name="AISG_2_N" class="0">
<segment>
<pinref part="R110" gate="G$1" pin="1"/>
<wire x1="43.18" y1="149.86" x2="22.86" y2="149.86" width="0.1524" layer="91"/>
<pinref part="J2" gate="-2" pin="1"/>
<label x="27.94" y="149.86" size="1.778" layer="95"/>
</segment>
</net>
<net name="AISG_2_P" class="0">
<segment>
<pinref part="R105" gate="G$1" pin="1"/>
<wire x1="43.18" y1="147.32" x2="22.86" y2="147.32" width="0.1524" layer="91"/>
<pinref part="J2" gate="-3" pin="1"/>
<label x="27.94" y="147.32" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$87" class="0">
<segment>
<pinref part="R24" gate="G$1" pin="1"/>
<wire x1="259.08" y1="195.58" x2="259.08" y2="210.82" width="0.1524" layer="91"/>
<wire x1="261.62" y1="210.82" x2="259.08" y2="210.82" width="0.1524" layer="91"/>
<pinref part="R26" gate="G$1" pin="1"/>
<wire x1="261.62" y1="195.58" x2="259.08" y2="195.58" width="0.1524" layer="91"/>
<junction x="259.08" y="210.82"/>
<pinref part="R27" gate="G$1" pin="1"/>
<wire x1="226.06" y1="210.82" x2="238.76" y2="210.82" width="0.1524" layer="91"/>
<wire x1="238.76" y1="210.82" x2="246.38" y2="210.82" width="0.1524" layer="91"/>
<wire x1="246.38" y1="210.82" x2="259.08" y2="210.82" width="0.1524" layer="91"/>
<wire x1="246.38" y1="200.66" x2="246.38" y2="210.82" width="0.1524" layer="91"/>
<junction x="246.38" y="210.82"/>
<pinref part="U1" gate="G$1" pin="10_VCC"/>
<wire x1="269.24" y1="182.88" x2="259.08" y2="182.88" width="0.1524" layer="91"/>
<wire x1="259.08" y1="182.88" x2="259.08" y2="195.58" width="0.1524" layer="91"/>
<junction x="259.08" y="195.58"/>
<wire x1="226.06" y1="200.66" x2="238.76" y2="200.66" width="0.1524" layer="91"/>
<wire x1="238.76" y1="200.66" x2="238.76" y2="210.82" width="0.1524" layer="91"/>
<junction x="238.76" y="210.82"/>
<wire x1="226.06" y1="190.5" x2="238.76" y2="190.5" width="0.1524" layer="91"/>
<wire x1="238.76" y1="190.5" x2="238.76" y2="200.66" width="0.1524" layer="91"/>
<junction x="238.76" y="200.66"/>
<pinref part="R162" gate="G$1" pin="2"/>
<pinref part="R159" gate="G$1" pin="2"/>
<pinref part="R164" gate="G$1" pin="2"/>
</segment>
</net>
<net name="VIN" class="0">
<segment>
<wire x1="314.96" y1="210.82" x2="322.58" y2="210.82" width="0.1524" layer="91"/>
<label x="340.36" y="210.82" size="1.27" layer="95" xref="yes"/>
<pinref part="U1" gate="G$1" pin="7_OUT"/>
<wire x1="322.58" y1="210.82" x2="332.74" y2="210.82" width="0.1524" layer="91"/>
<wire x1="332.74" y1="210.82" x2="340.36" y2="210.82" width="0.1524" layer="91"/>
<wire x1="287.02" y1="187.96" x2="287.02" y2="195.58" width="0.1524" layer="91"/>
<wire x1="287.02" y1="195.58" x2="302.26" y2="195.58" width="0.1524" layer="91"/>
<wire x1="302.26" y1="195.58" x2="302.26" y2="210.82" width="0.1524" layer="91"/>
<pinref part="Q10" gate="G$1" pin="S"/>
<pinref part="R33" gate="G$1" pin="1"/>
<wire x1="307.34" y1="205.74" x2="307.34" y2="210.82" width="0.1524" layer="91"/>
<wire x1="307.34" y1="210.82" x2="302.26" y2="210.82" width="0.1524" layer="91"/>
<junction x="302.26" y="210.82"/>
<pinref part="C17" gate="G$1" pin="2"/>
<wire x1="314.96" y1="208.28" x2="314.96" y2="210.82" width="0.1524" layer="91"/>
<wire x1="314.96" y1="210.82" x2="307.34" y2="210.82" width="0.1524" layer="91"/>
<junction x="307.34" y="210.82"/>
<junction x="314.96" y="210.82"/>
<pinref part="C18" gate="G$1" pin="2"/>
<wire x1="322.58" y1="208.28" x2="322.58" y2="210.82" width="0.1524" layer="91"/>
<junction x="322.58" y="210.82"/>
<wire x1="332.74" y1="207.01" x2="332.74" y2="210.82" width="0.1524" layer="91"/>
<junction x="332.74" y="210.82"/>
<pinref part="D16" gate="G$1" pin="K"/>
</segment>
</net>
<net name="N$88" class="0">
<segment>
<pinref part="R29" gate="G$1" pin="2"/>
<wire x1="266.7" y1="165.1" x2="266.7" y2="162.56" width="0.1524" layer="91"/>
<wire x1="266.7" y1="162.56" x2="276.86" y2="162.56" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="3_PROG"/>
<wire x1="276.86" y1="162.56" x2="276.86" y2="172.72" width="0.1524" layer="91"/>
<pinref part="R32" gate="G$1" pin="1"/>
<wire x1="269.24" y1="157.48" x2="266.7" y2="157.48" width="0.1524" layer="91"/>
<wire x1="266.7" y1="157.48" x2="266.7" y2="162.56" width="0.1524" layer="91"/>
<junction x="266.7" y="162.56"/>
</segment>
</net>
<net name="N$89" class="0">
<segment>
<pinref part="R27" gate="G$1" pin="2"/>
<pinref part="R28" gate="G$1" pin="1"/>
<wire x1="246.38" y1="190.5" x2="246.38" y2="180.34" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="1_EN"/>
<wire x1="246.38" y1="180.34" x2="246.38" y2="172.72" width="0.1524" layer="91"/>
<wire x1="269.24" y1="180.34" x2="246.38" y2="180.34" width="0.1524" layer="91"/>
<junction x="246.38" y="180.34"/>
</segment>
</net>
<net name="N$90" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="2_VREF"/>
<wire x1="269.24" y1="177.8" x2="266.7" y2="177.8" width="0.1524" layer="91"/>
<pinref part="R29" gate="G$1" pin="1"/>
<wire x1="266.7" y1="177.8" x2="266.7" y2="175.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$91" class="0">
<segment>
<pinref part="C14" gate="G$1" pin="2"/>
<pinref part="U1" gate="G$1" pin="4_TIMER"/>
<wire x1="281.94" y1="170.18" x2="281.94" y2="172.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$92" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="6_PG"/>
<pinref part="R33" gate="G$1" pin="2"/>
<wire x1="294.64" y1="182.88" x2="307.34" y2="182.88" width="0.1524" layer="91"/>
<wire x1="307.34" y1="182.88" x2="307.34" y2="195.58" width="0.1524" layer="91"/>
<wire x1="307.34" y1="182.88" x2="312.42" y2="182.88" width="0.1524" layer="91"/>
<junction x="307.34" y="182.88"/>
<pinref part="R35" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$94" class="0">
<segment>
<pinref part="R30" gate="G$1" pin="2"/>
<pinref part="C13" gate="G$1" pin="2"/>
<wire x1="297.18" y1="170.18" x2="297.18" y2="167.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$95" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="8_GATE"/>
<pinref part="R31" gate="G$1" pin="2"/>
<wire x1="281.94" y1="187.96" x2="281.94" y2="190.5" width="0.1524" layer="91"/>
<pinref part="R30" gate="G$1" pin="1"/>
<wire x1="281.94" y1="190.5" x2="281.94" y2="200.66" width="0.1524" layer="91"/>
<wire x1="281.94" y1="200.66" x2="284.48" y2="200.66" width="0.1524" layer="91"/>
<wire x1="281.94" y1="190.5" x2="297.18" y2="190.5" width="0.1524" layer="91"/>
<wire x1="297.18" y1="190.5" x2="297.18" y2="180.34" width="0.1524" layer="91"/>
<junction x="281.94" y="190.5"/>
</segment>
</net>
<net name="N$96" class="0">
<segment>
<pinref part="R24" gate="G$1" pin="2"/>
<wire x1="271.78" y1="210.82" x2="276.86" y2="210.82" width="0.1524" layer="91"/>
<pinref part="Q10" gate="G$1" pin="D"/>
<wire x1="276.86" y1="208.28" x2="276.86" y2="210.82" width="0.1524" layer="91"/>
<wire x1="276.86" y1="210.82" x2="287.02" y2="210.82" width="0.1524" layer="91"/>
<junction x="276.86" y="210.82"/>
<pinref part="R25" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$97" class="0">
<segment>
<pinref part="R31" gate="G$1" pin="1"/>
<pinref part="Q10" gate="G$1" pin="G"/>
<wire x1="294.64" y1="200.66" x2="297.18" y2="200.66" width="0.1524" layer="91"/>
<wire x1="297.18" y1="200.66" x2="297.18" y2="203.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$98" class="0">
<segment>
<pinref part="R25" gate="G$1" pin="1"/>
<pinref part="U1" gate="G$1" pin="9_SENSE"/>
<wire x1="276.86" y1="187.96" x2="276.86" y2="195.58" width="0.1524" layer="91"/>
<pinref part="R26" gate="G$1" pin="2"/>
<wire x1="276.86" y1="195.58" x2="276.86" y2="198.12" width="0.1524" layer="91"/>
<wire x1="271.78" y1="195.58" x2="276.86" y2="195.58" width="0.1524" layer="91"/>
<junction x="276.86" y="195.58"/>
</segment>
</net>
<net name="PG_OUT" class="0">
<segment>
<pinref part="R35" gate="G$1" pin="2"/>
<wire x1="322.58" y1="182.88" x2="325.12" y2="182.88" width="0.1524" layer="91"/>
<pinref part="TP7" gate="G$1" pin="PP"/>
<wire x1="325.12" y1="185.42" x2="325.12" y2="182.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="AISG_3_N" class="0">
<segment>
<pinref part="R107" gate="G$1" pin="1"/>
<wire x1="43.18" y1="83.82" x2="22.86" y2="83.82" width="0.1524" layer="91"/>
<pinref part="J3" gate="-2" pin="1"/>
<label x="27.94" y="83.82" size="1.778" layer="95"/>
</segment>
</net>
<net name="AISG_3_P" class="0">
<segment>
<pinref part="R100" gate="G$1" pin="1"/>
<wire x1="43.18" y1="81.28" x2="22.86" y2="81.28" width="0.1524" layer="91"/>
<pinref part="J3" gate="-3" pin="1"/>
<label x="27.94" y="81.28" size="1.778" layer="95"/>
</segment>
</net>
<net name="INTERFACE_1" class="0">
<segment>
<label x="200.66" y="210.82" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="R162" gate="G$1" pin="1"/>
<wire x1="215.9" y1="210.82" x2="200.66" y2="210.82" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="104.14" y1="223.52" x2="106.68" y2="223.52" width="0.1524" layer="91"/>
<label x="93.98" y="223.52" size="1.27" layer="95" align="bottom-right"/>
<label x="106.68" y="223.52" size="1.27" layer="95" xref="yes"/>
<pinref part="D1" gate="G$1" pin="K"/>
<pinref part="TP2" gate="G$1" pin="PP"/>
<wire x1="62.23" y1="223.52" x2="104.14" y2="223.52" width="0.1524" layer="91"/>
<wire x1="104.14" y1="223.52" x2="104.14" y2="226.06" width="0.1524" layer="91"/>
<junction x="104.14" y="223.52"/>
</segment>
</net>
<net name="INTERFACE_2" class="0">
<segment>
<label x="200.66" y="200.66" size="1.27" layer="95" rot="R180" xref="yes"/>
<wire x1="215.9" y1="200.66" x2="200.66" y2="200.66" width="0.1524" layer="91"/>
<pinref part="R159" gate="G$1" pin="1"/>
</segment>
<segment>
<label x="91.44" y="152.4" size="1.27" layer="95" align="bottom-right"/>
<label x="106.68" y="152.4" size="1.27" layer="95" xref="yes"/>
<pinref part="D2" gate="G$1" pin="K"/>
<wire x1="104.14" y1="152.4" x2="106.68" y2="152.4" width="0.1524" layer="91"/>
<wire x1="62.23" y1="152.4" x2="104.14" y2="152.4" width="0.1524" layer="91"/>
<pinref part="TP3" gate="G$1" pin="PP"/>
<wire x1="104.14" y1="154.94" x2="104.14" y2="152.4" width="0.1524" layer="91"/>
<junction x="104.14" y="152.4"/>
</segment>
</net>
<net name="INTERFACE_3" class="0">
<segment>
<label x="200.66" y="190.5" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="R164" gate="G$1" pin="1"/>
<wire x1="215.9" y1="190.5" x2="200.66" y2="190.5" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="104.14" y1="86.36" x2="106.68" y2="86.36" width="0.1524" layer="91"/>
<label x="91.44" y="86.36" size="1.27" layer="95" align="bottom-right"/>
<label x="106.68" y="86.36" size="1.27" layer="95" xref="yes"/>
<pinref part="D3" gate="G$1" pin="K"/>
<wire x1="62.23" y1="86.36" x2="104.14" y2="86.36" width="0.1524" layer="91"/>
<pinref part="TP1" gate="G$1" pin="PP"/>
<wire x1="104.14" y1="88.9" x2="104.14" y2="86.36" width="0.1524" layer="91"/>
<junction x="104.14" y="86.36"/>
</segment>
</net>
<net name="INTERFACE_3_RS485_N" class="0">
<segment>
<pinref part="R107" gate="G$1" pin="2"/>
<wire x1="53.34" y1="83.82" x2="76.2" y2="83.82" width="0.1524" layer="91"/>
<label x="106.68" y="83.82" size="1.27" layer="95" xref="yes"/>
<label x="91.44" y="83.82" size="1.27" layer="95" align="bottom-right"/>
<wire x1="76.2" y1="83.82" x2="106.68" y2="83.82" width="0.1524" layer="91"/>
<wire x1="76.2" y1="76.2" x2="76.2" y2="83.82" width="0.1524" layer="91"/>
<junction x="76.2" y="83.82"/>
<pinref part="TP25" gate="G$1" pin="PP"/>
</segment>
</net>
<net name="INTERFACE_3_RS485_P" class="0">
<segment>
<pinref part="R100" gate="G$1" pin="2"/>
<wire x1="53.34" y1="81.28" x2="78.74" y2="81.28" width="0.1524" layer="91"/>
<label x="106.68" y="81.28" size="1.27" layer="95" xref="yes"/>
<label x="91.44" y="81.28" size="1.27" layer="95" align="bottom-right"/>
<wire x1="78.74" y1="81.28" x2="106.68" y2="81.28" width="0.1524" layer="91"/>
<wire x1="78.74" y1="73.66" x2="78.74" y2="81.28" width="0.1524" layer="91"/>
<junction x="78.74" y="81.28"/>
<pinref part="TP22" gate="G$1" pin="PP"/>
</segment>
</net>
<net name="AISG_1_VIN" class="0">
<segment>
<wire x1="22.86" y1="223.52" x2="33.02" y2="223.52" width="0.1524" layer="91"/>
<pinref part="D23" gate="G$1" pin="K"/>
<wire x1="33.02" y1="212.09" x2="33.02" y2="223.52" width="0.1524" layer="91"/>
<pinref part="D1" gate="G$1" pin="A"/>
<wire x1="33.02" y1="223.52" x2="54.61" y2="223.52" width="0.1524" layer="91"/>
<junction x="33.02" y="223.52"/>
<label x="27.94" y="223.52" size="1.27" layer="95"/>
<pinref part="J1" gate="-1" pin="1"/>
</segment>
</net>
<net name="AISG_2_VIN" class="0">
<segment>
<wire x1="22.86" y1="152.4" x2="33.02" y2="152.4" width="0.1524" layer="91"/>
<pinref part="D24" gate="G$1" pin="K"/>
<wire x1="33.02" y1="140.97" x2="33.02" y2="152.4" width="0.1524" layer="91"/>
<pinref part="D2" gate="G$1" pin="A"/>
<wire x1="54.61" y1="152.4" x2="33.02" y2="152.4" width="0.1524" layer="91"/>
<junction x="33.02" y="152.4"/>
<label x="30.48" y="152.4" size="1.27" layer="95"/>
<pinref part="J2" gate="-1" pin="1"/>
</segment>
</net>
<net name="AISG_3_VIN" class="0">
<segment>
<wire x1="22.86" y1="86.36" x2="33.02" y2="86.36" width="0.1524" layer="91"/>
<pinref part="D22" gate="G$1" pin="K"/>
<wire x1="33.02" y1="69.85" x2="33.02" y2="86.36" width="0.1524" layer="91"/>
<pinref part="D3" gate="G$1" pin="A"/>
<wire x1="33.02" y1="86.36" x2="54.61" y2="86.36" width="0.1524" layer="91"/>
<junction x="33.02" y="86.36"/>
<label x="30.48" y="86.36" size="1.27" layer="95"/>
<pinref part="J3" gate="-1" pin="1"/>
</segment>
</net>
<net name="INTERFACE_1_RS485_N" class="0">
<segment>
<pinref part="R108" gate="G$1" pin="2"/>
<wire x1="53.34" y1="220.98" x2="76.2" y2="220.98" width="0.1524" layer="91"/>
<label x="106.68" y="220.98" size="1.27" layer="95" xref="yes"/>
<label x="93.98" y="220.98" size="1.27" layer="95" align="bottom-right"/>
<wire x1="76.2" y1="220.98" x2="106.68" y2="220.98" width="0.1524" layer="91"/>
<wire x1="76.2" y1="213.36" x2="76.2" y2="220.98" width="0.1524" layer="91"/>
<junction x="76.2" y="220.98"/>
<pinref part="TP26" gate="G$1" pin="PP"/>
</segment>
</net>
<net name="INTERFACE_2_RS485_N" class="0">
<segment>
<pinref part="R110" gate="G$1" pin="2"/>
<wire x1="53.34" y1="149.86" x2="76.2" y2="149.86" width="0.1524" layer="91"/>
<label x="106.68" y="149.86" size="1.27" layer="95" xref="yes"/>
<label x="91.44" y="149.86" size="1.27" layer="95" align="bottom-right"/>
<wire x1="76.2" y1="149.86" x2="106.68" y2="149.86" width="0.1524" layer="91"/>
<wire x1="76.2" y1="142.24" x2="76.2" y2="149.86" width="0.1524" layer="91"/>
<junction x="76.2" y="149.86"/>
<pinref part="TP27" gate="G$1" pin="PP"/>
</segment>
</net>
<net name="INTERFACE_1_RS485_P" class="0">
<segment>
<pinref part="R102" gate="G$1" pin="2"/>
<wire x1="53.34" y1="218.44" x2="78.74" y2="218.44" width="0.1524" layer="91"/>
<label x="106.68" y="218.44" size="1.27" layer="95" xref="yes"/>
<label x="93.98" y="218.44" size="1.27" layer="95" align="bottom-right"/>
<wire x1="78.74" y1="218.44" x2="106.68" y2="218.44" width="0.1524" layer="91"/>
<wire x1="78.74" y1="210.82" x2="78.74" y2="218.44" width="0.1524" layer="91"/>
<junction x="78.74" y="218.44"/>
<pinref part="TP23" gate="G$1" pin="PP"/>
</segment>
</net>
<net name="INTERFACE_2_RS485_P" class="0">
<segment>
<pinref part="R105" gate="G$1" pin="2"/>
<wire x1="53.34" y1="147.32" x2="78.74" y2="147.32" width="0.1524" layer="91"/>
<label x="106.68" y="147.32" size="1.27" layer="95" xref="yes"/>
<label x="91.44" y="147.32" size="1.27" layer="95" align="bottom-right"/>
<wire x1="78.74" y1="147.32" x2="106.68" y2="147.32" width="0.1524" layer="91"/>
<wire x1="78.74" y1="139.7" x2="78.74" y2="147.32" width="0.1524" layer="91"/>
<junction x="78.74" y="147.32"/>
<pinref part="TP24" gate="G$1" pin="PP"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
</plain>
<instances>
<instance part="C1" gate="G$1" x="88.9" y="218.44" smashed="yes" rot="R90">
<attribute name="NAME" x="88.392" y="213.36" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="88.392" y="219.71" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="C6" gate="G$1" x="144.78" y="218.44" smashed="yes" rot="R90">
<attribute name="NAME" x="144.272" y="213.36" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="144.272" y="219.71" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="C8" gate="G$1" x="167.64" y="218.44" smashed="yes" rot="R90">
<attribute name="NAME" x="167.132" y="213.36" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="167.132" y="219.71" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R12" gate="G$1" x="124.46" y="218.44" smashed="yes" rot="R90">
<attribute name="NAME" x="123.952" y="211.582" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="123.952" y="220.98" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R18" gate="G$1" x="129.54" y="203.2" smashed="yes" rot="R90">
<attribute name="NAME" x="131.064" y="204.1906" size="1.27" layer="95"/>
<attribute name="VALUE" x="131.064" y="202.184" size="1.27" layer="96"/>
</instance>
<instance part="Q9" gate="G$1" x="157.48" y="223.52" smashed="yes" rot="MR90">
<attribute name="NAME" x="165.1" y="233.68" size="1.27" layer="95" rot="R180"/>
<attribute name="VALUE" x="165.1" y="231.14" size="1.27" layer="96" rot="R180"/>
</instance>
<instance part="Q1" gate="G$1" x="99.06" y="223.52" smashed="yes" rot="R90">
<attribute name="NAME" x="106.426" y="226.568" size="1.27" layer="95" rot="MR0"/>
<attribute name="VALUE" x="106.426" y="228.854" size="1.27" layer="96" rot="MR0"/>
</instance>
<instance part="Q7" gate="G$1" x="127" y="190.5" smashed="yes">
<attribute name="NAME" x="134.62" y="190.5" size="1.27" layer="95"/>
<attribute name="VALUE" x="134.62" y="187.96" size="1.27" layer="96"/>
</instance>
<instance part="R15" gate="G$1" x="121.92" y="180.34" smashed="yes">
<attribute name="NAME" x="119.634" y="182.0926" size="1.27" layer="95"/>
<attribute name="VALUE" x="119.634" y="177.8" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY47" gate="GND" x="129.54" y="175.26" smashed="yes">
<attribute name="VALUE" x="132.08" y="175.26" size="1.27" layer="96"/>
</instance>
<instance part="R4" gate="G$1" x="91.44" y="203.2" smashed="yes" rot="R90">
<attribute name="NAME" x="92.964" y="204.1906" size="1.27" layer="95"/>
<attribute name="VALUE" x="92.964" y="202.438" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY48" gate="GND" x="91.44" y="193.04" smashed="yes">
<attribute name="VALUE" x="92.837" y="193.167" size="1.27" layer="96"/>
</instance>
<instance part="R9" gate="G$1" x="116.84" y="210.82" smashed="yes" rot="R180">
<attribute name="NAME" x="114.554" y="212.3186" size="1.27" layer="95"/>
<attribute name="VALUE" x="114.554" y="208.28" size="1.27" layer="96"/>
</instance>
<instance part="C11" gate="G$1" x="175.26" y="218.44" smashed="yes" rot="R90">
<attribute name="NAME" x="174.752" y="213.36" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="174.752" y="219.71" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R1" gate="G$1" x="99.06" y="231.14" smashed="yes" rot="R180">
<attribute name="NAME" x="91.694" y="231.6226" size="1.27" layer="95"/>
<attribute name="VALUE" x="101.6" y="231.648" size="1.27" layer="96"/>
</instance>
<instance part="D4" gate="G$1" x="109.22" y="218.44" smashed="yes" rot="R90">
<attribute name="NAME" x="107.315" y="215.9" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="113.03" y="215.9" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="D9" gate="G$1" x="134.62" y="218.44" smashed="yes" rot="R90">
<attribute name="NAME" x="132.715" y="215.9" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="138.43" y="215.9" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="D12" gate="G$1" x="187.96" y="226.06" smashed="yes">
<attribute name="NAME" x="185.42" y="223.52" size="1.27" layer="95"/>
<attribute name="VALUE" x="185.42" y="220.98" size="1.27" layer="96"/>
</instance>
<instance part="C2" gate="G$1" x="88.9" y="154.94" smashed="yes" rot="R90">
<attribute name="NAME" x="88.392" y="149.86" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="88.392" y="156.21" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="C5" gate="G$1" x="144.78" y="154.94" smashed="yes" rot="R90">
<attribute name="NAME" x="144.272" y="149.86" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="144.272" y="156.21" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="C9" gate="G$1" x="167.64" y="154.94" smashed="yes" rot="R90">
<attribute name="NAME" x="167.132" y="149.86" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="167.132" y="156.21" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R7" gate="G$1" x="124.46" y="154.94" smashed="yes" rot="R90">
<attribute name="NAME" x="123.952" y="148.082" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="123.952" y="157.48" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R16" gate="G$1" x="129.54" y="139.7" smashed="yes" rot="R90">
<attribute name="NAME" x="131.064" y="140.6906" size="1.27" layer="95"/>
<attribute name="VALUE" x="131.064" y="138.684" size="1.27" layer="96"/>
</instance>
<instance part="Q8" gate="G$1" x="157.48" y="160.02" smashed="yes" rot="MR90">
<attribute name="NAME" x="165.1" y="170.18" size="1.27" layer="95" rot="R180"/>
<attribute name="VALUE" x="165.1" y="167.64" size="1.27" layer="96" rot="R180"/>
</instance>
<instance part="Q2" gate="G$1" x="99.06" y="160.02" smashed="yes" rot="R90">
<attribute name="NAME" x="106.426" y="163.068" size="1.27" layer="95" rot="MR0"/>
<attribute name="VALUE" x="106.426" y="165.354" size="1.27" layer="96" rot="MR0"/>
</instance>
<instance part="Q4" gate="G$1" x="127" y="127" smashed="yes">
<attribute name="NAME" x="134.62" y="127" size="1.27" layer="95"/>
<attribute name="VALUE" x="134.62" y="124.46" size="1.27" layer="96"/>
</instance>
<instance part="R13" gate="G$1" x="121.92" y="116.84" smashed="yes">
<attribute name="NAME" x="119.634" y="118.5926" size="1.27" layer="95"/>
<attribute name="VALUE" x="119.634" y="114.3" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY51" gate="GND" x="129.54" y="111.76" smashed="yes">
<attribute name="VALUE" x="132.08" y="111.76" size="1.27" layer="96"/>
</instance>
<instance part="R5" gate="G$1" x="91.44" y="139.7" smashed="yes" rot="R90">
<attribute name="NAME" x="92.964" y="140.6906" size="1.27" layer="95"/>
<attribute name="VALUE" x="92.964" y="138.938" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY52" gate="GND" x="91.44" y="129.54" smashed="yes">
<attribute name="VALUE" x="92.837" y="129.667" size="1.27" layer="96"/>
</instance>
<instance part="R10" gate="G$1" x="116.84" y="147.32" smashed="yes" rot="R180">
<attribute name="NAME" x="114.554" y="148.8186" size="1.27" layer="95"/>
<attribute name="VALUE" x="114.554" y="144.78" size="1.27" layer="96"/>
</instance>
<instance part="C12" gate="G$1" x="175.26" y="154.94" smashed="yes" rot="R90">
<attribute name="NAME" x="174.752" y="149.86" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="174.752" y="156.21" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R2" gate="G$1" x="99.06" y="167.64" smashed="yes" rot="R180">
<attribute name="NAME" x="91.694" y="168.1226" size="1.27" layer="95"/>
<attribute name="VALUE" x="101.6" y="168.148" size="1.27" layer="96"/>
</instance>
<instance part="D5" gate="G$1" x="109.22" y="154.94" smashed="yes" rot="R90">
<attribute name="NAME" x="107.315" y="152.4" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="113.03" y="152.4" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="D7" gate="G$1" x="134.62" y="154.94" smashed="yes" rot="R90">
<attribute name="NAME" x="132.715" y="152.4" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="138.43" y="152.4" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="D11" gate="G$1" x="187.96" y="162.56" smashed="yes">
<attribute name="NAME" x="185.42" y="157.48" size="1.27" layer="95"/>
<attribute name="VALUE" x="185.42" y="154.94" size="1.27" layer="96"/>
</instance>
<instance part="C3" gate="G$1" x="88.9" y="96.52" smashed="yes" rot="R90">
<attribute name="NAME" x="88.392" y="91.44" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="88.392" y="97.79" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="C4" gate="G$1" x="144.78" y="96.52" smashed="yes" rot="R90">
<attribute name="NAME" x="144.272" y="91.44" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="144.272" y="97.79" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="C7" gate="G$1" x="167.64" y="96.52" smashed="yes" rot="R90">
<attribute name="NAME" x="167.132" y="91.44" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="167.132" y="97.79" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R8" gate="G$1" x="124.46" y="96.52" smashed="yes" rot="R90">
<attribute name="NAME" x="123.952" y="89.662" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="123.952" y="99.06" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R17" gate="G$1" x="129.54" y="81.28" smashed="yes" rot="R90">
<attribute name="NAME" x="131.064" y="82.2706" size="1.27" layer="95"/>
<attribute name="VALUE" x="131.064" y="80.264" size="1.27" layer="96"/>
</instance>
<instance part="Q6" gate="G$1" x="157.48" y="101.6" smashed="yes" rot="MR90">
<attribute name="NAME" x="165.1" y="111.76" size="1.27" layer="95" rot="R180"/>
<attribute name="VALUE" x="165.1" y="109.22" size="1.27" layer="96" rot="R180"/>
</instance>
<instance part="Q3" gate="G$1" x="99.06" y="101.6" smashed="yes" rot="R90">
<attribute name="NAME" x="106.426" y="104.648" size="1.27" layer="95" rot="MR0"/>
<attribute name="VALUE" x="106.426" y="106.934" size="1.27" layer="96" rot="MR0"/>
</instance>
<instance part="Q5" gate="G$1" x="127" y="68.58" smashed="yes">
<attribute name="NAME" x="134.62" y="68.58" size="1.27" layer="95"/>
<attribute name="VALUE" x="134.62" y="66.04" size="1.27" layer="96"/>
</instance>
<instance part="R14" gate="G$1" x="121.92" y="58.42" smashed="yes">
<attribute name="NAME" x="119.634" y="60.1726" size="1.27" layer="95"/>
</instance>
<instance part="SUPPLY49" gate="GND" x="129.54" y="53.34" smashed="yes">
<attribute name="VALUE" x="132.08" y="53.34" size="1.27" layer="96"/>
</instance>
<instance part="R6" gate="G$1" x="91.44" y="81.28" smashed="yes" rot="R90">
<attribute name="NAME" x="92.964" y="82.2706" size="1.27" layer="95"/>
<attribute name="VALUE" x="92.964" y="80.518" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY50" gate="GND" x="91.44" y="71.12" smashed="yes">
<attribute name="VALUE" x="92.837" y="71.247" size="1.27" layer="96"/>
</instance>
<instance part="R11" gate="G$1" x="116.84" y="88.9" smashed="yes" rot="R180">
<attribute name="NAME" x="114.554" y="90.3986" size="1.27" layer="95"/>
<attribute name="VALUE" x="114.554" y="86.36" size="1.27" layer="96"/>
</instance>
<instance part="C10" gate="G$1" x="175.26" y="96.52" smashed="yes" rot="R90">
<attribute name="NAME" x="174.752" y="91.44" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="174.752" y="97.79" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R3" gate="G$1" x="99.06" y="109.22" smashed="yes" rot="R180">
<attribute name="NAME" x="91.694" y="109.7026" size="1.27" layer="95"/>
<attribute name="VALUE" x="101.6" y="109.728" size="1.27" layer="96"/>
</instance>
<instance part="D6" gate="G$1" x="109.22" y="96.52" smashed="yes" rot="R90">
<attribute name="NAME" x="107.315" y="93.98" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="113.03" y="93.98" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="D8" gate="G$1" x="134.62" y="96.52" smashed="yes" rot="R90">
<attribute name="NAME" x="132.715" y="93.98" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="138.43" y="93.98" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="D10" gate="G$1" x="187.96" y="104.14" smashed="yes">
<attribute name="NAME" x="185.42" y="101.6" size="1.27" layer="95"/>
<attribute name="VALUE" x="185.42" y="99.06" size="1.27" layer="96"/>
</instance>
<instance part="R22" gate="G$1" x="233.68" y="134.62" smashed="yes" rot="R90">
<attribute name="NAME" x="232.156" y="132.08" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="236.982" y="132.08" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R23" gate="G$1" x="236.22" y="203.2" smashed="yes" rot="R90">
<attribute name="NAME" x="234.696" y="200.66" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="239.522" y="200.66" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="TP5" gate="G$1" x="248.92" y="162.56" smashed="yes" rot="R270">
<attribute name="NAME" x="252.73" y="163.83" size="1.27" layer="95" rot="R270"/>
</instance>
<instance part="TP4" gate="G$1" x="248.92" y="104.14" smashed="yes" rot="R270">
<attribute name="NAME" x="252.73" y="105.41" size="1.27" layer="95" rot="R270"/>
</instance>
<instance part="TP6" gate="G$1" x="248.92" y="220.98" smashed="yes" rot="R270">
<attribute name="NAME" x="252.73" y="222.25" size="1.27" layer="95" rot="R270"/>
</instance>
<instance part="R20" gate="G$1" x="187.96" y="231.14" smashed="yes">
<attribute name="NAME" x="185.42" y="233.68" size="1.778" layer="95"/>
<attribute name="VALUE" x="185.42" y="236.22" size="1.778" layer="96"/>
</instance>
<instance part="R19" gate="G$1" x="187.96" y="167.64" smashed="yes">
<attribute name="NAME" x="185.42" y="172.72" size="1.778" layer="95"/>
<attribute name="VALUE" x="185.42" y="170.18" size="1.778" layer="96"/>
</instance>
<instance part="R21" gate="G$1" x="187.96" y="109.22" smashed="yes">
<attribute name="NAME" x="185.42" y="114.3" size="1.778" layer="95"/>
<attribute name="VALUE" x="185.42" y="111.76" size="1.778" layer="96"/>
</instance>
<instance part="R163" gate="G$1" x="104.14" y="187.96" smashed="yes">
<attribute name="NAME" x="101.854" y="189.7126" size="1.27" layer="95"/>
<attribute name="VALUE" x="101.854" y="185.42" size="1.27" layer="96"/>
</instance>
<instance part="R160" gate="G$1" x="104.14" y="124.46" smashed="yes">
<attribute name="NAME" x="101.854" y="126.2126" size="1.27" layer="95"/>
<attribute name="VALUE" x="101.854" y="121.92" size="1.27" layer="96"/>
</instance>
<instance part="R161" gate="G$1" x="101.6" y="66.04" smashed="yes">
<attribute name="NAME" x="99.314" y="67.7926" size="1.27" layer="95"/>
<attribute name="VALUE" x="99.314" y="63.5" size="1.27" layer="96"/>
</instance>
<instance part="FRAME1" gate="G$1" x="0" y="0" smashed="yes">
<attribute name="DRAWING_NAME" x="344.17" y="15.24" size="2.54" layer="94"/>
<attribute name="LAST_DATE_TIME" x="344.17" y="10.16" size="2.286" layer="94"/>
<attribute name="SHEET" x="357.505" y="5.08" size="2.54" layer="94"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="Q7" gate="G$1" pin="S"/>
<wire x1="129.54" y1="182.88" x2="129.54" y2="180.34" width="0.1524" layer="91"/>
<pinref part="SUPPLY47" gate="GND" pin="GND"/>
<pinref part="R15" gate="G$1" pin="2"/>
<wire x1="129.54" y1="180.34" x2="129.54" y2="177.8" width="0.1524" layer="91"/>
<wire x1="127" y1="180.34" x2="129.54" y2="180.34" width="0.1524" layer="91"/>
<junction x="129.54" y="180.34"/>
</segment>
<segment>
<pinref part="R4" gate="G$1" pin="1"/>
<wire x1="91.44" y1="198.12" x2="91.44" y2="195.58" width="0.1524" layer="91"/>
<pinref part="SUPPLY48" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="Q4" gate="G$1" pin="S"/>
<wire x1="129.54" y1="119.38" x2="129.54" y2="116.84" width="0.1524" layer="91"/>
<pinref part="SUPPLY51" gate="GND" pin="GND"/>
<pinref part="R13" gate="G$1" pin="2"/>
<wire x1="129.54" y1="116.84" x2="129.54" y2="114.3" width="0.1524" layer="91"/>
<wire x1="127" y1="116.84" x2="129.54" y2="116.84" width="0.1524" layer="91"/>
<junction x="129.54" y="116.84"/>
</segment>
<segment>
<pinref part="R5" gate="G$1" pin="1"/>
<wire x1="91.44" y1="134.62" x2="91.44" y2="132.08" width="0.1524" layer="91"/>
<pinref part="SUPPLY52" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="Q5" gate="G$1" pin="S"/>
<wire x1="129.54" y1="60.96" x2="129.54" y2="58.42" width="0.1524" layer="91"/>
<pinref part="SUPPLY49" gate="GND" pin="GND"/>
<pinref part="R14" gate="G$1" pin="2"/>
<wire x1="129.54" y1="58.42" x2="129.54" y2="55.88" width="0.1524" layer="91"/>
<wire x1="127" y1="58.42" x2="129.54" y2="58.42" width="0.1524" layer="91"/>
<junction x="129.54" y="58.42"/>
</segment>
<segment>
<pinref part="R6" gate="G$1" pin="1"/>
<wire x1="91.44" y1="76.2" x2="91.44" y2="73.66" width="0.1524" layer="91"/>
<pinref part="SUPPLY50" gate="GND" pin="GND"/>
</segment>
</net>
<net name="N$51" class="0">
<segment>
<pinref part="Q9" gate="G$1" pin="S"/>
<pinref part="R12" gate="G$1" pin="2"/>
<wire x1="124.46" y1="226.06" x2="134.62" y2="226.06" width="0.1524" layer="91"/>
<wire x1="134.62" y1="226.06" x2="144.78" y2="226.06" width="0.1524" layer="91"/>
<wire x1="144.78" y1="226.06" x2="149.86" y2="226.06" width="0.1524" layer="91"/>
<wire x1="124.46" y1="223.52" x2="124.46" y2="226.06" width="0.1524" layer="91"/>
<pinref part="C6" gate="G$1" pin="2"/>
<wire x1="144.78" y1="223.52" x2="144.78" y2="226.06" width="0.1524" layer="91"/>
<junction x="144.78" y="226.06"/>
<pinref part="Q1" gate="G$1" pin="S"/>
<wire x1="106.68" y1="226.06" x2="109.22" y2="226.06" width="0.1524" layer="91"/>
<junction x="124.46" y="226.06"/>
<wire x1="109.22" y1="226.06" x2="111.76" y2="226.06" width="0.1524" layer="91"/>
<wire x1="111.76" y1="226.06" x2="124.46" y2="226.06" width="0.1524" layer="91"/>
<wire x1="109.22" y1="222.25" x2="109.22" y2="226.06" width="0.1524" layer="91"/>
<junction x="109.22" y="226.06"/>
<wire x1="134.62" y1="222.25" x2="134.62" y2="226.06" width="0.1524" layer="91"/>
<junction x="134.62" y="226.06"/>
<pinref part="R1" gate="G$1" pin="1"/>
<wire x1="104.14" y1="231.14" x2="111.76" y2="231.14" width="0.1524" layer="91"/>
<wire x1="111.76" y1="231.14" x2="111.76" y2="226.06" width="0.1524" layer="91"/>
<junction x="111.76" y="226.06"/>
<pinref part="D4" gate="G$1" pin="K"/>
<pinref part="D9" gate="G$1" pin="K"/>
</segment>
</net>
<net name="N$55" class="0">
<segment>
<pinref part="Q9" gate="G$1" pin="D"/>
<pinref part="C8" gate="G$1" pin="2"/>
<wire x1="165.1" y1="226.06" x2="167.64" y2="226.06" width="0.1524" layer="91"/>
<wire x1="167.64" y1="226.06" x2="167.64" y2="223.52" width="0.1524" layer="91"/>
<wire x1="167.64" y1="226.06" x2="175.26" y2="226.06" width="0.1524" layer="91"/>
<junction x="167.64" y="226.06"/>
<pinref part="C11" gate="G$1" pin="2"/>
<wire x1="175.26" y1="226.06" x2="180.34" y2="226.06" width="0.1524" layer="91"/>
<wire x1="180.34" y1="226.06" x2="184.15" y2="226.06" width="0.1524" layer="91"/>
<wire x1="175.26" y1="223.52" x2="175.26" y2="226.06" width="0.1524" layer="91"/>
<junction x="175.26" y="226.06"/>
<pinref part="D12" gate="G$1" pin="A"/>
<pinref part="R20" gate="G$1" pin="1"/>
<wire x1="182.88" y1="231.14" x2="180.34" y2="231.14" width="0.1524" layer="91"/>
<wire x1="180.34" y1="231.14" x2="180.34" y2="226.06" width="0.1524" layer="91"/>
<junction x="180.34" y="226.06"/>
</segment>
</net>
<net name="N$56" class="0">
<segment>
<pinref part="R12" gate="G$1" pin="1"/>
<wire x1="121.92" y1="210.82" x2="124.46" y2="210.82" width="0.1524" layer="91"/>
<wire x1="124.46" y1="210.82" x2="124.46" y2="213.36" width="0.1524" layer="91"/>
<pinref part="C6" gate="G$1" pin="1"/>
<wire x1="124.46" y1="210.82" x2="129.54" y2="210.82" width="0.1524" layer="91"/>
<wire x1="129.54" y1="210.82" x2="134.62" y2="210.82" width="0.1524" layer="91"/>
<wire x1="134.62" y1="210.82" x2="144.78" y2="210.82" width="0.1524" layer="91"/>
<wire x1="144.78" y1="210.82" x2="144.78" y2="213.36" width="0.1524" layer="91"/>
<junction x="124.46" y="210.82"/>
<pinref part="C8" gate="G$1" pin="1"/>
<wire x1="144.78" y1="210.82" x2="154.94" y2="210.82" width="0.1524" layer="91"/>
<wire x1="154.94" y1="210.82" x2="167.64" y2="210.82" width="0.1524" layer="91"/>
<wire x1="167.64" y1="210.82" x2="167.64" y2="213.36" width="0.1524" layer="91"/>
<junction x="144.78" y="210.82"/>
<pinref part="Q9" gate="G$1" pin="G"/>
<wire x1="154.94" y1="218.44" x2="154.94" y2="210.82" width="0.1524" layer="91"/>
<junction x="154.94" y="210.82"/>
<pinref part="R18" gate="G$1" pin="2"/>
<wire x1="129.54" y1="208.28" x2="129.54" y2="210.82" width="0.1524" layer="91"/>
<junction x="129.54" y="210.82"/>
<pinref part="R9" gate="G$1" pin="1"/>
<wire x1="134.62" y1="214.63" x2="134.62" y2="210.82" width="0.1524" layer="91"/>
<junction x="134.62" y="210.82"/>
<wire x1="167.64" y1="210.82" x2="175.26" y2="210.82" width="0.1524" layer="91"/>
<junction x="167.64" y="210.82"/>
<pinref part="C11" gate="G$1" pin="1"/>
<wire x1="175.26" y1="210.82" x2="175.26" y2="213.36" width="0.1524" layer="91"/>
<pinref part="D9" gate="G$1" pin="A"/>
</segment>
</net>
<net name="N$57" class="0">
<segment>
<pinref part="R18" gate="G$1" pin="1"/>
<pinref part="Q7" gate="G$1" pin="D"/>
</segment>
</net>
<net name="N$58" class="0">
<segment>
<pinref part="R9" gate="G$1" pin="2"/>
<wire x1="109.22" y1="210.82" x2="111.76" y2="210.82" width="0.1524" layer="91"/>
<wire x1="109.22" y1="214.63" x2="109.22" y2="210.82" width="0.1524" layer="91"/>
<junction x="109.22" y="210.82"/>
<wire x1="101.6" y1="210.82" x2="109.22" y2="210.82" width="0.1524" layer="91"/>
<pinref part="Q1" gate="G$1" pin="G"/>
<wire x1="101.6" y1="218.44" x2="101.6" y2="210.82" width="0.1524" layer="91"/>
<junction x="101.6" y="210.82"/>
<wire x1="91.44" y1="210.82" x2="101.6" y2="210.82" width="0.1524" layer="91"/>
<pinref part="R4" gate="G$1" pin="2"/>
<wire x1="91.44" y1="208.28" x2="91.44" y2="210.82" width="0.1524" layer="91"/>
<junction x="91.44" y="210.82"/>
<pinref part="C1" gate="G$1" pin="1"/>
<wire x1="88.9" y1="213.36" x2="88.9" y2="210.82" width="0.1524" layer="91"/>
<wire x1="88.9" y1="210.82" x2="91.44" y2="210.82" width="0.1524" layer="91"/>
<pinref part="D4" gate="G$1" pin="A"/>
</segment>
</net>
<net name="N$75" class="0">
<segment>
<pinref part="Q8" gate="G$1" pin="S"/>
<pinref part="R7" gate="G$1" pin="2"/>
<wire x1="124.46" y1="162.56" x2="134.62" y2="162.56" width="0.1524" layer="91"/>
<wire x1="134.62" y1="162.56" x2="144.78" y2="162.56" width="0.1524" layer="91"/>
<wire x1="144.78" y1="162.56" x2="149.86" y2="162.56" width="0.1524" layer="91"/>
<wire x1="124.46" y1="160.02" x2="124.46" y2="162.56" width="0.1524" layer="91"/>
<pinref part="C5" gate="G$1" pin="2"/>
<wire x1="144.78" y1="160.02" x2="144.78" y2="162.56" width="0.1524" layer="91"/>
<junction x="144.78" y="162.56"/>
<pinref part="Q2" gate="G$1" pin="S"/>
<wire x1="106.68" y1="162.56" x2="109.22" y2="162.56" width="0.1524" layer="91"/>
<junction x="124.46" y="162.56"/>
<wire x1="109.22" y1="162.56" x2="111.76" y2="162.56" width="0.1524" layer="91"/>
<wire x1="111.76" y1="162.56" x2="124.46" y2="162.56" width="0.1524" layer="91"/>
<wire x1="109.22" y1="158.75" x2="109.22" y2="162.56" width="0.1524" layer="91"/>
<junction x="109.22" y="162.56"/>
<wire x1="134.62" y1="158.75" x2="134.62" y2="162.56" width="0.1524" layer="91"/>
<junction x="134.62" y="162.56"/>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="104.14" y1="167.64" x2="111.76" y2="167.64" width="0.1524" layer="91"/>
<wire x1="111.76" y1="167.64" x2="111.76" y2="162.56" width="0.1524" layer="91"/>
<junction x="111.76" y="162.56"/>
<pinref part="D5" gate="G$1" pin="K"/>
<pinref part="D7" gate="G$1" pin="K"/>
</segment>
</net>
<net name="N$77" class="0">
<segment>
<pinref part="Q8" gate="G$1" pin="D"/>
<pinref part="C9" gate="G$1" pin="2"/>
<wire x1="165.1" y1="162.56" x2="167.64" y2="162.56" width="0.1524" layer="91"/>
<wire x1="167.64" y1="162.56" x2="167.64" y2="160.02" width="0.1524" layer="91"/>
<wire x1="167.64" y1="162.56" x2="175.26" y2="162.56" width="0.1524" layer="91"/>
<junction x="167.64" y="162.56"/>
<pinref part="C12" gate="G$1" pin="2"/>
<wire x1="175.26" y1="162.56" x2="180.34" y2="162.56" width="0.1524" layer="91"/>
<wire x1="180.34" y1="162.56" x2="184.15" y2="162.56" width="0.1524" layer="91"/>
<wire x1="175.26" y1="160.02" x2="175.26" y2="162.56" width="0.1524" layer="91"/>
<junction x="175.26" y="162.56"/>
<pinref part="D11" gate="G$1" pin="A"/>
<wire x1="180.34" y1="162.56" x2="180.34" y2="167.64" width="0.1524" layer="91"/>
<junction x="180.34" y="162.56"/>
<pinref part="R19" gate="G$1" pin="1"/>
<wire x1="180.34" y1="167.64" x2="182.88" y2="167.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$79" class="0">
<segment>
<pinref part="R7" gate="G$1" pin="1"/>
<wire x1="121.92" y1="147.32" x2="124.46" y2="147.32" width="0.1524" layer="91"/>
<wire x1="124.46" y1="147.32" x2="124.46" y2="149.86" width="0.1524" layer="91"/>
<pinref part="C5" gate="G$1" pin="1"/>
<wire x1="124.46" y1="147.32" x2="129.54" y2="147.32" width="0.1524" layer="91"/>
<wire x1="129.54" y1="147.32" x2="134.62" y2="147.32" width="0.1524" layer="91"/>
<wire x1="134.62" y1="147.32" x2="144.78" y2="147.32" width="0.1524" layer="91"/>
<wire x1="144.78" y1="147.32" x2="144.78" y2="149.86" width="0.1524" layer="91"/>
<junction x="124.46" y="147.32"/>
<pinref part="C9" gate="G$1" pin="1"/>
<wire x1="144.78" y1="147.32" x2="154.94" y2="147.32" width="0.1524" layer="91"/>
<wire x1="154.94" y1="147.32" x2="167.64" y2="147.32" width="0.1524" layer="91"/>
<wire x1="167.64" y1="147.32" x2="167.64" y2="149.86" width="0.1524" layer="91"/>
<junction x="144.78" y="147.32"/>
<pinref part="Q8" gate="G$1" pin="G"/>
<wire x1="154.94" y1="154.94" x2="154.94" y2="147.32" width="0.1524" layer="91"/>
<junction x="154.94" y="147.32"/>
<pinref part="R16" gate="G$1" pin="2"/>
<wire x1="129.54" y1="144.78" x2="129.54" y2="147.32" width="0.1524" layer="91"/>
<junction x="129.54" y="147.32"/>
<pinref part="R10" gate="G$1" pin="1"/>
<wire x1="134.62" y1="151.13" x2="134.62" y2="147.32" width="0.1524" layer="91"/>
<junction x="134.62" y="147.32"/>
<wire x1="167.64" y1="147.32" x2="175.26" y2="147.32" width="0.1524" layer="91"/>
<junction x="167.64" y="147.32"/>
<pinref part="C12" gate="G$1" pin="1"/>
<wire x1="175.26" y1="147.32" x2="175.26" y2="149.86" width="0.1524" layer="91"/>
<pinref part="D7" gate="G$1" pin="A"/>
</segment>
</net>
<net name="N$80" class="0">
<segment>
<pinref part="R16" gate="G$1" pin="1"/>
<pinref part="Q4" gate="G$1" pin="D"/>
</segment>
</net>
<net name="N$81" class="0">
<segment>
<pinref part="R10" gate="G$1" pin="2"/>
<wire x1="109.22" y1="147.32" x2="111.76" y2="147.32" width="0.1524" layer="91"/>
<wire x1="109.22" y1="151.13" x2="109.22" y2="147.32" width="0.1524" layer="91"/>
<junction x="109.22" y="147.32"/>
<wire x1="101.6" y1="147.32" x2="109.22" y2="147.32" width="0.1524" layer="91"/>
<pinref part="Q2" gate="G$1" pin="G"/>
<wire x1="101.6" y1="154.94" x2="101.6" y2="147.32" width="0.1524" layer="91"/>
<junction x="101.6" y="147.32"/>
<wire x1="91.44" y1="147.32" x2="101.6" y2="147.32" width="0.1524" layer="91"/>
<pinref part="R5" gate="G$1" pin="2"/>
<wire x1="91.44" y1="144.78" x2="91.44" y2="147.32" width="0.1524" layer="91"/>
<junction x="91.44" y="147.32"/>
<pinref part="C2" gate="G$1" pin="1"/>
<wire x1="88.9" y1="149.86" x2="88.9" y2="147.32" width="0.1524" layer="91"/>
<wire x1="88.9" y1="147.32" x2="91.44" y2="147.32" width="0.1524" layer="91"/>
<pinref part="D5" gate="G$1" pin="A"/>
</segment>
</net>
<net name="N$59" class="0">
<segment>
<pinref part="Q6" gate="G$1" pin="S"/>
<pinref part="R8" gate="G$1" pin="2"/>
<wire x1="124.46" y1="104.14" x2="134.62" y2="104.14" width="0.1524" layer="91"/>
<wire x1="134.62" y1="104.14" x2="144.78" y2="104.14" width="0.1524" layer="91"/>
<wire x1="144.78" y1="104.14" x2="149.86" y2="104.14" width="0.1524" layer="91"/>
<wire x1="124.46" y1="101.6" x2="124.46" y2="104.14" width="0.1524" layer="91"/>
<pinref part="C4" gate="G$1" pin="2"/>
<wire x1="144.78" y1="101.6" x2="144.78" y2="104.14" width="0.1524" layer="91"/>
<junction x="144.78" y="104.14"/>
<pinref part="Q3" gate="G$1" pin="S"/>
<wire x1="106.68" y1="104.14" x2="109.22" y2="104.14" width="0.1524" layer="91"/>
<junction x="124.46" y="104.14"/>
<wire x1="109.22" y1="104.14" x2="111.76" y2="104.14" width="0.1524" layer="91"/>
<wire x1="111.76" y1="104.14" x2="124.46" y2="104.14" width="0.1524" layer="91"/>
<wire x1="109.22" y1="100.33" x2="109.22" y2="104.14" width="0.1524" layer="91"/>
<junction x="109.22" y="104.14"/>
<wire x1="134.62" y1="100.33" x2="134.62" y2="104.14" width="0.1524" layer="91"/>
<junction x="134.62" y="104.14"/>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="104.14" y1="109.22" x2="111.76" y2="109.22" width="0.1524" layer="91"/>
<wire x1="111.76" y1="109.22" x2="111.76" y2="104.14" width="0.1524" layer="91"/>
<junction x="111.76" y="104.14"/>
<pinref part="D6" gate="G$1" pin="K"/>
<pinref part="D8" gate="G$1" pin="K"/>
</segment>
</net>
<net name="N$60" class="0">
<segment>
<pinref part="Q6" gate="G$1" pin="D"/>
<pinref part="C7" gate="G$1" pin="2"/>
<wire x1="165.1" y1="104.14" x2="167.64" y2="104.14" width="0.1524" layer="91"/>
<wire x1="167.64" y1="104.14" x2="167.64" y2="101.6" width="0.1524" layer="91"/>
<wire x1="167.64" y1="104.14" x2="175.26" y2="104.14" width="0.1524" layer="91"/>
<junction x="167.64" y="104.14"/>
<pinref part="C10" gate="G$1" pin="2"/>
<wire x1="175.26" y1="104.14" x2="180.34" y2="104.14" width="0.1524" layer="91"/>
<wire x1="180.34" y1="104.14" x2="184.15" y2="104.14" width="0.1524" layer="91"/>
<wire x1="175.26" y1="101.6" x2="175.26" y2="104.14" width="0.1524" layer="91"/>
<junction x="175.26" y="104.14"/>
<pinref part="D10" gate="G$1" pin="A"/>
<wire x1="180.34" y1="104.14" x2="180.34" y2="109.22" width="0.1524" layer="91"/>
<junction x="180.34" y="104.14"/>
<pinref part="R21" gate="G$1" pin="1"/>
<wire x1="180.34" y1="109.22" x2="182.88" y2="109.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$61" class="0">
<segment>
<pinref part="R8" gate="G$1" pin="1"/>
<wire x1="121.92" y1="88.9" x2="124.46" y2="88.9" width="0.1524" layer="91"/>
<wire x1="124.46" y1="88.9" x2="124.46" y2="91.44" width="0.1524" layer="91"/>
<pinref part="C4" gate="G$1" pin="1"/>
<wire x1="124.46" y1="88.9" x2="129.54" y2="88.9" width="0.1524" layer="91"/>
<wire x1="129.54" y1="88.9" x2="134.62" y2="88.9" width="0.1524" layer="91"/>
<wire x1="134.62" y1="88.9" x2="144.78" y2="88.9" width="0.1524" layer="91"/>
<wire x1="144.78" y1="88.9" x2="144.78" y2="91.44" width="0.1524" layer="91"/>
<junction x="124.46" y="88.9"/>
<pinref part="C7" gate="G$1" pin="1"/>
<wire x1="144.78" y1="88.9" x2="154.94" y2="88.9" width="0.1524" layer="91"/>
<wire x1="154.94" y1="88.9" x2="167.64" y2="88.9" width="0.1524" layer="91"/>
<wire x1="167.64" y1="88.9" x2="167.64" y2="91.44" width="0.1524" layer="91"/>
<junction x="144.78" y="88.9"/>
<pinref part="Q6" gate="G$1" pin="G"/>
<wire x1="154.94" y1="96.52" x2="154.94" y2="88.9" width="0.1524" layer="91"/>
<junction x="154.94" y="88.9"/>
<pinref part="R17" gate="G$1" pin="2"/>
<wire x1="129.54" y1="86.36" x2="129.54" y2="88.9" width="0.1524" layer="91"/>
<junction x="129.54" y="88.9"/>
<pinref part="R11" gate="G$1" pin="1"/>
<wire x1="134.62" y1="92.71" x2="134.62" y2="88.9" width="0.1524" layer="91"/>
<junction x="134.62" y="88.9"/>
<wire x1="167.64" y1="88.9" x2="175.26" y2="88.9" width="0.1524" layer="91"/>
<junction x="167.64" y="88.9"/>
<pinref part="C10" gate="G$1" pin="1"/>
<wire x1="175.26" y1="88.9" x2="175.26" y2="91.44" width="0.1524" layer="91"/>
<pinref part="D8" gate="G$1" pin="A"/>
</segment>
</net>
<net name="N$72" class="0">
<segment>
<pinref part="R17" gate="G$1" pin="1"/>
<pinref part="Q5" gate="G$1" pin="D"/>
</segment>
</net>
<net name="N$74" class="0">
<segment>
<pinref part="R11" gate="G$1" pin="2"/>
<wire x1="109.22" y1="88.9" x2="111.76" y2="88.9" width="0.1524" layer="91"/>
<wire x1="109.22" y1="92.71" x2="109.22" y2="88.9" width="0.1524" layer="91"/>
<junction x="109.22" y="88.9"/>
<wire x1="101.6" y1="88.9" x2="109.22" y2="88.9" width="0.1524" layer="91"/>
<pinref part="Q3" gate="G$1" pin="G"/>
<wire x1="101.6" y1="96.52" x2="101.6" y2="88.9" width="0.1524" layer="91"/>
<junction x="101.6" y="88.9"/>
<wire x1="91.44" y1="88.9" x2="101.6" y2="88.9" width="0.1524" layer="91"/>
<pinref part="R6" gate="G$1" pin="2"/>
<wire x1="91.44" y1="86.36" x2="91.44" y2="88.9" width="0.1524" layer="91"/>
<junction x="91.44" y="88.9"/>
<pinref part="C3" gate="G$1" pin="1"/>
<wire x1="88.9" y1="91.44" x2="88.9" y2="88.9" width="0.1524" layer="91"/>
<wire x1="88.9" y1="88.9" x2="91.44" y2="88.9" width="0.1524" layer="91"/>
<pinref part="D6" gate="G$1" pin="A"/>
</segment>
</net>
<net name="AISG_1_VIN" class="0">
<segment>
<pinref part="C1" gate="G$1" pin="2"/>
<wire x1="88.9" y1="226.06" x2="83.82" y2="226.06" width="0.1524" layer="91"/>
<wire x1="88.9" y1="223.52" x2="88.9" y2="226.06" width="0.1524" layer="91"/>
<pinref part="Q1" gate="G$1" pin="D"/>
<wire x1="91.44" y1="226.06" x2="90.932" y2="226.06" width="0.1524" layer="91"/>
<junction x="88.9" y="226.06"/>
<label x="83.82" y="226.06" size="1.27" layer="95" rot="R180" xref="yes"/>
<wire x1="90.932" y1="226.06" x2="88.9" y2="226.06" width="0.1524" layer="91"/>
<wire x1="90.932" y1="226.06" x2="90.932" y2="231.14" width="0.1524" layer="91"/>
<junction x="90.932" y="226.06"/>
<pinref part="R1" gate="G$1" pin="2"/>
<wire x1="90.932" y1="231.14" x2="93.98" y2="231.14" width="0.1524" layer="91"/>
</segment>
</net>
<net name="AISG_2_VIN" class="0">
<segment>
<pinref part="C2" gate="G$1" pin="2"/>
<wire x1="88.9" y1="162.56" x2="83.82" y2="162.56" width="0.1524" layer="91"/>
<wire x1="88.9" y1="160.02" x2="88.9" y2="162.56" width="0.1524" layer="91"/>
<pinref part="Q2" gate="G$1" pin="D"/>
<wire x1="91.44" y1="162.56" x2="90.932" y2="162.56" width="0.1524" layer="91"/>
<junction x="88.9" y="162.56"/>
<label x="83.82" y="162.56" size="1.27" layer="95" rot="R180" xref="yes"/>
<wire x1="90.932" y1="162.56" x2="88.9" y2="162.56" width="0.1524" layer="91"/>
<wire x1="90.932" y1="162.56" x2="90.932" y2="167.64" width="0.1524" layer="91"/>
<junction x="90.932" y="162.56"/>
<pinref part="R2" gate="G$1" pin="2"/>
<wire x1="90.932" y1="167.64" x2="93.98" y2="167.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="AISG_3_VIN" class="0">
<segment>
<pinref part="C3" gate="G$1" pin="2"/>
<wire x1="88.9" y1="104.14" x2="83.82" y2="104.14" width="0.1524" layer="91"/>
<wire x1="88.9" y1="101.6" x2="88.9" y2="104.14" width="0.1524" layer="91"/>
<pinref part="Q3" gate="G$1" pin="D"/>
<wire x1="91.44" y1="104.14" x2="90.932" y2="104.14" width="0.1524" layer="91"/>
<junction x="88.9" y="104.14"/>
<label x="83.82" y="104.14" size="1.27" layer="95" rot="R180" xref="yes"/>
<wire x1="90.932" y1="104.14" x2="88.9" y2="104.14" width="0.1524" layer="91"/>
<wire x1="90.932" y1="104.14" x2="90.932" y2="109.22" width="0.1524" layer="91"/>
<junction x="90.932" y="104.14"/>
<pinref part="R3" gate="G$1" pin="2"/>
<wire x1="90.932" y1="109.22" x2="93.98" y2="109.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="MOTOR_VIN" class="0">
<segment>
<pinref part="D12" gate="G$1" pin="K"/>
<label x="248.92" y="226.06" size="1.27" layer="95" xref="yes"/>
<wire x1="248.92" y1="226.06" x2="243.84" y2="226.06" width="0.1524" layer="91"/>
<pinref part="R23" gate="G$1" pin="2"/>
<wire x1="243.84" y1="226.06" x2="236.22" y2="226.06" width="0.1524" layer="91"/>
<wire x1="236.22" y1="226.06" x2="198.12" y2="226.06" width="0.1524" layer="91"/>
<wire x1="198.12" y1="226.06" x2="191.77" y2="226.06" width="0.1524" layer="91"/>
<wire x1="236.22" y1="208.28" x2="236.22" y2="226.06" width="0.1524" layer="91"/>
<junction x="236.22" y="226.06"/>
<pinref part="TP6" gate="G$1" pin="PP"/>
<wire x1="243.84" y1="226.06" x2="243.84" y2="220.98" width="0.1524" layer="91"/>
<wire x1="243.84" y1="220.98" x2="248.92" y2="220.98" width="0.1524" layer="91"/>
<junction x="243.84" y="226.06"/>
<pinref part="R20" gate="G$1" pin="2"/>
<wire x1="193.04" y1="231.14" x2="198.12" y2="231.14" width="0.1524" layer="91"/>
<wire x1="198.12" y1="231.14" x2="198.12" y2="226.06" width="0.1524" layer="91"/>
<junction x="198.12" y="226.06"/>
</segment>
</net>
<net name="CTRL1" class="0">
<segment>
<label x="93.98" y="187.96" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="R163" gate="G$1" pin="1"/>
<wire x1="99.06" y1="187.96" x2="93.98" y2="187.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="CTRL2" class="0">
<segment>
<label x="93.98" y="124.46" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="R160" gate="G$1" pin="1"/>
<wire x1="99.06" y1="124.46" x2="93.98" y2="124.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="CTRL3" class="0">
<segment>
<label x="91.44" y="66.04" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="R161" gate="G$1" pin="1"/>
<wire x1="96.52" y1="66.04" x2="91.44" y2="66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$26" class="0">
<segment>
<pinref part="D11" gate="G$1" pin="K"/>
<pinref part="TP5" gate="G$1" pin="PP"/>
<wire x1="191.77" y1="162.56" x2="195.58" y2="162.56" width="0.1524" layer="91"/>
<pinref part="R23" gate="G$1" pin="1"/>
<wire x1="195.58" y1="162.56" x2="233.68" y2="162.56" width="0.1524" layer="91"/>
<wire x1="233.68" y1="162.56" x2="236.22" y2="162.56" width="0.1524" layer="91"/>
<wire x1="236.22" y1="162.56" x2="248.92" y2="162.56" width="0.1524" layer="91"/>
<wire x1="236.22" y1="198.12" x2="236.22" y2="162.56" width="0.1524" layer="91"/>
<junction x="236.22" y="162.56"/>
<pinref part="R22" gate="G$1" pin="2"/>
<wire x1="233.68" y1="139.7" x2="233.68" y2="162.56" width="0.1524" layer="91"/>
<junction x="233.68" y="162.56"/>
<pinref part="R19" gate="G$1" pin="2"/>
<wire x1="193.04" y1="167.64" x2="195.58" y2="167.64" width="0.1524" layer="91"/>
<wire x1="195.58" y1="167.64" x2="195.58" y2="162.56" width="0.1524" layer="91"/>
<junction x="195.58" y="162.56"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<pinref part="D10" gate="G$1" pin="K"/>
<pinref part="TP4" gate="G$1" pin="PP"/>
<wire x1="191.77" y1="104.14" x2="195.58" y2="104.14" width="0.1524" layer="91"/>
<pinref part="R22" gate="G$1" pin="1"/>
<wire x1="195.58" y1="104.14" x2="233.68" y2="104.14" width="0.1524" layer="91"/>
<wire x1="233.68" y1="104.14" x2="248.92" y2="104.14" width="0.1524" layer="91"/>
<wire x1="233.68" y1="129.54" x2="233.68" y2="104.14" width="0.1524" layer="91"/>
<junction x="233.68" y="104.14"/>
<pinref part="R21" gate="G$1" pin="2"/>
<wire x1="193.04" y1="109.22" x2="195.58" y2="109.22" width="0.1524" layer="91"/>
<wire x1="195.58" y1="109.22" x2="195.58" y2="104.14" width="0.1524" layer="91"/>
<junction x="195.58" y="104.14"/>
</segment>
</net>
<net name="N$36" class="0">
<segment>
<pinref part="R163" gate="G$1" pin="2"/>
<pinref part="Q7" gate="G$1" pin="G"/>
<wire x1="121.92" y1="187.96" x2="114.3" y2="187.96" width="0.1524" layer="91"/>
<pinref part="R15" gate="G$1" pin="1"/>
<wire x1="116.84" y1="180.34" x2="114.3" y2="180.34" width="0.1524" layer="91"/>
<wire x1="114.3" y1="180.34" x2="114.3" y2="187.96" width="0.1524" layer="91"/>
<wire x1="109.22" y1="187.96" x2="114.3" y2="187.96" width="0.1524" layer="91"/>
<junction x="114.3" y="187.96"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<pinref part="R160" gate="G$1" pin="2"/>
<pinref part="Q4" gate="G$1" pin="G"/>
<wire x1="121.92" y1="124.46" x2="114.3" y2="124.46" width="0.1524" layer="91"/>
<pinref part="R13" gate="G$1" pin="1"/>
<wire x1="116.84" y1="116.84" x2="114.3" y2="116.84" width="0.1524" layer="91"/>
<wire x1="114.3" y1="116.84" x2="114.3" y2="124.46" width="0.1524" layer="91"/>
<wire x1="109.22" y1="124.46" x2="114.3" y2="124.46" width="0.1524" layer="91"/>
<junction x="114.3" y="124.46"/>
</segment>
</net>
<net name="N$37" class="0">
<segment>
<pinref part="R161" gate="G$1" pin="2"/>
<wire x1="106.68" y1="66.04" x2="114.3" y2="66.04" width="0.1524" layer="91"/>
<pinref part="Q5" gate="G$1" pin="G"/>
<wire x1="121.92" y1="66.04" x2="114.3" y2="66.04" width="0.1524" layer="91"/>
<pinref part="R14" gate="G$1" pin="1"/>
<wire x1="116.84" y1="58.42" x2="114.3" y2="58.42" width="0.1524" layer="91"/>
<wire x1="114.3" y1="58.42" x2="114.3" y2="66.04" width="0.1524" layer="91"/>
<junction x="114.3" y="66.04"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
<text x="64.897" y="220.726" size="1.27" layer="97" rot="R90">0603</text>
<text x="72.771" y="177.038" size="1.27" layer="97" rot="R90">0603</text>
<text x="106.68" y="180.34" size="1.27" layer="97">I2C Address = 0x00</text>
<text x="62.357" y="126.746" size="1.27" layer="97" rot="R90">0603</text>
<text x="70.231" y="83.058" size="1.27" layer="97" rot="R90">0603</text>
<text x="104.14" y="86.36" size="1.27" layer="97">I2C Address = 0x01</text>
<text x="212.217" y="220.726" size="1.27" layer="97" rot="R90">0603</text>
<text x="220.091" y="177.038" size="1.27" layer="97" rot="R90">0603</text>
<text x="254" y="180.34" size="1.27" layer="97">I2C Address = 0x00</text>
<text x="212.217" y="126.746" size="1.27" layer="97" rot="R90">0603</text>
<text x="220.091" y="83.058" size="1.27" layer="97" rot="R90">0603</text>
<text x="254" y="86.36" size="1.27" layer="97">I2C Address = 0x01</text>
</plain>
<instances>
<instance part="U7" gate="G$1" x="104.14" y="231.14" smashed="yes">
<attribute name="NAME" x="104.14" y="233.68" size="1.27" layer="95"/>
<attribute name="VALUE" x="104.14" y="236.22" size="1.27" layer="96"/>
</instance>
<instance part="C59" gate="G$1" x="144.78" y="187.96" smashed="yes">
<attribute name="NAME" x="139.573" y="188.341" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="146.05" y="188.341" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="C58" gate="G$1" x="144.78" y="193.04" smashed="yes">
<attribute name="NAME" x="139.573" y="193.421" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="146.05" y="193.421" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="SUPPLY92" gate="GND" x="137.16" y="172.72" smashed="yes">
<attribute name="VALUE" x="135.255" y="169.545" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY93" gate="GND" x="93.98" y="172.72" smashed="yes">
<attribute name="VALUE" x="92.075" y="169.545" size="1.27" layer="96"/>
</instance>
<instance part="NC18" gate="G$1" x="99.06" y="195.58" smashed="yes"/>
<instance part="NC19" gate="G$1" x="99.06" y="210.82" smashed="yes"/>
<instance part="NC20" gate="G$1" x="99.06" y="187.96" smashed="yes"/>
<instance part="C54" gate="G$1" x="71.12" y="182.88" smashed="yes" rot="R270">
<attribute name="NAME" x="70.739" y="177.038" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="70.739" y="184.023" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="R74" gate="G$1" x="81.28" y="190.5" smashed="yes">
<attribute name="NAME" x="73.66" y="190.5" size="1.27" layer="95"/>
<attribute name="VALUE" x="86.36" y="190.5" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY94" gate="GND" x="71.12" y="172.72" smashed="yes">
<attribute name="VALUE" x="69.215" y="169.545" size="1.27" layer="96"/>
</instance>
<instance part="R58" gate="G$1" x="78.74" y="213.36" smashed="yes">
<attribute name="NAME" x="71.12" y="213.36" size="1.27" layer="95"/>
<attribute name="VALUE" x="83.82" y="213.36" size="1.27" layer="96"/>
</instance>
<instance part="C48" gate="G$1" x="63.5" y="226.06" smashed="yes" rot="R270">
<attribute name="NAME" x="62.865" y="220.726" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="63.119" y="227.33" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="SUPPLY95" gate="GND" x="63.5" y="215.9" smashed="yes">
<attribute name="VALUE" x="61.595" y="212.725" size="1.27" layer="96"/>
</instance>
<instance part="R57" gate="G$1" x="53.34" y="233.68" smashed="yes">
<attribute name="NAME" x="45.72" y="233.68" size="1.27" layer="95"/>
<attribute name="VALUE" x="58.42" y="233.68" size="1.27" layer="96"/>
</instance>
<instance part="R75" gate="G$1" x="53.34" y="190.5" smashed="yes">
<attribute name="NAME" x="45.72" y="190.5" size="1.27" layer="95"/>
<attribute name="VALUE" x="58.42" y="190.5" size="1.27" layer="96"/>
</instance>
<instance part="R52" gate="G$1" x="53.34" y="228.6" smashed="yes">
<attribute name="NAME" x="45.72" y="228.6" size="1.27" layer="95"/>
<attribute name="VALUE" x="58.42" y="228.6" size="1.27" layer="96"/>
</instance>
<instance part="R70" gate="G$1" x="53.34" y="185.42" smashed="yes">
<attribute name="NAME" x="45.72" y="185.42" size="1.27" layer="95"/>
<attribute name="VALUE" x="58.42" y="185.42" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY96" gate="GND" x="45.72" y="223.52" smashed="yes">
<attribute name="VALUE" x="43.815" y="220.345" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY97" gate="GND" x="45.72" y="180.34" smashed="yes">
<attribute name="VALUE" x="43.815" y="177.165" size="1.27" layer="96"/>
</instance>
<instance part="C53" gate="G$1" x="91.44" y="182.88" smashed="yes" rot="R90">
<attribute name="NAME" x="91.059" y="177.673" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="91.059" y="184.15" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="R55" gate="G$1" x="78.74" y="210.82" smashed="yes">
<attribute name="NAME" x="71.12" y="210.82" size="1.27" layer="95"/>
<attribute name="VALUE" x="83.82" y="210.82" size="1.27" layer="96"/>
</instance>
<instance part="R59" gate="G$1" x="78.74" y="208.28" smashed="yes">
<attribute name="NAME" x="71.12" y="208.28" size="1.27" layer="95"/>
<attribute name="VALUE" x="83.82" y="208.28" size="1.27" layer="96"/>
</instance>
<instance part="R61" gate="G$1" x="78.74" y="205.74" smashed="yes">
<attribute name="NAME" x="71.12" y="205.74" size="1.27" layer="95"/>
<attribute name="VALUE" x="83.82" y="205.74" size="1.27" layer="96"/>
</instance>
<instance part="R63" gate="G$1" x="78.74" y="203.2" smashed="yes">
<attribute name="NAME" x="71.12" y="203.2" size="1.27" layer="95"/>
<attribute name="VALUE" x="83.82" y="203.2" size="1.27" layer="96"/>
</instance>
<instance part="U9" gate="G$1" x="101.6" y="137.16" smashed="yes">
<attribute name="NAME" x="101.6" y="139.7" size="1.27" layer="95"/>
<attribute name="VALUE" x="101.6" y="142.24" size="1.27" layer="96"/>
</instance>
<instance part="C67" gate="G$1" x="142.24" y="93.98" smashed="yes">
<attribute name="NAME" x="137.033" y="94.361" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="143.51" y="94.361" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="C69" gate="G$1" x="142.24" y="99.06" smashed="yes">
<attribute name="NAME" x="137.033" y="99.441" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="143.51" y="99.441" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="SUPPLY104" gate="GND" x="134.62" y="78.74" smashed="yes">
<attribute name="VALUE" x="132.715" y="75.565" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY105" gate="GND" x="91.44" y="78.74" smashed="yes">
<attribute name="VALUE" x="89.535" y="75.565" size="1.27" layer="96"/>
</instance>
<instance part="NC24" gate="G$1" x="96.52" y="101.6" smashed="yes"/>
<instance part="NC25" gate="G$1" x="96.52" y="116.84" smashed="yes"/>
<instance part="NC26" gate="G$1" x="96.52" y="93.98" smashed="yes"/>
<instance part="C61" gate="G$1" x="68.58" y="88.9" smashed="yes" rot="R270">
<attribute name="NAME" x="68.199" y="83.058" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="68.199" y="90.043" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="R83" gate="G$1" x="78.74" y="96.52" smashed="yes">
<attribute name="NAME" x="71.12" y="96.52" size="1.27" layer="95"/>
<attribute name="VALUE" x="83.82" y="96.52" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY106" gate="GND" x="68.58" y="78.74" smashed="yes">
<attribute name="VALUE" x="66.675" y="75.565" size="1.27" layer="96"/>
</instance>
<instance part="R94" gate="G$1" x="76.2" y="119.38" smashed="yes">
<attribute name="NAME" x="68.58" y="119.38" size="1.27" layer="95"/>
<attribute name="VALUE" x="81.28" y="119.38" size="1.27" layer="96"/>
</instance>
<instance part="C65" gate="G$1" x="60.96" y="132.08" smashed="yes" rot="R270">
<attribute name="NAME" x="60.325" y="126.746" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="60.579" y="133.35" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="SUPPLY107" gate="GND" x="60.96" y="121.92" smashed="yes">
<attribute name="VALUE" x="59.055" y="118.745" size="1.27" layer="96"/>
</instance>
<instance part="R96" gate="G$1" x="50.8" y="139.7" smashed="yes">
<attribute name="NAME" x="43.18" y="139.7" size="1.27" layer="95"/>
<attribute name="VALUE" x="55.88" y="139.7" size="1.27" layer="96"/>
</instance>
<instance part="R84" gate="G$1" x="50.8" y="96.52" smashed="yes">
<attribute name="NAME" x="43.18" y="96.52" size="1.27" layer="95"/>
<attribute name="VALUE" x="55.88" y="96.52" size="1.27" layer="96"/>
</instance>
<instance part="R92" gate="G$1" x="50.8" y="134.62" smashed="yes">
<attribute name="NAME" x="43.18" y="134.62" size="1.27" layer="95"/>
<attribute name="VALUE" x="55.88" y="134.62" size="1.27" layer="96"/>
</instance>
<instance part="R90" gate="G$1" x="50.8" y="91.44" smashed="yes">
<attribute name="NAME" x="43.18" y="91.44" size="1.27" layer="95"/>
<attribute name="VALUE" x="55.88" y="91.44" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY108" gate="GND" x="43.18" y="129.54" smashed="yes">
<attribute name="VALUE" x="41.275" y="126.365" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY109" gate="GND" x="43.18" y="86.36" smashed="yes">
<attribute name="VALUE" x="41.275" y="83.185" size="1.27" layer="96"/>
</instance>
<instance part="C63" gate="G$1" x="88.9" y="88.9" smashed="yes" rot="R90">
<attribute name="NAME" x="88.519" y="83.693" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="88.519" y="90.17" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="R85" gate="G$1" x="76.2" y="116.84" smashed="yes">
<attribute name="NAME" x="68.58" y="116.84" size="1.27" layer="95"/>
<attribute name="VALUE" x="81.28" y="116.84" size="1.27" layer="96"/>
</instance>
<instance part="R86" gate="G$1" x="76.2" y="114.3" smashed="yes">
<attribute name="NAME" x="68.58" y="114.3" size="1.27" layer="95"/>
<attribute name="VALUE" x="81.28" y="114.3" size="1.27" layer="96"/>
</instance>
<instance part="R87" gate="G$1" x="76.2" y="111.76" smashed="yes">
<attribute name="NAME" x="68.58" y="111.76" size="1.27" layer="95"/>
<attribute name="VALUE" x="81.28" y="111.76" size="1.27" layer="96"/>
</instance>
<instance part="R88" gate="G$1" x="76.2" y="109.22" smashed="yes">
<attribute name="NAME" x="68.58" y="109.22" size="1.27" layer="95"/>
<attribute name="VALUE" x="81.28" y="109.22" size="1.27" layer="96"/>
</instance>
<instance part="U8" gate="G$1" x="251.46" y="231.14" smashed="yes">
<attribute name="NAME" x="251.46" y="233.68" size="1.27" layer="95"/>
<attribute name="VALUE" x="251.46" y="236.22" size="1.27" layer="96"/>
</instance>
<instance part="C66" gate="G$1" x="292.1" y="187.96" smashed="yes">
<attribute name="NAME" x="286.893" y="188.341" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="293.37" y="188.341" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="C68" gate="G$1" x="292.1" y="193.04" smashed="yes">
<attribute name="NAME" x="286.893" y="193.421" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="293.37" y="193.421" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="SUPPLY116" gate="GND" x="284.48" y="172.72" smashed="yes">
<attribute name="VALUE" x="282.575" y="169.545" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY117" gate="GND" x="241.3" y="172.72" smashed="yes">
<attribute name="VALUE" x="239.395" y="169.545" size="1.27" layer="96"/>
</instance>
<instance part="NC30" gate="G$1" x="246.38" y="195.58" smashed="yes"/>
<instance part="NC31" gate="G$1" x="246.38" y="210.82" smashed="yes"/>
<instance part="NC32" gate="G$1" x="246.38" y="187.96" smashed="yes"/>
<instance part="C60" gate="G$1" x="218.44" y="182.88" smashed="yes" rot="R270">
<attribute name="NAME" x="218.059" y="177.038" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="218.059" y="184.023" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="R81" gate="G$1" x="228.6" y="190.5" smashed="yes">
<attribute name="NAME" x="220.98" y="190.5" size="1.27" layer="95"/>
<attribute name="VALUE" x="233.68" y="190.5" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY118" gate="GND" x="218.44" y="172.72" smashed="yes">
<attribute name="VALUE" x="216.535" y="169.545" size="1.27" layer="96"/>
</instance>
<instance part="R93" gate="G$1" x="226.06" y="213.36" smashed="yes">
<attribute name="NAME" x="218.44" y="213.36" size="1.27" layer="95"/>
<attribute name="VALUE" x="231.14" y="213.36" size="1.27" layer="96"/>
</instance>
<instance part="C64" gate="G$1" x="210.82" y="226.06" smashed="yes" rot="R270">
<attribute name="NAME" x="210.185" y="220.726" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="210.439" y="227.33" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="SUPPLY119" gate="GND" x="210.82" y="215.9" smashed="yes">
<attribute name="VALUE" x="208.915" y="212.725" size="1.27" layer="96"/>
</instance>
<instance part="R95" gate="G$1" x="200.66" y="233.68" smashed="yes">
<attribute name="NAME" x="193.04" y="233.68" size="1.27" layer="95"/>
<attribute name="VALUE" x="205.74" y="233.68" size="1.27" layer="96"/>
</instance>
<instance part="R82" gate="G$1" x="200.66" y="190.5" smashed="yes">
<attribute name="NAME" x="193.04" y="190.5" size="1.27" layer="95"/>
<attribute name="VALUE" x="205.74" y="190.5" size="1.27" layer="96"/>
</instance>
<instance part="R91" gate="G$1" x="200.66" y="228.6" smashed="yes">
<attribute name="NAME" x="193.04" y="228.6" size="1.27" layer="95"/>
<attribute name="VALUE" x="205.74" y="228.6" size="1.27" layer="96"/>
</instance>
<instance part="R89" gate="G$1" x="200.66" y="185.42" smashed="yes">
<attribute name="NAME" x="193.04" y="185.42" size="1.27" layer="95"/>
<attribute name="VALUE" x="205.74" y="185.42" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY120" gate="GND" x="193.04" y="223.52" smashed="yes">
<attribute name="VALUE" x="191.135" y="220.345" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY121" gate="GND" x="193.04" y="180.34" smashed="yes">
<attribute name="VALUE" x="191.135" y="177.165" size="1.27" layer="96"/>
</instance>
<instance part="C62" gate="G$1" x="238.76" y="182.88" smashed="yes" rot="R90">
<attribute name="NAME" x="238.379" y="177.673" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="238.379" y="184.15" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="R77" gate="G$1" x="226.06" y="210.82" smashed="yes">
<attribute name="NAME" x="218.44" y="210.82" size="1.27" layer="95"/>
<attribute name="VALUE" x="231.14" y="210.82" size="1.27" layer="96"/>
</instance>
<instance part="R78" gate="G$1" x="226.06" y="208.28" smashed="yes">
<attribute name="NAME" x="218.44" y="208.28" size="1.27" layer="95"/>
<attribute name="VALUE" x="231.14" y="208.28" size="1.27" layer="96"/>
</instance>
<instance part="R79" gate="G$1" x="226.06" y="205.74" smashed="yes">
<attribute name="NAME" x="218.44" y="205.74" size="1.27" layer="95"/>
<attribute name="VALUE" x="231.14" y="205.74" size="1.27" layer="96"/>
</instance>
<instance part="R80" gate="G$1" x="226.06" y="203.2" smashed="yes">
<attribute name="NAME" x="218.44" y="203.2" size="1.27" layer="95"/>
<attribute name="VALUE" x="231.14" y="203.2" size="1.27" layer="96"/>
</instance>
<instance part="U5" gate="G$1" x="251.46" y="137.16" smashed="yes">
<attribute name="NAME" x="251.46" y="139.7" size="1.27" layer="95"/>
<attribute name="VALUE" x="251.46" y="142.24" size="1.27" layer="96"/>
</instance>
<instance part="C46" gate="G$1" x="292.1" y="93.98" smashed="yes">
<attribute name="NAME" x="286.893" y="94.361" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="293.37" y="94.361" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="C47" gate="G$1" x="292.1" y="99.06" smashed="yes">
<attribute name="NAME" x="286.893" y="99.441" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="293.37" y="99.441" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="SUPPLY128" gate="GND" x="284.48" y="78.74" smashed="yes">
<attribute name="VALUE" x="282.575" y="75.565" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY129" gate="GND" x="241.3" y="78.74" smashed="yes">
<attribute name="VALUE" x="239.395" y="75.565" size="1.27" layer="96"/>
</instance>
<instance part="NC36" gate="G$1" x="246.38" y="101.6" smashed="yes"/>
<instance part="NC37" gate="G$1" x="246.38" y="116.84" smashed="yes"/>
<instance part="NC38" gate="G$1" x="246.38" y="93.98" smashed="yes"/>
<instance part="C51" gate="G$1" x="218.44" y="88.9" smashed="yes" rot="R270">
<attribute name="NAME" x="218.059" y="83.058" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="218.059" y="90.043" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="R54" gate="G$1" x="228.6" y="96.52" smashed="yes">
<attribute name="NAME" x="220.98" y="96.52" size="1.27" layer="95"/>
<attribute name="VALUE" x="233.68" y="96.52" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY130" gate="GND" x="218.44" y="78.74" smashed="yes">
<attribute name="VALUE" x="216.535" y="75.565" size="1.27" layer="96"/>
</instance>
<instance part="R71" gate="G$1" x="226.06" y="119.38" smashed="yes">
<attribute name="NAME" x="218.44" y="119.38" size="1.27" layer="95"/>
<attribute name="VALUE" x="231.14" y="119.38" size="1.27" layer="96"/>
</instance>
<instance part="C56" gate="G$1" x="210.82" y="132.08" smashed="yes" rot="R270">
<attribute name="NAME" x="210.185" y="126.746" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="210.439" y="133.35" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="SUPPLY131" gate="GND" x="210.82" y="121.92" smashed="yes">
<attribute name="VALUE" x="208.915" y="118.745" size="1.27" layer="96"/>
</instance>
<instance part="R72" gate="G$1" x="200.66" y="139.7" smashed="yes">
<attribute name="NAME" x="193.04" y="139.7" size="1.27" layer="95"/>
<attribute name="VALUE" x="205.74" y="139.7" size="1.27" layer="96"/>
</instance>
<instance part="R53" gate="G$1" x="200.66" y="96.52" smashed="yes">
<attribute name="NAME" x="193.04" y="96.52" size="1.27" layer="95"/>
<attribute name="VALUE" x="205.74" y="96.52" size="1.27" layer="96"/>
</instance>
<instance part="R76" gate="G$1" x="200.66" y="134.62" smashed="yes">
<attribute name="NAME" x="193.04" y="134.62" size="1.27" layer="95"/>
<attribute name="VALUE" x="205.74" y="134.62" size="1.27" layer="96"/>
</instance>
<instance part="R60" gate="G$1" x="200.66" y="91.44" smashed="yes">
<attribute name="NAME" x="193.04" y="91.44" size="1.27" layer="95"/>
<attribute name="VALUE" x="205.74" y="91.44" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY132" gate="GND" x="193.04" y="129.54" smashed="yes">
<attribute name="VALUE" x="191.135" y="126.365" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY133" gate="GND" x="193.04" y="86.36" smashed="yes">
<attribute name="VALUE" x="191.135" y="83.185" size="1.27" layer="96"/>
</instance>
<instance part="C52" gate="G$1" x="238.76" y="88.9" smashed="yes" rot="R90">
<attribute name="NAME" x="238.379" y="83.693" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="238.379" y="90.17" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="R73" gate="G$1" x="226.06" y="116.84" smashed="yes">
<attribute name="NAME" x="218.44" y="116.84" size="1.27" layer="95"/>
<attribute name="VALUE" x="231.14" y="116.84" size="1.27" layer="96"/>
</instance>
<instance part="R67" gate="G$1" x="226.06" y="114.3" smashed="yes">
<attribute name="NAME" x="218.44" y="114.3" size="1.27" layer="95"/>
<attribute name="VALUE" x="231.14" y="114.3" size="1.27" layer="96"/>
</instance>
<instance part="R66" gate="G$1" x="226.06" y="111.76" smashed="yes">
<attribute name="NAME" x="218.44" y="111.76" size="1.27" layer="95"/>
<attribute name="VALUE" x="231.14" y="111.76" size="1.27" layer="96"/>
</instance>
<instance part="R65" gate="G$1" x="226.06" y="109.22" smashed="yes">
<attribute name="NAME" x="218.44" y="109.22" size="1.27" layer="95"/>
<attribute name="VALUE" x="231.14" y="109.22" size="1.27" layer="96"/>
</instance>
<instance part="J5" gate="G$1" x="358.14" y="218.44" smashed="yes">
<attribute name="NAME" x="354.838" y="218.948" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J5" gate="G$2" x="358.14" y="215.9" smashed="yes">
<attribute name="NAME" x="354.838" y="216.408" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J5" gate="G$3" x="358.14" y="213.36" smashed="yes">
<attribute name="NAME" x="354.838" y="213.868" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J5" gate="G$4" x="358.14" y="210.82" smashed="yes">
<attribute name="NAME" x="354.838" y="211.328" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J5" gate="G$5" x="358.14" y="208.28" smashed="yes">
<attribute name="NAME" x="354.838" y="208.788" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J5" gate="G$6" x="358.14" y="205.74" smashed="yes">
<attribute name="NAME" x="354.838" y="206.248" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J5" gate="G$7" x="358.14" y="203.2" smashed="yes">
<attribute name="NAME" x="354.838" y="203.708" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J5" gate="G$8" x="358.14" y="200.66" smashed="yes">
<attribute name="NAME" x="354.838" y="201.168" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J6" gate="G$1" x="358.14" y="182.88" smashed="yes">
<attribute name="NAME" x="354.838" y="183.388" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J6" gate="G$2" x="358.14" y="180.34" smashed="yes">
<attribute name="NAME" x="354.838" y="180.848" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J6" gate="G$3" x="358.14" y="177.8" smashed="yes">
<attribute name="NAME" x="354.838" y="178.308" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J6" gate="G$4" x="358.14" y="175.26" smashed="yes">
<attribute name="NAME" x="354.838" y="175.768" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J6" gate="G$5" x="358.14" y="172.72" smashed="yes">
<attribute name="NAME" x="354.838" y="173.228" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J6" gate="G$6" x="358.14" y="170.18" smashed="yes">
<attribute name="NAME" x="354.838" y="170.688" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J6" gate="G$7" x="358.14" y="167.64" smashed="yes">
<attribute name="NAME" x="354.838" y="168.148" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J6" gate="G$8" x="358.14" y="165.1" smashed="yes">
<attribute name="NAME" x="354.838" y="165.608" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J7" gate="G$1" x="358.14" y="147.32" smashed="yes">
<attribute name="NAME" x="354.838" y="147.828" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J7" gate="G$2" x="358.14" y="144.78" smashed="yes">
<attribute name="NAME" x="354.838" y="145.288" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J7" gate="G$3" x="358.14" y="142.24" smashed="yes">
<attribute name="NAME" x="354.838" y="142.748" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J7" gate="G$4" x="358.14" y="139.7" smashed="yes">
<attribute name="NAME" x="354.838" y="140.208" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J7" gate="G$5" x="358.14" y="137.16" smashed="yes">
<attribute name="NAME" x="354.838" y="137.668" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J7" gate="G$6" x="358.14" y="134.62" smashed="yes">
<attribute name="NAME" x="354.838" y="135.128" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J7" gate="G$7" x="358.14" y="132.08" smashed="yes">
<attribute name="NAME" x="354.838" y="132.588" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J7" gate="G$8" x="358.14" y="129.54" smashed="yes">
<attribute name="NAME" x="354.838" y="130.048" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J4" gate="G$1" x="358.14" y="114.3" smashed="yes">
<attribute name="NAME" x="354.838" y="114.808" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J4" gate="G$2" x="358.14" y="111.76" smashed="yes">
<attribute name="NAME" x="354.838" y="112.268" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J4" gate="G$3" x="358.14" y="109.22" smashed="yes">
<attribute name="NAME" x="354.838" y="109.728" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J4" gate="G$4" x="358.14" y="106.68" smashed="yes">
<attribute name="NAME" x="354.838" y="107.188" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J4" gate="G$5" x="358.14" y="104.14" smashed="yes">
<attribute name="NAME" x="354.838" y="104.648" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J4" gate="G$6" x="358.14" y="101.6" smashed="yes">
<attribute name="NAME" x="354.838" y="102.108" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J4" gate="G$7" x="358.14" y="99.06" smashed="yes">
<attribute name="NAME" x="354.838" y="99.568" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J4" gate="G$8" x="358.14" y="96.52" smashed="yes">
<attribute name="NAME" x="354.838" y="97.028" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="R115" gate="G$1" x="53.34" y="195.58" smashed="yes">
<attribute name="NAME" x="45.72" y="195.58" size="1.27" layer="95"/>
<attribute name="VALUE" x="58.42" y="195.58" size="1.27" layer="96"/>
</instance>
<instance part="R116" gate="G$1" x="50.8" y="101.6" smashed="yes">
<attribute name="NAME" x="43.18" y="101.6" size="1.27" layer="95"/>
<attribute name="VALUE" x="55.88" y="101.6" size="1.27" layer="96"/>
</instance>
<instance part="R117" gate="G$1" x="200.66" y="195.58" smashed="yes">
<attribute name="NAME" x="193.04" y="195.58" size="1.27" layer="95"/>
<attribute name="VALUE" x="205.74" y="195.58" size="1.27" layer="96"/>
</instance>
<instance part="R118" gate="G$1" x="200.66" y="101.6" smashed="yes">
<attribute name="NAME" x="193.04" y="101.6" size="1.27" layer="95"/>
<attribute name="VALUE" x="205.74" y="101.6" size="1.27" layer="96"/>
</instance>
<instance part="FRAME6" gate="G$1" x="-2.54" y="0" smashed="yes">
<attribute name="DRAWING_NAME" x="341.63" y="15.24" size="2.54" layer="94"/>
<attribute name="LAST_DATE_TIME" x="341.63" y="10.16" size="2.286" layer="94"/>
<attribute name="SHEET" x="354.965" y="5.08" size="2.54" layer="94"/>
</instance>
<instance part="F1" gate="G$1" x="27.94" y="48.26" smashed="yes">
<attribute name="NAME" x="23.114" y="57.15" size="1.778" layer="95"/>
</instance>
<instance part="F2" gate="G$1" x="43.18" y="48.26" smashed="yes">
<attribute name="NAME" x="38.354" y="57.15" size="1.778" layer="95"/>
</instance>
<instance part="F3" gate="G$1" x="58.42" y="48.26" smashed="yes">
<attribute name="NAME" x="53.594" y="57.15" size="1.778" layer="95"/>
</instance>
<instance part="F4" gate="G$1" x="73.66" y="48.26" smashed="yes">
<attribute name="NAME" x="68.834" y="57.15" size="1.778" layer="95"/>
</instance>
<instance part="F5" gate="G$1" x="88.9" y="48.26" smashed="yes">
<attribute name="NAME" x="84.074" y="57.15" size="1.778" layer="95"/>
</instance>
<instance part="F6" gate="G$1" x="104.14" y="48.26" smashed="yes">
<attribute name="NAME" x="99.314" y="57.15" size="1.778" layer="95"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="U7" gate="G$1" pin="32"/>
<wire x1="134.62" y1="226.06" x2="137.16" y2="226.06" width="0.1524" layer="91"/>
<wire x1="137.16" y1="226.06" x2="137.16" y2="223.52" width="0.1524" layer="91"/>
<pinref part="U7" gate="G$1" pin="31"/>
<wire x1="137.16" y1="223.52" x2="137.16" y2="210.82" width="0.1524" layer="91"/>
<wire x1="137.16" y1="210.82" x2="137.16" y2="208.28" width="0.1524" layer="91"/>
<wire x1="137.16" y1="208.28" x2="137.16" y2="175.26" width="0.1524" layer="91"/>
<wire x1="134.62" y1="223.52" x2="137.16" y2="223.52" width="0.1524" layer="91"/>
<pinref part="U7" gate="G$1" pin="26"/>
<wire x1="134.62" y1="210.82" x2="137.16" y2="210.82" width="0.1524" layer="91"/>
<pinref part="U7" gate="G$1" pin="25"/>
<wire x1="134.62" y1="208.28" x2="137.16" y2="208.28" width="0.1524" layer="91"/>
<junction x="137.16" y="223.52"/>
<junction x="137.16" y="210.82"/>
<junction x="137.16" y="208.28"/>
<pinref part="SUPPLY92" gate="GND" pin="GND"/>
<pinref part="U7" gate="G$1" pin="33"/>
<wire x1="134.62" y1="228.6" x2="137.16" y2="228.6" width="0.1524" layer="91"/>
<wire x1="137.16" y1="228.6" x2="137.16" y2="226.06" width="0.1524" layer="91"/>
<junction x="137.16" y="226.06"/>
</segment>
<segment>
<pinref part="U7" gate="G$1" pin="11"/>
<wire x1="99.06" y1="200.66" x2="93.98" y2="200.66" width="0.1524" layer="91"/>
<wire x1="93.98" y1="200.66" x2="93.98" y2="198.12" width="0.1524" layer="91"/>
<pinref part="SUPPLY93" gate="GND" pin="GND"/>
<pinref part="U7" gate="G$1" pin="12"/>
<wire x1="93.98" y1="198.12" x2="93.98" y2="193.04" width="0.1524" layer="91"/>
<wire x1="93.98" y1="193.04" x2="93.98" y2="177.8" width="0.1524" layer="91"/>
<wire x1="93.98" y1="177.8" x2="93.98" y2="175.26" width="0.1524" layer="91"/>
<wire x1="99.06" y1="198.12" x2="93.98" y2="198.12" width="0.1524" layer="91"/>
<junction x="93.98" y="198.12"/>
<pinref part="U7" gate="G$1" pin="14"/>
<wire x1="99.06" y1="193.04" x2="93.98" y2="193.04" width="0.1524" layer="91"/>
<junction x="93.98" y="193.04"/>
<pinref part="C53" gate="G$1" pin="1"/>
<wire x1="91.44" y1="177.8" x2="93.98" y2="177.8" width="0.1524" layer="91"/>
<junction x="93.98" y="177.8"/>
</segment>
<segment>
<pinref part="SUPPLY94" gate="GND" pin="GND"/>
<pinref part="C54" gate="G$1" pin="2"/>
<wire x1="71.12" y1="175.26" x2="71.12" y2="177.8" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C48" gate="G$1" pin="2"/>
<pinref part="SUPPLY95" gate="GND" pin="GND"/>
<wire x1="63.5" y1="220.98" x2="63.5" y2="218.44" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R52" gate="G$1" pin="1"/>
<pinref part="SUPPLY96" gate="GND" pin="GND"/>
<wire x1="48.26" y1="228.6" x2="45.72" y2="228.6" width="0.1524" layer="91"/>
<wire x1="45.72" y1="228.6" x2="45.72" y2="226.06" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R70" gate="G$1" pin="1"/>
<pinref part="SUPPLY97" gate="GND" pin="GND"/>
<wire x1="48.26" y1="185.42" x2="45.72" y2="185.42" width="0.1524" layer="91"/>
<wire x1="45.72" y1="185.42" x2="45.72" y2="182.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U9" gate="G$1" pin="32"/>
<wire x1="132.08" y1="132.08" x2="134.62" y2="132.08" width="0.1524" layer="91"/>
<wire x1="134.62" y1="132.08" x2="134.62" y2="129.54" width="0.1524" layer="91"/>
<pinref part="U9" gate="G$1" pin="31"/>
<wire x1="134.62" y1="129.54" x2="134.62" y2="116.84" width="0.1524" layer="91"/>
<wire x1="134.62" y1="116.84" x2="134.62" y2="114.3" width="0.1524" layer="91"/>
<wire x1="134.62" y1="114.3" x2="134.62" y2="81.28" width="0.1524" layer="91"/>
<wire x1="132.08" y1="129.54" x2="134.62" y2="129.54" width="0.1524" layer="91"/>
<pinref part="U9" gate="G$1" pin="26"/>
<wire x1="132.08" y1="116.84" x2="134.62" y2="116.84" width="0.1524" layer="91"/>
<pinref part="U9" gate="G$1" pin="25"/>
<wire x1="132.08" y1="114.3" x2="134.62" y2="114.3" width="0.1524" layer="91"/>
<junction x="134.62" y="129.54"/>
<junction x="134.62" y="116.84"/>
<junction x="134.62" y="114.3"/>
<pinref part="SUPPLY104" gate="GND" pin="GND"/>
<pinref part="U9" gate="G$1" pin="33"/>
<wire x1="132.08" y1="134.62" x2="134.62" y2="134.62" width="0.1524" layer="91"/>
<wire x1="134.62" y1="134.62" x2="134.62" y2="132.08" width="0.1524" layer="91"/>
<junction x="134.62" y="132.08"/>
</segment>
<segment>
<pinref part="U9" gate="G$1" pin="11"/>
<wire x1="96.52" y1="106.68" x2="91.44" y2="106.68" width="0.1524" layer="91"/>
<wire x1="91.44" y1="106.68" x2="91.44" y2="104.14" width="0.1524" layer="91"/>
<pinref part="SUPPLY105" gate="GND" pin="GND"/>
<pinref part="U9" gate="G$1" pin="12"/>
<wire x1="91.44" y1="104.14" x2="91.44" y2="99.06" width="0.1524" layer="91"/>
<wire x1="91.44" y1="99.06" x2="91.44" y2="83.82" width="0.1524" layer="91"/>
<wire x1="91.44" y1="83.82" x2="91.44" y2="81.28" width="0.1524" layer="91"/>
<wire x1="96.52" y1="104.14" x2="91.44" y2="104.14" width="0.1524" layer="91"/>
<junction x="91.44" y="104.14"/>
<pinref part="U9" gate="G$1" pin="14"/>
<wire x1="96.52" y1="99.06" x2="91.44" y2="99.06" width="0.1524" layer="91"/>
<junction x="91.44" y="99.06"/>
<pinref part="C63" gate="G$1" pin="1"/>
<wire x1="88.9" y1="83.82" x2="91.44" y2="83.82" width="0.1524" layer="91"/>
<junction x="91.44" y="83.82"/>
</segment>
<segment>
<pinref part="SUPPLY106" gate="GND" pin="GND"/>
<pinref part="C61" gate="G$1" pin="2"/>
<wire x1="68.58" y1="81.28" x2="68.58" y2="83.82" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C65" gate="G$1" pin="2"/>
<pinref part="SUPPLY107" gate="GND" pin="GND"/>
<wire x1="60.96" y1="127" x2="60.96" y2="124.46" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R92" gate="G$1" pin="1"/>
<pinref part="SUPPLY108" gate="GND" pin="GND"/>
<wire x1="45.72" y1="134.62" x2="43.18" y2="134.62" width="0.1524" layer="91"/>
<wire x1="43.18" y1="134.62" x2="43.18" y2="132.08" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R90" gate="G$1" pin="1"/>
<pinref part="SUPPLY109" gate="GND" pin="GND"/>
<wire x1="45.72" y1="91.44" x2="43.18" y2="91.44" width="0.1524" layer="91"/>
<wire x1="43.18" y1="91.44" x2="43.18" y2="88.9" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U8" gate="G$1" pin="32"/>
<wire x1="281.94" y1="226.06" x2="284.48" y2="226.06" width="0.1524" layer="91"/>
<wire x1="284.48" y1="226.06" x2="284.48" y2="223.52" width="0.1524" layer="91"/>
<pinref part="U8" gate="G$1" pin="31"/>
<wire x1="284.48" y1="223.52" x2="284.48" y2="210.82" width="0.1524" layer="91"/>
<wire x1="284.48" y1="210.82" x2="284.48" y2="208.28" width="0.1524" layer="91"/>
<wire x1="284.48" y1="208.28" x2="284.48" y2="175.26" width="0.1524" layer="91"/>
<wire x1="281.94" y1="223.52" x2="284.48" y2="223.52" width="0.1524" layer="91"/>
<pinref part="U8" gate="G$1" pin="26"/>
<wire x1="281.94" y1="210.82" x2="284.48" y2="210.82" width="0.1524" layer="91"/>
<pinref part="U8" gate="G$1" pin="25"/>
<wire x1="281.94" y1="208.28" x2="284.48" y2="208.28" width="0.1524" layer="91"/>
<junction x="284.48" y="223.52"/>
<junction x="284.48" y="210.82"/>
<junction x="284.48" y="208.28"/>
<pinref part="SUPPLY116" gate="GND" pin="GND"/>
<pinref part="U8" gate="G$1" pin="33"/>
<wire x1="281.94" y1="228.6" x2="284.48" y2="228.6" width="0.1524" layer="91"/>
<wire x1="284.48" y1="228.6" x2="284.48" y2="226.06" width="0.1524" layer="91"/>
<junction x="284.48" y="226.06"/>
</segment>
<segment>
<pinref part="U8" gate="G$1" pin="11"/>
<wire x1="246.38" y1="200.66" x2="241.3" y2="200.66" width="0.1524" layer="91"/>
<wire x1="241.3" y1="200.66" x2="241.3" y2="198.12" width="0.1524" layer="91"/>
<pinref part="SUPPLY117" gate="GND" pin="GND"/>
<pinref part="U8" gate="G$1" pin="12"/>
<wire x1="241.3" y1="198.12" x2="241.3" y2="193.04" width="0.1524" layer="91"/>
<wire x1="241.3" y1="193.04" x2="241.3" y2="177.8" width="0.1524" layer="91"/>
<wire x1="241.3" y1="177.8" x2="241.3" y2="175.26" width="0.1524" layer="91"/>
<wire x1="246.38" y1="198.12" x2="241.3" y2="198.12" width="0.1524" layer="91"/>
<junction x="241.3" y="198.12"/>
<pinref part="U8" gate="G$1" pin="14"/>
<wire x1="246.38" y1="193.04" x2="241.3" y2="193.04" width="0.1524" layer="91"/>
<junction x="241.3" y="193.04"/>
<pinref part="C62" gate="G$1" pin="1"/>
<wire x1="238.76" y1="177.8" x2="241.3" y2="177.8" width="0.1524" layer="91"/>
<junction x="241.3" y="177.8"/>
</segment>
<segment>
<pinref part="SUPPLY118" gate="GND" pin="GND"/>
<pinref part="C60" gate="G$1" pin="2"/>
<wire x1="218.44" y1="175.26" x2="218.44" y2="177.8" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C64" gate="G$1" pin="2"/>
<pinref part="SUPPLY119" gate="GND" pin="GND"/>
<wire x1="210.82" y1="220.98" x2="210.82" y2="218.44" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R91" gate="G$1" pin="1"/>
<pinref part="SUPPLY120" gate="GND" pin="GND"/>
<wire x1="195.58" y1="228.6" x2="193.04" y2="228.6" width="0.1524" layer="91"/>
<wire x1="193.04" y1="228.6" x2="193.04" y2="226.06" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R89" gate="G$1" pin="1"/>
<pinref part="SUPPLY121" gate="GND" pin="GND"/>
<wire x1="195.58" y1="185.42" x2="193.04" y2="185.42" width="0.1524" layer="91"/>
<wire x1="193.04" y1="185.42" x2="193.04" y2="182.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="32"/>
<wire x1="281.94" y1="132.08" x2="284.48" y2="132.08" width="0.1524" layer="91"/>
<wire x1="284.48" y1="132.08" x2="284.48" y2="129.54" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="31"/>
<wire x1="284.48" y1="129.54" x2="284.48" y2="116.84" width="0.1524" layer="91"/>
<wire x1="284.48" y1="116.84" x2="284.48" y2="114.3" width="0.1524" layer="91"/>
<wire x1="284.48" y1="114.3" x2="284.48" y2="81.28" width="0.1524" layer="91"/>
<wire x1="281.94" y1="129.54" x2="284.48" y2="129.54" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="26"/>
<wire x1="281.94" y1="116.84" x2="284.48" y2="116.84" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="25"/>
<wire x1="281.94" y1="114.3" x2="284.48" y2="114.3" width="0.1524" layer="91"/>
<junction x="284.48" y="129.54"/>
<junction x="284.48" y="116.84"/>
<junction x="284.48" y="114.3"/>
<pinref part="SUPPLY128" gate="GND" pin="GND"/>
<pinref part="U5" gate="G$1" pin="33"/>
<wire x1="281.94" y1="134.62" x2="284.48" y2="134.62" width="0.1524" layer="91"/>
<wire x1="284.48" y1="134.62" x2="284.48" y2="132.08" width="0.1524" layer="91"/>
<junction x="284.48" y="132.08"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="11"/>
<wire x1="246.38" y1="106.68" x2="241.3" y2="106.68" width="0.1524" layer="91"/>
<wire x1="241.3" y1="106.68" x2="241.3" y2="104.14" width="0.1524" layer="91"/>
<pinref part="SUPPLY129" gate="GND" pin="GND"/>
<pinref part="U5" gate="G$1" pin="12"/>
<wire x1="241.3" y1="104.14" x2="241.3" y2="99.06" width="0.1524" layer="91"/>
<wire x1="241.3" y1="99.06" x2="241.3" y2="83.82" width="0.1524" layer="91"/>
<wire x1="241.3" y1="83.82" x2="241.3" y2="81.28" width="0.1524" layer="91"/>
<wire x1="246.38" y1="104.14" x2="241.3" y2="104.14" width="0.1524" layer="91"/>
<junction x="241.3" y="104.14"/>
<pinref part="U5" gate="G$1" pin="14"/>
<wire x1="246.38" y1="99.06" x2="241.3" y2="99.06" width="0.1524" layer="91"/>
<junction x="241.3" y="99.06"/>
<pinref part="C52" gate="G$1" pin="1"/>
<wire x1="238.76" y1="83.82" x2="241.3" y2="83.82" width="0.1524" layer="91"/>
<junction x="241.3" y="83.82"/>
</segment>
<segment>
<pinref part="SUPPLY130" gate="GND" pin="GND"/>
<pinref part="C51" gate="G$1" pin="2"/>
<wire x1="218.44" y1="81.28" x2="218.44" y2="83.82" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C56" gate="G$1" pin="2"/>
<pinref part="SUPPLY131" gate="GND" pin="GND"/>
<wire x1="210.82" y1="127" x2="210.82" y2="124.46" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R76" gate="G$1" pin="1"/>
<pinref part="SUPPLY132" gate="GND" pin="GND"/>
<wire x1="195.58" y1="134.62" x2="193.04" y2="134.62" width="0.1524" layer="91"/>
<wire x1="193.04" y1="134.62" x2="193.04" y2="132.08" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R60" gate="G$1" pin="1"/>
<pinref part="SUPPLY133" gate="GND" pin="GND"/>
<wire x1="195.58" y1="91.44" x2="193.04" y2="91.44" width="0.1524" layer="91"/>
<wire x1="193.04" y1="91.44" x2="193.04" y2="88.9" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J5" gate="G$6" pin="1"/>
<wire x1="353.06" y1="205.74" x2="335.28" y2="205.74" width="0.1524" layer="91"/>
<label x="337.82" y="205.74" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="J6" gate="G$6" pin="1"/>
<wire x1="353.06" y1="170.18" x2="335.28" y2="170.18" width="0.1524" layer="91"/>
<label x="337.82" y="170.18" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="J7" gate="G$6" pin="1"/>
<wire x1="353.06" y1="134.62" x2="335.28" y2="134.62" width="0.1524" layer="91"/>
<label x="337.82" y="134.62" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="J4" gate="G$6" pin="1"/>
<wire x1="353.06" y1="101.6" x2="335.28" y2="101.6" width="0.1524" layer="91"/>
<label x="337.82" y="101.6" size="1.27" layer="95"/>
</segment>
</net>
<net name="N$163" class="0">
<segment>
<pinref part="U7" gate="G$1" pin="17"/>
<pinref part="C59" gate="G$1" pin="1"/>
<wire x1="134.62" y1="187.96" x2="139.7" y2="187.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$164" class="0">
<segment>
<pinref part="U7" gate="G$1" pin="18"/>
<wire x1="134.62" y1="190.5" x2="152.4" y2="190.5" width="0.1524" layer="91"/>
<wire x1="152.4" y1="190.5" x2="152.4" y2="187.96" width="0.1524" layer="91"/>
<pinref part="C59" gate="G$1" pin="2"/>
<wire x1="152.4" y1="187.96" x2="149.86" y2="187.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$165" class="0">
<segment>
<pinref part="U7" gate="G$1" pin="19"/>
<pinref part="C58" gate="G$1" pin="1"/>
<wire x1="134.62" y1="193.04" x2="139.7" y2="193.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$169" class="0">
<segment>
<pinref part="U7" gate="G$1" pin="15"/>
<pinref part="R74" gate="G$1" pin="2"/>
<wire x1="99.06" y1="190.5" x2="86.36" y2="190.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$170" class="0">
<segment>
<pinref part="U7" gate="G$1" pin="6"/>
<wire x1="99.06" y1="213.36" x2="83.82" y2="213.36" width="0.1524" layer="91"/>
<pinref part="R58" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$173" class="0">
<segment>
<pinref part="R57" gate="G$1" pin="2"/>
<pinref part="R58" gate="G$1" pin="1"/>
<wire x1="58.42" y1="233.68" x2="60.96" y2="233.68" width="0.1524" layer="91"/>
<pinref part="C48" gate="G$1" pin="1"/>
<wire x1="60.96" y1="233.68" x2="63.5" y2="233.68" width="0.1524" layer="91"/>
<wire x1="63.5" y1="233.68" x2="68.58" y2="233.68" width="0.1524" layer="91"/>
<wire x1="68.58" y1="233.68" x2="68.58" y2="213.36" width="0.1524" layer="91"/>
<wire x1="68.58" y1="213.36" x2="73.66" y2="213.36" width="0.1524" layer="91"/>
<wire x1="63.5" y1="231.14" x2="63.5" y2="233.68" width="0.1524" layer="91"/>
<pinref part="R52" gate="G$1" pin="2"/>
<wire x1="58.42" y1="228.6" x2="60.96" y2="228.6" width="0.1524" layer="91"/>
<wire x1="60.96" y1="228.6" x2="60.96" y2="233.68" width="0.1524" layer="91"/>
<junction x="60.96" y="233.68"/>
<junction x="63.5" y="233.68"/>
</segment>
</net>
<net name="N$174" class="0">
<segment>
<pinref part="R75" gate="G$1" pin="2"/>
<pinref part="R74" gate="G$1" pin="1"/>
<wire x1="58.42" y1="190.5" x2="60.96" y2="190.5" width="0.1524" layer="91"/>
<pinref part="C54" gate="G$1" pin="1"/>
<wire x1="60.96" y1="190.5" x2="66.04" y2="190.5" width="0.1524" layer="91"/>
<wire x1="66.04" y1="190.5" x2="71.12" y2="190.5" width="0.1524" layer="91"/>
<wire x1="71.12" y1="190.5" x2="76.2" y2="190.5" width="0.1524" layer="91"/>
<wire x1="71.12" y1="187.96" x2="71.12" y2="190.5" width="0.1524" layer="91"/>
<pinref part="R70" gate="G$1" pin="2"/>
<wire x1="58.42" y1="185.42" x2="60.96" y2="185.42" width="0.1524" layer="91"/>
<wire x1="60.96" y1="185.42" x2="60.96" y2="190.5" width="0.1524" layer="91"/>
<junction x="60.96" y="190.5"/>
<junction x="71.12" y="190.5"/>
<pinref part="R115" gate="G$1" pin="2"/>
<wire x1="58.42" y1="195.58" x2="66.04" y2="195.58" width="0.1524" layer="91"/>
<wire x1="66.04" y1="195.58" x2="66.04" y2="190.5" width="0.1524" layer="91"/>
<junction x="66.04" y="190.5"/>
</segment>
</net>
<net name="LB1_MOTXN" class="0">
<segment>
<pinref part="U7" gate="G$1" pin="30"/>
<wire x1="134.62" y1="220.98" x2="139.7" y2="220.98" width="0.1524" layer="91"/>
<pinref part="U7" gate="G$1" pin="29"/>
<wire x1="139.7" y1="220.98" x2="154.94" y2="220.98" width="0.1524" layer="91"/>
<wire x1="134.62" y1="218.44" x2="139.7" y2="218.44" width="0.1524" layer="91"/>
<wire x1="139.7" y1="218.44" x2="139.7" y2="220.98" width="0.1524" layer="91"/>
<junction x="139.7" y="220.98"/>
<label x="142.24" y="220.98" size="1.27" layer="95"/>
<junction x="154.94" y="220.98"/>
</segment>
<segment>
<pinref part="J5" gate="G$3" pin="1"/>
<wire x1="353.06" y1="213.36" x2="335.28" y2="213.36" width="0.1524" layer="91"/>
<label x="337.82" y="213.36" size="1.27" layer="95"/>
</segment>
</net>
<net name="LB1_MOTYP" class="0">
<segment>
<pinref part="U7" gate="G$1" pin="28"/>
<wire x1="134.62" y1="215.9" x2="139.7" y2="215.9" width="0.1524" layer="91"/>
<pinref part="U7" gate="G$1" pin="27"/>
<wire x1="139.7" y1="215.9" x2="154.94" y2="215.9" width="0.1524" layer="91"/>
<wire x1="134.62" y1="213.36" x2="139.7" y2="213.36" width="0.1524" layer="91"/>
<wire x1="139.7" y1="213.36" x2="139.7" y2="215.9" width="0.1524" layer="91"/>
<junction x="139.7" y="215.9"/>
<label x="142.24" y="215.9" size="1.27" layer="95"/>
<junction x="154.94" y="215.9"/>
</segment>
<segment>
<pinref part="J5" gate="G$2" pin="1"/>
<wire x1="353.06" y1="215.9" x2="335.28" y2="215.9" width="0.1524" layer="91"/>
<label x="337.82" y="215.9" size="1.27" layer="95"/>
</segment>
</net>
<net name="LB1_MOTYN" class="0">
<segment>
<pinref part="U7" gate="G$1" pin="24"/>
<wire x1="134.62" y1="205.74" x2="139.7" y2="205.74" width="0.1524" layer="91"/>
<pinref part="U7" gate="G$1" pin="23"/>
<wire x1="139.7" y1="205.74" x2="154.94" y2="205.74" width="0.1524" layer="91"/>
<wire x1="134.62" y1="203.2" x2="139.7" y2="203.2" width="0.1524" layer="91"/>
<wire x1="139.7" y1="203.2" x2="139.7" y2="205.74" width="0.1524" layer="91"/>
<junction x="139.7" y="205.74"/>
<label x="142.24" y="205.74" size="1.27" layer="95"/>
<junction x="154.94" y="205.74"/>
</segment>
<segment>
<pinref part="J5" gate="G$1" pin="1"/>
<wire x1="353.06" y1="218.44" x2="335.28" y2="218.44" width="0.1524" layer="91"/>
<label x="337.82" y="218.44" size="1.27" layer="95"/>
</segment>
</net>
<net name="LB1_MOTXP" class="0">
<segment>
<pinref part="U7" gate="G$1" pin="2"/>
<wire x1="99.06" y1="223.52" x2="96.52" y2="223.52" width="0.1524" layer="91"/>
<wire x1="96.52" y1="223.52" x2="96.52" y2="226.06" width="0.1524" layer="91"/>
<wire x1="96.52" y1="226.06" x2="81.28" y2="226.06" width="0.1524" layer="91"/>
<pinref part="U7" gate="G$1" pin="1"/>
<wire x1="99.06" y1="226.06" x2="96.52" y2="226.06" width="0.1524" layer="91"/>
<junction x="96.52" y="226.06"/>
<label x="83.82" y="226.06" size="1.27" layer="95"/>
<junction x="81.28" y="226.06"/>
</segment>
<segment>
<pinref part="J5" gate="G$4" pin="1"/>
<wire x1="353.06" y1="210.82" x2="335.28" y2="210.82" width="0.1524" layer="91"/>
<label x="337.82" y="210.82" size="1.27" layer="95"/>
</segment>
</net>
<net name="N$175" class="0">
<segment>
<pinref part="R59" gate="G$1" pin="2"/>
<pinref part="U7" gate="G$1" pin="8"/>
<wire x1="83.82" y1="208.28" x2="88.9" y2="208.28" width="0.1524" layer="91"/>
<pinref part="R55" gate="G$1" pin="2"/>
<wire x1="88.9" y1="208.28" x2="99.06" y2="208.28" width="0.1524" layer="91"/>
<wire x1="83.82" y1="210.82" x2="88.9" y2="210.82" width="0.1524" layer="91"/>
<wire x1="88.9" y1="210.82" x2="88.9" y2="208.28" width="0.1524" layer="91"/>
<junction x="88.9" y="208.28"/>
</segment>
</net>
<net name="N$176" class="0">
<segment>
<pinref part="U7" gate="G$1" pin="9"/>
<pinref part="R61" gate="G$1" pin="2"/>
<wire x1="99.06" y1="205.74" x2="88.9" y2="205.74" width="0.1524" layer="91"/>
<pinref part="R63" gate="G$1" pin="2"/>
<wire x1="88.9" y1="205.74" x2="83.82" y2="205.74" width="0.1524" layer="91"/>
<wire x1="83.82" y1="203.2" x2="88.9" y2="203.2" width="0.1524" layer="91"/>
<wire x1="88.9" y1="203.2" x2="88.9" y2="205.74" width="0.1524" layer="91"/>
<junction x="88.9" y="205.74"/>
</segment>
</net>
<net name="SDA1_5V" class="0">
<segment>
<pinref part="R55" gate="G$1" pin="1"/>
<wire x1="73.66" y1="210.82" x2="68.58" y2="210.82" width="0.1524" layer="91"/>
<label x="68.58" y="210.82" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R85" gate="G$1" pin="1"/>
<wire x1="71.12" y1="116.84" x2="66.04" y2="116.84" width="0.1524" layer="91"/>
<label x="66.04" y="116.84" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R77" gate="G$1" pin="1"/>
<wire x1="220.98" y1="210.82" x2="215.9" y2="210.82" width="0.1524" layer="91"/>
<label x="215.9" y="210.82" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R73" gate="G$1" pin="1"/>
<wire x1="220.98" y1="116.84" x2="215.9" y2="116.84" width="0.1524" layer="91"/>
<label x="215.9" y="116.84" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SDA2_5V" class="0">
<segment>
<pinref part="R59" gate="G$1" pin="1"/>
<wire x1="73.66" y1="208.28" x2="68.58" y2="208.28" width="0.1524" layer="91"/>
<label x="68.58" y="208.28" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R86" gate="G$1" pin="1"/>
<wire x1="71.12" y1="114.3" x2="66.04" y2="114.3" width="0.1524" layer="91"/>
<label x="66.04" y="114.3" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R78" gate="G$1" pin="1"/>
<wire x1="220.98" y1="208.28" x2="215.9" y2="208.28" width="0.1524" layer="91"/>
<label x="215.9" y="208.28" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R67" gate="G$1" pin="1"/>
<wire x1="220.98" y1="114.3" x2="215.9" y2="114.3" width="0.1524" layer="91"/>
<label x="215.9" y="114.3" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SCL1_5V" class="0">
<segment>
<pinref part="R61" gate="G$1" pin="1"/>
<wire x1="73.66" y1="205.74" x2="68.58" y2="205.74" width="0.1524" layer="91"/>
<label x="68.58" y="205.74" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R87" gate="G$1" pin="1"/>
<wire x1="71.12" y1="111.76" x2="66.04" y2="111.76" width="0.1524" layer="91"/>
<label x="66.04" y="111.76" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R79" gate="G$1" pin="1"/>
<wire x1="220.98" y1="205.74" x2="215.9" y2="205.74" width="0.1524" layer="91"/>
<label x="215.9" y="205.74" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R66" gate="G$1" pin="1"/>
<wire x1="220.98" y1="111.76" x2="215.9" y2="111.76" width="0.1524" layer="91"/>
<label x="215.9" y="111.76" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SCL2_5V" class="0">
<segment>
<pinref part="R63" gate="G$1" pin="1"/>
<wire x1="73.66" y1="203.2" x2="68.58" y2="203.2" width="0.1524" layer="91"/>
<label x="68.58" y="203.2" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R88" gate="G$1" pin="1"/>
<wire x1="71.12" y1="109.22" x2="66.04" y2="109.22" width="0.1524" layer="91"/>
<label x="66.04" y="109.22" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R80" gate="G$1" pin="1"/>
<wire x1="220.98" y1="203.2" x2="215.9" y2="203.2" width="0.1524" layer="91"/>
<label x="215.9" y="203.2" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R65" gate="G$1" pin="1"/>
<wire x1="220.98" y1="109.22" x2="215.9" y2="109.22" width="0.1524" layer="91"/>
<label x="215.9" y="109.22" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$192" class="0">
<segment>
<pinref part="U9" gate="G$1" pin="17"/>
<pinref part="C67" gate="G$1" pin="1"/>
<wire x1="132.08" y1="93.98" x2="137.16" y2="93.98" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$193" class="0">
<segment>
<pinref part="U9" gate="G$1" pin="18"/>
<wire x1="132.08" y1="96.52" x2="149.86" y2="96.52" width="0.1524" layer="91"/>
<wire x1="149.86" y1="96.52" x2="149.86" y2="93.98" width="0.1524" layer="91"/>
<pinref part="C67" gate="G$1" pin="2"/>
<wire x1="149.86" y1="93.98" x2="147.32" y2="93.98" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$194" class="0">
<segment>
<pinref part="U9" gate="G$1" pin="19"/>
<pinref part="C69" gate="G$1" pin="1"/>
<wire x1="132.08" y1="99.06" x2="137.16" y2="99.06" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$197" class="0">
<segment>
<pinref part="U9" gate="G$1" pin="15"/>
<pinref part="R83" gate="G$1" pin="2"/>
<wire x1="96.52" y1="96.52" x2="83.82" y2="96.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$198" class="0">
<segment>
<pinref part="U9" gate="G$1" pin="6"/>
<wire x1="96.52" y1="119.38" x2="81.28" y2="119.38" width="0.1524" layer="91"/>
<pinref part="R94" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$200" class="0">
<segment>
<pinref part="R96" gate="G$1" pin="2"/>
<pinref part="R94" gate="G$1" pin="1"/>
<wire x1="55.88" y1="139.7" x2="58.42" y2="139.7" width="0.1524" layer="91"/>
<pinref part="C65" gate="G$1" pin="1"/>
<wire x1="58.42" y1="139.7" x2="60.96" y2="139.7" width="0.1524" layer="91"/>
<wire x1="60.96" y1="139.7" x2="66.04" y2="139.7" width="0.1524" layer="91"/>
<wire x1="66.04" y1="139.7" x2="66.04" y2="119.38" width="0.1524" layer="91"/>
<wire x1="66.04" y1="119.38" x2="71.12" y2="119.38" width="0.1524" layer="91"/>
<wire x1="60.96" y1="137.16" x2="60.96" y2="139.7" width="0.1524" layer="91"/>
<pinref part="R92" gate="G$1" pin="2"/>
<wire x1="55.88" y1="134.62" x2="58.42" y2="134.62" width="0.1524" layer="91"/>
<wire x1="58.42" y1="134.62" x2="58.42" y2="139.7" width="0.1524" layer="91"/>
<junction x="58.42" y="139.7"/>
<junction x="60.96" y="139.7"/>
</segment>
</net>
<net name="N$201" class="0">
<segment>
<pinref part="R84" gate="G$1" pin="2"/>
<pinref part="R83" gate="G$1" pin="1"/>
<wire x1="55.88" y1="96.52" x2="58.42" y2="96.52" width="0.1524" layer="91"/>
<pinref part="C61" gate="G$1" pin="1"/>
<wire x1="58.42" y1="96.52" x2="63.5" y2="96.52" width="0.1524" layer="91"/>
<wire x1="63.5" y1="96.52" x2="68.58" y2="96.52" width="0.1524" layer="91"/>
<wire x1="68.58" y1="96.52" x2="73.66" y2="96.52" width="0.1524" layer="91"/>
<wire x1="68.58" y1="93.98" x2="68.58" y2="96.52" width="0.1524" layer="91"/>
<pinref part="R90" gate="G$1" pin="2"/>
<wire x1="55.88" y1="91.44" x2="58.42" y2="91.44" width="0.1524" layer="91"/>
<wire x1="58.42" y1="91.44" x2="58.42" y2="96.52" width="0.1524" layer="91"/>
<junction x="58.42" y="96.52"/>
<junction x="68.58" y="96.52"/>
<pinref part="R116" gate="G$1" pin="2"/>
<wire x1="55.88" y1="101.6" x2="63.5" y2="101.6" width="0.1524" layer="91"/>
<wire x1="63.5" y1="101.6" x2="63.5" y2="96.52" width="0.1524" layer="91"/>
<junction x="63.5" y="96.52"/>
</segment>
</net>
<net name="N$202" class="0">
<segment>
<pinref part="R86" gate="G$1" pin="2"/>
<pinref part="U9" gate="G$1" pin="8"/>
<wire x1="81.28" y1="114.3" x2="86.36" y2="114.3" width="0.1524" layer="91"/>
<pinref part="R85" gate="G$1" pin="2"/>
<wire x1="86.36" y1="114.3" x2="96.52" y2="114.3" width="0.1524" layer="91"/>
<wire x1="81.28" y1="116.84" x2="86.36" y2="116.84" width="0.1524" layer="91"/>
<wire x1="86.36" y1="116.84" x2="86.36" y2="114.3" width="0.1524" layer="91"/>
<junction x="86.36" y="114.3"/>
</segment>
</net>
<net name="N$203" class="0">
<segment>
<pinref part="U9" gate="G$1" pin="9"/>
<pinref part="R87" gate="G$1" pin="2"/>
<wire x1="96.52" y1="111.76" x2="86.36" y2="111.76" width="0.1524" layer="91"/>
<pinref part="R88" gate="G$1" pin="2"/>
<wire x1="86.36" y1="111.76" x2="81.28" y2="111.76" width="0.1524" layer="91"/>
<wire x1="81.28" y1="109.22" x2="86.36" y2="109.22" width="0.1524" layer="91"/>
<wire x1="86.36" y1="109.22" x2="86.36" y2="111.76" width="0.1524" layer="91"/>
<junction x="86.36" y="111.76"/>
</segment>
</net>
<net name="N$216" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="17"/>
<pinref part="C66" gate="G$1" pin="1"/>
<wire x1="281.94" y1="187.96" x2="287.02" y2="187.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$217" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="18"/>
<wire x1="281.94" y1="190.5" x2="299.72" y2="190.5" width="0.1524" layer="91"/>
<wire x1="299.72" y1="190.5" x2="299.72" y2="187.96" width="0.1524" layer="91"/>
<pinref part="C66" gate="G$1" pin="2"/>
<wire x1="299.72" y1="187.96" x2="297.18" y2="187.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$218" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="19"/>
<pinref part="C68" gate="G$1" pin="1"/>
<wire x1="281.94" y1="193.04" x2="287.02" y2="193.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$221" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="15"/>
<pinref part="R81" gate="G$1" pin="2"/>
<wire x1="246.38" y1="190.5" x2="233.68" y2="190.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$222" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="6"/>
<wire x1="246.38" y1="213.36" x2="231.14" y2="213.36" width="0.1524" layer="91"/>
<pinref part="R93" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$224" class="0">
<segment>
<pinref part="R95" gate="G$1" pin="2"/>
<pinref part="R93" gate="G$1" pin="1"/>
<wire x1="205.74" y1="233.68" x2="208.28" y2="233.68" width="0.1524" layer="91"/>
<pinref part="C64" gate="G$1" pin="1"/>
<wire x1="208.28" y1="233.68" x2="210.82" y2="233.68" width="0.1524" layer="91"/>
<wire x1="210.82" y1="233.68" x2="215.9" y2="233.68" width="0.1524" layer="91"/>
<wire x1="215.9" y1="233.68" x2="215.9" y2="213.36" width="0.1524" layer="91"/>
<wire x1="215.9" y1="213.36" x2="220.98" y2="213.36" width="0.1524" layer="91"/>
<wire x1="210.82" y1="231.14" x2="210.82" y2="233.68" width="0.1524" layer="91"/>
<pinref part="R91" gate="G$1" pin="2"/>
<wire x1="205.74" y1="228.6" x2="208.28" y2="228.6" width="0.1524" layer="91"/>
<wire x1="208.28" y1="228.6" x2="208.28" y2="233.68" width="0.1524" layer="91"/>
<junction x="208.28" y="233.68"/>
<junction x="210.82" y="233.68"/>
</segment>
</net>
<net name="N$225" class="0">
<segment>
<pinref part="R82" gate="G$1" pin="2"/>
<pinref part="R81" gate="G$1" pin="1"/>
<wire x1="205.74" y1="190.5" x2="208.28" y2="190.5" width="0.1524" layer="91"/>
<pinref part="C60" gate="G$1" pin="1"/>
<wire x1="208.28" y1="190.5" x2="213.36" y2="190.5" width="0.1524" layer="91"/>
<wire x1="213.36" y1="190.5" x2="218.44" y2="190.5" width="0.1524" layer="91"/>
<wire x1="218.44" y1="190.5" x2="223.52" y2="190.5" width="0.1524" layer="91"/>
<wire x1="218.44" y1="187.96" x2="218.44" y2="190.5" width="0.1524" layer="91"/>
<pinref part="R89" gate="G$1" pin="2"/>
<wire x1="205.74" y1="185.42" x2="208.28" y2="185.42" width="0.1524" layer="91"/>
<wire x1="208.28" y1="185.42" x2="208.28" y2="190.5" width="0.1524" layer="91"/>
<junction x="208.28" y="190.5"/>
<junction x="218.44" y="190.5"/>
<pinref part="R117" gate="G$1" pin="2"/>
<wire x1="205.74" y1="195.58" x2="213.36" y2="195.58" width="0.1524" layer="91"/>
<wire x1="213.36" y1="195.58" x2="213.36" y2="190.5" width="0.1524" layer="91"/>
<junction x="213.36" y="190.5"/>
</segment>
</net>
<net name="N$226" class="0">
<segment>
<pinref part="R78" gate="G$1" pin="2"/>
<pinref part="U8" gate="G$1" pin="8"/>
<wire x1="231.14" y1="208.28" x2="236.22" y2="208.28" width="0.1524" layer="91"/>
<pinref part="R77" gate="G$1" pin="2"/>
<wire x1="236.22" y1="208.28" x2="246.38" y2="208.28" width="0.1524" layer="91"/>
<wire x1="231.14" y1="210.82" x2="236.22" y2="210.82" width="0.1524" layer="91"/>
<wire x1="236.22" y1="210.82" x2="236.22" y2="208.28" width="0.1524" layer="91"/>
<junction x="236.22" y="208.28"/>
</segment>
</net>
<net name="N$227" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="9"/>
<pinref part="R79" gate="G$1" pin="2"/>
<wire x1="246.38" y1="205.74" x2="236.22" y2="205.74" width="0.1524" layer="91"/>
<pinref part="R80" gate="G$1" pin="2"/>
<wire x1="236.22" y1="205.74" x2="231.14" y2="205.74" width="0.1524" layer="91"/>
<wire x1="231.14" y1="203.2" x2="236.22" y2="203.2" width="0.1524" layer="91"/>
<wire x1="236.22" y1="203.2" x2="236.22" y2="205.74" width="0.1524" layer="91"/>
<junction x="236.22" y="205.74"/>
</segment>
</net>
<net name="N$240" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="17"/>
<pinref part="C46" gate="G$1" pin="1"/>
<wire x1="281.94" y1="93.98" x2="287.02" y2="93.98" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$241" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="18"/>
<wire x1="281.94" y1="96.52" x2="299.72" y2="96.52" width="0.1524" layer="91"/>
<wire x1="299.72" y1="96.52" x2="299.72" y2="93.98" width="0.1524" layer="91"/>
<pinref part="C46" gate="G$1" pin="2"/>
<wire x1="299.72" y1="93.98" x2="297.18" y2="93.98" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$242" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="19"/>
<pinref part="C47" gate="G$1" pin="1"/>
<wire x1="281.94" y1="99.06" x2="287.02" y2="99.06" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$245" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="15"/>
<pinref part="R54" gate="G$1" pin="2"/>
<wire x1="246.38" y1="96.52" x2="233.68" y2="96.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$246" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="6"/>
<wire x1="246.38" y1="119.38" x2="231.14" y2="119.38" width="0.1524" layer="91"/>
<pinref part="R71" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$248" class="0">
<segment>
<pinref part="R72" gate="G$1" pin="2"/>
<pinref part="R71" gate="G$1" pin="1"/>
<wire x1="205.74" y1="139.7" x2="208.28" y2="139.7" width="0.1524" layer="91"/>
<pinref part="C56" gate="G$1" pin="1"/>
<wire x1="208.28" y1="139.7" x2="210.82" y2="139.7" width="0.1524" layer="91"/>
<wire x1="210.82" y1="139.7" x2="215.9" y2="139.7" width="0.1524" layer="91"/>
<wire x1="215.9" y1="139.7" x2="215.9" y2="119.38" width="0.1524" layer="91"/>
<wire x1="215.9" y1="119.38" x2="220.98" y2="119.38" width="0.1524" layer="91"/>
<wire x1="210.82" y1="137.16" x2="210.82" y2="139.7" width="0.1524" layer="91"/>
<pinref part="R76" gate="G$1" pin="2"/>
<wire x1="205.74" y1="134.62" x2="208.28" y2="134.62" width="0.1524" layer="91"/>
<wire x1="208.28" y1="134.62" x2="208.28" y2="139.7" width="0.1524" layer="91"/>
<junction x="208.28" y="139.7"/>
<junction x="210.82" y="139.7"/>
</segment>
</net>
<net name="N$249" class="0">
<segment>
<pinref part="R53" gate="G$1" pin="2"/>
<pinref part="R54" gate="G$1" pin="1"/>
<wire x1="205.74" y1="96.52" x2="208.28" y2="96.52" width="0.1524" layer="91"/>
<pinref part="C51" gate="G$1" pin="1"/>
<wire x1="208.28" y1="96.52" x2="213.36" y2="96.52" width="0.1524" layer="91"/>
<wire x1="213.36" y1="96.52" x2="218.44" y2="96.52" width="0.1524" layer="91"/>
<wire x1="218.44" y1="96.52" x2="223.52" y2="96.52" width="0.1524" layer="91"/>
<wire x1="218.44" y1="93.98" x2="218.44" y2="96.52" width="0.1524" layer="91"/>
<pinref part="R60" gate="G$1" pin="2"/>
<wire x1="205.74" y1="91.44" x2="208.28" y2="91.44" width="0.1524" layer="91"/>
<wire x1="208.28" y1="91.44" x2="208.28" y2="96.52" width="0.1524" layer="91"/>
<junction x="208.28" y="96.52"/>
<junction x="218.44" y="96.52"/>
<pinref part="R118" gate="G$1" pin="2"/>
<wire x1="205.74" y1="101.6" x2="213.36" y2="101.6" width="0.1524" layer="91"/>
<wire x1="213.36" y1="101.6" x2="213.36" y2="96.52" width="0.1524" layer="91"/>
<junction x="213.36" y="96.52"/>
</segment>
</net>
<net name="N$250" class="0">
<segment>
<pinref part="R67" gate="G$1" pin="2"/>
<pinref part="U5" gate="G$1" pin="8"/>
<wire x1="231.14" y1="114.3" x2="236.22" y2="114.3" width="0.1524" layer="91"/>
<pinref part="R73" gate="G$1" pin="2"/>
<wire x1="236.22" y1="114.3" x2="246.38" y2="114.3" width="0.1524" layer="91"/>
<wire x1="231.14" y1="116.84" x2="236.22" y2="116.84" width="0.1524" layer="91"/>
<wire x1="236.22" y1="116.84" x2="236.22" y2="114.3" width="0.1524" layer="91"/>
<junction x="236.22" y="114.3"/>
</segment>
</net>
<net name="N$251" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="9"/>
<pinref part="R66" gate="G$1" pin="2"/>
<wire x1="246.38" y1="111.76" x2="236.22" y2="111.76" width="0.1524" layer="91"/>
<pinref part="R65" gate="G$1" pin="2"/>
<wire x1="236.22" y1="111.76" x2="231.14" y2="111.76" width="0.1524" layer="91"/>
<wire x1="231.14" y1="109.22" x2="236.22" y2="109.22" width="0.1524" layer="91"/>
<wire x1="236.22" y1="109.22" x2="236.22" y2="111.76" width="0.1524" layer="91"/>
<junction x="236.22" y="111.76"/>
</segment>
</net>
<net name="DIR_1" class="0">
<segment>
<pinref part="J5" gate="G$7" pin="1"/>
<wire x1="353.06" y1="203.2" x2="335.28" y2="203.2" width="0.1524" layer="91"/>
<label x="337.82" y="203.2" size="1.27" layer="95"/>
</segment>
</net>
<net name="SPEED_1" class="0">
<segment>
<pinref part="J5" gate="G$8" pin="1"/>
<wire x1="353.06" y1="200.66" x2="335.28" y2="200.66" width="0.1524" layer="91"/>
<label x="337.82" y="200.66" size="1.27" layer="95"/>
</segment>
</net>
<net name="DIR_2" class="0">
<segment>
<pinref part="J6" gate="G$7" pin="1"/>
<wire x1="353.06" y1="167.64" x2="335.28" y2="167.64" width="0.1524" layer="91"/>
<label x="337.82" y="167.64" size="1.27" layer="95"/>
</segment>
</net>
<net name="SPEED_2" class="0">
<segment>
<pinref part="J6" gate="G$8" pin="1"/>
<wire x1="353.06" y1="165.1" x2="335.28" y2="165.1" width="0.1524" layer="91"/>
<label x="337.82" y="165.1" size="1.27" layer="95"/>
</segment>
</net>
<net name="DIR_3" class="0">
<segment>
<pinref part="J7" gate="G$7" pin="1"/>
<wire x1="353.06" y1="132.08" x2="335.28" y2="132.08" width="0.1524" layer="91"/>
<label x="337.82" y="132.08" size="1.27" layer="95"/>
</segment>
</net>
<net name="SPEED_3" class="0">
<segment>
<pinref part="J7" gate="G$8" pin="1"/>
<wire x1="353.06" y1="129.54" x2="335.28" y2="129.54" width="0.1524" layer="91"/>
<label x="337.82" y="129.54" size="1.27" layer="95"/>
</segment>
</net>
<net name="DIR_4" class="0">
<segment>
<pinref part="J4" gate="G$7" pin="1"/>
<wire x1="353.06" y1="99.06" x2="335.28" y2="99.06" width="0.1524" layer="91"/>
<label x="337.82" y="99.06" size="1.27" layer="95"/>
</segment>
</net>
<net name="SPEED_4" class="0">
<segment>
<pinref part="J4" gate="G$8" pin="1"/>
<wire x1="353.06" y1="96.52" x2="335.28" y2="96.52" width="0.1524" layer="91"/>
<label x="337.82" y="96.52" size="1.27" layer="95"/>
</segment>
</net>
<net name="MOTOR_VGE" class="0">
<segment>
<pinref part="J5" gate="G$5" pin="1"/>
<wire x1="353.06" y1="208.28" x2="335.28" y2="208.28" width="0.1524" layer="91"/>
<label x="337.82" y="208.28" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="J6" gate="G$5" pin="1"/>
<wire x1="353.06" y1="172.72" x2="335.28" y2="172.72" width="0.1524" layer="91"/>
<label x="337.82" y="172.72" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="J7" gate="G$5" pin="1"/>
<wire x1="353.06" y1="137.16" x2="335.28" y2="137.16" width="0.1524" layer="91"/>
<label x="337.82" y="137.16" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="J4" gate="G$5" pin="1"/>
<wire x1="353.06" y1="104.14" x2="335.28" y2="104.14" width="0.1524" layer="91"/>
<label x="337.82" y="104.14" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="R57" gate="G$1" pin="1"/>
<wire x1="48.26" y1="233.68" x2="45.72" y2="233.68" width="0.1524" layer="91"/>
<label x="45.72" y="233.68" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U7" gate="G$1" pin="4"/>
<wire x1="99.06" y1="218.44" x2="96.52" y2="218.44" width="0.1524" layer="91"/>
<pinref part="U7" gate="G$1" pin="3"/>
<wire x1="99.06" y1="220.98" x2="96.52" y2="220.98" width="0.1524" layer="91"/>
<pinref part="U7" gate="G$1" pin="5"/>
<wire x1="96.52" y1="220.98" x2="88.9" y2="220.98" width="0.1524" layer="91"/>
<wire x1="99.06" y1="215.9" x2="96.52" y2="215.9" width="0.1524" layer="91"/>
<wire x1="96.52" y1="215.9" x2="96.52" y2="218.44" width="0.1524" layer="91"/>
<junction x="96.52" y="220.98"/>
<junction x="96.52" y="218.44"/>
<wire x1="96.52" y1="218.44" x2="96.52" y2="220.98" width="0.1524" layer="91"/>
<label x="88.9" y="220.98" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R75" gate="G$1" pin="1"/>
<wire x1="48.26" y1="190.5" x2="45.72" y2="190.5" width="0.1524" layer="91"/>
<label x="45.72" y="190.5" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="C58" gate="G$1" pin="2"/>
<wire x1="149.86" y1="193.04" x2="152.4" y2="193.04" width="0.1524" layer="91"/>
<wire x1="152.4" y1="193.04" x2="152.4" y2="195.58" width="0.1524" layer="91"/>
<pinref part="U7" gate="G$1" pin="20"/>
<wire x1="134.62" y1="195.58" x2="152.4" y2="195.58" width="0.1524" layer="91"/>
<pinref part="U7" gate="G$1" pin="22"/>
<wire x1="134.62" y1="200.66" x2="152.4" y2="200.66" width="0.1524" layer="91"/>
<wire x1="152.4" y1="195.58" x2="152.4" y2="198.12" width="0.1524" layer="91"/>
<wire x1="152.4" y1="198.12" x2="152.4" y2="200.66" width="0.1524" layer="91"/>
<wire x1="152.4" y1="200.66" x2="154.94" y2="200.66" width="0.1524" layer="91"/>
<junction x="152.4" y="200.66"/>
<pinref part="U7" gate="G$1" pin="21"/>
<wire x1="134.62" y1="198.12" x2="152.4" y2="198.12" width="0.1524" layer="91"/>
<junction x="152.4" y="198.12"/>
<junction x="152.4" y="195.58"/>
<label x="154.94" y="200.66" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U9" gate="G$1" pin="4"/>
<wire x1="96.52" y1="124.46" x2="93.98" y2="124.46" width="0.1524" layer="91"/>
<pinref part="U9" gate="G$1" pin="3"/>
<wire x1="96.52" y1="127" x2="93.98" y2="127" width="0.1524" layer="91"/>
<pinref part="U9" gate="G$1" pin="5"/>
<wire x1="93.98" y1="127" x2="86.36" y2="127" width="0.1524" layer="91"/>
<wire x1="96.52" y1="121.92" x2="93.98" y2="121.92" width="0.1524" layer="91"/>
<wire x1="93.98" y1="121.92" x2="93.98" y2="124.46" width="0.1524" layer="91"/>
<junction x="93.98" y="127"/>
<junction x="93.98" y="124.46"/>
<wire x1="93.98" y1="124.46" x2="93.98" y2="127" width="0.1524" layer="91"/>
<label x="86.36" y="127" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R96" gate="G$1" pin="1"/>
<wire x1="45.72" y1="139.7" x2="43.18" y2="139.7" width="0.1524" layer="91"/>
<label x="43.18" y="139.7" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R84" gate="G$1" pin="1"/>
<wire x1="45.72" y1="96.52" x2="43.18" y2="96.52" width="0.1524" layer="91"/>
<label x="43.18" y="96.52" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="C69" gate="G$1" pin="2"/>
<wire x1="147.32" y1="99.06" x2="149.86" y2="99.06" width="0.1524" layer="91"/>
<wire x1="149.86" y1="99.06" x2="149.86" y2="101.6" width="0.1524" layer="91"/>
<pinref part="U9" gate="G$1" pin="20"/>
<wire x1="132.08" y1="101.6" x2="149.86" y2="101.6" width="0.1524" layer="91"/>
<pinref part="U9" gate="G$1" pin="22"/>
<wire x1="132.08" y1="106.68" x2="149.86" y2="106.68" width="0.1524" layer="91"/>
<wire x1="149.86" y1="101.6" x2="149.86" y2="104.14" width="0.1524" layer="91"/>
<wire x1="149.86" y1="104.14" x2="149.86" y2="106.68" width="0.1524" layer="91"/>
<wire x1="149.86" y1="106.68" x2="152.4" y2="106.68" width="0.1524" layer="91"/>
<junction x="149.86" y="106.68"/>
<pinref part="U9" gate="G$1" pin="21"/>
<wire x1="132.08" y1="104.14" x2="149.86" y2="104.14" width="0.1524" layer="91"/>
<junction x="149.86" y="104.14"/>
<junction x="149.86" y="101.6"/>
<label x="152.4" y="106.68" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U8" gate="G$1" pin="4"/>
<wire x1="246.38" y1="218.44" x2="243.84" y2="218.44" width="0.1524" layer="91"/>
<pinref part="U8" gate="G$1" pin="3"/>
<wire x1="246.38" y1="220.98" x2="243.84" y2="220.98" width="0.1524" layer="91"/>
<pinref part="U8" gate="G$1" pin="5"/>
<wire x1="243.84" y1="220.98" x2="236.22" y2="220.98" width="0.1524" layer="91"/>
<wire x1="246.38" y1="215.9" x2="243.84" y2="215.9" width="0.1524" layer="91"/>
<wire x1="243.84" y1="215.9" x2="243.84" y2="218.44" width="0.1524" layer="91"/>
<junction x="243.84" y="220.98"/>
<junction x="243.84" y="218.44"/>
<wire x1="243.84" y1="218.44" x2="243.84" y2="220.98" width="0.1524" layer="91"/>
<label x="236.22" y="220.98" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R95" gate="G$1" pin="1"/>
<wire x1="195.58" y1="233.68" x2="193.04" y2="233.68" width="0.1524" layer="91"/>
<label x="193.04" y="233.68" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R82" gate="G$1" pin="1"/>
<wire x1="195.58" y1="190.5" x2="193.04" y2="190.5" width="0.1524" layer="91"/>
<label x="193.04" y="190.5" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="C68" gate="G$1" pin="2"/>
<wire x1="297.18" y1="193.04" x2="299.72" y2="193.04" width="0.1524" layer="91"/>
<wire x1="299.72" y1="193.04" x2="299.72" y2="195.58" width="0.1524" layer="91"/>
<pinref part="U8" gate="G$1" pin="20"/>
<wire x1="281.94" y1="195.58" x2="299.72" y2="195.58" width="0.1524" layer="91"/>
<pinref part="U8" gate="G$1" pin="22"/>
<wire x1="281.94" y1="200.66" x2="299.72" y2="200.66" width="0.1524" layer="91"/>
<wire x1="299.72" y1="195.58" x2="299.72" y2="198.12" width="0.1524" layer="91"/>
<wire x1="299.72" y1="198.12" x2="299.72" y2="200.66" width="0.1524" layer="91"/>
<wire x1="299.72" y1="200.66" x2="302.26" y2="200.66" width="0.1524" layer="91"/>
<junction x="299.72" y="200.66"/>
<pinref part="U8" gate="G$1" pin="21"/>
<wire x1="281.94" y1="198.12" x2="299.72" y2="198.12" width="0.1524" layer="91"/>
<junction x="299.72" y="198.12"/>
<junction x="299.72" y="195.58"/>
<label x="302.26" y="200.66" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="4"/>
<wire x1="246.38" y1="124.46" x2="243.84" y2="124.46" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="3"/>
<wire x1="246.38" y1="127" x2="243.84" y2="127" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="5"/>
<wire x1="243.84" y1="127" x2="236.22" y2="127" width="0.1524" layer="91"/>
<wire x1="246.38" y1="121.92" x2="243.84" y2="121.92" width="0.1524" layer="91"/>
<wire x1="243.84" y1="121.92" x2="243.84" y2="124.46" width="0.1524" layer="91"/>
<junction x="243.84" y="127"/>
<junction x="243.84" y="124.46"/>
<wire x1="243.84" y1="124.46" x2="243.84" y2="127" width="0.1524" layer="91"/>
<label x="236.22" y="127" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R72" gate="G$1" pin="1"/>
<wire x1="195.58" y1="139.7" x2="193.04" y2="139.7" width="0.1524" layer="91"/>
<label x="193.04" y="139.7" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R53" gate="G$1" pin="1"/>
<wire x1="195.58" y1="96.52" x2="193.04" y2="96.52" width="0.1524" layer="91"/>
<label x="193.04" y="96.52" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="C47" gate="G$1" pin="2"/>
<wire x1="297.18" y1="99.06" x2="299.72" y2="99.06" width="0.1524" layer="91"/>
<wire x1="299.72" y1="99.06" x2="299.72" y2="101.6" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="20"/>
<wire x1="281.94" y1="101.6" x2="299.72" y2="101.6" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="22"/>
<wire x1="281.94" y1="106.68" x2="299.72" y2="106.68" width="0.1524" layer="91"/>
<wire x1="299.72" y1="101.6" x2="299.72" y2="104.14" width="0.1524" layer="91"/>
<wire x1="299.72" y1="104.14" x2="299.72" y2="106.68" width="0.1524" layer="91"/>
<wire x1="299.72" y1="106.68" x2="302.26" y2="106.68" width="0.1524" layer="91"/>
<junction x="299.72" y="106.68"/>
<pinref part="U5" gate="G$1" pin="21"/>
<wire x1="281.94" y1="104.14" x2="299.72" y2="104.14" width="0.1524" layer="91"/>
<junction x="299.72" y="104.14"/>
<junction x="299.72" y="101.6"/>
<label x="302.26" y="106.68" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="LB3_MOTYN" class="0">
<segment>
<pinref part="J7" gate="G$1" pin="1"/>
<wire x1="353.06" y1="147.32" x2="335.28" y2="147.32" width="0.1524" layer="91"/>
<label x="337.82" y="147.32" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="U8" gate="G$1" pin="24"/>
<wire x1="281.94" y1="205.74" x2="287.02" y2="205.74" width="0.1524" layer="91"/>
<pinref part="U8" gate="G$1" pin="23"/>
<wire x1="287.02" y1="205.74" x2="302.26" y2="205.74" width="0.1524" layer="91"/>
<wire x1="281.94" y1="203.2" x2="287.02" y2="203.2" width="0.1524" layer="91"/>
<wire x1="287.02" y1="203.2" x2="287.02" y2="205.74" width="0.1524" layer="91"/>
<junction x="287.02" y="205.74"/>
<label x="289.56" y="205.74" size="1.27" layer="95"/>
<junction x="302.26" y="205.74"/>
</segment>
</net>
<net name="LB3_MOTYP" class="0">
<segment>
<pinref part="J7" gate="G$2" pin="1"/>
<wire x1="353.06" y1="144.78" x2="335.28" y2="144.78" width="0.1524" layer="91"/>
<label x="337.82" y="144.78" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="U8" gate="G$1" pin="28"/>
<wire x1="281.94" y1="215.9" x2="287.02" y2="215.9" width="0.1524" layer="91"/>
<pinref part="U8" gate="G$1" pin="27"/>
<wire x1="287.02" y1="215.9" x2="302.26" y2="215.9" width="0.1524" layer="91"/>
<wire x1="281.94" y1="213.36" x2="287.02" y2="213.36" width="0.1524" layer="91"/>
<wire x1="287.02" y1="213.36" x2="287.02" y2="215.9" width="0.1524" layer="91"/>
<junction x="287.02" y="215.9"/>
<label x="289.56" y="215.9" size="1.27" layer="95"/>
<junction x="302.26" y="215.9"/>
</segment>
</net>
<net name="LB3_MOTXP" class="0">
<segment>
<pinref part="J7" gate="G$4" pin="1"/>
<wire x1="353.06" y1="139.7" x2="335.28" y2="139.7" width="0.1524" layer="91"/>
<label x="337.82" y="139.7" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="U8" gate="G$1" pin="2"/>
<wire x1="246.38" y1="223.52" x2="243.84" y2="223.52" width="0.1524" layer="91"/>
<wire x1="243.84" y1="223.52" x2="243.84" y2="226.06" width="0.1524" layer="91"/>
<wire x1="243.84" y1="226.06" x2="228.6" y2="226.06" width="0.1524" layer="91"/>
<pinref part="U8" gate="G$1" pin="1"/>
<wire x1="246.38" y1="226.06" x2="243.84" y2="226.06" width="0.1524" layer="91"/>
<junction x="243.84" y="226.06"/>
<label x="231.14" y="226.06" size="1.27" layer="95"/>
<junction x="228.6" y="226.06"/>
</segment>
</net>
<net name="LB2_MOTYN" class="0">
<segment>
<pinref part="J6" gate="G$1" pin="1"/>
<wire x1="353.06" y1="182.88" x2="335.28" y2="182.88" width="0.1524" layer="91"/>
<label x="337.82" y="182.88" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="U9" gate="G$1" pin="24"/>
<wire x1="132.08" y1="111.76" x2="137.16" y2="111.76" width="0.1524" layer="91"/>
<pinref part="U9" gate="G$1" pin="23"/>
<wire x1="137.16" y1="111.76" x2="152.4" y2="111.76" width="0.1524" layer="91"/>
<wire x1="132.08" y1="109.22" x2="137.16" y2="109.22" width="0.1524" layer="91"/>
<wire x1="137.16" y1="109.22" x2="137.16" y2="111.76" width="0.1524" layer="91"/>
<junction x="137.16" y="111.76"/>
<label x="139.7" y="111.76" size="1.27" layer="95"/>
<junction x="152.4" y="111.76"/>
</segment>
</net>
<net name="LB2_MOTYP" class="0">
<segment>
<pinref part="J6" gate="G$2" pin="1"/>
<wire x1="353.06" y1="180.34" x2="335.28" y2="180.34" width="0.1524" layer="91"/>
<label x="337.82" y="180.34" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="U9" gate="G$1" pin="28"/>
<wire x1="132.08" y1="121.92" x2="137.16" y2="121.92" width="0.1524" layer="91"/>
<pinref part="U9" gate="G$1" pin="27"/>
<wire x1="137.16" y1="121.92" x2="152.4" y2="121.92" width="0.1524" layer="91"/>
<wire x1="132.08" y1="119.38" x2="137.16" y2="119.38" width="0.1524" layer="91"/>
<wire x1="137.16" y1="119.38" x2="137.16" y2="121.92" width="0.1524" layer="91"/>
<junction x="137.16" y="121.92"/>
<label x="139.7" y="121.92" size="1.27" layer="95"/>
<junction x="152.4" y="121.92"/>
</segment>
</net>
<net name="LB2_MOTXN" class="0">
<segment>
<pinref part="J6" gate="G$3" pin="1"/>
<wire x1="353.06" y1="177.8" x2="335.28" y2="177.8" width="0.1524" layer="91"/>
<label x="337.82" y="177.8" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="U9" gate="G$1" pin="30"/>
<wire x1="132.08" y1="127" x2="137.16" y2="127" width="0.1524" layer="91"/>
<pinref part="U9" gate="G$1" pin="29"/>
<wire x1="137.16" y1="127" x2="152.4" y2="127" width="0.1524" layer="91"/>
<wire x1="132.08" y1="124.46" x2="137.16" y2="124.46" width="0.1524" layer="91"/>
<wire x1="137.16" y1="124.46" x2="137.16" y2="127" width="0.1524" layer="91"/>
<junction x="137.16" y="127"/>
<label x="139.7" y="127" size="1.27" layer="95"/>
<junction x="152.4" y="127"/>
</segment>
</net>
<net name="LB2_MOTXP" class="0">
<segment>
<pinref part="J6" gate="G$4" pin="1"/>
<wire x1="353.06" y1="175.26" x2="335.28" y2="175.26" width="0.1524" layer="91"/>
<label x="337.82" y="175.26" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="U9" gate="G$1" pin="2"/>
<wire x1="96.52" y1="129.54" x2="93.98" y2="129.54" width="0.1524" layer="91"/>
<wire x1="93.98" y1="129.54" x2="93.98" y2="132.08" width="0.1524" layer="91"/>
<wire x1="93.98" y1="132.08" x2="78.74" y2="132.08" width="0.1524" layer="91"/>
<pinref part="U9" gate="G$1" pin="1"/>
<wire x1="96.52" y1="132.08" x2="93.98" y2="132.08" width="0.1524" layer="91"/>
<junction x="93.98" y="132.08"/>
<label x="81.28" y="132.08" size="1.27" layer="95"/>
<junction x="78.74" y="132.08"/>
</segment>
</net>
<net name="LB4_MOTYN" class="0">
<segment>
<pinref part="J4" gate="G$1" pin="1"/>
<wire x1="353.06" y1="114.3" x2="335.28" y2="114.3" width="0.1524" layer="91"/>
<label x="337.82" y="114.3" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="24"/>
<wire x1="281.94" y1="111.76" x2="287.02" y2="111.76" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="23"/>
<wire x1="287.02" y1="111.76" x2="302.26" y2="111.76" width="0.1524" layer="91"/>
<wire x1="281.94" y1="109.22" x2="287.02" y2="109.22" width="0.1524" layer="91"/>
<wire x1="287.02" y1="109.22" x2="287.02" y2="111.76" width="0.1524" layer="91"/>
<junction x="287.02" y="111.76"/>
<label x="289.56" y="111.76" size="1.27" layer="95"/>
<junction x="302.26" y="111.76"/>
</segment>
</net>
<net name="LB4_MOTYP" class="0">
<segment>
<pinref part="J4" gate="G$2" pin="1"/>
<wire x1="353.06" y1="111.76" x2="335.28" y2="111.76" width="0.1524" layer="91"/>
<label x="337.82" y="111.76" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="28"/>
<wire x1="281.94" y1="121.92" x2="287.02" y2="121.92" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="27"/>
<wire x1="287.02" y1="121.92" x2="302.26" y2="121.92" width="0.1524" layer="91"/>
<wire x1="281.94" y1="119.38" x2="287.02" y2="119.38" width="0.1524" layer="91"/>
<wire x1="287.02" y1="119.38" x2="287.02" y2="121.92" width="0.1524" layer="91"/>
<junction x="287.02" y="121.92"/>
<label x="289.56" y="121.92" size="1.27" layer="95"/>
<junction x="302.26" y="121.92"/>
</segment>
</net>
<net name="LB4_MOTXN" class="0">
<segment>
<pinref part="J4" gate="G$3" pin="1"/>
<wire x1="353.06" y1="109.22" x2="335.28" y2="109.22" width="0.1524" layer="91"/>
<label x="337.82" y="109.22" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="30"/>
<wire x1="281.94" y1="127" x2="287.02" y2="127" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="29"/>
<wire x1="287.02" y1="127" x2="302.26" y2="127" width="0.1524" layer="91"/>
<wire x1="281.94" y1="124.46" x2="287.02" y2="124.46" width="0.1524" layer="91"/>
<wire x1="287.02" y1="124.46" x2="287.02" y2="127" width="0.1524" layer="91"/>
<junction x="287.02" y="127"/>
<label x="289.56" y="127" size="1.27" layer="95"/>
<junction x="302.26" y="127"/>
</segment>
</net>
<net name="LB4_MOTXP" class="0">
<segment>
<pinref part="J4" gate="G$4" pin="1"/>
<wire x1="353.06" y1="106.68" x2="335.28" y2="106.68" width="0.1524" layer="91"/>
<label x="337.82" y="106.68" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="2"/>
<wire x1="246.38" y1="129.54" x2="243.84" y2="129.54" width="0.1524" layer="91"/>
<wire x1="243.84" y1="129.54" x2="243.84" y2="132.08" width="0.1524" layer="91"/>
<wire x1="243.84" y1="132.08" x2="228.6" y2="132.08" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="1"/>
<wire x1="246.38" y1="132.08" x2="243.84" y2="132.08" width="0.1524" layer="91"/>
<junction x="243.84" y="132.08"/>
<label x="231.14" y="132.08" size="1.27" layer="95"/>
<junction x="228.6" y="132.08"/>
</segment>
</net>
<net name="LB3_MOTXN" class="0">
<segment>
<pinref part="J7" gate="G$3" pin="1"/>
<wire x1="353.06" y1="142.24" x2="335.28" y2="142.24" width="0.1524" layer="91"/>
<label x="337.82" y="142.24" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="U8" gate="G$1" pin="30"/>
<wire x1="281.94" y1="220.98" x2="287.02" y2="220.98" width="0.1524" layer="91"/>
<pinref part="U8" gate="G$1" pin="29"/>
<wire x1="287.02" y1="220.98" x2="302.26" y2="220.98" width="0.1524" layer="91"/>
<wire x1="281.94" y1="218.44" x2="287.02" y2="218.44" width="0.1524" layer="91"/>
<wire x1="287.02" y1="218.44" x2="287.02" y2="220.98" width="0.1524" layer="91"/>
<junction x="287.02" y="220.98"/>
<label x="289.56" y="220.98" size="1.27" layer="95"/>
<junction x="302.26" y="220.98"/>
</segment>
</net>
<net name="HW1" class="0">
<segment>
<pinref part="R115" gate="G$1" pin="1"/>
<wire x1="48.26" y1="195.58" x2="43.18" y2="195.58" width="0.1524" layer="91"/>
<label x="43.18" y="195.58" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="HW2" class="0">
<segment>
<pinref part="R116" gate="G$1" pin="1"/>
<wire x1="45.72" y1="101.6" x2="40.64" y2="101.6" width="0.1524" layer="91"/>
<label x="40.64" y="101.6" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="HW3" class="0">
<segment>
<pinref part="R117" gate="G$1" pin="1"/>
<wire x1="195.58" y1="195.58" x2="190.5" y2="195.58" width="0.1524" layer="91"/>
<label x="190.5" y="195.58" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="HW4" class="0">
<segment>
<pinref part="R118" gate="G$1" pin="1"/>
<wire x1="195.58" y1="101.6" x2="190.5" y2="101.6" width="0.1524" layer="91"/>
<label x="190.5" y="101.6" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="+5V0" class="0">
<segment>
<pinref part="U7" gate="G$1" pin="10"/>
<wire x1="99.06" y1="203.2" x2="91.44" y2="203.2" width="0.1524" layer="91"/>
<wire x1="91.44" y1="203.2" x2="91.44" y2="200.66" width="0.1524" layer="91"/>
<label x="68.58" y="200.66" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="C53" gate="G$1" pin="2"/>
<wire x1="91.44" y1="200.66" x2="68.58" y2="200.66" width="0.1524" layer="91"/>
<wire x1="91.44" y1="187.96" x2="91.44" y2="200.66" width="0.1524" layer="91"/>
<junction x="91.44" y="200.66"/>
</segment>
<segment>
<pinref part="U9" gate="G$1" pin="10"/>
<wire x1="96.52" y1="109.22" x2="88.9" y2="109.22" width="0.1524" layer="91"/>
<wire x1="88.9" y1="109.22" x2="88.9" y2="106.68" width="0.1524" layer="91"/>
<label x="66.04" y="106.68" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="C63" gate="G$1" pin="2"/>
<wire x1="88.9" y1="106.68" x2="66.04" y2="106.68" width="0.1524" layer="91"/>
<wire x1="88.9" y1="93.98" x2="88.9" y2="106.68" width="0.1524" layer="91"/>
<junction x="88.9" y="106.68"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="10"/>
<wire x1="246.38" y1="109.22" x2="238.76" y2="109.22" width="0.1524" layer="91"/>
<wire x1="238.76" y1="109.22" x2="238.76" y2="106.68" width="0.1524" layer="91"/>
<label x="215.9" y="106.68" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="C52" gate="G$1" pin="2"/>
<wire x1="238.76" y1="106.68" x2="215.9" y2="106.68" width="0.1524" layer="91"/>
<wire x1="238.76" y1="93.98" x2="238.76" y2="106.68" width="0.1524" layer="91"/>
<junction x="238.76" y="106.68"/>
</segment>
<segment>
<pinref part="U8" gate="G$1" pin="10"/>
<wire x1="246.38" y1="203.2" x2="238.76" y2="203.2" width="0.1524" layer="91"/>
<wire x1="238.76" y1="203.2" x2="238.76" y2="200.66" width="0.1524" layer="91"/>
<label x="215.9" y="200.66" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="C62" gate="G$1" pin="2"/>
<wire x1="238.76" y1="200.66" x2="215.9" y2="200.66" width="0.1524" layer="91"/>
<wire x1="238.76" y1="187.96" x2="238.76" y2="200.66" width="0.1524" layer="91"/>
<junction x="238.76" y="200.66"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
<errors>
<approved hash="113,5,193.571,130.071,FRAME2,,,,,"/>
<approved hash="113,4,193.571,130.071,FRAME3,,,,,"/>
<approved hash="113,3,193.571,130.071,FRAME4,,,,,"/>
<approved hash="113,2,193.571,130.071,FRAME5,,,,,"/>
<approved hash="113,1,193.571,130.071,FRAME7,,,,,"/>
</errors>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
<note version="9.5" severity="warning">
Since Version 9.5, EAGLE supports persistent groups with
schematics, and board files. Those persistent groups
will not be understood (or retained) with this version.
</note>
<note version="9.0" severity="warning">
Since Version 9.0, EAGLE supports the align property for labels. 
Labels in schematic will not be understood with this version. Update EAGLE to the latest version 
for full support of labels. 
</note>
</compatibility>
</eagle>
