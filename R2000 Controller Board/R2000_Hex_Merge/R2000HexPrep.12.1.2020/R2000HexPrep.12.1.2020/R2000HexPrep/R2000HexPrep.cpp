// R2000HexPrep.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdio.h>
#include <memory.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>

#define uint16_t unsigned short
#define uint8_t unsigned char
#define uint32_t unsigned long

#define APP_CODE_MAGIC_NUMBER_ADDR	0x0003FFF0
#define APP_CODE_REVISION_INFO_ADDR	0x0003FFF4	// (w.x.y.z where each letter corresponds to a byte)
#define APP_CODE_RAM_END_ADDR		0x0003FFF8
#define APP_CODE_CRC_LOCATION		0x0003FFFC

#define APP_MAGIC_NUMBER			0x4A4D4157	// (=�JMAW�)

#define MAX_IMAGE_SIZE			0x40000

#define APP_BASE_ADDRESS		0x2000

uint32_t maxAddr = 0;
uint32_t minAddr = 0;
uint32_t IHExtendedAddr = 0;
uint8_t *binImage;

uint16_t A2H( uint8_t *buf, uint8_t size );
int DecodeIntelHexLine( char *inLine);
uint32_t revIndian( uint32_t in);
uint8_t sum( char * inString );
uint32_t crc32b(uint8_t *message, uint32_t min, uint32_t max);

#pragma pack(1)
struct InfoHeader {
	uint8_t	Type[5];
	uint8_t Version;
	uint32_t Length; // file length
	uint8_t flags;	// future use
	uint8_t HwID;	// 
	char fileName[32];
	uint8_t Spare[12];
	uint32_t pgmEnd;
	uint32_t Checksum;
} infoHeader;	// size should be 64

int _tmain(int argc, char * argv[])
{
	FILE *fpIn, *fpOutBin, *fpOutHex;
	char inLine[80], outLine[80];
	uint32_t data;
	int i;
	int len;
	uint32_t VersionNumber;
	uint32_t outCrc32 = 0xffffffff;
	uint32_t binFileLen;

	// check syntax
	if( argc != 4 ) {
		printf("Usage: %s <Input file name> <Output file base name> <Version number>\n", (char *)argv[0] );
		return 0;
	}

	// open input file
	if( (fpIn = fopen((const char *)argv[1],"r")) == (FILE *)NULL ) {
		printf("Cannot open %s for reading\n", argv[1] );
		return 0;
	}
	
	// open output binary file
	sprintf(inLine,"%s.bin", (const char *)argv[2]);
	if( (fpOutBin = fopen(inLine,"wb")) == (FILE *)NULL ) {
		printf("Cannot open %s for writing\n", argv[2] );
		fclose(fpIn);
		return 0;
	}
	// open output hex file
	sprintf(inLine,"%s.hex", (const char *)argv[2]);
	if( (fpOutHex = fopen(inLine,"w")) == (FILE *)NULL ) {
		printf("Cannot open %s for writing\n", argv[2] );
		fclose(fpIn);
		return 0;
	}
	if ( sscanf(argv[3],"%08x", &VersionNumber ) != 1 ) {
		printf("Invalid version number %s\n", argv[3] );
		fclose(fpIn);
		fclose(fpOutHex);
		fclose(fpOutBin);
		return 0;
	}
	if( (binImage=(uint8_t *)malloc(MAX_IMAGE_SIZE)) == NULL ) {
		printf("Sufficient memory unavailable\n");
		fclose(fpIn);
		fclose(fpOutHex);
		fclose(fpOutBin);
		return 0;
	}
	// set all to 0xff
	memset((void *)binImage,(int)0xff,MAX_IMAGE_SIZE);

	// for each input line, read it
	while( fgets(inLine,80,fpIn) != NULL ) {
		len = strlen(inLine);
		if (DecodeIntelHexLine(inLine) != 0) {
			break;
		}
		// last line of input file?
		if( strncmp( inLine, ":00000001FF", 11 ) == 0 ) {
			// find max address (lowest address that is 0xffffffff)
			for( i=((MAX_IMAGE_SIZE-1)&0xfffffffc); i>0 ; i-=4 ) {
				data = binImage[i];
				data += (binImage[i+1])<<8;
				data += (binImage[i+2])<<16;
				data += (binImage[i+3])<<24;
				if( data != 0xffffffff ) {
					maxAddr = i;
					break;
				}
			}
			// find min address (lowest address that is not 0xffffffff)
			for( i=0; i<((MAX_IMAGE_SIZE-1)&0xfffffffc) ; i+=4 ) {
				data = binImage[i];
				data += (binImage[i+1])<<8;
				data += (binImage[i+2])<<16;
				data += (binImage[i+3])<<24;
				if( data != 0xffffffff ) {
					minAddr = i;
					break;
				}
			}
			// pad maxAddr to uint32_t boundary
			maxAddr += 0x3;
			maxAddr &= 0xffffc;		// max address is last non 0xffffffff
			// pad minAddr to uint32_t boundary
			minAddr -= 0x3;
			minAddr &= 0xffffc;
			minAddr += 4;		// max address is first 0xffffffff
			printf("Min is 0x%08x Max is 0x%08x\n", minAddr, maxAddr );
			outCrc32 = crc32b(binImage, minAddr, maxAddr);
			printf("crc is %08x\n", outCrc32);

			// add the bootloader information block to the end of the binary file
			*((uint32_t *)(&(binImage[APP_CODE_MAGIC_NUMBER_ADDR]))) = APP_MAGIC_NUMBER;
			*((uint32_t *)(&(binImage[APP_CODE_REVISION_INFO_ADDR]))) = VersionNumber;
			*((uint32_t *)(&(binImage[APP_CODE_RAM_END_ADDR]))) = maxAddr;
			*((uint32_t *)(&(binImage[APP_CODE_CRC_LOCATION]))) = outCrc32;

			//  actual file necessary length
			binFileLen = maxAddr - minAddr + 4;
			// round up to make length even muliple of 64
			binFileLen += 63;
			binFileLen -= binFileLen % 64;
			
			memcpy(infoHeader.Type,"R2000",5);
			infoHeader.Version = 0x1;
			infoHeader.Length = binFileLen + sizeof(struct InfoHeader);
			infoHeader.flags = 0;
			infoHeader.HwID = 0;
			memset(infoHeader.fileName,0,32);
			_snprintf(infoHeader.fileName,32,"%s.bin",argv[2]);
			memset(infoHeader.Spare,0,12);
			infoHeader.pgmEnd = maxAddr;
			infoHeader.Checksum = outCrc32;

			// create the header in the binary file
			if( fwrite(&infoHeader,1,sizeof(struct InfoHeader),fpOutBin) != sizeof(struct InfoHeader) ) {
				printf("Error writing file %s\n", argv[2] );
				break;
			}

			// create the rest of the binary file
			if( fwrite(&(binImage[minAddr]),1, binFileLen,fpOutBin) != binFileLen ) {
				printf("Error writing file %s\n", argv[2] );
				break;
			}
			printf("write 0x%x bytes, %d bytes\n", sizeof(struct InfoHeader)+ binFileLen, sizeof(struct InfoHeader)+ binFileLen);
			
			// Now create the bootloader info block at the end of the hex file
			sprintf(outLine,":02000004%04X",(APP_CODE_MAGIC_NUMBER_ADDR>>16) );
			fprintf(fpOutHex,"%s%02X\n", outLine, sum(outLine));
			sprintf(outLine,":04%04X00%08X", (APP_CODE_MAGIC_NUMBER_ADDR&0xffff), revIndian(APP_MAGIC_NUMBER));
			fprintf(fpOutHex,"%s%02X\n", outLine, sum(outLine));
			sprintf(outLine,":02000004%04X",(APP_CODE_REVISION_INFO_ADDR>>16) );
			fprintf(fpOutHex,"%s%02X\n", outLine, sum(outLine));
			sprintf(outLine,":04%04X00%08X", (APP_CODE_REVISION_INFO_ADDR&0xffff), revIndian(VersionNumber));
			fprintf(fpOutHex,"%s%02X\n", outLine, sum(outLine));
			sprintf(outLine,":02000004%04X",(APP_CODE_RAM_END_ADDR>>16) );
			fprintf(fpOutHex,"%s%02X\n", outLine, sum(outLine));
			sprintf(outLine,":04%04X00%08X", (APP_CODE_RAM_END_ADDR&0xffff), revIndian(maxAddr));
			fprintf(fpOutHex,"%s%02X\n", outLine, sum(outLine));
			sprintf(outLine,":02000004%04X",(APP_CODE_CRC_LOCATION>>16) );
			fprintf(fpOutHex,"%s%02X\n", outLine, sum(outLine));
			sprintf(outLine,":04%04X00%08X", (APP_CODE_CRC_LOCATION&0xffff), revIndian(outCrc32));
			fprintf(fpOutHex,"%s%02X\n", outLine, sum(outLine));

		}
		// write the input line to the output hex file
		if( fwrite(inLine,1,len,fpOutHex) != len ) {
			printf("Error writing file %s\n", argv[2] );
			break;
		}

	}

	fclose(fpIn);
	fclose(fpOutBin);
	fclose(fpOutHex);
	free(binImage);
	return 0;
}

uint16_t A2H( uint8_t *buf, uint8_t size )
{
	uint8_t i;
	uint16_t retval;
	
	retval = 0;
	for( i=0; i<size ; i++ ) {
		retval <<= 4;
		if( (buf[i] >= '0') && (buf[i] <= '9') )
			retval += (buf[i]) - '0';
		else if( (buf[i] >= 'A') && (buf[i] <= 'F') )
			retval += buf[i] - 'A' + 10;
		else if( (buf[i] >= 'a') && (buf[i] <= 'f') )
			retval += buf[i] - 'a' + 10;
	}
	return retval;
}


// :CCAAAATTDDDDD.....SS
// CC - byte count (Data Field Size)
// AAAA - Addr
// TT - type of line
//	00 - data
//	01 - End of file
//	02 - Extended segment addr
//	03 - Start segment addr
//	04 - Extended linear addr
//	05 - Start linear addr
// DDDD - Data
// SS sum

int DecodeIntelHexLine(char* inLine)
{
	uint8_t IHCount;
	uint8_t IHType;
	uint32_t IHAddr;
	uint8_t IHSum;
	uint8_t dataByte, i, mySum;

	IHCount = (uint8_t)A2H((uint8_t*)(&(inLine[1])), 2);
	IHAddr = A2H((uint8_t*)(&(inLine[3])), 4);
	IHType = (uint8_t)A2H( (uint8_t *)(&(inLine[7])), 2 );
	IHSum = (uint8_t)A2H( (uint8_t *)(&(inLine[9+(2*IHCount)])), 2 );
	// Check line's checksum
	mySum = IHCount + ((IHAddr>>8)&0xff) + (IHAddr&0xff) + IHType;
	for( i=0; i< IHCount; i++ )
		mySum += A2H( (uint8_t *)(&(inLine[9+(i*2)])), 2 );
	mySum ^= 0xff;
	mySum += 1;
	if( mySum != IHSum ) {
		puts("Input file contains bad checksum\n");
		return -2;		// CheckSum bad, ignore line
	}
	IHAddr += (IHExtendedAddr<<16);
	switch( IHType ) {
		case 0: //	00 - data
			if( IHAddr >= APP_CODE_REVISION_INFO_ADDR ) // throw out RAM data
				break;
			if (IHAddr < APP_BASE_ADDRESS) {
				puts("Input file has code that encroaches on bootloader space\n");
				return -1;
			}

			for (i = 0; i < IHCount; i++) {
				dataByte = (uint8_t)A2H((uint8_t*)(&(inLine[9 + (2 * i)])), 2);
				binImage[IHAddr + i] = dataByte;
			}
			break;
		case 1: //	01 - End of file
			break;
		case 2: //	02 - Extended segment addr
		case 3: //	03 - Start segment addr
		case 4: //	04 - Extended linear addr
		case 5: //	05 - Start linear addr
			IHExtendedAddr = A2H( (uint8_t *)(&(inLine[9])), 4 );
			break;
		default:
			break;
	}
	return 0;
	
}


uint32_t revIndian( uint32_t in)
{
	uint32_t out;
	out = (in&0xff) << 24;
	out += (in&0xff00) << 8;
	out += (in&0xff0000) >> 8;
	out += (in&0xff000000) >> 24;
	return out;
}

uint8_t sum( char * inString )
{
	uint8_t mySum = 0;
	uint8_t i, len;

	// number of 2 character entries in the string
	len = strlen(inString)/2;
	
	// for each 2 character entry (excluding beginning colon)
	for( i=0 ; i< len; i++ ) {
		mySum += (uint8_t)A2H( (uint8_t *)(&(inString[1+(2*i)])), 2 );
	}

	// twos compliment result
	mySum ^= 0xff;
	mySum += 1;
	return mySum;
}


uint32_t crc32b(uint8_t *message, uint32_t minAddr, uint32_t maxAddr) {
   uint32_t i;
   int j;
   uint32_t byte, crc, mask;
   uint8_t temp;

   crc = 0xFFFFFFFF;
   for (i=minAddr; i<maxAddr ; i++) {
      byte = message[i];            // Get next byte.
      crc = crc ^ byte;
      for (j = 7; j >= 0; j--) {    // Do eight times.
         mask = -(crc & 1);
         crc = (crc >> 1) ^ (0xEDB88320 & mask);
      }
	
   }
	
   return crc;

}
