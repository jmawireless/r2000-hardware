<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="8.6.3">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="5" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="no" active="no"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="no" active="no"/>
<layer number="103" name="tMap" color="7" fill="1" visible="no" active="no"/>
<layer number="104" name="Name" color="7" fill="1" visible="no" active="no"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="no" active="no"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="no" active="no"/>
<layer number="107" name="Crop" color="7" fill="1" visible="no" active="no"/>
<layer number="108" name="fp8" color="7" fill="1" visible="yes" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="yes" active="yes"/>
<layer number="110" name="110" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="111" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="7" fill="1" visible="yes" active="yes"/>
<layer number="114" name="FRNTMAAT1" color="7" fill="1" visible="no" active="no"/>
<layer number="115" name="FRNTMAAT2" color="7" fill="1" visible="no" active="no"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="no" active="no"/>
<layer number="117" name="BACKMAAT1" color="7" fill="1" visible="no" active="no"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="yes" active="yes"/>
<layer number="119" name="KAP_TEKEN" color="7" fill="1" visible="no" active="no"/>
<layer number="120" name="KAP_MAAT1" color="7" fill="1" visible="no" active="no"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="no" active="no"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="no" active="no"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="top_silk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="130" name="SMDSTROOK" color="7" fill="1" visible="no" active="no"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="133" name="bottom_silk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="DrillLegend" color="7" fill="1" visible="no" active="no"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="14" fill="1" visible="no" active="no"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="no"/>
<layer number="201" name="201bmp" color="2" fill="1" visible="no" active="no"/>
<layer number="202" name="202bmp" color="3" fill="1" visible="no" active="no"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="no" active="no"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="no" active="no"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="no" active="no"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="no" active="no"/>
<layer number="207" name="207bmp" color="15" fill="10" visible="no" active="no"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="no" active="no"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="Eagle3D_PG1" color="7" fill="1" visible="no" active="no"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="no"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="OrgLBR" color="13" fill="1" visible="no" active="no"/>
<layer number="255" name="Accent" color="7" fill="1" visible="no" active="no"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="frames">
<description>&lt;b&gt;Frames for Sheet and Layout&lt;/b&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="A3_LOC_JMA_RELEASE">
<wire x1="0" y1="241.3" x2="63.5" y2="241.3" width="0.025" layer="94"/>
<wire x1="63.5" y1="241.3" x2="127" y2="241.3" width="0.025" layer="94"/>
<wire x1="127" y1="241.3" x2="190.5" y2="241.3" width="0.025" layer="94"/>
<wire x1="190.5" y1="241.3" x2="254" y2="241.3" width="0.025" layer="94"/>
<wire x1="254" y1="241.3" x2="317.5" y2="241.3" width="0.025" layer="94"/>
<wire x1="317.5" y1="241.3" x2="380.97883125" y2="241.3" width="0.025" layer="94"/>
<wire x1="380.97883125" y1="241.3" x2="380.97883125" y2="0.02116875" width="0.025" layer="94"/>
<wire x1="380.97883125" y1="0.02116875" x2="0" y2="0.02116875" width="0.025" layer="94"/>
<wire x1="0" y1="0.02116875" x2="0" y2="60.325" width="0.025" layer="94"/>
<wire x1="0" y1="60.325" x2="0" y2="120.65" width="0.025" layer="94"/>
<wire x1="0" y1="120.65" x2="0" y2="180.975" width="0.025" layer="94"/>
<wire x1="0" y1="180.975" x2="0" y2="241.3" width="0.025" layer="94"/>
<wire x1="5.079890625" y1="236.22041875" x2="375.898909375" y2="236.22041875" width="0.025" layer="94"/>
<wire x1="375.898909375" y1="236.22041875" x2="375.898909375" y2="5.10075" width="0.025" layer="94"/>
<wire x1="375.898909375" y1="5.10075" x2="5.079890625" y2="5.10075" width="0.025" layer="94"/>
<wire x1="5.079890625" y1="5.10075" x2="5.079890625" y2="236.22041875" width="0.025" layer="94"/>
<wire x1="317.5" y1="5.08" x2="317.5" y2="0" width="0.025" layer="94"/>
<wire x1="317.5" y1="236.22" x2="317.5" y2="241.3" width="0.025" layer="94"/>
<wire x1="254" y1="5.08" x2="254" y2="0" width="0.025" layer="94"/>
<wire x1="254" y1="236.22" x2="254" y2="241.3" width="0.025" layer="94"/>
<wire x1="190.5" y1="5.08" x2="190.5" y2="0" width="0.025" layer="94"/>
<wire x1="190.5" y1="236.22" x2="190.5" y2="241.3" width="0.025" layer="94"/>
<wire x1="127" y1="5.08" x2="127" y2="0" width="0.025" layer="94"/>
<wire x1="127" y1="236.22" x2="127" y2="241.3" width="0.025" layer="94"/>
<wire x1="63.5" y1="5.08" x2="63.5" y2="0" width="0.025" layer="94"/>
<wire x1="63.5" y1="236.22" x2="63.5" y2="241.3" width="0.025" layer="94"/>
<wire x1="5.08" y1="180.975" x2="0" y2="180.975" width="0.025" layer="94"/>
<wire x1="375.92" y1="180.975" x2="381" y2="180.975" width="0.025" layer="94"/>
<wire x1="5.08" y1="120.65" x2="0" y2="120.65" width="0.025" layer="94"/>
<wire x1="375.92" y1="120.65" x2="381" y2="120.65" width="0.025" layer="94"/>
<wire x1="5.08" y1="60.325" x2="0" y2="60.325" width="0.025" layer="94"/>
<wire x1="375.92" y1="60.325" x2="381" y2="60.325" width="0.025" layer="94"/>
<wire x1="271.78" y1="5.08" x2="271.78" y2="21.59" width="0.025" layer="94"/>
<wire x1="271.78" y1="21.59" x2="271.78" y2="27.94" width="0.025" layer="94"/>
<wire x1="271.78" y1="27.94" x2="271.78" y2="34.29" width="0.025" layer="94"/>
<wire x1="271.78" y1="34.29" x2="271.78" y2="40.64" width="0.025" layer="94"/>
<wire x1="271.78" y1="40.64" x2="271.78" y2="45.72" width="0.025" layer="94"/>
<wire x1="271.78" y1="45.72" x2="285.75" y2="45.72" width="0.025" layer="94"/>
<wire x1="285.75" y1="45.72" x2="293.37" y2="45.72" width="0.025" layer="94"/>
<wire x1="293.37" y1="45.72" x2="304.8" y2="45.72" width="0.025" layer="94"/>
<wire x1="304.8" y1="45.72" x2="375.92" y2="45.72" width="0.025" layer="94"/>
<wire x1="304.8" y1="5.08" x2="304.8" y2="8.89" width="0.025" layer="94"/>
<wire x1="304.8" y1="8.89" x2="304.8" y2="15.24" width="0.025" layer="94"/>
<wire x1="304.8" y1="15.24" x2="304.8" y2="21.59" width="0.025" layer="94"/>
<wire x1="304.8" y1="21.59" x2="304.8" y2="27.94" width="0.025" layer="94"/>
<wire x1="304.8" y1="27.94" x2="304.8" y2="34.29" width="0.025" layer="94"/>
<wire x1="304.8" y1="34.29" x2="304.8" y2="40.64" width="0.025" layer="94"/>
<wire x1="304.8" y1="40.64" x2="304.8" y2="45.72" width="0.025" layer="94"/>
<wire x1="271.78" y1="40.64" x2="304.8" y2="40.64" width="0.025" layer="94"/>
<wire x1="271.78" y1="34.29" x2="304.8" y2="34.29" width="0.025" layer="94"/>
<wire x1="271.78" y1="27.94" x2="304.8" y2="27.94" width="0.025" layer="94"/>
<wire x1="271.78" y1="21.59" x2="285.75" y2="21.59" width="0.025" layer="94"/>
<wire x1="285.75" y1="21.59" x2="293.37" y2="21.59" width="0.025" layer="94"/>
<wire x1="293.37" y1="21.59" x2="304.8" y2="21.59" width="0.025" layer="94"/>
<wire x1="304.8" y1="8.89" x2="322.58" y2="8.89" width="0.025" layer="94"/>
<wire x1="322.58" y1="8.89" x2="359.41" y2="8.89" width="0.025" layer="94"/>
<wire x1="359.41" y1="8.89" x2="369.57" y2="8.89" width="0.025" layer="94"/>
<wire x1="369.57" y1="8.89" x2="375.92" y2="8.89" width="0.025" layer="94"/>
<wire x1="322.58" y1="8.89" x2="322.58" y2="5.08" width="0.025" layer="94"/>
<wire x1="359.41" y1="8.89" x2="359.41" y2="5.08" width="0.025" layer="94"/>
<wire x1="304.8" y1="15.24" x2="369.57" y2="15.24" width="0.025" layer="94"/>
<wire x1="369.57" y1="15.24" x2="375.92" y2="15.24" width="0.025" layer="94"/>
<wire x1="369.57" y1="15.24" x2="369.57" y2="8.89" width="0.025" layer="94"/>
<wire x1="304.8" y1="21.59" x2="375.92" y2="21.59" width="0.025" layer="94"/>
<wire x1="285.75" y1="45.72" x2="285.75" y2="21.59" width="0.025" layer="94"/>
<wire x1="293.37" y1="45.72" x2="293.37" y2="21.59" width="0.025" layer="94"/>
<wire x1="307.34" y1="31.75" x2="373.38" y2="31.75" width="0.025" layer="94"/>
<wire x1="329.29703125" y1="44.90211875" x2="329.35418125" y2="44.890690625" width="0.025" layer="94"/>
<wire x1="329.35418125" y1="44.890690625" x2="329.3999" y2="44.879259375" width="0.025" layer="94"/>
<wire x1="329.81138125" y1="44.776390625" x2="329.84566875" y2="44.764959375" width="0.025" layer="94"/>
<wire x1="329.84566875" y1="44.764959375" x2="329.891390625" y2="44.75353125" width="0.025" layer="94"/>
<wire x1="331.137259375" y1="44.1706" x2="331.365859375" y2="43.99915" width="0.025" layer="94"/>
<wire x1="331.365859375" y1="43.99915" x2="331.451590625" y2="43.91341875" width="0.025" layer="94"/>
<wire x1="331.92593125" y1="43.164759375" x2="331.937359375" y2="43.09618125" width="0.025" layer="94"/>
<wire x1="331.937359375" y1="43.09618125" x2="331.948790625" y2="42.993309375" width="0.025" layer="94"/>
<wire x1="331.891640625" y1="42.5704" x2="331.90306875" y2="42.604690625" width="0.025" layer="94"/>
<wire x1="331.84591875" y1="42.4561" x2="331.891640625" y2="42.5704" width="0.025" layer="94"/>
<wire x1="329.262740625" y1="40.947340625" x2="329.319890625" y2="40.95876875" width="0.025" layer="94"/>
<wire x1="329.194159375" y1="40.935909375" x2="329.262740625" y2="40.947340625" width="0.025" layer="94"/>
<wire x1="329.194159375" y1="40.935909375" x2="331.44586875" y2="32.512" width="0.025" layer="94"/>
<wire x1="323.764909375" y1="32.512" x2="324.9625" y2="37.03828125" width="0.025" layer="94"/>
<wire x1="324.9625" y1="37.03828125" x2="325.993759375" y2="40.935909375" width="0.025" layer="94"/>
<wire x1="325.719440625" y1="40.993059375" x2="325.78801875" y2="40.98163125" width="0.025" layer="94"/>
<wire x1="325.63943125" y1="41.01591875" x2="325.719440625" y2="40.993059375" width="0.025" layer="94"/>
<wire x1="325.45655" y1="41.061640625" x2="325.536559375" y2="41.03878125" width="0.025" layer="94"/>
<wire x1="325.3994" y1="41.07306875" x2="325.45655" y2="41.061640625" width="0.025" layer="94"/>
<wire x1="325.136509375" y1="41.15308125" x2="325.21651875" y2="41.13021875" width="0.025" layer="94"/>
<wire x1="325.06793125" y1="41.175940625" x2="325.136509375" y2="41.15308125" width="0.025" layer="94"/>
<wire x1="324.976490625" y1="41.21023125" x2="325.06793125" y2="41.175940625" width="0.025" layer="94"/>
<wire x1="324.97076875" y1="41.21251875" x2="324.976490625" y2="41.21023125" width="0.025" layer="94"/>
<wire x1="324.64501875" y1="41.347390625" x2="324.747890625" y2="41.30166875" width="0.025" layer="94"/>
<wire x1="324.565009375" y1="41.38168125" x2="324.64501875" y2="41.347390625" width="0.025" layer="94"/>
<wire x1="324.450709375" y1="41.43883125" x2="324.45185" y2="41.438259375" width="0.025" layer="94"/>
<wire x1="324.393559375" y1="41.47311875" x2="324.450709375" y2="41.43883125" width="0.025" layer="94"/>
<wire x1="324.26783125" y1="41.5417" x2="324.347840625" y2="41.49598125" width="0.025" layer="94"/>
<wire x1="324.233540625" y1="41.564559375" x2="324.26783125" y2="41.5417" width="0.025" layer="94"/>
<wire x1="324.18781875" y1="41.58741875" x2="324.233540625" y2="41.564559375" width="0.025" layer="94"/>
<wire x1="324.1421" y1="41.621709375" x2="324.18781875" y2="41.58741875" width="0.025" layer="94"/>
<wire x1="324.08495" y1="41.656" x2="324.1421" y2="41.621709375" width="0.025" layer="94"/>
<wire x1="323.822059375" y1="41.861740625" x2="323.85635" y2="41.82745" width="0.025" layer="94"/>
<wire x1="323.776340625" y1="41.89603125" x2="323.822059375" y2="41.861740625" width="0.025" layer="94"/>
<wire x1="323.62775" y1="42.05605" x2="323.662040625" y2="42.01033125" width="0.025" layer="94"/>
<wire x1="323.593459375" y1="42.090340625" x2="323.62775" y2="42.05605" width="0.025" layer="94"/>
<wire x1="323.35343125" y1="42.46753125" x2="323.354" y2="42.466390625" width="0.025" layer="94"/>
<wire x1="323.33056875" y1="42.52468125" x2="323.35343125" y2="42.46753125" width="0.025" layer="94"/>
<wire x1="323.536309375" y1="43.690540625" x2="323.58203125" y2="43.736259375" width="0.025" layer="94"/>
<wire x1="323.58203125" y1="43.736259375" x2="323.62775" y2="43.793409375" width="0.025" layer="94"/>
<wire x1="323.78776875" y1="43.95343125" x2="323.84491875" y2="43.99915" width="0.025" layer="94"/>
<wire x1="323.84491875" y1="43.99915" x2="323.87806875" y2="44.0323" width="0.025" layer="94"/>
<wire x1="323.87806875" y1="44.0323" x2="323.879209375" y2="44.033440625" width="0.025" layer="94"/>
<wire x1="323.879209375" y1="44.033440625" x2="323.936359375" y2="44.079159375" width="0.025" layer="94"/>
<wire x1="323.936359375" y1="44.079159375" x2="324.0278" y2="44.147740625" width="0.025" layer="94"/>
<wire x1="324.164959375" y1="44.23918125" x2="324.222109375" y2="44.27346875" width="0.025" layer="94"/>
<wire x1="324.222109375" y1="44.27346875" x2="324.290690625" y2="44.319190625" width="0.025" layer="94"/>
<wire x1="324.290690625" y1="44.319190625" x2="324.336409375" y2="44.34205" width="0.025" layer="94"/>
<wire x1="324.336409375" y1="44.34205" x2="324.393559375" y2="44.376340625" width="0.025" layer="94"/>
<wire x1="324.62101875" y1="44.49006875" x2="324.622159375" y2="44.490640625" width="0.025" layer="94"/>
<wire x1="324.622159375" y1="44.490640625" x2="324.679309375" y2="44.5135" width="0.025" layer="94"/>
<wire x1="324.679309375" y1="44.5135" x2="324.72503125" y2="44.536359375" width="0.025" layer="94"/>
<wire x1="324.72503125" y1="44.536359375" x2="325.01078125" y2="44.650659375" width="0.025" layer="94"/>
<wire x1="325.01078125" y1="44.650659375" x2="325.35368125" y2="44.764959375" width="0.025" layer="94"/>
<wire x1="325.35368125" y1="44.764959375" x2="325.433690625" y2="44.78781875" width="0.025" layer="94"/>
<wire x1="325.433690625" y1="44.78781875" x2="325.479409375" y2="44.79925" width="0.025" layer="94"/>
<wire x1="325.479409375" y1="44.79925" x2="325.5137" y2="44.81068125" width="0.025" layer="94"/>
<wire x1="325.5137" y1="44.81068125" x2="325.7423" y2="44.86783125" width="0.025" layer="94"/>
<wire x1="325.7423" y1="44.86783125" x2="325.79945" y2="44.879259375" width="0.025" layer="94"/>
<wire x1="325.79945" y1="44.879259375" x2="325.84516875" y2="44.890690625" width="0.025" layer="94"/>
<wire x1="326.393809375" y1="44.69638125" x2="326.450959375" y2="44.707809375" width="0.025" layer="94"/>
<wire x1="326.32523125" y1="44.68495" x2="326.393809375" y2="44.69638125" width="0.025" layer="94"/>
<wire x1="325.936609375" y1="44.604940625" x2="325.993759375" y2="44.61636875" width="0.025" layer="94"/>
<wire x1="325.84516875" y1="44.58208125" x2="325.936609375" y2="44.604940625" width="0.025" layer="94"/>
<wire x1="325.765159375" y1="44.55921875" x2="325.84516875" y2="44.58208125" width="0.025" layer="94"/>
<wire x1="325.67371875" y1="44.536359375" x2="325.765159375" y2="44.55921875" width="0.025" layer="94"/>
<wire x1="325.5137" y1="44.490640625" x2="325.520559375" y2="44.4926" width="0.025" layer="94"/>
<wire x1="325.23938125" y1="44.3992" x2="325.5137" y2="44.490640625" width="0.025" layer="94"/>
<wire x1="324.98791875" y1="44.29633125" x2="325.06793125" y2="44.33061875" width="0.025" layer="94"/>
<wire x1="324.93076875" y1="44.27346875" x2="324.98791875" y2="44.29633125" width="0.025" layer="94"/>
<wire x1="324.61073125" y1="44.10201875" x2="324.679309375" y2="44.147740625" width="0.025" layer="94"/>
<wire x1="324.53071875" y1="44.0563" x2="324.531859375" y2="44.05695" width="0.025" layer="94"/>
<wire x1="324.531859375" y1="44.05695" x2="324.61073125" y2="44.10201875" width="0.025" layer="94"/>
<wire x1="324.462140625" y1="44.01058125" x2="324.53071875" y2="44.0563" width="0.025" layer="94"/>
<wire x1="324.233540625" y1="43.8277" x2="324.279259375" y2="43.87341875" width="0.025" layer="94"/>
<wire x1="324.164959375" y1="43.77055" x2="324.233540625" y2="43.8277" width="0.025" layer="94"/>
<wire x1="323.73061875" y1="42.51325" x2="323.74205" y2="42.478959375" width="0.025" layer="94"/>
<wire x1="323.74205" y1="42.478959375" x2="323.75348125" y2="42.433240625" width="0.025" layer="94"/>
<wire x1="323.75348125" y1="42.433240625" x2="323.776340625" y2="42.38751875" width="0.025" layer="94"/>
<wire x1="323.776340625" y1="42.38751875" x2="323.7992" y2="42.33036875" width="0.025" layer="94"/>
<wire x1="324.08495" y1="41.95318125" x2="324.1421" y2="41.907459375" width="0.025" layer="94"/>
<wire x1="324.1421" y1="41.907459375" x2="324.176390625" y2="41.87316875" width="0.025" layer="94"/>
<wire x1="324.41641875" y1="41.690290625" x2="324.462140625" y2="41.656" width="0.025" layer="94"/>
<wire x1="324.462140625" y1="41.656" x2="324.53071875" y2="41.61028125" width="0.025" layer="94"/>
<wire x1="324.64501875" y1="41.5417" x2="324.690740625" y2="41.518840625" width="0.025" layer="94"/>
<wire x1="324.690740625" y1="41.518840625" x2="324.72503125" y2="41.49598125" width="0.025" layer="94"/>
<wire x1="324.72503125" y1="41.49598125" x2="324.78218125" y2="41.461690625" width="0.025" layer="94"/>
<wire x1="324.919340625" y1="41.393109375" x2="324.976490625" y2="41.37025" width="0.025" layer="94"/>
<wire x1="324.976490625" y1="41.37025" x2="325.022209375" y2="41.347390625" width="0.025" layer="94"/>
<wire x1="325.46798125" y1="41.18736875" x2="325.536559375" y2="41.164509375" width="0.025" layer="94"/>
<wire x1="325.536559375" y1="41.164509375" x2="325.58228125" y2="41.15308125" width="0.025" layer="94"/>
<wire x1="325.58228125" y1="41.15308125" x2="325.61656875" y2="41.14165" width="0.025" layer="94"/>
<wire x1="325.61656875" y1="41.14165" x2="325.708009375" y2="41.118790625" width="0.025" layer="94"/>
<wire x1="325.708009375" y1="41.118790625" x2="325.7423" y2="41.107359375" width="0.025" layer="94"/>
<wire x1="325.7423" y1="41.107359375" x2="326.01661875" y2="41.03878125" width="0.025" layer="94"/>
<wire x1="325.45655" y1="41.38168125" x2="325.5137" y2="41.35881875" width="0.025" layer="94"/>
<wire x1="325.433690625" y1="41.393109375" x2="325.45655" y2="41.38168125" width="0.025" layer="94"/>
<wire x1="325.15936875" y1="41.518840625" x2="325.319390625" y2="41.43883125" width="0.025" layer="94"/>
<wire x1="325.10221875" y1="41.55313125" x2="325.15936875" y2="41.518840625" width="0.025" layer="94"/>
<wire x1="324.70216875" y1="43.4848" x2="324.747890625" y2="43.519090625" width="0.025" layer="94"/>
<wire x1="324.747890625" y1="43.519090625" x2="324.77075" y2="43.54195" width="0.025" layer="94"/>
<wire x1="324.98678125" y1="43.68978125" x2="324.98791875" y2="43.690540625" width="0.025" layer="94"/>
<wire x1="324.98791875" y1="43.690540625" x2="325.10221875" y2="43.75911875" width="0.025" layer="94"/>
<wire x1="325.10221875" y1="43.75911875" x2="325.177659375" y2="43.796840625" width="0.025" layer="94"/>
<wire x1="325.45655" y1="43.93056875" x2="325.68515" y2="44.022009375" width="0.025" layer="94"/>
<wire x1="325.82116875" y1="44.06735" x2="325.822309375" y2="44.06773125" width="0.025" layer="94"/>
<wire x1="325.822309375" y1="44.06773125" x2="326.062340625" y2="44.136309375" width="0.025" layer="94"/>
<wire x1="326.062340625" y1="44.136309375" x2="326.15378125" y2="44.15916875" width="0.025" layer="94"/>
<wire x1="326.15378125" y1="44.15916875" x2="326.35951875" y2="44.204890625" width="0.025" layer="94"/>
<wire x1="326.279509375" y1="43.919140625" x2="326.462390625" y2="43.964859375" width="0.025" layer="94"/>
<wire x1="326.119490625" y1="43.87341875" x2="326.279509375" y2="43.919140625" width="0.025" layer="94"/>
<wire x1="326.02805" y1="43.850559375" x2="326.119490625" y2="43.87341875" width="0.025" layer="94"/>
<wire x1="325.68515" y1="43.72483125" x2="325.8566" y2="43.793409375" width="0.025" layer="94"/>
<wire x1="325.605140625" y1="43.690540625" x2="325.68515" y2="43.72483125" width="0.025" layer="94"/>
<wire x1="325.604" y1="43.68996875" x2="325.605140625" y2="43.690540625" width="0.025" layer="94"/>
<wire x1="325.34225" y1="43.54195" x2="325.45655" y2="43.61053125" width="0.025" layer="94"/>
<wire x1="325.341109375" y1="43.541190625" x2="325.34225" y2="43.54195" width="0.025" layer="94"/>
<wire x1="324.862190625" y1="43.15333125" x2="325.033640625" y2="43.32478125" width="0.025" layer="94"/>
<wire x1="324.8279" y1="43.107609375" x2="324.862190625" y2="43.15333125" width="0.025" layer="94"/>
<wire x1="324.75931875" y1="43.004740625" x2="324.8279" y2="43.107609375" width="0.025" layer="94"/>
<wire x1="324.633590625" y1="42.62755" x2="324.65645" y2="42.74185" width="0.025" layer="94"/>
<wire x1="324.633590625" y1="42.626409375" x2="324.633590625" y2="42.62755" width="0.025" layer="94"/>
<wire x1="324.690740625" y1="42.261790625" x2="324.70216875" y2="42.23893125" width="0.025" layer="94"/>
<wire x1="324.70216875" y1="42.23893125" x2="324.7136" y2="42.204640625" width="0.025" layer="94"/>
<wire x1="324.7136" y1="42.204640625" x2="324.72503125" y2="42.18178125" width="0.025" layer="94"/>
<wire x1="324.72503125" y1="42.18178125" x2="324.747890625" y2="42.147490625" width="0.025" layer="94"/>
<wire x1="324.793609375" y1="42.06748125" x2="324.87361875" y2="41.964609375" width="0.025" layer="94"/>
<wire x1="324.87361875" y1="41.964609375" x2="324.874759375" y2="41.96346875" width="0.025" layer="94"/>
<wire x1="325.84516875" y1="41.35881875" x2="325.969759375" y2="41.317290625" width="0.025" layer="94"/>
<wire x1="326.084059375" y1="41.279190625" x2="326.0852" y2="41.278809375" width="0.025" layer="94"/>
<wire x1="326.0852" y1="41.278809375" x2="326.119490625" y2="41.43883125" width="0.025" layer="94"/>
<wire x1="326.0852" y1="41.461690625" x2="326.119490625" y2="41.43883125" width="0.025" layer="94"/>
<wire x1="325.993759375" y1="41.49598125" x2="326.0852" y2="41.461690625" width="0.025" layer="94"/>
<wire x1="325.99261875" y1="41.49646875" x2="325.993759375" y2="41.49598125" width="0.025" layer="94"/>
<wire x1="325.650859375" y1="41.66743125" x2="325.652" y2="41.66678125" width="0.025" layer="94"/>
<wire x1="325.55941875" y1="41.72458125" x2="325.650859375" y2="41.66743125" width="0.025" layer="94"/>
<wire x1="325.44511875" y1="42.95901875" x2="325.536559375" y2="43.0276" width="0.025" layer="94"/>
<wire x1="325.536559375" y1="43.0276" x2="325.5377" y2="43.028359375" width="0.025" layer="94"/>
<wire x1="325.605140625" y1="43.07331875" x2="325.776590625" y2="43.176190625" width="0.025" layer="94"/>
<wire x1="326.49668125" y1="43.43908125" x2="326.64526875" y2="43.47336875" width="0.025" layer="94"/>
<wire x1="326.75956875" y1="43.30191875" x2="326.81671875" y2="43.31335" width="0.025" layer="94"/>
<wire x1="326.71385" y1="43.290490625" x2="326.75956875" y2="43.30191875" width="0.025" layer="94"/>
<wire x1="326.6567" y1="43.279059375" x2="326.71385" y2="43.290490625" width="0.025" layer="94"/>
<wire x1="326.565259375" y1="43.2562" x2="326.6567" y2="43.279059375" width="0.025" layer="94"/>
<wire x1="326.35951875" y1="43.18761875" x2="326.41666875" y2="43.21048125" width="0.025" layer="94"/>
<wire x1="326.32523125" y1="43.176190625" x2="326.35951875" y2="43.18761875" width="0.025" layer="94"/>
<wire x1="326.21093125" y1="43.13046875" x2="326.32523125" y2="43.176190625" width="0.025" layer="94"/>
<wire x1="326.005190625" y1="43.0276" x2="326.21093125" y2="43.13046875" width="0.025" layer="94"/>
<wire x1="325.547609375" y1="42.09148125" x2="325.547990625" y2="42.090340625" width="0.025" layer="94"/>
<wire x1="325.547990625" y1="42.090340625" x2="325.57085" y2="42.04461875" width="0.025" layer="94"/>
<wire x1="325.84516875" y1="41.747440625" x2="325.95946875" y2="41.678859375" width="0.025" layer="94"/>
<wire x1="325.95946875" y1="41.678859375" x2="326.03948125" y2="41.633140625" width="0.025" layer="94"/>
<wire x1="326.03948125" y1="41.633140625" x2="326.108059375" y2="41.59885" width="0.025" layer="94"/>
<wire x1="326.108059375" y1="41.59885" x2="326.165209375" y2="41.575990625" width="0.025" layer="94"/>
<wire x1="326.165209375" y1="41.575990625" x2="326.35951875" y2="42.307509375" width="0.025" layer="94"/>
<wire x1="329.022709375" y1="41.575990625" x2="329.04556875" y2="41.575990625" width="0.025" layer="94"/>
<wire x1="329.04556875" y1="41.575990625" x2="329.251309375" y2="41.678859375" width="0.025" layer="94"/>
<wire x1="329.251309375" y1="41.678859375" x2="329.25245" y2="41.67961875" width="0.025" layer="94"/>
<wire x1="329.42161875" y1="41.7924" x2="329.422759375" y2="41.793159375" width="0.025" layer="94"/>
<wire x1="329.422759375" y1="41.793159375" x2="329.548490625" y2="41.918890625" width="0.025" layer="94"/>
<wire x1="329.548490625" y1="41.918890625" x2="329.58278125" y2="41.964609375" width="0.025" layer="94"/>
<wire x1="329.58278125" y1="41.964609375" x2="329.6285" y2="42.033190625" width="0.025" layer="94"/>
<wire x1="329.662790625" y1="42.1132" x2="329.68565" y2="42.18178125" width="0.025" layer="94"/>
<wire x1="329.68565" y1="42.18178125" x2="329.69708125" y2="42.2275" width="0.025" layer="94"/>
<wire x1="329.57135" y1="42.718990625" x2="329.61706875" y2="42.650409375" width="0.025" layer="94"/>
<wire x1="329.537059375" y1="42.764709375" x2="329.57135" y2="42.718990625" width="0.025" layer="94"/>
<wire x1="329.434190625" y1="42.86758125" x2="329.43533125" y2="42.866440625" width="0.025" layer="94"/>
<wire x1="329.377040625" y1="42.9133" x2="329.434190625" y2="42.86758125" width="0.025" layer="94"/>
<wire x1="329.01128125" y1="43.119040625" x2="329.06843125" y2="43.09618125" width="0.025" layer="94"/>
<wire x1="328.965559375" y1="43.1419" x2="329.01128125" y2="43.119040625" width="0.025" layer="94"/>
<wire x1="328.32548125" y1="43.32478125" x2="328.43978125" y2="43.30191875" width="0.025" layer="94"/>
<wire x1="328.2569" y1="43.336209375" x2="328.32548125" y2="43.32478125" width="0.025" layer="94"/>
<wire x1="328.15403125" y1="43.347640625" x2="328.2569" y2="43.336209375" width="0.025" layer="94"/>
<wire x1="328.005440625" y1="43.35906875" x2="328.15403125" y2="43.347640625" width="0.025" layer="94"/>
<wire x1="328.005440625" y1="43.35906875" x2="328.005440625" y2="43.53051875" width="0.025" layer="94"/>
<wire x1="328.78268125" y1="43.41621875" x2="329.010140625" y2="43.34798125" width="0.025" layer="94"/>
<wire x1="329.010140625" y1="43.34798125" x2="329.01128125" y2="43.347640625" width="0.025" layer="94"/>
<wire x1="329.01128125" y1="43.347640625" x2="329.194159375" y2="43.279059375" width="0.025" layer="94"/>
<wire x1="329.194159375" y1="43.279059375" x2="329.308459375" y2="43.233340625" width="0.025" layer="94"/>
<wire x1="329.308459375" y1="43.233340625" x2="329.3096" y2="43.23276875" width="0.025" layer="94"/>
<wire x1="329.81138125" y1="42.9133" x2="329.891390625" y2="42.833290625" width="0.025" layer="94"/>
<wire x1="329.891390625" y1="42.833290625" x2="329.937109375" y2="42.776140625" width="0.025" layer="94"/>
<wire x1="329.95996875" y1="42.74185" x2="329.994259375" y2="42.6847" width="0.025" layer="94"/>
<wire x1="329.994259375" y1="42.6847" x2="330.03998125" y2="42.5704" width="0.025" layer="94"/>
<wire x1="330.03998125" y1="42.5704" x2="330.051409375" y2="42.51325" width="0.025" layer="94"/>
<wire x1="330.051409375" y1="42.51325" x2="330.062840625" y2="42.421809375" width="0.025" layer="94"/>
<wire x1="329.55991875" y1="41.66743125" x2="329.6285" y2="41.71315" width="0.025" layer="94"/>
<wire x1="329.45705" y1="41.61028125" x2="329.55991875" y2="41.66743125" width="0.025" layer="94"/>
<wire x1="329.15986875" y1="41.47311875" x2="329.1633" y2="41.474590625" width="0.025" layer="94"/>
<wire x1="329.06843125" y1="41.43883125" x2="329.15986875" y2="41.47311875" width="0.025" layer="94"/>
<wire x1="329.137009375" y1="41.290240625" x2="329.18273125" y2="41.30166875" width="0.025" layer="94"/>
<wire x1="329.18273125" y1="41.30166875" x2="329.21701875" y2="41.3131" width="0.025" layer="94"/>
<wire x1="329.21701875" y1="41.3131" x2="329.262740625" y2="41.32453125" width="0.025" layer="94"/>
<wire x1="329.262740625" y1="41.32453125" x2="329.3999" y2="41.37025" width="0.025" layer="94"/>
<wire x1="329.57135" y1="41.43883125" x2="329.605640625" y2="41.450259375" width="0.025" layer="94"/>
<wire x1="329.605640625" y1="41.450259375" x2="329.834240625" y2="41.564559375" width="0.025" layer="94"/>
<wire x1="329.834240625" y1="41.564559375" x2="329.86853125" y2="41.58741875" width="0.025" layer="94"/>
<wire x1="329.86853125" y1="41.58741875" x2="329.891390625" y2="41.59885" width="0.025" layer="94"/>
<wire x1="330.30286875" y1="41.93031875" x2="330.348590625" y2="41.98746875" width="0.025" layer="94"/>
<wire x1="330.348590625" y1="41.98746875" x2="330.37145" y2="42.01033125" width="0.025" layer="94"/>
<wire x1="330.405740625" y1="42.05605" x2="330.47431875" y2="42.17035" width="0.025" layer="94"/>
<wire x1="330.47431875" y1="42.17035" x2="330.520040625" y2="42.28465" width="0.025" layer="94"/>
<wire x1="330.54251875" y1="42.352090625" x2="330.5429" y2="42.35323125" width="0.025" layer="94"/>
<wire x1="330.5429" y1="42.35323125" x2="330.55433125" y2="42.41038125" width="0.025" layer="94"/>
<wire x1="330.55433125" y1="42.41038125" x2="330.565759375" y2="42.478959375" width="0.025" layer="94"/>
<wire x1="328.72553125" y1="44.22775" x2="328.95413125" y2="44.18203125" width="0.025" layer="94"/>
<wire x1="328.95413125" y1="44.18203125" x2="329.22845" y2="44.11345" width="0.025" layer="94"/>
<wire x1="329.69708125" y1="43.95343125" x2="329.7428" y2="43.93056875" width="0.025" layer="94"/>
<wire x1="329.7428" y1="43.93056875" x2="329.79995" y2="43.907709375" width="0.025" layer="94"/>
<wire x1="329.79995" y1="43.907709375" x2="329.84566875" y2="43.88485" width="0.025" layer="94"/>
<wire x1="329.84566875" y1="43.88485" x2="329.90281875" y2="43.861990625" width="0.025" layer="94"/>
<wire x1="329.90281875" y1="43.861990625" x2="330.03998125" y2="43.793409375" width="0.025" layer="94"/>
<wire x1="330.03998125" y1="43.793409375" x2="330.09713125" y2="43.75911875" width="0.025" layer="94"/>
<wire x1="330.09713125" y1="43.75911875" x2="330.14285" y2="43.736259375" width="0.025" layer="94"/>
<wire x1="330.55433125" y1="43.43908125" x2="330.60005" y2="43.393359375" width="0.025" layer="94"/>
<wire x1="330.60005" y1="43.393359375" x2="330.66863125" y2="43.31335" width="0.025" layer="94"/>
<wire x1="330.66863125" y1="43.31335" x2="330.70291875" y2="43.279059375" width="0.025" layer="94"/>
<wire x1="330.805790625" y1="42.193209375" x2="330.82865" y2="42.23893125" width="0.025" layer="94"/>
<wire x1="330.7715" y1="42.136059375" x2="330.805790625" y2="42.193209375" width="0.025" layer="94"/>
<wire x1="330.70291875" y1="42.04461875" x2="330.7715" y2="42.136059375" width="0.025" layer="94"/>
<wire x1="329.29703125" y1="41.233090625" x2="329.594209375" y2="41.32453125" width="0.025" layer="94"/>
<wire x1="329.1713" y1="41.03878125" x2="329.18273125" y2="41.03878125" width="0.025" layer="94"/>
<wire x1="329.18273125" y1="41.03878125" x2="329.26616875" y2="41.059640625" width="0.025" layer="94"/>
<wire x1="329.26616875" y1="41.059640625" x2="329.50276875" y2="41.118790625" width="0.025" layer="94"/>
<wire x1="329.50276875" y1="41.118790625" x2="329.7428" y2="41.18736875" width="0.025" layer="94"/>
<wire x1="329.7428" y1="41.18736875" x2="329.743940625" y2="41.18775" width="0.025" layer="94"/>
<wire x1="329.948540625" y1="41.25595" x2="330.005690625" y2="41.278809375" width="0.025" layer="94"/>
<wire x1="330.005690625" y1="41.278809375" x2="330.03998125" y2="41.290240625" width="0.025" layer="94"/>
<wire x1="330.60005" y1="41.564559375" x2="330.634340625" y2="41.58741875" width="0.025" layer="94"/>
<wire x1="330.634340625" y1="41.58741875" x2="330.691490625" y2="41.621709375" width="0.025" layer="94"/>
<wire x1="330.691490625" y1="41.621709375" x2="330.737209375" y2="41.656" width="0.025" layer="94"/>
<wire x1="331.2287" y1="42.06748125" x2="331.27441875" y2="42.12463125" width="0.025" layer="94"/>
<wire x1="331.27441875" y1="42.12463125" x2="331.308709375" y2="42.17035" width="0.025" layer="94"/>
<wire x1="331.33156875" y1="42.204640625" x2="331.343" y2="42.2275" width="0.025" layer="94"/>
<wire x1="331.343" y1="42.2275" x2="331.365859375" y2="42.261790625" width="0.025" layer="94"/>
<wire x1="331.50301875" y1="42.97045" x2="331.51445" y2="42.84471875" width="0.025" layer="94"/>
<wire x1="331.365859375" y1="43.3705" x2="331.36643125" y2="43.369359375" width="0.025" layer="94"/>
<wire x1="331.27441875" y1="43.507659375" x2="331.365859375" y2="43.3705" width="0.025" layer="94"/>
<wire x1="331.24013125" y1="43.55338125" x2="331.27441875" y2="43.507659375" width="0.025" layer="94"/>
<wire x1="330.61148125" y1="44.090590625" x2="330.66863125" y2="44.0563" width="0.025" layer="94"/>
<wire x1="330.610340625" y1="44.09135" x2="330.61148125" y2="44.090590625" width="0.025" layer="94"/>
<wire x1="329.95996875" y1="44.3992" x2="330.24571875" y2="44.2849" width="0.025" layer="94"/>
<wire x1="329.651359375" y1="44.50206875" x2="329.95996875" y2="44.3992" width="0.025" layer="94"/>
<wire x1="329.491340625" y1="44.547790625" x2="329.651359375" y2="44.50206875" width="0.025" layer="94"/>
<wire x1="329.21701875" y1="44.61636875" x2="329.491340625" y2="44.547790625" width="0.025" layer="94"/>
<wire x1="328.3712" y1="44.75353125" x2="328.47406875" y2="44.7421" width="0.025" layer="94"/>
<wire x1="328.2569" y1="44.764959375" x2="328.3712" y2="44.75353125" width="0.025" layer="94"/>
<wire x1="328.222609375" y1="44.764959375" x2="328.2569" y2="44.764959375" width="0.025" layer="94"/>
<wire x1="327.58253125" y1="40.78731875" x2="328.519790625" y2="36.21531875" width="0.025" layer="94"/>
<wire x1="326.690990625" y1="36.21531875" x2="327.58253125" y2="40.78731875" width="0.025" layer="94"/>
<wire x1="326.690990625" y1="36.21531875" x2="326.86038125" y2="37.084" width="0.025" layer="94"/>
<wire x1="324.9746" y1="37.084" x2="323.764909375" y2="32.512" width="0.025" layer="94"/>
<wire x1="323.764909375" y1="32.512" x2="325.8566" y2="32.512" width="0.025" layer="94"/>
<wire x1="325.8566" y1="32.512" x2="326.336659375" y2="34.5694" width="0.025" layer="94"/>
<wire x1="326.336659375" y1="34.5694" x2="328.862690625" y2="34.5694" width="0.025" layer="94"/>
<wire x1="328.862690625" y1="34.5694" x2="329.319890625" y2="32.512" width="0.025" layer="94"/>
<wire x1="329.319890625" y1="32.512" x2="331.44586875" y2="32.512" width="0.025" layer="94"/>
<wire x1="331.44586875" y1="32.512" x2="330.22376875" y2="37.084" width="0.025" layer="94"/>
<wire x1="328.341709375" y1="37.084" x2="328.35108125" y2="37.03828125" width="0.025" layer="94"/>
<wire x1="328.35108125" y1="37.03828125" x2="328.519790625" y2="36.21531875" width="0.025" layer="94"/>
<wire x1="328.519790625" y1="36.21531875" x2="326.690990625" y2="36.21531875" width="0.025" layer="94"/>
<wire x1="314.41516875" y1="42.29608125" x2="314.41516875" y2="32.512" width="0.025" layer="94"/>
<wire x1="314.41516875" y1="32.512" x2="316.26683125" y2="32.512" width="0.025" layer="94"/>
<wire x1="316.26683125" y1="32.512" x2="316.26683125" y2="40.21581875" width="0.025" layer="94"/>
<wire x1="316.26683125" y1="40.21581875" x2="317.98133125" y2="32.512" width="0.025" layer="94"/>
<wire x1="317.98133125" y1="32.512" x2="319.55866875" y2="32.512" width="0.025" layer="94"/>
<wire x1="319.55866875" y1="32.512" x2="321.250309375" y2="40.21581875" width="0.025" layer="94"/>
<wire x1="321.250309375" y1="40.21581875" x2="321.250309375" y2="32.512" width="0.025" layer="94"/>
<wire x1="321.250309375" y1="32.512" x2="323.12483125" y2="32.512" width="0.025" layer="94"/>
<wire x1="323.12483125" y1="32.512" x2="323.12483125" y2="42.29608125" width="0.025" layer="94"/>
<wire x1="323.12483125" y1="42.29608125" x2="320.15303125" y2="42.29608125" width="0.025" layer="94"/>
<wire x1="320.15303125" y1="42.29608125" x2="318.767990625" y2="35.575240625" width="0.025" layer="94"/>
<wire x1="318.767990625" y1="35.575240625" x2="317.364109375" y2="42.29608125" width="0.025" layer="94"/>
<wire x1="317.364109375" y1="42.29608125" x2="314.41516875" y2="42.29608125" width="0.025" layer="94"/>
<wire x1="312.83783125" y1="35.392359375" x2="312.83783125" y2="42.3418" width="0.025" layer="94"/>
<wire x1="312.83783125" y1="42.3418" x2="310.78043125" y2="42.3418" width="0.025" layer="94"/>
<wire x1="310.78043125" y1="42.3418" x2="310.78043125" y2="35.1409" width="0.025" layer="94"/>
<wire x1="310.78043125" y1="35.1409" x2="310.75756875" y2="35.0266" width="0.025" layer="94"/>
<wire x1="310.75756875" y1="35.0266" x2="310.71185" y2="34.889440625" width="0.025" layer="94"/>
<wire x1="310.71185" y1="34.889440625" x2="310.63346875" y2="34.75228125" width="0.025" layer="94"/>
<wire x1="310.63346875" y1="34.75228125" x2="310.620409375" y2="34.72941875" width="0.025" layer="94"/>
<wire x1="310.620409375" y1="34.72941875" x2="310.55183125" y2="34.660840625" width="0.025" layer="94"/>
<wire x1="310.55183125" y1="34.660840625" x2="310.506109375" y2="34.619690625" width="0.025" layer="94"/>
<wire x1="310.506109375" y1="34.619690625" x2="310.41466875" y2="34.546540625" width="0.025" layer="94"/>
<wire x1="310.41466875" y1="34.546540625" x2="310.30036875" y2="34.477959375" width="0.025" layer="94"/>
<wire x1="310.30036875" y1="34.477959375" x2="310.18606875" y2="34.432240625" width="0.025" layer="94"/>
<wire x1="310.18606875" y1="34.432240625" x2="310.02605" y2="34.38651875" width="0.025" layer="94"/>
<wire x1="310.02605" y1="34.38651875" x2="309.84316875" y2="34.363659375" width="0.025" layer="94"/>
<wire x1="309.84316875" y1="34.363659375" x2="309.660290625" y2="34.363659375" width="0.025" layer="94"/>
<wire x1="309.660290625" y1="34.363659375" x2="309.56885" y2="34.3789" width="0.025" layer="94"/>
<wire x1="309.56885" y1="34.3789" x2="309.52313125" y2="34.38651875" width="0.025" layer="94"/>
<wire x1="309.52313125" y1="34.38651875" x2="309.477409375" y2="34.39958125" width="0.025" layer="94"/>
<wire x1="309.477409375" y1="34.39958125" x2="309.431690625" y2="34.41265" width="0.025" layer="94"/>
<wire x1="309.431690625" y1="34.41265" x2="309.363109375" y2="34.432240625" width="0.025" layer="94"/>
<wire x1="309.363109375" y1="34.432240625" x2="309.22595" y2="34.50081875" width="0.025" layer="94"/>
<wire x1="309.22595" y1="34.50081875" x2="309.088790625" y2="34.592259375" width="0.025" layer="94"/>
<wire x1="309.088790625" y1="34.592259375" x2="308.95163125" y2="34.72941875" width="0.025" layer="94"/>
<wire x1="308.95163125" y1="34.72941875" x2="308.70733125" y2="35.05515" width="0.025" layer="94"/>
<wire x1="308.70733125" y1="35.05515" x2="307.145690625" y2="33.81501875" width="0.025" layer="94"/>
<wire x1="307.145690625" y1="33.81501875" x2="307.259990625" y2="33.58641875" width="0.025" layer="94"/>
<wire x1="307.259990625" y1="33.58641875" x2="307.305709375" y2="33.525459375" width="0.025" layer="94"/>
<wire x1="307.305709375" y1="33.525459375" x2="307.34571875" y2="33.47211875" width="0.025" layer="94"/>
<wire x1="307.34571875" y1="33.47211875" x2="307.39715" y2="33.403540625" width="0.025" layer="94"/>
<wire x1="307.39715" y1="33.403540625" x2="307.43935" y2="33.35781875" width="0.025" layer="94"/>
<wire x1="307.43935" y1="33.35781875" x2="307.488590625" y2="33.30448125" width="0.025" layer="94"/>
<wire x1="307.488590625" y1="33.30448125" x2="307.523759375" y2="33.26638125" width="0.025" layer="94"/>
<wire x1="307.523759375" y1="33.26638125" x2="307.55716875" y2="33.23018125" width="0.025" layer="94"/>
<wire x1="307.55716875" y1="33.23018125" x2="307.587059375" y2="33.1978" width="0.025" layer="94"/>
<wire x1="307.587059375" y1="33.1978" x2="307.62575" y2="33.155890625" width="0.025" layer="94"/>
<wire x1="307.62575" y1="33.155890625" x2="307.65036875" y2="33.12921875" width="0.025" layer="94"/>
<wire x1="307.65036875" y1="33.12921875" x2="307.67146875" y2="33.106359375" width="0.025" layer="94"/>
<wire x1="307.67146875" y1="33.106359375" x2="307.762909375" y2="33.02323125" width="0.025" layer="94"/>
<wire x1="307.762909375" y1="33.02323125" x2="307.7972" y2="32.992059375" width="0.025" layer="94"/>
<wire x1="307.7972" y1="32.992059375" x2="307.85435" y2="32.940109375" width="0.025" layer="94"/>
<wire x1="307.85435" y1="32.940109375" x2="307.89778125" y2="32.90061875" width="0.025" layer="94"/>
<wire x1="307.89778125" y1="32.90061875" x2="307.92293125" y2="32.877759375" width="0.025" layer="94"/>
<wire x1="307.92293125" y1="32.877759375" x2="308.12866875" y2="32.7406" width="0.025" layer="94"/>
<wire x1="308.12866875" y1="32.7406" x2="308.174390625" y2="32.71011875" width="0.025" layer="94"/>
<wire x1="308.174390625" y1="32.71011875" x2="308.24296875" y2="32.6705" width="0.025" layer="94"/>
<wire x1="308.24296875" y1="32.6705" x2="308.288690625" y2="32.646109375" width="0.025" layer="94"/>
<wire x1="308.288690625" y1="32.646109375" x2="308.3687" y2="32.603440625" width="0.025" layer="94"/>
<wire x1="308.3687" y1="32.603440625" x2="308.42585" y2="32.572959375" width="0.025" layer="94"/>
<wire x1="308.42585" y1="32.572959375" x2="308.54015" y2="32.512" width="0.025" layer="94"/>
<wire x1="308.54015" y1="32.512" x2="308.66206875" y2="32.46628125" width="0.025" layer="94"/>
<wire x1="308.66206875" y1="32.46628125" x2="308.72303125" y2="32.44341875" width="0.025" layer="94"/>
<wire x1="308.72303125" y1="32.44341875" x2="308.905909375" y2="32.374840625" width="0.025" layer="94"/>
<wire x1="308.905909375" y1="32.374840625" x2="308.99735" y2="32.353740625" width="0.025" layer="94"/>
<wire x1="308.99735" y1="32.353740625" x2="309.203090625" y2="32.306259375" width="0.025" layer="94"/>
<wire x1="309.203090625" y1="32.306259375" x2="309.27166875" y2="32.295709375" width="0.025" layer="94"/>
<wire x1="309.27166875" y1="32.295709375" x2="309.35168125" y2="32.2834" width="0.025" layer="94"/>
<wire x1="309.35168125" y1="32.2834" x2="309.40883125" y2="32.274609375" width="0.025" layer="94"/>
<wire x1="309.40883125" y1="32.274609375" x2="309.477409375" y2="32.264059375" width="0.025" layer="94"/>
<wire x1="309.477409375" y1="32.264059375" x2="309.50026875" y2="32.260540625" width="0.025" layer="94"/>
<wire x1="309.50026875" y1="32.260540625" x2="309.68315" y2="32.2453" width="0.025" layer="94"/>
<wire x1="309.68315" y1="32.2453" x2="309.774590625" y2="32.23768125" width="0.025" layer="94"/>
<wire x1="309.774590625" y1="32.23768125" x2="310.048909375" y2="32.260540625" width="0.025" layer="94"/>
<wire x1="310.048909375" y1="32.260540625" x2="310.231790625" y2="32.2834" width="0.025" layer="94"/>
<wire x1="310.231790625" y1="32.2834" x2="310.460390625" y2="32.32911875" width="0.025" layer="94"/>
<wire x1="310.460390625" y1="32.32911875" x2="310.620409375" y2="32.374840625" width="0.025" layer="94"/>
<wire x1="310.620409375" y1="32.374840625" x2="310.82615" y2="32.44341875" width="0.025" layer="94"/>
<wire x1="310.82615" y1="32.44341875" x2="311.05475" y2="32.534859375" width="0.025" layer="94"/>
<wire x1="311.05475" y1="32.534859375" x2="311.374790625" y2="32.69488125" width="0.025" layer="94"/>
<wire x1="311.374790625" y1="32.69488125" x2="311.58053125" y2="32.832040625" width="0.025" layer="94"/>
<wire x1="311.58053125" y1="32.832040625" x2="311.649109375" y2="32.889190625" width="0.025" layer="94"/>
<wire x1="311.649109375" y1="32.889190625" x2="311.69483125" y2="32.927290625" width="0.025" layer="94"/>
<wire x1="311.69483125" y1="32.927290625" x2="311.763409375" y2="32.984440625" width="0.025" layer="94"/>
<wire x1="311.763409375" y1="32.984440625" x2="311.85485" y2="33.060640625" width="0.025" layer="94"/>
<wire x1="311.85485" y1="33.060640625" x2="312.106309375" y2="33.3121" width="0.025" layer="94"/>
<wire x1="312.106309375" y1="33.3121" x2="312.15203125" y2="33.373059375" width="0.025" layer="94"/>
<wire x1="312.15203125" y1="33.373059375" x2="312.174890625" y2="33.403540625" width="0.025" layer="94"/>
<wire x1="312.174890625" y1="33.403540625" x2="312.220609375" y2="33.4645" width="0.025" layer="94"/>
<wire x1="312.220609375" y1="33.4645" x2="312.258709375" y2="33.517840625" width="0.025" layer="94"/>
<wire x1="312.258709375" y1="33.517840625" x2="312.30443125" y2="33.58641875" width="0.025" layer="94"/>
<wire x1="312.30443125" y1="33.58641875" x2="312.334909375" y2="33.632140625" width="0.025" layer="94"/>
<wire x1="312.334909375" y1="33.632140625" x2="312.39586875" y2="33.72358125" width="0.025" layer="94"/>
<wire x1="312.39586875" y1="33.72358125" x2="312.42635" y2="33.7693" width="0.025" layer="94"/>
<wire x1="312.42635" y1="33.7693" x2="312.60923125" y2="34.135059375" width="0.025" layer="94"/>
<wire x1="312.60923125" y1="34.135059375" x2="312.65495" y2="34.249359375" width="0.025" layer="94"/>
<wire x1="312.65495" y1="34.249359375" x2="312.68108125" y2="34.3408" width="0.025" layer="94"/>
<wire x1="312.68108125" y1="34.3408" x2="312.7121" y2="34.4551" width="0.025" layer="94"/>
<wire x1="312.7121" y1="34.4551" x2="312.746390625" y2="34.61511875" width="0.025" layer="94"/>
<wire x1="312.746390625" y1="34.61511875" x2="312.76925" y2="34.72941875" width="0.025" layer="94"/>
<wire x1="312.76925" y1="34.72941875" x2="312.792109375" y2="34.889440625" width="0.025" layer="94"/>
<wire x1="312.792109375" y1="34.889440625" x2="312.809890625" y2="35.049459375" width="0.025" layer="94"/>
<wire x1="312.809890625" y1="35.049459375" x2="312.81673125" y2="35.118040625" width="0.025" layer="94"/>
<wire x1="312.81673125" y1="35.118040625" x2="312.823759375" y2="35.20948125" width="0.025" layer="94"/>
<wire x1="312.823759375" y1="35.20948125" x2="312.829040625" y2="35.278059375" width="0.025" layer="94"/>
<wire x1="312.829040625" y1="35.278059375" x2="312.83783125" y2="35.392359375" width="0.025" layer="94"/>
<wire x1="324.9625" y1="37.03828125" x2="325.251490625" y2="38.13625" width="0.025" layer="94"/>
<wire x1="325.251490625" y1="38.13625" x2="325.993759375" y2="40.935909375" width="0.025" layer="94"/>
<wire x1="325.993759375" y1="40.935909375" x2="325.90231875" y2="40.95876875" width="0.025" layer="94"/>
<wire x1="325.90231875" y1="40.95876875" x2="325.78801875" y2="40.98163125" width="0.025" layer="94"/>
<wire x1="325.78801875" y1="40.98163125" x2="325.63943125" y2="41.01591875" width="0.025" layer="94"/>
<wire x1="325.63943125" y1="41.01591875" x2="325.536559375" y2="41.03878125" width="0.025" layer="94"/>
<wire x1="325.536559375" y1="41.03878125" x2="325.3994" y2="41.07306875" width="0.025" layer="94"/>
<wire x1="325.3994" y1="41.07306875" x2="325.319390625" y2="41.09593125" width="0.025" layer="94"/>
<wire x1="325.319390625" y1="41.09593125" x2="325.21651875" y2="41.13021875" width="0.025" layer="94"/>
<wire x1="325.21651875" y1="41.13021875" x2="325.06793125" y2="41.175940625" width="0.025" layer="94"/>
<wire x1="325.06793125" y1="41.175940625" x2="324.97076875" y2="41.21251875" width="0.025" layer="94"/>
<wire x1="324.97076875" y1="41.21251875" x2="324.747890625" y2="41.30166875" width="0.025" layer="94"/>
<wire x1="324.747890625" y1="41.30166875" x2="324.565009375" y2="41.38168125" width="0.025" layer="94"/>
<wire x1="324.565009375" y1="41.38168125" x2="324.45185" y2="41.438259375" width="0.025" layer="94"/>
<wire x1="324.45185" y1="41.438259375" x2="324.393559375" y2="41.47311875" width="0.025" layer="94"/>
<wire x1="324.393559375" y1="41.47311875" x2="324.347840625" y2="41.49598125" width="0.025" layer="94"/>
<wire x1="324.347840625" y1="41.49598125" x2="324.18781875" y2="41.58741875" width="0.025" layer="94"/>
<wire x1="324.18781875" y1="41.58741875" x2="324.08495" y2="41.656" width="0.025" layer="94"/>
<wire x1="324.08495" y1="41.656" x2="323.85635" y2="41.82745" width="0.025" layer="94"/>
<wire x1="323.85635" y1="41.82745" x2="323.776340625" y2="41.89603125" width="0.025" layer="94"/>
<wire x1="323.776340625" y1="41.89603125" x2="323.662040625" y2="42.01033125" width="0.025" layer="94"/>
<wire x1="323.662040625" y1="42.01033125" x2="323.593459375" y2="42.090340625" width="0.025" layer="94"/>
<wire x1="323.593459375" y1="42.090340625" x2="323.547740625" y2="42.147490625" width="0.025" layer="94"/>
<wire x1="323.547740625" y1="42.147490625" x2="323.52488125" y2="42.18178125" width="0.025" layer="94"/>
<wire x1="323.52488125" y1="42.18178125" x2="323.490590625" y2="42.2275" width="0.025" layer="94"/>
<wire x1="323.490590625" y1="42.2275" x2="323.46773125" y2="42.261790625" width="0.025" layer="94"/>
<wire x1="323.46773125" y1="42.261790625" x2="323.433440625" y2="42.318940625" width="0.025" layer="94"/>
<wire x1="323.433440625" y1="42.318940625" x2="323.41058125" y2="42.35323125" width="0.025" layer="94"/>
<wire x1="323.41058125" y1="42.35323125" x2="323.354" y2="42.466390625" width="0.025" layer="94"/>
<wire x1="323.354" y1="42.466390625" x2="323.33056875" y2="42.52468125" width="0.025" layer="94"/>
<wire x1="323.33056875" y1="42.52468125" x2="323.29628125" y2="42.62755" width="0.025" layer="94"/>
<wire x1="323.29628125" y1="42.62755" x2="323.28485" y2="42.67326875" width="0.025" layer="94"/>
<wire x1="323.28485" y1="42.67326875" x2="323.27341875" y2="42.73041875" width="0.025" layer="94"/>
<wire x1="323.27341875" y1="42.73041875" x2="323.261990625" y2="42.81043125" width="0.025" layer="94"/>
<wire x1="323.261990625" y1="42.81043125" x2="323.250559375" y2="42.936159375" width="0.025" layer="94"/>
<wire x1="323.250559375" y1="42.936159375" x2="323.261990625" y2="43.061890625" width="0.025" layer="94"/>
<wire x1="323.261990625" y1="43.061890625" x2="323.27341875" y2="43.13046875" width="0.025" layer="94"/>
<wire x1="323.27341875" y1="43.13046875" x2="323.28485" y2="43.18761875" width="0.025" layer="94"/>
<wire x1="323.28485" y1="43.18761875" x2="323.29628125" y2="43.233340625" width="0.025" layer="94"/>
<wire x1="323.29628125" y1="43.233340625" x2="323.33056875" y2="43.336209375" width="0.025" layer="94"/>
<wire x1="323.33056875" y1="43.336209375" x2="323.35343125" y2="43.393359375" width="0.025" layer="94"/>
<wire x1="323.35343125" y1="43.393359375" x2="323.41058125" y2="43.507659375" width="0.025" layer="94"/>
<wire x1="323.41058125" y1="43.507659375" x2="323.44486875" y2="43.564809375" width="0.025" layer="94"/>
<wire x1="323.44486875" y1="43.564809375" x2="323.479159375" y2="43.61053125" width="0.025" layer="94"/>
<wire x1="323.479159375" y1="43.61053125" x2="323.536309375" y2="43.690540625" width="0.025" layer="94"/>
<wire x1="323.536309375" y1="43.690540625" x2="323.62775" y2="43.793409375" width="0.025" layer="94"/>
<wire x1="323.62775" y1="43.793409375" x2="323.78776875" y2="43.95343125" width="0.025" layer="94"/>
<wire x1="323.78776875" y1="43.95343125" x2="323.87806875" y2="44.0323" width="0.025" layer="94"/>
<wire x1="323.87806875" y1="44.0323" x2="323.9375" y2="44.08001875" width="0.025" layer="94"/>
<wire x1="323.9375" y1="44.08001875" x2="324.0278" y2="44.147740625" width="0.025" layer="94"/>
<wire x1="324.0278" y1="44.147740625" x2="324.164959375" y2="44.23918125" width="0.025" layer="94"/>
<wire x1="324.164959375" y1="44.23918125" x2="324.290690625" y2="44.319190625" width="0.025" layer="94"/>
<wire x1="324.290690625" y1="44.319190625" x2="324.393559375" y2="44.376340625" width="0.025" layer="94"/>
<wire x1="324.393559375" y1="44.376340625" x2="324.62101875" y2="44.49006875" width="0.025" layer="94"/>
<wire x1="324.62101875" y1="44.49006875" x2="324.72503125" y2="44.536359375" width="0.025" layer="94"/>
<wire x1="324.72503125" y1="44.536359375" x2="325.009640625" y2="44.6502" width="0.025" layer="94"/>
<wire x1="325.009640625" y1="44.6502" x2="325.10908125" y2="44.68343125" width="0.025" layer="94"/>
<wire x1="325.10908125" y1="44.68343125" x2="325.287390625" y2="44.742859375" width="0.025" layer="94"/>
<wire x1="325.287390625" y1="44.742859375" x2="325.35368125" y2="44.764959375" width="0.025" layer="94"/>
<wire x1="325.35368125" y1="44.764959375" x2="325.43483125" y2="44.7881" width="0.025" layer="94"/>
<wire x1="325.43483125" y1="44.7881" x2="325.5137" y2="44.81068125" width="0.025" layer="94"/>
<wire x1="325.5137" y1="44.81068125" x2="325.741159375" y2="44.867540625" width="0.025" layer="94"/>
<wire x1="325.741159375" y1="44.867540625" x2="325.84516875" y2="44.890690625" width="0.025" layer="94"/>
<wire x1="325.84516875" y1="44.890690625" x2="326.07376875" y2="44.936409375" width="0.025" layer="94"/>
<wire x1="326.07376875" y1="44.936409375" x2="326.14235" y2="44.947840625" width="0.025" layer="94"/>
<wire x1="326.14235" y1="44.947840625" x2="326.222359375" y2="44.95926875" width="0.025" layer="94"/>
<wire x1="326.222359375" y1="44.95926875" x2="326.35951875" y2="44.98213125" width="0.025" layer="94"/>
<wire x1="326.35951875" y1="44.98213125" x2="326.43953125" y2="44.993559375" width="0.025" layer="94"/>
<wire x1="326.43953125" y1="44.993559375" x2="326.622409375" y2="45.01641875" width="0.025" layer="94"/>
<wire x1="326.622409375" y1="45.01641875" x2="326.72528125" y2="45.02785" width="0.025" layer="94"/>
<wire x1="326.72528125" y1="45.02785" x2="326.862440625" y2="45.03928125" width="0.025" layer="94"/>
<wire x1="326.862440625" y1="45.03928125" x2="326.862440625" y2="44.764959375" width="0.025" layer="94"/>
<wire x1="326.862440625" y1="44.764959375" x2="326.75956875" y2="44.75353125" width="0.025" layer="94"/>
<wire x1="326.75956875" y1="44.75353125" x2="326.519540625" y2="44.719240625" width="0.025" layer="94"/>
<wire x1="326.519540625" y1="44.719240625" x2="326.450959375" y2="44.707809375" width="0.025" layer="94"/>
<wire x1="326.450959375" y1="44.707809375" x2="326.32523125" y2="44.68495" width="0.025" layer="94"/>
<wire x1="326.32523125" y1="44.68495" x2="326.09663125" y2="44.63923125" width="0.025" layer="94"/>
<wire x1="326.09663125" y1="44.63923125" x2="325.993759375" y2="44.61636875" width="0.025" layer="94"/>
<wire x1="325.993759375" y1="44.61636875" x2="325.84516875" y2="44.58208125" width="0.025" layer="94"/>
<wire x1="325.84516875" y1="44.58208125" x2="325.67371875" y2="44.536359375" width="0.025" layer="94"/>
<wire x1="325.67371875" y1="44.536359375" x2="325.520559375" y2="44.4926" width="0.025" layer="94"/>
<wire x1="325.520559375" y1="44.4926" x2="325.4154" y2="44.45786875" width="0.025" layer="94"/>
<wire x1="325.4154" y1="44.45786875" x2="325.23938125" y2="44.3992" width="0.025" layer="94"/>
<wire x1="325.23938125" y1="44.3992" x2="325.06793125" y2="44.33061875" width="0.025" layer="94"/>
<wire x1="325.06793125" y1="44.33061875" x2="324.93076875" y2="44.27346875" width="0.025" layer="94"/>
<wire x1="324.93076875" y1="44.27346875" x2="324.679309375" y2="44.147740625" width="0.025" layer="94"/>
<wire x1="324.679309375" y1="44.147740625" x2="324.531859375" y2="44.05695" width="0.025" layer="94"/>
<wire x1="324.531859375" y1="44.05695" x2="324.462140625" y2="44.01058125" width="0.025" layer="94"/>
<wire x1="324.462140625" y1="44.01058125" x2="324.279259375" y2="43.87341875" width="0.025" layer="94"/>
<wire x1="324.279259375" y1="43.87341875" x2="324.164959375" y2="43.77055" width="0.025" layer="94"/>
<wire x1="324.164959375" y1="43.77055" x2="324.03923125" y2="43.64481875" width="0.025" layer="94"/>
<wire x1="324.03923125" y1="43.64481875" x2="323.97065" y2="43.564809375" width="0.025" layer="94"/>
<wire x1="323.97065" y1="43.564809375" x2="323.90206875" y2="43.47336875" width="0.025" layer="94"/>
<wire x1="323.90206875" y1="43.47336875" x2="323.86778125" y2="43.41621875" width="0.025" layer="94"/>
<wire x1="323.86778125" y1="43.41621875" x2="323.822059375" y2="43.336209375" width="0.025" layer="94"/>
<wire x1="323.822059375" y1="43.336209375" x2="323.78776875" y2="43.26763125" width="0.025" layer="94"/>
<wire x1="323.78776875" y1="43.26763125" x2="323.75348125" y2="43.176190625" width="0.025" layer="94"/>
<wire x1="323.75348125" y1="43.176190625" x2="323.73061875" y2="43.107609375" width="0.025" layer="94"/>
<wire x1="323.73061875" y1="43.107609375" x2="323.707759375" y2="43.004740625" width="0.025" layer="94"/>
<wire x1="323.707759375" y1="43.004740625" x2="323.69633125" y2="42.90186875" width="0.025" layer="94"/>
<wire x1="323.69633125" y1="42.90186875" x2="323.6849" y2="42.821859375" width="0.025" layer="94"/>
<wire x1="323.6849" y1="42.821859375" x2="323.6849" y2="42.776140625" width="0.025" layer="94"/>
<wire x1="323.6849" y1="42.776140625" x2="323.69633125" y2="42.650409375" width="0.025" layer="94"/>
<wire x1="323.69633125" y1="42.650409375" x2="323.73061875" y2="42.51325" width="0.025" layer="94"/>
<wire x1="323.73061875" y1="42.51325" x2="323.75348125" y2="42.433240625" width="0.025" layer="94"/>
<wire x1="323.75348125" y1="42.433240625" x2="323.7992" y2="42.33036875" width="0.025" layer="94"/>
<wire x1="323.7992" y1="42.33036875" x2="323.822059375" y2="42.28465" width="0.025" layer="94"/>
<wire x1="323.822059375" y1="42.28465" x2="323.890640625" y2="42.18178125" width="0.025" layer="94"/>
<wire x1="323.890640625" y1="42.18178125" x2="323.95921875" y2="42.090340625" width="0.025" layer="94"/>
<wire x1="323.95921875" y1="42.090340625" x2="324.004940625" y2="42.033190625" width="0.025" layer="94"/>
<wire x1="324.004940625" y1="42.033190625" x2="324.08495" y2="41.95318125" width="0.025" layer="94"/>
<wire x1="324.08495" y1="41.95318125" x2="324.176390625" y2="41.87316875" width="0.025" layer="94"/>
<wire x1="324.176390625" y1="41.87316875" x2="324.24496875" y2="41.81601875" width="0.025" layer="94"/>
<wire x1="324.24496875" y1="41.81601875" x2="324.30211875" y2="41.7703" width="0.025" layer="94"/>
<wire x1="324.30211875" y1="41.7703" x2="324.347840625" y2="41.736009375" width="0.025" layer="94"/>
<wire x1="324.347840625" y1="41.736009375" x2="324.41641875" y2="41.690290625" width="0.025" layer="94"/>
<wire x1="324.41641875" y1="41.690290625" x2="324.53071875" y2="41.61028125" width="0.025" layer="94"/>
<wire x1="324.53071875" y1="41.61028125" x2="324.64501875" y2="41.5417" width="0.025" layer="94"/>
<wire x1="324.64501875" y1="41.5417" x2="324.78218125" y2="41.461690625" width="0.025" layer="94"/>
<wire x1="324.78218125" y1="41.461690625" x2="324.919340625" y2="41.393109375" width="0.025" layer="94"/>
<wire x1="324.919340625" y1="41.393109375" x2="325.022209375" y2="41.347390625" width="0.025" layer="94"/>
<wire x1="325.022209375" y1="41.347390625" x2="325.079359375" y2="41.32453125" width="0.025" layer="94"/>
<wire x1="325.079359375" y1="41.32453125" x2="325.15936875" y2="41.290240625" width="0.025" layer="94"/>
<wire x1="325.15936875" y1="41.290240625" x2="325.250809375" y2="41.25595" width="0.025" layer="94"/>
<wire x1="325.250809375" y1="41.25595" x2="325.38796875" y2="41.21023125" width="0.025" layer="94"/>
<wire x1="325.38796875" y1="41.21023125" x2="325.46798125" y2="41.18736875" width="0.025" layer="94"/>
<wire x1="325.46798125" y1="41.18736875" x2="325.58228125" y2="41.15308125" width="0.025" layer="94"/>
<wire x1="325.58228125" y1="41.15308125" x2="325.708009375" y2="41.118790625" width="0.025" layer="94"/>
<wire x1="325.708009375" y1="41.118790625" x2="325.82116875" y2="41.087640625" width="0.025" layer="94"/>
<wire x1="325.82116875" y1="41.087640625" x2="326.01661875" y2="41.03878125" width="0.025" layer="94"/>
<wire x1="326.01661875" y1="41.03878125" x2="326.062340625" y2="41.18736875" width="0.025" layer="94"/>
<wire x1="326.062340625" y1="41.18736875" x2="326.02805" y2="41.1988" width="0.025" layer="94"/>
<wire x1="326.02805" y1="41.1988" x2="325.78801875" y2="41.26738125" width="0.025" layer="94"/>
<wire x1="325.78801875" y1="41.26738125" x2="325.5137" y2="41.35881875" width="0.025" layer="94"/>
<wire x1="325.5137" y1="41.35881875" x2="325.433690625" y2="41.393109375" width="0.025" layer="94"/>
<wire x1="325.433690625" y1="41.393109375" x2="325.319390625" y2="41.43883125" width="0.025" layer="94"/>
<wire x1="325.319390625" y1="41.43883125" x2="325.233659375" y2="41.481690625" width="0.025" layer="94"/>
<wire x1="325.233659375" y1="41.481690625" x2="325.10221875" y2="41.55313125" width="0.025" layer="94"/>
<wire x1="325.10221875" y1="41.55313125" x2="325.01078125" y2="41.614090625" width="0.025" layer="94"/>
<wire x1="325.01078125" y1="41.614090625" x2="324.93076875" y2="41.66743125" width="0.025" layer="94"/>
<wire x1="324.93076875" y1="41.66743125" x2="324.88505" y2="41.70171875" width="0.025" layer="94"/>
<wire x1="324.88505" y1="41.70171875" x2="324.8279" y2="41.747440625" width="0.025" layer="94"/>
<wire x1="324.8279" y1="41.747440625" x2="324.78218125" y2="41.78173125" width="0.025" layer="94"/>
<wire x1="324.78218125" y1="41.78173125" x2="324.66788125" y2="41.87316875" width="0.025" layer="94"/>
<wire x1="324.66788125" y1="41.87316875" x2="324.507859375" y2="42.033190625" width="0.025" layer="94"/>
<wire x1="324.507859375" y1="42.033190625" x2="324.47356875" y2="42.078909375" width="0.025" layer="94"/>
<wire x1="324.47356875" y1="42.078909375" x2="324.42785" y2="42.147490625" width="0.025" layer="94"/>
<wire x1="324.42785" y1="42.147490625" x2="324.393559375" y2="42.204640625" width="0.025" layer="94"/>
<wire x1="324.393559375" y1="42.204640625" x2="324.3707" y2="42.250359375" width="0.025" layer="94"/>
<wire x1="324.3707" y1="42.250359375" x2="324.336409375" y2="42.33036875" width="0.025" layer="94"/>
<wire x1="324.336409375" y1="42.33036875" x2="324.31355" y2="42.41038125" width="0.025" layer="94"/>
<wire x1="324.31355" y1="42.41038125" x2="324.30211875" y2="42.44466875" width="0.025" layer="94"/>
<wire x1="324.30211875" y1="42.44466875" x2="324.290690625" y2="42.50181875" width="0.025" layer="94"/>
<wire x1="324.290690625" y1="42.50181875" x2="324.279259375" y2="42.604690625" width="0.025" layer="94"/>
<wire x1="324.279259375" y1="42.604690625" x2="324.279259375" y2="42.764709375" width="0.025" layer="94"/>
<wire x1="324.279259375" y1="42.764709375" x2="324.290690625" y2="42.84471875" width="0.025" layer="94"/>
<wire x1="324.290690625" y1="42.84471875" x2="324.30211875" y2="42.90186875" width="0.025" layer="94"/>
<wire x1="324.30211875" y1="42.90186875" x2="324.347840625" y2="43.03903125" width="0.025" layer="94"/>
<wire x1="324.347840625" y1="43.03903125" x2="324.38213125" y2="43.107609375" width="0.025" layer="94"/>
<wire x1="324.38213125" y1="43.107609375" x2="324.41641875" y2="43.164759375" width="0.025" layer="94"/>
<wire x1="324.41641875" y1="43.164759375" x2="324.519290625" y2="43.30191875" width="0.025" layer="94"/>
<wire x1="324.519290625" y1="43.30191875" x2="324.70216875" y2="43.4848" width="0.025" layer="94"/>
<wire x1="324.70216875" y1="43.4848" x2="324.77075" y2="43.54195" width="0.025" layer="94"/>
<wire x1="324.77075" y1="43.54195" x2="324.81646875" y2="43.576240625" width="0.025" layer="94"/>
<wire x1="324.81646875" y1="43.576240625" x2="324.98678125" y2="43.68978125" width="0.025" layer="94"/>
<wire x1="324.98678125" y1="43.68978125" x2="325.10108125" y2="43.75843125" width="0.025" layer="94"/>
<wire x1="325.10108125" y1="43.75843125" x2="325.177659375" y2="43.796840625" width="0.025" layer="94"/>
<wire x1="325.177659375" y1="43.796840625" x2="325.376540625" y2="43.89628125" width="0.025" layer="94"/>
<wire x1="325.376540625" y1="43.89628125" x2="325.45655" y2="43.93056875" width="0.025" layer="94"/>
<wire x1="325.45655" y1="43.93056875" x2="325.53426875" y2="43.96143125" width="0.025" layer="94"/>
<wire x1="325.53426875" y1="43.96143125" x2="325.68515" y2="44.022009375" width="0.025" layer="94"/>
<wire x1="325.68515" y1="44.022009375" x2="325.82116875" y2="44.06735" width="0.025" layer="94"/>
<wire x1="325.82116875" y1="44.06735" x2="325.94346875" y2="44.10235" width="0.025" layer="94"/>
<wire x1="325.94346875" y1="44.10235" x2="326.06348125" y2="44.136590625" width="0.025" layer="94"/>
<wire x1="326.06348125" y1="44.136590625" x2="326.15378125" y2="44.15916875" width="0.025" layer="94"/>
<wire x1="326.15378125" y1="44.15916875" x2="326.230359375" y2="44.17631875" width="0.025" layer="94"/>
<wire x1="326.230359375" y1="44.17631875" x2="326.35951875" y2="44.204890625" width="0.025" layer="94"/>
<wire x1="326.35951875" y1="44.204890625" x2="326.47381875" y2="44.22775" width="0.025" layer="94"/>
<wire x1="326.47381875" y1="44.22775" x2="326.622409375" y2="44.250609375" width="0.025" layer="94"/>
<wire x1="326.622409375" y1="44.250609375" x2="326.78243125" y2="44.27346875" width="0.025" layer="94"/>
<wire x1="326.78243125" y1="44.27346875" x2="326.89673125" y2="44.2849" width="0.025" layer="94"/>
<wire x1="326.89673125" y1="44.2849" x2="327.091040625" y2="44.29633125" width="0.025" layer="94"/>
<wire x1="327.091040625" y1="44.29633125" x2="327.091040625" y2="44.033440625" width="0.025" layer="94"/>
<wire x1="327.091040625" y1="44.033440625" x2="326.919590625" y2="44.033440625" width="0.025" layer="94"/>
<wire x1="326.919590625" y1="44.033440625" x2="326.793859375" y2="44.022009375" width="0.025" layer="94"/>
<wire x1="326.793859375" y1="44.022009375" x2="326.71385" y2="44.01058125" width="0.025" layer="94"/>
<wire x1="326.71385" y1="44.01058125" x2="326.576690625" y2="43.98771875" width="0.025" layer="94"/>
<wire x1="326.576690625" y1="43.98771875" x2="326.462390625" y2="43.964859375" width="0.025" layer="94"/>
<wire x1="326.462390625" y1="43.964859375" x2="326.28065" y2="43.91941875" width="0.025" layer="94"/>
<wire x1="326.28065" y1="43.91941875" x2="326.12063125" y2="43.87375" width="0.025" layer="94"/>
<wire x1="326.12063125" y1="43.87375" x2="326.02805" y2="43.850559375" width="0.025" layer="94"/>
<wire x1="326.02805" y1="43.850559375" x2="325.8566" y2="43.793409375" width="0.025" layer="94"/>
<wire x1="325.8566" y1="43.793409375" x2="325.708009375" y2="43.73396875" width="0.025" layer="94"/>
<wire x1="325.708009375" y1="43.73396875" x2="325.604" y2="43.68996875" width="0.025" layer="94"/>
<wire x1="325.604" y1="43.68996875" x2="325.536559375" y2="43.65625" width="0.025" layer="94"/>
<wire x1="325.536559375" y1="43.65625" x2="325.45655" y2="43.61053125" width="0.025" layer="94"/>
<wire x1="325.45655" y1="43.61053125" x2="325.341109375" y2="43.541190625" width="0.025" layer="94"/>
<wire x1="325.341109375" y1="43.541190625" x2="325.205090625" y2="43.450509375" width="0.025" layer="94"/>
<wire x1="325.205090625" y1="43.450509375" x2="325.11365" y2="43.38193125" width="0.025" layer="94"/>
<wire x1="325.11365" y1="43.38193125" x2="325.033640625" y2="43.32478125" width="0.025" layer="94"/>
<wire x1="325.033640625" y1="43.32478125" x2="324.86133125" y2="43.152190625" width="0.025" layer="94"/>
<wire x1="324.86133125" y1="43.152190625" x2="324.828759375" y2="43.10875" width="0.025" layer="94"/>
<wire x1="324.828759375" y1="43.10875" x2="324.75931875" y2="43.004740625" width="0.025" layer="94"/>
<wire x1="324.75931875" y1="43.004740625" x2="324.70216875" y2="42.890440625" width="0.025" layer="94"/>
<wire x1="324.70216875" y1="42.890440625" x2="324.66788125" y2="42.78756875" width="0.025" layer="94"/>
<wire x1="324.66788125" y1="42.78756875" x2="324.65645" y2="42.74185" width="0.025" layer="94"/>
<wire x1="324.65645" y1="42.74185" x2="324.633590625" y2="42.626409375" width="0.025" layer="94"/>
<wire x1="324.633590625" y1="42.626409375" x2="324.633590625" y2="42.50181875" width="0.025" layer="94"/>
<wire x1="324.633590625" y1="42.50181875" x2="324.64501875" y2="42.421809375" width="0.025" layer="94"/>
<wire x1="324.64501875" y1="42.421809375" x2="324.66788125" y2="42.33036875" width="0.025" layer="94"/>
<wire x1="324.66788125" y1="42.33036875" x2="324.690740625" y2="42.261790625" width="0.025" layer="94"/>
<wire x1="324.690740625" y1="42.261790625" x2="324.7136" y2="42.204640625" width="0.025" layer="94"/>
<wire x1="324.7136" y1="42.204640625" x2="324.747890625" y2="42.147490625" width="0.025" layer="94"/>
<wire x1="324.747890625" y1="42.147490625" x2="324.793609375" y2="42.06748125" width="0.025" layer="94"/>
<wire x1="324.793609375" y1="42.06748125" x2="324.874759375" y2="41.96346875" width="0.025" layer="94"/>
<wire x1="324.874759375" y1="41.96346875" x2="324.976490625" y2="41.861740625" width="0.025" layer="94"/>
<wire x1="324.976490625" y1="41.861740625" x2="325.090790625" y2="41.75886875" width="0.025" layer="94"/>
<wire x1="325.090790625" y1="41.75886875" x2="325.193659375" y2="41.678859375" width="0.025" layer="94"/>
<wire x1="325.193659375" y1="41.678859375" x2="325.27366875" y2="41.621709375" width="0.025" layer="94"/>
<wire x1="325.27366875" y1="41.621709375" x2="325.433690625" y2="41.53026875" width="0.025" layer="94"/>
<wire x1="325.433690625" y1="41.53026875" x2="325.58228125" y2="41.461690625" width="0.025" layer="94"/>
<wire x1="325.58228125" y1="41.461690625" x2="325.68515" y2="41.41596875" width="0.025" layer="94"/>
<wire x1="325.68515" y1="41.41596875" x2="325.84516875" y2="41.35881875" width="0.025" layer="94"/>
<wire x1="325.84516875" y1="41.35881875" x2="325.91718125" y2="41.33481875" width="0.025" layer="94"/>
<wire x1="325.91718125" y1="41.33481875" x2="325.969759375" y2="41.317290625" width="0.025" layer="94"/>
<wire x1="325.969759375" y1="41.317290625" x2="326.084059375" y2="41.279190625" width="0.025" layer="94"/>
<wire x1="326.084059375" y1="41.279190625" x2="326.11925" y2="41.437690625" width="0.025" layer="94"/>
<wire x1="326.11925" y1="41.437690625" x2="325.99261875" y2="41.49646875" width="0.025" layer="94"/>
<wire x1="325.99261875" y1="41.49646875" x2="325.91375" y2="41.53026875" width="0.025" layer="94"/>
<wire x1="325.91375" y1="41.53026875" x2="325.81088125" y2="41.575990625" width="0.025" layer="94"/>
<wire x1="325.81088125" y1="41.575990625" x2="325.652" y2="41.66678125" width="0.025" layer="94"/>
<wire x1="325.652" y1="41.66678125" x2="325.55941875" y2="41.72458125" width="0.025" layer="94"/>
<wire x1="325.55941875" y1="41.72458125" x2="325.422259375" y2="41.83888125" width="0.025" layer="94"/>
<wire x1="325.422259375" y1="41.83888125" x2="325.365109375" y2="41.89603125" width="0.025" layer="94"/>
<wire x1="325.365109375" y1="41.89603125" x2="325.307959375" y2="41.964609375" width="0.025" layer="94"/>
<wire x1="325.307959375" y1="41.964609375" x2="325.21651875" y2="42.10176875" width="0.025" layer="94"/>
<wire x1="325.21651875" y1="42.10176875" x2="325.18223125" y2="42.18178125" width="0.025" layer="94"/>
<wire x1="325.18223125" y1="42.18178125" x2="325.15936875" y2="42.250359375" width="0.025" layer="94"/>
<wire x1="325.15936875" y1="42.250359375" x2="325.147940625" y2="42.318940625" width="0.025" layer="94"/>
<wire x1="325.147940625" y1="42.318940625" x2="325.147940625" y2="42.478959375" width="0.025" layer="94"/>
<wire x1="325.147940625" y1="42.478959375" x2="325.15936875" y2="42.547540625" width="0.025" layer="94"/>
<wire x1="325.15936875" y1="42.547540625" x2="325.1708" y2="42.593259375" width="0.025" layer="94"/>
<wire x1="325.1708" y1="42.593259375" x2="325.193659375" y2="42.650409375" width="0.025" layer="94"/>
<wire x1="325.193659375" y1="42.650409375" x2="325.22795" y2="42.718990625" width="0.025" layer="94"/>
<wire x1="325.22795" y1="42.718990625" x2="325.250809375" y2="42.75328125" width="0.025" layer="94"/>
<wire x1="325.250809375" y1="42.75328125" x2="325.2851" y2="42.799" width="0.025" layer="94"/>
<wire x1="325.2851" y1="42.799" x2="325.44511875" y2="42.95901875" width="0.025" layer="94"/>
<wire x1="325.44511875" y1="42.95901875" x2="325.5377" y2="43.028359375" width="0.025" layer="94"/>
<wire x1="325.5377" y1="43.028359375" x2="325.605140625" y2="43.07331875" width="0.025" layer="94"/>
<wire x1="325.605140625" y1="43.07331875" x2="325.68858125" y2="43.123609375" width="0.025" layer="94"/>
<wire x1="325.68858125" y1="43.123609375" x2="325.776590625" y2="43.176190625" width="0.025" layer="94"/>
<wire x1="325.776590625" y1="43.176190625" x2="325.84516875" y2="43.21048125" width="0.025" layer="94"/>
<wire x1="325.84516875" y1="43.21048125" x2="326.005190625" y2="43.279059375" width="0.025" layer="94"/>
<wire x1="326.005190625" y1="43.279059375" x2="326.09663125" y2="43.31335" width="0.025" layer="94"/>
<wire x1="326.09663125" y1="43.31335" x2="326.37095" y2="43.404790625" width="0.025" layer="94"/>
<wire x1="326.37095" y1="43.404790625" x2="326.49668125" y2="43.43908125" width="0.025" layer="94"/>
<wire x1="326.49668125" y1="43.43908125" x2="326.56411875" y2="43.454640625" width="0.025" layer="94"/>
<wire x1="326.56411875" y1="43.454640625" x2="326.64526875" y2="43.47336875" width="0.025" layer="94"/>
<wire x1="326.64526875" y1="43.47336875" x2="326.75956875" y2="43.49623125" width="0.025" layer="94"/>
<wire x1="326.75956875" y1="43.49623125" x2="326.93101875" y2="43.519090625" width="0.025" layer="94"/>
<wire x1="326.93101875" y1="43.519090625" x2="327.06818125" y2="43.53051875" width="0.025" layer="94"/>
<wire x1="327.06818125" y1="43.53051875" x2="327.205340625" y2="43.53051875" width="0.025" layer="94"/>
<wire x1="327.205340625" y1="43.53051875" x2="327.205340625" y2="43.35906875" width="0.025" layer="94"/>
<wire x1="327.205340625" y1="43.35906875" x2="327.17105" y2="43.35906875" width="0.025" layer="94"/>
<wire x1="327.17105" y1="43.35906875" x2="327.04531875" y2="43.347640625" width="0.025" layer="94"/>
<wire x1="327.04531875" y1="43.347640625" x2="326.95388125" y2="43.336209375" width="0.025" layer="94"/>
<wire x1="326.95388125" y1="43.336209375" x2="326.81671875" y2="43.31335" width="0.025" layer="94"/>
<wire x1="326.81671875" y1="43.31335" x2="326.71385" y2="43.290490625" width="0.025" layer="94"/>
<wire x1="326.71385" y1="43.290490625" x2="326.565259375" y2="43.2562" width="0.025" layer="94"/>
<wire x1="326.565259375" y1="43.2562" x2="326.48525" y2="43.233340625" width="0.025" layer="94"/>
<wire x1="326.48525" y1="43.233340625" x2="326.41666875" y2="43.21048125" width="0.025" layer="94"/>
<wire x1="326.41666875" y1="43.21048125" x2="326.32523125" y2="43.176190625" width="0.025" layer="94"/>
<wire x1="326.32523125" y1="43.176190625" x2="326.21206875" y2="43.13093125" width="0.025" layer="94"/>
<wire x1="326.21206875" y1="43.13093125" x2="326.005190625" y2="43.0276" width="0.025" layer="94"/>
<wire x1="326.005190625" y1="43.0276" x2="325.948040625" y2="42.993309375" width="0.025" layer="94"/>
<wire x1="325.948040625" y1="42.993309375" x2="325.84516875" y2="42.92473125" width="0.025" layer="94"/>
<wire x1="325.84516875" y1="42.92473125" x2="325.79945" y2="42.890440625" width="0.025" layer="94"/>
<wire x1="325.79945" y1="42.890440625" x2="325.7423" y2="42.84471875" width="0.025" layer="94"/>
<wire x1="325.7423" y1="42.84471875" x2="325.708009375" y2="42.81043125" width="0.025" layer="94"/>
<wire x1="325.708009375" y1="42.81043125" x2="325.63943125" y2="42.73041875" width="0.025" layer="94"/>
<wire x1="325.63943125" y1="42.73041875" x2="325.593709375" y2="42.661840625" width="0.025" layer="94"/>
<wire x1="325.593709375" y1="42.661840625" x2="325.55941875" y2="42.593259375" width="0.025" layer="94"/>
<wire x1="325.55941875" y1="42.593259375" x2="325.536559375" y2="42.536109375" width="0.025" layer="94"/>
<wire x1="325.536559375" y1="42.536109375" x2="325.5137" y2="42.4561" width="0.025" layer="94"/>
<wire x1="325.5137" y1="42.4561" x2="325.50226875" y2="42.39895" width="0.025" layer="94"/>
<wire x1="325.50226875" y1="42.39895" x2="325.50226875" y2="42.261790625" width="0.025" layer="94"/>
<wire x1="325.50226875" y1="42.261790625" x2="325.5137" y2="42.204640625" width="0.025" layer="94"/>
<wire x1="325.5137" y1="42.204640625" x2="325.52513125" y2="42.15891875" width="0.025" layer="94"/>
<wire x1="325.52513125" y1="42.15891875" x2="325.547609375" y2="42.09148125" width="0.025" layer="94"/>
<wire x1="325.547609375" y1="42.09148125" x2="325.57085" y2="42.04461875" width="0.025" layer="94"/>
<wire x1="325.57085" y1="42.04461875" x2="325.605140625" y2="41.98746875" width="0.025" layer="94"/>
<wire x1="325.605140625" y1="41.98746875" x2="325.67371875" y2="41.89603125" width="0.025" layer="94"/>
<wire x1="325.67371875" y1="41.89603125" x2="325.75373125" y2="41.81601875" width="0.025" layer="94"/>
<wire x1="325.75373125" y1="41.81601875" x2="325.84516875" y2="41.747440625" width="0.025" layer="94"/>
<wire x1="325.84516875" y1="41.747440625" x2="325.95833125" y2="41.67955" width="0.025" layer="94"/>
<wire x1="325.95833125" y1="41.67955" x2="326.04061875" y2="41.63256875" width="0.025" layer="94"/>
<wire x1="326.04061875" y1="41.63256875" x2="326.108059375" y2="41.59885" width="0.025" layer="94"/>
<wire x1="326.108059375" y1="41.59885" x2="326.165509375" y2="41.57713125" width="0.025" layer="94"/>
<wire x1="326.165509375" y1="41.57713125" x2="326.35951875" y2="42.307509375" width="0.025" layer="94"/>
<wire x1="326.35951875" y1="42.307509375" x2="328.8284" y2="42.307509375" width="0.025" layer="94"/>
<wire x1="328.8284" y1="42.307509375" x2="329.022709375" y2="41.575990625" width="0.025" layer="94"/>
<wire x1="329.022709375" y1="41.575990625" x2="329.087859375" y2="41.597140625" width="0.025" layer="94"/>
<wire x1="329.087859375" y1="41.597140625" x2="329.14158125" y2="41.624" width="0.025" layer="94"/>
<wire x1="329.14158125" y1="41.624" x2="329.25245" y2="41.67961875" width="0.025" layer="94"/>
<wire x1="329.25245" y1="41.67961875" x2="329.42161875" y2="41.7924" width="0.025" layer="94"/>
<wire x1="329.42161875" y1="41.7924" x2="329.548490625" y2="41.918890625" width="0.025" layer="94"/>
<wire x1="329.548490625" y1="41.918890625" x2="329.6285" y2="42.033190625" width="0.025" layer="94"/>
<wire x1="329.6285" y1="42.033190625" x2="329.662790625" y2="42.1132" width="0.025" layer="94"/>
<wire x1="329.662790625" y1="42.1132" x2="329.68593125" y2="42.18291875" width="0.025" layer="94"/>
<wire x1="329.68593125" y1="42.18291875" x2="329.69708125" y2="42.2275" width="0.025" layer="94"/>
<wire x1="329.69708125" y1="42.2275" x2="329.708509375" y2="42.307509375" width="0.025" layer="94"/>
<wire x1="329.708509375" y1="42.307509375" x2="329.708509375" y2="42.35323125" width="0.025" layer="94"/>
<wire x1="329.708509375" y1="42.35323125" x2="329.69708125" y2="42.433240625" width="0.025" layer="94"/>
<wire x1="329.69708125" y1="42.433240625" x2="329.67421875" y2="42.52468125" width="0.025" layer="94"/>
<wire x1="329.67421875" y1="42.52468125" x2="329.651359375" y2="42.58183125" width="0.025" layer="94"/>
<wire x1="329.651359375" y1="42.58183125" x2="329.61706875" y2="42.650409375" width="0.025" layer="94"/>
<wire x1="329.61706875" y1="42.650409375" x2="329.570490625" y2="42.72013125" width="0.025" layer="94"/>
<wire x1="329.570490625" y1="42.72013125" x2="329.537059375" y2="42.764709375" width="0.025" layer="94"/>
<wire x1="329.537059375" y1="42.764709375" x2="329.43533125" y2="42.866440625" width="0.025" layer="94"/>
<wire x1="329.43533125" y1="42.866440625" x2="329.377040625" y2="42.9133" width="0.025" layer="94"/>
<wire x1="329.377040625" y1="42.9133" x2="329.33131875" y2="42.947590625" width="0.025" layer="94"/>
<wire x1="329.33131875" y1="42.947590625" x2="329.21701875" y2="43.01616875" width="0.025" layer="94"/>
<wire x1="329.21701875" y1="43.01616875" x2="329.137009375" y2="43.061890625" width="0.025" layer="94"/>
<wire x1="329.137009375" y1="43.061890625" x2="329.06843125" y2="43.09618125" width="0.025" layer="94"/>
<wire x1="329.06843125" y1="43.09618125" x2="328.965559375" y2="43.1419" width="0.025" layer="94"/>
<wire x1="328.965559375" y1="43.1419" x2="328.908409375" y2="43.164759375" width="0.025" layer="94"/>
<wire x1="328.908409375" y1="43.164759375" x2="328.81696875" y2="43.19905" width="0.025" layer="94"/>
<wire x1="328.81696875" y1="43.19905" x2="328.7141" y2="43.233340625" width="0.025" layer="94"/>
<wire x1="328.7141" y1="43.233340625" x2="328.634090625" y2="43.2562" width="0.025" layer="94"/>
<wire x1="328.634090625" y1="43.2562" x2="328.54265" y2="43.279059375" width="0.025" layer="94"/>
<wire x1="328.54265" y1="43.279059375" x2="328.43978125" y2="43.30191875" width="0.025" layer="94"/>
<wire x1="328.43978125" y1="43.30191875" x2="328.32661875" y2="43.32478125" width="0.025" layer="94"/>
<wire x1="328.32661875" y1="43.32478125" x2="328.255759375" y2="43.336209375" width="0.025" layer="94"/>
<wire x1="328.255759375" y1="43.336209375" x2="328.15516875" y2="43.347640625" width="0.025" layer="94"/>
<wire x1="328.15516875" y1="43.347640625" x2="328.075159375" y2="43.353709375" width="0.025" layer="94"/>
<wire x1="328.075159375" y1="43.353709375" x2="328.00658125" y2="43.35906875" width="0.025" layer="94"/>
<wire x1="328.00658125" y1="43.35906875" x2="328.005440625" y2="43.53051875" width="0.025" layer="94"/>
<wire x1="328.005440625" y1="43.53051875" x2="328.176890625" y2="43.53051875" width="0.025" layer="94"/>
<wire x1="328.176890625" y1="43.53051875" x2="328.291190625" y2="43.519090625" width="0.025" layer="94"/>
<wire x1="328.291190625" y1="43.519090625" x2="328.3712" y2="43.507659375" width="0.025" layer="94"/>
<wire x1="328.3712" y1="43.507659375" x2="328.43978125" y2="43.49623125" width="0.025" layer="94"/>
<wire x1="328.43978125" y1="43.49623125" x2="328.61123125" y2="43.461940625" width="0.025" layer="94"/>
<wire x1="328.61123125" y1="43.461940625" x2="328.70266875" y2="43.43908125" width="0.025" layer="94"/>
<wire x1="328.70266875" y1="43.43908125" x2="328.78268125" y2="43.41621875" width="0.025" layer="94"/>
<wire x1="328.78268125" y1="43.41621875" x2="328.892409375" y2="43.38306875" width="0.025" layer="94"/>
<wire x1="328.892409375" y1="43.38306875" x2="329.010140625" y2="43.34798125" width="0.025" layer="94"/>
<wire x1="329.010140625" y1="43.34798125" x2="329.079859375" y2="43.32191875" width="0.025" layer="94"/>
<wire x1="329.079859375" y1="43.32191875" x2="329.194159375" y2="43.279059375" width="0.025" layer="94"/>
<wire x1="329.194159375" y1="43.279059375" x2="329.3096" y2="43.23276875" width="0.025" layer="94"/>
<wire x1="329.3096" y1="43.23276875" x2="329.44561875" y2="43.164759375" width="0.025" layer="94"/>
<wire x1="329.44561875" y1="43.164759375" x2="329.50276875" y2="43.13046875" width="0.025" layer="94"/>
<wire x1="329.50276875" y1="43.13046875" x2="329.548490625" y2="43.107609375" width="0.025" layer="94"/>
<wire x1="329.548490625" y1="43.107609375" x2="329.61706875" y2="43.061890625" width="0.025" layer="94"/>
<wire x1="329.61706875" y1="43.061890625" x2="329.75423125" y2="42.95901875" width="0.025" layer="94"/>
<wire x1="329.75423125" y1="42.95901875" x2="329.81138125" y2="42.9133" width="0.025" layer="94"/>
<wire x1="329.891390625" y1="42.833290625" x2="329.89253125" y2="42.83215" width="0.025" layer="94"/>
<wire x1="329.89253125" y1="42.83215" x2="329.937109375" y2="42.776140625" width="0.025" layer="94"/>
<wire x1="329.937109375" y1="42.776140625" x2="329.95996875" y2="42.74185" width="0.025" layer="94"/>
<wire x1="329.95996875" y1="42.74185" x2="329.9954" y2="42.682409375" width="0.025" layer="94"/>
<wire x1="329.9954" y1="42.682409375" x2="330.03998125" y2="42.5704" width="0.025" layer="94"/>
<wire x1="330.03998125" y1="42.5704" x2="330.051409375" y2="42.512109375" width="0.025" layer="94"/>
<wire x1="330.051409375" y1="42.512109375" x2="330.062840625" y2="42.421809375" width="0.025" layer="94"/>
<wire x1="330.062840625" y1="42.421809375" x2="330.062840625" y2="42.35323125" width="0.025" layer="94"/>
<wire x1="330.062840625" y1="42.35323125" x2="330.051409375" y2="42.261790625" width="0.025" layer="94"/>
<wire x1="330.051409375" y1="42.261790625" x2="330.03998125" y2="42.21606875" width="0.025" layer="94"/>
<wire x1="330.03998125" y1="42.21606875" x2="330.01711875" y2="42.15891875" width="0.025" layer="94"/>
<wire x1="330.01711875" y1="42.15891875" x2="329.98283125" y2="42.090340625" width="0.025" layer="94"/>
<wire x1="329.98283125" y1="42.090340625" x2="329.95996875" y2="42.05605" width="0.025" layer="94"/>
<wire x1="329.95996875" y1="42.05605" x2="329.90281875" y2="41.976040625" width="0.025" layer="94"/>
<wire x1="329.90281875" y1="41.976040625" x2="329.834240625" y2="41.89603125" width="0.025" layer="94"/>
<wire x1="329.834240625" y1="41.89603125" x2="329.7428" y2="41.804590625" width="0.025" layer="94"/>
<wire x1="329.7428" y1="41.804590625" x2="329.6285" y2="41.71315" width="0.025" layer="94"/>
<wire x1="329.6285" y1="41.71315" x2="329.55878125" y2="41.666790625" width="0.025" layer="94"/>
<wire x1="329.55878125" y1="41.666790625" x2="329.45705" y2="41.61028125" width="0.025" layer="94"/>
<wire x1="329.45705" y1="41.61028125" x2="329.319890625" y2="41.5417" width="0.025" layer="94"/>
<wire x1="329.319890625" y1="41.5417" x2="329.1633" y2="41.474590625" width="0.025" layer="94"/>
<wire x1="329.1633" y1="41.474590625" x2="329.06843125" y2="41.43883125" width="0.025" layer="94"/>
<wire x1="329.06843125" y1="41.43883125" x2="329.10271875" y2="41.278809375" width="0.025" layer="94"/>
<wire x1="329.10271875" y1="41.278809375" x2="329.137009375" y2="41.290240625" width="0.025" layer="94"/>
<wire x1="329.137009375" y1="41.290240625" x2="329.21701875" y2="41.3131" width="0.025" layer="94"/>
<wire x1="329.21701875" y1="41.3131" x2="329.3999" y2="41.37025" width="0.025" layer="94"/>
<wire x1="329.3999" y1="41.37025" x2="329.57135" y2="41.43883125" width="0.025" layer="94"/>
<wire x1="329.57135" y1="41.43883125" x2="329.834240625" y2="41.564559375" width="0.025" layer="94"/>
<wire x1="329.834240625" y1="41.564559375" x2="329.891390625" y2="41.59885" width="0.025" layer="94"/>
<wire x1="329.891390625" y1="41.59885" x2="329.994259375" y2="41.66743125" width="0.025" layer="94"/>
<wire x1="329.994259375" y1="41.66743125" x2="330.051409375" y2="41.71315" width="0.025" layer="94"/>
<wire x1="330.051409375" y1="41.71315" x2="330.14285" y2="41.78173125" width="0.025" layer="94"/>
<wire x1="330.14285" y1="41.78173125" x2="330.177140625" y2="41.81601875" width="0.025" layer="94"/>
<wire x1="330.177140625" y1="41.81601875" x2="330.25715" y2="41.8846" width="0.025" layer="94"/>
<wire x1="330.25715" y1="41.8846" x2="330.30286875" y2="41.93031875" width="0.025" layer="94"/>
<wire x1="330.30286875" y1="41.93031875" x2="330.37145" y2="42.01033125" width="0.025" layer="94"/>
<wire x1="330.37145" y1="42.01033125" x2="330.405740625" y2="42.05605" width="0.025" layer="94"/>
<wire x1="330.405740625" y1="42.05605" x2="330.47478125" y2="42.171490625" width="0.025" layer="94"/>
<wire x1="330.47478125" y1="42.171490625" x2="330.520040625" y2="42.28465" width="0.025" layer="94"/>
<wire x1="330.520040625" y1="42.28465" x2="330.54251875" y2="42.352090625" width="0.025" layer="94"/>
<wire x1="330.54251875" y1="42.352090625" x2="330.565759375" y2="42.478959375" width="0.025" layer="94"/>
<wire x1="330.565759375" y1="42.478959375" x2="330.565759375" y2="42.650409375" width="0.025" layer="94"/>
<wire x1="330.565759375" y1="42.650409375" x2="330.55433125" y2="42.718990625" width="0.025" layer="94"/>
<wire x1="330.55433125" y1="42.718990625" x2="330.5429" y2="42.776140625" width="0.025" layer="94"/>
<wire x1="330.5429" y1="42.776140625" x2="330.520040625" y2="42.84471875" width="0.025" layer="94"/>
<wire x1="330.520040625" y1="42.84471875" x2="330.49718125" y2="42.90186875" width="0.025" layer="94"/>
<wire x1="330.49718125" y1="42.90186875" x2="330.451459375" y2="42.993309375" width="0.025" layer="94"/>
<wire x1="330.451459375" y1="42.993309375" x2="330.41716875" y2="43.050459375" width="0.025" layer="94"/>
<wire x1="330.41716875" y1="43.050459375" x2="330.394309375" y2="43.08475" width="0.025" layer="94"/>
<wire x1="330.394309375" y1="43.08475" x2="330.36001875" y2="43.13046875" width="0.025" layer="94"/>
<wire x1="330.36001875" y1="43.13046875" x2="330.291440625" y2="43.21048125" width="0.025" layer="94"/>
<wire x1="330.291440625" y1="43.21048125" x2="330.234290625" y2="43.26763125" width="0.025" layer="94"/>
<wire x1="330.234290625" y1="43.26763125" x2="330.15428125" y2="43.336209375" width="0.025" layer="94"/>
<wire x1="330.15428125" y1="43.336209375" x2="330.01711875" y2="43.43908125" width="0.025" layer="94"/>
<wire x1="330.01711875" y1="43.43908125" x2="329.879959375" y2="43.53051875" width="0.025" layer="94"/>
<wire x1="329.879959375" y1="43.53051875" x2="329.765659375" y2="43.5991" width="0.025" layer="94"/>
<wire x1="329.765659375" y1="43.5991" x2="329.68565" y2="43.64481875" width="0.025" layer="94"/>
<wire x1="329.68565" y1="43.64481875" x2="329.52563125" y2="43.72483125" width="0.025" layer="94"/>
<wire x1="329.52563125" y1="43.72483125" x2="329.44561875" y2="43.75911875" width="0.025" layer="94"/>
<wire x1="329.44561875" y1="43.75911875" x2="329.1713" y2="43.850559375" width="0.025" layer="94"/>
<wire x1="329.1713" y1="43.850559375" x2="329.04556875" y2="43.88485" width="0.025" layer="94"/>
<wire x1="329.04556875" y1="43.88485" x2="328.965559375" y2="43.907709375" width="0.025" layer="94"/>
<wire x1="328.965559375" y1="43.907709375" x2="328.691240625" y2="43.976290625" width="0.025" layer="94"/>
<wire x1="328.691240625" y1="43.976290625" x2="328.634090625" y2="43.98771875" width="0.025" layer="94"/>
<wire x1="328.634090625" y1="43.98771875" x2="328.565509375" y2="43.99915" width="0.025" layer="94"/>
<wire x1="328.565509375" y1="43.99915" x2="328.394059375" y2="44.022009375" width="0.025" layer="94"/>
<wire x1="328.394059375" y1="44.022009375" x2="328.2569" y2="44.033440625" width="0.025" layer="94"/>
<wire x1="328.2569" y1="44.033440625" x2="328.119740625" y2="44.033440625" width="0.025" layer="94"/>
<wire x1="328.119740625" y1="44.033440625" x2="328.119740625" y2="44.29633125" width="0.025" layer="94"/>
<wire x1="328.119740625" y1="44.29633125" x2="328.165459375" y2="44.29633125" width="0.025" layer="94"/>
<wire x1="328.165459375" y1="44.29633125" x2="328.32548125" y2="44.2849" width="0.025" layer="94"/>
<wire x1="328.32548125" y1="44.2849" x2="328.42835" y2="44.27346875" width="0.025" layer="94"/>
<wire x1="328.42835" y1="44.27346875" x2="328.519790625" y2="44.262040625" width="0.025" layer="94"/>
<wire x1="328.519790625" y1="44.262040625" x2="328.5998" y2="44.250609375" width="0.025" layer="94"/>
<wire x1="328.5998" y1="44.250609375" x2="328.72553125" y2="44.22775" width="0.025" layer="94"/>
<wire x1="328.72553125" y1="44.22775" x2="328.952990625" y2="44.18203125" width="0.025" layer="94"/>
<wire x1="328.952990625" y1="44.18203125" x2="329.22845" y2="44.11345" width="0.025" layer="94"/>
<wire x1="329.22845" y1="44.11345" x2="329.38846875" y2="44.06773125" width="0.025" layer="94"/>
<wire x1="329.38846875" y1="44.06773125" x2="329.52563125" y2="44.022009375" width="0.025" layer="94"/>
<wire x1="329.52563125" y1="44.022009375" x2="329.69708125" y2="43.95343125" width="0.025" layer="94"/>
<wire x1="329.69708125" y1="43.95343125" x2="329.90281875" y2="43.861990625" width="0.025" layer="94"/>
<wire x1="329.90281875" y1="43.861990625" x2="330.04111875" y2="43.79271875" width="0.025" layer="94"/>
<wire x1="330.04111875" y1="43.79271875" x2="330.14285" y2="43.736259375" width="0.025" layer="94"/>
<wire x1="330.14285" y1="43.736259375" x2="330.2" y2="43.70196875" width="0.025" layer="94"/>
<wire x1="330.2" y1="43.70196875" x2="330.337159375" y2="43.61053125" width="0.025" layer="94"/>
<wire x1="330.337159375" y1="43.61053125" x2="330.38288125" y2="43.576240625" width="0.025" layer="94"/>
<wire x1="330.38288125" y1="43.576240625" x2="330.55433125" y2="43.43908125" width="0.025" layer="94"/>
<wire x1="330.55433125" y1="43.43908125" x2="330.601190625" y2="43.39221875" width="0.025" layer="94"/>
<wire x1="330.601190625" y1="43.39221875" x2="330.644140625" y2="43.34193125" width="0.025" layer="94"/>
<wire x1="330.644140625" y1="43.34193125" x2="330.70291875" y2="43.279059375" width="0.025" layer="94"/>
<wire x1="330.70291875" y1="43.279059375" x2="330.7715" y2="43.18761875" width="0.025" layer="94"/>
<wire x1="330.7715" y1="43.18761875" x2="330.81721875" y2="43.119040625" width="0.025" layer="94"/>
<wire x1="330.81721875" y1="43.119040625" x2="330.862940625" y2="43.0276" width="0.025" layer="94"/>
<wire x1="330.862940625" y1="43.0276" x2="330.8858" y2="42.95901875" width="0.025" layer="94"/>
<wire x1="330.8858" y1="42.95901875" x2="330.908659375" y2="42.879009375" width="0.025" layer="94"/>
<wire x1="330.908659375" y1="42.879009375" x2="330.920090625" y2="42.81043125" width="0.025" layer="94"/>
<wire x1="330.920090625" y1="42.81043125" x2="330.93151875" y2="42.69613125" width="0.025" layer="94"/>
<wire x1="330.93151875" y1="42.69613125" x2="330.93151875" y2="42.63898125" width="0.025" layer="94"/>
<wire x1="330.93151875" y1="42.63898125" x2="330.920090625" y2="42.536109375" width="0.025" layer="94"/>
<wire x1="330.920090625" y1="42.536109375" x2="330.908659375" y2="42.478959375" width="0.025" layer="94"/>
<wire x1="330.908659375" y1="42.478959375" x2="330.8858" y2="42.38751875" width="0.025" layer="94"/>
<wire x1="330.8858" y1="42.38751875" x2="330.87436875" y2="42.35323125" width="0.025" layer="94"/>
<wire x1="330.87436875" y1="42.35323125" x2="330.82865" y2="42.23893125" width="0.025" layer="94"/>
<wire x1="330.82865" y1="42.23893125" x2="330.8051" y2="42.19206875" width="0.025" layer="94"/>
<wire x1="330.8051" y1="42.19206875" x2="330.770640625" y2="42.13491875" width="0.025" layer="94"/>
<wire x1="330.770640625" y1="42.13491875" x2="330.70291875" y2="42.04461875" width="0.025" layer="94"/>
<wire x1="330.70291875" y1="42.04461875" x2="330.6572" y2="41.98746875" width="0.025" layer="94"/>
<wire x1="330.6572" y1="41.98746875" x2="330.577190625" y2="41.907459375" width="0.025" layer="94"/>
<wire x1="330.577190625" y1="41.907459375" x2="330.49718125" y2="41.83888125" width="0.025" layer="94"/>
<wire x1="330.49718125" y1="41.83888125" x2="330.44003125" y2="41.793159375" width="0.025" layer="94"/>
<wire x1="330.44003125" y1="41.793159375" x2="330.394309375" y2="41.75886875" width="0.025" layer="94"/>
<wire x1="330.394309375" y1="41.75886875" x2="330.337159375" y2="41.71315" width="0.025" layer="94"/>
<wire x1="330.337159375" y1="41.71315" x2="330.291440625" y2="41.678859375" width="0.025" layer="94"/>
<wire x1="330.291440625" y1="41.678859375" x2="330.18856875" y2="41.61028125" width="0.025" layer="94"/>
<wire x1="330.18856875" y1="41.61028125" x2="330.14285" y2="41.575990625" width="0.025" layer="94"/>
<wire x1="330.14285" y1="41.575990625" x2="330.0857" y2="41.5417" width="0.025" layer="94"/>
<wire x1="330.0857" y1="41.5417" x2="329.834240625" y2="41.41596875" width="0.025" layer="94"/>
<wire x1="329.834240625" y1="41.41596875" x2="329.719940625" y2="41.37025" width="0.025" layer="94"/>
<wire x1="329.719940625" y1="41.37025" x2="329.594209375" y2="41.32453125" width="0.025" layer="94"/>
<wire x1="329.594209375" y1="41.32453125" x2="329.44333125" y2="41.278109375" width="0.025" layer="94"/>
<wire x1="329.44333125" y1="41.278109375" x2="329.29703125" y2="41.233090625" width="0.025" layer="94"/>
<wire x1="329.29703125" y1="41.233090625" x2="329.137009375" y2="41.18736875" width="0.025" layer="94"/>
<wire x1="329.137009375" y1="41.18736875" x2="329.1713" y2="41.03878125" width="0.025" layer="94"/>
<wire x1="329.1713" y1="41.03878125" x2="329.26616875" y2="41.059640625" width="0.025" layer="94"/>
<wire x1="329.26616875" y1="41.059640625" x2="329.50163125" y2="41.1185" width="0.025" layer="94"/>
<wire x1="329.50163125" y1="41.1185" x2="329.6045" y2="41.147859375" width="0.025" layer="94"/>
<wire x1="329.6045" y1="41.147859375" x2="329.743940625" y2="41.18775" width="0.025" layer="94"/>
<wire x1="329.743940625" y1="41.18775" x2="329.948540625" y2="41.25595" width="0.025" layer="94"/>
<wire x1="329.948540625" y1="41.25595" x2="330.03998125" y2="41.290240625" width="0.025" layer="94"/>
<wire x1="330.03998125" y1="41.290240625" x2="330.21143125" y2="41.35881875" width="0.025" layer="94"/>
<wire x1="330.21143125" y1="41.35881875" x2="330.48575" y2="41.49598125" width="0.025" layer="94"/>
<wire x1="330.48575" y1="41.49598125" x2="330.60005" y2="41.564559375" width="0.025" layer="94"/>
<wire x1="330.60005" y1="41.564559375" x2="330.69035" y2="41.62101875" width="0.025" layer="94"/>
<wire x1="330.69035" y1="41.62101875" x2="330.737209375" y2="41.656" width="0.025" layer="94"/>
<wire x1="330.737209375" y1="41.656" x2="330.805790625" y2="41.70171875" width="0.025" layer="94"/>
<wire x1="330.805790625" y1="41.70171875" x2="330.851509375" y2="41.736009375" width="0.025" layer="94"/>
<wire x1="330.851509375" y1="41.736009375" x2="331.080109375" y2="41.918890625" width="0.025" layer="94"/>
<wire x1="331.080109375" y1="41.918890625" x2="331.2287" y2="42.06748125" width="0.025" layer="94"/>
<wire x1="331.2287" y1="42.06748125" x2="331.308709375" y2="42.17035" width="0.025" layer="94"/>
<wire x1="331.308709375" y1="42.17035" x2="331.33156875" y2="42.204640625" width="0.025" layer="94"/>
<wire x1="331.33156875" y1="42.204640625" x2="331.365859375" y2="42.261790625" width="0.025" layer="94"/>
<wire x1="331.365859375" y1="42.261790625" x2="331.434440625" y2="42.39895" width="0.025" layer="94"/>
<wire x1="331.434440625" y1="42.39895" x2="331.4573" y2="42.4561" width="0.025" layer="94"/>
<wire x1="331.4573" y1="42.4561" x2="331.46873125" y2="42.490390625" width="0.025" layer="94"/>
<wire x1="331.46873125" y1="42.490390625" x2="331.491590625" y2="42.58183125" width="0.025" layer="94"/>
<wire x1="331.491590625" y1="42.58183125" x2="331.50301875" y2="42.650409375" width="0.025" layer="94"/>
<wire x1="331.50301875" y1="42.650409375" x2="331.51445" y2="42.764709375" width="0.025" layer="94"/>
<wire x1="331.51445" y1="42.764709375" x2="331.51445" y2="42.84471875" width="0.025" layer="94"/>
<wire x1="331.51445" y1="42.84471875" x2="331.50988125" y2="42.89615" width="0.025" layer="94"/>
<wire x1="331.50988125" y1="42.89615" x2="331.50301875" y2="42.97045" width="0.025" layer="94"/>
<wire x1="331.50301875" y1="42.97045" x2="331.480159375" y2="43.08475" width="0.025" layer="94"/>
<wire x1="331.480159375" y1="43.08475" x2="331.434440625" y2="43.221909375" width="0.025" layer="94"/>
<wire x1="331.434440625" y1="43.221909375" x2="331.41158125" y2="43.279059375" width="0.025" layer="94"/>
<wire x1="331.41158125" y1="43.279059375" x2="331.36643125" y2="43.369359375" width="0.025" layer="94"/>
<wire x1="331.36643125" y1="43.369359375" x2="331.27518125" y2="43.50651875" width="0.025" layer="94"/>
<wire x1="331.27518125" y1="43.50651875" x2="331.24013125" y2="43.55338125" width="0.025" layer="94"/>
<wire x1="331.24013125" y1="43.55338125" x2="331.194409375" y2="43.61053125" width="0.025" layer="94"/>
<wire x1="331.194409375" y1="43.61053125" x2="330.98866875" y2="43.81626875" width="0.025" layer="94"/>
<wire x1="330.98866875" y1="43.81626875" x2="330.920090625" y2="43.87341875" width="0.025" layer="94"/>
<wire x1="330.920090625" y1="43.87341875" x2="330.862940625" y2="43.919140625" width="0.025" layer="94"/>
<wire x1="330.862940625" y1="43.919140625" x2="330.7715" y2="43.98771875" width="0.025" layer="94"/>
<wire x1="330.7715" y1="43.98771875" x2="330.66863125" y2="44.0563" width="0.025" layer="94"/>
<wire x1="330.66863125" y1="44.0563" x2="330.610340625" y2="44.09135" width="0.025" layer="94"/>
<wire x1="330.610340625" y1="44.09135" x2="330.577190625" y2="44.11345" width="0.025" layer="94"/>
<wire x1="330.577190625" y1="44.11345" x2="330.520040625" y2="44.147740625" width="0.025" layer="94"/>
<wire x1="330.520040625" y1="44.147740625" x2="330.24571875" y2="44.2849" width="0.025" layer="94"/>
<wire x1="330.24571875" y1="44.2849" x2="329.95768125" y2="44.399959375" width="0.025" layer="94"/>
<wire x1="329.95768125" y1="44.399959375" x2="329.651359375" y2="44.50206875" width="0.025" layer="94"/>
<wire x1="329.651359375" y1="44.50206875" x2="329.4902" y2="44.54806875" width="0.025" layer="94"/>
<wire x1="329.4902" y1="44.54806875" x2="329.21701875" y2="44.61636875" width="0.025" layer="94"/>
<wire x1="329.21701875" y1="44.61636875" x2="328.87411875" y2="44.68495" width="0.025" layer="94"/>
<wire x1="328.87411875" y1="44.68495" x2="328.736959375" y2="44.707809375" width="0.025" layer="94"/>
<wire x1="328.736959375" y1="44.707809375" x2="328.65695" y2="44.719240625" width="0.025" layer="94"/>
<wire x1="328.65695" y1="44.719240625" x2="328.47406875" y2="44.7421" width="0.025" layer="94"/>
<wire x1="328.47406875" y1="44.7421" x2="328.370059375" y2="44.75353125" width="0.025" layer="94"/>
<wire x1="328.370059375" y1="44.75353125" x2="328.222609375" y2="44.764959375" width="0.025" layer="94"/>
<wire x1="328.222609375" y1="44.764959375" x2="328.222609375" y2="45.03928125" width="0.025" layer="94"/>
<wire x1="328.222609375" y1="45.03928125" x2="328.279759375" y2="45.03928125" width="0.025" layer="94"/>
<wire x1="328.279759375" y1="45.03928125" x2="328.42835" y2="45.02785" width="0.025" layer="94"/>
<wire x1="328.42835" y1="45.02785" x2="328.55408125" y2="45.01641875" width="0.025" layer="94"/>
<wire x1="328.55408125" y1="45.01641875" x2="328.65695" y2="45.004990625" width="0.025" layer="94"/>
<wire x1="328.65695" y1="45.004990625" x2="328.736959375" y2="44.994990625" width="0.025" layer="94"/>
<wire x1="328.736959375" y1="44.994990625" x2="328.79983125" y2="44.98713125" width="0.025" layer="94"/>
<wire x1="328.79983125" y1="44.98713125" x2="328.93126875" y2="44.9707" width="0.025" layer="94"/>
<wire x1="328.93126875" y1="44.9707" x2="329.137009375" y2="44.936409375" width="0.025" layer="94"/>
<wire x1="329.137009375" y1="44.936409375" x2="329.251309375" y2="44.91355" width="0.025" layer="94"/>
<wire x1="329.251309375" y1="44.91355" x2="329.29703125" y2="44.90211875" width="0.025" layer="94"/>
<wire x1="329.29703125" y1="44.90211875" x2="329.3999" y2="44.879259375" width="0.025" layer="94"/>
<wire x1="329.3999" y1="44.879259375" x2="329.45705" y2="44.86783125" width="0.025" layer="94"/>
<wire x1="329.45705" y1="44.86783125" x2="329.63993125" y2="44.822109375" width="0.025" layer="94"/>
<wire x1="329.63993125" y1="44.822109375" x2="329.719940625" y2="44.79925" width="0.025" layer="94"/>
<wire x1="329.719940625" y1="44.79925" x2="329.81138125" y2="44.776390625" width="0.025" layer="94"/>
<wire x1="329.81138125" y1="44.776390625" x2="329.891390625" y2="44.75353125" width="0.025" layer="94"/>
<wire x1="329.891390625" y1="44.75353125" x2="330.165709375" y2="44.662090625" width="0.025" layer="94"/>
<wire x1="330.165709375" y1="44.662090625" x2="330.25715" y2="44.6278" width="0.025" layer="94"/>
<wire x1="330.25715" y1="44.6278" x2="330.3143" y2="44.604940625" width="0.025" layer="94"/>
<wire x1="330.3143" y1="44.604940625" x2="330.394309375" y2="44.57065" width="0.025" layer="94"/>
<wire x1="330.394309375" y1="44.57065" x2="330.508609375" y2="44.52493125" width="0.025" layer="94"/>
<wire x1="330.508609375" y1="44.52493125" x2="330.87436875" y2="44.34205" width="0.025" layer="94"/>
<wire x1="330.87436875" y1="44.34205" x2="331.04581875" y2="44.23918125" width="0.025" layer="94"/>
<wire x1="331.04581875" y1="44.23918125" x2="331.080109375" y2="44.21631875" width="0.025" layer="94"/>
<wire x1="331.080109375" y1="44.21631875" x2="331.137259375" y2="44.1706" width="0.025" layer="94"/>
<wire x1="331.137259375" y1="44.1706" x2="331.36471875" y2="44.000009375" width="0.025" layer="94"/>
<wire x1="331.36471875" y1="44.000009375" x2="331.451590625" y2="43.91341875" width="0.025" layer="94"/>
<wire x1="331.451590625" y1="43.91341875" x2="331.62875" y2="43.736259375" width="0.025" layer="94"/>
<wire x1="331.62875" y1="43.736259375" x2="331.67446875" y2="43.679109375" width="0.025" layer="94"/>
<wire x1="331.67446875" y1="43.679109375" x2="331.74305" y2="43.58766875" width="0.025" layer="94"/>
<wire x1="331.74305" y1="43.58766875" x2="331.777340625" y2="43.53051875" width="0.025" layer="94"/>
<wire x1="331.777340625" y1="43.53051875" x2="331.823059375" y2="43.450509375" width="0.025" layer="94"/>
<wire x1="331.823059375" y1="43.450509375" x2="331.85735" y2="43.38193125" width="0.025" layer="94"/>
<wire x1="331.85735" y1="43.38193125" x2="331.880209375" y2="43.32478125" width="0.025" layer="94"/>
<wire x1="331.880209375" y1="43.32478125" x2="331.90306875" y2="43.2562" width="0.025" layer="94"/>
<wire x1="331.90306875" y1="43.2562" x2="331.92593125" y2="43.164759375" width="0.025" layer="94"/>
<wire x1="331.92593125" y1="43.164759375" x2="331.937359375" y2="43.095040625" width="0.025" layer="94"/>
<wire x1="331.937359375" y1="43.095040625" x2="331.948790625" y2="42.993309375" width="0.025" layer="94"/>
<wire x1="331.948790625" y1="42.993309375" x2="331.948790625" y2="42.84471875" width="0.025" layer="94"/>
<wire x1="331.948790625" y1="42.84471875" x2="331.937359375" y2="42.75328125" width="0.025" layer="94"/>
<wire x1="331.937359375" y1="42.75328125" x2="331.92593125" y2="42.69613125" width="0.025" layer="94"/>
<wire x1="331.92593125" y1="42.69613125" x2="331.90306875" y2="42.604690625" width="0.025" layer="94"/>
<wire x1="331.90306875" y1="42.604690625" x2="331.886609375" y2="42.55783125" width="0.025" layer="94"/>
<wire x1="331.886609375" y1="42.55783125" x2="331.84591875" y2="42.4561" width="0.025" layer="94"/>
<wire x1="331.84591875" y1="42.4561" x2="331.777340625" y2="42.318940625" width="0.025" layer="94"/>
<wire x1="331.777340625" y1="42.318940625" x2="331.708759375" y2="42.21606875" width="0.025" layer="94"/>
<wire x1="331.708759375" y1="42.21606875" x2="331.67446875" y2="42.17035" width="0.025" layer="94"/>
<wire x1="331.67446875" y1="42.17035" x2="331.56016875" y2="42.033190625" width="0.025" layer="94"/>
<wire x1="331.56016875" y1="42.033190625" x2="331.40015" y2="41.87316875" width="0.025" layer="94"/>
<wire x1="331.40015" y1="41.87316875" x2="331.262990625" y2="41.75886875" width="0.025" layer="94"/>
<wire x1="331.262990625" y1="41.75886875" x2="331.17155" y2="41.690290625" width="0.025" layer="94"/>
<wire x1="331.17155" y1="41.690290625" x2="331.034390625" y2="41.59885" width="0.025" layer="94"/>
<wire x1="331.034390625" y1="41.59885" x2="330.977240625" y2="41.564559375" width="0.025" layer="94"/>
<wire x1="330.977240625" y1="41.564559375" x2="330.89723125" y2="41.518840625" width="0.025" layer="94"/>
<wire x1="330.89723125" y1="41.518840625" x2="330.84008125" y2="41.48455" width="0.025" layer="94"/>
<wire x1="330.84008125" y1="41.48455" x2="330.5429" y2="41.335959375" width="0.025" layer="94"/>
<wire x1="330.5429" y1="41.335959375" x2="330.462890625" y2="41.30166875" width="0.025" layer="94"/>
<wire x1="330.462890625" y1="41.30166875" x2="330.348590625" y2="41.25595" width="0.025" layer="94"/>
<wire x1="330.348590625" y1="41.25595" x2="330.25715" y2="41.221659375" width="0.025" layer="94"/>
<wire x1="330.25715" y1="41.221659375" x2="330.13141875" y2="41.175940625" width="0.025" layer="94"/>
<wire x1="330.13141875" y1="41.175940625" x2="329.994259375" y2="41.13021875" width="0.025" layer="94"/>
<wire x1="329.994259375" y1="41.13021875" x2="329.879959375" y2="41.09593125" width="0.025" layer="94"/>
<wire x1="329.879959375" y1="41.09593125" x2="329.75423125" y2="41.061640625" width="0.025" layer="94"/>
<wire x1="329.75423125" y1="41.061640625" x2="329.67421875" y2="41.03878125" width="0.025" layer="94"/>
<wire x1="329.67421875" y1="41.03878125" x2="329.52563125" y2="41.004490625" width="0.025" layer="94"/>
<wire x1="329.52563125" y1="41.004490625" x2="329.319890625" y2="40.95876875" width="0.025" layer="94"/>
<wire x1="329.319890625" y1="40.95876875" x2="329.194159375" y2="40.935909375" width="0.025" layer="94"/>
<wire x1="328.35108125" y1="37.03828125" x2="327.58253125" y2="40.78731875" width="0.025" layer="94"/>
<wire x1="327.58253125" y1="40.78731875" x2="326.86038125" y2="37.084" width="0.025" layer="94"/>
<wire x1="326.86038125" y1="37.084" x2="326.805290625" y2="37.01541875" width="0.025" layer="94"/>
<wire x1="271.78" y1="236.22" x2="271.78" y2="231.14" width="0.025" layer="94"/>
<wire x1="271.78" y1="231.14" x2="271.78" y2="226.06" width="0.025" layer="94"/>
<wire x1="271.78" y1="226.06" x2="279.4" y2="226.06" width="0.025" layer="94"/>
<wire x1="279.4" y1="226.06" x2="289.56" y2="226.06" width="0.025" layer="94"/>
<wire x1="289.56" y1="226.06" x2="299.72" y2="226.06" width="0.025" layer="94"/>
<wire x1="299.72" y1="226.06" x2="309.88" y2="226.06" width="0.025" layer="94"/>
<wire x1="309.88" y1="226.06" x2="375.92" y2="226.06" width="0.025" layer="94"/>
<wire x1="271.78" y1="231.14" x2="375.92" y2="231.14" width="0.025" layer="94"/>
<wire x1="279.4" y1="236.22" x2="279.4" y2="226.06" width="0.025" layer="94"/>
<wire x1="289.56" y1="236.22" x2="289.56" y2="226.06" width="0.025" layer="94"/>
<wire x1="299.72" y1="236.22" x2="299.72" y2="226.06" width="0.025" layer="94"/>
<wire x1="309.88" y1="236.22" x2="309.88" y2="226.06" width="0.025" layer="94"/>
<text x="272.796" y="35.306" size="1.778" layer="94" font="vector" ratio="12">DRAWN
BY</text>
<text x="272.542" y="28.702" size="1.778" layer="94" font="vector" ratio="12">CHECKED
BY</text>
<text x="272.542" y="22.352" size="1.778" layer="94" font="vector" ratio="12">DESIGN
APPROVAL</text>
<text x="287.02" y="42.164" size="1.778" layer="94" font="vector" ratio="12">INIT</text>
<text x="294.386" y="42.164" size="1.778" layer="94" font="vector" ratio="12">DATE</text>
<text x="333.502" y="33.528" size="1.6764" layer="94" font="vector" ratio="12">JOHN MEZZALINGUA ASSOCIATES
 7645 HENRY CLAY BOULEVARD
  LIVERPOOL, NEW YORK 13088
       TEL. 315-431-7100</text>
<text x="307.34" y="22.86" size="1.27" layer="94" font="vector" ratio="12">THIS DRAWING AND SPECIFICATIONS ARE THE PROPERTY OF
JOHN MEZZALINGUA ASSOCIATES AND SHALL NOT BE 
REPRODICED, COPIED, OR USED AS A BASIS FOR MANUFACTURING OR
SALE OF EQUIPMENT OR DEVICES WITHOUT WRITTEN PERMISSION.</text>
<text x="305.562" y="19.812" size="1.27" layer="94" font="vector" ratio="12">TITLE: </text>
<text x="305.562" y="13.462" size="1.27" layer="94" font="vector" ratio="12">DWG. NO.</text>
<text x="370.332" y="13.462" size="1.27" layer="94" font="vector" ratio="12">REV.</text>
<text x="360.172" y="7.112" size="1.27" layer="94" font="vector" ratio="12">SIZE:   B</text>
<text x="323.342" y="7.112" size="1.27" layer="94" font="vector" ratio="12">SHEET</text>
<text x="305.562" y="7.112" size="1.27" layer="94" font="vector" ratio="12">SCALE: NONE</text>
<text x="317.5" y="10.16" size="1.778" layer="94" font="vector">&gt;DRAWING_NAME</text>
<text x="332.486" y="5.842" size="1.27" layer="94" font="vector">&gt;SHEET</text>
<text x="273.304" y="233.68" size="1.778" layer="94" font="vector" ratio="12">REV</text>
<text x="281.178" y="233.68" size="1.778" layer="94" font="vector" ratio="12">ZONE</text>
<text x="291.592" y="233.68" size="1.778" layer="94" font="vector" ratio="12">DATE</text>
<text x="302.26" y="233.68" size="1.778" layer="94" font="vector" ratio="12">ECO</text>
<text x="333.502" y="233.426" size="1.778" layer="94" font="vector" ratio="12">DESCRIPTION</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="A3_LOC_JMA_RELEASE">
<gates>
<gate name="G$1" symbol="A3_LOC_JMA_RELEASE" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="JMA_LBR">
<packages>
<package name="CAPC1005X55N">
<wire x1="-0.48" y1="-0.93" x2="-0.48" y2="0.93" width="0.127" layer="21"/>
<wire x1="-0.48" y1="0.93" x2="0.48" y2="0.93" width="0.127" layer="21"/>
<wire x1="0.48" y1="0.93" x2="0.48" y2="-0.93" width="0.127" layer="21"/>
<wire x1="0.48" y1="-0.93" x2="-0.48" y2="-0.93" width="0.127" layer="21" style="shortdash"/>
<smd name="1" x="0" y="0.45" dx="0.62" dy="0.62" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-0.45" dx="0.62" dy="0.62" layer="1" roundness="25" rot="R270"/>
<text x="0.635" y="1.27" size="0.889" layer="25" ratio="11" rot="R270">&gt;NAME</text>
<text x="-1.524" y="1.397" size="0.635" layer="27" font="vector" ratio="10" rot="R270">&gt;VALUE</text>
<wire x1="-0.48" y1="-0.93" x2="-0.48" y2="0.93" width="0.127" layer="51"/>
<wire x1="-0.48" y1="0.93" x2="0.48" y2="0.93" width="0.127" layer="51"/>
<wire x1="0.48" y1="0.93" x2="0.48" y2="-0.93" width="0.127" layer="51"/>
<wire x1="0.48" y1="-0.93" x2="-0.48" y2="-0.93" width="0.127" layer="51" style="shortdash"/>
</package>
<package name="CAPC1608X55N">
<wire x1="0.675" y1="1.47" x2="0.675" y2="-1.47" width="0.127" layer="21"/>
<wire x1="0.675" y1="-1.47" x2="-0.675" y2="-1.47" width="0.127" layer="21"/>
<wire x1="-0.675" y1="-1.47" x2="-0.675" y2="1.47" width="0.127" layer="21"/>
<wire x1="-0.675" y1="1.47" x2="0.675" y2="1.47" width="0.127" layer="21"/>
<smd name="1" x="0" y="0.762" dx="0.9" dy="0.95" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-0.762" dx="0.9" dy="0.95" layer="1" roundness="25" rot="R270"/>
<text x="0.889" y="1.27" size="0.889" layer="25" ratio="11" rot="R270">&gt;NAME</text>
<text x="-1.5875" y="1.27" size="0.635" layer="27" ratio="10" rot="R270">&gt;VALUE</text>
<rectangle x1="-1.27" y1="-0.508" x2="1.27" y2="0.508" layer="39" rot="R270"/>
<wire x1="0.675" y1="1.47" x2="0.675" y2="-1.47" width="0.127" layer="51"/>
<wire x1="0.675" y1="-1.47" x2="-0.675" y2="-1.47" width="0.127" layer="51"/>
<wire x1="-0.675" y1="-1.47" x2="-0.675" y2="1.47" width="0.127" layer="51"/>
<wire x1="-0.675" y1="1.47" x2="0.675" y2="1.47" width="0.127" layer="51"/>
</package>
<package name="CAPC2013X145N">
<wire x1="0.925" y1="1.72" x2="0.925" y2="-1.72" width="0.127" layer="21"/>
<wire x1="0.925" y1="-1.72" x2="-0.925" y2="-1.72" width="0.127" layer="21"/>
<wire x1="-0.925" y1="-1.72" x2="-0.925" y2="1.72" width="0.127" layer="21"/>
<wire x1="-0.925" y1="1.72" x2="0.925" y2="1.72" width="0.127" layer="21"/>
<smd name="1" x="0" y="0.9" dx="1.15" dy="1.45" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-0.9" dx="1.15" dy="1.45" layer="1" roundness="25" rot="R270"/>
<text x="2.111" y="-2.02" size="0.889" layer="25" ratio="11" rot="R90">&gt;NAME</text>
<text x="-1.1625" y="-1.52" size="0.635" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.27" y1="-0.508" x2="1.27" y2="0.508" layer="39" rot="R270"/>
<wire x1="0.925" y1="1.72" x2="0.925" y2="-1.72" width="0.127" layer="51"/>
<wire x1="0.925" y1="-1.72" x2="-0.925" y2="-1.72" width="0.127" layer="51"/>
<wire x1="-0.925" y1="-1.72" x2="-0.925" y2="1.72" width="0.127" layer="51"/>
<wire x1="-0.925" y1="1.72" x2="0.925" y2="1.72" width="0.127" layer="51"/>
</package>
<package name="CAPC3216X190N">
<wire x1="1.27" y1="2.54" x2="1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="1.27" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-2.54" x2="-1.27" y2="2.54" width="0.127" layer="21"/>
<wire x1="-1.27" y1="2.54" x2="1.27" y2="2.54" width="0.127" layer="21"/>
<smd name="1" x="0" y="1.5" dx="1.15" dy="1.8" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-1.5" dx="1.15" dy="1.8" layer="1" roundness="25" rot="R270"/>
<text x="2.361" y="-2.02" size="0.889" layer="25" ratio="11" rot="R90">&gt;NAME</text>
<text x="-1.4125" y="-1.52" size="0.635" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.27" y1="-0.75" x2="1.27" y2="0.75" layer="39" rot="R270"/>
<wire x1="0.925" y1="1.72" x2="0.925" y2="-1.72" width="0.127" layer="51"/>
<wire x1="0.925" y1="-1.72" x2="-0.925" y2="-1.72" width="0.127" layer="51"/>
<wire x1="-0.925" y1="-1.72" x2="-0.925" y2="1.72" width="0.127" layer="51"/>
<wire x1="-0.925" y1="1.72" x2="0.925" y2="1.72" width="0.127" layer="51"/>
</package>
<package name="CAPC4532X279N">
<wire x1="2.175" y1="3.22" x2="2.175" y2="-3.22" width="0.127" layer="21"/>
<wire x1="2.175" y1="-3.22" x2="-2.175" y2="-3.22" width="0.127" layer="21"/>
<wire x1="-2.175" y1="-3.22" x2="-2.175" y2="3.22" width="0.127" layer="21"/>
<wire x1="-2.175" y1="3.22" x2="2.175" y2="3.22" width="0.127" layer="21"/>
<smd name="1" x="0" y="2.05" dx="1.4" dy="3.4" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-2.05" dx="1.4" dy="3.4" layer="1" roundness="25" rot="R270"/>
<text x="3.361" y="-2.02" size="0.889" layer="25" ratio="11" rot="R90">&gt;NAME</text>
<text x="-2.6625" y="-1.52" size="0.635" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-2.264" y1="-1.494" x2="2.256" y2="1.514" layer="39" rot="R270"/>
<wire x1="1.56" y1="2.331" x2="1.56" y2="-2.355" width="0.127" layer="51"/>
<wire x1="1.56" y1="-2.355" x2="-1.56" y2="-2.355" width="0.127" layer="51"/>
<wire x1="-1.56" y1="-2.355" x2="-1.56" y2="2.331" width="0.127" layer="51"/>
<wire x1="-1.56" y1="2.331" x2="1.56" y2="2.331" width="0.127" layer="51"/>
</package>
<package name="IND1608X50N">
<wire x1="0.675" y1="1.47" x2="0.675" y2="-1.47" width="0.127" layer="21"/>
<wire x1="0.675" y1="-1.47" x2="-0.675" y2="-1.47" width="0.127" layer="21"/>
<wire x1="-0.675" y1="-1.47" x2="-0.675" y2="1.47" width="0.127" layer="21"/>
<wire x1="-0.675" y1="1.47" x2="0.675" y2="1.47" width="0.127" layer="21"/>
<smd name="1" x="0" y="0.8" dx="0.95" dy="1" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-0.8" dx="0.95" dy="1" layer="1" roundness="25" rot="R270"/>
<text x="0.889" y="1.27" size="0.889" layer="25" ratio="11" rot="R270">&gt;NAME</text>
<text x="-1.5875" y="1.27" size="0.635" layer="27" ratio="10" rot="R270">&gt;VALUE</text>
<rectangle x1="-1.27" y1="-0.508" x2="1.27" y2="0.508" layer="39" rot="R270"/>
<wire x1="0.675" y1="1.47" x2="0.675" y2="-1.47" width="0.127" layer="51"/>
<wire x1="0.675" y1="-1.47" x2="-0.675" y2="-1.47" width="0.127" layer="51"/>
<wire x1="-0.675" y1="-1.47" x2="-0.675" y2="1.47" width="0.127" layer="51"/>
<wire x1="-0.675" y1="1.47" x2="0.675" y2="1.47" width="0.127" layer="51"/>
</package>
<package name="RESC1005X40N">
<wire x1="-0.48" y1="-0.94" x2="-0.48" y2="0.94" width="0.127" layer="21"/>
<wire x1="-0.48" y1="0.94" x2="0.48" y2="0.94" width="0.127" layer="21"/>
<wire x1="0.48" y1="0.94" x2="0.48" y2="-0.94" width="0.127" layer="21"/>
<wire x1="0.48" y1="-0.94" x2="-0.48" y2="-0.94" width="0.127" layer="21" style="shortdash"/>
<smd name="1" x="0" y="0.48" dx="0.57" dy="0.62" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-0.48" dx="0.57" dy="0.62" layer="1" roundness="25" rot="R270"/>
<text x="0.635" y="1.27" size="0.889" layer="25" ratio="11" rot="R270">&gt;NAME</text>
<text x="-1.524" y="1.397" size="0.635" layer="27" font="vector" ratio="10" rot="R270">&gt;VALUE</text>
<wire x1="-0.48" y1="-0.93" x2="-0.48" y2="0.93" width="0.127" layer="51"/>
<wire x1="-0.48" y1="0.93" x2="0.48" y2="0.93" width="0.127" layer="51"/>
<wire x1="0.48" y1="0.93" x2="0.48" y2="-0.93" width="0.127" layer="51"/>
<wire x1="0.48" y1="-0.93" x2="-0.48" y2="-0.93" width="0.127" layer="51" style="shortdash"/>
</package>
<package name="RESC1608X50N">
<wire x1="0.675" y1="1.47" x2="0.675" y2="-1.47" width="0.127" layer="21"/>
<wire x1="0.675" y1="-1.47" x2="-0.675" y2="-1.47" width="0.127" layer="21"/>
<wire x1="-0.675" y1="-1.47" x2="-0.675" y2="1.47" width="0.127" layer="21"/>
<wire x1="-0.675" y1="1.47" x2="0.675" y2="1.47" width="0.127" layer="21"/>
<smd name="1" x="0" y="0.8" dx="0.95" dy="1" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-0.8" dx="0.95" dy="1" layer="1" roundness="25" rot="R270"/>
<text x="0.889" y="1.27" size="0.889" layer="25" ratio="11" rot="R270">&gt;NAME</text>
<text x="-1.5875" y="1.27" size="0.635" layer="27" ratio="10" rot="R270">&gt;VALUE</text>
<rectangle x1="-1.27" y1="-0.508" x2="1.27" y2="0.508" layer="39" rot="R270"/>
<wire x1="0.675" y1="1.47" x2="0.675" y2="-1.47" width="0.127" layer="51"/>
<wire x1="0.675" y1="-1.47" x2="-0.675" y2="-1.47" width="0.127" layer="51"/>
<wire x1="-0.675" y1="-1.47" x2="-0.675" y2="1.47" width="0.127" layer="51"/>
<wire x1="-0.675" y1="1.47" x2="0.675" y2="1.47" width="0.127" layer="51"/>
</package>
<package name="RESC2013X70N">
<wire x1="1" y1="1.8" x2="1" y2="-1.8" width="0.127" layer="21"/>
<wire x1="1" y1="-1.8" x2="-1" y2="-1.8" width="0.127" layer="21"/>
<wire x1="-1" y1="-1.8" x2="-1" y2="1.8" width="0.127" layer="21"/>
<wire x1="-1" y1="1.8" x2="1" y2="1.8" width="0.127" layer="21"/>
<smd name="1" x="0" y="0.95" dx="1.05" dy="1.4" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-0.95" dx="1.05" dy="1.4" layer="1" roundness="25" rot="R270"/>
<text x="1.511" y="-1.47" size="0.4064" layer="25" ratio="11" rot="R90">&gt;NAME</text>
<text x="-1.2125" y="-1.47" size="0.4064" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.27" y1="-0.508" x2="1.27" y2="0.508" layer="39" rot="R270"/>
<wire x1="0.675" y1="1.47" x2="0.675" y2="-1.47" width="0.127" layer="51"/>
<wire x1="0.675" y1="-1.47" x2="-0.675" y2="-1.47" width="0.127" layer="51"/>
<wire x1="-0.675" y1="-1.47" x2="-0.675" y2="1.47" width="0.127" layer="51"/>
<wire x1="-0.675" y1="1.47" x2="0.675" y2="1.47" width="0.127" layer="51"/>
</package>
<package name="RESC3216X84N">
<wire x1="1.275" y1="2.27" x2="1.275" y2="-2.27" width="0.127" layer="21"/>
<wire x1="1.275" y1="-2.27" x2="-1.275" y2="-2.27" width="0.127" layer="21"/>
<wire x1="-1.275" y1="-2.27" x2="-1.275" y2="2.27" width="0.127" layer="21"/>
<wire x1="-1.275" y1="2.27" x2="1.275" y2="2.27" width="0.127" layer="21"/>
<smd name="1" x="0" y="1.5" dx="1.05" dy="1.75" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-1.5" dx="1.05" dy="1.75" layer="1" roundness="25" rot="R270"/>
<text x="1.95" y="-2" size="0.4064" layer="25" ratio="11" rot="R90">&gt;NAME</text>
<text x="-1.5" y="-2" size="0.4064" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.27" y1="-0.75" x2="1.27" y2="0.75" layer="39" rot="R270"/>
<wire x1="0.675" y1="1.47" x2="0.675" y2="-1.47" width="0.127" layer="51"/>
<wire x1="0.675" y1="-1.47" x2="-0.675" y2="-1.47" width="0.127" layer="51"/>
<wire x1="-0.675" y1="-1.47" x2="-0.675" y2="1.47" width="0.127" layer="51"/>
<wire x1="-0.675" y1="1.47" x2="0.675" y2="1.47" width="0.127" layer="51"/>
</package>
<package name="RES2013X38N">
<wire x1="0.925" y1="1.72" x2="0.925" y2="-1.72" width="0.127" layer="21"/>
<wire x1="0.925" y1="-1.72" x2="-0.925" y2="-1.72" width="0.127" layer="21"/>
<wire x1="-0.925" y1="-1.72" x2="-0.925" y2="1.72" width="0.127" layer="21"/>
<wire x1="-0.925" y1="1.72" x2="0.925" y2="1.72" width="0.127" layer="21"/>
<smd name="1" x="0" y="1" dx="0.95" dy="1.45" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-1" dx="0.95" dy="1.45" layer="1" roundness="25" rot="R270"/>
<text x="2" y="-2" size="0.889" layer="25" ratio="11" rot="R90">&gt;NAME</text>
<text x="-1" y="-2" size="0.635" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.27" y1="-0.508" x2="1.27" y2="0.508" layer="39" rot="R270"/>
<wire x1="0.925" y1="1.72" x2="0.925" y2="-1.72" width="0.127" layer="51"/>
<wire x1="0.925" y1="-1.72" x2="-0.925" y2="-1.72" width="0.127" layer="51"/>
<wire x1="-0.925" y1="-1.72" x2="-0.925" y2="1.72" width="0.127" layer="51"/>
<wire x1="-0.925" y1="1.72" x2="0.925" y2="1.72" width="0.127" layer="51"/>
</package>
<package name="RESC5325X84N">
<wire x1="1.775" y1="3.52" x2="1.775" y2="-3.52" width="0.127" layer="21"/>
<wire x1="1.775" y1="-3.52" x2="-1.775" y2="-3.52" width="0.127" layer="21"/>
<wire x1="-1.775" y1="-3.52" x2="-1.775" y2="3.52" width="0.127" layer="21"/>
<wire x1="-1.775" y1="3.52" x2="1.775" y2="3.52" width="0.127" layer="21"/>
<smd name="1" x="0" y="2.55" dx="1.1" dy="2.65" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-2.55" dx="1.1" dy="2.65" layer="1" roundness="25" rot="R270"/>
<text x="2.45" y="-2" size="0.4064" layer="25" ratio="11" rot="R90">&gt;NAME</text>
<text x="-2" y="-2" size="0.4064" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-3" y1="-1.27" x2="3" y2="1.27" layer="39" rot="R270"/>
</package>
<package name="CKSWITCHES_KXT3">
<smd name="1" x="-1.625" y="0" dx="1.5" dy="0.55" layer="1" rot="R90"/>
<smd name="2" x="1.625" y="0" dx="1.5" dy="0.55" layer="1" rot="R90"/>
<wire x1="-2.1" y1="1.1" x2="-2.1" y2="-1.1" width="0.127" layer="21"/>
<wire x1="-2.1" y1="-1.1" x2="2.1" y2="-1.1" width="0.127" layer="21"/>
<wire x1="2.1" y1="-1.1" x2="2.1" y2="1.1" width="0.127" layer="21"/>
<wire x1="2.1" y1="1.1" x2="-2.1" y2="1.1" width="0.127" layer="21"/>
<text x="-2" y="1.5" size="0.4064" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="-2" y="-2" size="0.4064" layer="27" font="vector" ratio="15">&gt;VALUE</text>
<wire x1="-2.1" y1="-1.1" x2="2.1" y2="-1.1" width="0.127" layer="51"/>
<wire x1="-2.1" y1="1.1" x2="-2.1" y2="-1.1" width="0.127" layer="51"/>
<wire x1="2.1" y1="1.1" x2="-2.1" y2="1.1" width="0.127" layer="51"/>
<wire x1="2.1" y1="-1.1" x2="2.1" y2="1.1" width="0.127" layer="51"/>
</package>
<package name="QFP50P1600X1600X120-100N">
<smd name="1" x="-7.7" y="6" dx="1.5" dy="0.3" layer="1"/>
<smd name="2" x="-7.7" y="5.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="3" x="-7.7" y="5" dx="1.5" dy="0.3" layer="1"/>
<smd name="4" x="-7.7" y="4.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="5" x="-7.7" y="4" dx="1.5" dy="0.3" layer="1"/>
<smd name="6" x="-7.7" y="3.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="7" x="-7.7" y="3" dx="1.5" dy="0.3" layer="1"/>
<smd name="8" x="-7.7" y="2.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="9" x="-7.7" y="2" dx="1.5" dy="0.3" layer="1"/>
<smd name="10" x="-7.7" y="1.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="11" x="-7.7" y="1" dx="1.5" dy="0.3" layer="1"/>
<smd name="12" x="-7.7" y="0.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="13" x="-7.7" y="0" dx="1.5" dy="0.3" layer="1"/>
<smd name="14" x="-7.7" y="-0.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="15" x="-7.7" y="-1" dx="1.5" dy="0.3" layer="1"/>
<smd name="16" x="-7.7" y="-1.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="17" x="-7.7" y="-2" dx="1.5" dy="0.3" layer="1"/>
<smd name="18" x="-7.7" y="-2.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="19" x="-7.7" y="-3" dx="1.5" dy="0.3" layer="1"/>
<smd name="20" x="-7.7" y="-3.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="21" x="-7.7" y="-4" dx="1.5" dy="0.3" layer="1"/>
<smd name="22" x="-7.7" y="-4.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="23" x="-7.7" y="-5" dx="1.5" dy="0.3" layer="1"/>
<smd name="24" x="-7.7" y="-5.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="25" x="-7.7" y="-6" dx="1.5" dy="0.3" layer="1"/>
<smd name="26" x="-6" y="-7.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="27" x="-5.5" y="-7.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="28" x="-5" y="-7.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="29" x="-4.5" y="-7.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="30" x="-4" y="-7.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="31" x="-3.5" y="-7.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="32" x="-3" y="-7.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="33" x="-2.5" y="-7.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="34" x="-2" y="-7.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="35" x="-1.5" y="-7.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="36" x="-1" y="-7.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="37" x="-0.5" y="-7.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="38" x="0" y="-7.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="39" x="0.5" y="-7.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="40" x="1" y="-7.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="41" x="1.5" y="-7.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="42" x="2" y="-7.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="43" x="2.5" y="-7.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="44" x="3" y="-7.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="45" x="3.5" y="-7.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="46" x="4" y="-7.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="47" x="4.5" y="-7.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="48" x="5" y="-7.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="49" x="5.5" y="-7.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="50" x="6" y="-7.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="51" x="7.7" y="-6" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="52" x="7.7" y="-5.5" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="53" x="7.7" y="-5" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="54" x="7.7" y="-4.5" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="55" x="7.7" y="-4" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="56" x="7.7" y="-3.5" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="57" x="7.7" y="-3" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="58" x="7.7" y="-2.5" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="59" x="7.7" y="-2" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="60" x="7.7" y="-1.5" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="61" x="7.7" y="-1" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="62" x="7.7" y="-0.5" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="63" x="7.7" y="0" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="64" x="7.7" y="0.5" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="65" x="7.7" y="1" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="66" x="7.7" y="1.5" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="67" x="7.7" y="2" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="68" x="7.7" y="2.5" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="69" x="7.7" y="3" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="70" x="7.7" y="3.5" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="71" x="7.7" y="4" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="72" x="7.7" y="4.5" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="73" x="7.7" y="5" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="74" x="7.7" y="5.5" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="75" x="7.7" y="6" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="76" x="6" y="7.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="77" x="5.5" y="7.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="78" x="5" y="7.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="79" x="4.5" y="7.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="80" x="4" y="7.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="81" x="3.5" y="7.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="82" x="3" y="7.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="83" x="2.5" y="7.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="84" x="2" y="7.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="85" x="1.5" y="7.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="86" x="1" y="7.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="87" x="0.5" y="7.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="88" x="0" y="7.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="89" x="-0.5" y="7.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="90" x="-1" y="7.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="91" x="-1.5" y="7.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="92" x="-2" y="7.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="93" x="-2.5" y="7.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="94" x="-3" y="7.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="95" x="-3.5" y="7.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="96" x="-4" y="7.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="97" x="-4.5" y="7.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="98" x="-5" y="7.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="99" x="-5.5" y="7.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="100" x="-6" y="7.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<wire x1="-6.5" y1="6.5" x2="6.5" y2="6.5" width="0.127" layer="21"/>
<wire x1="6.5" y1="6.5" x2="6.5" y2="-6.5" width="0.127" layer="21"/>
<wire x1="6.5" y1="-6.5" x2="-6.5" y2="-6.5" width="0.127" layer="21"/>
<wire x1="-6.5" y1="-6.5" x2="-6.5" y2="6.5" width="0.127" layer="21"/>
<wire x1="-9" y1="7" x2="-9" y2="-9" width="0.127" layer="21"/>
<wire x1="-9" y1="-9" x2="9" y2="-9" width="0.127" layer="21"/>
<wire x1="9" y1="-9" x2="9" y2="9" width="0.127" layer="21"/>
<wire x1="9" y1="9" x2="-7" y2="9" width="0.127" layer="21"/>
<wire x1="-7" y1="9" x2="-9" y2="7" width="0.127" layer="21"/>
<polygon width="0.127" layer="21">
<vertex x="-9" y="9"/>
<vertex x="-8.25" y="9"/>
<vertex x="-8.25" y="8.25"/>
<vertex x="-9" y="8.25"/>
</polygon>
<wire x1="-6.5" y1="6.5" x2="-6.5" y2="-6.5" width="0.127" layer="51"/>
<wire x1="-6.5" y1="-6.5" x2="6.5" y2="-6.5" width="0.127" layer="51"/>
<wire x1="6.5" y1="-6.5" x2="6.5" y2="6.5" width="0.127" layer="51"/>
<wire x1="6.5" y1="6.5" x2="-6.5" y2="6.5" width="0.127" layer="51"/>
<wire x1="-9" y1="7" x2="-9" y2="-9" width="0.127" layer="51"/>
<wire x1="-9" y1="-9" x2="9" y2="-9" width="0.127" layer="51"/>
<wire x1="9" y1="-9" x2="9" y2="9" width="0.127" layer="51"/>
<wire x1="9" y1="9" x2="-7" y2="9" width="0.127" layer="51"/>
<wire x1="-7" y1="9" x2="-9" y2="7" width="0.127" layer="51"/>
<wire x1="-9" y1="9" x2="-8.25" y2="9" width="0.127" layer="51"/>
<wire x1="-8.25" y1="9" x2="-8.25" y2="8.25" width="0.127" layer="51"/>
<wire x1="-8.25" y1="8.25" x2="-9" y2="8.25" width="0.127" layer="51"/>
<wire x1="-9" y1="8.25" x2="-9" y2="9" width="0.127" layer="51"/>
<text x="-7" y="9.5" size="0.6096" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="-7" y="10.5" size="0.6096" layer="27" font="vector" ratio="15">&gt;VALUE</text>
</package>
<package name="CYRSTAL_ECS_ECX-32">
<smd name="1" x="-1.3" y="-1.05" dx="1.3" dy="1.1" layer="1"/>
<smd name="3" x="1.3" y="1.05" dx="1.3" dy="1.1" layer="1"/>
<smd name="4" x="-1.3" y="1.05" dx="1.3" dy="1.1" layer="1"/>
<smd name="2" x="1.3" y="-1.05" dx="1.3" dy="1.1" layer="1"/>
<wire x1="-2.35" y1="-1.6" x2="-1.9" y2="-2.05" width="0.127" layer="21"/>
<wire x1="-1.9" y1="-2.05" x2="2.3" y2="-2.05" width="0.127" layer="21"/>
<wire x1="2.3" y1="-2.05" x2="2.3" y2="1.9" width="0.127" layer="21"/>
<wire x1="2.3" y1="1.9" x2="-2.35" y2="1.9" width="0.127" layer="21"/>
<wire x1="-2.35" y1="1.9" x2="-2.35" y2="-1.6" width="0.127" layer="21"/>
<wire x1="-2.35" y1="1.9" x2="2.3" y2="1.9" width="0.127" layer="51"/>
<wire x1="2.3" y1="1.9" x2="2.3" y2="-2.05" width="0.127" layer="51"/>
<wire x1="2.3" y1="-2.05" x2="-1.8984" y2="-2.05" width="0.127" layer="51"/>
<wire x1="-1.8984" y1="-2.05" x2="-2.35" y2="-1.5984" width="0.127" layer="51"/>
<wire x1="-2.35" y1="-1.5984" x2="-2.35" y2="1.9" width="0.127" layer="51"/>
<text x="2.95" y="1.7" size="0.3048" layer="25">&gt;NAME</text>
<text x="2.95" y="1.2" size="0.3048" layer="27">&gt;VALUE</text>
</package>
<package name="CRYSTAL_ECS_ECX-53B-DU">
<smd name="1" x="-1.85" y="-1.1" dx="1.3" dy="1.1" layer="1"/>
<smd name="3" x="1.85" y="1.1" dx="1.3" dy="1.1" layer="1"/>
<smd name="4" x="-1.85" y="1.1" dx="1.3" dy="1.1" layer="1"/>
<smd name="2" x="1.85" y="-1.1" dx="1.3" dy="1.1" layer="1"/>
<wire x1="-2.7" y1="-1.9" x2="2.7" y2="-1.9" width="0.127" layer="21"/>
<wire x1="2.7" y1="-1.9" x2="2.7" y2="1.9" width="0.127" layer="21"/>
<wire x1="2.7" y1="1.9" x2="-2.7" y2="1.9" width="0.127" layer="21"/>
<wire x1="-2.7" y1="1.9" x2="-2.7" y2="-1.9" width="0.127" layer="21"/>
<wire x1="-2.7" y1="1.9" x2="2.7" y2="1.9" width="0.127" layer="51"/>
<wire x1="2.7" y1="1.9" x2="2.7" y2="-1.9" width="0.127" layer="51"/>
<wire x1="2.7" y1="-1.9" x2="-2.7" y2="-1.9" width="0.127" layer="51"/>
<wire x1="-2.7" y1="-1.9" x2="-2.7" y2="1.9" width="0.127" layer="51"/>
<text x="2.95" y="1.7" size="0.3048" layer="25">&gt;NAME</text>
<text x="2.95" y="1.2" size="0.3048" layer="27">&gt;VALUE</text>
<wire x1="-3" y1="-1.25" x2="-3" y2="-2.25" width="0.1524" layer="21"/>
<wire x1="-3" y1="-2.25" x2="-2" y2="-2.25" width="0.1524" layer="21"/>
</package>
<package name="SOP65P490X110-8AN">
<smd name="1" x="-2.15" y="0.975" dx="1.35" dy="0.45" layer="1"/>
<smd name="2" x="-2.15" y="0.325" dx="1.35" dy="0.45" layer="1"/>
<smd name="3" x="-2.15" y="-0.325" dx="1.35" dy="0.45" layer="1"/>
<smd name="4" x="-2.15" y="-0.975" dx="1.35" dy="0.45" layer="1"/>
<smd name="5" x="2.15" y="-0.975" dx="1.35" dy="0.45" layer="1"/>
<smd name="6" x="2.15" y="-0.325" dx="1.35" dy="0.45" layer="1"/>
<smd name="7" x="2.15" y="0.325" dx="1.35" dy="0.45" layer="1"/>
<smd name="8" x="2.15" y="0.975" dx="1.35" dy="0.45" layer="1"/>
<wire x1="-2" y1="2" x2="-1.5" y2="2" width="0.1524" layer="21"/>
<wire x1="-1.5" y1="2" x2="2" y2="2" width="0.1524" layer="21"/>
<wire x1="-2" y1="-2" x2="2" y2="-2" width="0.1524" layer="21"/>
<wire x1="-2" y1="2" x2="-2" y2="2.5" width="0.1524" layer="21"/>
<wire x1="-2" y1="2.5" x2="-1.5" y2="2.5" width="0.1524" layer="21"/>
<wire x1="-1.5" y1="2.5" x2="-1.5" y2="2" width="0.1524" layer="21"/>
</package>
<package name="TP_1.5MM">
<pad name="1" x="0" y="0" drill="1" diameter="1.5" rot="R180"/>
<text x="-1.905" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<rectangle x1="-0.35" y1="-0.35" x2="0.35" y2="0.35" layer="51"/>
</package>
<package name="C120H40">
<pad name="1" x="0" y="0" drill="0.6" diameter="1.2" thermals="no"/>
<text x="0.9" y="-0.1" size="0.254" layer="25">&gt;NAME</text>
</package>
<package name="C131H51">
<pad name="1" x="0" y="0" drill="0.71" diameter="1.31" thermals="no"/>
<text x="1.2" y="0.1" size="0.254" layer="25">&gt;NAME</text>
<text x="1.2" y="-0.2" size="0.254" layer="21">&gt;LABEL</text>
</package>
<package name="C406H326">
<pad name="1" x="0" y="0" drill="3.26" diameter="4.06" thermals="no"/>
<text x="2.2" y="1.1" size="0.254" layer="25">&gt;NAME</text>
<text x="2.2" y="0.8" size="0.254" layer="21">&gt;LABEL</text>
</package>
<package name="C491H411">
<pad name="1" x="0" y="0" drill="4.31" diameter="4.91" thermals="no"/>
<text x="3.2" y="1.1" size="0.254" layer="25">&gt;NAME</text>
<text x="3.2" y="0.8" size="0.254" layer="21">&gt;LABEL</text>
</package>
<package name="TP_1.5MM_PAD">
<pad name="1" x="0" y="0" drill="1" diameter="1.5" rot="R180"/>
<text x="-1.27" y="4.445" size="1.27" layer="25">&gt;NAME</text>
<rectangle x1="-0.35" y1="-0.35" x2="0.35" y2="0.35" layer="51"/>
<rectangle x1="-2" y1="-2.5" x2="8" y2="2.5" layer="1"/>
</package>
<package name="TP_1.7MM">
<pad name="1" x="0" y="0" drill="1" diameter="1.6764" rot="R180"/>
<text x="-1.905" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<rectangle x1="-0.35" y1="-0.35" x2="0.35" y2="0.35" layer="51"/>
</package>
<package name="QFN65P700X700X80-33N">
<smd name="1" x="-3.35" y="2.275" dx="1" dy="0.35" layer="1"/>
<smd name="2" x="-3.35" y="1.625" dx="1" dy="0.35" layer="1"/>
<smd name="3" x="-3.35" y="0.975" dx="1" dy="0.35" layer="1"/>
<smd name="4" x="-3.35" y="0.325" dx="1" dy="0.35" layer="1"/>
<smd name="5" x="-3.35" y="-0.325" dx="1" dy="0.35" layer="1"/>
<smd name="6" x="-3.35" y="-0.975" dx="1" dy="0.35" layer="1"/>
<smd name="7" x="-3.35" y="-1.625" dx="1" dy="0.35" layer="1"/>
<smd name="8" x="-3.35" y="-2.275" dx="1" dy="0.35" layer="1"/>
<smd name="9" x="-2.275" y="-3.35" dx="1" dy="0.35" layer="1" rot="R90"/>
<smd name="10" x="-1.625" y="-3.35" dx="1" dy="0.35" layer="1" rot="R90"/>
<smd name="11" x="-0.975" y="-3.35" dx="1" dy="0.35" layer="1" rot="R90"/>
<smd name="12" x="-0.325" y="-3.35" dx="1" dy="0.35" layer="1" rot="R90"/>
<smd name="13" x="0.325" y="-3.35" dx="1" dy="0.35" layer="1" rot="R90"/>
<smd name="14" x="0.975" y="-3.35" dx="1" dy="0.35" layer="1" rot="R90"/>
<smd name="15" x="1.625" y="-3.35" dx="1" dy="0.35" layer="1" rot="R90"/>
<smd name="16" x="2.275" y="-3.35" dx="1" dy="0.35" layer="1" rot="R90"/>
<smd name="17" x="3.35" y="-2.275" dx="1" dy="0.35" layer="1" rot="R180"/>
<smd name="18" x="3.35" y="-1.625" dx="1" dy="0.35" layer="1" rot="R180"/>
<smd name="19" x="3.35" y="-0.975" dx="1" dy="0.35" layer="1" rot="R180"/>
<smd name="20" x="3.35" y="-0.325" dx="1" dy="0.35" layer="1" rot="R180"/>
<smd name="21" x="3.35" y="0.325" dx="1" dy="0.35" layer="1" rot="R180"/>
<smd name="22" x="3.35" y="0.975" dx="1" dy="0.35" layer="1" rot="R180"/>
<smd name="23" x="3.35" y="1.625" dx="1" dy="0.35" layer="1" rot="R180"/>
<smd name="24" x="3.35" y="2.275" dx="1" dy="0.35" layer="1" rot="R180"/>
<smd name="25" x="2.275" y="3.35" dx="1" dy="0.35" layer="1" rot="R270"/>
<smd name="26" x="1.625" y="3.35" dx="1" dy="0.35" layer="1" rot="R270"/>
<smd name="27" x="0.975" y="3.35" dx="1" dy="0.35" layer="1" rot="R270"/>
<smd name="28" x="0.325" y="3.35" dx="1" dy="0.35" layer="1" rot="R270"/>
<smd name="29" x="-0.325" y="3.35" dx="1" dy="0.35" layer="1" rot="R270"/>
<smd name="30" x="-0.975" y="3.35" dx="1" dy="0.35" layer="1" rot="R270"/>
<smd name="31" x="-1.625" y="3.35" dx="1" dy="0.35" layer="1" rot="R270"/>
<smd name="32" x="-2.275" y="3.35" dx="1" dy="0.35" layer="1" rot="R270"/>
<smd name="33" x="0" y="0" dx="4.85" dy="4.85" layer="1"/>
<wire x1="-2.8275" y1="4.1275" x2="-4.1275" y2="2.652" width="0.127" layer="51"/>
<wire x1="-4.1275" y1="2.652" x2="-4.1275" y2="-4.1275" width="0.127" layer="51"/>
<wire x1="-4.1275" y1="-4.1275" x2="4.1275" y2="-4.1275" width="0.127" layer="51"/>
<wire x1="4.1275" y1="-4.1275" x2="4.1275" y2="4.1275" width="0.127" layer="51"/>
<wire x1="4.1275" y1="4.1275" x2="-2.8275" y2="4.1275" width="0.127" layer="51"/>
<wire x1="-2.8275" y1="4.1275" x2="-4.1275" y2="2.652" width="0.127" layer="21"/>
<wire x1="-4.1275" y1="2.652" x2="-4.1275" y2="-4.1275" width="0.127" layer="21"/>
<wire x1="-4.1275" y1="-4.1275" x2="4.1275" y2="-4.1275" width="0.127" layer="21"/>
<wire x1="4.1275" y1="-4.1275" x2="4.1275" y2="4.1275" width="0.127" layer="21"/>
<wire x1="4.1275" y1="4.1275" x2="-2.8275" y2="4.1275" width="0.127" layer="21"/>
<text x="-3.9" y="4.55" size="1.27" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="-3.9" y="5.85" size="1.27" layer="27" font="vector" ratio="15">&gt;VALUE</text>
<rectangle x1="-2" y1="2.8" x2="-1.9" y2="3.9" layer="41"/>
<rectangle x1="1.9" y1="2.8" x2="2" y2="3.9" layer="41"/>
<rectangle x1="-0.7" y1="-3.9" x2="-0.6" y2="-2.8" layer="41"/>
</package>
<package name="RESC6332X65N">
<wire x1="2" y1="4" x2="2" y2="-4" width="0.127" layer="21"/>
<wire x1="2" y1="-4" x2="-2" y2="-4" width="0.127" layer="21"/>
<wire x1="-2" y1="-4" x2="-2" y2="4" width="0.127" layer="21"/>
<wire x1="-2" y1="4" x2="2" y2="4" width="0.127" layer="21"/>
<smd name="1" x="0" y="3" dx="1.25" dy="3.35" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-3" dx="1.25" dy="3.35" layer="1" roundness="25" rot="R270"/>
<text x="2" y="-2" size="0.889" layer="25" ratio="11" rot="R90">&gt;NAME</text>
<text x="-1" y="-2" size="0.635" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<wire x1="2" y1="4" x2="2" y2="-4" width="0.127" layer="51"/>
<wire x1="2" y1="-4" x2="-2" y2="-4" width="0.127" layer="51"/>
<wire x1="-2" y1="-4" x2="-2" y2="4" width="0.127" layer="51"/>
<wire x1="-2" y1="4" x2="2" y2="4" width="0.127" layer="51"/>
<rectangle x1="-1.5" y1="-3" x2="1.5" y2="3" layer="39"/>
</package>
<package name="TDFN50PC300X200X75-9N">
<smd name="1" x="-0.75" y="-1.5" dx="0.75" dy="0.3" layer="1" rot="R90"/>
<smd name="2" x="-0.25" y="-1.5" dx="0.75" dy="0.3" layer="1" rot="R90"/>
<smd name="3" x="0.25" y="-1.5" dx="0.75" dy="0.3" layer="1" rot="R90"/>
<smd name="4" x="0.75" y="-1.5" dx="0.75" dy="0.3" layer="1" rot="R90"/>
<smd name="5" x="0.75" y="1.5" dx="0.75" dy="0.3" layer="1" rot="R90"/>
<smd name="6" x="0.25" y="1.5" dx="0.75" dy="0.3" layer="1" rot="R90"/>
<smd name="7" x="-0.25" y="1.5" dx="0.75" dy="0.3" layer="1" rot="R90"/>
<smd name="8" x="-0.75" y="1.5" dx="0.75" dy="0.3" layer="1" rot="R90"/>
<smd name="9" x="0" y="0" dx="1.36" dy="1.46" layer="1" rot="R90"/>
<wire x1="-1.25" y1="1.5" x2="-1.25" y2="-1.5" width="0.127" layer="21"/>
<wire x1="1.25" y1="1.5" x2="1.25" y2="-1.5" width="0.127" layer="21"/>
<wire x1="-1.75" y1="-1.25" x2="-1.5" y2="-1.25" width="0.127" layer="21"/>
<wire x1="-1.5" y1="-1.25" x2="-1.5" y2="-1.5" width="0.127" layer="21"/>
<wire x1="-1.5" y1="-1.5" x2="-1.75" y2="-1.5" width="0.127" layer="21"/>
<wire x1="-1.75" y1="-1.5" x2="-1.75" y2="-1.25" width="0.127" layer="21"/>
<wire x1="-1.25" y1="1.5" x2="-1.25" y2="-1.5" width="0.127" layer="51"/>
<wire x1="1.25" y1="1.5" x2="1.25" y2="-1.5" width="0.127" layer="51"/>
<wire x1="-1.5" y1="-1.25" x2="-1.75" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-1.75" y1="-1.25" x2="-1.75" y2="-1.5" width="0.127" layer="51"/>
<wire x1="-1.75" y1="-1.5" x2="-1.5" y2="-1.5" width="0.127" layer="51"/>
<wire x1="-1.5" y1="-1.5" x2="-1.5" y2="-1.25" width="0.127" layer="51"/>
<text x="-1.5" y="-1" size="0.8128" layer="25" font="vector" ratio="15" rot="R90">&gt;NAME</text>
<text x="-2.5" y="-1" size="0.8128" layer="27" font="vector" ratio="15" rot="R90">&gt;VALUE</text>
</package>
<package name="DFN6X5MM">
<smd name="1" x="-1.905" y="-2.8" dx="0.75" dy="0.3" layer="1" rot="R90"/>
<smd name="2" x="-0.635" y="-2.8" dx="0.75" dy="0.3" layer="1" rot="R90"/>
<smd name="3" x="0.635" y="-2.8" dx="0.75" dy="0.3" layer="1" rot="R90"/>
<smd name="4" x="1.905" y="-2.8" dx="0.75" dy="0.3" layer="1" rot="R90"/>
<smd name="5" x="1.905" y="2.8" dx="0.75" dy="0.3" layer="1" rot="R90"/>
<smd name="6" x="0.635" y="2.8" dx="0.75" dy="0.3" layer="1" rot="R90"/>
<smd name="7" x="-0.635" y="2.8" dx="0.75" dy="0.3" layer="1" rot="R90"/>
<smd name="8" x="-1.905" y="2.8" dx="0.75" dy="0.3" layer="1" rot="R90"/>
<smd name="9" x="0" y="0" dx="2.4" dy="4.1" layer="1" rot="R90"/>
<wire x1="-2.52" y1="1.5" x2="-2.52" y2="-1.5" width="0.127" layer="21"/>
<wire x1="2.52" y1="1.5" x2="2.52" y2="-1.5" width="0.127" layer="21"/>
<wire x1="-2.75" y1="-1.25" x2="-2.5" y2="-1.25" width="0.127" layer="21"/>
<wire x1="-2.5" y1="-1.25" x2="-2.5" y2="-1.5" width="0.127" layer="21"/>
<wire x1="-2.5" y1="-1.5" x2="-2.75" y2="-1.5" width="0.127" layer="21"/>
<wire x1="-2.75" y1="-1.5" x2="-2.75" y2="-1.25" width="0.127" layer="21"/>
<text x="-3.5" y="-1" size="0.8128" layer="25" font="vector" ratio="15" rot="R90">&gt;NAME</text>
<text x="-4.5" y="-1" size="0.8128" layer="27" font="vector" ratio="15" rot="R90">&gt;VALUE</text>
</package>
<package name="JST_B4P-VH">
<pad name="P$1" x="-5.94" y="0" drill="1.65"/>
<pad name="P$2" x="-1.98" y="0" drill="1.65"/>
<text x="-7.96" y="-7" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.96" y="-7" size="1.27" layer="27">&gt;VALUE</text>
<pad name="P$3" x="1.98" y="0" drill="1.65"/>
<wire x1="-7.91" y1="-5.25" x2="-7.91" y2="2.5" width="0.127" layer="51"/>
<wire x1="-7.91" y1="2.5" x2="-7.91" y2="3.25" width="0.127" layer="51"/>
<wire x1="7.89" y1="3.25" x2="7.89" y2="2.5" width="0.127" layer="51"/>
<wire x1="7.89" y1="2.5" x2="7.9" y2="-5.25" width="0.127" layer="51"/>
<wire x1="-7.91" y1="3.25" x2="7.89" y2="3.25" width="0.127" layer="51"/>
<wire x1="-7.91" y1="-5.25" x2="7.9" y2="-5.25" width="0.127" layer="51"/>
<wire x1="-7.91" y1="2.5" x2="7.89" y2="2.5" width="0.127" layer="51"/>
<wire x1="-7.91" y1="-5.25" x2="-7.91" y2="2.5" width="0.127" layer="21"/>
<wire x1="-7.91" y1="2.5" x2="-7.91" y2="3.25" width="0.127" layer="21"/>
<wire x1="7.89" y1="3.25" x2="7.89" y2="2.5" width="0.127" layer="21"/>
<wire x1="7.89" y1="2.5" x2="7.9" y2="-5.25" width="0.127" layer="21"/>
<wire x1="-7.91" y1="3.25" x2="7.89" y2="3.25" width="0.127" layer="21"/>
<wire x1="-7.91" y1="-5.25" x2="7.9" y2="-5.25" width="0.127" layer="21"/>
<wire x1="-7.91" y1="2.5" x2="7.89" y2="2.5" width="0.127" layer="21"/>
<pad name="P$4" x="5.94" y="0" drill="1.65"/>
</package>
<package name="JST_B4PS-VH">
<pad name="P$1" x="-5.94" y="0" drill="1.65"/>
<pad name="P$2" x="-1.98" y="0" drill="1.65"/>
<text x="-7.96" y="3" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.96" y="3" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-7.89" y1="-7.2" x2="-7.89" y2="1.72" width="0.127" layer="51"/>
<wire x1="-7.36" y1="2.25" x2="7.92" y2="2.25" width="0.127" layer="51"/>
<wire x1="-7.89" y1="-7.2" x2="7.92" y2="-7.2" width="0.127" layer="51"/>
<wire x1="-7.89" y1="1.72" x2="-7.36" y2="2.25" width="0.127" layer="51"/>
<wire x1="7.92" y1="2.25" x2="7.92" y2="-7.2" width="0.127" layer="51"/>
<wire x1="-7.89" y1="-7.2" x2="-7.89" y2="1.72" width="0.127" layer="21"/>
<wire x1="-7.36" y1="2.25" x2="7.92" y2="2.25" width="0.127" layer="21"/>
<wire x1="-7.89" y1="-7.2" x2="7.92" y2="-7.2" width="0.127" layer="21"/>
<wire x1="-7.89" y1="1.72" x2="-7.36" y2="2.25" width="0.127" layer="21"/>
<wire x1="7.92" y1="2.25" x2="7.92" y2="-7.2" width="0.127" layer="21"/>
<pad name="P$3" x="1.98" y="0" drill="1.65"/>
<pad name="P$4" x="5.94" y="0" drill="1.65"/>
</package>
<package name="JST_B4P-VH-FB-B">
<pad name="P$1" x="-5.94" y="0" drill="1.65"/>
<pad name="P$2" x="-1.98" y="0" drill="1.65"/>
<hole x="-7.44" y="-3.4" drill="1.4"/>
<wire x1="-8.86" y1="4.85" x2="8.86" y2="4.85" width="0.127" layer="21"/>
<wire x1="-8.86" y1="-4.85" x2="8.86" y2="-4.85" width="0.127" layer="21"/>
<wire x1="-8.86" y1="4.85" x2="-8.86" y2="-4.85" width="0.127" layer="21"/>
<wire x1="8.86" y1="4.85" x2="8.86" y2="-4.85" width="0.127" layer="21"/>
<wire x1="-8.86" y1="4.85" x2="8.86" y2="4.85" width="0.127" layer="51"/>
<wire x1="-8.86" y1="-4.85" x2="8.86" y2="-4.85" width="0.127" layer="51"/>
<wire x1="-8.86" y1="4.85" x2="-8.86" y2="-4.85" width="0.127" layer="51"/>
<wire x1="8.86" y1="4.85" x2="8.86" y2="-4.85" width="0.127" layer="51"/>
<text x="-8.96" y="5" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.96" y="5" size="1.27" layer="27">&gt;VALUE</text>
<pad name="P$3" x="1.98" y="0" drill="1.65"/>
<pad name="P$4" x="5.94" y="0" drill="1.65"/>
</package>
<package name="IND_SMD_4.9X4.9">
<wire x1="2.4035" y1="2.41875" x2="-2.3965" y2="2.41875" width="0.127" layer="21"/>
<wire x1="-2.3965" y1="-2.38125" x2="2.4035" y2="-2.38125" width="0.127" layer="21"/>
<wire x1="2.4035" y1="2.41875" x2="-2.3965" y2="2.41875" width="0.127" layer="51"/>
<wire x1="-2.3965" y1="-2.38125" x2="2.4035" y2="-2.38125" width="0.127" layer="51"/>
<smd name="P$1" x="-1.80525" y="0.01875" dx="4" dy="1.5" layer="1" rot="R90"/>
<smd name="P$2" x="1.76975" y="0.01875" dx="4" dy="1.5" layer="1" rot="R90"/>
<text x="-2.95275" y="2.69875" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.95275" y="-3.96875" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="SOIC127P600X173-8N">
<smd name="1" x="-2.7" y="1.905" dx="1.6" dy="0.6" layer="1"/>
<smd name="2" x="-2.7" y="0.635" dx="1.6" dy="0.6" layer="1"/>
<smd name="3" x="-2.7" y="-0.635" dx="1.6" dy="0.6" layer="1"/>
<smd name="4" x="-2.7" y="-1.905" dx="1.6" dy="0.6" layer="1"/>
<smd name="5" x="2.7" y="-1.905" dx="1.6" dy="0.6" layer="1"/>
<smd name="6" x="2.7" y="-0.635" dx="1.6" dy="0.6" layer="1"/>
<smd name="7" x="2.7" y="0.635" dx="1.6" dy="0.6" layer="1"/>
<smd name="8" x="2.7" y="1.905" dx="1.6" dy="0.6" layer="1"/>
<wire x1="2.5" y1="3" x2="2.5" y2="2.5" width="0.127" layer="21"/>
<wire x1="-2.5" y1="-2.5" x2="-2.5" y2="-3" width="0.127" layer="21"/>
<wire x1="-2.5" y1="-3" x2="2.5" y2="-3" width="0.127" layer="21"/>
<wire x1="2.5" y1="-3" x2="2.5" y2="-2.5" width="0.127" layer="21"/>
<wire x1="2.5" y1="3" x2="-2.5" y2="3" width="0.127" layer="21"/>
<wire x1="-2.5" y1="3" x2="-2.5" y2="2.5" width="0.127" layer="21"/>
<wire x1="-2.5" y1="2.5" x2="-2.5" y2="3" width="0.127" layer="51"/>
<wire x1="-2.5" y1="3" x2="2.5" y2="3" width="0.127" layer="51"/>
<wire x1="2.5" y1="3" x2="2.5" y2="2.5" width="0.127" layer="51"/>
<wire x1="-2.5" y1="-2.5" x2="-2.5" y2="-3" width="0.127" layer="51"/>
<wire x1="-2.5" y1="-3" x2="2.5" y2="-3" width="0.127" layer="51"/>
<wire x1="2.5" y1="-3" x2="2.5" y2="-2.5" width="0.127" layer="51"/>
<rectangle x1="-3.1985" y1="2.738" x2="-2.6985" y2="3.238" layer="51"/>
<rectangle x1="-3.1985" y1="2.738" x2="-2.6985" y2="3.238" layer="21"/>
<text x="-1.27" y="3.81" size="1.27" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="-1.27" y="5.08" size="1.27" layer="27" font="vector" ratio="15">&gt;VALUE</text>
<wire x1="-2.54" y1="2.54" x2="-3.81" y2="2.54" width="0.127" layer="51"/>
<wire x1="-3.81" y1="2.54" x2="-3.81" y2="-2.54" width="0.127" layer="51"/>
<wire x1="-3.81" y1="-2.54" x2="-2.54" y2="-2.54" width="0.127" layer="51"/>
<wire x1="2.54" y1="2.54" x2="3.81" y2="2.54" width="0.127" layer="51"/>
<wire x1="3.81" y1="2.54" x2="3.81" y2="-2.54" width="0.127" layer="51"/>
<wire x1="3.81" y1="-2.54" x2="2.54" y2="-2.54" width="0.127" layer="51"/>
</package>
<package name="DO-214AC_SMA">
<wire x1="-2.75" y1="-3.5" x2="-2.75" y2="3.5" width="0.127" layer="21"/>
<smd name="A" x="0" y="-2" dx="2.5" dy="1.7" layer="1" rot="R90"/>
<smd name="K" x="0" y="2" dx="2.5" dy="1.7" layer="1" rot="R90"/>
<wire x1="2.75" y1="3.5" x2="2.75" y2="-3.5" width="0.127" layer="21"/>
<wire x1="-2.75" y1="3.5" x2="2.75" y2="3.5" width="0.127" layer="21"/>
<wire x1="-2.75" y1="-3.5" x2="2.75" y2="-3.5" width="0.127" layer="21"/>
<wire x1="-3" y1="4" x2="3" y2="4" width="0.127" layer="21"/>
<wire x1="-2.75" y1="-3.5" x2="-2.75" y2="3.5" width="0.127" layer="51"/>
<wire x1="2.75" y1="3.5" x2="2.75" y2="-3.5" width="0.127" layer="51"/>
<wire x1="-2.75" y1="3.5" x2="2.75" y2="3.5" width="0.127" layer="51"/>
<wire x1="-2.75" y1="-3.5" x2="2.75" y2="-3.5" width="0.127" layer="51"/>
<wire x1="-3" y1="4" x2="3" y2="4" width="0.127" layer="51"/>
<text x="2.905" y="1.54" size="1.27" layer="25">&gt;NAME</text>
<text x="2.905" y="0" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="IND_SMD_5.0X5.0">
<smd name="P$1" x="-1.79" y="-0.01" dx="1.5" dy="4" layer="1"/>
<smd name="P$2" x="1.81" y="-0.01" dx="1.5" dy="4" layer="1"/>
<wire x1="-2.5" y1="2.5" x2="2.5" y2="2.5" width="0.127" layer="21"/>
<wire x1="-2.5" y1="-2.5" x2="2.5" y2="-2.5" width="0.127" layer="21"/>
<wire x1="-2.5" y1="2.5" x2="2.5" y2="2.5" width="0.127" layer="51"/>
<wire x1="-2.5" y1="-2.5" x2="2.5" y2="-2.5" width="0.127" layer="51"/>
<text x="-3.4" y="2.7" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.4" y="-4" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="SOT-23-3">
<smd name="3" x="0" y="-1.1" dx="1.4" dy="1" layer="1" rot="R90"/>
<smd name="2" x="-0.95" y="1.1" dx="1.4" dy="1" layer="1" rot="R90"/>
<smd name="1" x="0.95" y="1.1" dx="1.4" dy="1" layer="1" rot="R90"/>
<wire x1="-2" y1="1" x2="-2" y2="-1" width="0.127" layer="21"/>
<wire x1="-2" y1="-1" x2="-1" y2="-1" width="0.127" layer="21"/>
<wire x1="1" y1="-1" x2="2" y2="-1" width="0.127" layer="21"/>
<wire x1="2" y1="-1" x2="2" y2="1" width="0.127" layer="21"/>
<wire x1="-0.25" y1="1" x2="0.25" y2="1" width="0.127" layer="21"/>
<wire x1="-2" y1="1" x2="-2" y2="-1" width="0.127" layer="51"/>
<wire x1="-2" y1="-1" x2="-1" y2="-1" width="0.127" layer="51"/>
<wire x1="2" y1="1" x2="2" y2="-1" width="0.127" layer="51"/>
<wire x1="2" y1="-1" x2="1" y2="-1" width="0.127" layer="51"/>
<wire x1="-0.25" y1="1" x2="0.25" y2="1" width="0.127" layer="51"/>
<text x="-2" y="2" size="1.27" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="-2" y="4" size="1.27" layer="27" font="vector" ratio="15">&gt;VALUE</text>
</package>
<package name="CAPAE430X460N">
<smd name="2" x="-1.8" y="0" dx="2.65" dy="1.65" layer="1"/>
<smd name="1" x="1.8" y="0" dx="2.65" dy="1.65" layer="1"/>
<wire x1="1.5" y1="-2.15" x2="-2.15" y2="-2.15" width="0.2032" layer="21"/>
<wire x1="-2.15" y1="-2.15" x2="-2.15" y2="-1.1" width="0.2032" layer="21"/>
<wire x1="-2.15" y1="1.1" x2="-2.15" y2="2.15" width="0.2032" layer="21"/>
<wire x1="-2.15" y1="2.15" x2="1.5" y2="2.15" width="0.2032" layer="21"/>
<wire x1="2.15" y1="1.5" x2="2.15" y2="1.1" width="0.2032" layer="21"/>
<wire x1="2.15" y1="-1.1" x2="2.15" y2="-1.5" width="0.2032" layer="21"/>
<wire x1="2.15" y1="-1.5" x2="1.5" y2="-2.15" width="0.2032" layer="21"/>
<wire x1="1.5" y1="2.15" x2="2.15" y2="1.5" width="0.2032" layer="21"/>
<text x="-2.74" y="2.98" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.775" y="-4.235" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="1.5" y1="-2.15" x2="-2.15" y2="-2.15" width="0.2032" layer="51"/>
<wire x1="-2.15" y1="-2.15" x2="-2.15" y2="-1.1" width="0.2032" layer="51"/>
<wire x1="-2.15" y1="1.1" x2="-2.15" y2="2.15" width="0.2032" layer="51"/>
<wire x1="-2.15" y1="2.15" x2="1.5" y2="2.15" width="0.2032" layer="51"/>
<wire x1="2.15" y1="1.5" x2="2.15" y2="1.1" width="0.2032" layer="51"/>
<wire x1="2.15" y1="-1.1" x2="2.15" y2="-1.5" width="0.2032" layer="51"/>
<wire x1="2.15" y1="-1.5" x2="1.5" y2="-2.15" width="0.2032" layer="51"/>
<wire x1="1.5" y1="2.15" x2="2.15" y2="1.5" width="0.2032" layer="51"/>
</package>
<package name="CAPAE660X790N">
<smd name="2" x="-2.45" y="0" dx="3.15" dy="1.6" layer="1"/>
<smd name="1" x="2.45" y="0" dx="3.15" dy="1.6" layer="1"/>
<wire x1="2.25" y1="-3.3" x2="-3.3" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="-3.3" y1="-3.3" x2="-3.3" y2="-1.1" width="0.2032" layer="21"/>
<wire x1="-3.3" y1="1.1" x2="-3.3" y2="3.3" width="0.2032" layer="21"/>
<wire x1="-3.3" y1="3.3" x2="2.25" y2="3.3" width="0.2032" layer="21"/>
<wire x1="3.3" y1="2.25" x2="3.3" y2="1.1" width="0.2032" layer="21"/>
<wire x1="3.3" y1="-1.1" x2="3.3" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="3.3" y1="-2.25" x2="2.25" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="2.25" y1="3.3" x2="3.3" y2="2.25" width="0.2032" layer="21"/>
<text x="-2.54" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-5.08" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="2.25" y1="-3.3" x2="-3.3" y2="-3.3" width="0.2032" layer="51"/>
<wire x1="-3.3" y1="-3.3" x2="-3.3" y2="-1.1" width="0.2032" layer="51"/>
<wire x1="-3.3" y1="1.1" x2="-3.3" y2="3.3" width="0.2032" layer="51"/>
<wire x1="-3.3" y1="3.3" x2="2.25" y2="3.3" width="0.2032" layer="51"/>
<wire x1="3.3" y1="2.25" x2="3.3" y2="1.1" width="0.2032" layer="51"/>
<wire x1="3.3" y1="-1.1" x2="3.3" y2="-2.25" width="0.2032" layer="51"/>
<wire x1="3.3" y1="-2.25" x2="2.25" y2="-3.3" width="0.2032" layer="51"/>
<wire x1="2.25" y1="3.3" x2="3.3" y2="2.25" width="0.2032" layer="51"/>
</package>
<package name="CAPAE830X680N">
<smd name="2" x="-3.05" y="0" dx="4.15" dy="1.65" layer="1"/>
<smd name="1" x="3.05" y="0" dx="4.15" dy="1.65" layer="1"/>
<wire x1="2.75" y1="-4.2" x2="-4.2" y2="-4.2" width="0.2032" layer="21"/>
<wire x1="-4.2" y1="-4.2" x2="-4.2" y2="-1.1" width="0.2032" layer="21"/>
<wire x1="-4.2" y1="1.1" x2="-4.2" y2="4.2" width="0.2032" layer="21"/>
<wire x1="-4.2" y1="4.2" x2="2.75" y2="4.2" width="0.2032" layer="21"/>
<wire x1="4.2" y1="2.75" x2="4.2" y2="1.1" width="0.2032" layer="21"/>
<wire x1="4.2" y1="-1.1" x2="4.2" y2="-2.75" width="0.2032" layer="21"/>
<wire x1="4.2" y1="-2.75" x2="2.75" y2="-4.2" width="0.2032" layer="21"/>
<wire x1="2.75" y1="4.2" x2="4.2" y2="2.75" width="0.2032" layer="21"/>
<text x="-3.04" y="4.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.04" y="-6.08" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="2.75" y1="-4.2" x2="-4.2" y2="-4.2" width="0.2032" layer="51"/>
<wire x1="-4.2" y1="-4.2" x2="-4.2" y2="-1.1" width="0.2032" layer="51"/>
<wire x1="-4.2" y1="1.1" x2="-4.2" y2="4.2" width="0.2032" layer="51"/>
<wire x1="-4.2" y1="4.2" x2="2.75" y2="4.2" width="0.2032" layer="51"/>
<wire x1="4.2" y1="2.75" x2="4.2" y2="1.1" width="0.2032" layer="51"/>
<wire x1="4.2" y1="-1.1" x2="4.2" y2="-2.75" width="0.2032" layer="51"/>
<wire x1="4.2" y1="-2.75" x2="2.75" y2="-4.2" width="0.2032" layer="51"/>
<wire x1="2.75" y1="4.2" x2="4.2" y2="2.75" width="0.2032" layer="51"/>
</package>
<package name="MRA08B">
<smd name="1" x="-2.6543" y="1.905" dx="1.6002" dy="0.5334" layer="1"/>
<smd name="2" x="-2.6543" y="0.635" dx="1.6002" dy="0.5334" layer="1"/>
<smd name="3" x="-2.6543" y="-0.635" dx="1.6002" dy="0.5334" layer="1"/>
<smd name="4" x="-2.6543" y="-1.905" dx="1.6002" dy="0.5334" layer="1"/>
<smd name="5" x="2.6543" y="-1.905" dx="1.6002" dy="0.5334" layer="1"/>
<smd name="6" x="2.6543" y="-0.635" dx="1.6002" dy="0.5334" layer="1"/>
<smd name="7" x="2.6543" y="0.635" dx="1.6002" dy="0.5334" layer="1"/>
<smd name="8" x="2.6543" y="1.905" dx="1.6002" dy="0.5334" layer="1"/>
<smd name="9" x="0" y="0" dx="2.413" dy="3.0988" layer="1" cream="no"/>
<wire x1="-2.0066" y1="1.6764" x2="-2.0066" y2="2.1336" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="2.1336" x2="-3.0988" y2="2.1336" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="2.1336" x2="-3.0988" y2="1.651" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="1.651" x2="-2.0066" y2="1.6764" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="0.4064" x2="-2.0066" y2="0.8636" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="0.8636" x2="-3.0988" y2="0.8636" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="0.8636" x2="-3.0988" y2="0.381" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="0.381" x2="-2.0066" y2="0.4064" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="-0.8636" x2="-2.0066" y2="-0.4064" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="-0.4064" x2="-3.0988" y2="-0.4064" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="-0.4064" x2="-3.0988" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="-0.889" x2="-2.0066" y2="-0.8636" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="-2.1336" x2="-2.0066" y2="-1.6764" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="-1.6764" x2="-3.0988" y2="-1.6764" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="-1.6764" x2="-3.0988" y2="-2.159" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="-2.159" x2="-2.0066" y2="-2.1336" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="-1.6764" x2="2.0066" y2="-2.1336" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="-2.1336" x2="3.0988" y2="-2.1336" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="-2.1336" x2="3.0988" y2="-1.651" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="-1.651" x2="2.0066" y2="-1.6764" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="-0.4064" x2="2.0066" y2="-0.8636" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="-0.8636" x2="3.0988" y2="-0.8636" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="-0.8636" x2="3.0988" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="-0.381" x2="2.0066" y2="-0.4064" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="0.8636" x2="2.0066" y2="0.4064" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="0.4064" x2="3.0988" y2="0.4064" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="0.4064" x2="3.0988" y2="0.889" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="0.889" x2="2.0066" y2="0.8636" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="2.1336" x2="2.0066" y2="1.6764" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="1.6764" x2="3.0988" y2="1.6764" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="1.6764" x2="3.0988" y2="2.159" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="2.159" x2="2.0066" y2="2.1336" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="-2.4892" x2="2.0066" y2="-2.4892" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="-2.4892" x2="2.0066" y2="2.4892" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="2.4892" x2="-0.3048" y2="2.4892" width="0.1524" layer="51"/>
<wire x1="-0.3048" y1="2.4892" x2="-2.0066" y2="2.4892" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="2.4892" x2="-2.0066" y2="-2.4892" width="0.1524" layer="51"/>
<wire x1="0.3048" y1="2.5146" x2="-0.3048" y2="2.4892" width="0.1524" layer="51" curve="-180"/>
<text x="-2.2098" y="1.1684" size="1.27" layer="51" ratio="6" rot="SR0">*</text>
<polygon width="0.0254" layer="31">
<vertex x="-1.1065" y="1.4494"/>
<vertex x="-1.1065" y="0.1"/>
<vertex x="1.1065" y="0.1"/>
<vertex x="1.1065" y="1.4494"/>
</polygon>
<polygon width="0.0254" layer="31">
<vertex x="-1.1065" y="-0.1"/>
<vertex x="-1.1065" y="-1.4494"/>
<vertex x="1.1065" y="-1.4494"/>
<vertex x="1.1065" y="-0.1"/>
</polygon>
<wire x1="-2.1336" y1="-2.6416" x2="2.1336" y2="-2.6416" width="0.1524" layer="21"/>
<wire x1="2.1336" y1="2.6416" x2="-2.1336" y2="2.6416" width="0.1524" layer="21"/>
<text x="-3.4798" y="2.3114" size="1.27" layer="21" ratio="6" rot="SR0">*</text>
<text x="-3.2766" y="-0.635" size="1.27" layer="25" ratio="6" rot="SR0">&gt;Name</text>
</package>
<package name="MRA08B-M">
<smd name="1" x="-2.7051" y="1.905" dx="1.905" dy="0.5842" layer="1"/>
<smd name="2" x="-2.7051" y="0.635" dx="1.905" dy="0.5842" layer="1"/>
<smd name="3" x="-2.7051" y="-0.635" dx="1.905" dy="0.5842" layer="1"/>
<smd name="4" x="-2.7051" y="-1.905" dx="1.905" dy="0.5842" layer="1"/>
<smd name="5" x="2.7051" y="-1.905" dx="1.905" dy="0.5842" layer="1"/>
<smd name="6" x="2.7051" y="-0.635" dx="1.905" dy="0.5842" layer="1"/>
<smd name="7" x="2.7051" y="0.635" dx="1.905" dy="0.5842" layer="1"/>
<smd name="8" x="2.7051" y="1.905" dx="1.905" dy="0.5842" layer="1"/>
<smd name="EPAD" x="0" y="0" dx="2.413" dy="3.0988" layer="1" cream="no"/>
<wire x1="-2.0066" y1="-2.4892" x2="2.0066" y2="-2.4892" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="2.4892" x2="0.2794" y2="2.4892" width="0.1524" layer="51"/>
<wire x1="0.2794" y1="2.4892" x2="-0.3048" y2="2.4892" width="0.1524" layer="51"/>
<wire x1="-0.3048" y1="2.4892" x2="-2.0066" y2="2.4892" width="0.1524" layer="51"/>
<wire x1="0.2794" y1="2.4892" x2="-0.3048" y2="2.4892" width="0.1524" layer="51" curve="-180"/>
<polygon width="0.0254" layer="31">
<vertex x="-1.1065" y="1.4494"/>
<vertex x="-1.1065" y="0.1"/>
<vertex x="1.1065" y="0.1"/>
<vertex x="1.1065" y="1.4494"/>
</polygon>
<polygon width="0.0254" layer="31">
<vertex x="-1.1065" y="-0.1"/>
<vertex x="-1.1065" y="-1.4494"/>
<vertex x="1.1065" y="-1.4494"/>
<vertex x="1.1065" y="-0.1"/>
</polygon>
<text x="-3.2766" y="-4.191" size="1.27" layer="25" ratio="6" rot="SR0">&gt;Name</text>
<rectangle x1="-3.048" y1="2.54" x2="-2.54" y2="2.9972" layer="21"/>
<wire x1="-2.0066" y1="-2.4892" x2="2.0066" y2="-2.4892" width="0.1524" layer="21"/>
<wire x1="2.0066" y1="2.4892" x2="0.2794" y2="2.4892" width="0.1524" layer="21"/>
<wire x1="0.2794" y1="2.4892" x2="-0.3048" y2="2.4892" width="0.1524" layer="21"/>
<wire x1="-0.3048" y1="2.4892" x2="-2.0066" y2="2.4892" width="0.1524" layer="21"/>
<wire x1="0.2794" y1="2.4892" x2="-0.3048" y2="2.4892" width="0.1524" layer="21" curve="-180"/>
<rectangle x1="-3.048" y1="2.54" x2="-2.54" y2="2.9972" layer="51"/>
</package>
<package name="MRA08B-L">
<smd name="1" x="-2.6035" y="1.905" dx="1.2954" dy="0.4826" layer="1"/>
<smd name="2" x="-2.6035" y="0.635" dx="1.2954" dy="0.4826" layer="1"/>
<smd name="3" x="-2.6035" y="-0.635" dx="1.2954" dy="0.4826" layer="1"/>
<smd name="4" x="-2.6035" y="-1.905" dx="1.2954" dy="0.4826" layer="1"/>
<smd name="5" x="2.6035" y="-1.905" dx="1.2954" dy="0.4826" layer="1"/>
<smd name="6" x="2.6035" y="-0.635" dx="1.2954" dy="0.4826" layer="1"/>
<smd name="7" x="2.6035" y="0.635" dx="1.2954" dy="0.4826" layer="1"/>
<smd name="8" x="2.6035" y="1.905" dx="1.2954" dy="0.4826" layer="1"/>
<smd name="EPAD" x="0" y="0" dx="2.413" dy="3.0988" layer="1" cream="no"/>
<wire x1="-2.0066" y1="1.6764" x2="-2.0066" y2="2.1336" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="2.1336" x2="-3.0988" y2="2.1336" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="2.1336" x2="-3.0988" y2="1.651" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="1.651" x2="-2.0066" y2="1.6764" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="0.4064" x2="-2.0066" y2="0.8636" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="0.8636" x2="-3.0988" y2="0.8636" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="0.8636" x2="-3.0988" y2="0.381" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="0.381" x2="-2.0066" y2="0.4064" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="-0.8636" x2="-2.0066" y2="-0.4064" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="-0.4064" x2="-3.0988" y2="-0.4064" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="-0.4064" x2="-3.0988" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="-0.889" x2="-2.0066" y2="-0.8636" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="-2.1336" x2="-2.0066" y2="-1.6764" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="-1.6764" x2="-3.0988" y2="-1.6764" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="-1.6764" x2="-3.0988" y2="-2.159" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="-2.159" x2="-2.0066" y2="-2.1336" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="-1.6764" x2="2.0066" y2="-2.1336" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="-2.1336" x2="3.0988" y2="-2.1336" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="-2.1336" x2="3.0988" y2="-1.651" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="-1.651" x2="2.0066" y2="-1.6764" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="-0.4064" x2="2.0066" y2="-0.8636" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="-0.8636" x2="3.0988" y2="-0.8636" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="-0.8636" x2="3.0988" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="-0.381" x2="2.0066" y2="-0.4064" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="0.8636" x2="2.0066" y2="0.4064" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="0.4064" x2="3.0988" y2="0.4064" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="0.4064" x2="3.0988" y2="0.889" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="0.889" x2="2.0066" y2="0.8636" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="2.1336" x2="2.0066" y2="1.6764" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="1.6764" x2="3.0988" y2="1.6764" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="1.6764" x2="3.0988" y2="2.159" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="2.159" x2="2.0066" y2="2.1336" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="-2.4892" x2="2.0066" y2="-2.4892" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="-2.4892" x2="2.0066" y2="2.4892" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="2.4892" x2="-0.3048" y2="2.4892" width="0.1524" layer="51"/>
<wire x1="-0.3048" y1="2.4892" x2="-2.0066" y2="2.4892" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="2.4892" x2="-2.0066" y2="-2.4892" width="0.1524" layer="51"/>
<wire x1="0.3048" y1="2.5146" x2="-0.3048" y2="2.4892" width="0.1524" layer="51" curve="-180"/>
<text x="-2.2098" y="1.1684" size="1.27" layer="51" ratio="6" rot="SR0">*</text>
<polygon width="0.0254" layer="31">
<vertex x="-1.1065" y="1.4494"/>
<vertex x="-1.1065" y="0.1"/>
<vertex x="1.1065" y="0.1"/>
<vertex x="1.1065" y="1.4494"/>
</polygon>
<polygon width="0.0254" layer="31">
<vertex x="-1.1065" y="-0.1"/>
<vertex x="-1.1065" y="-1.4494"/>
<vertex x="1.1065" y="-1.4494"/>
<vertex x="1.1065" y="-0.1"/>
</polygon>
<wire x1="-2.1336" y1="-2.6416" x2="2.1336" y2="-2.6416" width="0.1524" layer="21"/>
<wire x1="2.1336" y1="2.6416" x2="-2.1336" y2="2.6416" width="0.1524" layer="21"/>
<text x="-3.429" y="2.2606" size="1.27" layer="21" ratio="6" rot="SR0">*</text>
<text x="-3.2766" y="-0.635" size="1.27" layer="25" ratio="6" rot="SR0">&gt;Name</text>
</package>
<package name="DO-214AC_475X290">
<wire x1="-2.02" y1="-3.43" x2="-2.02" y2="3.43" width="0.127" layer="21"/>
<wire x1="-2.02" y1="3.43" x2="2.02" y2="3.43" width="0.127" layer="21"/>
<wire x1="2.02" y1="3.43" x2="2.02" y2="-3.43" width="0.127" layer="21"/>
<wire x1="2.02" y1="-3.43" x2="-2.02" y2="-3.43" width="0.127" layer="21" style="shortdash"/>
<smd name="1" x="0" y="2.25" dx="1.7" dy="2.5" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-2.25" dx="1.7" dy="2.5" layer="1" roundness="25" rot="R270"/>
<text x="3" y="-2" size="0.635" layer="21" ratio="11" rot="R90">&gt;NAME</text>
<text x="-2.25" y="-2" size="0.635" layer="27" font="vector" ratio="10" rot="R90">&gt;VALUE</text>
<wire x1="-2.02" y1="-3.43" x2="-2.02" y2="3.43" width="0.127" layer="21"/>
<wire x1="-2.02" y1="3.43" x2="2.02" y2="3.43" width="0.127" layer="21"/>
<wire x1="2.02" y1="3.43" x2="2.02" y2="-3.43" width="0.127" layer="21"/>
<wire x1="2.02" y1="-3.43" x2="-2.02" y2="-3.43" width="0.127" layer="21" style="shortdash"/>
<wire x1="-2" y1="-3.65" x2="2" y2="-3.65" width="0.127" layer="21"/>
<wire x1="-2" y1="-3.65" x2="2" y2="-3.65" width="0.127" layer="21"/>
<wire x1="-0.5" y1="-3.43" x2="-2.02" y2="-3.43" width="0.127" layer="51"/>
<wire x1="-2.02" y1="-3.43" x2="-2.02" y2="3.43" width="0.127" layer="51"/>
<wire x1="-2.02" y1="3.43" x2="2.02" y2="3.43" width="0.127" layer="51"/>
<wire x1="2.01" y1="3.42" x2="2.02" y2="3.42" width="0.127" layer="51"/>
<wire x1="2.02" y1="3.42" x2="2.02" y2="-3.43" width="0.127" layer="51"/>
<wire x1="2.02" y1="-3.43" x2="0.5" y2="-3.43" width="0.127" layer="51"/>
<wire x1="1.74" y1="-3.65" x2="2" y2="-3.65" width="0.127" layer="51"/>
<wire x1="1.69" y1="-3.65" x2="1.74" y2="-3.65" width="0.127" layer="51"/>
<wire x1="1.72" y1="-3.65" x2="-2" y2="-3.65" width="0.127" layer="51"/>
</package>
<package name="SOT230P700X180-4N">
<smd name="1" x="-2.9" y="2.3" dx="2.15" dy="0.95" layer="1"/>
<smd name="2" x="-2.9" y="0" dx="2.15" dy="0.95" layer="1"/>
<smd name="3" x="-2.9" y="-2.3" dx="2.15" dy="0.95" layer="1"/>
<smd name="4" x="2.9" y="0" dx="2.15" dy="3.25" layer="1"/>
<wire x1="-1.85" y1="3.35" x2="1.85" y2="3.35" width="0.127" layer="21"/>
<wire x1="1.85" y1="3.35" x2="1.85" y2="1.8" width="0.127" layer="21"/>
<wire x1="-1.85" y1="3.35" x2="-1.85" y2="2.95" width="0.127" layer="21"/>
<wire x1="-1.85" y1="1.65" x2="-1.85" y2="0.65" width="0.127" layer="21"/>
<wire x1="-1.85" y1="-0.65" x2="-1.85" y2="-1.65" width="0.127" layer="21"/>
<wire x1="-1.85" y1="-2.95" x2="-1.85" y2="-3.35" width="0.127" layer="21"/>
<wire x1="-1.85" y1="-3.35" x2="1.85" y2="-3.35" width="0.127" layer="21"/>
<wire x1="1.85" y1="-3.35" x2="1.85" y2="-1.8" width="0.127" layer="21"/>
<wire x1="-1.85" y1="3.35" x2="1.85" y2="3.35" width="0.127" layer="51"/>
<wire x1="1.85" y1="3.35" x2="1.85" y2="1.8" width="0.127" layer="51"/>
<wire x1="-1.85" y1="3.35" x2="-1.85" y2="2.95" width="0.127" layer="51"/>
<wire x1="-1.85" y1="1.65" x2="-1.85" y2="0.65" width="0.127" layer="51"/>
<wire x1="-1.85" y1="-0.65" x2="-1.85" y2="-1.65" width="0.127" layer="51"/>
<wire x1="-1.85" y1="-2.95" x2="-1.85" y2="-3.35" width="0.127" layer="51"/>
<wire x1="-1.85" y1="-3.35" x2="1.85" y2="-3.35" width="0.127" layer="51"/>
<wire x1="1.85" y1="-3.35" x2="1.85" y2="-1.8" width="0.127" layer="51"/>
<text x="2.54" y="3.81" size="0.4064" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="2.54" y="2.54" size="0.4064" layer="27" font="vector" ratio="15">&gt;VALUE</text>
</package>
<package name="JST_B08B-ZESK-1D">
<pad name="P$1" x="5.25" y="1.98" drill="0.7" rot="R180"/>
<hole x="6.9" y="-1.82" drill="0.8"/>
<pad name="P$2" x="3.75" y="-0.02" drill="0.7" rot="R180"/>
<pad name="P$3" x="2.25" y="1.98" drill="0.7" rot="R180"/>
<pad name="P$4" x="0.75" y="-0.02" drill="0.7" rot="R180"/>
<pad name="P$5" x="-0.75" y="1.98" drill="0.7" rot="R180"/>
<pad name="P$6" x="-2.25" y="-0.02" drill="0.7" rot="R180"/>
<pad name="P$7" x="-3.75" y="1.98" drill="0.7" rot="R180"/>
<pad name="P$8" x="-5.25" y="-0.02" drill="0.7" rot="R180"/>
<wire x1="-7.505" y1="3.115" x2="-7.505" y2="-3.105" width="0.127" layer="51"/>
<wire x1="7.505" y1="3.115" x2="7.505" y2="-3.105" width="0.127" layer="51"/>
<wire x1="-7.505" y1="3.115" x2="7.505" y2="3.115" width="0.127" layer="51"/>
<wire x1="-7.505" y1="-3.105" x2="7.505" y2="-3.105" width="0.127" layer="51"/>
<text x="1.5" y="3.48" size="1.27" layer="25">&gt;NAME</text>
<text x="-6.75" y="3.48" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-7.505" y1="3.115" x2="-7.505" y2="-3.105" width="0.127" layer="21"/>
<wire x1="7.505" y1="3.115" x2="7.505" y2="-3.105" width="0.127" layer="21"/>
<wire x1="-7.505" y1="3.115" x2="7.505" y2="3.115" width="0.127" layer="21"/>
<wire x1="-7.505" y1="-3.105" x2="7.505" y2="-3.105" width="0.127" layer="21"/>
</package>
<package name="JST_S08B-ZESK-2D">
<pad name="P$1" x="5.25" y="-7.5" drill="0.7"/>
<pad name="P$2" x="3.75" y="-3.8" drill="0.7"/>
<pad name="P$3" x="2.25" y="-7.5" drill="0.7"/>
<pad name="P$4" x="0.75" y="-3.8" drill="0.7"/>
<pad name="P$5" x="-0.75" y="-7.5" drill="0.7"/>
<pad name="P$6" x="-2.25" y="-3.8" drill="0.7"/>
<pad name="P$7" x="-3.75" y="-7.5" drill="0.7"/>
<pad name="P$8" x="-5.25" y="-3.8" drill="0.7"/>
<hole x="6.8" y="-5.65" drill="1.1"/>
<hole x="-6.8" y="-5.65" drill="1.1"/>
<wire x1="7.5" y1="7.22" x2="7.5" y2="6.47" width="0.127" layer="21"/>
<wire x1="7.5" y1="6.47" x2="7.5" y2="-8.38" width="0.127" layer="21"/>
<wire x1="-7.5" y1="7.22" x2="-7.5" y2="6.47" width="0.127" layer="21"/>
<wire x1="-7.5" y1="6.47" x2="-7.5" y2="-8.38" width="0.127" layer="21"/>
<wire x1="-7.5" y1="-8.38" x2="7.5" y2="-8.38" width="0.127" layer="21"/>
<wire x1="-7.5" y1="7.22" x2="7.5" y2="7.22" width="0.127" layer="21"/>
<wire x1="7.5" y1="7.22" x2="7.5" y2="6.47" width="0.127" layer="51"/>
<wire x1="7.5" y1="6.47" x2="7.5" y2="-8.38" width="0.127" layer="51"/>
<wire x1="-7.5" y1="7.22" x2="-7.5" y2="6.47" width="0.127" layer="51"/>
<wire x1="-7.5" y1="6.47" x2="-7.5" y2="-8.38" width="0.127" layer="51"/>
<wire x1="-7.5" y1="-8.38" x2="7.5" y2="-8.38" width="0.127" layer="51"/>
<wire x1="-7.5" y1="7.22" x2="7.5" y2="7.22" width="0.127" layer="51"/>
<wire x1="-7.5" y1="6.47" x2="7.5" y2="6.47" width="0.127" layer="21"/>
<wire x1="-7.5" y1="6.47" x2="7.5" y2="6.47" width="0.127" layer="51"/>
<text x="1.8" y="-10" size="1.27" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="-6.45" y="-10" size="1.27" layer="27" font="vector" ratio="15">&gt;VALUE</text>
</package>
<package name="SOT95P240X110-3N">
<smd name="3" x="0" y="-1.15" dx="1.05" dy="0.65" layer="1" rot="R90"/>
<smd name="2" x="-1.15" y="1.15" dx="1.05" dy="0.65" layer="1" rot="R90"/>
<smd name="1" x="1.15" y="1.15" dx="1.05" dy="0.65" layer="1" rot="R90"/>
<wire x1="-1.5" y1="0.44" x2="-1.5" y2="-0.65" width="0.127" layer="21"/>
<wire x1="-1.5" y1="-0.65" x2="-0.51" y2="-0.65" width="0.127" layer="21"/>
<wire x1="0.51" y1="-0.65" x2="1.5" y2="-0.65" width="0.127" layer="21"/>
<wire x1="1.5" y1="-0.65" x2="1.5" y2="0.44" width="0.127" layer="21"/>
<wire x1="-0.64" y1="0.65" x2="0.64" y2="0.65" width="0.127" layer="21"/>
<wire x1="-1.5" y1="0.44" x2="-1.5" y2="-0.65" width="0.127" layer="51"/>
<wire x1="-1.5" y1="-0.65" x2="-0.51" y2="-0.65" width="0.127" layer="51"/>
<wire x1="1.5" y1="0.44" x2="1.5" y2="-0.65" width="0.127" layer="51"/>
<wire x1="1.5" y1="-0.65" x2="0.51" y2="-0.65" width="0.127" layer="51"/>
<wire x1="-0.64" y1="0.65" x2="0.64" y2="0.65" width="0.127" layer="51"/>
<text x="-2" y="2" size="1.27" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="-2" y="4" size="1.27" layer="27" font="vector" ratio="15">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="CAPACITOR">
<wire x1="-0.635" y1="-1.016" x2="-0.635" y2="0" width="0.254" layer="94"/>
<wire x1="-0.635" y1="0" x2="-0.635" y2="1.016" width="0.254" layer="94"/>
<wire x1="0.635" y1="1.016" x2="0.635" y2="0" width="0.254" layer="94"/>
<wire x1="0.635" y1="0" x2="0.635" y2="-1.016" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-0.635" y2="0" width="0.1524" layer="94"/>
<wire x1="0.635" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<text x="-1.27" y="1.27" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="INDUCTOR">
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="-1.27" y1="0" x2="0" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="0" y1="0" x2="1.27" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.254" layer="94" curve="-180"/>
<text x="-2.54" y="1.27" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="RESISTER">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.2032" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.2032" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.2032" layer="94"/>
<text x="-1.27" y="1.4986" size="1.27" layer="95">&gt;NAME</text>
<text x="-1.27" y="-3.302" size="1.27" layer="96" distance="49">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="SWITCH">
<pin name="1" x="-5.08" y="0" visible="pad" length="short"/>
<pin name="2" x="5.08" y="0" visible="pad" length="short" rot="R180"/>
<wire x1="-2.54" y1="0" x2="-1.016" y2="0" width="0.254" layer="94"/>
<wire x1="-1.016" y1="0" x2="0.508" y2="1.524" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="1.016" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="2.54" size="1.016" layer="95" font="vector" ratio="15">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.016" layer="96" font="vector" ratio="15">&gt;VALUE</text>
</symbol>
<symbol name="ATMEL_ATSAMD51J_100">
<pin name="1" x="-5.08" y="-10.16" visible="pad" length="middle"/>
<pin name="2" x="-5.08" y="-12.7" visible="pad" length="middle"/>
<pin name="3" x="-5.08" y="-15.24" visible="pad" length="middle"/>
<pin name="4" x="-5.08" y="-17.78" visible="pad" length="middle"/>
<pin name="5" x="-5.08" y="-20.32" visible="pad" length="middle"/>
<pin name="6" x="-5.08" y="-22.86" visible="pad" length="middle"/>
<pin name="7" x="-5.08" y="-25.4" visible="pad" length="middle"/>
<pin name="8" x="-5.08" y="-27.94" visible="pad" length="middle"/>
<pin name="9" x="-5.08" y="-30.48" visible="pad" length="middle"/>
<pin name="10" x="-5.08" y="-33.02" visible="pad" length="middle"/>
<pin name="11" x="-5.08" y="-35.56" visible="pad" length="middle"/>
<pin name="12" x="-5.08" y="-38.1" visible="pad" length="middle"/>
<pin name="13" x="-5.08" y="-40.64" visible="pad" length="middle"/>
<pin name="14" x="-5.08" y="-43.18" visible="pad" length="middle"/>
<pin name="15" x="-5.08" y="-45.72" visible="pad" length="middle"/>
<pin name="16" x="-5.08" y="-48.26" visible="pad" length="middle"/>
<pin name="17" x="-5.08" y="-50.8" visible="pad" length="middle"/>
<pin name="18" x="-5.08" y="-53.34" visible="pad" length="middle"/>
<pin name="19" x="-5.08" y="-55.88" visible="pad" length="middle"/>
<pin name="20" x="-5.08" y="-58.42" visible="pad" length="middle"/>
<pin name="21" x="-5.08" y="-60.96" visible="pad" length="middle"/>
<pin name="22" x="-5.08" y="-63.5" visible="pad" length="middle"/>
<pin name="23" x="-5.08" y="-66.04" visible="pad" length="middle"/>
<pin name="24" x="-5.08" y="-68.58" visible="pad" length="middle"/>
<pin name="25" x="-5.08" y="-71.12" visible="pad" length="middle"/>
<text x="1.016" y="-10.16" size="1.016" layer="94" ratio="15">PA00</text>
<text x="1.016" y="-12.954" size="1.016" layer="94" ratio="15">PA01</text>
<text x="1.016" y="-15.24" size="1.016" layer="94" ratio="15">PC00</text>
<text x="1.016" y="-17.78" size="1.016" layer="94" ratio="15">PC01</text>
<text x="1.016" y="-20.32" size="1.016" layer="94" ratio="15">PC02</text>
<text x="1.016" y="-22.86" size="1.016" layer="94" ratio="15">PC03</text>
<text x="1.016" y="-25.4" size="1.016" layer="94" ratio="15">PA02</text>
<text x="1.016" y="-27.94" size="1.016" layer="94" ratio="15">PA03</text>
<text x="1.016" y="-30.48" size="1.016" layer="94" ratio="15">PB04</text>
<text x="1.016" y="-33.02" size="1.016" layer="94" ratio="15">PB05</text>
<text x="1.016" y="-35.56" size="1.016" layer="94" ratio="15">GNDANA</text>
<text x="1.016" y="-38.1" size="1.016" layer="94" ratio="15">VDDANA</text>
<text x="1.016" y="-40.64" size="1.016" layer="94" ratio="15">PB06</text>
<text x="1.016" y="-43.18" size="1.016" layer="94" ratio="15">PB07</text>
<text x="1.016" y="-45.72" size="1.016" layer="94" ratio="15">PB08</text>
<text x="1.016" y="-48.26" size="1.016" layer="94" ratio="15">PB09</text>
<text x="1.016" y="-50.8" size="1.016" layer="94" ratio="15">PA04</text>
<text x="1.016" y="-53.34" size="1.016" layer="94" ratio="15">PA05</text>
<text x="1.016" y="-55.88" size="1.016" layer="94" ratio="15">PA06</text>
<text x="1.016" y="-58.42" size="1.016" layer="94" ratio="15">PA07</text>
<text x="1.016" y="-60.96" size="1.016" layer="94" ratio="15">PC04</text>
<text x="1.016" y="-63.5" size="1.016" layer="94" ratio="15">PC06</text>
<text x="1.016" y="-66.04" size="1.016" layer="94" ratio="15">PC07</text>
<text x="1.016" y="-68.58" size="1.016" layer="94" ratio="15">GND</text>
<text x="1.016" y="-71.12" size="1.016" layer="94" ratio="15">VDDIOB</text>
<pin name="26" x="10.16" y="-86.36" visible="pad" length="middle" rot="R90"/>
<pin name="27" x="12.7" y="-86.36" visible="pad" length="middle" rot="R90"/>
<pin name="28" x="15.24" y="-86.36" visible="pad" length="middle" rot="R90"/>
<pin name="29" x="17.78" y="-86.36" visible="pad" length="middle" rot="R90"/>
<pin name="30" x="20.32" y="-86.36" visible="pad" length="middle" rot="R90"/>
<pin name="31" x="22.86" y="-86.36" visible="pad" length="middle" rot="R90"/>
<pin name="32" x="25.4" y="-86.36" visible="pad" length="middle" rot="R90"/>
<pin name="33" x="27.94" y="-86.36" visible="pad" length="middle" rot="R90"/>
<pin name="34" x="30.48" y="-86.36" visible="pad" length="middle" rot="R90"/>
<pin name="35" x="33.02" y="-86.36" visible="pad" length="middle" rot="R90"/>
<pin name="36" x="35.56" y="-86.36" visible="pad" length="middle" rot="R90"/>
<pin name="37" x="38.1" y="-86.36" visible="pad" length="middle" rot="R90"/>
<pin name="38" x="40.64" y="-86.36" visible="pad" length="middle" rot="R90"/>
<pin name="39" x="43.18" y="-86.36" visible="pad" length="middle" rot="R90"/>
<pin name="40" x="45.72" y="-86.36" visible="pad" length="middle" rot="R90"/>
<pin name="41" x="48.26" y="-86.36" visible="pad" length="middle" rot="R90"/>
<pin name="42" x="50.8" y="-86.36" visible="pad" length="middle" rot="R90"/>
<pin name="43" x="53.34" y="-86.36" visible="pad" length="middle" rot="R90"/>
<pin name="44" x="55.88" y="-86.36" visible="pad" length="middle" rot="R90"/>
<pin name="45" x="58.42" y="-86.36" visible="pad" length="middle" rot="R90"/>
<pin name="46" x="60.96" y="-86.36" visible="pad" length="middle" rot="R90"/>
<pin name="47" x="63.5" y="-86.36" visible="pad" length="middle" rot="R90"/>
<pin name="48" x="66.04" y="-86.36" visible="pad" length="middle" rot="R90"/>
<pin name="49" x="68.58" y="-86.36" visible="pad" length="middle" rot="R90"/>
<pin name="50" x="71.12" y="-86.36" visible="pad" length="middle" rot="R90"/>
<text x="10.16" y="-80.264" size="1.016" layer="94" ratio="15" rot="R90">PA08</text>
<text x="12.954" y="-80.264" size="1.016" layer="94" ratio="15" rot="R90">PA09</text>
<text x="15.24" y="-80.264" size="1.016" layer="94" ratio="15" rot="R90">PA10</text>
<text x="17.78" y="-80.264" size="1.016" layer="94" ratio="15" rot="R90">PA11</text>
<text x="20.32" y="-80.264" size="1.016" layer="94" ratio="15" rot="R90">VDDIOB</text>
<text x="22.86" y="-80.264" size="1.016" layer="94" ratio="15" rot="R90">GND</text>
<text x="25.4" y="-80.264" size="1.016" layer="94" ratio="15" rot="R90">PB10</text>
<text x="27.94" y="-80.264" size="1.016" layer="94" ratio="15" rot="R90">PB11</text>
<text x="30.48" y="-80.264" size="1.016" layer="94" ratio="15" rot="R90">PB12</text>
<text x="33.02" y="-80.264" size="1.016" layer="94" ratio="15" rot="R90">PB13</text>
<text x="35.56" y="-80.264" size="1.016" layer="94" ratio="15" rot="R90">PB14</text>
<text x="38.1" y="-80.264" size="1.016" layer="94" ratio="15" rot="R90">PB15</text>
<text x="40.64" y="-80.264" size="1.016" layer="94" ratio="15" rot="R90">GND</text>
<text x="43.18" y="-80.264" size="1.016" layer="94" ratio="15" rot="R90">VDDIO</text>
<text x="45.72" y="-80.264" size="1.016" layer="94" ratio="15" rot="R90">PC10</text>
<text x="48.26" y="-80.264" size="1.016" layer="94" ratio="15" rot="R90">PC11</text>
<text x="50.8" y="-80.264" size="1.016" layer="94" ratio="15" rot="R90">PC12</text>
<text x="53.34" y="-80.264" size="1.016" layer="94" ratio="15" rot="R90">PC13</text>
<text x="55.88" y="-80.264" size="1.016" layer="94" ratio="15" rot="R90">PC14</text>
<text x="58.42" y="-80.264" size="1.016" layer="94" ratio="15" rot="R90">PC15</text>
<text x="60.96" y="-80.264" size="1.016" layer="94" ratio="15" rot="R90">PA12</text>
<text x="63.5" y="-80.264" size="1.016" layer="94" ratio="15" rot="R90">PA13</text>
<text x="66.04" y="-80.264" size="1.016" layer="94" ratio="15" rot="R90">PA14</text>
<text x="68.58" y="-80.264" size="1.016" layer="94" ratio="15" rot="R90">PA15</text>
<text x="71.12" y="-80.264" size="1.016" layer="94" ratio="15" rot="R90">GND</text>
<pin name="51" x="86.36" y="-71.12" visible="pad" length="middle" rot="R180"/>
<pin name="52" x="86.36" y="-68.58" visible="pad" length="middle" rot="R180"/>
<pin name="53" x="86.36" y="-66.04" visible="pad" length="middle" rot="R180"/>
<pin name="54" x="86.36" y="-63.5" visible="pad" length="middle" rot="R180"/>
<pin name="55" x="86.36" y="-60.96" visible="pad" length="middle" rot="R180"/>
<pin name="56" x="86.36" y="-58.42" visible="pad" length="middle" rot="R180"/>
<pin name="57" x="86.36" y="-55.88" visible="pad" length="middle" rot="R180"/>
<pin name="58" x="86.36" y="-53.34" visible="pad" length="middle" rot="R180"/>
<pin name="59" x="86.36" y="-50.8" visible="pad" length="middle" rot="R180"/>
<pin name="60" x="86.36" y="-48.26" visible="pad" length="middle" rot="R180"/>
<pin name="61" x="86.36" y="-45.72" visible="pad" length="middle" rot="R180"/>
<pin name="62" x="86.36" y="-43.18" visible="pad" length="middle" rot="R180"/>
<pin name="63" x="86.36" y="-40.64" visible="pad" length="middle" rot="R180"/>
<pin name="64" x="86.36" y="-38.1" visible="pad" length="middle" rot="R180"/>
<pin name="65" x="86.36" y="-35.56" visible="pad" length="middle" rot="R180"/>
<pin name="66" x="86.36" y="-33.02" visible="pad" length="middle" rot="R180"/>
<pin name="67" x="86.36" y="-30.48" visible="pad" length="middle" rot="R180"/>
<pin name="68" x="86.36" y="-27.94" visible="pad" length="middle" rot="R180"/>
<pin name="69" x="86.36" y="-25.4" visible="pad" length="middle" rot="R180"/>
<pin name="70" x="86.36" y="-22.86" visible="pad" length="middle" rot="R180"/>
<pin name="71" x="86.36" y="-20.32" visible="pad" length="middle" rot="R180"/>
<pin name="72" x="86.36" y="-17.78" visible="pad" length="middle" rot="R180"/>
<pin name="73" x="86.36" y="-15.24" visible="pad" length="middle" rot="R180"/>
<pin name="74" x="86.36" y="-12.7" visible="pad" length="middle" rot="R180"/>
<pin name="75" x="86.36" y="-10.16" visible="pad" length="middle" rot="R180"/>
<text x="80.264" y="-71.12" size="1.016" layer="94" ratio="15" rot="R180">VDDIO</text>
<text x="80.264" y="-68.326" size="1.016" layer="94" ratio="15" rot="R180">PA16</text>
<text x="80.264" y="-66.04" size="1.016" layer="94" ratio="15" rot="R180">PA17</text>
<text x="80.264" y="-63.5" size="1.016" layer="94" ratio="15" rot="R180">PA18</text>
<text x="80.264" y="-60.96" size="1.016" layer="94" ratio="15" rot="R180">PA19</text>
<text x="80.264" y="-58.42" size="1.016" layer="94" ratio="15" rot="R180">PC16</text>
<text x="80.264" y="-55.88" size="1.016" layer="94" ratio="15" rot="R180">PC17</text>
<text x="80.264" y="-53.34" size="1.016" layer="94" ratio="15" rot="R180">PC18</text>
<text x="80.264" y="-50.8" size="1.016" layer="94" ratio="15" rot="R180">PC19</text>
<text x="80.264" y="-48.26" size="1.016" layer="94" ratio="15" rot="R180">PC20</text>
<text x="80.264" y="-45.72" size="1.016" layer="94" ratio="15" rot="R180">PC21</text>
<text x="80.264" y="-43.18" size="1.016" layer="94" ratio="15" rot="R180">GND</text>
<text x="80.264" y="-40.64" size="1.016" layer="94" ratio="15" rot="R180">VDDIO</text>
<text x="80.264" y="-38.1" size="1.016" layer="94" ratio="15" rot="R180">PB16</text>
<text x="80.264" y="-35.56" size="1.016" layer="94" ratio="15" rot="R180">PB17</text>
<text x="80.264" y="-33.02" size="1.016" layer="94" ratio="15" rot="R180">PB18</text>
<text x="80.264" y="-30.48" size="1.016" layer="94" ratio="15" rot="R180">PB19</text>
<text x="80.264" y="-27.94" size="1.016" layer="94" ratio="15" rot="R180">PB20</text>
<text x="80.264" y="-25.4" size="1.016" layer="94" ratio="15" rot="R180">PB21</text>
<text x="80.264" y="-22.86" size="1.016" layer="94" ratio="15" rot="R180">PA20</text>
<text x="80.264" y="-20.32" size="1.016" layer="94" ratio="15" rot="R180">PA21</text>
<text x="80.264" y="-17.78" size="1.016" layer="94" ratio="15" rot="R180">PA22</text>
<text x="80.264" y="-15.24" size="1.016" layer="94" ratio="15" rot="R180">PA23</text>
<text x="80.264" y="-12.7" size="1.016" layer="94" ratio="15" rot="R180">PA24</text>
<text x="80.264" y="-10.16" size="1.016" layer="94" ratio="15" rot="R180">PA25</text>
<pin name="76" x="71.12" y="5.08" visible="pad" length="middle" rot="R270"/>
<pin name="77" x="68.58" y="5.08" visible="pad" length="middle" rot="R270"/>
<pin name="78" x="66.04" y="5.08" visible="pad" length="middle" rot="R270"/>
<pin name="79" x="63.5" y="5.08" visible="pad" length="middle" rot="R270"/>
<pin name="80" x="60.96" y="5.08" visible="pad" length="middle" rot="R270"/>
<pin name="81" x="58.42" y="5.08" visible="pad" length="middle" rot="R270"/>
<pin name="82" x="55.88" y="5.08" visible="pad" length="middle" rot="R270"/>
<pin name="83" x="53.34" y="5.08" visible="pad" length="middle" rot="R270"/>
<pin name="84" x="50.8" y="5.08" visible="pad" length="middle" rot="R270"/>
<pin name="85" x="48.26" y="5.08" visible="pad" length="middle" rot="R270"/>
<pin name="86" x="45.72" y="5.08" visible="pad" length="middle" rot="R270"/>
<pin name="87" x="43.18" y="5.08" visible="pad" length="middle" rot="R270"/>
<pin name="88" x="40.64" y="5.08" visible="pad" length="middle" rot="R270"/>
<pin name="89" x="38.1" y="5.08" visible="pad" length="middle" rot="R270"/>
<pin name="90" x="35.56" y="5.08" visible="pad" length="middle" rot="R270"/>
<pin name="91" x="33.02" y="5.08" visible="pad" length="middle" rot="R270"/>
<pin name="92" x="30.48" y="5.08" visible="pad" length="middle" rot="R270"/>
<pin name="93" x="27.94" y="5.08" visible="pad" length="middle" rot="R270"/>
<pin name="94" x="25.4" y="5.08" visible="pad" length="middle" rot="R270"/>
<pin name="95" x="22.86" y="5.08" visible="pad" length="middle" rot="R270"/>
<pin name="96" x="20.32" y="5.08" visible="pad" length="middle" rot="R270"/>
<pin name="97" x="17.78" y="5.08" visible="pad" length="middle" rot="R270"/>
<pin name="98" x="15.24" y="5.08" visible="pad" length="middle" rot="R270"/>
<pin name="99" x="12.7" y="5.08" visible="pad" length="middle" rot="R270"/>
<pin name="100" x="10.16" y="5.08" visible="pad" length="middle" rot="R270"/>
<text x="71.12" y="-1.016" size="1.016" layer="94" ratio="15" rot="R270">GND</text>
<text x="68.326" y="-1.016" size="1.016" layer="94" ratio="15" rot="R270">VDDIO</text>
<text x="66.04" y="-1.016" size="1.016" layer="94" ratio="15" rot="R270">PB22</text>
<text x="63.5" y="-1.016" size="1.016" layer="94" ratio="15" rot="R270">PB23</text>
<text x="60.96" y="-1.016" size="1.016" layer="94" ratio="15" rot="R270">PB24</text>
<text x="58.42" y="-1.016" size="1.016" layer="94" ratio="15" rot="R270">PB25</text>
<text x="55.88" y="-1.016" size="1.016" layer="94" ratio="15" rot="R270">PC24</text>
<text x="53.34" y="-1.016" size="1.016" layer="94" ratio="15" rot="R270">PC25</text>
<text x="50.8" y="-1.016" size="1.016" layer="94" ratio="15" rot="R270">PC26</text>
<text x="48.26" y="-1.016" size="1.016" layer="94" ratio="15" rot="R270">PC27</text>
<text x="45.72" y="-1.016" size="1.016" layer="94" ratio="15" rot="R270">PC28</text>
<text x="43.18" y="-1.016" size="1.016" layer="94" ratio="15" rot="R270">PA27</text>
<text x="40.64" y="-1.016" size="1.016" layer="94" ratio="15" rot="R270">RESETN</text>
<text x="38.1" y="-1.016" size="1.016" layer="94" ratio="15" rot="R270">VDDCORE</text>
<text x="35.56" y="-1.016" size="1.016" layer="94" ratio="15" rot="R270">GND</text>
<text x="33.02" y="-1.016" size="1.016" layer="94" ratio="15" rot="R270">VSW</text>
<text x="30.48" y="-1.016" size="1.016" layer="94" ratio="15" rot="R270">VDDIO</text>
<text x="27.94" y="-1.016" size="1.016" layer="94" ratio="15" rot="R270">PA30</text>
<text x="25.4" y="-1.016" size="1.016" layer="94" ratio="15" rot="R270">PA31</text>
<text x="22.86" y="-1.016" size="1.016" layer="94" ratio="15" rot="R270">PB30</text>
<text x="20.32" y="-1.016" size="1.016" layer="94" ratio="15" rot="R270">PB31</text>
<text x="17.78" y="-1.016" size="1.016" layer="94" ratio="15" rot="R270">PB00</text>
<text x="15.24" y="-1.016" size="1.016" layer="94" ratio="15" rot="R270">PB01</text>
<text x="12.7" y="-1.016" size="1.016" layer="94" ratio="15" rot="R270">PB02</text>
<text x="10.16" y="-1.016" size="1.016" layer="94" ratio="15" rot="R270">PB03</text>
<wire x1="0" y1="-5.08" x2="0" y2="-81.28" width="0.254" layer="94"/>
<wire x1="0" y1="-81.28" x2="81.28" y2="-81.28" width="0.254" layer="94"/>
<wire x1="81.28" y1="-81.28" x2="81.28" y2="0" width="0.254" layer="94"/>
<wire x1="81.28" y1="0" x2="5.08" y2="0" width="0.254" layer="94"/>
<wire x1="5.08" y1="0" x2="0" y2="-5.08" width="0.254" layer="94"/>
<text x="0" y="15.24" size="1.778" layer="95">&gt;NAME</text>
<text x="0" y="12.7" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="CRYSTAL_4PIN">
<wire x1="1.016" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.016" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.381" y1="1.524" x2="-0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="-0.381" y1="-1.524" x2="0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="-1.524" x2="0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="1.524" x2="-0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="1.016" y1="1.778" x2="1.016" y2="0" width="0.254" layer="94"/>
<wire x1="1.016" y1="0" x2="1.016" y2="-1.778" width="0.254" layer="94"/>
<wire x1="-1.016" y1="1.778" x2="-1.016" y2="0" width="0.254" layer="94"/>
<wire x1="-1.016" y1="0" x2="-1.016" y2="-1.778" width="0.254" layer="94"/>
<text x="-2.54" y="2.54" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="-2.54" y="3.81" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="1"/>
<pin name="3" x="0" y="-5.08" visible="pad" length="short" direction="pas" swaplevel="1" rot="R90"/>
<wire x1="-1.524" y1="-2.032" x2="-1.524" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-1.524" y1="-2.54" x2="1.524" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.524" y1="-2.54" x2="1.524" y2="-2.032" width="0.254" layer="94"/>
</symbol>
<symbol name="I2C_ACCR_ISL33003">
<pin name="1" x="22.86" y="-5.08" visible="pad" length="middle" rot="R180"/>
<pin name="2" x="22.86" y="-10.16" visible="pad" length="middle" rot="R180"/>
<pin name="3" x="-5.08" y="-10.16" visible="pad" length="middle"/>
<pin name="4" x="-5.08" y="-12.7" visible="pad" length="middle"/>
<pin name="5" x="22.86" y="-12.7" visible="pad" length="middle" rot="R180"/>
<pin name="6" x="-5.08" y="-7.62" visible="pad" length="middle"/>
<pin name="7" x="22.86" y="-7.62" visible="pad" length="middle" rot="R180"/>
<pin name="8" x="-5.08" y="-5.08" visible="pad" length="middle"/>
<wire x1="0" y1="0" x2="0" y2="-15.24" width="0.1524" layer="94"/>
<wire x1="0" y1="-15.24" x2="17.78" y2="-15.24" width="0.1524" layer="94"/>
<wire x1="17.78" y1="-15.24" x2="17.78" y2="0" width="0.1524" layer="94"/>
<wire x1="17.78" y1="0" x2="0" y2="0" width="0.1524" layer="94"/>
<text x="17.272" y="-5.08" size="1.27" layer="94" align="bottom-right">VCC2</text>
<text x="17.272" y="-10.16" size="1.27" layer="94" align="bottom-right">SCLOUT</text>
<text x="0.508" y="-10.16" size="1.27" layer="94">SCLIN</text>
<text x="0.508" y="-12.7" size="1.27" layer="94">GND</text>
<text x="17.272" y="-12.7" size="1.27" layer="94" align="bottom-right">EN</text>
<text x="0.508" y="-7.62" size="1.27" layer="94">SDAIN</text>
<text x="17.272" y="-7.62" size="1.27" layer="94" align="bottom-right">SDAOUT</text>
<text x="0.508" y="-5.08" size="1.27" layer="94">VCC1</text>
<text x="0" y="5.08" size="1.778" layer="95">&gt;NAME</text>
<text x="0" y="2.54" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="TP">
<wire x1="0.762" y1="1.778" x2="0" y2="0.254" width="0.254" layer="94"/>
<wire x1="0" y1="0.254" x2="-0.762" y2="1.778" width="0.254" layer="94"/>
<wire x1="-0.762" y1="1.778" x2="0.762" y2="1.778" width="0.254" layer="94" curve="-180"/>
<text x="-1.27" y="3.81" size="1.778" layer="95">&gt;NAME</text>
<pin name="PP" x="0" y="0" visible="off" length="short" direction="in" rot="R90"/>
</symbol>
<symbol name="NC">
<wire x1="-0.635" y1="0.635" x2="0.635" y2="-0.635" width="0.254" layer="94"/>
<wire x1="0.635" y1="0.635" x2="-0.635" y2="-0.635" width="0.254" layer="94"/>
</symbol>
<symbol name="AMIS_30624">
<pin name="1" x="-5.08" y="-7.62" visible="pad" length="middle"/>
<pin name="2" x="-5.08" y="-10.16" visible="pad" length="middle"/>
<pin name="3" x="-5.08" y="-12.7" visible="pad" length="middle"/>
<pin name="4" x="-5.08" y="-15.24" visible="pad" length="middle"/>
<pin name="5" x="-5.08" y="-17.78" visible="pad" length="middle"/>
<pin name="6" x="-5.08" y="-20.32" visible="pad" length="middle"/>
<pin name="7" x="-5.08" y="-22.86" visible="pad" length="middle"/>
<pin name="8" x="-5.08" y="-25.4" visible="pad" length="middle"/>
<pin name="9" x="-5.08" y="-27.94" visible="pad" length="middle"/>
<pin name="10" x="-5.08" y="-30.48" visible="pad" length="middle"/>
<pin name="11" x="-5.08" y="-33.02" visible="pad" length="middle"/>
<pin name="12" x="-5.08" y="-35.56" visible="pad" length="middle"/>
<pin name="13" x="-5.08" y="-38.1" visible="pad" length="middle"/>
<pin name="14" x="-5.08" y="-40.64" visible="pad" length="middle"/>
<pin name="15" x="-5.08" y="-43.18" visible="pad" length="middle"/>
<pin name="16" x="-5.08" y="-45.72" visible="pad" length="middle"/>
<pin name="17" x="30.48" y="-45.72" visible="pad" length="middle" rot="R180"/>
<pin name="18" x="30.48" y="-43.18" visible="pad" length="middle" rot="R180"/>
<pin name="19" x="30.48" y="-40.64" visible="pad" length="middle" rot="R180"/>
<pin name="20" x="30.48" y="-38.1" visible="pad" length="middle" rot="R180"/>
<pin name="21" x="30.48" y="-35.56" visible="pad" length="middle" rot="R180"/>
<pin name="22" x="30.48" y="-33.02" visible="pad" length="middle" rot="R180"/>
<pin name="23" x="30.48" y="-30.48" visible="pad" length="middle" rot="R180"/>
<pin name="24" x="30.48" y="-27.94" visible="pad" length="middle" rot="R180"/>
<pin name="25" x="30.48" y="-25.4" visible="pad" length="middle" rot="R180"/>
<pin name="26" x="30.48" y="-22.86" visible="pad" length="middle" rot="R180"/>
<pin name="27" x="30.48" y="-20.32" visible="pad" length="middle" rot="R180"/>
<pin name="28" x="30.48" y="-17.78" visible="pad" length="middle" rot="R180"/>
<pin name="29" x="30.48" y="-15.24" visible="pad" length="middle" rot="R180"/>
<pin name="30" x="30.48" y="-12.7" visible="pad" length="middle" rot="R180"/>
<pin name="31" x="30.48" y="-10.16" visible="pad" length="middle" rot="R180"/>
<pin name="32" x="30.48" y="-7.62" visible="pad" length="middle" rot="R180"/>
<text x="1.27" y="-7.62" size="1.778" layer="94">XP</text>
<text x="1.27" y="-10.16" size="1.778" layer="94">XP</text>
<text x="1.27" y="-12.7" size="1.778" layer="94">VBB</text>
<text x="1.27" y="-15.24" size="1.778" layer="94">VBB</text>
<text x="1.27" y="-17.78" size="1.778" layer="94">VBB</text>
<text x="1.27" y="-20.32" size="1.778" layer="94">SWI</text>
<text x="1.27" y="-22.86" size="1.778" layer="94">NC</text>
<text x="1.27" y="-25.4" size="1.778" layer="94">SDA</text>
<text x="1.27" y="-27.94" size="1.778" layer="94">SCK</text>
<text x="1.27" y="-30.48" size="1.778" layer="94">VDD</text>
<text x="1.27" y="-33.02" size="1.778" layer="94">GND</text>
<text x="1.27" y="-35.56" size="1.778" layer="94">TST1</text>
<text x="1.27" y="-38.1" size="1.778" layer="94">TST2</text>
<text x="1.27" y="-40.64" size="1.778" layer="94">GND</text>
<text x="1.27" y="-43.18" size="1.778" layer="94">HW</text>
<text x="1.27" y="-45.72" size="1.778" layer="94">NC</text>
<text x="24.13" y="-45.72" size="1.778" layer="94" align="bottom-right">CPN</text>
<text x="24.13" y="-43.18" size="1.778" layer="94" align="bottom-right">CPP</text>
<text x="24.13" y="-40.64" size="1.778" layer="94" align="bottom-right">VCP</text>
<text x="24.13" y="-38.1" size="1.778" layer="94" align="bottom-right">VBB</text>
<text x="24.13" y="-35.56" size="1.778" layer="94" align="bottom-right">VBB</text>
<text x="24.13" y="-33.02" size="1.778" layer="94" align="bottom-right">VBB</text>
<text x="24.13" y="-30.48" size="1.778" layer="94" align="bottom-right">YN</text>
<text x="24.13" y="-27.94" size="1.778" layer="94" align="bottom-right">YN</text>
<text x="24.13" y="-25.4" size="1.778" layer="94" align="bottom-right">GND</text>
<text x="24.13" y="-22.86" size="1.778" layer="94" align="bottom-right">GND</text>
<text x="24.13" y="-20.32" size="1.778" layer="94" align="bottom-right">YP</text>
<text x="24.13" y="-17.78" size="1.778" layer="94" align="bottom-right">YP</text>
<text x="24.13" y="-15.24" size="1.778" layer="94" align="bottom-right">XN</text>
<text x="24.13" y="-12.7" size="1.778" layer="94" align="bottom-right">XN</text>
<text x="24.13" y="-10.16" size="1.778" layer="94" align="bottom-right">GND</text>
<text x="24.13" y="-7.62" size="1.778" layer="94" align="bottom-right">GND</text>
<wire x1="0" y1="-2.54" x2="0" y2="-48.26" width="0.254" layer="94"/>
<wire x1="0" y1="-48.26" x2="25.4" y2="-48.26" width="0.254" layer="94"/>
<wire x1="25.4" y1="-48.26" x2="25.4" y2="0" width="0.254" layer="94"/>
<wire x1="25.4" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="0" y2="-2.54" width="0.254" layer="94"/>
<text x="3.81" y="-2.54" size="1.778" layer="94">DC MOTOR DR</text>
<text x="0" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="0" y="5.08" size="1.778" layer="96">&gt;VALUE</text>
<pin name="33" x="30.48" y="-5.08" visible="pad" length="middle" rot="R180"/>
<text x="24.13" y="-5.08" size="1.778" layer="94" align="bottom-right">PAD</text>
</symbol>
<symbol name="MICROCHIP_24/5XC256">
<text x="3.81" y="-10.16" size="1.778" layer="94">!CS</text>
<text x="3.81" y="-15.24" size="1.778" layer="94">SO</text>
<text x="3.81" y="-20.32" size="1.778" layer="94">!WP</text>
<text x="3.81" y="-25.4" size="1.778" layer="94">VSS</text>
<text x="22.86" y="-25.4" size="1.778" layer="94" align="bottom-right">SI</text>
<text x="22.86" y="-20.32" size="1.778" layer="94" align="bottom-right">SCK</text>
<text x="22.86" y="-15.24" size="1.778" layer="94" align="bottom-right">!HOLD</text>
<text x="22.86" y="-10.16" size="1.778" layer="94" align="bottom-right">VCC</text>
<wire x1="0" y1="-2.54" x2="0" y2="-27.94" width="0.254" layer="94"/>
<wire x1="0" y1="-27.94" x2="25.4" y2="-27.94" width="0.254" layer="94"/>
<wire x1="25.4" y1="-27.94" x2="25.4" y2="0" width="0.254" layer="94"/>
<wire x1="25.4" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="0" y2="-2.54" width="0.254" layer="94"/>
<text x="2.54" y="-2.54" size="1.27" layer="94">DFN,PDIP/SOIC, TSSOP</text>
<text x="2.54" y="5.08" size="1.778" layer="95" ratio="15">&gt;NAME</text>
<text x="2.54" y="2.54" size="1.778" layer="96" ratio="15">&gt;VALUE</text>
<pin name="P$1" x="-2.54" y="-10.16" visible="pad" length="short"/>
<pin name="P$2" x="-2.54" y="-15.24" visible="pad" length="short"/>
<pin name="P$3" x="-2.54" y="-20.32" visible="pad" length="short"/>
<pin name="P$4" x="-2.54" y="-25.4" visible="pad" length="short"/>
<pin name="P$5" x="27.94" y="-25.4" visible="pad" length="short" rot="R180"/>
<pin name="P$6" x="27.94" y="-20.32" visible="pad" length="short" rot="R180"/>
<pin name="P$7" x="27.94" y="-15.24" visible="pad" length="short" rot="R180"/>
<pin name="P$8" x="27.94" y="-10.16" visible="pad" length="short" rot="R180"/>
<pin name="P$9" x="27.94" y="-5.08" visible="pad" length="short" rot="R180"/>
<text x="22.86" y="-5.08" size="1.778" layer="94" align="bottom-right">PAD</text>
</symbol>
<symbol name="ZENER">
<wire x1="-1.27" y1="-2.54" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<text x="-7.62" y="1.27" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-5.08" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
<pin name="+" x="-5.08" y="0" visible="pin" length="short" direction="pas"/>
<pin name="-" x="5.08" y="0" visible="pin" length="short" direction="pas" rot="R180"/>
<wire x1="-1.27" y1="0" x2="-2.54" y2="0" width="0.15875" layer="94"/>
<wire x1="1.27" y1="-1.905" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="1.905" width="0.254" layer="94"/>
<wire x1="1.27" y1="-1.905" x2="0.635" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.905" x2="1.905" y2="2.54" width="0.254" layer="94"/>
<wire x1="2.7178" y1="-0.0254" x2="1.4478" y2="-0.0254" width="0.15875" layer="94"/>
</symbol>
<symbol name="NMOS_FET">
<pin name="G" x="-2.54" y="-2.54" visible="pad" length="short"/>
<pin name="S" x="2.54" y="-5.08" visible="pad" length="short" rot="R90"/>
<pin name="D" x="2.54" y="5.08" visible="pad" length="short" rot="R270"/>
<wire x1="0" y1="2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<text x="5.08" y="0" size="1.27" layer="95" ratio="15">&gt;NAME</text>
<text x="5.08" y="-2.54" size="1.27" layer="96" ratio="15">&gt;VALUE</text>
<wire x1="0.508" y1="2.54" x2="0.508" y2="2.032" width="0.254" layer="94"/>
<wire x1="0.508" y1="2.032" x2="0.508" y2="1.524" width="0.254" layer="94"/>
<wire x1="0.508" y1="0.762" x2="0.508" y2="0" width="0.254" layer="94"/>
<wire x1="0.508" y1="0" x2="0.508" y2="-0.762" width="0.254" layer="94"/>
<wire x1="0.508" y1="-1.524" x2="0.508" y2="-2.032" width="0.254" layer="94"/>
<wire x1="0.508" y1="-2.032" x2="0.508" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0.508" y1="2.032" x2="2.54" y2="2.032" width="0.254" layer="94"/>
<wire x1="2.54" y1="2.032" x2="2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="0.508" y1="-2.032" x2="2.54" y2="-2.032" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.032" x2="2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0.508" y1="0" x2="0.762" y2="0" width="0.254" layer="94"/>
<wire x1="0.762" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-2.032" width="0.254" layer="94"/>
<wire x1="0.762" y1="0" x2="1.524" y2="0.254" width="0.254" layer="94"/>
<wire x1="0.762" y1="0" x2="1.524" y2="-0.254" width="0.254" layer="94"/>
<wire x1="1.524" y1="-0.254" x2="1.524" y2="0.254" width="0.254" layer="94"/>
<wire x1="2.54" y1="2.54" x2="4.064" y2="2.54" width="0.254" layer="94"/>
<wire x1="4.064" y1="2.54" x2="4.064" y2="0.508" width="0.254" layer="94"/>
<wire x1="4.064" y1="0.508" x2="3.556" y2="0.508" width="0.254" layer="94"/>
<wire x1="4.572" y1="0.508" x2="4.064" y2="0.508" width="0.254" layer="94"/>
<wire x1="4.064" y1="-0.508" x2="4.572" y2="-0.508" width="0.254" layer="94"/>
<wire x1="4.572" y1="-0.508" x2="4.064" y2="0.508" width="0.254" layer="94"/>
<wire x1="4.064" y1="0.508" x2="3.556" y2="-0.508" width="0.254" layer="94"/>
<wire x1="3.556" y1="-0.508" x2="4.064" y2="-0.508" width="0.254" layer="94"/>
<wire x1="4.064" y1="-0.508" x2="4.064" y2="-2.54" width="0.254" layer="94"/>
<wire x1="4.064" y1="-2.54" x2="2.54" y2="-2.54" width="0.254" layer="94"/>
</symbol>
<symbol name="CAPACITOR_ELECTRO">
<wire x1="-0.635" y1="-1.27" x2="-0.635" y2="0" width="0.254" layer="94"/>
<wire x1="-0.635" y1="0" x2="-0.635" y2="1.27" width="0.254" layer="94"/>
<wire x1="0.889" y1="1.27" x2="0.381" y2="0" width="0.254" layer="94" curve="50"/>
<wire x1="0.381" y1="0" x2="0.889" y2="-1.27" width="0.254" layer="94" curve="50"/>
<wire x1="-2.54" y1="0" x2="-0.635" y2="0" width="0.1524" layer="94"/>
<wire x1="0.381" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<text x="-5.08" y="2.54" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<text x="-2.032" y="0.254" size="1.27" layer="94" ratio="15">+</text>
</symbol>
<symbol name="PIN">
<pin name="1" x="-5.08" y="0" visible="off"/>
<wire x1="-3.302" y1="0" x2="-1.27" y2="0" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="0" x2="2.54" y2="0" width="0.8128" layer="94"/>
<text x="-3.302" y="0.508" size="1.27" layer="95" ratio="15">&gt;NAME</text>
</symbol>
<symbol name="RS485">
<text x="1.27" y="-7.62" size="1.778" layer="94" ratio="15">R0</text>
<text x="1.27" y="-12.7" size="1.778" layer="94" ratio="15">!RE</text>
<text x="1.27" y="-17.78" size="1.778" layer="94" ratio="15">DE</text>
<text x="1.27" y="-22.86" size="1.778" layer="94" ratio="15">DI</text>
<text x="19.05" y="-22.86" size="1.778" layer="94" ratio="15" align="bottom-right">GND</text>
<text x="19.05" y="-17.78" size="1.778" layer="94" ratio="15" align="bottom-right">A(D0/RI)</text>
<text x="19.05" y="-12.7" size="1.778" layer="94" ratio="15" align="bottom-right">B(!D0!/!RI!)</text>
<text x="19.05" y="-7.62" size="1.778" layer="94" ratio="15" align="bottom-right">VCC</text>
<wire x1="0" y1="-2.54" x2="0" y2="-25.4" width="0.254" layer="94"/>
<wire x1="0" y1="-25.4" x2="20.32" y2="-25.4" width="0.254" layer="94"/>
<wire x1="20.32" y1="-25.4" x2="20.32" y2="0" width="0.254" layer="94"/>
<wire x1="20.32" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="0" y2="-2.54" width="0.254" layer="94"/>
<pin name="1" x="-2.54" y="-7.62" visible="pad" length="short"/>
<pin name="2" x="-2.54" y="-12.7" visible="pad" length="short"/>
<pin name="3" x="-2.54" y="-17.78" visible="pad" length="short"/>
<pin name="4" x="-2.54" y="-22.86" visible="pad" length="short"/>
<pin name="5" x="22.86" y="-22.86" visible="pad" length="short" rot="R180"/>
<pin name="6" x="22.86" y="-17.78" visible="pad" length="short" rot="R180"/>
<pin name="7" x="22.86" y="-12.7" visible="pad" length="short" rot="R180"/>
<pin name="8" x="22.86" y="-7.62" visible="pad" length="short" rot="R180"/>
<text x="6.35" y="-2.54" size="1.778" layer="94" ratio="15">RS485</text>
<text x="0" y="2.54" size="1.778" layer="95" ratio="15">&gt;NAME</text>
<text x="0" y="-27.94" size="1.778" layer="96" ratio="15">&gt;VALUE</text>
</symbol>
<symbol name="DIODE">
<wire x1="-1.27" y1="-2.54" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="2.54" x2="1.27" y2="0" width="0.254" layer="94"/>
<text x="-7.62" y="1.27" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-5.08" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
<pin name="+" x="-5.08" y="0" visible="off" length="short" direction="pas"/>
<pin name="-" x="5.08" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.15875" layer="94"/>
<wire x1="-1.27" y1="0" x2="-2.54" y2="0" width="0.15875" layer="94"/>
</symbol>
<symbol name="PMOS_FET-1">
<pin name="G" x="-2.54" y="2.54" visible="pad" length="short"/>
<pin name="S" x="2.54" y="5.08" visible="pad" length="short" rot="R270"/>
<pin name="D" x="2.54" y="-5.08" visible="pad" length="short" rot="R90"/>
<wire x1="0" y1="-2.54" x2="0" y2="2.54" width="0.254" layer="94"/>
<text x="5.08" y="0" size="1.27" layer="95" ratio="15" rot="MR180">&gt;NAME</text>
<text x="5.08" y="2.54" size="1.27" layer="96" ratio="15" rot="MR180">&gt;VALUE</text>
<wire x1="0.508" y1="-2.54" x2="0.508" y2="-2.032" width="0.254" layer="94"/>
<wire x1="0.508" y1="-2.032" x2="0.508" y2="-1.524" width="0.254" layer="94"/>
<wire x1="0.508" y1="-0.762" x2="0.508" y2="0" width="0.254" layer="94"/>
<wire x1="0.508" y1="0" x2="0.508" y2="0.762" width="0.254" layer="94"/>
<wire x1="0.508" y1="1.524" x2="0.508" y2="2.032" width="0.254" layer="94"/>
<wire x1="0.508" y1="2.032" x2="0.508" y2="2.54" width="0.254" layer="94"/>
<wire x1="0.508" y1="-2.032" x2="2.54" y2="-2.032" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.032" x2="2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0.508" y1="2.032" x2="2.54" y2="2.032" width="0.254" layer="94"/>
<wire x1="2.54" y1="2.032" x2="2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="0.508" y1="0" x2="2.286" y2="0" width="0.254" layer="94"/>
<wire x1="2.286" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="2.032" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="4.064" y2="-2.54" width="0.254" layer="94"/>
<wire x1="4.064" y1="-2.54" x2="4.064" y2="-0.508" width="0.254" layer="94"/>
<wire x1="4.064" y1="-0.508" x2="3.556" y2="-0.508" width="0.254" layer="94"/>
<wire x1="4.572" y1="-0.508" x2="4.064" y2="-0.508" width="0.254" layer="94"/>
<wire x1="4.064" y1="0.508" x2="4.572" y2="0.508" width="0.254" layer="94"/>
<wire x1="3.556" y1="0.508" x2="4.064" y2="0.508" width="0.254" layer="94"/>
<wire x1="4.064" y1="0.508" x2="4.064" y2="2.54" width="0.254" layer="94"/>
<wire x1="4.064" y1="2.54" x2="2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="3.556" y1="-0.508" x2="4.064" y2="0.508" width="0.254" layer="94"/>
<wire x1="4.064" y1="0.508" x2="4.572" y2="-0.508" width="0.254" layer="94"/>
<wire x1="2.286" y1="0" x2="1.524" y2="-0.254" width="0.254" layer="94"/>
<wire x1="1.524" y1="-0.254" x2="1.524" y2="0.254" width="0.254" layer="94"/>
<wire x1="1.524" y1="0.254" x2="2.286" y2="0" width="0.254" layer="94"/>
</symbol>
<symbol name="LM22672MRX-ADJ">
<pin name="BOOT" x="25.4" y="15.24" length="middle" direction="pas" rot="R180"/>
<pin name="SS" x="-5.08" y="2.54" length="middle" direction="in"/>
<pin name="RT/SYNC" x="-5.08" y="7.62" length="middle" direction="in"/>
<pin name="FB" x="25.4" y="10.16" length="middle" direction="in" rot="R180"/>
<pin name="EN" x="-5.08" y="12.7" length="middle" direction="in"/>
<pin name="GND" x="25.4" y="5.08" length="middle" direction="pwr" rot="R180"/>
<pin name="VIN" x="-5.08" y="17.78" length="middle" direction="pwr"/>
<pin name="SW" x="25.4" y="17.78" length="middle" direction="pwr" rot="R180"/>
<pin name="DAP" x="25.4" y="2.54" length="middle" direction="pwr" rot="R180"/>
<wire x1="0" y1="20.32" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="20.32" y2="0" width="0.1524" layer="94"/>
<wire x1="20.32" y1="0" x2="20.32" y2="20.32" width="0.1524" layer="94"/>
<wire x1="20.32" y1="20.32" x2="0" y2="20.32" width="0.1524" layer="94"/>
<text x="5.08" y="22.86" size="2.0828" layer="95" ratio="6" rot="SR0">&gt;Name</text>
<text x="5.08" y="20.574" size="2.0828" layer="96" ratio="6" rot="SR0">&gt;Value</text>
</symbol>
<symbol name="SCHOTTKY_DIODE">
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="1.524" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.524" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.778" x2="1.27" y2="0" width="0.254" layer="94"/>
<text x="-7.62" y="1.27" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-5.08" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
<pin name="A" x="-5.08" y="0" visible="off" length="short" direction="pas"/>
<pin name="C" x="5.08" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.524" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0" x2="-2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="1.27" y1="1.778" x2="0.508" y2="1.778" width="0.254" layer="94"/>
<wire x1="0.508" y1="1.778" x2="0.508" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="-1.524" x2="2.032" y2="-1.524" width="0.254" layer="94"/>
<wire x1="2.032" y1="-1.524" x2="2.032" y2="-1.016" width="0.254" layer="94"/>
</symbol>
<symbol name="LM317DCYR">
<wire x1="0" y1="0" x2="0" y2="-10.16" width="0.254" layer="94"/>
<wire x1="0" y1="-10.16" x2="15.24" y2="-10.16" width="0.254" layer="94"/>
<wire x1="15.24" y1="-10.16" x2="15.24" y2="0" width="0.254" layer="94"/>
<wire x1="15.24" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<pin name="1" x="7.62" y="-15.24" visible="pad" length="middle" rot="R90"/>
<pin name="3" x="-5.08" y="-2.54" visible="pad" length="middle"/>
<pin name="2" x="20.32" y="-2.54" visible="pad" length="middle" rot="R180"/>
<text x="0" y="0.254" size="1.27" layer="95" font="vector">&gt;NAME</text>
<text x="7.62" y="0.254" size="1.27" layer="96" font="vector">&gt;VALUE</text>
<text x="0" y="-10.16" size="0.8128" layer="94" rot="R90"></text>
<text x="-12.7" y="2.54" size="0.8128" layer="94"></text>
<text x="1.016" y="-2.54" size="1.778" layer="94" ratio="15">IN</text>
<text x="14.224" y="-2.54" size="1.778" layer="94" ratio="15" align="bottom-right">OUT</text>
<text x="6.096" y="-9.652" size="1.778" layer="94" ratio="15">ADJ</text>
</symbol>
<symbol name="PNP_TRANSISTOR">
<wire x1="-2.086" y1="-1.678" x2="-1.578" y2="-2.594" width="0.1524" layer="94"/>
<wire x1="-1.578" y1="-2.594" x2="-0.516" y2="-1.478" width="0.1524" layer="94"/>
<wire x1="-0.516" y1="-1.478" x2="-2.086" y2="-1.678" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-1.808" y2="-2.124" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-0.508" y2="1.524" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="-1.778" x2="-1.524" y2="-2.413" width="0.254" layer="94"/>
<wire x1="-1.524" y1="-2.413" x2="-0.762" y2="-1.651" width="0.254" layer="94"/>
<wire x1="-0.762" y1="-1.651" x2="-1.778" y2="-1.778" width="0.254" layer="94"/>
<wire x1="-1.778" y1="-1.778" x2="-1.524" y2="-2.159" width="0.254" layer="94"/>
<wire x1="-1.524" y1="-2.159" x2="-1.143" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-1.143" y1="-1.905" x2="-1.524" y2="-1.905" width="0.254" layer="94"/>
<text x="10.16" y="-7.62" size="1.778" layer="95" rot="R180">&gt;NAME</text>
<text x="10.16" y="-5.08" size="1.778" layer="96" rot="R180">&gt;VALUE</text>
<rectangle x1="-0.508" y1="-2.54" x2="0.254" y2="2.54" layer="94" rot="R180"/>
<pin name="B" x="2.54" y="0" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="E" x="-2.54" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="C" x="-2.54" y="5.08" visible="pad" length="short" direction="pas" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CAP_SMD_" prefix="C" uservalue="yes">
<gates>
<gate name="G$1" symbol="CAPACITOR" x="0" y="0"/>
</gates>
<devices>
<device name="0402" package="CAPC1005X55N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="CAPC1608X55N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="CAPC2013X145N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="CAPC3216X190N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1812" package="CAPC4532X279N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="IND_SMD_">
<gates>
<gate name="G$1" symbol="INDUCTOR" x="0" y="0"/>
</gates>
<devices>
<device name="0603" package="IND1608X50N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RES_SMD_" prefix="R" uservalue="yes">
<gates>
<gate name="G$1" symbol="RESISTER" x="0" y="0"/>
</gates>
<devices>
<device name="0402" package="RESC1005X40N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="RESC1608X50N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="RESC2013X70N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="RESC3216X84N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805_SH" package="RES2013X38N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2010" package="RESC5325X84N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2512" package="RESC6332X65N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CKSWITCHES_KXT3" prefix="SW" uservalue="yes">
<gates>
<gate name="G$1" symbol="SWITCH" x="0" y="0"/>
</gates>
<devices>
<device name="" package="CKSWITCHES_KXT3">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ATMEL_ATSAMD51N19" prefix="U" uservalue="yes">
<gates>
<gate name="G$1" symbol="ATMEL_ATSAMD51J_100" x="0" y="0"/>
</gates>
<devices>
<device name="" package="QFP50P1600X1600X120-100N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="100" pad="100"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="13" pad="13"/>
<connect gate="G$1" pin="14" pad="14"/>
<connect gate="G$1" pin="15" pad="15"/>
<connect gate="G$1" pin="16" pad="16"/>
<connect gate="G$1" pin="17" pad="17"/>
<connect gate="G$1" pin="18" pad="18"/>
<connect gate="G$1" pin="19" pad="19"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="20" pad="20"/>
<connect gate="G$1" pin="21" pad="21"/>
<connect gate="G$1" pin="22" pad="22"/>
<connect gate="G$1" pin="23" pad="23"/>
<connect gate="G$1" pin="24" pad="24"/>
<connect gate="G$1" pin="25" pad="25"/>
<connect gate="G$1" pin="26" pad="26"/>
<connect gate="G$1" pin="27" pad="27"/>
<connect gate="G$1" pin="28" pad="28"/>
<connect gate="G$1" pin="29" pad="29"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="30" pad="30"/>
<connect gate="G$1" pin="31" pad="31"/>
<connect gate="G$1" pin="32" pad="32"/>
<connect gate="G$1" pin="33" pad="33"/>
<connect gate="G$1" pin="34" pad="34"/>
<connect gate="G$1" pin="35" pad="35"/>
<connect gate="G$1" pin="36" pad="36"/>
<connect gate="G$1" pin="37" pad="37"/>
<connect gate="G$1" pin="38" pad="38"/>
<connect gate="G$1" pin="39" pad="39"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="40" pad="40"/>
<connect gate="G$1" pin="41" pad="41"/>
<connect gate="G$1" pin="42" pad="42"/>
<connect gate="G$1" pin="43" pad="43"/>
<connect gate="G$1" pin="44" pad="44"/>
<connect gate="G$1" pin="45" pad="45"/>
<connect gate="G$1" pin="46" pad="46"/>
<connect gate="G$1" pin="47" pad="47"/>
<connect gate="G$1" pin="48" pad="48"/>
<connect gate="G$1" pin="49" pad="49"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="50" pad="50"/>
<connect gate="G$1" pin="51" pad="51"/>
<connect gate="G$1" pin="52" pad="52"/>
<connect gate="G$1" pin="53" pad="53"/>
<connect gate="G$1" pin="54" pad="54"/>
<connect gate="G$1" pin="55" pad="55"/>
<connect gate="G$1" pin="56" pad="56"/>
<connect gate="G$1" pin="57" pad="57"/>
<connect gate="G$1" pin="58" pad="58"/>
<connect gate="G$1" pin="59" pad="59"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="60" pad="60"/>
<connect gate="G$1" pin="61" pad="61"/>
<connect gate="G$1" pin="62" pad="62"/>
<connect gate="G$1" pin="63" pad="63"/>
<connect gate="G$1" pin="64" pad="64"/>
<connect gate="G$1" pin="65" pad="65"/>
<connect gate="G$1" pin="66" pad="66"/>
<connect gate="G$1" pin="67" pad="67"/>
<connect gate="G$1" pin="68" pad="68"/>
<connect gate="G$1" pin="69" pad="69"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="70" pad="70"/>
<connect gate="G$1" pin="71" pad="71"/>
<connect gate="G$1" pin="72" pad="72"/>
<connect gate="G$1" pin="73" pad="73"/>
<connect gate="G$1" pin="74" pad="74"/>
<connect gate="G$1" pin="75" pad="75"/>
<connect gate="G$1" pin="76" pad="76"/>
<connect gate="G$1" pin="77" pad="77"/>
<connect gate="G$1" pin="78" pad="78"/>
<connect gate="G$1" pin="79" pad="79"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="80" pad="80"/>
<connect gate="G$1" pin="81" pad="81"/>
<connect gate="G$1" pin="82" pad="82"/>
<connect gate="G$1" pin="83" pad="83"/>
<connect gate="G$1" pin="84" pad="84"/>
<connect gate="G$1" pin="85" pad="85"/>
<connect gate="G$1" pin="86" pad="86"/>
<connect gate="G$1" pin="87" pad="87"/>
<connect gate="G$1" pin="88" pad="88"/>
<connect gate="G$1" pin="89" pad="89"/>
<connect gate="G$1" pin="9" pad="9"/>
<connect gate="G$1" pin="90" pad="90"/>
<connect gate="G$1" pin="91" pad="91"/>
<connect gate="G$1" pin="92" pad="92"/>
<connect gate="G$1" pin="93" pad="93"/>
<connect gate="G$1" pin="94" pad="94"/>
<connect gate="G$1" pin="95" pad="95"/>
<connect gate="G$1" pin="96" pad="96"/>
<connect gate="G$1" pin="97" pad="97"/>
<connect gate="G$1" pin="98" pad="98"/>
<connect gate="G$1" pin="99" pad="99"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CRYSTAL_ECS_" prefix="X" uservalue="yes">
<gates>
<gate name="G$1" symbol="CRYSTAL_4PIN" x="0" y="0"/>
</gates>
<devices>
<device name="ECX-32" package="CYRSTAL_ECS_ECX-32">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="3"/>
<connect gate="G$1" pin="3" pad="2 4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="ECX-53B-DU" package="CRYSTAL_ECS_ECX-53B-DU">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="3"/>
<connect gate="G$1" pin="3" pad="2 4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ISL33003" prefix="U" uservalue="yes">
<gates>
<gate name="G$1" symbol="I2C_ACCR_ISL33003" x="0" y="0"/>
</gates>
<devices>
<device name="8-MSOP" package="SOP65P490X110-8AN">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TP_CLASSB_" prefix="TP" uservalue="yes">
<gates>
<gate name="G$1" symbol="TP" x="0" y="0"/>
</gates>
<devices>
<device name="1.5MM" package="TP_1.5MM">
<connects>
<connect gate="G$1" pin="PP" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0.4MM" package="C120H40">
<connects>
<connect gate="G$1" pin="PP" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0.51MM" package="C131H51">
<connects>
<connect gate="G$1" pin="PP" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3.26MM" package="C406H326">
<connects>
<connect gate="G$1" pin="PP" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="4.11MM" package="C491H411">
<connects>
<connect gate="G$1" pin="PP" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1.5MM_PAD" package="TP_1.5MM_PAD">
<connects>
<connect gate="G$1" pin="PP" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1.7MM" package="TP_1.7MM">
<connects>
<connect gate="G$1" pin="PP" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="NC" prefix="NC">
<gates>
<gate name="G$1" symbol="NC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="AMIS_30642" prefix="U" uservalue="yes">
<gates>
<gate name="G$1" symbol="AMIS_30624" x="12.7" y="-22.86"/>
</gates>
<devices>
<device name="" package="QFN65P700X700X80-33N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="13" pad="13"/>
<connect gate="G$1" pin="14" pad="14"/>
<connect gate="G$1" pin="15" pad="15"/>
<connect gate="G$1" pin="16" pad="16"/>
<connect gate="G$1" pin="17" pad="17"/>
<connect gate="G$1" pin="18" pad="18"/>
<connect gate="G$1" pin="19" pad="19"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="20" pad="20"/>
<connect gate="G$1" pin="21" pad="21"/>
<connect gate="G$1" pin="22" pad="22"/>
<connect gate="G$1" pin="23" pad="23"/>
<connect gate="G$1" pin="24" pad="24"/>
<connect gate="G$1" pin="25" pad="25"/>
<connect gate="G$1" pin="26" pad="26"/>
<connect gate="G$1" pin="27" pad="27"/>
<connect gate="G$1" pin="28" pad="28"/>
<connect gate="G$1" pin="29" pad="29"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="30" pad="30"/>
<connect gate="G$1" pin="31" pad="31"/>
<connect gate="G$1" pin="32" pad="32"/>
<connect gate="G$1" pin="33" pad="33"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MICROHIP_24/5XC256" prefix="U" uservalue="yes">
<gates>
<gate name="G$1" symbol="MICROCHIP_24/5XC256" x="12.7" y="-12.7"/>
</gates>
<devices>
<device name="NOT_AVAILABLE" package="TDFN50PC300X200X75-9N">
<connects>
<connect gate="G$1" pin="P$1" pad="1"/>
<connect gate="G$1" pin="P$2" pad="2"/>
<connect gate="G$1" pin="P$3" pad="3"/>
<connect gate="G$1" pin="P$4" pad="4"/>
<connect gate="G$1" pin="P$5" pad="5"/>
<connect gate="G$1" pin="P$6" pad="6"/>
<connect gate="G$1" pin="P$7" pad="7"/>
<connect gate="G$1" pin="P$8" pad="8"/>
<connect gate="G$1" pin="P$9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="8DFN" package="DFN6X5MM">
<connects>
<connect gate="G$1" pin="P$1" pad="1"/>
<connect gate="G$1" pin="P$2" pad="2"/>
<connect gate="G$1" pin="P$3" pad="3"/>
<connect gate="G$1" pin="P$4" pad="4"/>
<connect gate="G$1" pin="P$5" pad="5"/>
<connect gate="G$1" pin="P$6" pad="6"/>
<connect gate="G$1" pin="P$7" pad="7"/>
<connect gate="G$1" pin="P$8" pad="8"/>
<connect gate="G$1" pin="P$9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="JST_VH_4" prefix="J" uservalue="yes">
<gates>
<gate name="G$1" symbol="PIN" x="5.08" y="3.81"/>
<gate name="G$2" symbol="PIN" x="5.08" y="1.27"/>
<gate name="G$3" symbol="PIN" x="5.08" y="-1.27"/>
<gate name="G$4" symbol="PIN" x="5.08" y="-3.81"/>
</gates>
<devices>
<device name="TOP" package="JST_B4P-VH">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$2" pin="1" pad="P$2"/>
<connect gate="G$3" pin="1" pad="P$3"/>
<connect gate="G$4" pin="1" pad="P$4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SIDE" package="JST_B4PS-VH">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$2" pin="1" pad="P$2"/>
<connect gate="G$3" pin="1" pad="P$3"/>
<connect gate="G$4" pin="1" pad="P$4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SHROUDED" package="JST_B4P-VH-FB-B">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$2" pin="1" pad="P$2"/>
<connect gate="G$3" pin="1" pad="P$3"/>
<connect gate="G$4" pin="1" pad="P$4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="IND_SMD_4.9X4.9" prefix="L" uservalue="yes">
<gates>
<gate name="G$1" symbol="INDUCTOR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="IND_SMD_4.9X4.9">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TI_DS3695AX" prefix="U" uservalue="yes">
<gates>
<gate name="G$1" symbol="RS485" x="10.16" y="-12.7"/>
</gates>
<devices>
<device name="" package="SOIC127P600X173-8N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DIODE_SMD_" prefix="D" uservalue="yes">
<gates>
<gate name="G$1" symbol="DIODE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="DO-214AC_SMA">
<connects>
<connect gate="G$1" pin="+" pad="A"/>
<connect gate="G$1" pin="-" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="IND_SMD_5.0X5.0" prefix="L" uservalue="yes">
<gates>
<gate name="G$1" symbol="INDUCTOR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="IND_SMD_5.0X5.0">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MOSFET_PMOS" prefix="Q" uservalue="yes">
<gates>
<gate name="G$1" symbol="PMOS_FET-1" x="-2.54" y="0"/>
</gates>
<devices>
<device name="SOT-23-3" package="SOT-23-3">
<connects>
<connect gate="G$1" pin="D" pad="3"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MOSFET_NMOS" prefix="Q" uservalue="yes">
<gates>
<gate name="G$1" symbol="NMOS_FET" x="-2.54" y="0"/>
</gates>
<devices>
<device name="SOT-23-3" package="SOT-23-3">
<connects>
<connect gate="G$1" pin="D" pad="3"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ZENER_DO-214AC" prefix="D" uservalue="yes">
<gates>
<gate name="G$1" symbol="ZENER" x="0" y="0"/>
</gates>
<devices>
<device name="" package="DO-214AC_SMA">
<connects>
<connect gate="G$1" pin="+" pad="A"/>
<connect gate="G$1" pin="-" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAP_POLARIZED_SMD_" prefix="CP" uservalue="yes">
<gates>
<gate name="G$1" symbol="CAPACITOR_ELECTRO" x="0" y="0"/>
</gates>
<devices>
<device name="4.3MMX4.6MM" package="CAPAE430X460N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="6.6MMX7.9MM" package="CAPAE660X790N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="8.3MMX6.8MM" package="CAPAE830X680N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TI_LM22672MRX-ADJ" prefix="U" uservalue="yes">
<gates>
<gate name="A" symbol="LM22672MRX-ADJ" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MRA08B">
<connects>
<connect gate="A" pin="BOOT" pad="1"/>
<connect gate="A" pin="DAP" pad="9"/>
<connect gate="A" pin="EN" pad="5"/>
<connect gate="A" pin="FB" pad="4"/>
<connect gate="A" pin="GND" pad="6"/>
<connect gate="A" pin="RT/SYNC" pad="3"/>
<connect gate="A" pin="SS" pad="2"/>
<connect gate="A" pin="SW" pad="8"/>
<connect gate="A" pin="VIN" pad="7"/>
</connects>
<technologies>
<technology name="">
<attribute name="MANUFACTURER_PART_NUMBER" value="lm22672mrxadj" constant="no"/>
<attribute name="VENDOR" value="Texas Instruments" constant="no"/>
</technology>
</technologies>
</device>
<device name="MRA08B-M" package="MRA08B-M">
<connects>
<connect gate="A" pin="BOOT" pad="1"/>
<connect gate="A" pin="DAP" pad="EPAD"/>
<connect gate="A" pin="EN" pad="5"/>
<connect gate="A" pin="FB" pad="4"/>
<connect gate="A" pin="GND" pad="6"/>
<connect gate="A" pin="RT/SYNC" pad="3"/>
<connect gate="A" pin="SS" pad="2"/>
<connect gate="A" pin="SW" pad="8"/>
<connect gate="A" pin="VIN" pad="7"/>
</connects>
<technologies>
<technology name="">
<attribute name="MANUFACTURER_PART_NUMBER" value="lm22672mrxadj" constant="no"/>
<attribute name="VENDOR" value="Texas Instruments" constant="no"/>
</technology>
</technologies>
</device>
<device name="MRA08B-L" package="MRA08B-L">
<connects>
<connect gate="A" pin="BOOT" pad="1"/>
<connect gate="A" pin="DAP" pad="EPAD"/>
<connect gate="A" pin="EN" pad="5"/>
<connect gate="A" pin="FB" pad="4"/>
<connect gate="A" pin="GND" pad="6"/>
<connect gate="A" pin="RT/SYNC" pad="3"/>
<connect gate="A" pin="SS" pad="2"/>
<connect gate="A" pin="SW" pad="8"/>
<connect gate="A" pin="VIN" pad="7"/>
</connects>
<technologies>
<technology name="">
<attribute name="MANUFACTURER_PART_NUMBER" value="lm22672mrxadj" constant="no"/>
<attribute name="VENDOR" value="Texas Instruments" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DIODE_SCHOTTKY_DO-214AC" prefix="D" uservalue="yes">
<gates>
<gate name="A" symbol="SCHOTTKY_DIODE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="DO-214AC_475X290">
<connects>
<connect gate="A" pin="A" pad="2"/>
<connect gate="A" pin="C" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TI_LM317DCYR" prefix="U" uservalue="yes">
<gates>
<gate name="A" symbol="LM317DCYR" x="0" y="0"/>
</gates>
<devices>
<device name="TO-261AA" package="SOT230P700X180-4N">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2 4"/>
<connect gate="A" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="JST_ZE_8" prefix="J" uservalue="yes">
<gates>
<gate name="G$1" symbol="PIN" x="5.08" y="7.62"/>
<gate name="G$2" symbol="PIN" x="5.08" y="5.08"/>
<gate name="G$3" symbol="PIN" x="5.08" y="2.54"/>
<gate name="G$4" symbol="PIN" x="5.08" y="0"/>
<gate name="G$5" symbol="PIN" x="5.08" y="-2.54"/>
<gate name="G$6" symbol="PIN" x="5.08" y="-5.08"/>
<gate name="G$7" symbol="PIN" x="5.08" y="-7.62"/>
<gate name="G$8" symbol="PIN" x="5.08" y="-10.16"/>
</gates>
<devices>
<device name="THT_TOP" package="JST_B08B-ZESK-1D">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$2" pin="1" pad="P$2"/>
<connect gate="G$3" pin="1" pad="P$3"/>
<connect gate="G$4" pin="1" pad="P$4"/>
<connect gate="G$5" pin="1" pad="P$5"/>
<connect gate="G$6" pin="1" pad="P$6"/>
<connect gate="G$7" pin="1" pad="P$7"/>
<connect gate="G$8" pin="1" pad="P$8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="THT_SIDE" package="JST_S08B-ZESK-2D">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$2" pin="1" pad="P$2"/>
<connect gate="G$3" pin="1" pad="P$3"/>
<connect gate="G$4" pin="1" pad="P$4"/>
<connect gate="G$5" pin="1" pad="P$5"/>
<connect gate="G$6" pin="1" pad="P$6"/>
<connect gate="G$7" pin="1" pad="P$7"/>
<connect gate="G$8" pin="1" pad="P$8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SMMBT2907ALT1G" prefix="Q" uservalue="yes">
<gates>
<gate name="G$1" symbol="PNP_TRANSISTOR" x="0" y="2.54"/>
</gates>
<devices>
<device name="" package="SOT95P240X110-3N">
<connects>
<connect gate="G$1" pin="B" pad="1"/>
<connect gate="G$1" pin="C" pad="3"/>
<connect gate="G$1" pin="E" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply2" urn="urn:adsk.eagle:library:372">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
Please keep in mind, that these devices are necessary for the
automatic wiring of the supply signals.&lt;p&gt;
The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND" urn="urn:adsk.eagle:symbol:26990/1" library_version="2">
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="-1.27" y2="0" width="0.254" layer="94"/>
<text x="-1.905" y="-3.175" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" urn="urn:adsk.eagle:component:27037/1" prefix="SUPPLY" library_version="2">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="GND" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1" urn="urn:adsk.eagle:library:371">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="+3V3" urn="urn:adsk.eagle:symbol:26950/1" library_version="1">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+3V3" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="+5V" urn="urn:adsk.eagle:symbol:26929/1" library_version="1">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+5V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="+3V3" urn="urn:adsk.eagle:component:26981/1" prefix="+3V3" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="+3V3" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+5V" urn="urn:adsk.eagle:component:26963/1" prefix="P+" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="+5V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="con-molex" urn="urn:adsk.eagle:library:165">
<description>&lt;b&gt;Molex Connectors&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="87758-1016" library_version="2">
<description>&lt;b&gt;2.00mm Pitch Milli-Grid™ Header, Through Hole, Vertical, 10 Circuits, 0.38µm Gold (Au) Selective Plating, Pocket Tray Packaging, Lead-Free&lt;/b&gt;&lt;p&gt;&lt;a href =http://www.molex.com/pdm_docs/sd/877581016_sd.pdf&gt;Datasheet &lt;/a&gt;</description>
<wire x1="-4.85" y1="-1.9" x2="4.85" y2="-1.9" width="0.2032" layer="21"/>
<wire x1="4.85" y1="-1.9" x2="4.85" y2="-0.4" width="0.2032" layer="21"/>
<wire x1="4.85" y1="0.4" x2="4.85" y2="1.9" width="0.2032" layer="21"/>
<wire x1="4.85" y1="1.9" x2="-4.85" y2="1.9" width="0.2032" layer="21"/>
<wire x1="-4.85" y1="1.9" x2="-4.85" y2="0.4" width="0.2032" layer="21"/>
<wire x1="-4.85" y1="-0.4" x2="-4.85" y2="-1.9" width="0.2032" layer="21"/>
<wire x1="-4.85" y1="0.4" x2="-4.85" y2="-0.4" width="0.2032" layer="21" curve="-129.184564"/>
<wire x1="4.85" y1="-0.4" x2="4.85" y2="0.4" width="0.2032" layer="21" curve="-129.184564"/>
<pad name="1" x="-4" y="-1" drill="0.9" diameter="1.27"/>
<pad name="2" x="-4" y="1" drill="0.9" diameter="1.27"/>
<pad name="3" x="-2" y="-1" drill="0.9" diameter="1.27"/>
<pad name="4" x="-2" y="1" drill="0.9" diameter="1.27"/>
<pad name="5" x="0" y="-1" drill="0.9" diameter="1.27"/>
<pad name="6" x="0" y="1" drill="0.9" diameter="1.27"/>
<pad name="7" x="2" y="-1" drill="0.9" diameter="1.27"/>
<pad name="8" x="2" y="1" drill="0.9" diameter="1.27"/>
<pad name="9" x="4" y="-1" drill="0.9" diameter="1.27"/>
<pad name="10" x="4" y="1" drill="0.9" diameter="1.27"/>
<text x="-4.65" y="-1.75" size="0.3048" layer="21" font="vector">1</text>
<text x="-4.62" y="-3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="1.73" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-4.25" y1="-1.25" x2="-3.75" y2="-0.75" layer="51"/>
<rectangle x1="-4.25" y1="0.75" x2="-3.75" y2="1.25" layer="51"/>
<rectangle x1="-2.25" y1="-1.25" x2="-1.75" y2="-0.75" layer="51"/>
<rectangle x1="-2.25" y1="0.75" x2="-1.75" y2="1.25" layer="51"/>
<rectangle x1="-0.25" y1="-1.25" x2="0.25" y2="-0.75" layer="51"/>
<rectangle x1="-0.25" y1="0.75" x2="0.25" y2="1.25" layer="51"/>
<rectangle x1="1.75" y1="-1.25" x2="2.25" y2="-0.75" layer="51"/>
<rectangle x1="1.75" y1="0.75" x2="2.25" y2="1.25" layer="51"/>
<rectangle x1="3.75" y1="-1.25" x2="4.25" y2="-0.75" layer="51"/>
<rectangle x1="3.75" y1="0.75" x2="4.25" y2="1.25" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="MV" library_version="2">
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
<text x="2.54" y="-0.762" size="1.524" layer="95">&gt;NAME</text>
<text x="-0.762" y="1.397" size="1.778" layer="96">&gt;VALUE</text>
<pin name="S" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
</symbol>
<symbol name="M" library_version="2">
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
<text x="2.54" y="-0.762" size="1.524" layer="95">&gt;NAME</text>
<pin name="S" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="87758-1016" prefix="X" library_version="2">
<description>&lt;b&gt;10 Pin - 2mm Dual Row Single Wafer, Vertical T/H HDR&lt;/b&gt;&lt;p&gt;
Source: http://www.molex.com/pdm_docs/sd/877580616_sd.pdf</description>
<gates>
<gate name="-1" symbol="MV" x="-10.16" y="5.08" addlevel="always"/>
<gate name="-2" symbol="MV" x="10.16" y="5.08" addlevel="always"/>
<gate name="-3" symbol="M" x="-10.16" y="2.54" addlevel="always"/>
<gate name="-4" symbol="M" x="10.16" y="2.54" addlevel="always"/>
<gate name="-5" symbol="M" x="-10.16" y="0" addlevel="always"/>
<gate name="-6" symbol="M" x="10.16" y="0" addlevel="always"/>
<gate name="-7" symbol="M" x="-10.16" y="-2.54" addlevel="always"/>
<gate name="-8" symbol="M" x="10.16" y="-2.54" addlevel="always"/>
<gate name="-9" symbol="M" x="-10.16" y="-5.08" addlevel="always"/>
<gate name="-10" symbol="M" x="10.16" y="-5.08" addlevel="always"/>
</gates>
<devices>
<device name="" package="87758-1016">
<connects>
<connect gate="-1" pin="S" pad="1"/>
<connect gate="-10" pin="S" pad="10"/>
<connect gate="-2" pin="S" pad="2"/>
<connect gate="-3" pin="S" pad="3"/>
<connect gate="-4" pin="S" pad="4"/>
<connect gate="-5" pin="S" pad="5"/>
<connect gate="-6" pin="S" pad="6"/>
<connect gate="-7" pin="S" pad="7"/>
<connect gate="-8" pin="S" pad="8"/>
<connect gate="-9" pin="S" pad="9"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="MOLEX" constant="no"/>
<attribute name="MPN" value="87758-1016" constant="no"/>
<attribute name="OC_FARNELL" value="7472340" constant="no"/>
<attribute name="OC_NEWARK" value="59J1558" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply2">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
Please keep in mind, that these devices are necessary for the
automatic wiring of the supply signals.&lt;p&gt;
The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND">
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="-1.27" y2="0" width="0.254" layer="94"/>
<text x="-1.905" y="-3.175" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="SUPPLY">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="GND" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="U$1" library="frames" deviceset="A3_LOC_JMA_RELEASE" device=""/>
<part name="SUPPLY1" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY2" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="C1" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="0.1uF"/>
<part name="C2" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="10nF"/>
<part name="C3" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="0.1uF"/>
<part name="C4" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="0.1uF"/>
<part name="C5" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="10nF"/>
<part name="C6" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="0.1uF"/>
<part name="C7" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="10nF"/>
<part name="C8" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="0.1uF"/>
<part name="C9" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="10nF"/>
<part name="L1" library="JMA_LBR" deviceset="IND_SMD_" device="0603" value="10uH"/>
<part name="C10" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="DNP"/>
<part name="SUPPLY3" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY4" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="R1" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="100K"/>
<part name="R2" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="100"/>
<part name="C11" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="0.1uF"/>
<part name="SW1" library="JMA_LBR" deviceset="CKSWITCHES_KXT3" device="" value="KXT331LHS"/>
<part name="+3V3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="SUPPLY5" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="R3" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="100K"/>
<part name="X1" library="con-molex" library_urn="urn:adsk.eagle:library:165" deviceset="87758-1016" device=""/>
<part name="+3V5" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="SUPPLY6" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY7" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="C12" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="0.1uF"/>
<part name="C13" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="10nF"/>
<part name="C14" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="0.1uF"/>
<part name="C15" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="10nF"/>
<part name="C16" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="0.1uF"/>
<part name="C17" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="4.7uF"/>
<part name="U1" library="JMA_LBR" deviceset="ATMEL_ATSAMD51N19" device="" value="SAM51N19/SAMD51N20"/>
<part name="X2" library="JMA_LBR" deviceset="CRYSTAL_ECS_" device="ECX-53B-DU" value="ECS_ECX-53B-DU"/>
<part name="C18" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="20pF"/>
<part name="C19" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="20pF"/>
<part name="SUPPLY10" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="U$3" library="frames" deviceset="A3_LOC_JMA_RELEASE" device=""/>
<part name="C20" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="0.1uF"/>
<part name="C21" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="0.1uF"/>
<part name="C22" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="1uF"/>
<part name="C23" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="1uF"/>
<part name="R4" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="1K"/>
<part name="R5" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="4.7K"/>
<part name="R6" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="4.7K"/>
<part name="R7" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="4.7K"/>
<part name="SUPPLY8" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY9" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="R8" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="4.7K"/>
<part name="U4" library="JMA_LBR" deviceset="ISL33003" device="8-MSOP"/>
<part name="SUPPLY12" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="C24" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="0.1uF"/>
<part name="C25" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="0.1uF"/>
<part name="C26" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="1uF"/>
<part name="C27" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="1uF"/>
<part name="R9" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="1K"/>
<part name="R10" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="4.7K"/>
<part name="R11" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="4.7K"/>
<part name="R12" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="4.7K"/>
<part name="SUPPLY11" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY13" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="R13" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="4.7K"/>
<part name="U2" library="JMA_LBR" deviceset="ISL33003" device="8-MSOP"/>
<part name="SUPPLY14" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="C28" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="0.1uF"/>
<part name="R14" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="0"/>
<part name="TP1" library="JMA_LBR" deviceset="TP_CLASSB_" device="0.4MM"/>
<part name="TP2" library="JMA_LBR" deviceset="TP_CLASSB_" device="0.4MM"/>
<part name="C29" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="10nF"/>
<part name="C30" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="0.1uF"/>
<part name="TP3" library="JMA_LBR" deviceset="TP_CLASSB_" device="0.4MM"/>
<part name="U$2" library="frames" deviceset="A3_LOC_JMA_RELEASE" device=""/>
<part name="U8" library="JMA_LBR" deviceset="AMIS_30642" device="" value="AMIS 30624"/>
<part name="C45" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="220nF"/>
<part name="C48" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="220nF"/>
<part name="SUPPLY39" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY37" library="supply2" deviceset="GND" device=""/>
<part name="NC5" library="JMA_LBR" deviceset="NC" device=""/>
<part name="NC4" library="JMA_LBR" deviceset="NC" device=""/>
<part name="NC6" library="JMA_LBR" deviceset="NC" device=""/>
<part name="C43" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="3.3uF"/>
<part name="R44" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="1K"/>
<part name="SUPPLY36" library="supply2" deviceset="GND" device=""/>
<part name="R37" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="1K">
<attribute name="SPICEPREFIX" value="R"/>
</part>
<part name="C34" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="3.3uF">
<attribute name="SPICEPREFIX" value="C"/>
</part>
<part name="SUPPLY34" library="supply2" deviceset="GND" device="">
<attribute name="SPICEPREFIX" value="G"/>
</part>
<part name="R38" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="DNP">
<attribute name="SPICEPREFIX" value="R"/>
</part>
<part name="R47" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="DNP"/>
<part name="R35" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="0">
<attribute name="SPICEPREFIX" value="R"/>
</part>
<part name="R46" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="0"/>
<part name="SUPPLY31" library="supply2" deviceset="GND" device="">
<attribute name="SPICEPREFIX" value="G"/>
</part>
<part name="SUPPLY32" library="supply2" deviceset="GND" device=""/>
<part name="U3" library="JMA_LBR" deviceset="AMIS_30642" device="" value="AMIS 30624"/>
<part name="C31" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="220nF"/>
<part name="C32" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="220nF"/>
<part name="SUPPLY15" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY16" library="supply2" deviceset="GND" device=""/>
<part name="NC1" library="JMA_LBR" deviceset="NC" device=""/>
<part name="NC2" library="JMA_LBR" deviceset="NC" device=""/>
<part name="NC3" library="JMA_LBR" deviceset="NC" device=""/>
<part name="C33" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="3.3uF"/>
<part name="R15" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="1K"/>
<part name="SUPPLY17" library="supply2" deviceset="GND" device=""/>
<part name="R16" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="1K"/>
<part name="C35" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="3.3uF"/>
<part name="SUPPLY18" library="supply2" deviceset="GND" device=""/>
<part name="R17" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="DNP"/>
<part name="R18" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="0"/>
<part name="R19" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="0"/>
<part name="R20" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="DNP"/>
<part name="SUPPLY19" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY20" library="supply2" deviceset="GND" device=""/>
<part name="U5" library="JMA_LBR" deviceset="AMIS_30642" device="" value="AMIS 30624"/>
<part name="C36" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="220nF"/>
<part name="C37" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="220nF"/>
<part name="SUPPLY21" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY22" library="supply2" deviceset="GND" device=""/>
<part name="NC7" library="JMA_LBR" deviceset="NC" device=""/>
<part name="NC8" library="JMA_LBR" deviceset="NC" device=""/>
<part name="NC9" library="JMA_LBR" deviceset="NC" device=""/>
<part name="C38" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="3.3uF"/>
<part name="R21" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="1K"/>
<part name="SUPPLY23" library="supply2" deviceset="GND" device=""/>
<part name="R22" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="1K"/>
<part name="C39" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="3.3uF"/>
<part name="SUPPLY24" library="supply2" deviceset="GND" device=""/>
<part name="R23" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="0"/>
<part name="R24" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="DNP"/>
<part name="R25" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="DNP"/>
<part name="R26" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="DNP"/>
<part name="SUPPLY25" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY26" library="supply2" deviceset="GND" device=""/>
<part name="U6" library="JMA_LBR" deviceset="AMIS_30642" device="" value="AMIS 30624"/>
<part name="C40" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="220nF"/>
<part name="C41" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="220nF"/>
<part name="SUPPLY27" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY28" library="supply2" deviceset="GND" device=""/>
<part name="NC10" library="JMA_LBR" deviceset="NC" device=""/>
<part name="NC11" library="JMA_LBR" deviceset="NC" device=""/>
<part name="NC12" library="JMA_LBR" deviceset="NC" device=""/>
<part name="C42" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="3.3uF"/>
<part name="R27" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="1K"/>
<part name="SUPPLY29" library="supply2" deviceset="GND" device=""/>
<part name="R28" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="1K"/>
<part name="C44" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="3.3uF"/>
<part name="SUPPLY30" library="supply2" deviceset="GND" device=""/>
<part name="R29" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="DNP"/>
<part name="R30" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="DNP"/>
<part name="R31" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="0"/>
<part name="R32" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="0"/>
<part name="SUPPLY33" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY35" library="supply2" deviceset="GND" device=""/>
<part name="U7" library="JMA_LBR" deviceset="AMIS_30642" device="" value="AMIS 30624"/>
<part name="C46" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="220nF"/>
<part name="C47" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="220nF"/>
<part name="SUPPLY38" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY40" library="supply2" deviceset="GND" device=""/>
<part name="NC13" library="JMA_LBR" deviceset="NC" device=""/>
<part name="NC14" library="JMA_LBR" deviceset="NC" device=""/>
<part name="NC15" library="JMA_LBR" deviceset="NC" device=""/>
<part name="C49" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="3.3uF"/>
<part name="R33" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="1K"/>
<part name="SUPPLY41" library="supply2" deviceset="GND" device=""/>
<part name="R34" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="1K"/>
<part name="C50" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="3.3uF"/>
<part name="SUPPLY42" library="supply2" deviceset="GND" device=""/>
<part name="R36" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="DNP"/>
<part name="R39" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="0"/>
<part name="R40" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="0"/>
<part name="R41" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="DNP"/>
<part name="SUPPLY43" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY44" library="supply2" deviceset="GND" device=""/>
<part name="U9" library="JMA_LBR" deviceset="AMIS_30642" device="" value="AMIS 30624"/>
<part name="C51" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="220nF"/>
<part name="C52" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="220nF"/>
<part name="SUPPLY45" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY46" library="supply2" deviceset="GND" device=""/>
<part name="NC16" library="JMA_LBR" deviceset="NC" device=""/>
<part name="NC17" library="JMA_LBR" deviceset="NC" device=""/>
<part name="NC18" library="JMA_LBR" deviceset="NC" device=""/>
<part name="C53" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="3.3uF"/>
<part name="R42" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="1K"/>
<part name="SUPPLY47" library="supply2" deviceset="GND" device=""/>
<part name="R43" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="1K"/>
<part name="C54" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="3.3uF"/>
<part name="SUPPLY48" library="supply2" deviceset="GND" device=""/>
<part name="R45" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="0"/>
<part name="R48" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="DNP"/>
<part name="R49" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="DNP"/>
<part name="R50" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="DNP"/>
<part name="SUPPLY49" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY50" library="supply2" deviceset="GND" device=""/>
<part name="U10" library="JMA_LBR" deviceset="MICROHIP_24/5XC256" device="8DFN"/>
<part name="R51" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="10K"/>
<part name="R52" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="DNP"/>
<part name="R53" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="10K"/>
<part name="R54" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="10K"/>
<part name="R55" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="DNP"/>
<part name="R56" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="10K"/>
<part name="SUPPLY51" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY52" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="C55" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="0.1uF"/>
<part name="+3V1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="+3V2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="+3V6" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="P+1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="P+2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="U$4" library="frames" deviceset="A3_LOC_JMA_RELEASE" device=""/>
<part name="J1" library="JMA_LBR" deviceset="JST_VH_4" device="SHROUDED"/>
<part name="J2" library="JMA_LBR" deviceset="JST_VH_4" device="SHROUDED"/>
<part name="J3" library="JMA_LBR" deviceset="JST_VH_4" device="SHROUDED"/>
<part name="J4" library="JMA_LBR" deviceset="JST_VH_4" device="SHROUDED"/>
<part name="R87" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="0"/>
<part name="R88" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="0"/>
<part name="R89" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="0"/>
<part name="R90" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="0"/>
<part name="R91" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="DNP"/>
<part name="R92" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="DNP"/>
<part name="R93" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="0"/>
<part name="R94" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="0"/>
<part name="R95" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="0"/>
<part name="R96" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="0"/>
<part name="R97" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="DNP"/>
<part name="R98" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="DNP"/>
<part name="U$5" library="frames" deviceset="A3_LOC_JMA_RELEASE" device=""/>
<part name="C65" library="JMA_LBR" deviceset="CAP_SMD_" device="0603"/>
<part name="C66" library="JMA_LBR" deviceset="CAP_SMD_" device="0402"/>
<part name="C67" library="JMA_LBR" deviceset="CAP_SMD_" device="0402"/>
<part name="C68" library="JMA_LBR" deviceset="CAP_SMD_" device="0603"/>
<part name="L2" library="JMA_LBR" deviceset="IND_SMD_4.9X4.9" device=""/>
<part name="L3" library="JMA_LBR" deviceset="IND_SMD_4.9X4.9" device=""/>
<part name="R99" library="JMA_LBR" deviceset="RES_SMD_" device="0402"/>
<part name="R100" library="JMA_LBR" deviceset="RES_SMD_" device="0402"/>
<part name="U16" library="JMA_LBR" deviceset="TI_DS3695AX" device=""/>
<part name="R101" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="0"/>
<part name="R102" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="DNP"/>
<part name="R103" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="0"/>
<part name="R104" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="DNP"/>
<part name="R105" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="0\"/>
<part name="R106" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="0"/>
<part name="C69" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="15pF"/>
<part name="C70" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="15pF"/>
<part name="C71" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="1uF"/>
<part name="SUPPLY55" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="P+3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="SUPPLY56" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="U17" library="JMA_LBR" deviceset="TI_DS3695AX" device=""/>
<part name="R107" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="0"/>
<part name="R108" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="DNP"/>
<part name="R109" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="0"/>
<part name="R110" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="DNP"/>
<part name="R111" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="0"/>
<part name="R112" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="0"/>
<part name="C72" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="15pF"/>
<part name="C73" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="15pF"/>
<part name="C74" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="1uF"/>
<part name="SUPPLY57" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="P+4" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="SUPPLY58" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="U18" library="JMA_LBR" deviceset="TI_DS3695AX" device=""/>
<part name="R113" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="0"/>
<part name="R114" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="DNP"/>
<part name="R115" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="0"/>
<part name="R116" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="DNP"/>
<part name="R117" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="0"/>
<part name="R118" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="0"/>
<part name="C75" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="15pF"/>
<part name="C76" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="15pF"/>
<part name="C77" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="1uF"/>
<part name="SUPPLY59" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="P+5" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="SUPPLY60" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="U19" library="JMA_LBR" deviceset="TI_DS3695AX" device=""/>
<part name="R119" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="0"/>
<part name="R120" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="DNP"/>
<part name="R121" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="0"/>
<part name="R122" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="DNP"/>
<part name="R123" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="0"/>
<part name="R124" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="0"/>
<part name="C78" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="15pF"/>
<part name="C79" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="15pF"/>
<part name="C80" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="1uF"/>
<part name="SUPPLY61" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="P+6" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="SUPPLY62" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="D2" library="JMA_LBR" deviceset="DIODE_SMD_" device="" value="SBRT3U45SAF-13"/>
<part name="D3" library="JMA_LBR" deviceset="DIODE_SMD_" device="" value="SBRT3U45SAF-13"/>
<part name="D4" library="JMA_LBR" deviceset="DIODE_SMD_" device="" value="SBRT3U45SAF-13"/>
<part name="D5" library="JMA_LBR" deviceset="DIODE_SMD_" device="" value="SBRT3U45SAF-13"/>
<part name="C81" library="JMA_LBR" deviceset="CAP_SMD_" device="0603"/>
<part name="C82" library="JMA_LBR" deviceset="CAP_SMD_" device="0603"/>
<part name="C83" library="JMA_LBR" deviceset="CAP_SMD_" device="0603"/>
<part name="R125" library="JMA_LBR" deviceset="RES_SMD_" device="0603"/>
<part name="R126" library="JMA_LBR" deviceset="RES_SMD_" device="0603"/>
<part name="L4" library="JMA_LBR" deviceset="IND_SMD_5.0X5.0" device=""/>
<part name="R127" library="JMA_LBR" deviceset="RES_SMD_" device="0603"/>
<part name="Q2" library="JMA_LBR" deviceset="MOSFET_PMOS" device="SOT-23-3" value="SSM3J356R,LF "/>
<part name="Q3" library="JMA_LBR" deviceset="MOSFET_PMOS" device="SOT-23-3" value="SSM3J356R,LF "/>
<part name="Q4" library="JMA_LBR" deviceset="MOSFET_NMOS" device="SOT-23-3" value="SSM3K2615R,LF "/>
<part name="R129" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="10K"/>
<part name="SUPPLY63" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY64" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY65" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY66" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY67" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY68" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="D6" library="JMA_LBR" deviceset="ZENER_DO-214AC" device="" value="DNP"/>
<part name="R128" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="DNP"/>
<part name="SUPPLY69" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="R130" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="0"/>
<part name="D7" library="JMA_LBR" deviceset="ZENER_DO-214AC" device="" value="DNP"/>
<part name="L5" library="JMA_LBR" deviceset="IND_SMD_5.0X5.0" device=""/>
<part name="C84" library="JMA_LBR" deviceset="CAP_SMD_" device="0603"/>
<part name="C85" library="JMA_LBR" deviceset="CAP_SMD_" device="0603"/>
<part name="C86" library="JMA_LBR" deviceset="CAP_SMD_" device="0805"/>
<part name="C87" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="DNP"/>
<part name="C88" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="0.1uF"/>
<part name="C89" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="10nF"/>
<part name="R131" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="442K"/>
<part name="R132" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="30K"/>
<part name="Q5" library="JMA_LBR" deviceset="MOSFET_PMOS" device="SOT-23-3" value="SSM3J356R,LF "/>
<part name="Q6" library="JMA_LBR" deviceset="MOSFET_PMOS" device="SOT-23-3" value="SSM3J356R,LF "/>
<part name="Q7" library="JMA_LBR" deviceset="MOSFET_NMOS" device="SOT-23-3" value="SSM3K2615R,LF "/>
<part name="R133" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="10K"/>
<part name="SUPPLY70" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="D8" library="JMA_LBR" deviceset="ZENER_DO-214AC" device="" value="DNP"/>
<part name="R134" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="DNP"/>
<part name="SUPPLY71" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="R135" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="0"/>
<part name="D9" library="JMA_LBR" deviceset="ZENER_DO-214AC" device="" value="3SMAJ5925B-TP"/>
<part name="C90" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="DNP"/>
<part name="C91" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="DNP"/>
<part name="C92" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="0.1uF"/>
<part name="C93" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="10nF"/>
<part name="R136" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="442K"/>
<part name="R137" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="30K"/>
<part name="Q8" library="JMA_LBR" deviceset="MOSFET_PMOS" device="SOT-23-3" value="SSM3J356R,LF "/>
<part name="Q9" library="JMA_LBR" deviceset="MOSFET_PMOS" device="SOT-23-3" value="SSM3J356R,LF "/>
<part name="Q10" library="JMA_LBR" deviceset="MOSFET_NMOS" device="SOT-23-3" value="SSM3K2615R,LF "/>
<part name="R138" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="10K"/>
<part name="SUPPLY72" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="D10" library="JMA_LBR" deviceset="ZENER_DO-214AC" device="" value="DNP"/>
<part name="R139" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="DNP"/>
<part name="SUPPLY73" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="R140" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="0"/>
<part name="D11" library="JMA_LBR" deviceset="ZENER_DO-214AC" device="" value="3SMAJ5925B-TP"/>
<part name="C94" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="DNP"/>
<part name="C95" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="DNP"/>
<part name="C96" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="0.1uF"/>
<part name="C97" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="10nF"/>
<part name="R141" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="442K"/>
<part name="R142" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="30K"/>
<part name="Q11" library="JMA_LBR" deviceset="MOSFET_PMOS" device="SOT-23-3" value="SSM3J356R,LF "/>
<part name="Q12" library="JMA_LBR" deviceset="MOSFET_PMOS" device="SOT-23-3" value="SSM3J356R,LF "/>
<part name="Q13" library="JMA_LBR" deviceset="MOSFET_NMOS" device="SOT-23-3" value="SSM3K2615R,LF "/>
<part name="R143" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="10K"/>
<part name="SUPPLY74" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="D12" library="JMA_LBR" deviceset="ZENER_DO-214AC" device="" value="DNP"/>
<part name="R144" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="DNP"/>
<part name="SUPPLY75" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="R145" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="0"/>
<part name="D13" library="JMA_LBR" deviceset="ZENER_DO-214AC" device="" value="3SMAJ5925B-TP"/>
<part name="C98" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="DNP"/>
<part name="C99" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="DNP"/>
<part name="C100" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="0.1uF"/>
<part name="C101" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="10nF"/>
<part name="R146" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="442K"/>
<part name="R147" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="30K"/>
<part name="Q14" library="JMA_LBR" deviceset="MOSFET_PMOS" device="SOT-23-3" value="SSM3J356R,LF "/>
<part name="Q15" library="JMA_LBR" deviceset="MOSFET_PMOS" device="SOT-23-3" value="SSM3J356R,LF "/>
<part name="Q16" library="JMA_LBR" deviceset="MOSFET_NMOS" device="SOT-23-3" value="SSM3K2615R,LF "/>
<part name="R148" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="10K"/>
<part name="SUPPLY76" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="D14" library="JMA_LBR" deviceset="ZENER_DO-214AC" device="" value="DNP"/>
<part name="R149" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="DNP"/>
<part name="SUPPLY77" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="R150" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="0"/>
<part name="D15" library="JMA_LBR" deviceset="ZENER_DO-214AC" device="" value="3SMAJ5925B-TP"/>
<part name="C102" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="DNP"/>
<part name="SUPPLY78" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="D16" library="JMA_LBR" deviceset="DIODE_SMD_" device="" value="SBRT3U45SAF-13"/>
<part name="D17" library="JMA_LBR" deviceset="DIODE_SMD_" device="" value="SBRT3U45SAF-13"/>
<part name="D18" library="JMA_LBR" deviceset="DIODE_SMD_" device="" value="SBRT3U45SAF-13"/>
<part name="D19" library="JMA_LBR" deviceset="DIODE_SMD_" device="" value="SBRT3U45SAF-13"/>
<part name="CP2" library="JMA_LBR" deviceset="CAP_POLARIZED_SMD_" device="6.6MMX7.9MM" value="47uF"/>
<part name="C103" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="0.1uF"/>
<part name="L6" library="JMA_LBR" deviceset="IND_SMD_5.0X5.0" device="" value="10uF"/>
<part name="R151" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="DNP"/>
<part name="C104" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="0.1uF"/>
<part name="C105" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="10nF"/>
<part name="U20" library="JMA_LBR" deviceset="TI_LM22672MRX-ADJ" device="" value="LM22672MRX-ADJ"/>
<part name="R152" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="DNP"/>
<part name="SUPPLY79" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY80" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="R153" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="6.65K"/>
<part name="R154" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="1K"/>
<part name="L7" library="JMA_LBR" deviceset="IND_SMD_5.0X5.0" device="" value="22uH"/>
<part name="C106" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="10nF"/>
<part name="SUPPLY81" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY82" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY83" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="D20" library="JMA_LBR" deviceset="DIODE_SCHOTTKY_DO-214AC" device="" value="B320A-13-F/B220A-13-F "/>
<part name="R155" library="JMA_LBR" deviceset="RES_SMD_" device="0805" value="DNP"/>
<part name="CP4" library="JMA_LBR" deviceset="CAP_POLARIZED_SMD_" device="6.6MMX7.9MM" value="47uF"/>
<part name="C107" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="0.1uF"/>
<part name="L8" library="JMA_LBR" deviceset="IND_SMD_5.0X5.0" device="" value="10uF"/>
<part name="R156" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="DNP"/>
<part name="C108" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="0.1uF"/>
<part name="C109" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="10nF"/>
<part name="U21" library="JMA_LBR" deviceset="TI_LM22672MRX-ADJ" device="" value="LM22672MRX-ADJ"/>
<part name="R157" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="DNP"/>
<part name="SUPPLY84" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY85" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="R158" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="6.34K"/>
<part name="R159" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="1K"/>
<part name="L9" library="JMA_LBR" deviceset="IND_SMD_5.0X5.0" device="" value="22uH"/>
<part name="C110" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="10nF"/>
<part name="SUPPLY86" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY87" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY88" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="D21" library="JMA_LBR" deviceset="DIODE_SCHOTTKY_DO-214AC" device="" value="B320A-13-F/B220A-13-F "/>
<part name="C111" library="JMA_LBR" deviceset="CAP_SMD_" device="0603"/>
<part name="L10" library="JMA_LBR" deviceset="IND_SMD_5.0X5.0" device=""/>
<part name="R160" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="DNP"/>
<part name="C112" library="JMA_LBR" deviceset="CAP_SMD_" device="0603"/>
<part name="C113" library="JMA_LBR" deviceset="CAP_SMD_" device="0402"/>
<part name="U22" library="JMA_LBR" deviceset="TI_LM22672MRX-ADJ" device=""/>
<part name="R161" library="JMA_LBR" deviceset="RES_SMD_" device="0402"/>
<part name="SUPPLY89" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY90" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="R162" library="JMA_LBR" deviceset="RES_SMD_" device="0603"/>
<part name="R163" library="JMA_LBR" deviceset="RES_SMD_" device="0603"/>
<part name="L11" library="JMA_LBR" deviceset="IND_SMD_5.0X5.0" device=""/>
<part name="C114" library="JMA_LBR" deviceset="CAP_SMD_" device="0603"/>
<part name="SUPPLY91" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY92" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY93" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="D22" library="JMA_LBR" deviceset="DIODE_SCHOTTKY_DO-214AC" device="" value="B320A-13-F/B220A-13-F "/>
<part name="C115" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="0.1uF"/>
<part name="C116" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="0.1uF"/>
<part name="C117" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="0.1uF"/>
<part name="C118" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="0.1uF"/>
<part name="C119" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="0.1uF"/>
<part name="C120" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="0.1uF"/>
<part name="R164" library="JMA_LBR" deviceset="RES_SMD_" device="0603"/>
<part name="U13" library="JMA_LBR" deviceset="TI_LM317DCYR" device="TO-261AA"/>
<part name="R165" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="DNP"/>
<part name="J5" library="JMA_LBR" deviceset="JST_ZE_8" device="THT_TOP"/>
<part name="J6" library="JMA_LBR" deviceset="JST_ZE_8" device="THT_TOP"/>
<part name="J7" library="JMA_LBR" deviceset="JST_ZE_8" device="THT_TOP"/>
<part name="J8" library="JMA_LBR" deviceset="JST_ZE_8" device="THT_TOP"/>
<part name="J9" library="JMA_LBR" deviceset="JST_ZE_8" device="THT_TOP"/>
<part name="J10" library="JMA_LBR" deviceset="JST_ZE_8" device="THT_TOP"/>
<part name="+3V8" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="+3V9" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="+3V10" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="C121" library="JMA_LBR" deviceset="CAP_SMD_" device="0603"/>
<part name="C122" library="JMA_LBR" deviceset="CAP_SMD_" device="0603"/>
<part name="C123" library="JMA_LBR" deviceset="CAP_SMD_" device="0603"/>
<part name="C124" library="JMA_LBR" deviceset="CAP_SMD_" device="0603"/>
<part name="R166" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="3"/>
<part name="R167" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="5"/>
<part name="C125" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="330pF"/>
<part name="D23" library="JMA_LBR" deviceset="DIODE_SMD_" device=""/>
<part name="R168" library="JMA_LBR" deviceset="RES_SMD_" device="0402"/>
<part name="Q17" library="JMA_LBR" deviceset="SMMBT2907ALT1G" device=""/>
<part name="C126" library="JMA_LBR" deviceset="CAP_SMD_" device="0402"/>
<part name="SUPPLY94" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="TP4" library="JMA_LBR" deviceset="TP_CLASSB_" device="0.4MM"/>
<part name="C56" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="10nF"/>
<part name="C57" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="10nF"/>
<part name="R57" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="100K"/>
<part name="R58" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="DNP"/>
<part name="R59" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="100"/>
<part name="C58" library="JMA_LBR" deviceset="CAP_SMD_" device="0805" value="4.7uF"/>
<part name="C59" library="JMA_LBR" deviceset="CAP_SMD_" device="0805" value="4.7uF"/>
<part name="R60" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="3"/>
<part name="R61" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="3"/>
<part name="R62" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="5"/>
<part name="R63" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="5"/>
<part name="C60" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="330pF"/>
<part name="C61" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="330pF"/>
</parts>
<sheets>
<sheet>
<plain>
<text x="287.02" y="35.56" size="1.27" layer="94">RBG</text>
<text x="312.42" y="17.78" size="1.27" layer="94">RET 2000 </text>
<text x="370.84" y="10.16" size="1.27" layer="94">0</text>
<text x="112.776" y="203.2" size="1.27" layer="97" rot="R90">Replace Inductor with 0 ohm
if Linear regualtors used.</text>
<text x="109.22" y="203.2" size="1.27" layer="97" rot="R90">LQM18PN1R0MFHD </text>
<text x="233.68" y="30.48" size="1.27" layer="97">Add 0.1uF for analog pin to gnd</text>
<text x="312.42" y="124.46" size="1.27" layer="97">Optional #LTMM-105-02-F-D</text>
<text x="84.582" y="43.434" size="1.27" layer="97">I2C_LB</text>
<text x="135.636" y="42.926" size="1.27" layer="97">I2C_HB</text>
<text x="210.82" y="88.9" size="1.27" layer="97">AISG_3</text>
<text x="210.82" y="139.7" size="1.27" layer="97">AISG_1</text>
<text x="313.182" y="180.594" size="1.27" layer="97">CORE VOLTAGE</text>
<text x="231.14" y="226.06" size="1.27" layer="97">VDDIO</text>
<text x="102.87" y="45.72" size="1.27" layer="97">SPI_MM</text>
<text x="12.7" y="144.78" size="1.27" layer="97">AISG_2</text>
<text x="12.7" y="109.22" size="1.27" layer="97">AISG_4</text>
<text x="139.7" y="30.48" size="1.27" layer="94" rot="R90">SCL/RXD_IN</text>
<text x="137.16" y="30.48" size="1.27" layer="94" rot="R90">SDA/TXD_IN</text>
</plain>
<instances>
<instance part="U$1" gate="G$1" x="0" y="0"/>
<instance part="SUPPLY1" gate="GND" x="66.04" y="60.96" smashed="yes">
<attribute name="VALUE" x="64.135" y="57.785" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY2" gate="GND" x="152.4" y="162.56" smashed="yes">
<attribute name="VALUE" x="150.495" y="159.385" size="1.27" layer="96"/>
</instance>
<instance part="C1" gate="G$1" x="304.8" y="210.82" smashed="yes" rot="R90">
<attribute name="NAME" x="304.292" y="205.74" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="304.292" y="212.09" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C2" gate="G$1" x="309.88" y="210.82" smashed="yes" rot="R90">
<attribute name="NAME" x="309.372" y="205.74" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="309.372" y="212.09" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C3" gate="G$1" x="325.12" y="162.56" smashed="yes" rot="R90">
<attribute name="NAME" x="324.612" y="157.48" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="324.612" y="163.83" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C4" gate="G$1" x="320.04" y="210.82" smashed="yes" rot="R90">
<attribute name="NAME" x="319.532" y="205.74" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="319.532" y="212.09" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C5" gate="G$1" x="325.12" y="210.82" smashed="yes" rot="R90">
<attribute name="NAME" x="324.612" y="205.74" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="324.612" y="212.09" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C6" gate="G$1" x="335.28" y="210.82" smashed="yes" rot="R90">
<attribute name="NAME" x="334.772" y="205.74" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="334.772" y="212.09" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C7" gate="G$1" x="340.36" y="210.82" smashed="yes" rot="R90">
<attribute name="NAME" x="339.852" y="205.74" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="339.852" y="212.09" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C8" gate="G$1" x="350.52" y="210.82" smashed="yes" rot="R90">
<attribute name="NAME" x="350.012" y="205.74" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="350.012" y="212.09" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C9" gate="G$1" x="355.6" y="210.82" smashed="yes" rot="R90">
<attribute name="NAME" x="355.092" y="205.74" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="355.092" y="212.09" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="L1" gate="G$1" x="109.22" y="170.18" smashed="yes" rot="R90">
<attribute name="NAME" x="108.712" y="164.084" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="108.712" y="173.228" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C10" gate="G$1" x="330.2" y="162.56" smashed="yes" rot="R90">
<attribute name="NAME" x="329.692" y="157.48" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="329.692" y="163.83" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="SUPPLY3" gate="GND" x="299.72" y="198.12" smashed="yes">
<attribute name="VALUE" x="297.815" y="194.945" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY4" gate="GND" x="325.12" y="147.32" smashed="yes">
<attribute name="VALUE" x="322.199" y="144.145" size="1.27" layer="96"/>
</instance>
<instance part="R1" gate="G$1" x="353.06" y="177.8" smashed="yes" rot="MR90">
<attribute name="NAME" x="352.5774" y="175.26" size="1.27" layer="95" rot="MR270"/>
<attribute name="VALUE" x="352.552" y="184.658" size="1.27" layer="96" rot="MR270"/>
</instance>
<instance part="R2" gate="G$1" x="360.68" y="167.64" smashed="yes" rot="MR180">
<attribute name="NAME" x="358.14" y="168.1226" size="1.27" layer="95" rot="MR0"/>
<attribute name="VALUE" x="366.776" y="168.148" size="1.27" layer="96" rot="MR0"/>
</instance>
<instance part="C11" gate="G$1" x="353.06" y="160.02" smashed="yes" rot="MR90">
<attribute name="NAME" x="353.568" y="154.94" size="1.27" layer="95" ratio="10" rot="MR90"/>
<attribute name="VALUE" x="353.568" y="161.29" size="1.27" layer="96" ratio="10" rot="MR90"/>
</instance>
<instance part="SW1" gate="G$1" x="368.3" y="160.02" smashed="yes" rot="MR90">
<attribute name="NAME" x="368.808" y="154.94" size="1.27" layer="95" ratio="15" rot="MR90"/>
<attribute name="VALUE" x="370.84" y="154.94" size="1.27" layer="96" ratio="15" rot="MR90"/>
</instance>
<instance part="+3V3" gate="G$1" x="353.06" y="187.96" smashed="yes" rot="MR0">
<attribute name="VALUE" x="355.854" y="188.214" size="1.27" layer="96" rot="MR0"/>
</instance>
<instance part="SUPPLY5" gate="GND" x="368.3" y="147.32" smashed="yes" rot="MR0">
<attribute name="VALUE" x="365.379" y="145.923" size="1.27" layer="96" rot="MR180"/>
</instance>
<instance part="R3" gate="G$1" x="322.58" y="127" smashed="yes" rot="R180">
<attribute name="NAME" x="316.484" y="127.4826" size="1.27" layer="95"/>
<attribute name="VALUE" x="325.12" y="127.508" size="1.27" layer="96"/>
</instance>
<instance part="X1" gate="-1" x="312.42" y="119.38" smashed="yes">
<attribute name="NAME" x="314.96" y="118.618" size="1.27" layer="95"/>
<attribute name="VALUE" x="311.658" y="120.777" size="1.27" layer="96"/>
</instance>
<instance part="X1" gate="-2" x="337.82" y="119.38" smashed="yes" rot="MR0">
<attribute name="NAME" x="335.28" y="118.618" size="1.27" layer="95" rot="MR0"/>
</instance>
<instance part="X1" gate="-3" x="312.42" y="116.84" smashed="yes">
<attribute name="NAME" x="314.96" y="116.078" size="1.27" layer="95"/>
</instance>
<instance part="X1" gate="-4" x="337.82" y="116.84" smashed="yes" rot="MR0">
<attribute name="NAME" x="335.28" y="116.078" size="1.27" layer="95" rot="MR0"/>
</instance>
<instance part="X1" gate="-5" x="312.42" y="114.3" smashed="yes">
<attribute name="NAME" x="314.96" y="113.538" size="1.27" layer="95"/>
</instance>
<instance part="X1" gate="-6" x="337.82" y="114.3" smashed="yes" rot="MR0">
<attribute name="NAME" x="335.28" y="113.538" size="1.27" layer="95" rot="MR0"/>
</instance>
<instance part="X1" gate="-7" x="312.42" y="111.76" smashed="yes">
<attribute name="NAME" x="314.96" y="110.998" size="1.27" layer="95"/>
</instance>
<instance part="X1" gate="-8" x="337.82" y="111.76" smashed="yes" rot="MR0">
<attribute name="NAME" x="335.28" y="110.998" size="1.27" layer="95" rot="MR0"/>
</instance>
<instance part="X1" gate="-9" x="312.42" y="109.22" smashed="yes">
<attribute name="NAME" x="314.96" y="108.458" size="1.27" layer="95"/>
</instance>
<instance part="X1" gate="-10" x="337.82" y="109.22" smashed="yes" rot="MR0">
<attribute name="NAME" x="335.28" y="108.458" size="1.27" layer="95" rot="MR0"/>
</instance>
<instance part="+3V5" gate="G$1" x="302.26" y="137.16" smashed="yes">
<attribute name="VALUE" x="299.72" y="132.08" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="SUPPLY6" gate="GND" x="294.64" y="99.06" smashed="yes">
<attribute name="VALUE" x="292.735" y="95.885" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY7" gate="GND" x="167.64" y="60.96" smashed="yes">
<attribute name="VALUE" x="165.735" y="57.785" size="1.27" layer="96"/>
</instance>
<instance part="C12" gate="G$1" x="289.56" y="210.82" smashed="yes" rot="R90">
<attribute name="NAME" x="289.052" y="205.74" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="289.052" y="212.09" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C13" gate="G$1" x="294.64" y="210.82" smashed="yes" rot="R90">
<attribute name="NAME" x="294.132" y="205.74" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="294.132" y="212.09" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C14" gate="G$1" x="274.32" y="210.82" smashed="yes" rot="R90">
<attribute name="NAME" x="273.812" y="205.74" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="273.812" y="212.09" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C15" gate="G$1" x="279.4" y="210.82" smashed="yes" rot="R90">
<attribute name="NAME" x="278.892" y="205.74" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="278.892" y="212.09" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C16" gate="G$1" x="259.08" y="210.82" smashed="yes" rot="R90">
<attribute name="NAME" x="258.572" y="205.74" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="258.572" y="212.09" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C17" gate="G$1" x="264.16" y="210.82" smashed="yes" rot="R90">
<attribute name="NAME" x="263.652" y="205.74" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="263.652" y="212.09" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="U1" gate="G$1" x="76.2" y="157.48" smashed="yes">
<attribute name="NAME" x="76.2" y="160.02" size="1.27" layer="95"/>
<attribute name="VALUE" x="48.26" y="157.48" size="1.27" layer="96"/>
</instance>
<instance part="X2" gate="G$1" x="281.94" y="170.18" smashed="yes">
<attribute name="NAME" x="280.924" y="175.006" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="275.336" y="173.228" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="C18" gate="G$1" x="274.32" y="162.56" smashed="yes" rot="R90">
<attribute name="NAME" x="273.812" y="157.48" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="273.812" y="163.83" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C19" gate="G$1" x="289.56" y="162.56" smashed="yes" rot="R90">
<attribute name="NAME" x="289.052" y="157.48" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="289.052" y="163.83" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="SUPPLY10" gate="GND" x="281.94" y="147.32" smashed="yes" rot="MR0">
<attribute name="VALUE" x="279.019" y="145.923" size="1.27" layer="96" rot="MR180"/>
</instance>
<instance part="C28" gate="G$1" x="289.56" y="111.76" smashed="yes" rot="R90">
<attribute name="NAME" x="289.052" y="106.68" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="289.052" y="113.03" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="R14" gate="G$1" x="53.34" y="88.9" smashed="yes" rot="MR0">
<attribute name="NAME" x="47.244" y="88.4174" size="1.27" layer="95" rot="MR180"/>
<attribute name="VALUE" x="55.88" y="88.392" size="1.27" layer="96" rot="MR180"/>
</instance>
<instance part="TP1" gate="G$1" x="40.64" y="88.9" smashed="yes" rot="R90">
<attribute name="NAME" x="33.274" y="87.884" size="1.27" layer="95"/>
</instance>
<instance part="TP2" gate="G$1" x="243.84" y="218.44" smashed="yes" rot="R90">
<attribute name="NAME" x="240.03" y="217.17" size="1.27" layer="95" rot="R90"/>
</instance>
<instance part="C29" gate="G$1" x="370.84" y="210.82" smashed="yes" rot="R90">
<attribute name="NAME" x="370.332" y="205.74" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="370.332" y="212.09" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C30" gate="G$1" x="365.76" y="210.82" smashed="yes" rot="R90">
<attribute name="NAME" x="365.252" y="205.74" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="365.252" y="212.09" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="TP3" gate="G$1" x="314.96" y="170.18" smashed="yes" rot="R90">
<attribute name="NAME" x="311.15" y="168.91" size="1.27" layer="95" rot="R90"/>
</instance>
<instance part="+3V9" gate="G$1" x="246.38" y="228.6" smashed="yes">
<attribute name="VALUE" x="243.078" y="229.108" size="1.27" layer="96"/>
</instance>
<instance part="+3V10" gate="G$1" x="317.5" y="177.8" smashed="yes">
<attribute name="VALUE" x="314.452" y="178.308" size="1.27" layer="96"/>
</instance>
<instance part="TP4" gate="G$1" x="243.84" y="203.2" smashed="yes" rot="R90">
<attribute name="NAME" x="240.03" y="201.93" size="1.27" layer="95" rot="R90"/>
</instance>
<instance part="C56" gate="G$1" x="254" y="210.82" smashed="yes" rot="R90">
<attribute name="NAME" x="253.492" y="205.74" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="253.492" y="212.09" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C57" gate="G$1" x="320.04" y="162.56" smashed="yes" rot="R90">
<attribute name="NAME" x="319.532" y="157.48" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="319.532" y="163.83" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="R57" gate="G$1" x="322.58" y="132.08" smashed="yes" rot="R180">
<attribute name="NAME" x="316.484" y="132.5626" size="1.27" layer="95"/>
<attribute name="VALUE" x="325.12" y="132.588" size="1.27" layer="96"/>
</instance>
<instance part="R58" gate="G$1" x="302.26" y="109.22" smashed="yes" rot="R180">
<attribute name="NAME" x="295.91" y="109.7026" size="1.27" layer="95"/>
<attribute name="VALUE" x="304.8" y="109.728" size="1.27" layer="96"/>
</instance>
<instance part="R59" gate="G$1" x="116.84" y="177.8" smashed="yes" rot="R270">
<attribute name="NAME" x="116.3574" y="171.45" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="116.332" y="180.34" size="1.27" layer="96" rot="R90"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="SUPPLY2" gate="GND" pin="GND"/>
<wire x1="111.76" y1="167.64" x2="147.32" y2="167.64" width="0.1524" layer="91"/>
<wire x1="147.32" y1="167.64" x2="152.4" y2="167.64" width="0.1524" layer="91"/>
<wire x1="152.4" y1="167.64" x2="152.4" y2="165.1" width="0.1524" layer="91"/>
<wire x1="111.76" y1="162.56" x2="111.76" y2="167.64" width="0.1524" layer="91"/>
<wire x1="147.32" y1="162.56" x2="147.32" y2="167.64" width="0.1524" layer="91"/>
<junction x="147.32" y="167.64"/>
<pinref part="U1" gate="G$1" pin="76"/>
<pinref part="U1" gate="G$1" pin="90"/>
</segment>
<segment>
<wire x1="71.12" y1="121.92" x2="66.04" y2="121.92" width="0.1524" layer="91"/>
<pinref part="SUPPLY1" gate="GND" pin="GND"/>
<wire x1="66.04" y1="121.92" x2="66.04" y2="88.9" width="0.1524" layer="91"/>
<wire x1="66.04" y1="88.9" x2="66.04" y2="66.04" width="0.1524" layer="91"/>
<wire x1="66.04" y1="66.04" x2="66.04" y2="63.5" width="0.1524" layer="91"/>
<wire x1="71.12" y1="88.9" x2="66.04" y2="88.9" width="0.1524" layer="91"/>
<junction x="66.04" y="88.9"/>
<wire x1="99.06" y1="71.12" x2="99.06" y2="66.04" width="0.1524" layer="91"/>
<wire x1="99.06" y1="66.04" x2="66.04" y2="66.04" width="0.1524" layer="91"/>
<junction x="66.04" y="66.04"/>
<pinref part="U1" gate="G$1" pin="11"/>
<pinref part="U1" gate="G$1" pin="24"/>
<pinref part="U1" gate="G$1" pin="31"/>
</segment>
<segment>
<pinref part="C3" gate="G$1" pin="1"/>
<wire x1="325.12" y1="154.94" x2="325.12" y2="157.48" width="0.1524" layer="91"/>
<pinref part="C10" gate="G$1" pin="1"/>
<wire x1="330.2" y1="157.48" x2="330.2" y2="154.94" width="0.1524" layer="91"/>
<wire x1="330.2" y1="154.94" x2="325.12" y2="154.94" width="0.1524" layer="91"/>
<pinref part="SUPPLY4" gate="GND" pin="GND"/>
<wire x1="325.12" y1="149.86" x2="325.12" y2="152.4" width="0.1524" layer="91"/>
<junction x="325.12" y="154.94"/>
<pinref part="C57" gate="G$1" pin="1"/>
<wire x1="325.12" y1="152.4" x2="325.12" y2="154.94" width="0.1524" layer="91"/>
<wire x1="320.04" y1="157.48" x2="320.04" y2="152.4" width="0.1524" layer="91"/>
<wire x1="320.04" y1="152.4" x2="325.12" y2="152.4" width="0.1524" layer="91"/>
<junction x="325.12" y="152.4"/>
</segment>
<segment>
<pinref part="SW1" gate="G$1" pin="1"/>
<wire x1="368.3" y1="154.94" x2="368.3" y2="152.4" width="0.1524" layer="91"/>
<wire x1="368.3" y1="152.4" x2="353.06" y2="152.4" width="0.1524" layer="91"/>
<pinref part="C11" gate="G$1" pin="1"/>
<wire x1="353.06" y1="152.4" x2="353.06" y2="154.94" width="0.1524" layer="91"/>
<pinref part="SUPPLY5" gate="GND" pin="GND"/>
<wire x1="368.3" y1="149.86" x2="368.3" y2="152.4" width="0.1524" layer="91"/>
<junction x="368.3" y="152.4"/>
</segment>
<segment>
<pinref part="X1" gate="-3" pin="S"/>
<wire x1="309.88" y1="116.84" x2="294.64" y2="116.84" width="0.1524" layer="91"/>
<wire x1="294.64" y1="116.84" x2="294.64" y2="114.3" width="0.1524" layer="91"/>
<pinref part="X1" gate="-5" pin="S"/>
<wire x1="294.64" y1="109.22" x2="294.64" y2="104.14" width="0.1524" layer="91"/>
<wire x1="294.64" y1="104.14" x2="294.64" y2="101.6" width="0.1524" layer="91"/>
<wire x1="309.88" y1="114.3" x2="294.64" y2="114.3" width="0.1524" layer="91"/>
<junction x="294.64" y="114.3"/>
<wire x1="294.64" y1="114.3" x2="294.64" y2="109.22" width="0.1524" layer="91"/>
<junction x="294.64" y="109.22"/>
<pinref part="SUPPLY6" gate="GND" pin="GND"/>
<pinref part="C28" gate="G$1" pin="1"/>
<wire x1="297.18" y1="109.22" x2="294.64" y2="109.22" width="0.1524" layer="91"/>
<wire x1="294.64" y1="104.14" x2="289.56" y2="104.14" width="0.1524" layer="91"/>
<wire x1="289.56" y1="104.14" x2="289.56" y2="106.68" width="0.1524" layer="91"/>
<junction x="294.64" y="104.14"/>
<pinref part="R58" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="162.56" y1="114.3" x2="167.64" y2="114.3" width="0.1524" layer="91"/>
<pinref part="SUPPLY7" gate="GND" pin="GND"/>
<wire x1="167.64" y1="114.3" x2="167.64" y2="66.04" width="0.1524" layer="91"/>
<wire x1="167.64" y1="66.04" x2="167.64" y2="63.5" width="0.1524" layer="91"/>
<wire x1="116.84" y1="71.12" x2="116.84" y2="66.04" width="0.1524" layer="91"/>
<wire x1="116.84" y1="66.04" x2="147.32" y2="66.04" width="0.1524" layer="91"/>
<junction x="167.64" y="66.04"/>
<pinref part="U1" gate="G$1" pin="38"/>
<pinref part="U1" gate="G$1" pin="62"/>
<pinref part="U1" gate="G$1" pin="50"/>
<wire x1="147.32" y1="66.04" x2="167.64" y2="66.04" width="0.1524" layer="91"/>
<wire x1="147.32" y1="71.12" x2="147.32" y2="66.04" width="0.1524" layer="91"/>
<junction x="147.32" y="66.04"/>
</segment>
<segment>
<pinref part="X2" gate="G$1" pin="3"/>
<pinref part="SUPPLY10" gate="GND" pin="GND"/>
<wire x1="281.94" y1="165.1" x2="281.94" y2="154.94" width="0.1524" layer="91"/>
<pinref part="C18" gate="G$1" pin="1"/>
<wire x1="281.94" y1="154.94" x2="281.94" y2="152.4" width="0.1524" layer="91"/>
<wire x1="281.94" y1="152.4" x2="281.94" y2="149.86" width="0.1524" layer="91"/>
<wire x1="274.32" y1="157.48" x2="274.32" y2="154.94" width="0.1524" layer="91"/>
<wire x1="274.32" y1="154.94" x2="281.94" y2="154.94" width="0.1524" layer="91"/>
<pinref part="C19" gate="G$1" pin="1"/>
<wire x1="289.56" y1="157.48" x2="289.56" y2="152.4" width="0.1524" layer="91"/>
<wire x1="289.56" y1="152.4" x2="281.94" y2="152.4" width="0.1524" layer="91"/>
<junction x="281.94" y="154.94"/>
<junction x="281.94" y="152.4"/>
</segment>
<segment>
<pinref part="C2" gate="G$1" pin="1"/>
<wire x1="304.8" y1="203.2" x2="309.88" y2="203.2" width="0.1524" layer="91"/>
<wire x1="309.88" y1="203.2" x2="309.88" y2="205.74" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="1"/>
<wire x1="304.8" y1="203.2" x2="304.8" y2="205.74" width="0.1524" layer="91"/>
<pinref part="C5" gate="G$1" pin="1"/>
<wire x1="320.04" y1="203.2" x2="325.12" y2="203.2" width="0.1524" layer="91"/>
<wire x1="325.12" y1="203.2" x2="325.12" y2="205.74" width="0.1524" layer="91"/>
<pinref part="C4" gate="G$1" pin="1"/>
<wire x1="320.04" y1="203.2" x2="320.04" y2="205.74" width="0.1524" layer="91"/>
<wire x1="309.88" y1="203.2" x2="320.04" y2="203.2" width="0.1524" layer="91"/>
<junction x="309.88" y="203.2"/>
<junction x="320.04" y="203.2"/>
<pinref part="C7" gate="G$1" pin="1"/>
<wire x1="335.28" y1="203.2" x2="340.36" y2="203.2" width="0.1524" layer="91"/>
<wire x1="340.36" y1="203.2" x2="340.36" y2="205.74" width="0.1524" layer="91"/>
<pinref part="C6" gate="G$1" pin="1"/>
<wire x1="335.28" y1="203.2" x2="335.28" y2="205.74" width="0.1524" layer="91"/>
<wire x1="325.12" y1="203.2" x2="335.28" y2="203.2" width="0.1524" layer="91"/>
<junction x="335.28" y="203.2"/>
<junction x="325.12" y="203.2"/>
<pinref part="C9" gate="G$1" pin="1"/>
<wire x1="350.52" y1="203.2" x2="355.6" y2="203.2" width="0.1524" layer="91"/>
<wire x1="355.6" y1="203.2" x2="355.6" y2="205.74" width="0.1524" layer="91"/>
<pinref part="C8" gate="G$1" pin="1"/>
<wire x1="350.52" y1="203.2" x2="350.52" y2="205.74" width="0.1524" layer="91"/>
<wire x1="337.82" y1="203.2" x2="340.36" y2="203.2" width="0.1524" layer="91"/>
<junction x="350.52" y="203.2"/>
<junction x="340.36" y="203.2"/>
<wire x1="340.36" y1="203.2" x2="350.52" y2="203.2" width="0.1524" layer="91"/>
<junction x="304.8" y="203.2"/>
<pinref part="C13" gate="G$1" pin="1"/>
<wire x1="289.56" y1="203.2" x2="294.64" y2="203.2" width="0.1524" layer="91"/>
<wire x1="294.64" y1="203.2" x2="294.64" y2="205.74" width="0.1524" layer="91"/>
<pinref part="C12" gate="G$1" pin="1"/>
<wire x1="289.56" y1="203.2" x2="289.56" y2="205.74" width="0.1524" layer="91"/>
<wire x1="279.4" y1="203.2" x2="289.56" y2="203.2" width="0.1524" layer="91"/>
<junction x="289.56" y="203.2"/>
<pinref part="C15" gate="G$1" pin="1"/>
<wire x1="274.32" y1="203.2" x2="279.4" y2="203.2" width="0.1524" layer="91"/>
<wire x1="279.4" y1="203.2" x2="279.4" y2="205.74" width="0.1524" layer="91"/>
<pinref part="C14" gate="G$1" pin="1"/>
<wire x1="274.32" y1="203.2" x2="274.32" y2="205.74" width="0.1524" layer="91"/>
<wire x1="264.16" y1="203.2" x2="274.32" y2="203.2" width="0.1524" layer="91"/>
<junction x="274.32" y="203.2"/>
<junction x="279.4" y="203.2"/>
<pinref part="C17" gate="G$1" pin="1"/>
<wire x1="259.08" y1="203.2" x2="264.16" y2="203.2" width="0.1524" layer="91"/>
<wire x1="264.16" y1="203.2" x2="264.16" y2="205.74" width="0.1524" layer="91"/>
<pinref part="C16" gate="G$1" pin="1"/>
<wire x1="259.08" y1="203.2" x2="259.08" y2="205.74" width="0.1524" layer="91"/>
<junction x="264.16" y="203.2"/>
<wire x1="294.64" y1="203.2" x2="299.72" y2="203.2" width="0.1524" layer="91"/>
<junction x="294.64" y="203.2"/>
<wire x1="299.72" y1="203.2" x2="304.8" y2="203.2" width="0.1524" layer="91"/>
<wire x1="355.6" y1="203.2" x2="365.76" y2="203.2" width="0.1524" layer="91"/>
<junction x="355.6" y="203.2"/>
<pinref part="C30" gate="G$1" pin="1"/>
<wire x1="365.76" y1="203.2" x2="365.76" y2="205.74" width="0.1524" layer="91"/>
<pinref part="C29" gate="G$1" pin="1"/>
<wire x1="365.76" y1="203.2" x2="370.84" y2="203.2" width="0.1524" layer="91"/>
<wire x1="370.84" y1="203.2" x2="370.84" y2="205.74" width="0.1524" layer="91"/>
<junction x="365.76" y="203.2"/>
<pinref part="SUPPLY3" gate="GND" pin="GND"/>
<wire x1="299.72" y1="200.66" x2="299.72" y2="203.2" width="0.1524" layer="91"/>
<junction x="299.72" y="203.2"/>
<pinref part="TP4" gate="G$1" pin="PP"/>
<wire x1="243.84" y1="203.2" x2="254" y2="203.2" width="0.1524" layer="91"/>
<junction x="259.08" y="203.2"/>
<pinref part="C56" gate="G$1" pin="1"/>
<wire x1="254" y1="203.2" x2="259.08" y2="203.2" width="0.1524" layer="91"/>
<wire x1="254" y1="205.74" x2="254" y2="203.2" width="0.1524" layer="91"/>
<junction x="254" y="203.2"/>
</segment>
</net>
<net name="RSTN" class="0">
<segment>
<wire x1="116.84" y1="162.56" x2="116.84" y2="172.72" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="88"/>
<pinref part="R59" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="355.6" y1="167.64" x2="353.06" y2="167.64" width="0.1524" layer="91"/>
<pinref part="C11" gate="G$1" pin="2"/>
<wire x1="353.06" y1="167.64" x2="353.06" y2="165.1" width="0.1524" layer="91"/>
<pinref part="R1" gate="G$1" pin="1"/>
<wire x1="353.06" y1="172.72" x2="353.06" y2="170.18" width="0.1524" layer="91"/>
<junction x="353.06" y="167.64"/>
<wire x1="353.06" y1="170.18" x2="353.06" y2="167.64" width="0.1524" layer="91"/>
<wire x1="353.06" y1="170.18" x2="347.98" y2="170.18" width="0.1524" layer="91"/>
<junction x="353.06" y="170.18"/>
<label x="347.98" y="170.18" size="1.27" layer="95" rot="MR0" xref="yes"/>
</segment>
<segment>
<pinref part="X1" gate="-10" pin="S"/>
<wire x1="340.36" y1="109.22" x2="347.98" y2="109.22" width="0.1524" layer="91"/>
<label x="347.98" y="109.22" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="VDD_A" class="0">
<segment>
<wire x1="71.12" y1="119.38" x2="45.72" y2="119.38" width="0.1524" layer="91"/>
<label x="40.64" y="119.38" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="U1" gate="G$1" pin="12"/>
<pinref part="R14" gate="G$1" pin="2"/>
<wire x1="45.72" y1="119.38" x2="40.64" y2="119.38" width="0.1524" layer="91"/>
<wire x1="45.72" y1="119.38" x2="45.72" y2="88.9" width="0.1524" layer="91"/>
<junction x="45.72" y="119.38"/>
<pinref part="TP1" gate="G$1" pin="PP"/>
<wire x1="45.72" y1="88.9" x2="48.26" y2="88.9" width="0.1524" layer="91"/>
<wire x1="40.64" y1="88.9" x2="45.72" y2="88.9" width="0.1524" layer="91"/>
<junction x="45.72" y="88.9"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="L1" gate="G$1" pin="1"/>
<wire x1="109.22" y1="162.56" x2="109.22" y2="165.1" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="SW1" gate="G$1" pin="2"/>
<wire x1="368.3" y1="165.1" x2="368.3" y2="167.64" width="0.1524" layer="91"/>
<pinref part="R2" gate="G$1" pin="2"/>
<wire x1="368.3" y1="167.64" x2="365.76" y2="167.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<pinref part="R1" gate="G$1" pin="2"/>
<wire x1="353.06" y1="182.88" x2="353.06" y2="185.42" width="0.1524" layer="91"/>
<pinref part="+3V3" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="X1" gate="-1" pin="S"/>
<wire x1="309.88" y1="119.38" x2="302.26" y2="119.38" width="0.1524" layer="91"/>
<wire x1="302.26" y1="119.38" x2="302.26" y2="127" width="0.1524" layer="91"/>
<pinref part="+3V5" gate="G$1" pin="+3V3"/>
<wire x1="302.26" y1="127" x2="302.26" y2="132.08" width="0.1524" layer="91"/>
<wire x1="302.26" y1="132.08" x2="302.26" y2="134.62" width="0.1524" layer="91"/>
<wire x1="302.26" y1="119.38" x2="289.56" y2="119.38" width="0.1524" layer="91"/>
<junction x="302.26" y="119.38"/>
<pinref part="C28" gate="G$1" pin="2"/>
<wire x1="289.56" y1="119.38" x2="289.56" y2="116.84" width="0.1524" layer="91"/>
<pinref part="R57" gate="G$1" pin="2"/>
<wire x1="317.5" y1="132.08" x2="302.26" y2="132.08" width="0.1524" layer="91"/>
<junction x="302.26" y="132.08"/>
<pinref part="R3" gate="G$1" pin="2"/>
<wire x1="317.5" y1="127" x2="302.26" y2="127" width="0.1524" layer="91"/>
<junction x="302.26" y="127"/>
</segment>
<segment>
<pinref part="C2" gate="G$1" pin="2"/>
<wire x1="304.8" y1="218.44" x2="309.88" y2="218.44" width="0.1524" layer="91"/>
<wire x1="309.88" y1="218.44" x2="309.88" y2="215.9" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="2"/>
<wire x1="304.8" y1="215.9" x2="304.8" y2="218.44" width="0.1524" layer="91"/>
<pinref part="C5" gate="G$1" pin="2"/>
<wire x1="309.88" y1="218.44" x2="320.04" y2="218.44" width="0.1524" layer="91"/>
<wire x1="320.04" y1="218.44" x2="325.12" y2="218.44" width="0.1524" layer="91"/>
<wire x1="325.12" y1="218.44" x2="325.12" y2="215.9" width="0.1524" layer="91"/>
<pinref part="C4" gate="G$1" pin="2"/>
<wire x1="320.04" y1="215.9" x2="320.04" y2="218.44" width="0.1524" layer="91"/>
<junction x="320.04" y="218.44"/>
<junction x="309.88" y="218.44"/>
<pinref part="C7" gate="G$1" pin="2"/>
<wire x1="325.12" y1="218.44" x2="335.28" y2="218.44" width="0.1524" layer="91"/>
<wire x1="335.28" y1="218.44" x2="340.36" y2="218.44" width="0.1524" layer="91"/>
<wire x1="340.36" y1="218.44" x2="340.36" y2="215.9" width="0.1524" layer="91"/>
<pinref part="C6" gate="G$1" pin="2"/>
<wire x1="335.28" y1="215.9" x2="335.28" y2="218.44" width="0.1524" layer="91"/>
<junction x="335.28" y="218.44"/>
<junction x="325.12" y="218.44"/>
<pinref part="C9" gate="G$1" pin="2"/>
<wire x1="337.82" y1="218.44" x2="340.36" y2="218.44" width="0.1524" layer="91"/>
<wire x1="340.36" y1="218.44" x2="350.52" y2="218.44" width="0.1524" layer="91"/>
<wire x1="350.52" y1="218.44" x2="355.6" y2="218.44" width="0.1524" layer="91"/>
<wire x1="355.6" y1="218.44" x2="355.6" y2="215.9" width="0.1524" layer="91"/>
<pinref part="C8" gate="G$1" pin="2"/>
<wire x1="350.52" y1="215.9" x2="350.52" y2="218.44" width="0.1524" layer="91"/>
<junction x="350.52" y="218.44"/>
<junction x="340.36" y="218.44"/>
<pinref part="C13" gate="G$1" pin="2"/>
<wire x1="279.4" y1="218.44" x2="289.56" y2="218.44" width="0.1524" layer="91"/>
<wire x1="289.56" y1="218.44" x2="294.64" y2="218.44" width="0.1524" layer="91"/>
<wire x1="294.64" y1="218.44" x2="294.64" y2="215.9" width="0.1524" layer="91"/>
<pinref part="C12" gate="G$1" pin="2"/>
<wire x1="289.56" y1="215.9" x2="289.56" y2="218.44" width="0.1524" layer="91"/>
<junction x="289.56" y="218.44"/>
<pinref part="C15" gate="G$1" pin="2"/>
<wire x1="264.16" y1="218.44" x2="274.32" y2="218.44" width="0.1524" layer="91"/>
<wire x1="274.32" y1="218.44" x2="279.4" y2="218.44" width="0.1524" layer="91"/>
<wire x1="279.4" y1="218.44" x2="279.4" y2="215.9" width="0.1524" layer="91"/>
<pinref part="C14" gate="G$1" pin="2"/>
<wire x1="274.32" y1="215.9" x2="274.32" y2="218.44" width="0.1524" layer="91"/>
<junction x="274.32" y="218.44"/>
<junction x="279.4" y="218.44"/>
<pinref part="C17" gate="G$1" pin="2"/>
<wire x1="259.08" y1="218.44" x2="264.16" y2="218.44" width="0.1524" layer="91"/>
<wire x1="264.16" y1="218.44" x2="264.16" y2="215.9" width="0.1524" layer="91"/>
<pinref part="C16" gate="G$1" pin="2"/>
<wire x1="259.08" y1="215.9" x2="259.08" y2="218.44" width="0.1524" layer="91"/>
<junction x="264.16" y="218.44"/>
<wire x1="294.64" y1="218.44" x2="304.8" y2="218.44" width="0.1524" layer="91"/>
<junction x="294.64" y="218.44"/>
<junction x="304.8" y="218.44"/>
<junction x="259.08" y="218.44"/>
<pinref part="TP2" gate="G$1" pin="PP"/>
<wire x1="243.84" y1="218.44" x2="246.38" y2="218.44" width="0.1524" layer="91"/>
<wire x1="246.38" y1="218.44" x2="254" y2="218.44" width="0.1524" layer="91"/>
<wire x1="254" y1="218.44" x2="259.08" y2="218.44" width="0.1524" layer="91"/>
<wire x1="355.6" y1="218.44" x2="365.76" y2="218.44" width="0.1524" layer="91"/>
<junction x="355.6" y="218.44"/>
<pinref part="C30" gate="G$1" pin="2"/>
<wire x1="365.76" y1="215.9" x2="365.76" y2="218.44" width="0.1524" layer="91"/>
<pinref part="C29" gate="G$1" pin="2"/>
<wire x1="365.76" y1="218.44" x2="370.84" y2="218.44" width="0.1524" layer="91"/>
<wire x1="370.84" y1="218.44" x2="370.84" y2="215.9" width="0.1524" layer="91"/>
<junction x="365.76" y="218.44"/>
<pinref part="+3V9" gate="G$1" pin="+3V3"/>
<wire x1="246.38" y1="226.06" x2="246.38" y2="218.44" width="0.1524" layer="91"/>
<junction x="246.38" y="218.44"/>
<pinref part="C56" gate="G$1" pin="2"/>
<wire x1="254" y1="215.9" x2="254" y2="218.44" width="0.1524" layer="91"/>
<junction x="254" y="218.44"/>
</segment>
<segment>
<pinref part="C10" gate="G$1" pin="2"/>
<wire x1="330.2" y1="167.64" x2="330.2" y2="170.18" width="0.1524" layer="91"/>
<wire x1="330.2" y1="170.18" x2="325.12" y2="170.18" width="0.1524" layer="91"/>
<pinref part="C3" gate="G$1" pin="2"/>
<wire x1="325.12" y1="167.64" x2="325.12" y2="170.18" width="0.1524" layer="91"/>
<junction x="325.12" y="170.18"/>
<wire x1="325.12" y1="170.18" x2="320.04" y2="170.18" width="0.1524" layer="91"/>
<pinref part="TP3" gate="G$1" pin="PP"/>
<pinref part="+3V10" gate="G$1" pin="+3V3"/>
<wire x1="320.04" y1="170.18" x2="317.5" y2="170.18" width="0.1524" layer="91"/>
<wire x1="317.5" y1="170.18" x2="314.96" y2="170.18" width="0.1524" layer="91"/>
<wire x1="317.5" y1="175.26" x2="317.5" y2="170.18" width="0.1524" layer="91"/>
<junction x="317.5" y="170.18"/>
<pinref part="C57" gate="G$1" pin="2"/>
<wire x1="320.04" y1="167.64" x2="320.04" y2="170.18" width="0.1524" layer="91"/>
<junction x="320.04" y="170.18"/>
</segment>
<segment>
<wire x1="162.56" y1="116.84" x2="208.28" y2="116.84" width="0.1524" layer="91"/>
<label x="208.28" y="116.84" size="1.27" layer="95" xref="yes"/>
<pinref part="U1" gate="G$1" pin="63"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="51"/>
<wire x1="162.56" y1="86.36" x2="208.28" y2="86.36" width="0.1524" layer="91"/>
<label x="208.28" y="86.36" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="119.38" y1="71.12" x2="119.38" y2="48.26" width="0.1524" layer="91"/>
<label x="119.38" y="48.26" size="1.27" layer="95" rot="R270" xref="yes"/>
<pinref part="U1" gate="G$1" pin="39"/>
</segment>
<segment>
<wire x1="96.52" y1="71.12" x2="96.52" y2="48.26" width="0.1524" layer="91"/>
<label x="96.52" y="48.26" size="1.27" layer="95" rot="R270" xref="yes"/>
<pinref part="U1" gate="G$1" pin="30"/>
</segment>
<segment>
<wire x1="71.12" y1="86.36" x2="60.96" y2="86.36" width="0.1524" layer="91"/>
<label x="40.64" y="86.36" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="U1" gate="G$1" pin="25"/>
<pinref part="R14" gate="G$1" pin="1"/>
<wire x1="60.96" y1="86.36" x2="40.64" y2="86.36" width="0.1524" layer="91"/>
<wire x1="58.42" y1="88.9" x2="60.96" y2="88.9" width="0.1524" layer="91"/>
<wire x1="60.96" y1="88.9" x2="60.96" y2="86.36" width="0.1524" layer="91"/>
<junction x="60.96" y="86.36"/>
</segment>
<segment>
<wire x1="106.68" y1="162.56" x2="106.68" y2="187.96" width="0.1524" layer="91"/>
<label x="106.68" y="187.96" size="1.27" layer="95" rot="R90" xref="yes"/>
<pinref part="U1" gate="G$1" pin="92"/>
</segment>
<segment>
<wire x1="144.78" y1="162.56" x2="144.78" y2="187.96" width="0.1524" layer="91"/>
<label x="144.78" y="187.96" size="1.27" layer="95" rot="R90" xref="yes"/>
<pinref part="U1" gate="G$1" pin="77"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="89"/>
<wire x1="114.3" y1="162.56" x2="114.3" y2="177.8" width="0.1524" layer="91"/>
<pinref part="L1" gate="G$1" pin="2"/>
<wire x1="114.3" y1="177.8" x2="114.3" y2="187.96" width="0.1524" layer="91"/>
<wire x1="109.22" y1="175.26" x2="109.22" y2="177.8" width="0.1524" layer="91"/>
<wire x1="109.22" y1="177.8" x2="114.3" y2="177.8" width="0.1524" layer="91"/>
<junction x="114.3" y="177.8"/>
<label x="114.3" y="187.96" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="SWCLK" class="0">
<segment>
<label x="104.14" y="187.96" size="1.27" layer="95" rot="R90" xref="yes"/>
<wire x1="104.14" y1="162.56" x2="104.14" y2="187.96" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="93"/>
</segment>
<segment>
<pinref part="X1" gate="-4" pin="S"/>
<wire x1="340.36" y1="116.84" x2="342.9" y2="116.84" width="0.1524" layer="91"/>
<label x="347.98" y="116.84" size="1.27" layer="95" xref="yes"/>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="342.9" y1="116.84" x2="347.98" y2="116.84" width="0.1524" layer="91"/>
<wire x1="327.66" y1="127" x2="342.9" y2="127" width="0.1524" layer="91"/>
<wire x1="342.9" y1="127" x2="342.9" y2="116.84" width="0.1524" layer="91"/>
<junction x="342.9" y="116.84"/>
</segment>
</net>
<net name="SWO" class="0">
<segment>
<wire x1="99.06" y1="162.56" x2="99.06" y2="187.96" width="0.1524" layer="91"/>
<label x="99.06" y="187.96" size="1.27" layer="95" rot="R90" xref="yes"/>
<pinref part="U1" gate="G$1" pin="95"/>
</segment>
<segment>
<pinref part="X1" gate="-6" pin="S"/>
<wire x1="340.36" y1="114.3" x2="347.98" y2="114.3" width="0.1524" layer="91"/>
<label x="347.98" y="114.3" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="SWDIO" class="0">
<segment>
<pinref part="X1" gate="-2" pin="S"/>
<wire x1="340.36" y1="119.38" x2="345.44" y2="119.38" width="0.1524" layer="91"/>
<label x="347.98" y="119.38" size="1.27" layer="95" xref="yes"/>
<pinref part="R57" gate="G$1" pin="1"/>
<wire x1="345.44" y1="119.38" x2="347.98" y2="119.38" width="0.1524" layer="91"/>
<wire x1="327.66" y1="132.08" x2="345.44" y2="132.08" width="0.1524" layer="91"/>
<wire x1="345.44" y1="132.08" x2="345.44" y2="119.38" width="0.1524" layer="91"/>
<junction x="345.44" y="119.38"/>
</segment>
<segment>
<wire x1="101.6" y1="162.56" x2="101.6" y2="187.96" width="0.1524" layer="91"/>
<label x="101.6" y="187.96" size="1.27" layer="95" rot="R90" xref="yes"/>
<pinref part="U1" gate="G$1" pin="94"/>
</segment>
</net>
<net name="COM0_SDA" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="26"/>
<wire x1="86.36" y1="71.12" x2="86.36" y2="60.96" width="0.1524" layer="91"/>
<label x="86.36" y="60.96" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
</net>
<net name="COM0_SCL" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="27"/>
<wire x1="88.9" y1="71.12" x2="88.9" y2="60.96" width="0.1524" layer="91"/>
<label x="88.9" y="60.96" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
</net>
<net name="COM_0.2" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="28"/>
<wire x1="91.44" y1="71.12" x2="91.44" y2="60.96" width="0.1524" layer="91"/>
<label x="91.44" y="60.96" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
</net>
<net name="COM_0.3" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="29"/>
<wire x1="93.98" y1="71.12" x2="93.98" y2="60.96" width="0.1524" layer="91"/>
<label x="93.98" y="60.96" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
</net>
<net name="COM4_MISO" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="34"/>
<wire x1="106.68" y1="71.12" x2="106.68" y2="60.96" width="0.1524" layer="91"/>
<label x="106.68" y="60.96" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
</net>
<net name="COM4_SCK" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="35"/>
<wire x1="109.22" y1="71.12" x2="109.22" y2="60.96" width="0.1524" layer="91"/>
<label x="109.22" y="60.96" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
</net>
<net name="COM_CSN" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="36"/>
<wire x1="111.76" y1="71.12" x2="111.76" y2="60.96" width="0.1524" layer="91"/>
<label x="111.76" y="60.96" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
</net>
<net name="COM_4.3" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="37"/>
<wire x1="114.3" y1="71.12" x2="114.3" y2="60.96" width="0.1524" layer="91"/>
<label x="114.3" y="60.96" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
</net>
<net name="COM7_TXD_OUT" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="42"/>
<wire x1="127" y1="71.12" x2="127" y2="60.96" width="0.1524" layer="91"/>
<label x="127" y="60.96" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
</net>
<net name="COM7_RXD_IN" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="43"/>
<wire x1="129.54" y1="71.12" x2="129.54" y2="60.96" width="0.1524" layer="91"/>
<label x="129.54" y="60.96" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
</net>
<net name="COM7_EN" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="44"/>
<wire x1="132.08" y1="71.12" x2="132.08" y2="60.96" width="0.1524" layer="91"/>
<label x="132.08" y="60.96" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
</net>
<net name="COM_7.3" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="45"/>
<wire x1="134.62" y1="71.12" x2="134.62" y2="60.96" width="0.1524" layer="91"/>
<label x="134.62" y="60.96" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
</net>
<net name="COM2_SDA" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="46"/>
<wire x1="137.16" y1="71.12" x2="137.16" y2="60.96" width="0.1524" layer="91"/>
<label x="137.16" y="60.96" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
</net>
<net name="COM2_SCL" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="47"/>
<wire x1="139.7" y1="71.12" x2="139.7" y2="60.96" width="0.1524" layer="91"/>
<label x="139.7" y="60.96" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
</net>
<net name="COM_2.2" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="48"/>
<wire x1="142.24" y1="71.12" x2="142.24" y2="60.96" width="0.1524" layer="91"/>
<label x="142.24" y="60.96" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
</net>
<net name="COM_2.3" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="49"/>
<wire x1="144.78" y1="71.12" x2="144.78" y2="60.96" width="0.1524" layer="91"/>
<label x="144.78" y="60.96" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
</net>
<net name="COM3_RXD_IN" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="52"/>
<wire x1="162.56" y1="88.9" x2="193.04" y2="88.9" width="0.1524" layer="91"/>
<label x="193.04" y="88.9" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="COM3_TXD_OUT" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="53"/>
<wire x1="162.56" y1="91.44" x2="193.04" y2="91.44" width="0.1524" layer="91"/>
<label x="193.04" y="91.44" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="COM6_MOSI" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="56"/>
<wire x1="162.56" y1="99.06" x2="193.04" y2="99.06" width="0.1524" layer="91"/>
<label x="193.04" y="99.06" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="COM5_TXD_OUT" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="73"/>
<wire x1="162.56" y1="142.24" x2="193.04" y2="142.24" width="0.1524" layer="91"/>
<label x="193.04" y="142.24" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="COM5_RXD_IN" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="72"/>
<wire x1="162.56" y1="139.7" x2="193.04" y2="139.7" width="0.1524" layer="91"/>
<label x="193.04" y="139.7" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="XOUT1" class="0">
<segment>
<pinref part="X2" gate="G$1" pin="1"/>
<wire x1="276.86" y1="170.18" x2="274.32" y2="170.18" width="0.1524" layer="91"/>
<pinref part="C18" gate="G$1" pin="2"/>
<wire x1="274.32" y1="170.18" x2="269.24" y2="170.18" width="0.1524" layer="91"/>
<wire x1="274.32" y1="167.64" x2="274.32" y2="170.18" width="0.1524" layer="91"/>
<junction x="274.32" y="170.18"/>
<label x="269.24" y="170.18" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="79"/>
<wire x1="139.7" y1="162.56" x2="139.7" y2="172.72" width="0.1524" layer="91"/>
<label x="139.7" y="172.72" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="XIN1" class="0">
<segment>
<pinref part="X2" gate="G$1" pin="2"/>
<wire x1="287.02" y1="170.18" x2="289.56" y2="170.18" width="0.1524" layer="91"/>
<pinref part="C19" gate="G$1" pin="2"/>
<wire x1="289.56" y1="170.18" x2="294.64" y2="170.18" width="0.1524" layer="91"/>
<wire x1="289.56" y1="167.64" x2="289.56" y2="170.18" width="0.1524" layer="91"/>
<junction x="289.56" y="170.18"/>
<label x="294.64" y="170.18" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="78"/>
<wire x1="142.24" y1="162.56" x2="142.24" y2="172.72" width="0.1524" layer="91"/>
<label x="142.24" y="172.72" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="COM5_EN" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="71"/>
<wire x1="162.56" y1="137.16" x2="193.04" y2="137.16" width="0.1524" layer="91"/>
<label x="193.04" y="137.16" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="COM4_MOSI" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="33"/>
<wire x1="104.14" y1="71.12" x2="104.14" y2="60.96" width="0.1524" layer="91"/>
<label x="104.14" y="60.96" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
</net>
<net name="COM1_TXD_OUT" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="1"/>
<wire x1="71.12" y1="147.32" x2="40.64" y2="147.32" width="0.1524" layer="91"/>
<label x="40.64" y="147.32" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="COM1_RXD_IN" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="2"/>
<wire x1="71.12" y1="144.78" x2="40.64" y2="144.78" width="0.1524" layer="91"/>
<label x="40.64" y="144.78" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="COM4_RXD_IN" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="16"/>
<wire x1="71.12" y1="109.22" x2="40.64" y2="109.22" width="0.1524" layer="91"/>
<label x="40.64" y="109.22" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="COM4_TXD_OUT" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="15"/>
<wire x1="71.12" y1="111.76" x2="40.64" y2="111.76" width="0.1524" layer="91"/>
<label x="40.64" y="111.76" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="COM1_EN" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="3"/>
<wire x1="71.12" y1="142.24" x2="40.64" y2="142.24" width="0.1524" layer="91"/>
<label x="40.64" y="142.24" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="COM4_EN" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="17"/>
<wire x1="71.12" y1="106.68" x2="40.64" y2="106.68" width="0.1524" layer="91"/>
<label x="40.64" y="106.68" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="COM6_MISO" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="22"/>
<wire x1="71.12" y1="93.98" x2="40.64" y2="93.98" width="0.1524" layer="91"/>
<label x="40.64" y="93.98" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="COM6_SCK" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="21"/>
<wire x1="71.12" y1="96.52" x2="40.64" y2="96.52" width="0.1524" layer="91"/>
<label x="40.64" y="96.52" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="COM6_CSN" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="23"/>
<wire x1="71.12" y1="91.44" x2="40.64" y2="91.44" width="0.1524" layer="91"/>
<label x="40.64" y="91.44" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$151" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="54"/>
<wire x1="162.56" y1="93.98" x2="193.04" y2="93.98" width="0.1524" layer="91"/>
<label x="193.04" y="93.98" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="X1" gate="-9" pin="S"/>
<wire x1="309.88" y1="109.22" x2="307.34" y2="109.22" width="0.1524" layer="91"/>
<pinref part="R58" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="R59" gate="G$1" pin="1"/>
<wire x1="116.84" y1="182.88" x2="116.84" y2="187.96" width="0.1524" layer="91"/>
<label x="116.84" y="187.96" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="CTRL4" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="86"/>
<wire x1="121.92" y1="162.56" x2="121.92" y2="177.8" width="0.1524" layer="91"/>
<label x="121.92" y="177.8" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="CTRL2" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="87"/>
<wire x1="119.38" y1="162.56" x2="119.38" y2="177.8" width="0.1524" layer="91"/>
<label x="119.38" y="177.8" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="CTRL3" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="85"/>
<wire x1="124.46" y1="162.56" x2="124.46" y2="177.8" width="0.1524" layer="91"/>
<label x="124.46" y="177.8" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="CTRL1" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="84"/>
<wire x1="127" y1="162.56" x2="127" y2="177.8" width="0.1524" layer="91"/>
<label x="127" y="177.8" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
<text x="127.762" y="233.426" size="1.27" layer="97">SBRT3U45SAF-13/SBRT3U45SA-13</text>
</plain>
<instances>
<instance part="U$4" gate="G$1" x="0" y="0" smashed="yes">
<attribute name="DRAWING_NAME" x="317.5" y="10.16" size="1.27" layer="94" ratio="15"/>
<attribute name="SHEET" x="332.486" y="5.842" size="1.27" layer="94" ratio="15"/>
</instance>
<instance part="C87" gate="G$1" x="43.18" y="50.8" smashed="yes" rot="R90">
<attribute name="NAME" x="42.672" y="45.72" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="42.672" y="52.07" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C88" gate="G$1" x="99.06" y="50.8" smashed="yes" rot="R90">
<attribute name="NAME" x="98.552" y="45.72" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="98.552" y="52.07" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C89" gate="G$1" x="121.92" y="50.8" smashed="yes" rot="R90">
<attribute name="NAME" x="121.412" y="45.72" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="121.412" y="52.07" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="R131" gate="G$1" x="78.74" y="50.8" smashed="yes" rot="R90">
<attribute name="NAME" x="78.2574" y="43.942" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="78.232" y="53.34" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R132" gate="G$1" x="83.82" y="35.56" smashed="yes" rot="R90">
<attribute name="NAME" x="85.344" y="36.5506" size="1.27" layer="95"/>
<attribute name="VALUE" x="85.344" y="35.052" size="1.27" layer="96"/>
</instance>
<instance part="Q5" gate="G$1" x="111.76" y="55.88" smashed="yes" rot="R90">
<attribute name="NAME" x="104.648" y="63.754" size="1.27" layer="95" ratio="15" rot="MR180"/>
<attribute name="VALUE" x="104.648" y="61.976" size="1.27" layer="96" ratio="15" rot="MR180"/>
</instance>
<instance part="Q6" gate="G$1" x="53.34" y="55.88" smashed="yes" rot="MR90">
<attribute name="NAME" x="50.8" y="62.738" size="1.27" layer="95" ratio="15"/>
<attribute name="VALUE" x="50.8" y="60.96" size="1.27" layer="96" ratio="15"/>
</instance>
<instance part="Q7" gate="G$1" x="81.28" y="22.86" smashed="yes">
<attribute name="NAME" x="86.36" y="22.86" size="1.27" layer="95" ratio="15"/>
<attribute name="VALUE" x="86.36" y="20.32" size="1.27" layer="96" ratio="15"/>
</instance>
<instance part="R133" gate="G$1" x="76.2" y="15.24" smashed="yes">
<attribute name="NAME" x="73.914" y="16.7386" size="1.27" layer="95"/>
<attribute name="VALUE" x="73.914" y="12.7" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY70" gate="GND" x="83.82" y="10.16" smashed="yes">
<attribute name="VALUE" x="81.915" y="6.985" size="1.27" layer="96"/>
</instance>
<instance part="D8" gate="G$1" x="63.5" y="50.8" smashed="yes" rot="R90">
<attribute name="NAME" x="62.992" y="45.72" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="62.992" y="52.578" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="R134" gate="G$1" x="50.8" y="35.56" smashed="yes" rot="R90">
<attribute name="NAME" x="52.324" y="36.5506" size="1.27" layer="95"/>
<attribute name="VALUE" x="52.324" y="35.052" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY71" gate="GND" x="50.8" y="25.4" smashed="yes">
<attribute name="VALUE" x="52.197" y="25.527" size="1.27" layer="96"/>
</instance>
<instance part="R135" gate="G$1" x="71.12" y="43.18" smashed="yes" rot="R180">
<attribute name="NAME" x="68.834" y="44.6786" size="1.27" layer="95"/>
<attribute name="VALUE" x="68.58" y="40.64" size="1.27" layer="96"/>
</instance>
<instance part="D9" gate="G$1" x="88.9" y="50.8" smashed="yes" rot="R90">
<attribute name="NAME" x="88.392" y="45.72" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="93.472" y="43.942" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C90" gate="G$1" x="129.54" y="50.8" smashed="yes" rot="R90">
<attribute name="NAME" x="129.032" y="45.72" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="129.032" y="52.07" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C91" gate="G$1" x="40.64" y="220.98" smashed="yes" rot="R90">
<attribute name="NAME" x="40.132" y="215.9" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="40.132" y="222.25" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C92" gate="G$1" x="96.52" y="220.98" smashed="yes" rot="R90">
<attribute name="NAME" x="96.012" y="215.9" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="96.012" y="222.25" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C93" gate="G$1" x="119.38" y="220.98" smashed="yes" rot="R90">
<attribute name="NAME" x="118.872" y="215.9" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="118.872" y="222.25" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="R136" gate="G$1" x="76.2" y="220.98" smashed="yes" rot="R90">
<attribute name="NAME" x="75.692" y="214.122" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="75.692" y="223.52" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R137" gate="G$1" x="81.28" y="205.74" smashed="yes" rot="R90">
<attribute name="NAME" x="82.804" y="206.7306" size="1.27" layer="95"/>
<attribute name="VALUE" x="82.804" y="205.232" size="1.27" layer="96"/>
</instance>
<instance part="Q8" gate="G$1" x="109.22" y="226.06" smashed="yes" rot="R90">
<attribute name="NAME" x="101.854" y="233.934" size="1.27" layer="95" ratio="15" rot="MR180"/>
<attribute name="VALUE" x="101.854" y="232.156" size="1.27" layer="96" ratio="15" rot="MR180"/>
</instance>
<instance part="Q9" gate="G$1" x="50.8" y="226.06" smashed="yes" rot="MR90">
<attribute name="NAME" x="48.26" y="232.664" size="1.27" layer="95" ratio="15"/>
<attribute name="VALUE" x="48.26" y="230.886" size="1.27" layer="96" ratio="15"/>
</instance>
<instance part="Q10" gate="G$1" x="78.74" y="193.04" smashed="yes">
<attribute name="NAME" x="83.82" y="193.04" size="1.27" layer="95" ratio="15"/>
<attribute name="VALUE" x="83.82" y="190.5" size="1.27" layer="96" ratio="15"/>
</instance>
<instance part="R138" gate="G$1" x="73.66" y="185.42" smashed="yes">
<attribute name="NAME" x="71.374" y="187.1726" size="1.27" layer="95"/>
<attribute name="VALUE" x="71.374" y="182.88" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY72" gate="GND" x="81.28" y="180.34" smashed="yes">
<attribute name="VALUE" x="81.915" y="179.705" size="1.27" layer="96"/>
</instance>
<instance part="D10" gate="G$1" x="60.96" y="220.98" smashed="yes" rot="R90">
<attribute name="NAME" x="60.452" y="214.884" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="60.452" y="222.758" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="R139" gate="G$1" x="48.26" y="205.74" smashed="yes" rot="R90">
<attribute name="NAME" x="49.784" y="206.7306" size="1.27" layer="95"/>
<attribute name="VALUE" x="49.784" y="204.978" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY73" gate="GND" x="48.26" y="195.58" smashed="yes">
<attribute name="VALUE" x="49.657" y="195.707" size="1.27" layer="96"/>
</instance>
<instance part="R140" gate="G$1" x="68.58" y="213.36" smashed="yes" rot="R180">
<attribute name="NAME" x="66.294" y="214.8586" size="1.27" layer="95"/>
<attribute name="VALUE" x="66.294" y="210.82" size="1.27" layer="96"/>
</instance>
<instance part="D11" gate="G$1" x="86.36" y="220.98" smashed="yes" rot="R90">
<attribute name="NAME" x="85.852" y="214.884" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="90.424" y="214.122" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C94" gate="G$1" x="127" y="220.98" smashed="yes" rot="R90">
<attribute name="NAME" x="126.492" y="215.9" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="126.492" y="222.25" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C95" gate="G$1" x="43.18" y="109.22" smashed="yes" rot="R90">
<attribute name="NAME" x="42.672" y="104.14" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="42.672" y="110.49" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C96" gate="G$1" x="99.06" y="109.22" smashed="yes" rot="R90">
<attribute name="NAME" x="98.552" y="104.14" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="98.552" y="110.49" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C97" gate="G$1" x="121.92" y="109.22" smashed="yes" rot="R90">
<attribute name="NAME" x="121.412" y="104.14" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="121.412" y="110.49" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="R141" gate="G$1" x="78.74" y="109.22" smashed="yes" rot="R90">
<attribute name="NAME" x="78.2574" y="102.108" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="78.232" y="111.76" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R142" gate="G$1" x="83.82" y="93.98" smashed="yes" rot="R90">
<attribute name="NAME" x="85.344" y="94.9706" size="1.27" layer="95"/>
<attribute name="VALUE" x="85.344" y="93.472" size="1.27" layer="96"/>
</instance>
<instance part="Q11" gate="G$1" x="111.76" y="114.3" smashed="yes" rot="R90">
<attribute name="NAME" x="104.394" y="122.174" size="1.27" layer="95" ratio="15" rot="MR180"/>
<attribute name="VALUE" x="104.394" y="120.396" size="1.27" layer="96" ratio="15" rot="MR180"/>
</instance>
<instance part="Q12" gate="G$1" x="53.34" y="114.3" smashed="yes" rot="MR90">
<attribute name="NAME" x="50.8" y="121.158" size="1.27" layer="95" ratio="15"/>
<attribute name="VALUE" x="50.8" y="119.38" size="1.27" layer="96" ratio="15"/>
</instance>
<instance part="Q13" gate="G$1" x="81.28" y="81.28" smashed="yes">
<attribute name="NAME" x="86.36" y="81.28" size="1.27" layer="95" ratio="15"/>
<attribute name="VALUE" x="86.36" y="78.74" size="1.27" layer="96" ratio="15"/>
</instance>
<instance part="R143" gate="G$1" x="76.2" y="73.66" smashed="yes">
<attribute name="NAME" x="73.914" y="75.1586" size="1.27" layer="95"/>
<attribute name="VALUE" x="73.914" y="71.12" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY74" gate="GND" x="83.82" y="68.58" smashed="yes">
<attribute name="VALUE" x="81.915" y="65.405" size="1.27" layer="96"/>
</instance>
<instance part="D12" gate="G$1" x="63.5" y="109.22" smashed="yes" rot="R90">
<attribute name="NAME" x="62.992" y="103.378" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="62.992" y="110.998" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="R144" gate="G$1" x="50.8" y="93.98" smashed="yes" rot="R90">
<attribute name="NAME" x="52.324" y="94.9706" size="1.27" layer="95"/>
<attribute name="VALUE" x="52.324" y="93.472" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY75" gate="GND" x="50.8" y="83.82" smashed="yes">
<attribute name="VALUE" x="52.451" y="83.947" size="1.27" layer="96"/>
</instance>
<instance part="R145" gate="G$1" x="71.12" y="101.6" smashed="yes" rot="R180">
<attribute name="NAME" x="68.834" y="103.0986" size="1.27" layer="95"/>
<attribute name="VALUE" x="68.58" y="99.06" size="1.27" layer="96"/>
</instance>
<instance part="D13" gate="G$1" x="88.9" y="109.22" smashed="yes" rot="R90">
<attribute name="NAME" x="88.392" y="103.124" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="93.218" y="102.362" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C98" gate="G$1" x="129.54" y="109.22" smashed="yes" rot="R90">
<attribute name="NAME" x="129.032" y="104.14" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="129.032" y="110.49" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C99" gate="G$1" x="43.18" y="165.1" smashed="yes" rot="R90">
<attribute name="NAME" x="42.672" y="160.02" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="42.672" y="166.37" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C100" gate="G$1" x="99.06" y="165.1" smashed="yes" rot="R90">
<attribute name="NAME" x="98.552" y="160.02" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="98.552" y="166.37" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C101" gate="G$1" x="121.92" y="165.1" smashed="yes" rot="R90">
<attribute name="NAME" x="121.412" y="160.02" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="121.412" y="166.37" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="R146" gate="G$1" x="78.74" y="165.1" smashed="yes" rot="R90">
<attribute name="NAME" x="78.2574" y="158.496" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="78.232" y="167.64" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R147" gate="G$1" x="83.82" y="149.86" smashed="yes" rot="R90">
<attribute name="NAME" x="85.344" y="150.8506" size="1.27" layer="95"/>
<attribute name="VALUE" x="85.344" y="149.352" size="1.27" layer="96"/>
</instance>
<instance part="Q14" gate="G$1" x="111.76" y="170.18" smashed="yes" rot="R90">
<attribute name="NAME" x="104.648" y="178.308" size="1.27" layer="95" ratio="15" rot="MR180"/>
<attribute name="VALUE" x="104.648" y="176.53" size="1.27" layer="96" ratio="15" rot="MR180"/>
</instance>
<instance part="Q15" gate="G$1" x="53.34" y="170.18" smashed="yes" rot="MR90">
<attribute name="NAME" x="50.546" y="176.784" size="1.27" layer="95" ratio="15"/>
<attribute name="VALUE" x="50.546" y="175.006" size="1.27" layer="96" ratio="15"/>
</instance>
<instance part="Q16" gate="G$1" x="81.28" y="137.16" smashed="yes">
<attribute name="NAME" x="86.36" y="137.16" size="1.27" layer="95" ratio="15"/>
<attribute name="VALUE" x="86.36" y="134.62" size="1.27" layer="96" ratio="15"/>
</instance>
<instance part="R148" gate="G$1" x="76.2" y="129.54" smashed="yes">
<attribute name="NAME" x="73.914" y="131.0386" size="1.27" layer="95"/>
<attribute name="VALUE" x="73.66" y="127" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY76" gate="GND" x="83.82" y="124.46" smashed="yes">
<attribute name="VALUE" x="81.915" y="121.285" size="1.27" layer="96"/>
</instance>
<instance part="D14" gate="G$1" x="63.5" y="165.1" smashed="yes" rot="R90">
<attribute name="NAME" x="62.992" y="159.258" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="62.992" y="166.878" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="R149" gate="G$1" x="50.8" y="149.86" smashed="yes" rot="R90">
<attribute name="NAME" x="52.578" y="150.8506" size="1.27" layer="95"/>
<attribute name="VALUE" x="52.578" y="149.352" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY77" gate="GND" x="50.8" y="139.7" smashed="yes">
<attribute name="VALUE" x="52.705" y="139.827" size="1.27" layer="96"/>
</instance>
<instance part="R150" gate="G$1" x="71.12" y="157.48" smashed="yes" rot="R180">
<attribute name="NAME" x="68.834" y="159.2326" size="1.27" layer="95"/>
<attribute name="VALUE" x="68.58" y="154.94" size="1.27" layer="96"/>
</instance>
<instance part="D15" gate="G$1" x="88.9" y="165.1" smashed="yes" rot="R90">
<attribute name="NAME" x="88.392" y="159.258" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="93.218" y="158.242" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C102" gate="G$1" x="129.54" y="165.1" smashed="yes" rot="R90">
<attribute name="NAME" x="129.032" y="160.02" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="129.032" y="166.37" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="D16" gate="G$1" x="139.7" y="228.6" smashed="yes">
<attribute name="NAME" x="132.588" y="229.108" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="132.588" y="231.648" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="D17" gate="G$1" x="139.7" y="172.72" smashed="yes">
<attribute name="NAME" x="132.588" y="173.228" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="132.588" y="175.768" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="D18" gate="G$1" x="139.7" y="116.84" smashed="yes">
<attribute name="NAME" x="132.588" y="117.348" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="132.588" y="119.888" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="D19" gate="G$1" x="139.7" y="58.42" smashed="yes">
<attribute name="NAME" x="132.334" y="58.928" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="132.334" y="61.468" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="CP2" gate="G$1" x="317.5" y="78.74" smashed="yes" rot="R270">
<attribute name="NAME" x="316.992" y="73.66" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="316.992" y="80.01" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C103" gate="G$1" x="220.98" y="63.5" smashed="yes" rot="R270">
<attribute name="NAME" x="220.472" y="57.658" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="220.472" y="64.77" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="L6" gate="G$1" x="177.8" y="86.36" smashed="yes">
<attribute name="NAME" x="172.72" y="86.868" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="180.848" y="86.868" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="R151" gate="G$1" x="177.8" y="93.98" smashed="yes">
<attribute name="NAME" x="170.18" y="94.4626" size="1.27" layer="95"/>
<attribute name="VALUE" x="180.34" y="94.488" size="1.27" layer="96"/>
</instance>
<instance part="C104" gate="G$1" x="198.12" y="78.74" smashed="yes" rot="R90">
<attribute name="NAME" x="197.612" y="72.898" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="197.612" y="80.01" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C105" gate="G$1" x="205.74" y="78.74" smashed="yes" rot="R90">
<attribute name="NAME" x="205.232" y="72.898" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="205.232" y="80.01" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="U20" gate="A" x="231.14" y="68.58" smashed="yes">
<attribute name="NAME" x="231.14" y="89.408" size="1.27" layer="95" ratio="6" rot="SR0"/>
<attribute name="VALUE" x="231.14" y="91.948" size="1.27" layer="96" ratio="6" rot="SR0"/>
</instance>
<instance part="R152" gate="G$1" x="213.36" y="66.04" smashed="yes" rot="R90">
<attribute name="NAME" x="212.8774" y="58.928" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="212.852" y="68.58" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="SUPPLY79" gate="GND" x="190.5" y="66.04" smashed="yes">
<attribute name="VALUE" x="188.595" y="62.865" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY80" gate="GND" x="213.36" y="50.8" smashed="yes">
<attribute name="VALUE" x="211.455" y="47.625" size="1.27" layer="96"/>
</instance>
<instance part="R153" gate="G$1" x="330.2" y="76.2" smashed="yes" rot="R90">
<attribute name="NAME" x="329.7174" y="69.088" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="329.692" y="78.74" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R154" gate="G$1" x="337.82" y="53.34" smashed="yes" rot="R180">
<attribute name="NAME" x="330.962" y="53.8226" size="1.27" layer="95"/>
<attribute name="VALUE" x="340.36" y="53.848" size="1.27" layer="96"/>
</instance>
<instance part="L7" gate="G$1" x="307.34" y="86.36" smashed="yes">
<attribute name="NAME" x="301.498" y="86.868" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="310.388" y="86.868" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="C106" gate="G$1" x="279.4" y="81.28" smashed="yes">
<attribute name="NAME" x="274.32" y="81.788" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="280.67" y="81.788" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="SUPPLY81" gate="GND" x="345.44" y="48.26" smashed="yes">
<attribute name="VALUE" x="346.837" y="48.387" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY82" gate="GND" x="261.62" y="66.04" smashed="yes">
<attribute name="VALUE" x="259.715" y="62.865" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY83" gate="GND" x="317.5" y="68.58" smashed="yes">
<attribute name="VALUE" x="315.595" y="65.405" size="1.27" layer="96"/>
</instance>
<instance part="D20" gate="A" x="297.18" y="76.2" smashed="yes" rot="R90">
<attribute name="NAME" x="296.672" y="70.104" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="300.482" y="65.532" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="R155" gate="G$1" x="149.86" y="144.78" smashed="yes" rot="R90">
<attribute name="NAME" x="148.3614" y="143.51" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="153.162" y="143.51" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="CP4" gate="G$1" x="317.5" y="193.04" smashed="yes" rot="R270">
<attribute name="NAME" x="316.992" y="187.96" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="316.992" y="194.31" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C107" gate="G$1" x="220.98" y="177.8" smashed="yes" rot="R270">
<attribute name="NAME" x="220.472" y="171.704" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="220.472" y="179.07" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="L8" gate="G$1" x="177.8" y="200.66" smashed="yes">
<attribute name="NAME" x="172.212" y="201.168" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="180.848" y="201.168" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="R156" gate="G$1" x="177.8" y="208.28" smashed="yes">
<attribute name="NAME" x="170.688" y="208.7626" size="1.27" layer="95"/>
<attribute name="VALUE" x="180.594" y="208.788" size="1.27" layer="96"/>
</instance>
<instance part="C108" gate="G$1" x="198.12" y="193.04" smashed="yes" rot="R90">
<attribute name="NAME" x="197.612" y="187.198" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="197.612" y="194.31" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C109" gate="G$1" x="205.74" y="193.04" smashed="yes" rot="R90">
<attribute name="NAME" x="205.232" y="187.198" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="205.232" y="194.31" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="U21" gate="A" x="231.14" y="182.88" smashed="yes">
<attribute name="NAME" x="231.14" y="203.708" size="1.27" layer="95" ratio="6" rot="SR0"/>
<attribute name="VALUE" x="231.14" y="206.248" size="1.27" layer="96" ratio="6" rot="SR0"/>
</instance>
<instance part="R157" gate="G$1" x="213.36" y="180.34" smashed="yes" rot="R90">
<attribute name="NAME" x="212.8774" y="173.228" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="212.852" y="182.88" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="SUPPLY84" gate="GND" x="190.5" y="180.34" smashed="yes">
<attribute name="VALUE" x="188.595" y="177.165" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY85" gate="GND" x="213.36" y="165.1" smashed="yes">
<attribute name="VALUE" x="211.455" y="161.925" size="1.27" layer="96"/>
</instance>
<instance part="R158" gate="G$1" x="330.2" y="190.5" smashed="yes" rot="R90">
<attribute name="NAME" x="329.7174" y="183.642" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="329.692" y="193.04" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R159" gate="G$1" x="337.82" y="167.64" smashed="yes" rot="R180">
<attribute name="NAME" x="330.962" y="168.1226" size="1.27" layer="95"/>
<attribute name="VALUE" x="340.36" y="168.148" size="1.27" layer="96"/>
</instance>
<instance part="L9" gate="G$1" x="307.34" y="200.66" smashed="yes">
<attribute name="NAME" x="301.244" y="201.168" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="310.388" y="201.168" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="C110" gate="G$1" x="279.4" y="195.58" smashed="yes">
<attribute name="NAME" x="274.32" y="196.088" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="280.67" y="196.088" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="SUPPLY86" gate="GND" x="345.44" y="162.56" smashed="yes">
<attribute name="VALUE" x="346.837" y="162.687" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY87" gate="GND" x="261.62" y="180.34" smashed="yes">
<attribute name="VALUE" x="259.715" y="177.165" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY88" gate="GND" x="317.5" y="182.88" smashed="yes">
<attribute name="VALUE" x="315.595" y="179.705" size="1.27" layer="96"/>
</instance>
<instance part="D21" gate="A" x="297.18" y="190.5" smashed="yes" rot="R90">
<attribute name="NAME" x="296.672" y="184.404" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="300.482" y="179.578" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C58" gate="G$1" x="190.5" y="193.04" smashed="yes" rot="R90">
<attribute name="NAME" x="189.992" y="187.198" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="189.992" y="194.31" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C59" gate="G$1" x="190.5" y="78.74" smashed="yes" rot="R90">
<attribute name="NAME" x="189.992" y="72.898" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="189.992" y="80.01" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="R60" gate="G$1" x="266.7" y="81.28" smashed="yes">
<attribute name="NAME" x="260.604" y="81.7626" size="1.27" layer="95"/>
<attribute name="VALUE" x="269.24" y="81.788" size="1.27" layer="96"/>
</instance>
<instance part="R61" gate="G$1" x="266.7" y="195.58" smashed="yes">
<attribute name="NAME" x="260.35" y="196.0626" size="1.27" layer="95"/>
<attribute name="VALUE" x="269.24" y="196.088" size="1.27" layer="96"/>
</instance>
<instance part="R62" gate="G$1" x="287.02" y="187.96" smashed="yes" rot="R90">
<attribute name="NAME" x="282.448" y="188.1886" size="1.27" layer="95"/>
<attribute name="VALUE" x="282.448" y="186.69" size="1.27" layer="96"/>
</instance>
<instance part="R63" gate="G$1" x="287.02" y="73.66" smashed="yes" rot="R90">
<attribute name="NAME" x="282.448" y="74.1426" size="1.27" layer="95"/>
<attribute name="VALUE" x="282.448" y="72.644" size="1.27" layer="96"/>
</instance>
<instance part="C60" gate="G$1" x="287.02" y="175.26" smashed="yes" rot="R90">
<attribute name="NAME" x="286.512" y="170.18" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="286.512" y="176.53" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C61" gate="G$1" x="287.02" y="60.96" smashed="yes" rot="R90">
<attribute name="NAME" x="286.512" y="55.88" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="286.512" y="62.23" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="R133" gate="G$1" pin="2"/>
<wire x1="81.28" y1="15.24" x2="83.82" y2="15.24" width="0.1524" layer="91"/>
<pinref part="Q7" gate="G$1" pin="S"/>
<wire x1="83.82" y1="15.24" x2="83.82" y2="17.78" width="0.1524" layer="91"/>
<wire x1="83.82" y1="15.24" x2="83.82" y2="12.7" width="0.1524" layer="91"/>
<junction x="83.82" y="15.24"/>
<pinref part="SUPPLY70" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="R134" gate="G$1" pin="1"/>
<wire x1="50.8" y1="30.48" x2="50.8" y2="27.94" width="0.1524" layer="91"/>
<pinref part="SUPPLY71" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="R138" gate="G$1" pin="2"/>
<wire x1="78.74" y1="185.42" x2="81.28" y2="185.42" width="0.1524" layer="91"/>
<pinref part="Q10" gate="G$1" pin="S"/>
<wire x1="81.28" y1="185.42" x2="81.28" y2="187.96" width="0.1524" layer="91"/>
<wire x1="81.28" y1="185.42" x2="81.28" y2="182.88" width="0.1524" layer="91"/>
<junction x="81.28" y="185.42"/>
<pinref part="SUPPLY72" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="R139" gate="G$1" pin="1"/>
<wire x1="48.26" y1="200.66" x2="48.26" y2="198.12" width="0.1524" layer="91"/>
<pinref part="SUPPLY73" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="R143" gate="G$1" pin="2"/>
<wire x1="81.28" y1="73.66" x2="83.82" y2="73.66" width="0.1524" layer="91"/>
<pinref part="Q13" gate="G$1" pin="S"/>
<wire x1="83.82" y1="73.66" x2="83.82" y2="76.2" width="0.1524" layer="91"/>
<wire x1="83.82" y1="73.66" x2="83.82" y2="71.12" width="0.1524" layer="91"/>
<junction x="83.82" y="73.66"/>
<pinref part="SUPPLY74" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="R144" gate="G$1" pin="1"/>
<wire x1="50.8" y1="88.9" x2="50.8" y2="86.36" width="0.1524" layer="91"/>
<pinref part="SUPPLY75" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="R148" gate="G$1" pin="2"/>
<wire x1="81.28" y1="129.54" x2="83.82" y2="129.54" width="0.1524" layer="91"/>
<pinref part="Q16" gate="G$1" pin="S"/>
<wire x1="83.82" y1="129.54" x2="83.82" y2="132.08" width="0.1524" layer="91"/>
<wire x1="83.82" y1="129.54" x2="83.82" y2="127" width="0.1524" layer="91"/>
<junction x="83.82" y="129.54"/>
<pinref part="SUPPLY76" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="R149" gate="G$1" pin="1"/>
<wire x1="50.8" y1="144.78" x2="50.8" y2="142.24" width="0.1524" layer="91"/>
<pinref part="SUPPLY77" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C105" gate="G$1" pin="1"/>
<wire x1="205.74" y1="73.66" x2="205.74" y2="71.12" width="0.1524" layer="91"/>
<wire x1="205.74" y1="71.12" x2="198.12" y2="71.12" width="0.1524" layer="91"/>
<wire x1="198.12" y1="71.12" x2="190.5" y2="71.12" width="0.1524" layer="91"/>
<wire x1="190.5" y1="71.12" x2="190.5" y2="73.66" width="0.1524" layer="91"/>
<pinref part="C104" gate="G$1" pin="1"/>
<wire x1="198.12" y1="73.66" x2="198.12" y2="71.12" width="0.1524" layer="91"/>
<junction x="198.12" y="71.12"/>
<wire x1="190.5" y1="71.12" x2="190.5" y2="68.58" width="0.1524" layer="91"/>
<junction x="190.5" y="71.12"/>
<pinref part="SUPPLY79" gate="GND" pin="GND"/>
<pinref part="C59" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="R152" gate="G$1" pin="1"/>
<wire x1="213.36" y1="60.96" x2="213.36" y2="55.88" width="0.1524" layer="91"/>
<wire x1="213.36" y1="55.88" x2="220.98" y2="55.88" width="0.1524" layer="91"/>
<pinref part="C103" gate="G$1" pin="2"/>
<wire x1="220.98" y1="55.88" x2="220.98" y2="58.42" width="0.1524" layer="91"/>
<pinref part="SUPPLY80" gate="GND" pin="GND"/>
<wire x1="213.36" y1="55.88" x2="213.36" y2="53.34" width="0.1524" layer="91"/>
<junction x="213.36" y="55.88"/>
</segment>
<segment>
<pinref part="R154" gate="G$1" pin="1"/>
<pinref part="SUPPLY81" gate="GND" pin="GND"/>
<wire x1="342.9" y1="53.34" x2="345.44" y2="53.34" width="0.1524" layer="91"/>
<wire x1="345.44" y1="53.34" x2="345.44" y2="50.8" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="CP2" gate="G$1" pin="2"/>
<pinref part="SUPPLY83" gate="GND" pin="GND"/>
<wire x1="317.5" y1="73.66" x2="317.5" y2="71.12" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U20" gate="A" pin="GND"/>
<wire x1="256.54" y1="73.66" x2="261.62" y2="73.66" width="0.1524" layer="91"/>
<wire x1="261.62" y1="73.66" x2="261.62" y2="71.12" width="0.1524" layer="91"/>
<pinref part="U20" gate="A" pin="DAP"/>
<wire x1="261.62" y1="71.12" x2="261.62" y2="68.58" width="0.1524" layer="91"/>
<wire x1="256.54" y1="71.12" x2="261.62" y2="71.12" width="0.1524" layer="91"/>
<junction x="261.62" y="71.12"/>
<pinref part="SUPPLY82" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C109" gate="G$1" pin="1"/>
<wire x1="205.74" y1="187.96" x2="205.74" y2="185.42" width="0.1524" layer="91"/>
<wire x1="205.74" y1="185.42" x2="198.12" y2="185.42" width="0.1524" layer="91"/>
<wire x1="198.12" y1="185.42" x2="190.5" y2="185.42" width="0.1524" layer="91"/>
<wire x1="190.5" y1="185.42" x2="190.5" y2="187.96" width="0.1524" layer="91"/>
<pinref part="C108" gate="G$1" pin="1"/>
<wire x1="198.12" y1="187.96" x2="198.12" y2="185.42" width="0.1524" layer="91"/>
<junction x="198.12" y="185.42"/>
<wire x1="190.5" y1="185.42" x2="190.5" y2="182.88" width="0.1524" layer="91"/>
<junction x="190.5" y="185.42"/>
<pinref part="SUPPLY84" gate="GND" pin="GND"/>
<pinref part="C58" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="R157" gate="G$1" pin="1"/>
<wire x1="213.36" y1="175.26" x2="213.36" y2="170.18" width="0.1524" layer="91"/>
<wire x1="213.36" y1="170.18" x2="220.98" y2="170.18" width="0.1524" layer="91"/>
<pinref part="C107" gate="G$1" pin="2"/>
<wire x1="220.98" y1="170.18" x2="220.98" y2="172.72" width="0.1524" layer="91"/>
<pinref part="SUPPLY85" gate="GND" pin="GND"/>
<wire x1="213.36" y1="170.18" x2="213.36" y2="167.64" width="0.1524" layer="91"/>
<junction x="213.36" y="170.18"/>
</segment>
<segment>
<pinref part="R159" gate="G$1" pin="1"/>
<pinref part="SUPPLY86" gate="GND" pin="GND"/>
<wire x1="342.9" y1="167.64" x2="345.44" y2="167.64" width="0.1524" layer="91"/>
<wire x1="345.44" y1="167.64" x2="345.44" y2="165.1" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="CP4" gate="G$1" pin="2"/>
<pinref part="SUPPLY88" gate="GND" pin="GND"/>
<wire x1="317.5" y1="187.96" x2="317.5" y2="185.42" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U21" gate="A" pin="GND"/>
<wire x1="256.54" y1="187.96" x2="261.62" y2="187.96" width="0.1524" layer="91"/>
<wire x1="261.62" y1="187.96" x2="261.62" y2="185.42" width="0.1524" layer="91"/>
<pinref part="U21" gate="A" pin="DAP"/>
<wire x1="261.62" y1="185.42" x2="261.62" y2="182.88" width="0.1524" layer="91"/>
<wire x1="256.54" y1="185.42" x2="261.62" y2="185.42" width="0.1524" layer="91"/>
<junction x="261.62" y="185.42"/>
<pinref part="SUPPLY87" gate="GND" pin="GND"/>
</segment>
</net>
<net name="AISG_4_VIN" class="0">
<segment>
<pinref part="C87" gate="G$1" pin="2"/>
<wire x1="43.18" y1="58.42" x2="38.1" y2="58.42" width="0.1524" layer="91"/>
<wire x1="43.18" y1="55.88" x2="43.18" y2="58.42" width="0.1524" layer="91"/>
<pinref part="Q6" gate="G$1" pin="D"/>
<wire x1="48.26" y1="58.42" x2="43.18" y2="58.42" width="0.1524" layer="91"/>
<junction x="43.18" y="58.42"/>
<label x="38.1" y="58.42" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$147" class="0">
<segment>
<pinref part="Q5" gate="G$1" pin="S"/>
<pinref part="R131" gate="G$1" pin="2"/>
<wire x1="78.74" y1="58.42" x2="88.9" y2="58.42" width="0.1524" layer="91"/>
<wire x1="88.9" y1="58.42" x2="99.06" y2="58.42" width="0.1524" layer="91"/>
<wire x1="99.06" y1="58.42" x2="106.68" y2="58.42" width="0.1524" layer="91"/>
<wire x1="78.74" y1="55.88" x2="78.74" y2="58.42" width="0.1524" layer="91"/>
<pinref part="C88" gate="G$1" pin="2"/>
<wire x1="99.06" y1="55.88" x2="99.06" y2="58.42" width="0.1524" layer="91"/>
<junction x="99.06" y="58.42"/>
<pinref part="Q6" gate="G$1" pin="S"/>
<wire x1="58.42" y1="58.42" x2="63.5" y2="58.42" width="0.1524" layer="91"/>
<junction x="78.74" y="58.42"/>
<pinref part="D8" gate="G$1" pin="-"/>
<wire x1="63.5" y1="58.42" x2="78.74" y2="58.42" width="0.1524" layer="91"/>
<wire x1="63.5" y1="55.88" x2="63.5" y2="58.42" width="0.1524" layer="91"/>
<junction x="63.5" y="58.42"/>
<pinref part="D9" gate="G$1" pin="-"/>
<wire x1="88.9" y1="55.88" x2="88.9" y2="58.42" width="0.1524" layer="91"/>
<junction x="88.9" y="58.42"/>
</segment>
</net>
<net name="N$148" class="0">
<segment>
<pinref part="Q5" gate="G$1" pin="D"/>
<pinref part="C89" gate="G$1" pin="2"/>
<wire x1="116.84" y1="58.42" x2="121.92" y2="58.42" width="0.1524" layer="91"/>
<wire x1="121.92" y1="58.42" x2="121.92" y2="55.88" width="0.1524" layer="91"/>
<wire x1="121.92" y1="58.42" x2="129.54" y2="58.42" width="0.1524" layer="91"/>
<junction x="121.92" y="58.42"/>
<pinref part="C90" gate="G$1" pin="2"/>
<wire x1="129.54" y1="58.42" x2="134.62" y2="58.42" width="0.1524" layer="91"/>
<wire x1="129.54" y1="55.88" x2="129.54" y2="58.42" width="0.1524" layer="91"/>
<junction x="129.54" y="58.42"/>
<pinref part="D19" gate="G$1" pin="+"/>
</segment>
</net>
<net name="N$149" class="0">
<segment>
<pinref part="R131" gate="G$1" pin="1"/>
<wire x1="76.2" y1="43.18" x2="78.74" y2="43.18" width="0.1524" layer="91"/>
<wire x1="78.74" y1="43.18" x2="78.74" y2="45.72" width="0.1524" layer="91"/>
<pinref part="C88" gate="G$1" pin="1"/>
<wire x1="78.74" y1="43.18" x2="83.82" y2="43.18" width="0.1524" layer="91"/>
<wire x1="83.82" y1="43.18" x2="88.9" y2="43.18" width="0.1524" layer="91"/>
<wire x1="88.9" y1="43.18" x2="99.06" y2="43.18" width="0.1524" layer="91"/>
<wire x1="99.06" y1="43.18" x2="99.06" y2="45.72" width="0.1524" layer="91"/>
<junction x="78.74" y="43.18"/>
<pinref part="C89" gate="G$1" pin="1"/>
<wire x1="99.06" y1="43.18" x2="109.22" y2="43.18" width="0.1524" layer="91"/>
<wire x1="109.22" y1="43.18" x2="121.92" y2="43.18" width="0.1524" layer="91"/>
<wire x1="121.92" y1="43.18" x2="121.92" y2="45.72" width="0.1524" layer="91"/>
<junction x="99.06" y="43.18"/>
<pinref part="Q5" gate="G$1" pin="G"/>
<wire x1="109.22" y1="53.34" x2="109.22" y2="43.18" width="0.1524" layer="91"/>
<junction x="109.22" y="43.18"/>
<pinref part="R132" gate="G$1" pin="2"/>
<wire x1="83.82" y1="40.64" x2="83.82" y2="43.18" width="0.1524" layer="91"/>
<junction x="83.82" y="43.18"/>
<pinref part="R135" gate="G$1" pin="1"/>
<pinref part="D9" gate="G$1" pin="+"/>
<wire x1="88.9" y1="45.72" x2="88.9" y2="43.18" width="0.1524" layer="91"/>
<junction x="88.9" y="43.18"/>
<wire x1="121.92" y1="43.18" x2="129.54" y2="43.18" width="0.1524" layer="91"/>
<junction x="121.92" y="43.18"/>
<pinref part="C90" gate="G$1" pin="1"/>
<wire x1="129.54" y1="43.18" x2="129.54" y2="45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$150" class="0">
<segment>
<pinref part="R132" gate="G$1" pin="1"/>
<pinref part="Q7" gate="G$1" pin="D"/>
<wire x1="83.82" y1="30.48" x2="83.82" y2="27.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="CTRL4" class="0">
<segment>
<pinref part="Q7" gate="G$1" pin="G"/>
<wire x1="78.74" y1="20.32" x2="68.58" y2="20.32" width="0.1524" layer="91"/>
<pinref part="R133" gate="G$1" pin="1"/>
<wire x1="71.12" y1="15.24" x2="68.58" y2="15.24" width="0.1524" layer="91"/>
<wire x1="68.58" y1="15.24" x2="68.58" y2="20.32" width="0.1524" layer="91"/>
<wire x1="68.58" y1="20.32" x2="63.5" y2="20.32" width="0.1524" layer="91"/>
<junction x="68.58" y="20.32"/>
<label x="63.5" y="20.32" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$152" class="0">
<segment>
<pinref part="R135" gate="G$1" pin="2"/>
<wire x1="63.5" y1="43.18" x2="66.04" y2="43.18" width="0.1524" layer="91"/>
<pinref part="D8" gate="G$1" pin="+"/>
<wire x1="63.5" y1="45.72" x2="63.5" y2="43.18" width="0.1524" layer="91"/>
<junction x="63.5" y="43.18"/>
<wire x1="55.88" y1="43.18" x2="63.5" y2="43.18" width="0.1524" layer="91"/>
<pinref part="Q6" gate="G$1" pin="G"/>
<wire x1="55.88" y1="53.34" x2="55.88" y2="43.18" width="0.1524" layer="91"/>
<junction x="55.88" y="43.18"/>
<wire x1="50.8" y1="43.18" x2="55.88" y2="43.18" width="0.1524" layer="91"/>
<pinref part="R134" gate="G$1" pin="2"/>
<wire x1="50.8" y1="40.64" x2="50.8" y2="43.18" width="0.1524" layer="91"/>
<junction x="50.8" y="43.18"/>
<pinref part="C87" gate="G$1" pin="1"/>
<wire x1="43.18" y1="45.72" x2="43.18" y2="43.18" width="0.1524" layer="91"/>
<wire x1="43.18" y1="43.18" x2="50.8" y2="43.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="AISG_1_VIN" class="0">
<segment>
<pinref part="C91" gate="G$1" pin="2"/>
<wire x1="40.64" y1="228.6" x2="35.56" y2="228.6" width="0.1524" layer="91"/>
<wire x1="40.64" y1="226.06" x2="40.64" y2="228.6" width="0.1524" layer="91"/>
<pinref part="Q9" gate="G$1" pin="D"/>
<wire x1="45.72" y1="228.6" x2="40.64" y2="228.6" width="0.1524" layer="91"/>
<junction x="40.64" y="228.6"/>
<label x="35.56" y="228.6" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$154" class="0">
<segment>
<pinref part="Q8" gate="G$1" pin="S"/>
<pinref part="R136" gate="G$1" pin="2"/>
<wire x1="76.2" y1="228.6" x2="86.36" y2="228.6" width="0.1524" layer="91"/>
<wire x1="86.36" y1="228.6" x2="96.52" y2="228.6" width="0.1524" layer="91"/>
<wire x1="96.52" y1="228.6" x2="104.14" y2="228.6" width="0.1524" layer="91"/>
<wire x1="76.2" y1="226.06" x2="76.2" y2="228.6" width="0.1524" layer="91"/>
<pinref part="C92" gate="G$1" pin="2"/>
<wire x1="96.52" y1="226.06" x2="96.52" y2="228.6" width="0.1524" layer="91"/>
<junction x="96.52" y="228.6"/>
<pinref part="Q9" gate="G$1" pin="S"/>
<wire x1="55.88" y1="228.6" x2="60.96" y2="228.6" width="0.1524" layer="91"/>
<junction x="76.2" y="228.6"/>
<pinref part="D10" gate="G$1" pin="-"/>
<wire x1="60.96" y1="228.6" x2="76.2" y2="228.6" width="0.1524" layer="91"/>
<wire x1="60.96" y1="226.06" x2="60.96" y2="228.6" width="0.1524" layer="91"/>
<junction x="60.96" y="228.6"/>
<pinref part="D11" gate="G$1" pin="-"/>
<wire x1="86.36" y1="226.06" x2="86.36" y2="228.6" width="0.1524" layer="91"/>
<junction x="86.36" y="228.6"/>
</segment>
</net>
<net name="N$155" class="0">
<segment>
<pinref part="Q8" gate="G$1" pin="D"/>
<pinref part="C93" gate="G$1" pin="2"/>
<wire x1="114.3" y1="228.6" x2="119.38" y2="228.6" width="0.1524" layer="91"/>
<wire x1="119.38" y1="228.6" x2="119.38" y2="226.06" width="0.1524" layer="91"/>
<wire x1="119.38" y1="228.6" x2="127" y2="228.6" width="0.1524" layer="91"/>
<junction x="119.38" y="228.6"/>
<pinref part="C94" gate="G$1" pin="2"/>
<wire x1="127" y1="228.6" x2="134.62" y2="228.6" width="0.1524" layer="91"/>
<wire x1="127" y1="226.06" x2="127" y2="228.6" width="0.1524" layer="91"/>
<junction x="127" y="228.6"/>
<pinref part="D16" gate="G$1" pin="+"/>
</segment>
</net>
<net name="N$156" class="0">
<segment>
<pinref part="R136" gate="G$1" pin="1"/>
<wire x1="73.66" y1="213.36" x2="76.2" y2="213.36" width="0.1524" layer="91"/>
<wire x1="76.2" y1="213.36" x2="76.2" y2="215.9" width="0.1524" layer="91"/>
<pinref part="C92" gate="G$1" pin="1"/>
<wire x1="76.2" y1="213.36" x2="81.28" y2="213.36" width="0.1524" layer="91"/>
<wire x1="81.28" y1="213.36" x2="86.36" y2="213.36" width="0.1524" layer="91"/>
<wire x1="86.36" y1="213.36" x2="96.52" y2="213.36" width="0.1524" layer="91"/>
<wire x1="96.52" y1="213.36" x2="96.52" y2="215.9" width="0.1524" layer="91"/>
<junction x="76.2" y="213.36"/>
<pinref part="C93" gate="G$1" pin="1"/>
<wire x1="96.52" y1="213.36" x2="106.68" y2="213.36" width="0.1524" layer="91"/>
<wire x1="106.68" y1="213.36" x2="119.38" y2="213.36" width="0.1524" layer="91"/>
<wire x1="119.38" y1="213.36" x2="119.38" y2="215.9" width="0.1524" layer="91"/>
<junction x="96.52" y="213.36"/>
<pinref part="Q8" gate="G$1" pin="G"/>
<wire x1="106.68" y1="223.52" x2="106.68" y2="213.36" width="0.1524" layer="91"/>
<junction x="106.68" y="213.36"/>
<pinref part="R137" gate="G$1" pin="2"/>
<wire x1="81.28" y1="210.82" x2="81.28" y2="213.36" width="0.1524" layer="91"/>
<junction x="81.28" y="213.36"/>
<pinref part="R140" gate="G$1" pin="1"/>
<pinref part="D11" gate="G$1" pin="+"/>
<wire x1="86.36" y1="215.9" x2="86.36" y2="213.36" width="0.1524" layer="91"/>
<junction x="86.36" y="213.36"/>
<wire x1="119.38" y1="213.36" x2="127" y2="213.36" width="0.1524" layer="91"/>
<junction x="119.38" y="213.36"/>
<pinref part="C94" gate="G$1" pin="1"/>
<wire x1="127" y1="213.36" x2="127" y2="215.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$157" class="0">
<segment>
<pinref part="R137" gate="G$1" pin="1"/>
<pinref part="Q10" gate="G$1" pin="D"/>
<wire x1="81.28" y1="200.66" x2="81.28" y2="198.12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="CTRL2" class="0">
<segment>
<pinref part="Q10" gate="G$1" pin="G"/>
<wire x1="76.2" y1="190.5" x2="66.04" y2="190.5" width="0.1524" layer="91"/>
<pinref part="R138" gate="G$1" pin="1"/>
<wire x1="68.58" y1="185.42" x2="66.04" y2="185.42" width="0.1524" layer="91"/>
<wire x1="66.04" y1="185.42" x2="66.04" y2="190.5" width="0.1524" layer="91"/>
<wire x1="66.04" y1="190.5" x2="60.96" y2="190.5" width="0.1524" layer="91"/>
<junction x="66.04" y="190.5"/>
<label x="60.96" y="190.5" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$159" class="0">
<segment>
<pinref part="R140" gate="G$1" pin="2"/>
<wire x1="60.96" y1="213.36" x2="63.5" y2="213.36" width="0.1524" layer="91"/>
<pinref part="D10" gate="G$1" pin="+"/>
<wire x1="60.96" y1="215.9" x2="60.96" y2="213.36" width="0.1524" layer="91"/>
<junction x="60.96" y="213.36"/>
<wire x1="53.34" y1="213.36" x2="60.96" y2="213.36" width="0.1524" layer="91"/>
<pinref part="Q9" gate="G$1" pin="G"/>
<wire x1="53.34" y1="223.52" x2="53.34" y2="213.36" width="0.1524" layer="91"/>
<junction x="53.34" y="213.36"/>
<wire x1="48.26" y1="213.36" x2="53.34" y2="213.36" width="0.1524" layer="91"/>
<pinref part="R139" gate="G$1" pin="2"/>
<wire x1="48.26" y1="210.82" x2="48.26" y2="213.36" width="0.1524" layer="91"/>
<junction x="48.26" y="213.36"/>
<pinref part="C91" gate="G$1" pin="1"/>
<wire x1="40.64" y1="215.9" x2="40.64" y2="213.36" width="0.1524" layer="91"/>
<wire x1="40.64" y1="213.36" x2="48.26" y2="213.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="AISG_3_VIN" class="0">
<segment>
<pinref part="C95" gate="G$1" pin="2"/>
<wire x1="43.18" y1="116.84" x2="38.1" y2="116.84" width="0.1524" layer="91"/>
<wire x1="43.18" y1="114.3" x2="43.18" y2="116.84" width="0.1524" layer="91"/>
<pinref part="Q12" gate="G$1" pin="D"/>
<wire x1="48.26" y1="116.84" x2="43.18" y2="116.84" width="0.1524" layer="91"/>
<junction x="43.18" y="116.84"/>
<label x="38.1" y="116.84" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$161" class="0">
<segment>
<pinref part="Q11" gate="G$1" pin="S"/>
<pinref part="R141" gate="G$1" pin="2"/>
<wire x1="78.74" y1="116.84" x2="88.9" y2="116.84" width="0.1524" layer="91"/>
<wire x1="88.9" y1="116.84" x2="99.06" y2="116.84" width="0.1524" layer="91"/>
<wire x1="99.06" y1="116.84" x2="106.68" y2="116.84" width="0.1524" layer="91"/>
<wire x1="78.74" y1="114.3" x2="78.74" y2="116.84" width="0.1524" layer="91"/>
<pinref part="C96" gate="G$1" pin="2"/>
<wire x1="99.06" y1="114.3" x2="99.06" y2="116.84" width="0.1524" layer="91"/>
<junction x="99.06" y="116.84"/>
<pinref part="Q12" gate="G$1" pin="S"/>
<wire x1="58.42" y1="116.84" x2="63.5" y2="116.84" width="0.1524" layer="91"/>
<junction x="78.74" y="116.84"/>
<pinref part="D12" gate="G$1" pin="-"/>
<wire x1="63.5" y1="116.84" x2="78.74" y2="116.84" width="0.1524" layer="91"/>
<wire x1="63.5" y1="114.3" x2="63.5" y2="116.84" width="0.1524" layer="91"/>
<junction x="63.5" y="116.84"/>
<pinref part="D13" gate="G$1" pin="-"/>
<wire x1="88.9" y1="114.3" x2="88.9" y2="116.84" width="0.1524" layer="91"/>
<junction x="88.9" y="116.84"/>
</segment>
</net>
<net name="N$162" class="0">
<segment>
<pinref part="Q11" gate="G$1" pin="D"/>
<pinref part="C97" gate="G$1" pin="2"/>
<wire x1="116.84" y1="116.84" x2="121.92" y2="116.84" width="0.1524" layer="91"/>
<wire x1="121.92" y1="116.84" x2="121.92" y2="114.3" width="0.1524" layer="91"/>
<wire x1="121.92" y1="116.84" x2="129.54" y2="116.84" width="0.1524" layer="91"/>
<junction x="121.92" y="116.84"/>
<pinref part="C98" gate="G$1" pin="2"/>
<wire x1="129.54" y1="116.84" x2="134.62" y2="116.84" width="0.1524" layer="91"/>
<wire x1="129.54" y1="114.3" x2="129.54" y2="116.84" width="0.1524" layer="91"/>
<junction x="129.54" y="116.84"/>
<pinref part="D18" gate="G$1" pin="+"/>
</segment>
</net>
<net name="N$163" class="0">
<segment>
<pinref part="R141" gate="G$1" pin="1"/>
<wire x1="76.2" y1="101.6" x2="78.74" y2="101.6" width="0.1524" layer="91"/>
<wire x1="78.74" y1="101.6" x2="78.74" y2="104.14" width="0.1524" layer="91"/>
<pinref part="C96" gate="G$1" pin="1"/>
<wire x1="78.74" y1="101.6" x2="83.82" y2="101.6" width="0.1524" layer="91"/>
<wire x1="83.82" y1="101.6" x2="88.9" y2="101.6" width="0.1524" layer="91"/>
<wire x1="88.9" y1="101.6" x2="99.06" y2="101.6" width="0.1524" layer="91"/>
<wire x1="99.06" y1="101.6" x2="99.06" y2="104.14" width="0.1524" layer="91"/>
<junction x="78.74" y="101.6"/>
<pinref part="C97" gate="G$1" pin="1"/>
<wire x1="99.06" y1="101.6" x2="109.22" y2="101.6" width="0.1524" layer="91"/>
<wire x1="109.22" y1="101.6" x2="121.92" y2="101.6" width="0.1524" layer="91"/>
<wire x1="121.92" y1="101.6" x2="121.92" y2="104.14" width="0.1524" layer="91"/>
<junction x="99.06" y="101.6"/>
<pinref part="Q11" gate="G$1" pin="G"/>
<wire x1="109.22" y1="111.76" x2="109.22" y2="101.6" width="0.1524" layer="91"/>
<junction x="109.22" y="101.6"/>
<pinref part="R142" gate="G$1" pin="2"/>
<wire x1="83.82" y1="99.06" x2="83.82" y2="101.6" width="0.1524" layer="91"/>
<junction x="83.82" y="101.6"/>
<pinref part="R145" gate="G$1" pin="1"/>
<pinref part="D13" gate="G$1" pin="+"/>
<wire x1="88.9" y1="104.14" x2="88.9" y2="101.6" width="0.1524" layer="91"/>
<junction x="88.9" y="101.6"/>
<wire x1="121.92" y1="101.6" x2="129.54" y2="101.6" width="0.1524" layer="91"/>
<junction x="121.92" y="101.6"/>
<pinref part="C98" gate="G$1" pin="1"/>
<wire x1="129.54" y1="101.6" x2="129.54" y2="104.14" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$164" class="0">
<segment>
<pinref part="R142" gate="G$1" pin="1"/>
<pinref part="Q13" gate="G$1" pin="D"/>
<wire x1="83.82" y1="88.9" x2="83.82" y2="86.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="CTRL3" class="0">
<segment>
<pinref part="Q13" gate="G$1" pin="G"/>
<wire x1="78.74" y1="78.74" x2="68.58" y2="78.74" width="0.1524" layer="91"/>
<pinref part="R143" gate="G$1" pin="1"/>
<wire x1="71.12" y1="73.66" x2="68.58" y2="73.66" width="0.1524" layer="91"/>
<wire x1="68.58" y1="73.66" x2="68.58" y2="78.74" width="0.1524" layer="91"/>
<wire x1="68.58" y1="78.74" x2="63.5" y2="78.74" width="0.1524" layer="91"/>
<junction x="68.58" y="78.74"/>
<label x="63.5" y="78.74" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$166" class="0">
<segment>
<pinref part="R145" gate="G$1" pin="2"/>
<wire x1="63.5" y1="101.6" x2="66.04" y2="101.6" width="0.1524" layer="91"/>
<pinref part="D12" gate="G$1" pin="+"/>
<wire x1="63.5" y1="104.14" x2="63.5" y2="101.6" width="0.1524" layer="91"/>
<junction x="63.5" y="101.6"/>
<wire x1="55.88" y1="101.6" x2="63.5" y2="101.6" width="0.1524" layer="91"/>
<pinref part="Q12" gate="G$1" pin="G"/>
<wire x1="55.88" y1="111.76" x2="55.88" y2="101.6" width="0.1524" layer="91"/>
<junction x="55.88" y="101.6"/>
<wire x1="50.8" y1="101.6" x2="55.88" y2="101.6" width="0.1524" layer="91"/>
<pinref part="R144" gate="G$1" pin="2"/>
<wire x1="50.8" y1="99.06" x2="50.8" y2="101.6" width="0.1524" layer="91"/>
<junction x="50.8" y="101.6"/>
<pinref part="C95" gate="G$1" pin="1"/>
<wire x1="43.18" y1="104.14" x2="43.18" y2="101.6" width="0.1524" layer="91"/>
<wire x1="43.18" y1="101.6" x2="50.8" y2="101.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="AISG_2_VIN" class="0">
<segment>
<pinref part="C99" gate="G$1" pin="2"/>
<wire x1="43.18" y1="172.72" x2="38.1" y2="172.72" width="0.1524" layer="91"/>
<wire x1="43.18" y1="170.18" x2="43.18" y2="172.72" width="0.1524" layer="91"/>
<pinref part="Q15" gate="G$1" pin="D"/>
<wire x1="48.26" y1="172.72" x2="43.18" y2="172.72" width="0.1524" layer="91"/>
<junction x="43.18" y="172.72"/>
<label x="38.1" y="172.72" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$168" class="0">
<segment>
<pinref part="Q14" gate="G$1" pin="S"/>
<pinref part="R146" gate="G$1" pin="2"/>
<wire x1="78.74" y1="172.72" x2="88.9" y2="172.72" width="0.1524" layer="91"/>
<wire x1="88.9" y1="172.72" x2="99.06" y2="172.72" width="0.1524" layer="91"/>
<wire x1="99.06" y1="172.72" x2="106.68" y2="172.72" width="0.1524" layer="91"/>
<wire x1="78.74" y1="170.18" x2="78.74" y2="172.72" width="0.1524" layer="91"/>
<pinref part="C100" gate="G$1" pin="2"/>
<wire x1="99.06" y1="170.18" x2="99.06" y2="172.72" width="0.1524" layer="91"/>
<junction x="99.06" y="172.72"/>
<pinref part="Q15" gate="G$1" pin="S"/>
<wire x1="58.42" y1="172.72" x2="63.5" y2="172.72" width="0.1524" layer="91"/>
<junction x="78.74" y="172.72"/>
<pinref part="D14" gate="G$1" pin="-"/>
<wire x1="63.5" y1="172.72" x2="78.74" y2="172.72" width="0.1524" layer="91"/>
<wire x1="63.5" y1="170.18" x2="63.5" y2="172.72" width="0.1524" layer="91"/>
<junction x="63.5" y="172.72"/>
<pinref part="D15" gate="G$1" pin="-"/>
<wire x1="88.9" y1="170.18" x2="88.9" y2="172.72" width="0.1524" layer="91"/>
<junction x="88.9" y="172.72"/>
</segment>
</net>
<net name="N$169" class="0">
<segment>
<pinref part="Q14" gate="G$1" pin="D"/>
<pinref part="C101" gate="G$1" pin="2"/>
<wire x1="116.84" y1="172.72" x2="121.92" y2="172.72" width="0.1524" layer="91"/>
<wire x1="121.92" y1="172.72" x2="121.92" y2="170.18" width="0.1524" layer="91"/>
<wire x1="121.92" y1="172.72" x2="129.54" y2="172.72" width="0.1524" layer="91"/>
<junction x="121.92" y="172.72"/>
<pinref part="C102" gate="G$1" pin="2"/>
<wire x1="129.54" y1="172.72" x2="134.62" y2="172.72" width="0.1524" layer="91"/>
<wire x1="129.54" y1="170.18" x2="129.54" y2="172.72" width="0.1524" layer="91"/>
<junction x="129.54" y="172.72"/>
<pinref part="D17" gate="G$1" pin="+"/>
</segment>
</net>
<net name="N$170" class="0">
<segment>
<pinref part="R146" gate="G$1" pin="1"/>
<wire x1="76.2" y1="157.48" x2="78.74" y2="157.48" width="0.1524" layer="91"/>
<wire x1="78.74" y1="157.48" x2="78.74" y2="160.02" width="0.1524" layer="91"/>
<pinref part="C100" gate="G$1" pin="1"/>
<wire x1="78.74" y1="157.48" x2="83.82" y2="157.48" width="0.1524" layer="91"/>
<wire x1="83.82" y1="157.48" x2="88.9" y2="157.48" width="0.1524" layer="91"/>
<wire x1="88.9" y1="157.48" x2="99.06" y2="157.48" width="0.1524" layer="91"/>
<wire x1="99.06" y1="157.48" x2="99.06" y2="160.02" width="0.1524" layer="91"/>
<junction x="78.74" y="157.48"/>
<pinref part="C101" gate="G$1" pin="1"/>
<wire x1="99.06" y1="157.48" x2="109.22" y2="157.48" width="0.1524" layer="91"/>
<wire x1="109.22" y1="157.48" x2="121.92" y2="157.48" width="0.1524" layer="91"/>
<wire x1="121.92" y1="157.48" x2="121.92" y2="160.02" width="0.1524" layer="91"/>
<junction x="99.06" y="157.48"/>
<pinref part="Q14" gate="G$1" pin="G"/>
<wire x1="109.22" y1="167.64" x2="109.22" y2="157.48" width="0.1524" layer="91"/>
<junction x="109.22" y="157.48"/>
<pinref part="R147" gate="G$1" pin="2"/>
<wire x1="83.82" y1="154.94" x2="83.82" y2="157.48" width="0.1524" layer="91"/>
<junction x="83.82" y="157.48"/>
<pinref part="R150" gate="G$1" pin="1"/>
<pinref part="D15" gate="G$1" pin="+"/>
<wire x1="88.9" y1="160.02" x2="88.9" y2="157.48" width="0.1524" layer="91"/>
<junction x="88.9" y="157.48"/>
<wire x1="121.92" y1="157.48" x2="129.54" y2="157.48" width="0.1524" layer="91"/>
<junction x="121.92" y="157.48"/>
<pinref part="C102" gate="G$1" pin="1"/>
<wire x1="129.54" y1="157.48" x2="129.54" y2="160.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$171" class="0">
<segment>
<pinref part="R147" gate="G$1" pin="1"/>
<pinref part="Q16" gate="G$1" pin="D"/>
<wire x1="83.82" y1="144.78" x2="83.82" y2="142.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="CTRL1" class="0">
<segment>
<pinref part="Q16" gate="G$1" pin="G"/>
<wire x1="78.74" y1="134.62" x2="68.58" y2="134.62" width="0.1524" layer="91"/>
<pinref part="R148" gate="G$1" pin="1"/>
<wire x1="71.12" y1="129.54" x2="68.58" y2="129.54" width="0.1524" layer="91"/>
<wire x1="68.58" y1="129.54" x2="68.58" y2="134.62" width="0.1524" layer="91"/>
<wire x1="68.58" y1="134.62" x2="63.5" y2="134.62" width="0.1524" layer="91"/>
<junction x="68.58" y="134.62"/>
<label x="63.5" y="134.62" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$173" class="0">
<segment>
<pinref part="R150" gate="G$1" pin="2"/>
<wire x1="63.5" y1="157.48" x2="66.04" y2="157.48" width="0.1524" layer="91"/>
<pinref part="D14" gate="G$1" pin="+"/>
<wire x1="63.5" y1="160.02" x2="63.5" y2="157.48" width="0.1524" layer="91"/>
<junction x="63.5" y="157.48"/>
<wire x1="55.88" y1="157.48" x2="63.5" y2="157.48" width="0.1524" layer="91"/>
<pinref part="Q15" gate="G$1" pin="G"/>
<wire x1="55.88" y1="167.64" x2="55.88" y2="157.48" width="0.1524" layer="91"/>
<junction x="55.88" y="157.48"/>
<wire x1="50.8" y1="157.48" x2="55.88" y2="157.48" width="0.1524" layer="91"/>
<pinref part="R149" gate="G$1" pin="2"/>
<wire x1="50.8" y1="154.94" x2="50.8" y2="157.48" width="0.1524" layer="91"/>
<junction x="50.8" y="157.48"/>
<pinref part="C99" gate="G$1" pin="1"/>
<wire x1="43.18" y1="160.02" x2="43.18" y2="157.48" width="0.1524" layer="91"/>
<wire x1="43.18" y1="157.48" x2="50.8" y2="157.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$118" class="0">
<segment>
<pinref part="D16" gate="G$1" pin="-"/>
<wire x1="144.78" y1="228.6" x2="149.86" y2="228.6" width="0.1524" layer="91"/>
<wire x1="149.86" y1="228.6" x2="149.86" y2="200.66" width="0.1524" layer="91"/>
<pinref part="D17" gate="G$1" pin="-"/>
<wire x1="149.86" y1="200.66" x2="149.86" y2="172.72" width="0.1524" layer="91"/>
<wire x1="149.86" y1="172.72" x2="144.78" y2="172.72" width="0.1524" layer="91"/>
<pinref part="R155" gate="G$1" pin="2"/>
<wire x1="149.86" y1="149.86" x2="149.86" y2="172.72" width="0.1524" layer="91"/>
<junction x="149.86" y="172.72"/>
<wire x1="149.86" y1="200.66" x2="170.18" y2="200.66" width="0.1524" layer="91"/>
<pinref part="R156" gate="G$1" pin="1"/>
<wire x1="172.72" y1="208.28" x2="170.18" y2="208.28" width="0.1524" layer="91"/>
<wire x1="170.18" y1="208.28" x2="170.18" y2="200.66" width="0.1524" layer="91"/>
<pinref part="L8" gate="G$1" pin="1"/>
<wire x1="172.72" y1="200.66" x2="170.18" y2="200.66" width="0.1524" layer="91"/>
<junction x="170.18" y="200.66"/>
<junction x="149.86" y="200.66"/>
</segment>
</net>
<net name="N$146" class="0">
<segment>
<pinref part="D18" gate="G$1" pin="-"/>
<wire x1="144.78" y1="116.84" x2="149.86" y2="116.84" width="0.1524" layer="91"/>
<pinref part="D19" gate="G$1" pin="-"/>
<wire x1="144.78" y1="58.42" x2="149.86" y2="58.42" width="0.1524" layer="91"/>
<wire x1="149.86" y1="58.42" x2="149.86" y2="86.36" width="0.1524" layer="91"/>
<wire x1="149.86" y1="86.36" x2="149.86" y2="116.84" width="0.1524" layer="91"/>
<wire x1="149.86" y1="86.36" x2="170.18" y2="86.36" width="0.1524" layer="91"/>
<junction x="149.86" y="86.36"/>
<pinref part="R151" gate="G$1" pin="1"/>
<wire x1="172.72" y1="93.98" x2="170.18" y2="93.98" width="0.1524" layer="91"/>
<wire x1="170.18" y1="93.98" x2="170.18" y2="86.36" width="0.1524" layer="91"/>
<pinref part="L6" gate="G$1" pin="1"/>
<wire x1="172.72" y1="86.36" x2="170.18" y2="86.36" width="0.1524" layer="91"/>
<junction x="170.18" y="86.36"/>
<pinref part="R155" gate="G$1" pin="1"/>
<wire x1="149.86" y1="139.7" x2="149.86" y2="116.84" width="0.1524" layer="91"/>
<junction x="149.86" y="116.84"/>
</segment>
</net>
<net name="N$153" class="0">
<segment>
<pinref part="R151" gate="G$1" pin="2"/>
<wire x1="182.88" y1="93.98" x2="185.42" y2="93.98" width="0.1524" layer="91"/>
<wire x1="185.42" y1="93.98" x2="185.42" y2="86.36" width="0.1524" layer="91"/>
<pinref part="L6" gate="G$1" pin="2"/>
<wire x1="185.42" y1="86.36" x2="182.88" y2="86.36" width="0.1524" layer="91"/>
<wire x1="185.42" y1="86.36" x2="190.5" y2="86.36" width="0.1524" layer="91"/>
<junction x="185.42" y="86.36"/>
<pinref part="C105" gate="G$1" pin="2"/>
<wire x1="190.5" y1="86.36" x2="198.12" y2="86.36" width="0.1524" layer="91"/>
<wire x1="198.12" y1="86.36" x2="205.74" y2="86.36" width="0.1524" layer="91"/>
<wire x1="205.74" y1="86.36" x2="205.74" y2="83.82" width="0.1524" layer="91"/>
<pinref part="C104" gate="G$1" pin="2"/>
<wire x1="198.12" y1="83.82" x2="198.12" y2="86.36" width="0.1524" layer="91"/>
<junction x="198.12" y="86.36"/>
<wire x1="190.5" y1="83.82" x2="190.5" y2="86.36" width="0.1524" layer="91"/>
<junction x="190.5" y="86.36"/>
<pinref part="U20" gate="A" pin="VIN"/>
<wire x1="205.74" y1="86.36" x2="218.44" y2="86.36" width="0.1524" layer="91"/>
<junction x="205.74" y="86.36"/>
<pinref part="U20" gate="A" pin="EN"/>
<wire x1="218.44" y1="86.36" x2="226.06" y2="86.36" width="0.1524" layer="91"/>
<wire x1="226.06" y1="81.28" x2="218.44" y2="81.28" width="0.1524" layer="91"/>
<wire x1="218.44" y1="81.28" x2="218.44" y2="86.36" width="0.1524" layer="91"/>
<junction x="218.44" y="86.36"/>
<pinref part="C59" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$160" class="0">
<segment>
<pinref part="U20" gate="A" pin="RT/SYNC"/>
<pinref part="R152" gate="G$1" pin="2"/>
<wire x1="226.06" y1="76.2" x2="213.36" y2="76.2" width="0.1524" layer="91"/>
<wire x1="213.36" y1="76.2" x2="213.36" y2="71.12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$165" class="0">
<segment>
<pinref part="U20" gate="A" pin="SS"/>
<pinref part="C103" gate="G$1" pin="1"/>
<wire x1="226.06" y1="71.12" x2="220.98" y2="71.12" width="0.1524" layer="91"/>
<wire x1="220.98" y1="71.12" x2="220.98" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$174" class="0">
<segment>
<pinref part="U20" gate="A" pin="BOOT"/>
<wire x1="256.54" y1="83.82" x2="259.08" y2="83.82" width="0.1524" layer="91"/>
<wire x1="259.08" y1="83.82" x2="259.08" y2="81.28" width="0.1524" layer="91"/>
<wire x1="259.08" y1="81.28" x2="261.62" y2="81.28" width="0.1524" layer="91"/>
<pinref part="R60" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$175" class="0">
<segment>
<pinref part="C106" gate="G$1" pin="2"/>
<wire x1="284.48" y1="81.28" x2="287.02" y2="81.28" width="0.1524" layer="91"/>
<pinref part="U20" gate="A" pin="SW"/>
<wire x1="287.02" y1="81.28" x2="287.02" y2="86.36" width="0.1524" layer="91"/>
<wire x1="287.02" y1="86.36" x2="256.54" y2="86.36" width="0.1524" layer="91"/>
<pinref part="L7" gate="G$1" pin="1"/>
<wire x1="287.02" y1="86.36" x2="297.18" y2="86.36" width="0.1524" layer="91"/>
<junction x="287.02" y="86.36"/>
<pinref part="D20" gate="A" pin="C"/>
<wire x1="297.18" y1="86.36" x2="302.26" y2="86.36" width="0.1524" layer="91"/>
<wire x1="297.18" y1="81.28" x2="297.18" y2="86.36" width="0.1524" layer="91"/>
<junction x="297.18" y="86.36"/>
<pinref part="R63" gate="G$1" pin="2"/>
<wire x1="287.02" y1="78.74" x2="287.02" y2="81.28" width="0.1524" layer="91"/>
<junction x="287.02" y="81.28"/>
</segment>
</net>
<net name="N$178" class="0">
<segment>
<pinref part="R153" gate="G$1" pin="1"/>
<pinref part="R154" gate="G$1" pin="2"/>
<wire x1="330.2" y1="71.12" x2="330.2" y2="53.34" width="0.1524" layer="91"/>
<pinref part="U20" gate="A" pin="FB"/>
<wire x1="330.2" y1="53.34" x2="332.74" y2="53.34" width="0.1524" layer="91"/>
<wire x1="256.54" y1="78.74" x2="269.24" y2="78.74" width="0.1524" layer="91"/>
<wire x1="269.24" y1="78.74" x2="269.24" y2="53.34" width="0.1524" layer="91"/>
<wire x1="269.24" y1="53.34" x2="287.02" y2="53.34" width="0.1524" layer="91"/>
<junction x="330.2" y="53.34"/>
<pinref part="D20" gate="A" pin="A"/>
<wire x1="287.02" y1="53.34" x2="297.18" y2="53.34" width="0.1524" layer="91"/>
<wire x1="297.18" y1="53.34" x2="330.2" y2="53.34" width="0.1524" layer="91"/>
<wire x1="297.18" y1="71.12" x2="297.18" y2="53.34" width="0.1524" layer="91"/>
<junction x="297.18" y="53.34"/>
<pinref part="C61" gate="G$1" pin="1"/>
<wire x1="287.02" y1="55.88" x2="287.02" y2="53.34" width="0.1524" layer="91"/>
<junction x="287.02" y="53.34"/>
</segment>
</net>
<net name="N$158" class="0">
<segment>
<pinref part="R156" gate="G$1" pin="2"/>
<wire x1="182.88" y1="208.28" x2="185.42" y2="208.28" width="0.1524" layer="91"/>
<wire x1="185.42" y1="208.28" x2="185.42" y2="200.66" width="0.1524" layer="91"/>
<pinref part="L8" gate="G$1" pin="2"/>
<wire x1="185.42" y1="200.66" x2="182.88" y2="200.66" width="0.1524" layer="91"/>
<wire x1="185.42" y1="200.66" x2="190.5" y2="200.66" width="0.1524" layer="91"/>
<junction x="185.42" y="200.66"/>
<pinref part="C109" gate="G$1" pin="2"/>
<wire x1="190.5" y1="200.66" x2="198.12" y2="200.66" width="0.1524" layer="91"/>
<wire x1="198.12" y1="200.66" x2="205.74" y2="200.66" width="0.1524" layer="91"/>
<wire x1="205.74" y1="200.66" x2="205.74" y2="198.12" width="0.1524" layer="91"/>
<pinref part="C108" gate="G$1" pin="2"/>
<wire x1="198.12" y1="198.12" x2="198.12" y2="200.66" width="0.1524" layer="91"/>
<junction x="198.12" y="200.66"/>
<wire x1="190.5" y1="198.12" x2="190.5" y2="200.66" width="0.1524" layer="91"/>
<junction x="190.5" y="200.66"/>
<pinref part="U21" gate="A" pin="VIN"/>
<wire x1="205.74" y1="200.66" x2="218.44" y2="200.66" width="0.1524" layer="91"/>
<junction x="205.74" y="200.66"/>
<pinref part="U21" gate="A" pin="EN"/>
<wire x1="218.44" y1="200.66" x2="226.06" y2="200.66" width="0.1524" layer="91"/>
<wire x1="226.06" y1="195.58" x2="218.44" y2="195.58" width="0.1524" layer="91"/>
<wire x1="218.44" y1="195.58" x2="218.44" y2="200.66" width="0.1524" layer="91"/>
<junction x="218.44" y="200.66"/>
<pinref part="C58" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$167" class="0">
<segment>
<pinref part="U21" gate="A" pin="RT/SYNC"/>
<pinref part="R157" gate="G$1" pin="2"/>
<wire x1="226.06" y1="190.5" x2="213.36" y2="190.5" width="0.1524" layer="91"/>
<wire x1="213.36" y1="190.5" x2="213.36" y2="185.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$172" class="0">
<segment>
<pinref part="U21" gate="A" pin="SS"/>
<pinref part="C107" gate="G$1" pin="1"/>
<wire x1="226.06" y1="185.42" x2="220.98" y2="185.42" width="0.1524" layer="91"/>
<wire x1="220.98" y1="185.42" x2="220.98" y2="182.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$177" class="0">
<segment>
<pinref part="U21" gate="A" pin="BOOT"/>
<wire x1="256.54" y1="198.12" x2="259.08" y2="198.12" width="0.1524" layer="91"/>
<wire x1="259.08" y1="198.12" x2="259.08" y2="195.58" width="0.1524" layer="91"/>
<wire x1="259.08" y1="195.58" x2="261.62" y2="195.58" width="0.1524" layer="91"/>
<pinref part="R61" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$179" class="0">
<segment>
<pinref part="C110" gate="G$1" pin="2"/>
<wire x1="284.48" y1="195.58" x2="287.02" y2="195.58" width="0.1524" layer="91"/>
<pinref part="U21" gate="A" pin="SW"/>
<wire x1="287.02" y1="195.58" x2="287.02" y2="200.66" width="0.1524" layer="91"/>
<wire x1="287.02" y1="200.66" x2="256.54" y2="200.66" width="0.1524" layer="91"/>
<pinref part="L9" gate="G$1" pin="1"/>
<wire x1="287.02" y1="200.66" x2="297.18" y2="200.66" width="0.1524" layer="91"/>
<junction x="287.02" y="200.66"/>
<pinref part="D21" gate="A" pin="C"/>
<wire x1="297.18" y1="200.66" x2="302.26" y2="200.66" width="0.1524" layer="91"/>
<wire x1="297.18" y1="195.58" x2="297.18" y2="200.66" width="0.1524" layer="91"/>
<junction x="297.18" y="200.66"/>
<pinref part="R62" gate="G$1" pin="2"/>
<wire x1="287.02" y1="193.04" x2="287.02" y2="195.58" width="0.1524" layer="91"/>
<junction x="287.02" y="195.58"/>
</segment>
</net>
<net name="LB_9V" class="0">
<segment>
<pinref part="L7" gate="G$1" pin="2"/>
<wire x1="312.42" y1="86.36" x2="317.5" y2="86.36" width="0.1524" layer="91"/>
<pinref part="CP2" gate="G$1" pin="1"/>
<wire x1="317.5" y1="86.36" x2="317.5" y2="83.82" width="0.1524" layer="91"/>
<pinref part="R153" gate="G$1" pin="2"/>
<wire x1="330.2" y1="81.28" x2="330.2" y2="86.36" width="0.1524" layer="91"/>
<wire x1="330.2" y1="86.36" x2="317.5" y2="86.36" width="0.1524" layer="91"/>
<junction x="317.5" y="86.36"/>
<wire x1="330.2" y1="86.36" x2="345.44" y2="86.36" width="0.1524" layer="91"/>
<junction x="330.2" y="86.36"/>
<label x="345.44" y="86.36" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$181" class="0">
<segment>
<pinref part="R158" gate="G$1" pin="1"/>
<pinref part="R159" gate="G$1" pin="2"/>
<wire x1="330.2" y1="185.42" x2="330.2" y2="167.64" width="0.1524" layer="91"/>
<pinref part="U21" gate="A" pin="FB"/>
<wire x1="330.2" y1="167.64" x2="332.74" y2="167.64" width="0.1524" layer="91"/>
<wire x1="256.54" y1="193.04" x2="269.24" y2="193.04" width="0.1524" layer="91"/>
<wire x1="269.24" y1="193.04" x2="269.24" y2="167.64" width="0.1524" layer="91"/>
<wire x1="269.24" y1="167.64" x2="287.02" y2="167.64" width="0.1524" layer="91"/>
<junction x="330.2" y="167.64"/>
<pinref part="D21" gate="A" pin="A"/>
<wire x1="287.02" y1="167.64" x2="297.18" y2="167.64" width="0.1524" layer="91"/>
<wire x1="297.18" y1="167.64" x2="330.2" y2="167.64" width="0.1524" layer="91"/>
<wire x1="297.18" y1="185.42" x2="297.18" y2="167.64" width="0.1524" layer="91"/>
<junction x="297.18" y="167.64"/>
<pinref part="C60" gate="G$1" pin="1"/>
<wire x1="287.02" y1="170.18" x2="287.02" y2="167.64" width="0.1524" layer="91"/>
<junction x="287.02" y="167.64"/>
</segment>
</net>
<net name="HB_9V" class="0">
<segment>
<pinref part="L9" gate="G$1" pin="2"/>
<wire x1="312.42" y1="200.66" x2="317.5" y2="200.66" width="0.1524" layer="91"/>
<pinref part="CP4" gate="G$1" pin="1"/>
<wire x1="317.5" y1="200.66" x2="317.5" y2="198.12" width="0.1524" layer="91"/>
<pinref part="R158" gate="G$1" pin="2"/>
<wire x1="330.2" y1="195.58" x2="330.2" y2="200.66" width="0.1524" layer="91"/>
<wire x1="330.2" y1="200.66" x2="317.5" y2="200.66" width="0.1524" layer="91"/>
<junction x="317.5" y="200.66"/>
<wire x1="330.2" y1="200.66" x2="345.44" y2="200.66" width="0.1524" layer="91"/>
<junction x="330.2" y="200.66"/>
<label x="345.44" y="200.66" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$76" class="0">
<segment>
<pinref part="C110" gate="G$1" pin="1"/>
<pinref part="R61" gate="G$1" pin="2"/>
<wire x1="271.78" y1="195.58" x2="274.32" y2="195.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$77" class="0">
<segment>
<pinref part="C106" gate="G$1" pin="1"/>
<pinref part="R60" gate="G$1" pin="2"/>
<wire x1="271.78" y1="81.28" x2="274.32" y2="81.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$78" class="0">
<segment>
<pinref part="R62" gate="G$1" pin="1"/>
<pinref part="C60" gate="G$1" pin="2"/>
<wire x1="287.02" y1="182.88" x2="287.02" y2="180.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$79" class="0">
<segment>
<pinref part="R63" gate="G$1" pin="1"/>
<pinref part="C61" gate="G$1" pin="2"/>
<wire x1="287.02" y1="68.58" x2="287.02" y2="66.04" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
<text x="121.92" y="226.06" size="1.27" layer="97" rot="MR180">Stepper Motor Driver Side</text>
<text x="60.96" y="226.06" size="1.27" layer="97" rot="MR180">Processor Side</text>
<text x="35.56" y="233.68" size="1.27" layer="97">Low Band I2C level translator</text>
<text x="35.56" y="177.8" size="1.27" layer="97">High Band I2C level translator</text>
<text x="121.92" y="170.18" size="1.27" layer="97" rot="MR180">Stepper Motor Driver Side</text>
<text x="60.96" y="170.18" size="1.27" layer="97" rot="MR180">Processor Side</text>
<text x="223.52" y="218.44" size="1.27" layer="97">PROCESSOR-&gt;EEPROM </text>
<text x="96.52" y="207.01" size="1.27" layer="97" ratio="15">ISL33003IUZ
TCA4311ADGKR</text>
<text x="96.52" y="151.13" size="1.27" layer="97" ratio="15">ISL33003IUZ
TCA4311ADGKR</text>
<text x="256.54" y="162.56" size="1.27" layer="97" ratio="15">W25x40CL
MX25L4006EM1I</text>
<text x="127" y="104.14" size="1.27" layer="97">DRIVE OUT -&gt; MP_RXD</text>
<text x="251.46" y="104.14" size="1.27" layer="97">DRIVE OUT -&gt; MP_RXD</text>
<text x="251.46" y="50.8" size="1.27" layer="97">DRIVE OUT -&gt; MP_RXD</text>
<text x="124.46" y="50.8" size="1.27" layer="97">DRIVE OUT -&gt; MP_RXD</text>
<text x="127" y="81.28" size="1.27" layer="97">DRIVE IN &lt;- MP_TXD</text>
<text x="124.46" y="27.94" size="1.27" layer="97">DRIVE IN &lt;- MP_TXD</text>
<text x="251.46" y="27.94" size="1.27" layer="97">DRIVE IN &lt;- MP_TXD</text>
<text x="251.46" y="81.28" size="1.27" layer="97">DRIVE IN &lt;- MP_TXD</text>
</plain>
<instances>
<instance part="U$3" gate="G$1" x="0" y="0" smashed="yes">
<attribute name="DRAWING_NAME" x="317.5" y="10.16" size="1.27" layer="94" ratio="15"/>
<attribute name="SHEET" x="332.486" y="5.842" size="1.27" layer="94" ratio="15"/>
</instance>
<instance part="C20" gate="G$1" x="129.54" y="215.9" smashed="yes" rot="MR90">
<attribute name="NAME" x="129.032" y="214.63" size="1.27" layer="95" ratio="10" rot="MR270"/>
<attribute name="VALUE" x="128.016" y="216.916" size="1.27" layer="96" ratio="10" rot="MR90"/>
</instance>
<instance part="C21" gate="G$1" x="81.28" y="215.9" smashed="yes" rot="MR90">
<attribute name="NAME" x="80.772" y="214.63" size="1.27" layer="95" ratio="10" rot="MR270"/>
<attribute name="VALUE" x="79.756" y="217.17" size="1.27" layer="96" ratio="10" rot="MR90"/>
</instance>
<instance part="C22" gate="G$1" x="137.16" y="215.9" smashed="yes" rot="MR90">
<attribute name="NAME" x="136.652" y="214.63" size="1.27" layer="95" ratio="10" rot="MR270"/>
<attribute name="VALUE" x="135.636" y="216.916" size="1.27" layer="96" ratio="10" rot="MR90"/>
</instance>
<instance part="C23" gate="G$1" x="73.66" y="215.9" smashed="yes" rot="MR90">
<attribute name="NAME" x="73.152" y="214.63" size="1.27" layer="95" ratio="10" rot="MR270"/>
<attribute name="VALUE" x="72.136" y="217.17" size="1.27" layer="96" ratio="10" rot="MR90"/>
</instance>
<instance part="R4" gate="G$1" x="66.04" y="215.9" smashed="yes" rot="MR90">
<attribute name="NAME" x="65.8114" y="213.36" size="1.27" layer="95" rot="MR270"/>
<attribute name="VALUE" x="64.516" y="218.694" size="1.27" layer="96" rot="MR90"/>
</instance>
<instance part="R5" gate="G$1" x="58.42" y="215.9" smashed="yes" rot="MR90">
<attribute name="NAME" x="57.9374" y="213.36" size="1.27" layer="95" rot="MR270"/>
<attribute name="VALUE" x="56.896" y="218.694" size="1.27" layer="96" rot="MR90"/>
</instance>
<instance part="R6" gate="G$1" x="144.78" y="215.9" smashed="yes" rot="MR90">
<attribute name="NAME" x="144.2974" y="213.36" size="1.27" layer="95" rot="MR270"/>
<attribute name="VALUE" x="143.256" y="218.694" size="1.27" layer="96" rot="MR90"/>
</instance>
<instance part="R7" gate="G$1" x="152.4" y="215.9" smashed="yes" rot="MR90">
<attribute name="NAME" x="151.9174" y="213.36" size="1.27" layer="95" rot="MR270"/>
<attribute name="VALUE" x="150.876" y="218.694" size="1.27" layer="96" rot="MR90"/>
</instance>
<instance part="SUPPLY8" gate="GND" x="139.7" y="203.2" smashed="yes">
<attribute name="VALUE" x="137.795" y="200.025" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY9" gate="GND" x="71.12" y="203.2" smashed="yes">
<attribute name="VALUE" x="69.215" y="200.025" size="1.27" layer="96"/>
</instance>
<instance part="R8" gate="G$1" x="50.8" y="215.9" smashed="yes" rot="MR90">
<attribute name="NAME" x="50.3174" y="213.36" size="1.27" layer="95" rot="MR270"/>
<attribute name="VALUE" x="49.276" y="218.694" size="1.27" layer="96" rot="MR90"/>
</instance>
<instance part="U4" gate="G$1" x="96.52" y="205.74" smashed="yes">
<attribute name="NAME" x="96.52" y="210.82" size="1.27" layer="95"/>
<attribute name="VALUE" x="96.52" y="208.28" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY12" gate="GND" x="88.9" y="190.5" smashed="yes">
<attribute name="VALUE" x="86.995" y="187.325" size="1.27" layer="96"/>
</instance>
<instance part="C24" gate="G$1" x="129.54" y="160.02" smashed="yes" rot="MR90">
<attribute name="NAME" x="129.032" y="158.75" size="1.27" layer="95" ratio="10" rot="MR270"/>
<attribute name="VALUE" x="128.016" y="161.29" size="1.27" layer="96" ratio="10" rot="MR90"/>
</instance>
<instance part="C25" gate="G$1" x="81.28" y="160.02" smashed="yes" rot="MR90">
<attribute name="NAME" x="79.756" y="161.036" size="1.27" layer="95" ratio="10" rot="MR90"/>
<attribute name="VALUE" x="80.772" y="158.75" size="1.27" layer="96" ratio="10" rot="MR270"/>
</instance>
<instance part="C26" gate="G$1" x="137.16" y="160.02" smashed="yes" rot="MR90">
<attribute name="NAME" x="136.652" y="158.75" size="1.27" layer="95" ratio="10" rot="MR270"/>
<attribute name="VALUE" x="135.636" y="161.29" size="1.27" layer="96" ratio="10" rot="MR90"/>
</instance>
<instance part="C27" gate="G$1" x="73.66" y="160.02" smashed="yes" rot="MR90">
<attribute name="NAME" x="73.152" y="158.75" size="1.27" layer="95" ratio="10" rot="MR270"/>
<attribute name="VALUE" x="72.136" y="161.036" size="1.27" layer="96" ratio="10" rot="MR90"/>
</instance>
<instance part="R9" gate="G$1" x="66.04" y="160.02" smashed="yes" rot="MR90">
<attribute name="NAME" x="65.8114" y="157.48" size="1.27" layer="95" rot="MR270"/>
<attribute name="VALUE" x="64.516" y="162.814" size="1.27" layer="96" rot="MR90"/>
</instance>
<instance part="R10" gate="G$1" x="58.42" y="160.02" smashed="yes" rot="MR90">
<attribute name="NAME" x="58.1914" y="157.48" size="1.27" layer="95" rot="MR270"/>
<attribute name="VALUE" x="56.896" y="162.814" size="1.27" layer="96" rot="MR90"/>
</instance>
<instance part="R11" gate="G$1" x="144.78" y="160.02" smashed="yes" rot="MR90">
<attribute name="NAME" x="144.2974" y="157.48" size="1.27" layer="95" rot="MR270"/>
<attribute name="VALUE" x="143.256" y="163.068" size="1.27" layer="96" rot="MR90"/>
</instance>
<instance part="R12" gate="G$1" x="152.4" y="160.02" smashed="yes" rot="MR90">
<attribute name="NAME" x="151.9174" y="157.48" size="1.27" layer="95" rot="MR270"/>
<attribute name="VALUE" x="150.876" y="163.068" size="1.27" layer="96" rot="MR90"/>
</instance>
<instance part="SUPPLY11" gate="GND" x="139.7" y="147.32" smashed="yes">
<attribute name="VALUE" x="137.795" y="144.145" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY13" gate="GND" x="71.12" y="147.32" smashed="yes">
<attribute name="VALUE" x="69.215" y="144.145" size="1.27" layer="96"/>
</instance>
<instance part="R13" gate="G$1" x="50.8" y="160.02" smashed="yes" rot="MR90">
<attribute name="NAME" x="50.5714" y="157.48" size="1.27" layer="95" rot="MR270"/>
<attribute name="VALUE" x="49.276" y="162.814" size="1.27" layer="96" rot="MR90"/>
</instance>
<instance part="U2" gate="G$1" x="96.52" y="149.86" smashed="yes">
<attribute name="NAME" x="96.52" y="154.94" size="1.27" layer="95"/>
<attribute name="VALUE" x="96.52" y="152.4" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY14" gate="GND" x="88.9" y="134.62" smashed="yes">
<attribute name="VALUE" x="86.995" y="131.445" size="1.27" layer="96"/>
</instance>
<instance part="U10" gate="G$1" x="256.54" y="195.58" smashed="yes">
<attribute name="NAME" x="259.08" y="200.66" size="1.27" layer="95" ratio="15"/>
<attribute name="VALUE" x="259.08" y="198.12" size="1.778" layer="96" ratio="15"/>
</instance>
<instance part="R51" gate="G$1" x="238.76" y="198.12" smashed="yes" rot="R90">
<attribute name="NAME" x="238.2774" y="192.024" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="238.252" y="200.66" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R52" gate="G$1" x="231.14" y="198.12" smashed="yes" rot="R90">
<attribute name="NAME" x="230.6574" y="191.77" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="230.632" y="200.66" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R53" gate="G$1" x="302.26" y="198.12" smashed="yes" rot="R90">
<attribute name="NAME" x="301.7774" y="192.024" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="301.752" y="200.66" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R54" gate="G$1" x="309.88" y="198.12" smashed="yes" rot="R90">
<attribute name="NAME" x="309.3974" y="192.024" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="309.372" y="200.66" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R55" gate="G$1" x="317.5" y="198.12" smashed="yes" rot="R90">
<attribute name="NAME" x="317.0174" y="192.024" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="316.992" y="200.66" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R56" gate="G$1" x="223.52" y="198.12" smashed="yes" rot="R90">
<attribute name="NAME" x="223.0374" y="191.516" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="223.012" y="200.66" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="SUPPLY51" gate="GND" x="248.92" y="162.56" smashed="yes">
<attribute name="VALUE" x="247.015" y="159.385" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY52" gate="GND" x="289.56" y="162.56" smashed="yes">
<attribute name="VALUE" x="287.655" y="159.385" size="1.27" layer="96"/>
</instance>
<instance part="C55" gate="G$1" x="289.56" y="198.12" smashed="yes" rot="R90">
<attribute name="NAME" x="288.29" y="196.85" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="292.1" y="196.85" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="+3V1" gate="G$1" x="43.18" y="231.14" smashed="yes">
<attribute name="VALUE" x="41.148" y="231.394" size="1.27" layer="96"/>
</instance>
<instance part="+3V2" gate="G$1" x="43.18" y="175.26" smashed="yes">
<attribute name="VALUE" x="40.894" y="175.26" size="1.27" layer="96"/>
</instance>
<instance part="+3V6" gate="G$1" x="322.58" y="215.9" smashed="yes">
<attribute name="VALUE" x="320.04" y="210.82" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="P+1" gate="1" x="160.02" y="231.14" smashed="yes">
<attribute name="VALUE" x="158.75" y="231.648" size="1.27" layer="96"/>
</instance>
<instance part="P+2" gate="1" x="160.02" y="175.26" smashed="yes">
<attribute name="VALUE" x="158.242" y="175.768" size="1.27" layer="96"/>
</instance>
<instance part="U16" gate="G$1" x="106.68" y="109.22" smashed="yes" rot="MR0">
<attribute name="NAME" x="106.68" y="111.76" size="1.27" layer="95" ratio="15" rot="MR0"/>
<attribute name="VALUE" x="106.68" y="81.28" size="1.778" layer="96" ratio="15" rot="MR0"/>
</instance>
<instance part="R101" gate="G$1" x="66.04" y="99.06" smashed="yes">
<attribute name="NAME" x="59.182" y="99.5426" size="1.27" layer="95"/>
<attribute name="VALUE" x="68.58" y="99.568" size="1.27" layer="96"/>
</instance>
<instance part="R102" gate="G$1" x="66.04" y="96.52" smashed="yes">
<attribute name="NAME" x="59.182" y="97.0026" size="1.27" layer="95"/>
<attribute name="VALUE" x="68.58" y="97.028" size="1.27" layer="96"/>
</instance>
<instance part="R103" gate="G$1" x="66.04" y="91.44" smashed="yes">
<attribute name="NAME" x="59.182" y="91.9226" size="1.27" layer="95"/>
<attribute name="VALUE" x="68.58" y="91.948" size="1.27" layer="96"/>
</instance>
<instance part="R104" gate="G$1" x="66.04" y="88.9" smashed="yes">
<attribute name="NAME" x="59.182" y="89.3826" size="1.27" layer="95"/>
<attribute name="VALUE" x="68.58" y="89.408" size="1.27" layer="96"/>
</instance>
<instance part="R105" gate="G$1" x="119.38" y="101.6" smashed="yes">
<attribute name="NAME" x="112.268" y="102.0826" size="1.27" layer="95"/>
<attribute name="VALUE" x="121.92" y="102.108" size="1.27" layer="96"/>
</instance>
<instance part="R106" gate="G$1" x="119.38" y="86.36" smashed="yes">
<attribute name="NAME" x="112.268" y="86.8426" size="1.27" layer="95"/>
<attribute name="VALUE" x="121.92" y="86.868" size="1.27" layer="96"/>
</instance>
<instance part="C69" gate="G$1" x="45.72" y="81.28" smashed="yes" rot="R90">
<attribute name="NAME" x="44.196" y="80.01" size="1.27" layer="95" ratio="10" rot="R270"/>
<attribute name="VALUE" x="45.212" y="82.55" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C70" gate="G$1" x="50.8" y="81.28" smashed="yes" rot="R90">
<attribute name="NAME" x="49.276" y="80.01" size="1.27" layer="95" ratio="10" rot="R270"/>
<attribute name="VALUE" x="50.292" y="82.55" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C71" gate="G$1" x="71.12" y="111.76" smashed="yes" rot="R180">
<attribute name="NAME" x="66.04" y="112.268" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="72.39" y="112.268" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="SUPPLY55" gate="GND" x="63.5" y="106.68" smashed="yes">
<attribute name="VALUE" x="61.595" y="103.505" size="1.27" layer="96"/>
</instance>
<instance part="P+3" gate="1" x="78.74" y="119.38" smashed="yes">
<attribute name="VALUE" x="76.708" y="119.634" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY56" gate="GND" x="45.72" y="68.58" smashed="yes">
<attribute name="VALUE" x="43.815" y="65.405" size="1.27" layer="96"/>
</instance>
<instance part="U17" gate="G$1" x="106.68" y="55.88" smashed="yes" rot="MR0">
<attribute name="NAME" x="106.68" y="58.42" size="1.27" layer="95" ratio="15" rot="MR0"/>
<attribute name="VALUE" x="106.68" y="27.94" size="1.778" layer="96" ratio="15" rot="MR0"/>
</instance>
<instance part="R107" gate="G$1" x="66.04" y="45.72" smashed="yes">
<attribute name="NAME" x="59.436" y="46.2026" size="1.27" layer="95"/>
<attribute name="VALUE" x="68.58" y="46.228" size="1.27" layer="96"/>
</instance>
<instance part="R108" gate="G$1" x="66.04" y="43.18" smashed="yes">
<attribute name="NAME" x="59.436" y="43.6626" size="1.27" layer="95"/>
<attribute name="VALUE" x="68.58" y="43.688" size="1.27" layer="96"/>
</instance>
<instance part="R109" gate="G$1" x="66.04" y="38.1" smashed="yes">
<attribute name="NAME" x="59.436" y="38.5826" size="1.27" layer="95"/>
<attribute name="VALUE" x="68.58" y="38.608" size="1.27" layer="96"/>
</instance>
<instance part="R110" gate="G$1" x="66.04" y="35.56" smashed="yes">
<attribute name="NAME" x="59.436" y="36.0426" size="1.27" layer="95"/>
<attribute name="VALUE" x="68.58" y="36.068" size="1.27" layer="96"/>
</instance>
<instance part="R111" gate="G$1" x="119.38" y="48.26" smashed="yes">
<attribute name="NAME" x="112.268" y="48.7426" size="1.27" layer="95"/>
<attribute name="VALUE" x="121.92" y="48.768" size="1.27" layer="96"/>
</instance>
<instance part="R112" gate="G$1" x="119.38" y="33.02" smashed="yes">
<attribute name="NAME" x="112.268" y="33.5026" size="1.27" layer="95"/>
<attribute name="VALUE" x="121.92" y="33.528" size="1.27" layer="96"/>
</instance>
<instance part="C72" gate="G$1" x="45.72" y="27.94" smashed="yes" rot="R90">
<attribute name="NAME" x="44.196" y="26.67" size="1.27" layer="95" ratio="10" rot="R270"/>
<attribute name="VALUE" x="45.212" y="29.21" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C73" gate="G$1" x="50.8" y="27.94" smashed="yes" rot="R90">
<attribute name="NAME" x="49.276" y="26.67" size="1.27" layer="95" ratio="10" rot="R270"/>
<attribute name="VALUE" x="50.292" y="29.21" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C74" gate="G$1" x="71.12" y="58.42" smashed="yes" rot="R180">
<attribute name="NAME" x="66.04" y="58.928" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="72.39" y="58.928" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="SUPPLY57" gate="GND" x="63.5" y="53.34" smashed="yes">
<attribute name="VALUE" x="61.595" y="50.165" size="1.27" layer="96"/>
</instance>
<instance part="P+4" gate="1" x="78.74" y="66.04" smashed="yes">
<attribute name="VALUE" x="76.708" y="66.04" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY58" gate="GND" x="45.72" y="15.24" smashed="yes">
<attribute name="VALUE" x="43.815" y="12.065" size="1.27" layer="96"/>
</instance>
<instance part="U18" gate="G$1" x="233.68" y="109.22" smashed="yes" rot="MR0">
<attribute name="NAME" x="233.68" y="111.76" size="1.27" layer="95" ratio="15" rot="MR0"/>
<attribute name="VALUE" x="233.68" y="81.28" size="1.778" layer="96" ratio="15" rot="MR0"/>
</instance>
<instance part="R113" gate="G$1" x="193.04" y="99.06" smashed="yes">
<attribute name="NAME" x="186.436" y="99.5426" size="1.27" layer="95"/>
<attribute name="VALUE" x="195.58" y="99.568" size="1.27" layer="96"/>
</instance>
<instance part="R114" gate="G$1" x="193.04" y="96.52" smashed="yes">
<attribute name="NAME" x="186.436" y="97.0026" size="1.27" layer="95"/>
<attribute name="VALUE" x="195.58" y="97.028" size="1.27" layer="96"/>
</instance>
<instance part="R115" gate="G$1" x="193.04" y="91.44" smashed="yes">
<attribute name="NAME" x="186.436" y="91.9226" size="1.27" layer="95"/>
<attribute name="VALUE" x="195.58" y="91.948" size="1.27" layer="96"/>
</instance>
<instance part="R116" gate="G$1" x="193.04" y="88.9" smashed="yes">
<attribute name="NAME" x="186.436" y="89.3826" size="1.27" layer="95"/>
<attribute name="VALUE" x="195.58" y="89.408" size="1.27" layer="96"/>
</instance>
<instance part="R117" gate="G$1" x="246.38" y="101.6" smashed="yes">
<attribute name="NAME" x="239.776" y="102.0826" size="1.27" layer="95"/>
<attribute name="VALUE" x="248.92" y="102.108" size="1.27" layer="96"/>
</instance>
<instance part="R118" gate="G$1" x="246.38" y="86.36" smashed="yes">
<attribute name="NAME" x="239.522" y="86.8426" size="1.27" layer="95"/>
<attribute name="VALUE" x="248.92" y="86.868" size="1.27" layer="96"/>
</instance>
<instance part="C75" gate="G$1" x="172.72" y="81.28" smashed="yes" rot="R90">
<attribute name="NAME" x="171.196" y="80.01" size="1.27" layer="95" ratio="10" rot="R270"/>
<attribute name="VALUE" x="172.212" y="82.55" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C76" gate="G$1" x="177.8" y="81.28" smashed="yes" rot="R90">
<attribute name="NAME" x="176.276" y="80.01" size="1.27" layer="95" ratio="10" rot="R270"/>
<attribute name="VALUE" x="177.292" y="82.55" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C77" gate="G$1" x="198.12" y="111.76" smashed="yes" rot="R180">
<attribute name="NAME" x="193.04" y="112.268" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="199.39" y="112.268" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="SUPPLY59" gate="GND" x="190.5" y="106.68" smashed="yes">
<attribute name="VALUE" x="188.595" y="103.505" size="1.27" layer="96"/>
</instance>
<instance part="P+5" gate="1" x="205.74" y="119.38" smashed="yes">
<attribute name="VALUE" x="203.708" y="119.38" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY60" gate="GND" x="172.72" y="68.58" smashed="yes">
<attribute name="VALUE" x="170.815" y="65.405" size="1.27" layer="96"/>
</instance>
<instance part="U19" gate="G$1" x="233.68" y="55.88" smashed="yes" rot="MR0">
<attribute name="NAME" x="233.68" y="58.42" size="1.27" layer="95" ratio="15" rot="MR0"/>
<attribute name="VALUE" x="233.68" y="27.94" size="1.778" layer="96" ratio="15" rot="MR0"/>
</instance>
<instance part="R119" gate="G$1" x="193.04" y="45.72" smashed="yes">
<attribute name="NAME" x="186.182" y="46.2026" size="1.27" layer="95"/>
<attribute name="VALUE" x="195.58" y="46.228" size="1.27" layer="96"/>
</instance>
<instance part="R120" gate="G$1" x="193.04" y="43.18" smashed="yes">
<attribute name="NAME" x="186.182" y="43.6626" size="1.27" layer="95"/>
<attribute name="VALUE" x="195.58" y="43.688" size="1.27" layer="96"/>
</instance>
<instance part="R121" gate="G$1" x="193.04" y="38.1" smashed="yes">
<attribute name="NAME" x="186.436" y="38.5826" size="1.27" layer="95"/>
<attribute name="VALUE" x="195.58" y="38.608" size="1.27" layer="96"/>
</instance>
<instance part="R122" gate="G$1" x="193.04" y="35.56" smashed="yes">
<attribute name="NAME" x="186.436" y="36.0426" size="1.27" layer="95"/>
<attribute name="VALUE" x="195.58" y="36.068" size="1.27" layer="96"/>
</instance>
<instance part="R123" gate="G$1" x="246.38" y="48.26" smashed="yes">
<attribute name="NAME" x="239.268" y="48.7426" size="1.27" layer="95"/>
<attribute name="VALUE" x="248.92" y="48.768" size="1.27" layer="96"/>
</instance>
<instance part="R124" gate="G$1" x="246.38" y="33.02" smashed="yes">
<attribute name="NAME" x="239.522" y="33.5026" size="1.27" layer="95"/>
<attribute name="VALUE" x="248.92" y="33.528" size="1.27" layer="96"/>
</instance>
<instance part="C78" gate="G$1" x="172.72" y="27.94" smashed="yes" rot="R90">
<attribute name="NAME" x="171.196" y="26.67" size="1.27" layer="95" ratio="10" rot="R270"/>
<attribute name="VALUE" x="172.212" y="29.21" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C79" gate="G$1" x="177.8" y="27.94" smashed="yes" rot="R90">
<attribute name="NAME" x="176.276" y="26.67" size="1.27" layer="95" ratio="10" rot="R270"/>
<attribute name="VALUE" x="177.292" y="29.21" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C80" gate="G$1" x="198.12" y="58.42" smashed="yes" rot="R180">
<attribute name="NAME" x="193.04" y="58.928" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="199.39" y="58.928" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="SUPPLY61" gate="GND" x="190.5" y="53.34" smashed="yes">
<attribute name="VALUE" x="188.595" y="50.165" size="1.27" layer="96"/>
</instance>
<instance part="P+6" gate="1" x="205.74" y="66.04" smashed="yes">
<attribute name="VALUE" x="203.708" y="66.04" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY62" gate="GND" x="172.72" y="15.24" smashed="yes">
<attribute name="VALUE" x="170.815" y="12.065" size="1.27" layer="96"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="SCL2_5V" class="0">
<segment>
<pinref part="R12" gate="G$1" pin="1"/>
<wire x1="152.4" y1="139.7" x2="162.56" y2="139.7" width="0.1524" layer="91"/>
<wire x1="152.4" y1="154.94" x2="152.4" y2="139.7" width="0.1524" layer="91"/>
<label x="162.56" y="139.7" size="1.27" layer="95" xref="yes"/>
<pinref part="U2" gate="G$1" pin="2"/>
<wire x1="119.38" y1="139.7" x2="152.4" y2="139.7" width="0.1524" layer="91"/>
<junction x="152.4" y="139.7"/>
</segment>
</net>
<net name="SDA2_5V" class="0">
<segment>
<pinref part="R11" gate="G$1" pin="1"/>
<wire x1="144.78" y1="142.24" x2="162.56" y2="142.24" width="0.1524" layer="91"/>
<wire x1="144.78" y1="154.94" x2="144.78" y2="142.24" width="0.1524" layer="91"/>
<label x="162.56" y="142.24" size="1.27" layer="95" xref="yes"/>
<pinref part="U2" gate="G$1" pin="7"/>
<wire x1="119.38" y1="142.24" x2="144.78" y2="142.24" width="0.1524" layer="91"/>
<junction x="144.78" y="142.24"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<label x="40.64" y="185.42" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="R5" gate="G$1" pin="1"/>
<wire x1="58.42" y1="185.42" x2="40.64" y2="185.42" width="0.1524" layer="91"/>
<wire x1="58.42" y1="210.82" x2="58.42" y2="185.42" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="5"/>
<wire x1="119.38" y1="193.04" x2="121.92" y2="193.04" width="0.1524" layer="91"/>
<wire x1="121.92" y1="193.04" x2="121.92" y2="185.42" width="0.1524" layer="91"/>
<wire x1="121.92" y1="185.42" x2="58.42" y2="185.42" width="0.1524" layer="91"/>
<junction x="58.42" y="185.42"/>
</segment>
<segment>
<label x="40.64" y="129.54" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="R10" gate="G$1" pin="1"/>
<wire x1="58.42" y1="129.54" x2="40.64" y2="129.54" width="0.1524" layer="91"/>
<wire x1="58.42" y1="154.94" x2="58.42" y2="129.54" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="5"/>
<wire x1="119.38" y1="137.16" x2="121.92" y2="137.16" width="0.1524" layer="91"/>
<wire x1="121.92" y1="137.16" x2="121.92" y2="129.54" width="0.1524" layer="91"/>
<wire x1="121.92" y1="129.54" x2="58.42" y2="129.54" width="0.1524" layer="91"/>
<junction x="58.42" y="129.54"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="C20" gate="G$1" pin="1"/>
<wire x1="129.54" y1="210.82" x2="129.54" y2="208.28" width="0.1524" layer="91"/>
<wire x1="129.54" y1="208.28" x2="137.16" y2="208.28" width="0.1524" layer="91"/>
<wire x1="137.16" y1="208.28" x2="139.7" y2="208.28" width="0.1524" layer="91"/>
<wire x1="139.7" y1="208.28" x2="139.7" y2="205.74" width="0.1524" layer="91"/>
<pinref part="C22" gate="G$1" pin="1"/>
<wire x1="137.16" y1="210.82" x2="137.16" y2="208.28" width="0.1524" layer="91"/>
<junction x="137.16" y="208.28"/>
<pinref part="SUPPLY8" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C21" gate="G$1" pin="1"/>
<wire x1="81.28" y1="210.82" x2="81.28" y2="208.28" width="0.1524" layer="91"/>
<wire x1="81.28" y1="208.28" x2="73.66" y2="208.28" width="0.1524" layer="91"/>
<wire x1="73.66" y1="208.28" x2="71.12" y2="208.28" width="0.1524" layer="91"/>
<wire x1="71.12" y1="208.28" x2="71.12" y2="205.74" width="0.1524" layer="91"/>
<pinref part="C23" gate="G$1" pin="1"/>
<wire x1="73.66" y1="210.82" x2="73.66" y2="208.28" width="0.1524" layer="91"/>
<junction x="73.66" y="208.28"/>
<pinref part="SUPPLY9" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="4"/>
<pinref part="SUPPLY12" gate="GND" pin="GND"/>
<wire x1="91.44" y1="193.04" x2="88.9" y2="193.04" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C24" gate="G$1" pin="1"/>
<wire x1="129.54" y1="154.94" x2="129.54" y2="152.4" width="0.1524" layer="91"/>
<wire x1="129.54" y1="152.4" x2="137.16" y2="152.4" width="0.1524" layer="91"/>
<wire x1="137.16" y1="152.4" x2="139.7" y2="152.4" width="0.1524" layer="91"/>
<wire x1="139.7" y1="152.4" x2="139.7" y2="149.86" width="0.1524" layer="91"/>
<pinref part="C26" gate="G$1" pin="1"/>
<wire x1="137.16" y1="154.94" x2="137.16" y2="152.4" width="0.1524" layer="91"/>
<junction x="137.16" y="152.4"/>
<pinref part="SUPPLY11" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C25" gate="G$1" pin="1"/>
<wire x1="81.28" y1="154.94" x2="81.28" y2="152.4" width="0.1524" layer="91"/>
<wire x1="81.28" y1="152.4" x2="73.66" y2="152.4" width="0.1524" layer="91"/>
<wire x1="73.66" y1="152.4" x2="71.12" y2="152.4" width="0.1524" layer="91"/>
<wire x1="71.12" y1="152.4" x2="71.12" y2="149.86" width="0.1524" layer="91"/>
<pinref part="C27" gate="G$1" pin="1"/>
<wire x1="73.66" y1="154.94" x2="73.66" y2="152.4" width="0.1524" layer="91"/>
<junction x="73.66" y="152.4"/>
<pinref part="SUPPLY13" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="4"/>
<pinref part="SUPPLY14" gate="GND" pin="GND"/>
<wire x1="91.44" y1="137.16" x2="88.9" y2="137.16" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U10" gate="G$1" pin="P$4"/>
<wire x1="254" y1="170.18" x2="248.92" y2="170.18" width="0.1524" layer="91"/>
<wire x1="248.92" y1="170.18" x2="248.92" y2="165.1" width="0.1524" layer="91"/>
<pinref part="SUPPLY51" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="U10" gate="G$1" pin="P$9"/>
<wire x1="284.48" y1="190.5" x2="289.56" y2="190.5" width="0.1524" layer="91"/>
<wire x1="289.56" y1="190.5" x2="289.56" y2="165.1" width="0.1524" layer="91"/>
<pinref part="SUPPLY52" gate="GND" pin="GND"/>
<pinref part="C55" gate="G$1" pin="1"/>
<wire x1="289.56" y1="193.04" x2="289.56" y2="190.5" width="0.1524" layer="91"/>
<junction x="289.56" y="190.5"/>
</segment>
<segment>
<pinref part="C71" gate="G$1" pin="2"/>
<wire x1="66.04" y1="111.76" x2="63.5" y2="111.76" width="0.1524" layer="91"/>
<pinref part="SUPPLY55" gate="GND" pin="GND"/>
<wire x1="63.5" y1="111.76" x2="63.5" y2="109.22" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C69" gate="G$1" pin="1"/>
<pinref part="SUPPLY56" gate="GND" pin="GND"/>
<wire x1="45.72" y1="76.2" x2="45.72" y2="73.66" width="0.1524" layer="91"/>
<pinref part="C70" gate="G$1" pin="1"/>
<wire x1="45.72" y1="73.66" x2="45.72" y2="71.12" width="0.1524" layer="91"/>
<wire x1="50.8" y1="76.2" x2="50.8" y2="73.66" width="0.1524" layer="91"/>
<wire x1="50.8" y1="73.66" x2="45.72" y2="73.66" width="0.1524" layer="91"/>
<junction x="45.72" y="73.66"/>
</segment>
<segment>
<pinref part="C74" gate="G$1" pin="2"/>
<wire x1="66.04" y1="58.42" x2="63.5" y2="58.42" width="0.1524" layer="91"/>
<pinref part="SUPPLY57" gate="GND" pin="GND"/>
<wire x1="63.5" y1="58.42" x2="63.5" y2="55.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C72" gate="G$1" pin="1"/>
<pinref part="SUPPLY58" gate="GND" pin="GND"/>
<wire x1="45.72" y1="22.86" x2="45.72" y2="20.32" width="0.1524" layer="91"/>
<pinref part="C73" gate="G$1" pin="1"/>
<wire x1="45.72" y1="20.32" x2="45.72" y2="17.78" width="0.1524" layer="91"/>
<wire x1="50.8" y1="22.86" x2="50.8" y2="20.32" width="0.1524" layer="91"/>
<wire x1="50.8" y1="20.32" x2="45.72" y2="20.32" width="0.1524" layer="91"/>
<junction x="45.72" y="20.32"/>
</segment>
<segment>
<pinref part="C77" gate="G$1" pin="2"/>
<wire x1="193.04" y1="111.76" x2="190.5" y2="111.76" width="0.1524" layer="91"/>
<pinref part="SUPPLY59" gate="GND" pin="GND"/>
<wire x1="190.5" y1="111.76" x2="190.5" y2="109.22" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C75" gate="G$1" pin="1"/>
<pinref part="SUPPLY60" gate="GND" pin="GND"/>
<wire x1="172.72" y1="76.2" x2="172.72" y2="73.66" width="0.1524" layer="91"/>
<pinref part="C76" gate="G$1" pin="1"/>
<wire x1="172.72" y1="73.66" x2="172.72" y2="71.12" width="0.1524" layer="91"/>
<wire x1="177.8" y1="76.2" x2="177.8" y2="73.66" width="0.1524" layer="91"/>
<wire x1="177.8" y1="73.66" x2="172.72" y2="73.66" width="0.1524" layer="91"/>
<junction x="172.72" y="73.66"/>
</segment>
<segment>
<pinref part="C80" gate="G$1" pin="2"/>
<wire x1="193.04" y1="58.42" x2="190.5" y2="58.42" width="0.1524" layer="91"/>
<pinref part="SUPPLY61" gate="GND" pin="GND"/>
<wire x1="190.5" y1="58.42" x2="190.5" y2="55.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C78" gate="G$1" pin="1"/>
<pinref part="SUPPLY62" gate="GND" pin="GND"/>
<wire x1="172.72" y1="22.86" x2="172.72" y2="20.32" width="0.1524" layer="91"/>
<pinref part="C79" gate="G$1" pin="1"/>
<wire x1="172.72" y1="20.32" x2="172.72" y2="17.78" width="0.1524" layer="91"/>
<wire x1="177.8" y1="22.86" x2="177.8" y2="20.32" width="0.1524" layer="91"/>
<wire x1="177.8" y1="20.32" x2="172.72" y2="20.32" width="0.1524" layer="91"/>
<junction x="172.72" y="20.32"/>
</segment>
</net>
<net name="COM2_SCL" class="0">
<segment>
<pinref part="R9" gate="G$1" pin="1"/>
<wire x1="66.04" y1="154.94" x2="66.04" y2="139.7" width="0.1524" layer="91"/>
<wire x1="40.64" y1="139.7" x2="66.04" y2="139.7" width="0.1524" layer="91"/>
<label x="40.64" y="139.7" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="U2" gate="G$1" pin="3"/>
<wire x1="91.44" y1="139.7" x2="66.04" y2="139.7" width="0.1524" layer="91"/>
<junction x="66.04" y="139.7"/>
</segment>
</net>
<net name="COM2_SDA" class="0">
<segment>
<pinref part="R13" gate="G$1" pin="1"/>
<wire x1="50.8" y1="154.94" x2="50.8" y2="142.24" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="6"/>
<wire x1="91.44" y1="142.24" x2="50.8" y2="142.24" width="0.1524" layer="91"/>
<wire x1="50.8" y1="142.24" x2="40.64" y2="142.24" width="0.1524" layer="91"/>
<junction x="50.8" y="142.24"/>
<label x="40.64" y="142.24" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$70" class="0">
<segment>
<pinref part="U10" gate="G$1" pin="P$1"/>
<pinref part="R51" gate="G$1" pin="1"/>
<wire x1="254" y1="185.42" x2="238.76" y2="185.42" width="0.1524" layer="91"/>
<wire x1="238.76" y1="185.42" x2="238.76" y2="193.04" width="0.1524" layer="91"/>
<wire x1="238.76" y1="185.42" x2="210.82" y2="185.42" width="0.1524" layer="91"/>
<junction x="238.76" y="185.42"/>
<label x="210.82" y="185.42" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$71" class="0">
<segment>
<pinref part="U10" gate="G$1" pin="P$3"/>
<wire x1="254" y1="175.26" x2="223.52" y2="175.26" width="0.1524" layer="91"/>
<pinref part="R56" gate="G$1" pin="1"/>
<wire x1="223.52" y1="175.26" x2="223.52" y2="193.04" width="0.1524" layer="91"/>
<wire x1="223.52" y1="175.26" x2="210.82" y2="175.26" width="0.1524" layer="91"/>
<junction x="223.52" y="175.26"/>
<label x="210.82" y="175.26" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$72" class="0">
<segment>
<pinref part="U10" gate="G$1" pin="P$2"/>
<pinref part="R52" gate="G$1" pin="1"/>
<wire x1="254" y1="180.34" x2="231.14" y2="180.34" width="0.1524" layer="91"/>
<wire x1="231.14" y1="180.34" x2="231.14" y2="193.04" width="0.1524" layer="91"/>
<wire x1="231.14" y1="180.34" x2="210.82" y2="180.34" width="0.1524" layer="91"/>
<junction x="231.14" y="180.34"/>
<label x="210.82" y="180.34" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$73" class="0">
<segment>
<pinref part="U10" gate="G$1" pin="P$7"/>
<pinref part="R53" gate="G$1" pin="1"/>
<wire x1="284.48" y1="180.34" x2="302.26" y2="180.34" width="0.1524" layer="91"/>
<wire x1="302.26" y1="180.34" x2="302.26" y2="193.04" width="0.1524" layer="91"/>
<wire x1="302.26" y1="180.34" x2="327.66" y2="180.34" width="0.1524" layer="91"/>
<junction x="302.26" y="180.34"/>
<label x="327.66" y="180.34" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$74" class="0">
<segment>
<pinref part="U10" gate="G$1" pin="P$6"/>
<pinref part="R54" gate="G$1" pin="1"/>
<wire x1="284.48" y1="175.26" x2="309.88" y2="175.26" width="0.1524" layer="91"/>
<wire x1="309.88" y1="175.26" x2="309.88" y2="193.04" width="0.1524" layer="91"/>
<wire x1="309.88" y1="175.26" x2="327.66" y2="175.26" width="0.1524" layer="91"/>
<junction x="309.88" y="175.26"/>
<label x="327.66" y="175.26" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$75" class="0">
<segment>
<pinref part="U10" gate="G$1" pin="P$5"/>
<pinref part="R55" gate="G$1" pin="1"/>
<wire x1="284.48" y1="170.18" x2="317.5" y2="170.18" width="0.1524" layer="91"/>
<wire x1="317.5" y1="170.18" x2="317.5" y2="193.04" width="0.1524" layer="91"/>
<wire x1="317.5" y1="170.18" x2="327.66" y2="170.18" width="0.1524" layer="91"/>
<junction x="317.5" y="170.18"/>
<label x="327.66" y="170.18" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<pinref part="R56" gate="G$1" pin="2"/>
<wire x1="223.52" y1="203.2" x2="223.52" y2="208.28" width="0.1524" layer="91"/>
<pinref part="R55" gate="G$1" pin="2"/>
<wire x1="223.52" y1="208.28" x2="231.14" y2="208.28" width="0.1524" layer="91"/>
<wire x1="231.14" y1="208.28" x2="238.76" y2="208.28" width="0.1524" layer="91"/>
<wire x1="238.76" y1="208.28" x2="294.64" y2="208.28" width="0.1524" layer="91"/>
<wire x1="294.64" y1="208.28" x2="302.26" y2="208.28" width="0.1524" layer="91"/>
<wire x1="302.26" y1="208.28" x2="309.88" y2="208.28" width="0.1524" layer="91"/>
<wire x1="309.88" y1="208.28" x2="317.5" y2="208.28" width="0.1524" layer="91"/>
<wire x1="317.5" y1="208.28" x2="317.5" y2="203.2" width="0.1524" layer="91"/>
<pinref part="R54" gate="G$1" pin="2"/>
<wire x1="309.88" y1="203.2" x2="309.88" y2="208.28" width="0.1524" layer="91"/>
<junction x="309.88" y="208.28"/>
<pinref part="R53" gate="G$1" pin="2"/>
<wire x1="302.26" y1="203.2" x2="302.26" y2="208.28" width="0.1524" layer="91"/>
<junction x="302.26" y="208.28"/>
<pinref part="R51" gate="G$1" pin="2"/>
<wire x1="238.76" y1="203.2" x2="238.76" y2="208.28" width="0.1524" layer="91"/>
<junction x="238.76" y="208.28"/>
<pinref part="R52" gate="G$1" pin="2"/>
<wire x1="231.14" y1="203.2" x2="231.14" y2="208.28" width="0.1524" layer="91"/>
<junction x="231.14" y="208.28"/>
<pinref part="U10" gate="G$1" pin="P$8"/>
<wire x1="284.48" y1="185.42" x2="294.64" y2="185.42" width="0.1524" layer="91"/>
<wire x1="294.64" y1="185.42" x2="294.64" y2="205.74" width="0.1524" layer="91"/>
<junction x="294.64" y="208.28"/>
<pinref part="C55" gate="G$1" pin="2"/>
<wire x1="294.64" y1="205.74" x2="294.64" y2="208.28" width="0.1524" layer="91"/>
<wire x1="289.56" y1="203.2" x2="289.56" y2="205.74" width="0.1524" layer="91"/>
<wire x1="289.56" y1="205.74" x2="294.64" y2="205.74" width="0.1524" layer="91"/>
<junction x="294.64" y="205.74"/>
<wire x1="317.5" y1="208.28" x2="322.58" y2="208.28" width="0.1524" layer="91"/>
<junction x="317.5" y="208.28"/>
<pinref part="+3V6" gate="G$1" pin="+3V3"/>
<wire x1="322.58" y1="208.28" x2="322.58" y2="213.36" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="91.44" y1="200.66" x2="88.9" y2="200.66" width="0.1524" layer="91"/>
<wire x1="88.9" y1="200.66" x2="88.9" y2="223.52" width="0.1524" layer="91"/>
<wire x1="88.9" y1="223.52" x2="81.28" y2="223.52" width="0.1524" layer="91"/>
<pinref part="C21" gate="G$1" pin="2"/>
<wire x1="81.28" y1="223.52" x2="73.66" y2="223.52" width="0.1524" layer="91"/>
<wire x1="73.66" y1="223.52" x2="66.04" y2="223.52" width="0.1524" layer="91"/>
<wire x1="66.04" y1="223.52" x2="58.42" y2="223.52" width="0.1524" layer="91"/>
<wire x1="58.42" y1="223.52" x2="50.8" y2="223.52" width="0.1524" layer="91"/>
<wire x1="50.8" y1="223.52" x2="43.18" y2="223.52" width="0.1524" layer="91"/>
<wire x1="43.18" y1="223.52" x2="40.64" y2="223.52" width="0.1524" layer="91"/>
<wire x1="81.28" y1="220.98" x2="81.28" y2="223.52" width="0.1524" layer="91"/>
<pinref part="C23" gate="G$1" pin="2"/>
<wire x1="73.66" y1="220.98" x2="73.66" y2="223.52" width="0.1524" layer="91"/>
<pinref part="R4" gate="G$1" pin="2"/>
<wire x1="66.04" y1="220.98" x2="66.04" y2="223.52" width="0.1524" layer="91"/>
<pinref part="R5" gate="G$1" pin="2"/>
<wire x1="58.42" y1="220.98" x2="58.42" y2="223.52" width="0.1524" layer="91"/>
<junction x="81.28" y="223.52"/>
<junction x="73.66" y="223.52"/>
<junction x="66.04" y="223.52"/>
<junction x="58.42" y="223.52"/>
<label x="40.64" y="223.52" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="R8" gate="G$1" pin="2"/>
<wire x1="50.8" y1="220.98" x2="50.8" y2="223.52" width="0.1524" layer="91"/>
<junction x="50.8" y="223.52"/>
<pinref part="U4" gate="G$1" pin="8"/>
<pinref part="+3V1" gate="G$1" pin="+3V3"/>
<wire x1="43.18" y1="228.6" x2="43.18" y2="223.52" width="0.1524" layer="91"/>
<junction x="43.18" y="223.52"/>
</segment>
<segment>
<wire x1="91.44" y1="144.78" x2="88.9" y2="144.78" width="0.1524" layer="91"/>
<wire x1="88.9" y1="144.78" x2="88.9" y2="167.64" width="0.1524" layer="91"/>
<wire x1="88.9" y1="167.64" x2="81.28" y2="167.64" width="0.1524" layer="91"/>
<pinref part="C25" gate="G$1" pin="2"/>
<wire x1="81.28" y1="167.64" x2="73.66" y2="167.64" width="0.1524" layer="91"/>
<wire x1="73.66" y1="167.64" x2="66.04" y2="167.64" width="0.1524" layer="91"/>
<wire x1="66.04" y1="167.64" x2="58.42" y2="167.64" width="0.1524" layer="91"/>
<wire x1="58.42" y1="167.64" x2="50.8" y2="167.64" width="0.1524" layer="91"/>
<wire x1="50.8" y1="167.64" x2="43.18" y2="167.64" width="0.1524" layer="91"/>
<wire x1="43.18" y1="167.64" x2="40.64" y2="167.64" width="0.1524" layer="91"/>
<wire x1="81.28" y1="165.1" x2="81.28" y2="167.64" width="0.1524" layer="91"/>
<pinref part="C27" gate="G$1" pin="2"/>
<wire x1="73.66" y1="165.1" x2="73.66" y2="167.64" width="0.1524" layer="91"/>
<pinref part="R9" gate="G$1" pin="2"/>
<wire x1="66.04" y1="165.1" x2="66.04" y2="167.64" width="0.1524" layer="91"/>
<pinref part="R10" gate="G$1" pin="2"/>
<wire x1="58.42" y1="165.1" x2="58.42" y2="167.64" width="0.1524" layer="91"/>
<junction x="81.28" y="167.64"/>
<junction x="73.66" y="167.64"/>
<junction x="66.04" y="167.64"/>
<junction x="58.42" y="167.64"/>
<label x="40.64" y="167.64" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="R13" gate="G$1" pin="2"/>
<wire x1="50.8" y1="165.1" x2="50.8" y2="167.64" width="0.1524" layer="91"/>
<junction x="50.8" y="167.64"/>
<pinref part="U2" gate="G$1" pin="8"/>
<pinref part="+3V2" gate="G$1" pin="+3V3"/>
<wire x1="43.18" y1="172.72" x2="43.18" y2="167.64" width="0.1524" layer="91"/>
<junction x="43.18" y="167.64"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<wire x1="119.38" y1="200.66" x2="121.92" y2="200.66" width="0.1524" layer="91"/>
<wire x1="121.92" y1="200.66" x2="121.92" y2="223.52" width="0.1524" layer="91"/>
<wire x1="121.92" y1="223.52" x2="129.54" y2="223.52" width="0.1524" layer="91"/>
<pinref part="C20" gate="G$1" pin="2"/>
<wire x1="129.54" y1="223.52" x2="137.16" y2="223.52" width="0.1524" layer="91"/>
<wire x1="137.16" y1="223.52" x2="144.78" y2="223.52" width="0.1524" layer="91"/>
<wire x1="144.78" y1="223.52" x2="152.4" y2="223.52" width="0.1524" layer="91"/>
<wire x1="152.4" y1="223.52" x2="160.02" y2="223.52" width="0.1524" layer="91"/>
<wire x1="160.02" y1="223.52" x2="162.56" y2="223.52" width="0.1524" layer="91"/>
<wire x1="129.54" y1="220.98" x2="129.54" y2="223.52" width="0.1524" layer="91"/>
<pinref part="C22" gate="G$1" pin="2"/>
<wire x1="137.16" y1="220.98" x2="137.16" y2="223.52" width="0.1524" layer="91"/>
<pinref part="R6" gate="G$1" pin="2"/>
<wire x1="144.78" y1="220.98" x2="144.78" y2="223.52" width="0.1524" layer="91"/>
<pinref part="R7" gate="G$1" pin="2"/>
<wire x1="152.4" y1="220.98" x2="152.4" y2="223.52" width="0.1524" layer="91"/>
<junction x="152.4" y="223.52"/>
<junction x="144.78" y="223.52"/>
<junction x="137.16" y="223.52"/>
<junction x="129.54" y="223.52"/>
<label x="162.56" y="223.52" size="1.27" layer="95" xref="yes"/>
<pinref part="U4" gate="G$1" pin="1"/>
<pinref part="P+1" gate="1" pin="+5V"/>
<wire x1="160.02" y1="228.6" x2="160.02" y2="223.52" width="0.1524" layer="91"/>
<junction x="160.02" y="223.52"/>
</segment>
<segment>
<wire x1="119.38" y1="144.78" x2="121.92" y2="144.78" width="0.1524" layer="91"/>
<wire x1="121.92" y1="144.78" x2="121.92" y2="167.64" width="0.1524" layer="91"/>
<wire x1="121.92" y1="167.64" x2="129.54" y2="167.64" width="0.1524" layer="91"/>
<pinref part="C24" gate="G$1" pin="2"/>
<wire x1="129.54" y1="167.64" x2="137.16" y2="167.64" width="0.1524" layer="91"/>
<wire x1="137.16" y1="167.64" x2="144.78" y2="167.64" width="0.1524" layer="91"/>
<wire x1="144.78" y1="167.64" x2="152.4" y2="167.64" width="0.1524" layer="91"/>
<wire x1="152.4" y1="167.64" x2="160.02" y2="167.64" width="0.1524" layer="91"/>
<wire x1="160.02" y1="167.64" x2="162.56" y2="167.64" width="0.1524" layer="91"/>
<wire x1="129.54" y1="165.1" x2="129.54" y2="167.64" width="0.1524" layer="91"/>
<pinref part="C26" gate="G$1" pin="2"/>
<wire x1="137.16" y1="165.1" x2="137.16" y2="167.64" width="0.1524" layer="91"/>
<pinref part="R11" gate="G$1" pin="2"/>
<wire x1="144.78" y1="165.1" x2="144.78" y2="167.64" width="0.1524" layer="91"/>
<pinref part="R12" gate="G$1" pin="2"/>
<wire x1="152.4" y1="165.1" x2="152.4" y2="167.64" width="0.1524" layer="91"/>
<junction x="152.4" y="167.64"/>
<junction x="144.78" y="167.64"/>
<junction x="137.16" y="167.64"/>
<junction x="129.54" y="167.64"/>
<label x="162.56" y="167.64" size="1.27" layer="95" xref="yes"/>
<pinref part="U2" gate="G$1" pin="1"/>
<pinref part="P+2" gate="1" pin="+5V"/>
<wire x1="160.02" y1="172.72" x2="160.02" y2="167.64" width="0.1524" layer="91"/>
<junction x="160.02" y="167.64"/>
</segment>
<segment>
<pinref part="U16" gate="G$1" pin="8"/>
<wire x1="83.82" y1="101.6" x2="78.74" y2="101.6" width="0.1524" layer="91"/>
<wire x1="78.74" y1="101.6" x2="78.74" y2="111.76" width="0.1524" layer="91"/>
<pinref part="C71" gate="G$1" pin="1"/>
<wire x1="78.74" y1="111.76" x2="78.74" y2="116.84" width="0.1524" layer="91"/>
<wire x1="76.2" y1="111.76" x2="78.74" y2="111.76" width="0.1524" layer="91"/>
<junction x="78.74" y="111.76"/>
<pinref part="P+3" gate="1" pin="+5V"/>
</segment>
<segment>
<pinref part="U17" gate="G$1" pin="8"/>
<wire x1="83.82" y1="48.26" x2="78.74" y2="48.26" width="0.1524" layer="91"/>
<wire x1="78.74" y1="48.26" x2="78.74" y2="58.42" width="0.1524" layer="91"/>
<pinref part="C74" gate="G$1" pin="1"/>
<wire x1="78.74" y1="58.42" x2="78.74" y2="63.5" width="0.1524" layer="91"/>
<wire x1="76.2" y1="58.42" x2="78.74" y2="58.42" width="0.1524" layer="91"/>
<junction x="78.74" y="58.42"/>
<pinref part="P+4" gate="1" pin="+5V"/>
</segment>
<segment>
<pinref part="U18" gate="G$1" pin="8"/>
<wire x1="210.82" y1="101.6" x2="205.74" y2="101.6" width="0.1524" layer="91"/>
<wire x1="205.74" y1="101.6" x2="205.74" y2="111.76" width="0.1524" layer="91"/>
<pinref part="C77" gate="G$1" pin="1"/>
<wire x1="205.74" y1="111.76" x2="205.74" y2="116.84" width="0.1524" layer="91"/>
<wire x1="203.2" y1="111.76" x2="205.74" y2="111.76" width="0.1524" layer="91"/>
<junction x="205.74" y="111.76"/>
<pinref part="P+5" gate="1" pin="+5V"/>
</segment>
<segment>
<pinref part="U19" gate="G$1" pin="8"/>
<wire x1="210.82" y1="48.26" x2="205.74" y2="48.26" width="0.1524" layer="91"/>
<wire x1="205.74" y1="48.26" x2="205.74" y2="58.42" width="0.1524" layer="91"/>
<pinref part="C80" gate="G$1" pin="1"/>
<wire x1="205.74" y1="58.42" x2="205.74" y2="63.5" width="0.1524" layer="91"/>
<wire x1="203.2" y1="58.42" x2="205.74" y2="58.42" width="0.1524" layer="91"/>
<junction x="205.74" y="58.42"/>
<pinref part="P+6" gate="1" pin="+5V"/>
</segment>
</net>
<net name="N$99" class="0">
<segment>
<pinref part="R101" gate="G$1" pin="2"/>
<wire x1="71.12" y1="99.06" x2="76.2" y2="99.06" width="0.1524" layer="91"/>
<wire x1="76.2" y1="99.06" x2="76.2" y2="96.52" width="0.1524" layer="91"/>
<pinref part="U16" gate="G$1" pin="7"/>
<wire x1="76.2" y1="96.52" x2="83.82" y2="96.52" width="0.1524" layer="91"/>
<pinref part="R102" gate="G$1" pin="2"/>
<wire x1="71.12" y1="96.52" x2="76.2" y2="96.52" width="0.1524" layer="91"/>
<junction x="76.2" y="96.52"/>
</segment>
</net>
<net name="N$120" class="0">
<segment>
<pinref part="R104" gate="G$1" pin="2"/>
<wire x1="71.12" y1="88.9" x2="76.2" y2="88.9" width="0.1524" layer="91"/>
<wire x1="76.2" y1="88.9" x2="76.2" y2="91.44" width="0.1524" layer="91"/>
<pinref part="U16" gate="G$1" pin="6"/>
<wire x1="76.2" y1="91.44" x2="83.82" y2="91.44" width="0.1524" layer="91"/>
<pinref part="R103" gate="G$1" pin="2"/>
<wire x1="71.12" y1="91.44" x2="76.2" y2="91.44" width="0.1524" layer="91"/>
<junction x="76.2" y="91.44"/>
</segment>
</net>
<net name="AISG_4_RS485_P" class="0">
<segment>
<pinref part="R120" gate="G$1" pin="1"/>
<wire x1="187.96" y1="43.18" x2="185.42" y2="43.18" width="0.1524" layer="91"/>
<wire x1="185.42" y1="43.18" x2="185.42" y2="38.1" width="0.1524" layer="91"/>
<pinref part="R121" gate="G$1" pin="1"/>
<wire x1="185.42" y1="38.1" x2="187.96" y2="38.1" width="0.1524" layer="91"/>
<junction x="185.42" y="38.1"/>
<wire x1="185.42" y1="38.1" x2="172.72" y2="38.1" width="0.1524" layer="91"/>
<pinref part="C78" gate="G$1" pin="2"/>
<wire x1="172.72" y1="38.1" x2="165.1" y2="38.1" width="0.1524" layer="91"/>
<wire x1="172.72" y1="33.02" x2="172.72" y2="38.1" width="0.1524" layer="91"/>
<junction x="172.72" y="38.1"/>
<label x="165.1" y="38.1" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="AISG_4_RS485_N" class="0">
<segment>
<pinref part="R119" gate="G$1" pin="1"/>
<wire x1="187.96" y1="45.72" x2="182.88" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R122" gate="G$1" pin="1"/>
<wire x1="182.88" y1="45.72" x2="182.88" y2="35.56" width="0.1524" layer="91"/>
<wire x1="182.88" y1="35.56" x2="187.96" y2="35.56" width="0.1524" layer="91"/>
<junction x="182.88" y="35.56"/>
<wire x1="182.88" y1="35.56" x2="177.8" y2="35.56" width="0.1524" layer="91"/>
<pinref part="C79" gate="G$1" pin="2"/>
<wire x1="177.8" y1="35.56" x2="165.1" y2="35.56" width="0.1524" layer="91"/>
<wire x1="177.8" y1="33.02" x2="177.8" y2="35.56" width="0.1524" layer="91"/>
<junction x="177.8" y="35.56"/>
<label x="165.1" y="35.56" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$124" class="0">
<segment>
<pinref part="U16" gate="G$1" pin="1"/>
<pinref part="R105" gate="G$1" pin="1"/>
<wire x1="109.22" y1="101.6" x2="114.3" y2="101.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$125" class="0">
<segment>
<pinref part="R105" gate="G$1" pin="2"/>
<wire x1="124.46" y1="101.6" x2="129.54" y2="101.6" width="0.1524" layer="91"/>
<label x="129.54" y="101.6" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R111" gate="G$1" pin="2"/>
<wire x1="124.46" y1="48.26" x2="129.54" y2="48.26" width="0.1524" layer="91"/>
<label x="129.54" y="48.26" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R117" gate="G$1" pin="2"/>
<wire x1="251.46" y1="101.6" x2="256.54" y2="101.6" width="0.1524" layer="91"/>
<label x="256.54" y="101.6" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R123" gate="G$1" pin="2"/>
<wire x1="251.46" y1="48.26" x2="256.54" y2="48.26" width="0.1524" layer="91"/>
<label x="256.54" y="48.26" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$126" class="0">
<segment>
<pinref part="U16" gate="G$1" pin="4"/>
<pinref part="R106" gate="G$1" pin="1"/>
<wire x1="109.22" y1="86.36" x2="114.3" y2="86.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$127" class="0">
<segment>
<pinref part="R106" gate="G$1" pin="2"/>
<wire x1="124.46" y1="86.36" x2="129.54" y2="86.36" width="0.1524" layer="91"/>
<label x="129.54" y="86.36" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R112" gate="G$1" pin="2"/>
<wire x1="124.46" y1="33.02" x2="129.54" y2="33.02" width="0.1524" layer="91"/>
<label x="129.54" y="33.02" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R118" gate="G$1" pin="2"/>
<wire x1="251.46" y1="86.36" x2="256.54" y2="86.36" width="0.1524" layer="91"/>
<label x="256.54" y="86.36" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R124" gate="G$1" pin="2"/>
<wire x1="251.46" y1="33.02" x2="256.54" y2="33.02" width="0.1524" layer="91"/>
<label x="256.54" y="33.02" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$128" class="0">
<segment>
<pinref part="U16" gate="G$1" pin="2"/>
<wire x1="109.22" y1="96.52" x2="114.3" y2="96.52" width="0.1524" layer="91"/>
<wire x1="114.3" y1="96.52" x2="114.3" y2="91.44" width="0.1524" layer="91"/>
<pinref part="U16" gate="G$1" pin="3"/>
<wire x1="114.3" y1="91.44" x2="109.22" y2="91.44" width="0.1524" layer="91"/>
<wire x1="114.3" y1="91.44" x2="129.54" y2="91.44" width="0.1524" layer="91"/>
<junction x="114.3" y="91.44"/>
<label x="129.54" y="91.44" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U17" gate="G$1" pin="2"/>
<wire x1="109.22" y1="43.18" x2="114.3" y2="43.18" width="0.1524" layer="91"/>
<wire x1="114.3" y1="43.18" x2="114.3" y2="38.1" width="0.1524" layer="91"/>
<pinref part="U17" gate="G$1" pin="3"/>
<wire x1="114.3" y1="38.1" x2="109.22" y2="38.1" width="0.1524" layer="91"/>
<wire x1="114.3" y1="38.1" x2="129.54" y2="38.1" width="0.1524" layer="91"/>
<junction x="114.3" y="38.1"/>
<label x="129.54" y="38.1" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U18" gate="G$1" pin="2"/>
<wire x1="236.22" y1="96.52" x2="241.3" y2="96.52" width="0.1524" layer="91"/>
<wire x1="241.3" y1="96.52" x2="241.3" y2="91.44" width="0.1524" layer="91"/>
<pinref part="U18" gate="G$1" pin="3"/>
<wire x1="241.3" y1="91.44" x2="236.22" y2="91.44" width="0.1524" layer="91"/>
<wire x1="241.3" y1="91.44" x2="256.54" y2="91.44" width="0.1524" layer="91"/>
<junction x="241.3" y="91.44"/>
<label x="256.54" y="91.44" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U19" gate="G$1" pin="2"/>
<wire x1="236.22" y1="43.18" x2="241.3" y2="43.18" width="0.1524" layer="91"/>
<wire x1="241.3" y1="43.18" x2="241.3" y2="38.1" width="0.1524" layer="91"/>
<pinref part="U19" gate="G$1" pin="3"/>
<wire x1="241.3" y1="38.1" x2="236.22" y2="38.1" width="0.1524" layer="91"/>
<wire x1="241.3" y1="38.1" x2="256.54" y2="38.1" width="0.1524" layer="91"/>
<junction x="241.3" y="38.1"/>
<label x="256.54" y="38.1" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$129" class="0">
<segment>
<pinref part="R107" gate="G$1" pin="2"/>
<wire x1="71.12" y1="45.72" x2="76.2" y2="45.72" width="0.1524" layer="91"/>
<wire x1="76.2" y1="45.72" x2="76.2" y2="43.18" width="0.1524" layer="91"/>
<pinref part="U17" gate="G$1" pin="7"/>
<wire x1="76.2" y1="43.18" x2="83.82" y2="43.18" width="0.1524" layer="91"/>
<pinref part="R108" gate="G$1" pin="2"/>
<wire x1="71.12" y1="43.18" x2="76.2" y2="43.18" width="0.1524" layer="91"/>
<junction x="76.2" y="43.18"/>
</segment>
</net>
<net name="N$130" class="0">
<segment>
<pinref part="R110" gate="G$1" pin="2"/>
<wire x1="71.12" y1="35.56" x2="76.2" y2="35.56" width="0.1524" layer="91"/>
<wire x1="76.2" y1="35.56" x2="76.2" y2="38.1" width="0.1524" layer="91"/>
<pinref part="U17" gate="G$1" pin="6"/>
<wire x1="76.2" y1="38.1" x2="83.82" y2="38.1" width="0.1524" layer="91"/>
<pinref part="R109" gate="G$1" pin="2"/>
<wire x1="71.12" y1="38.1" x2="76.2" y2="38.1" width="0.1524" layer="91"/>
<junction x="76.2" y="38.1"/>
</segment>
</net>
<net name="N$131" class="0">
<segment>
<pinref part="U17" gate="G$1" pin="1"/>
<pinref part="R111" gate="G$1" pin="1"/>
<wire x1="109.22" y1="48.26" x2="114.3" y2="48.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$132" class="0">
<segment>
<pinref part="U17" gate="G$1" pin="4"/>
<pinref part="R112" gate="G$1" pin="1"/>
<wire x1="109.22" y1="33.02" x2="114.3" y2="33.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$133" class="0">
<segment>
<pinref part="R113" gate="G$1" pin="2"/>
<wire x1="198.12" y1="99.06" x2="203.2" y2="99.06" width="0.1524" layer="91"/>
<wire x1="203.2" y1="99.06" x2="203.2" y2="96.52" width="0.1524" layer="91"/>
<pinref part="U18" gate="G$1" pin="7"/>
<wire x1="203.2" y1="96.52" x2="210.82" y2="96.52" width="0.1524" layer="91"/>
<pinref part="R114" gate="G$1" pin="2"/>
<wire x1="198.12" y1="96.52" x2="203.2" y2="96.52" width="0.1524" layer="91"/>
<junction x="203.2" y="96.52"/>
</segment>
</net>
<net name="N$134" class="0">
<segment>
<pinref part="R116" gate="G$1" pin="2"/>
<wire x1="198.12" y1="88.9" x2="203.2" y2="88.9" width="0.1524" layer="91"/>
<wire x1="203.2" y1="88.9" x2="203.2" y2="91.44" width="0.1524" layer="91"/>
<pinref part="U18" gate="G$1" pin="6"/>
<wire x1="203.2" y1="91.44" x2="210.82" y2="91.44" width="0.1524" layer="91"/>
<pinref part="R115" gate="G$1" pin="2"/>
<wire x1="198.12" y1="91.44" x2="203.2" y2="91.44" width="0.1524" layer="91"/>
<junction x="203.2" y="91.44"/>
</segment>
</net>
<net name="N$135" class="0">
<segment>
<pinref part="U18" gate="G$1" pin="1"/>
<pinref part="R117" gate="G$1" pin="1"/>
<wire x1="236.22" y1="101.6" x2="241.3" y2="101.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$136" class="0">
<segment>
<pinref part="U18" gate="G$1" pin="4"/>
<pinref part="R118" gate="G$1" pin="1"/>
<wire x1="236.22" y1="86.36" x2="241.3" y2="86.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$137" class="0">
<segment>
<pinref part="R119" gate="G$1" pin="2"/>
<wire x1="198.12" y1="45.72" x2="203.2" y2="45.72" width="0.1524" layer="91"/>
<wire x1="203.2" y1="45.72" x2="203.2" y2="43.18" width="0.1524" layer="91"/>
<pinref part="U19" gate="G$1" pin="7"/>
<wire x1="203.2" y1="43.18" x2="210.82" y2="43.18" width="0.1524" layer="91"/>
<pinref part="R120" gate="G$1" pin="2"/>
<wire x1="198.12" y1="43.18" x2="203.2" y2="43.18" width="0.1524" layer="91"/>
<junction x="203.2" y="43.18"/>
</segment>
</net>
<net name="N$138" class="0">
<segment>
<pinref part="R122" gate="G$1" pin="2"/>
<wire x1="198.12" y1="35.56" x2="203.2" y2="35.56" width="0.1524" layer="91"/>
<wire x1="203.2" y1="35.56" x2="203.2" y2="38.1" width="0.1524" layer="91"/>
<pinref part="U19" gate="G$1" pin="6"/>
<wire x1="203.2" y1="38.1" x2="210.82" y2="38.1" width="0.1524" layer="91"/>
<pinref part="R121" gate="G$1" pin="2"/>
<wire x1="198.12" y1="38.1" x2="203.2" y2="38.1" width="0.1524" layer="91"/>
<junction x="203.2" y="38.1"/>
</segment>
</net>
<net name="N$139" class="0">
<segment>
<pinref part="U19" gate="G$1" pin="1"/>
<pinref part="R123" gate="G$1" pin="1"/>
<wire x1="236.22" y1="48.26" x2="241.3" y2="48.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$140" class="0">
<segment>
<pinref part="U19" gate="G$1" pin="4"/>
<pinref part="R124" gate="G$1" pin="1"/>
<wire x1="236.22" y1="33.02" x2="241.3" y2="33.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="AISG_1_RS485_N" class="0">
<segment>
<pinref part="R101" gate="G$1" pin="1"/>
<wire x1="60.96" y1="99.06" x2="55.88" y2="99.06" width="0.1524" layer="91"/>
<pinref part="R104" gate="G$1" pin="1"/>
<wire x1="55.88" y1="99.06" x2="55.88" y2="88.9" width="0.1524" layer="91"/>
<wire x1="55.88" y1="88.9" x2="60.96" y2="88.9" width="0.1524" layer="91"/>
<junction x="55.88" y="88.9"/>
<wire x1="55.88" y1="88.9" x2="50.8" y2="88.9" width="0.1524" layer="91"/>
<pinref part="C70" gate="G$1" pin="2"/>
<wire x1="50.8" y1="88.9" x2="38.1" y2="88.9" width="0.1524" layer="91"/>
<wire x1="50.8" y1="86.36" x2="50.8" y2="88.9" width="0.1524" layer="91"/>
<junction x="50.8" y="88.9"/>
<label x="38.1" y="88.9" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="AISG_1_RS485_P" class="0">
<segment>
<pinref part="R102" gate="G$1" pin="1"/>
<wire x1="60.96" y1="96.52" x2="58.42" y2="96.52" width="0.1524" layer="91"/>
<wire x1="58.42" y1="96.52" x2="58.42" y2="91.44" width="0.1524" layer="91"/>
<pinref part="R103" gate="G$1" pin="1"/>
<wire x1="58.42" y1="91.44" x2="60.96" y2="91.44" width="0.1524" layer="91"/>
<junction x="58.42" y="91.44"/>
<wire x1="58.42" y1="91.44" x2="45.72" y2="91.44" width="0.1524" layer="91"/>
<pinref part="C69" gate="G$1" pin="2"/>
<wire x1="45.72" y1="91.44" x2="38.1" y2="91.44" width="0.1524" layer="91"/>
<wire x1="45.72" y1="86.36" x2="45.72" y2="91.44" width="0.1524" layer="91"/>
<junction x="45.72" y="91.44"/>
<label x="38.1" y="91.44" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="AISG_2_RS485_P" class="0">
<segment>
<pinref part="R108" gate="G$1" pin="1"/>
<wire x1="60.96" y1="43.18" x2="58.42" y2="43.18" width="0.1524" layer="91"/>
<wire x1="58.42" y1="43.18" x2="58.42" y2="38.1" width="0.1524" layer="91"/>
<pinref part="R109" gate="G$1" pin="1"/>
<wire x1="58.42" y1="38.1" x2="60.96" y2="38.1" width="0.1524" layer="91"/>
<junction x="58.42" y="38.1"/>
<wire x1="58.42" y1="38.1" x2="45.72" y2="38.1" width="0.1524" layer="91"/>
<pinref part="C72" gate="G$1" pin="2"/>
<wire x1="45.72" y1="38.1" x2="38.1" y2="38.1" width="0.1524" layer="91"/>
<wire x1="45.72" y1="33.02" x2="45.72" y2="38.1" width="0.1524" layer="91"/>
<junction x="45.72" y="38.1"/>
<label x="38.1" y="38.1" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="AISG_2_RS485_N" class="0">
<segment>
<pinref part="R107" gate="G$1" pin="1"/>
<wire x1="60.96" y1="45.72" x2="55.88" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R110" gate="G$1" pin="1"/>
<wire x1="55.88" y1="45.72" x2="55.88" y2="35.56" width="0.1524" layer="91"/>
<wire x1="55.88" y1="35.56" x2="60.96" y2="35.56" width="0.1524" layer="91"/>
<junction x="55.88" y="35.56"/>
<wire x1="55.88" y1="35.56" x2="50.8" y2="35.56" width="0.1524" layer="91"/>
<pinref part="C73" gate="G$1" pin="2"/>
<wire x1="50.8" y1="35.56" x2="38.1" y2="35.56" width="0.1524" layer="91"/>
<wire x1="50.8" y1="33.02" x2="50.8" y2="35.56" width="0.1524" layer="91"/>
<junction x="50.8" y="35.56"/>
<label x="38.1" y="35.56" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="AISG_3_RS485_P" class="0">
<segment>
<pinref part="R114" gate="G$1" pin="1"/>
<wire x1="187.96" y1="96.52" x2="185.42" y2="96.52" width="0.1524" layer="91"/>
<wire x1="185.42" y1="96.52" x2="185.42" y2="91.44" width="0.1524" layer="91"/>
<pinref part="R115" gate="G$1" pin="1"/>
<wire x1="185.42" y1="91.44" x2="187.96" y2="91.44" width="0.1524" layer="91"/>
<junction x="185.42" y="91.44"/>
<wire x1="185.42" y1="91.44" x2="172.72" y2="91.44" width="0.1524" layer="91"/>
<pinref part="C75" gate="G$1" pin="2"/>
<wire x1="172.72" y1="91.44" x2="165.1" y2="91.44" width="0.1524" layer="91"/>
<wire x1="172.72" y1="86.36" x2="172.72" y2="91.44" width="0.1524" layer="91"/>
<junction x="172.72" y="91.44"/>
<label x="165.1" y="91.44" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="AISG_3_RS485_N" class="0">
<segment>
<pinref part="R113" gate="G$1" pin="1"/>
<wire x1="187.96" y1="99.06" x2="182.88" y2="99.06" width="0.1524" layer="91"/>
<pinref part="R116" gate="G$1" pin="1"/>
<wire x1="182.88" y1="99.06" x2="182.88" y2="88.9" width="0.1524" layer="91"/>
<wire x1="182.88" y1="88.9" x2="187.96" y2="88.9" width="0.1524" layer="91"/>
<junction x="182.88" y="88.9"/>
<wire x1="182.88" y1="88.9" x2="177.8" y2="88.9" width="0.1524" layer="91"/>
<pinref part="C76" gate="G$1" pin="2"/>
<wire x1="177.8" y1="88.9" x2="165.1" y2="88.9" width="0.1524" layer="91"/>
<wire x1="177.8" y1="86.36" x2="177.8" y2="88.9" width="0.1524" layer="91"/>
<junction x="177.8" y="88.9"/>
<label x="165.1" y="88.9" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="COM0_SDA" class="0">
<segment>
<pinref part="R8" gate="G$1" pin="1"/>
<wire x1="50.8" y1="210.82" x2="50.8" y2="198.12" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="6"/>
<wire x1="91.44" y1="198.12" x2="50.8" y2="198.12" width="0.1524" layer="91"/>
<wire x1="50.8" y1="198.12" x2="40.64" y2="198.12" width="0.1524" layer="91"/>
<junction x="50.8" y="198.12"/>
<label x="40.64" y="198.12" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="COM0_SCL" class="0">
<segment>
<pinref part="R4" gate="G$1" pin="1"/>
<wire x1="66.04" y1="210.82" x2="66.04" y2="195.58" width="0.1524" layer="91"/>
<wire x1="40.64" y1="195.58" x2="66.04" y2="195.58" width="0.1524" layer="91"/>
<label x="40.64" y="195.58" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="U4" gate="G$1" pin="3"/>
<wire x1="91.44" y1="195.58" x2="66.04" y2="195.58" width="0.1524" layer="91"/>
<junction x="66.04" y="195.58"/>
</segment>
</net>
<net name="SDA1_5V" class="0">
<segment>
<pinref part="R6" gate="G$1" pin="1"/>
<wire x1="144.78" y1="198.12" x2="162.56" y2="198.12" width="0.1524" layer="91"/>
<wire x1="144.78" y1="210.82" x2="144.78" y2="198.12" width="0.1524" layer="91"/>
<label x="162.56" y="198.12" size="1.27" layer="95" xref="yes"/>
<pinref part="U4" gate="G$1" pin="7"/>
<wire x1="119.38" y1="198.12" x2="144.78" y2="198.12" width="0.1524" layer="91"/>
<junction x="144.78" y="198.12"/>
</segment>
</net>
<net name="SCL1_5V" class="0">
<segment>
<pinref part="R7" gate="G$1" pin="1"/>
<wire x1="152.4" y1="195.58" x2="162.56" y2="195.58" width="0.1524" layer="91"/>
<wire x1="152.4" y1="210.82" x2="152.4" y2="195.58" width="0.1524" layer="91"/>
<label x="162.56" y="195.58" size="1.27" layer="95" xref="yes"/>
<pinref part="U4" gate="G$1" pin="2"/>
<wire x1="119.38" y1="195.58" x2="152.4" y2="195.58" width="0.1524" layer="91"/>
<junction x="152.4" y="195.58"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
<text x="42.037" y="197.866" size="1.27" layer="97" rot="R90">0603</text>
<text x="47.371" y="174.498" size="1.27" layer="97" rot="R90">0603</text>
<text x="42.291" y="129.032" size="1.27" layer="97" rot="R90">0603</text>
<text x="47.117" y="105.664" size="1.27" layer="97" rot="R90">0603</text>
<text x="42.291" y="60.452" size="1.27" layer="97" rot="R90">0603</text>
<text x="47.371" y="37.084" size="1.27" layer="97" rot="R90">0603</text>
<text x="176.911" y="197.612" size="1.27" layer="97" rot="R90">0603</text>
<text x="181.991" y="174.752" size="1.27" layer="97" rot="R90">0603</text>
<text x="176.911" y="129.032" size="1.27" layer="97" rot="R90">0603</text>
<text x="181.991" y="105.918" size="1.27" layer="97" rot="R90">0603</text>
<text x="176.911" y="60.198" size="1.27" layer="97" rot="R90">0603</text>
<text x="181.991" y="37.084" size="1.27" layer="97" rot="R90">0603</text>
<text x="81.28" y="177.8" size="1.27" layer="97">I2C Address = 0x00</text>
<text x="215.9" y="177.8" size="1.27" layer="97">I2C Address = 0x00</text>
<text x="81.28" y="109.22" size="1.27" layer="97">I2C Address = 0x01</text>
<text x="215.9" y="109.22" size="1.27" layer="97">I2C Address = 0x01</text>
<text x="73.66" y="43.18" size="1.27" layer="97">I2C Address = Custom Address</text>
<text x="208.28" y="43.18" size="1.27" layer="97">I2C Address = Custom Address</text>
</plain>
<instances>
<instance part="U$2" gate="G$1" x="0" y="0" smashed="yes">
<attribute name="DRAWING_NAME" x="317.5" y="10.16" size="1.27" layer="94" ratio="15"/>
<attribute name="SHEET" x="332.486" y="5.842" size="1.27" layer="94" ratio="15"/>
</instance>
<instance part="U8" gate="G$1" x="78.74" y="231.14" smashed="yes">
<attribute name="NAME" x="78.74" y="231.14" size="1.27" layer="95"/>
<attribute name="VALUE" x="93.98" y="231.14" size="1.27" layer="96"/>
</instance>
<instance part="C45" gate="G$1" x="119.38" y="185.42" smashed="yes">
<attribute name="NAME" x="114.173" y="185.801" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="120.65" y="185.801" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="C48" gate="G$1" x="119.38" y="190.5" smashed="yes">
<attribute name="NAME" x="114.173" y="190.881" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="120.65" y="190.881" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="SUPPLY39" gate="GND" x="111.76" y="170.18" smashed="yes">
<attribute name="VALUE" x="109.855" y="167.005" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY37" gate="GND" x="68.58" y="170.18" smashed="yes">
<attribute name="VALUE" x="66.675" y="167.005" size="1.27" layer="96"/>
</instance>
<instance part="NC5" gate="G$1" x="71.12" y="193.04" smashed="yes"/>
<instance part="NC4" gate="G$1" x="71.12" y="208.28" smashed="yes"/>
<instance part="NC6" gate="G$1" x="71.12" y="185.42" smashed="yes"/>
<instance part="C43" gate="G$1" x="45.72" y="180.34" smashed="yes" rot="R270">
<attribute name="NAME" x="45.339" y="174.498" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="45.339" y="181.483" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="R44" gate="G$1" x="55.88" y="187.96" smashed="yes">
<attribute name="NAME" x="49.403" y="188.341" size="1.27" layer="95"/>
<attribute name="VALUE" x="58.42" y="188.468" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY36" gate="GND" x="45.72" y="170.18" smashed="yes">
<attribute name="VALUE" x="43.815" y="167.005" size="1.27" layer="96"/>
</instance>
<instance part="R37" gate="G$1" x="50.8" y="210.82" smashed="yes">
<attribute name="NAME" x="44.45" y="211.201" size="1.27" layer="95"/>
<attribute name="VALUE" x="53.467" y="211.328" size="1.27" layer="96"/>
</instance>
<instance part="C34" gate="G$1" x="40.64" y="203.2" smashed="yes" rot="R270">
<attribute name="NAME" x="40.005" y="197.866" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="40.259" y="204.47" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="SUPPLY34" gate="GND" x="40.64" y="193.04" smashed="yes">
<attribute name="VALUE" x="38.735" y="189.865" size="1.27" layer="96"/>
</instance>
<instance part="R38" gate="G$1" x="27.94" y="210.82" smashed="yes">
<attribute name="NAME" x="22.225" y="211.455" size="1.27" layer="95"/>
<attribute name="VALUE" x="31.623" y="211.328" size="1.27" layer="96"/>
</instance>
<instance part="R47" gate="G$1" x="27.94" y="187.96" smashed="yes">
<attribute name="NAME" x="21.844" y="188.3156" size="1.27" layer="95"/>
<attribute name="VALUE" x="30.48" y="188.468" size="1.27" layer="96"/>
</instance>
<instance part="R35" gate="G$1" x="27.94" y="205.74" smashed="yes">
<attribute name="NAME" x="22.225" y="206.375" size="1.27" layer="95"/>
<attribute name="VALUE" x="30.607" y="206.375" size="1.27" layer="96"/>
</instance>
<instance part="R46" gate="G$1" x="27.94" y="182.88" smashed="yes">
<attribute name="NAME" x="21.844" y="183.2356" size="1.27" layer="95"/>
<attribute name="VALUE" x="30.48" y="183.261" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY31" gate="GND" x="20.32" y="200.66" smashed="yes">
<attribute name="VALUE" x="18.415" y="197.485" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY32" gate="GND" x="20.32" y="177.8" smashed="yes">
<attribute name="VALUE" x="18.415" y="174.625" size="1.27" layer="96"/>
</instance>
<instance part="U3" gate="G$1" x="78.74" y="162.56" smashed="yes">
<attribute name="NAME" x="78.74" y="162.56" size="1.27" layer="95"/>
<attribute name="VALUE" x="93.98" y="162.56" size="1.27" layer="96"/>
</instance>
<instance part="C31" gate="G$1" x="119.38" y="116.84" smashed="yes">
<attribute name="NAME" x="113.665" y="117.221" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="120.65" y="117.221" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="C32" gate="G$1" x="119.38" y="121.92" smashed="yes">
<attribute name="NAME" x="113.665" y="122.301" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="120.65" y="122.301" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="SUPPLY15" gate="GND" x="111.76" y="101.6" smashed="yes">
<attribute name="VALUE" x="109.855" y="98.425" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY16" gate="GND" x="68.58" y="101.6" smashed="yes">
<attribute name="VALUE" x="66.675" y="98.425" size="1.27" layer="96"/>
</instance>
<instance part="NC1" gate="G$1" x="71.12" y="124.46" smashed="yes"/>
<instance part="NC2" gate="G$1" x="71.12" y="139.7" smashed="yes"/>
<instance part="NC3" gate="G$1" x="71.12" y="116.84" smashed="yes"/>
<instance part="C33" gate="G$1" x="45.72" y="111.76" smashed="yes" rot="R270">
<attribute name="NAME" x="45.339" y="105.664" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="45.339" y="112.903" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="R15" gate="G$1" x="55.88" y="119.38" smashed="yes">
<attribute name="NAME" x="49.403" y="119.761" size="1.27" layer="95"/>
<attribute name="VALUE" x="58.42" y="119.888" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY17" gate="GND" x="45.72" y="101.6" smashed="yes">
<attribute name="VALUE" x="43.815" y="98.425" size="1.27" layer="96"/>
</instance>
<instance part="R16" gate="G$1" x="50.8" y="142.24" smashed="yes">
<attribute name="NAME" x="44.45" y="142.621" size="1.27" layer="95"/>
<attribute name="VALUE" x="53.34" y="142.748" size="1.27" layer="96"/>
</instance>
<instance part="C35" gate="G$1" x="40.64" y="134.62" smashed="yes" rot="R270">
<attribute name="NAME" x="40.259" y="129.032" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="40.259" y="135.89" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="SUPPLY18" gate="GND" x="40.64" y="124.46" smashed="yes">
<attribute name="VALUE" x="38.735" y="121.285" size="1.27" layer="96"/>
</instance>
<instance part="R17" gate="G$1" x="27.94" y="142.24" smashed="yes">
<attribute name="NAME" x="22.225" y="142.875" size="1.27" layer="95"/>
<attribute name="VALUE" x="31.623" y="142.748" size="1.27" layer="96"/>
</instance>
<instance part="R18" gate="G$1" x="27.94" y="119.38" smashed="yes">
<attribute name="NAME" x="22.098" y="119.7356" size="1.27" layer="95"/>
<attribute name="VALUE" x="30.48" y="119.888" size="1.27" layer="96"/>
</instance>
<instance part="R19" gate="G$1" x="27.94" y="137.16" smashed="yes">
<attribute name="NAME" x="22.225" y="137.541" size="1.27" layer="95"/>
<attribute name="VALUE" x="30.607" y="137.541" size="1.27" layer="96"/>
</instance>
<instance part="R20" gate="G$1" x="27.94" y="114.3" smashed="yes">
<attribute name="NAME" x="21.844" y="114.6556" size="1.27" layer="95"/>
<attribute name="VALUE" x="30.48" y="114.681" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY19" gate="GND" x="20.32" y="132.08" smashed="yes">
<attribute name="VALUE" x="18.415" y="128.905" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY20" gate="GND" x="20.32" y="109.22" smashed="yes">
<attribute name="VALUE" x="18.415" y="106.045" size="1.27" layer="96"/>
</instance>
<instance part="U5" gate="G$1" x="78.74" y="93.98" smashed="yes">
<attribute name="NAME" x="78.74" y="93.98" size="1.27" layer="95"/>
<attribute name="VALUE" x="93.98" y="93.98" size="1.27" layer="96"/>
</instance>
<instance part="C36" gate="G$1" x="119.38" y="48.26" smashed="yes">
<attribute name="NAME" x="113.665" y="48.641" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="120.65" y="48.641" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="C37" gate="G$1" x="119.38" y="53.34" smashed="yes">
<attribute name="NAME" x="113.665" y="53.721" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="120.65" y="53.721" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="SUPPLY21" gate="GND" x="111.76" y="33.02" smashed="yes">
<attribute name="VALUE" x="109.855" y="29.845" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY22" gate="GND" x="68.58" y="33.02" smashed="yes">
<attribute name="VALUE" x="66.675" y="29.845" size="1.27" layer="96"/>
</instance>
<instance part="NC7" gate="G$1" x="71.12" y="55.88" smashed="yes"/>
<instance part="NC8" gate="G$1" x="71.12" y="71.12" smashed="yes"/>
<instance part="NC9" gate="G$1" x="71.12" y="48.26" smashed="yes"/>
<instance part="C38" gate="G$1" x="45.72" y="43.18" smashed="yes" rot="R270">
<attribute name="NAME" x="45.339" y="37.084" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="45.339" y="44.323" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="R21" gate="G$1" x="55.88" y="50.8" smashed="yes">
<attribute name="NAME" x="49.657" y="51.181" size="1.27" layer="95"/>
<attribute name="VALUE" x="58.547" y="51.308" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY23" gate="GND" x="45.72" y="33.02" smashed="yes">
<attribute name="VALUE" x="43.815" y="29.845" size="1.27" layer="96"/>
</instance>
<instance part="R22" gate="G$1" x="50.8" y="73.66" smashed="yes">
<attribute name="NAME" x="44.45" y="74.041" size="1.27" layer="95"/>
<attribute name="VALUE" x="53.467" y="74.168" size="1.27" layer="96"/>
</instance>
<instance part="C39" gate="G$1" x="40.64" y="66.04" smashed="yes" rot="R270">
<attribute name="NAME" x="40.259" y="60.452" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="40.259" y="67.31" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="SUPPLY24" gate="GND" x="40.64" y="55.88" smashed="yes">
<attribute name="VALUE" x="38.735" y="52.705" size="1.27" layer="96"/>
</instance>
<instance part="R23" gate="G$1" x="27.94" y="73.66" smashed="yes">
<attribute name="NAME" x="22.225" y="74.295" size="1.27" layer="95"/>
<attribute name="VALUE" x="30.48" y="74.168" size="1.27" layer="96"/>
</instance>
<instance part="R24" gate="G$1" x="27.94" y="50.8" smashed="yes">
<attribute name="NAME" x="21.844" y="51.1556" size="1.27" layer="95"/>
<attribute name="VALUE" x="30.48" y="51.308" size="1.27" layer="96"/>
</instance>
<instance part="R25" gate="G$1" x="27.94" y="68.58" smashed="yes">
<attribute name="NAME" x="21.717" y="68.961" size="1.27" layer="95"/>
<attribute name="VALUE" x="30.48" y="69.088" size="1.27" layer="96"/>
</instance>
<instance part="R26" gate="G$1" x="27.94" y="45.72" smashed="yes">
<attribute name="NAME" x="21.844" y="46.0756" size="1.27" layer="95"/>
<attribute name="VALUE" x="30.48" y="46.101" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY25" gate="GND" x="20.32" y="63.5" smashed="yes">
<attribute name="VALUE" x="18.415" y="60.325" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY26" gate="GND" x="20.32" y="40.64" smashed="yes">
<attribute name="VALUE" x="18.415" y="37.465" size="1.27" layer="96"/>
</instance>
<instance part="U6" gate="G$1" x="213.36" y="231.14" smashed="yes">
<attribute name="NAME" x="213.36" y="231.14" size="1.27" layer="95"/>
<attribute name="VALUE" x="228.6" y="231.14" size="1.27" layer="96"/>
</instance>
<instance part="C40" gate="G$1" x="254" y="185.42" smashed="yes">
<attribute name="NAME" x="248.793" y="185.801" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="255.27" y="185.801" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="C41" gate="G$1" x="254" y="190.5" smashed="yes">
<attribute name="NAME" x="248.793" y="190.881" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="255.27" y="190.881" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="SUPPLY27" gate="GND" x="246.38" y="170.18" smashed="yes">
<attribute name="VALUE" x="244.475" y="167.005" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY28" gate="GND" x="203.2" y="170.18" smashed="yes">
<attribute name="VALUE" x="201.295" y="167.005" size="1.27" layer="96"/>
</instance>
<instance part="NC10" gate="G$1" x="205.74" y="193.04" smashed="yes"/>
<instance part="NC11" gate="G$1" x="205.74" y="208.28" smashed="yes"/>
<instance part="NC12" gate="G$1" x="205.74" y="185.42" smashed="yes"/>
<instance part="C42" gate="G$1" x="180.34" y="180.34" smashed="yes" rot="R270">
<attribute name="NAME" x="179.959" y="174.752" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="179.959" y="181.483" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="R27" gate="G$1" x="190.5" y="187.96" smashed="yes">
<attribute name="NAME" x="184.658" y="188.468" size="1.27" layer="95"/>
<attribute name="VALUE" x="193.04" y="188.468" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY29" gate="GND" x="180.34" y="170.18" smashed="yes">
<attribute name="VALUE" x="178.435" y="167.005" size="1.27" layer="96"/>
</instance>
<instance part="R28" gate="G$1" x="185.42" y="210.82" smashed="yes">
<attribute name="NAME" x="179.07" y="211.201" size="1.27" layer="95"/>
<attribute name="VALUE" x="187.833" y="211.328" size="1.27" layer="96"/>
</instance>
<instance part="C44" gate="G$1" x="175.26" y="203.2" smashed="yes" rot="R270">
<attribute name="NAME" x="174.879" y="197.612" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="174.879" y="204.47" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="SUPPLY30" gate="GND" x="175.26" y="193.04" smashed="yes">
<attribute name="VALUE" x="173.355" y="189.865" size="1.27" layer="96"/>
</instance>
<instance part="R29" gate="G$1" x="162.56" y="210.82" smashed="yes">
<attribute name="NAME" x="156.591" y="211.201" size="1.27" layer="95"/>
<attribute name="VALUE" x="165.227" y="211.328" size="1.27" layer="96"/>
</instance>
<instance part="R30" gate="G$1" x="162.56" y="187.96" smashed="yes">
<attribute name="NAME" x="156.464" y="188.3156" size="1.27" layer="95"/>
<attribute name="VALUE" x="165.1" y="188.468" size="1.27" layer="96"/>
</instance>
<instance part="R31" gate="G$1" x="162.56" y="205.74" smashed="yes">
<attribute name="NAME" x="156.337" y="206.121" size="1.27" layer="95"/>
<attribute name="VALUE" x="165.227" y="206.121" size="1.27" layer="96"/>
</instance>
<instance part="R32" gate="G$1" x="162.56" y="182.88" smashed="yes">
<attribute name="NAME" x="156.464" y="183.2356" size="1.27" layer="95"/>
<attribute name="VALUE" x="165.1" y="183.261" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY33" gate="GND" x="154.94" y="200.66" smashed="yes">
<attribute name="VALUE" x="153.035" y="197.485" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY35" gate="GND" x="154.94" y="177.8" smashed="yes">
<attribute name="VALUE" x="153.035" y="174.625" size="1.27" layer="96"/>
</instance>
<instance part="U7" gate="G$1" x="213.36" y="162.56" smashed="yes">
<attribute name="NAME" x="213.36" y="162.56" size="1.27" layer="95"/>
<attribute name="VALUE" x="228.6" y="162.56" size="1.27" layer="96"/>
</instance>
<instance part="C46" gate="G$1" x="254" y="116.84" smashed="yes">
<attribute name="NAME" x="248.539" y="117.221" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="255.27" y="117.221" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="C47" gate="G$1" x="254" y="121.92" smashed="yes">
<attribute name="NAME" x="248.539" y="122.301" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="255.27" y="122.301" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="SUPPLY38" gate="GND" x="246.38" y="101.6" smashed="yes">
<attribute name="VALUE" x="244.475" y="98.425" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY40" gate="GND" x="203.2" y="101.6" smashed="yes">
<attribute name="VALUE" x="201.295" y="98.425" size="1.27" layer="96"/>
</instance>
<instance part="NC13" gate="G$1" x="205.74" y="124.46" smashed="yes"/>
<instance part="NC14" gate="G$1" x="205.74" y="139.7" smashed="yes"/>
<instance part="NC15" gate="G$1" x="205.74" y="116.84" smashed="yes"/>
<instance part="C49" gate="G$1" x="180.34" y="111.76" smashed="yes" rot="R270">
<attribute name="NAME" x="179.959" y="105.918" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="179.959" y="112.903" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="R33" gate="G$1" x="190.5" y="119.38" smashed="yes">
<attribute name="NAME" x="184.785" y="120.015" size="1.27" layer="95"/>
<attribute name="VALUE" x="193.675" y="119.888" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY41" gate="GND" x="180.34" y="101.6" smashed="yes">
<attribute name="VALUE" x="178.435" y="98.425" size="1.27" layer="96"/>
</instance>
<instance part="R34" gate="G$1" x="185.42" y="142.24" smashed="yes">
<attribute name="NAME" x="179.07" y="142.621" size="1.27" layer="95"/>
<attribute name="VALUE" x="188.087" y="142.748" size="1.27" layer="96"/>
</instance>
<instance part="C50" gate="G$1" x="175.26" y="134.62" smashed="yes" rot="R270">
<attribute name="NAME" x="174.879" y="129.032" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="174.879" y="135.89" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="SUPPLY42" gate="GND" x="175.26" y="124.46" smashed="yes">
<attribute name="VALUE" x="173.355" y="121.285" size="1.27" layer="96"/>
</instance>
<instance part="R36" gate="G$1" x="162.56" y="142.24" smashed="yes">
<attribute name="NAME" x="156.337" y="142.621" size="1.27" layer="95"/>
<attribute name="VALUE" x="165.227" y="142.748" size="1.27" layer="96"/>
</instance>
<instance part="R39" gate="G$1" x="162.56" y="119.38" smashed="yes">
<attribute name="NAME" x="156.21" y="119.7356" size="1.27" layer="95"/>
<attribute name="VALUE" x="165.1" y="119.888" size="1.27" layer="96"/>
</instance>
<instance part="R40" gate="G$1" x="162.56" y="137.16" smashed="yes">
<attribute name="NAME" x="156.083" y="137.541" size="1.27" layer="95"/>
<attribute name="VALUE" x="165.227" y="137.541" size="1.27" layer="96"/>
</instance>
<instance part="R41" gate="G$1" x="162.56" y="114.3" smashed="yes">
<attribute name="NAME" x="156.21" y="114.6556" size="1.27" layer="95"/>
<attribute name="VALUE" x="165.1" y="114.681" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY43" gate="GND" x="154.94" y="132.08" smashed="yes">
<attribute name="VALUE" x="153.035" y="128.905" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY44" gate="GND" x="154.94" y="109.22" smashed="yes">
<attribute name="VALUE" x="153.035" y="106.045" size="1.27" layer="96"/>
</instance>
<instance part="U9" gate="G$1" x="213.36" y="93.98" smashed="yes">
<attribute name="NAME" x="213.36" y="93.98" size="1.27" layer="95"/>
<attribute name="VALUE" x="228.6" y="93.98" size="1.27" layer="96"/>
</instance>
<instance part="C51" gate="G$1" x="254" y="48.26" smashed="yes">
<attribute name="NAME" x="248.285" y="48.641" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="255.27" y="48.641" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="C52" gate="G$1" x="254" y="53.34" smashed="yes">
<attribute name="NAME" x="248.285" y="53.721" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="255.27" y="53.721" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="SUPPLY45" gate="GND" x="246.38" y="33.02" smashed="yes">
<attribute name="VALUE" x="244.475" y="29.845" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY46" gate="GND" x="203.2" y="33.02" smashed="yes">
<attribute name="VALUE" x="201.295" y="29.845" size="1.27" layer="96"/>
</instance>
<instance part="NC16" gate="G$1" x="205.74" y="55.88" smashed="yes"/>
<instance part="NC17" gate="G$1" x="205.74" y="71.12" smashed="yes"/>
<instance part="NC18" gate="G$1" x="205.74" y="48.26" smashed="yes"/>
<instance part="C53" gate="G$1" x="180.34" y="43.18" smashed="yes" rot="R270">
<attribute name="NAME" x="179.959" y="37.084" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="179.959" y="44.323" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="R42" gate="G$1" x="190.5" y="50.8" smashed="yes">
<attribute name="NAME" x="184.785" y="51.435" size="1.27" layer="95"/>
<attribute name="VALUE" x="193.675" y="51.308" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY47" gate="GND" x="180.34" y="33.02" smashed="yes">
<attribute name="VALUE" x="178.435" y="29.845" size="1.27" layer="96"/>
</instance>
<instance part="R43" gate="G$1" x="185.42" y="73.66" smashed="yes">
<attribute name="NAME" x="179.07" y="74.041" size="1.27" layer="95"/>
<attribute name="VALUE" x="188.087" y="74.168" size="1.27" layer="96"/>
</instance>
<instance part="C54" gate="G$1" x="175.26" y="66.04" smashed="yes" rot="R270">
<attribute name="NAME" x="174.879" y="60.198" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="174.879" y="67.31" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="SUPPLY48" gate="GND" x="175.26" y="55.88" smashed="yes">
<attribute name="VALUE" x="173.355" y="52.705" size="1.27" layer="96"/>
</instance>
<instance part="R45" gate="G$1" x="162.56" y="73.66" smashed="yes">
<attribute name="NAME" x="156.337" y="74.041" size="1.27" layer="95"/>
<attribute name="VALUE" x="165.1" y="74.168" size="1.27" layer="96"/>
</instance>
<instance part="R48" gate="G$1" x="162.56" y="50.8" smashed="yes">
<attribute name="NAME" x="157.48" y="51.6636" size="1.27" layer="95"/>
<attribute name="VALUE" x="165.1" y="51.308" size="1.27" layer="96"/>
</instance>
<instance part="R49" gate="G$1" x="162.56" y="68.58" smashed="yes">
<attribute name="NAME" x="156.337" y="68.961" size="1.27" layer="95"/>
<attribute name="VALUE" x="165.1" y="69.088" size="1.27" layer="96"/>
</instance>
<instance part="R50" gate="G$1" x="162.56" y="45.72" smashed="yes">
<attribute name="NAME" x="157.48" y="46.5836" size="1.27" layer="95"/>
<attribute name="VALUE" x="165.1" y="46.355" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY49" gate="GND" x="154.94" y="63.5" smashed="yes">
<attribute name="VALUE" x="153.035" y="60.325" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY50" gate="GND" x="154.94" y="40.64" smashed="yes">
<attribute name="VALUE" x="153.035" y="37.465" size="1.27" layer="96"/>
</instance>
<instance part="C115" gate="G$1" x="63.5" y="111.76" smashed="yes" rot="R90">
<attribute name="NAME" x="63.119" y="106.299" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="63.119" y="113.03" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C116" gate="G$1" x="63.5" y="180.34" smashed="yes" rot="R90">
<attribute name="NAME" x="63.119" y="175.133" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="63.119" y="181.61" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C117" gate="G$1" x="63.5" y="43.18" smashed="yes" rot="R90">
<attribute name="NAME" x="63.119" y="37.719" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="63.119" y="44.45" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C118" gate="G$1" x="198.12" y="43.18" smashed="yes" rot="R90">
<attribute name="NAME" x="197.739" y="37.465" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="197.739" y="44.45" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C119" gate="G$1" x="198.12" y="111.76" smashed="yes" rot="R90">
<attribute name="NAME" x="197.485" y="106.299" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="197.739" y="113.03" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C120" gate="G$1" x="198.12" y="180.34" smashed="yes" rot="R90">
<attribute name="NAME" x="197.739" y="174.879" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="197.739" y="181.61" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="J5" gate="G$1" x="345.44" y="220.98" smashed="yes">
<attribute name="NAME" x="342.138" y="221.488" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J5" gate="G$2" x="345.44" y="218.44" smashed="yes">
<attribute name="NAME" x="342.138" y="218.948" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J5" gate="G$3" x="345.44" y="215.9" smashed="yes">
<attribute name="NAME" x="342.138" y="216.408" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J5" gate="G$4" x="345.44" y="213.36" smashed="yes">
<attribute name="NAME" x="342.138" y="213.868" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J5" gate="G$5" x="345.44" y="210.82" smashed="yes">
<attribute name="NAME" x="342.138" y="211.328" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J5" gate="G$6" x="345.44" y="208.28" smashed="yes">
<attribute name="NAME" x="342.138" y="208.788" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J5" gate="G$7" x="345.44" y="205.74" smashed="yes">
<attribute name="NAME" x="342.138" y="206.248" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J5" gate="G$8" x="345.44" y="203.2" smashed="yes">
<attribute name="NAME" x="342.138" y="203.708" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J6" gate="G$1" x="345.44" y="195.58" smashed="yes">
<attribute name="NAME" x="342.138" y="196.088" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J6" gate="G$2" x="345.44" y="193.04" smashed="yes">
<attribute name="NAME" x="342.138" y="193.548" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J6" gate="G$3" x="345.44" y="190.5" smashed="yes">
<attribute name="NAME" x="342.138" y="191.008" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J6" gate="G$4" x="345.44" y="187.96" smashed="yes">
<attribute name="NAME" x="342.138" y="188.468" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J6" gate="G$5" x="345.44" y="185.42" smashed="yes">
<attribute name="NAME" x="342.138" y="185.928" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J6" gate="G$6" x="345.44" y="182.88" smashed="yes">
<attribute name="NAME" x="342.138" y="183.388" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J6" gate="G$7" x="345.44" y="180.34" smashed="yes">
<attribute name="NAME" x="342.138" y="180.848" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J6" gate="G$8" x="345.44" y="177.8" smashed="yes">
<attribute name="NAME" x="342.138" y="178.308" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J7" gate="G$1" x="345.44" y="170.18" smashed="yes">
<attribute name="NAME" x="342.138" y="170.688" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J7" gate="G$2" x="345.44" y="167.64" smashed="yes">
<attribute name="NAME" x="342.138" y="168.148" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J7" gate="G$3" x="345.44" y="165.1" smashed="yes">
<attribute name="NAME" x="342.138" y="165.608" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J7" gate="G$4" x="345.44" y="162.56" smashed="yes">
<attribute name="NAME" x="342.138" y="163.068" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J7" gate="G$5" x="345.44" y="160.02" smashed="yes">
<attribute name="NAME" x="342.138" y="160.528" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J7" gate="G$6" x="345.44" y="157.48" smashed="yes">
<attribute name="NAME" x="342.138" y="157.988" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J7" gate="G$7" x="345.44" y="154.94" smashed="yes">
<attribute name="NAME" x="342.138" y="155.448" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J7" gate="G$8" x="345.44" y="152.4" smashed="yes">
<attribute name="NAME" x="342.138" y="152.908" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J8" gate="G$1" x="345.44" y="144.78" smashed="yes">
<attribute name="NAME" x="342.138" y="145.288" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J8" gate="G$2" x="345.44" y="142.24" smashed="yes">
<attribute name="NAME" x="342.138" y="142.748" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J8" gate="G$3" x="345.44" y="139.7" smashed="yes">
<attribute name="NAME" x="342.138" y="140.208" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J8" gate="G$4" x="345.44" y="137.16" smashed="yes">
<attribute name="NAME" x="342.138" y="137.668" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J8" gate="G$5" x="345.44" y="134.62" smashed="yes">
<attribute name="NAME" x="342.138" y="135.128" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J8" gate="G$6" x="345.44" y="132.08" smashed="yes">
<attribute name="NAME" x="342.138" y="132.588" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J8" gate="G$7" x="345.44" y="129.54" smashed="yes">
<attribute name="NAME" x="342.138" y="130.048" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J8" gate="G$8" x="345.44" y="127" smashed="yes">
<attribute name="NAME" x="342.138" y="127.508" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J9" gate="G$1" x="345.44" y="119.38" smashed="yes">
<attribute name="NAME" x="342.138" y="119.888" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J9" gate="G$2" x="345.44" y="116.84" smashed="yes">
<attribute name="NAME" x="342.138" y="117.348" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J9" gate="G$3" x="345.44" y="114.3" smashed="yes">
<attribute name="NAME" x="342.138" y="114.808" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J9" gate="G$4" x="345.44" y="111.76" smashed="yes">
<attribute name="NAME" x="342.138" y="112.268" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J9" gate="G$5" x="345.44" y="109.22" smashed="yes">
<attribute name="NAME" x="342.138" y="109.728" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J9" gate="G$6" x="345.44" y="106.68" smashed="yes">
<attribute name="NAME" x="342.138" y="107.188" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J9" gate="G$7" x="345.44" y="104.14" smashed="yes">
<attribute name="NAME" x="342.138" y="104.648" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J9" gate="G$8" x="345.44" y="101.6" smashed="yes">
<attribute name="NAME" x="342.138" y="102.108" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J10" gate="G$1" x="345.44" y="93.98" smashed="yes">
<attribute name="NAME" x="342.138" y="94.488" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J10" gate="G$2" x="345.44" y="91.44" smashed="yes">
<attribute name="NAME" x="342.138" y="91.948" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J10" gate="G$3" x="345.44" y="88.9" smashed="yes">
<attribute name="NAME" x="342.138" y="89.408" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J10" gate="G$4" x="345.44" y="86.36" smashed="yes">
<attribute name="NAME" x="342.138" y="86.868" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J10" gate="G$5" x="345.44" y="83.82" smashed="yes">
<attribute name="NAME" x="342.138" y="84.328" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J10" gate="G$6" x="345.44" y="81.28" smashed="yes">
<attribute name="NAME" x="342.138" y="81.788" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J10" gate="G$7" x="345.44" y="78.74" smashed="yes">
<attribute name="NAME" x="342.138" y="79.248" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J10" gate="G$8" x="345.44" y="76.2" smashed="yes">
<attribute name="NAME" x="342.138" y="76.708" size="1.27" layer="95" ratio="15"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="32"/>
<wire x1="109.22" y1="223.52" x2="111.76" y2="223.52" width="0.1524" layer="91"/>
<wire x1="111.76" y1="223.52" x2="111.76" y2="220.98" width="0.1524" layer="91"/>
<pinref part="U8" gate="G$1" pin="31"/>
<wire x1="111.76" y1="220.98" x2="111.76" y2="208.28" width="0.1524" layer="91"/>
<wire x1="111.76" y1="208.28" x2="111.76" y2="205.74" width="0.1524" layer="91"/>
<wire x1="111.76" y1="205.74" x2="111.76" y2="172.72" width="0.1524" layer="91"/>
<wire x1="109.22" y1="220.98" x2="111.76" y2="220.98" width="0.1524" layer="91"/>
<pinref part="U8" gate="G$1" pin="26"/>
<wire x1="109.22" y1="208.28" x2="111.76" y2="208.28" width="0.1524" layer="91"/>
<pinref part="U8" gate="G$1" pin="25"/>
<wire x1="109.22" y1="205.74" x2="111.76" y2="205.74" width="0.1524" layer="91"/>
<junction x="111.76" y="220.98"/>
<junction x="111.76" y="208.28"/>
<junction x="111.76" y="205.74"/>
<pinref part="SUPPLY39" gate="GND" pin="GND"/>
<pinref part="U8" gate="G$1" pin="33"/>
<wire x1="109.22" y1="226.06" x2="111.76" y2="226.06" width="0.1524" layer="91"/>
<wire x1="111.76" y1="226.06" x2="111.76" y2="223.52" width="0.1524" layer="91"/>
<junction x="111.76" y="223.52"/>
</segment>
<segment>
<pinref part="U8" gate="G$1" pin="11"/>
<wire x1="73.66" y1="198.12" x2="68.58" y2="198.12" width="0.1524" layer="91"/>
<wire x1="68.58" y1="198.12" x2="68.58" y2="195.58" width="0.1524" layer="91"/>
<pinref part="SUPPLY37" gate="GND" pin="GND"/>
<pinref part="U8" gate="G$1" pin="12"/>
<wire x1="68.58" y1="195.58" x2="68.58" y2="190.5" width="0.1524" layer="91"/>
<wire x1="68.58" y1="190.5" x2="68.58" y2="175.26" width="0.1524" layer="91"/>
<wire x1="68.58" y1="175.26" x2="68.58" y2="172.72" width="0.1524" layer="91"/>
<wire x1="73.66" y1="195.58" x2="68.58" y2="195.58" width="0.1524" layer="91"/>
<junction x="68.58" y="195.58"/>
<pinref part="U8" gate="G$1" pin="14"/>
<wire x1="73.66" y1="190.5" x2="68.58" y2="190.5" width="0.1524" layer="91"/>
<junction x="68.58" y="190.5"/>
<pinref part="C116" gate="G$1" pin="1"/>
<wire x1="63.5" y1="175.26" x2="68.58" y2="175.26" width="0.1524" layer="91"/>
<junction x="68.58" y="175.26"/>
</segment>
<segment>
<pinref part="SUPPLY36" gate="GND" pin="GND"/>
<pinref part="C43" gate="G$1" pin="2"/>
<wire x1="45.72" y1="172.72" x2="45.72" y2="175.26" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C34" gate="G$1" pin="2"/>
<pinref part="SUPPLY34" gate="GND" pin="GND"/>
<wire x1="40.64" y1="198.12" x2="40.64" y2="195.58" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R35" gate="G$1" pin="1"/>
<pinref part="SUPPLY31" gate="GND" pin="GND"/>
<wire x1="22.86" y1="205.74" x2="20.32" y2="205.74" width="0.1524" layer="91"/>
<wire x1="20.32" y1="205.74" x2="20.32" y2="203.2" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R46" gate="G$1" pin="1"/>
<pinref part="SUPPLY32" gate="GND" pin="GND"/>
<wire x1="22.86" y1="182.88" x2="20.32" y2="182.88" width="0.1524" layer="91"/>
<wire x1="20.32" y1="182.88" x2="20.32" y2="180.34" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="32"/>
<wire x1="109.22" y1="154.94" x2="111.76" y2="154.94" width="0.1524" layer="91"/>
<wire x1="111.76" y1="154.94" x2="111.76" y2="152.4" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="31"/>
<wire x1="111.76" y1="152.4" x2="111.76" y2="139.7" width="0.1524" layer="91"/>
<wire x1="111.76" y1="139.7" x2="111.76" y2="137.16" width="0.1524" layer="91"/>
<wire x1="111.76" y1="137.16" x2="111.76" y2="104.14" width="0.1524" layer="91"/>
<wire x1="109.22" y1="152.4" x2="111.76" y2="152.4" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="26"/>
<wire x1="109.22" y1="139.7" x2="111.76" y2="139.7" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="25"/>
<wire x1="109.22" y1="137.16" x2="111.76" y2="137.16" width="0.1524" layer="91"/>
<junction x="111.76" y="152.4"/>
<junction x="111.76" y="139.7"/>
<junction x="111.76" y="137.16"/>
<pinref part="SUPPLY15" gate="GND" pin="GND"/>
<pinref part="U3" gate="G$1" pin="33"/>
<wire x1="109.22" y1="157.48" x2="111.76" y2="157.48" width="0.1524" layer="91"/>
<wire x1="111.76" y1="157.48" x2="111.76" y2="154.94" width="0.1524" layer="91"/>
<junction x="111.76" y="154.94"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="11"/>
<wire x1="73.66" y1="129.54" x2="68.58" y2="129.54" width="0.1524" layer="91"/>
<wire x1="68.58" y1="129.54" x2="68.58" y2="127" width="0.1524" layer="91"/>
<pinref part="SUPPLY16" gate="GND" pin="GND"/>
<pinref part="U3" gate="G$1" pin="12"/>
<wire x1="68.58" y1="127" x2="68.58" y2="121.92" width="0.1524" layer="91"/>
<wire x1="68.58" y1="121.92" x2="68.58" y2="106.68" width="0.1524" layer="91"/>
<wire x1="68.58" y1="106.68" x2="68.58" y2="104.14" width="0.1524" layer="91"/>
<wire x1="73.66" y1="127" x2="68.58" y2="127" width="0.1524" layer="91"/>
<junction x="68.58" y="127"/>
<pinref part="U3" gate="G$1" pin="14"/>
<wire x1="73.66" y1="121.92" x2="68.58" y2="121.92" width="0.1524" layer="91"/>
<junction x="68.58" y="121.92"/>
<pinref part="C115" gate="G$1" pin="1"/>
<wire x1="63.5" y1="106.68" x2="68.58" y2="106.68" width="0.1524" layer="91"/>
<junction x="68.58" y="106.68"/>
</segment>
<segment>
<pinref part="SUPPLY17" gate="GND" pin="GND"/>
<pinref part="C33" gate="G$1" pin="2"/>
<wire x1="45.72" y1="104.14" x2="45.72" y2="106.68" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C35" gate="G$1" pin="2"/>
<pinref part="SUPPLY18" gate="GND" pin="GND"/>
<wire x1="40.64" y1="129.54" x2="40.64" y2="127" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R19" gate="G$1" pin="1"/>
<pinref part="SUPPLY19" gate="GND" pin="GND"/>
<wire x1="22.86" y1="137.16" x2="20.32" y2="137.16" width="0.1524" layer="91"/>
<wire x1="20.32" y1="137.16" x2="20.32" y2="134.62" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R20" gate="G$1" pin="1"/>
<pinref part="SUPPLY20" gate="GND" pin="GND"/>
<wire x1="22.86" y1="114.3" x2="20.32" y2="114.3" width="0.1524" layer="91"/>
<wire x1="20.32" y1="114.3" x2="20.32" y2="111.76" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="32"/>
<wire x1="109.22" y1="86.36" x2="111.76" y2="86.36" width="0.1524" layer="91"/>
<wire x1="111.76" y1="86.36" x2="111.76" y2="83.82" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="31"/>
<wire x1="111.76" y1="83.82" x2="111.76" y2="71.12" width="0.1524" layer="91"/>
<wire x1="111.76" y1="71.12" x2="111.76" y2="68.58" width="0.1524" layer="91"/>
<wire x1="111.76" y1="68.58" x2="111.76" y2="35.56" width="0.1524" layer="91"/>
<wire x1="109.22" y1="83.82" x2="111.76" y2="83.82" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="26"/>
<wire x1="109.22" y1="71.12" x2="111.76" y2="71.12" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="25"/>
<wire x1="109.22" y1="68.58" x2="111.76" y2="68.58" width="0.1524" layer="91"/>
<junction x="111.76" y="83.82"/>
<junction x="111.76" y="71.12"/>
<junction x="111.76" y="68.58"/>
<pinref part="SUPPLY21" gate="GND" pin="GND"/>
<pinref part="U5" gate="G$1" pin="33"/>
<wire x1="109.22" y1="88.9" x2="111.76" y2="88.9" width="0.1524" layer="91"/>
<wire x1="111.76" y1="88.9" x2="111.76" y2="86.36" width="0.1524" layer="91"/>
<junction x="111.76" y="86.36"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="11"/>
<wire x1="73.66" y1="60.96" x2="68.58" y2="60.96" width="0.1524" layer="91"/>
<wire x1="68.58" y1="60.96" x2="68.58" y2="58.42" width="0.1524" layer="91"/>
<pinref part="SUPPLY22" gate="GND" pin="GND"/>
<pinref part="U5" gate="G$1" pin="12"/>
<wire x1="68.58" y1="58.42" x2="68.58" y2="53.34" width="0.1524" layer="91"/>
<wire x1="68.58" y1="53.34" x2="68.58" y2="38.1" width="0.1524" layer="91"/>
<wire x1="68.58" y1="38.1" x2="68.58" y2="35.56" width="0.1524" layer="91"/>
<wire x1="73.66" y1="58.42" x2="68.58" y2="58.42" width="0.1524" layer="91"/>
<junction x="68.58" y="58.42"/>
<pinref part="U5" gate="G$1" pin="14"/>
<wire x1="73.66" y1="53.34" x2="68.58" y2="53.34" width="0.1524" layer="91"/>
<junction x="68.58" y="53.34"/>
<pinref part="C117" gate="G$1" pin="1"/>
<wire x1="63.5" y1="38.1" x2="68.58" y2="38.1" width="0.1524" layer="91"/>
<junction x="68.58" y="38.1"/>
</segment>
<segment>
<pinref part="SUPPLY23" gate="GND" pin="GND"/>
<pinref part="C38" gate="G$1" pin="2"/>
<wire x1="45.72" y1="35.56" x2="45.72" y2="38.1" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C39" gate="G$1" pin="2"/>
<pinref part="SUPPLY24" gate="GND" pin="GND"/>
<wire x1="40.64" y1="60.96" x2="40.64" y2="58.42" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R25" gate="G$1" pin="1"/>
<pinref part="SUPPLY25" gate="GND" pin="GND"/>
<wire x1="22.86" y1="68.58" x2="20.32" y2="68.58" width="0.1524" layer="91"/>
<wire x1="20.32" y1="68.58" x2="20.32" y2="66.04" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R26" gate="G$1" pin="1"/>
<pinref part="SUPPLY26" gate="GND" pin="GND"/>
<wire x1="22.86" y1="45.72" x2="20.32" y2="45.72" width="0.1524" layer="91"/>
<wire x1="20.32" y1="45.72" x2="20.32" y2="43.18" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U6" gate="G$1" pin="32"/>
<wire x1="243.84" y1="223.52" x2="246.38" y2="223.52" width="0.1524" layer="91"/>
<wire x1="246.38" y1="223.52" x2="246.38" y2="220.98" width="0.1524" layer="91"/>
<pinref part="U6" gate="G$1" pin="31"/>
<wire x1="246.38" y1="220.98" x2="246.38" y2="208.28" width="0.1524" layer="91"/>
<wire x1="246.38" y1="208.28" x2="246.38" y2="205.74" width="0.1524" layer="91"/>
<wire x1="246.38" y1="205.74" x2="246.38" y2="172.72" width="0.1524" layer="91"/>
<wire x1="243.84" y1="220.98" x2="246.38" y2="220.98" width="0.1524" layer="91"/>
<pinref part="U6" gate="G$1" pin="26"/>
<wire x1="243.84" y1="208.28" x2="246.38" y2="208.28" width="0.1524" layer="91"/>
<pinref part="U6" gate="G$1" pin="25"/>
<wire x1="243.84" y1="205.74" x2="246.38" y2="205.74" width="0.1524" layer="91"/>
<junction x="246.38" y="220.98"/>
<junction x="246.38" y="208.28"/>
<junction x="246.38" y="205.74"/>
<pinref part="SUPPLY27" gate="GND" pin="GND"/>
<pinref part="U6" gate="G$1" pin="33"/>
<wire x1="243.84" y1="226.06" x2="246.38" y2="226.06" width="0.1524" layer="91"/>
<wire x1="246.38" y1="226.06" x2="246.38" y2="223.52" width="0.1524" layer="91"/>
<junction x="246.38" y="223.52"/>
</segment>
<segment>
<pinref part="U6" gate="G$1" pin="11"/>
<wire x1="208.28" y1="198.12" x2="203.2" y2="198.12" width="0.1524" layer="91"/>
<wire x1="203.2" y1="198.12" x2="203.2" y2="195.58" width="0.1524" layer="91"/>
<pinref part="SUPPLY28" gate="GND" pin="GND"/>
<pinref part="U6" gate="G$1" pin="12"/>
<wire x1="203.2" y1="195.58" x2="203.2" y2="190.5" width="0.1524" layer="91"/>
<wire x1="203.2" y1="190.5" x2="203.2" y2="175.26" width="0.1524" layer="91"/>
<wire x1="203.2" y1="175.26" x2="203.2" y2="172.72" width="0.1524" layer="91"/>
<wire x1="208.28" y1="195.58" x2="203.2" y2="195.58" width="0.1524" layer="91"/>
<junction x="203.2" y="195.58"/>
<pinref part="U6" gate="G$1" pin="14"/>
<wire x1="208.28" y1="190.5" x2="203.2" y2="190.5" width="0.1524" layer="91"/>
<junction x="203.2" y="190.5"/>
<pinref part="C120" gate="G$1" pin="1"/>
<wire x1="198.12" y1="175.26" x2="203.2" y2="175.26" width="0.1524" layer="91"/>
<junction x="203.2" y="175.26"/>
</segment>
<segment>
<pinref part="SUPPLY29" gate="GND" pin="GND"/>
<pinref part="C42" gate="G$1" pin="2"/>
<wire x1="180.34" y1="172.72" x2="180.34" y2="175.26" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C44" gate="G$1" pin="2"/>
<pinref part="SUPPLY30" gate="GND" pin="GND"/>
<wire x1="175.26" y1="198.12" x2="175.26" y2="195.58" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R31" gate="G$1" pin="1"/>
<pinref part="SUPPLY33" gate="GND" pin="GND"/>
<wire x1="157.48" y1="205.74" x2="154.94" y2="205.74" width="0.1524" layer="91"/>
<wire x1="154.94" y1="205.74" x2="154.94" y2="203.2" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R32" gate="G$1" pin="1"/>
<pinref part="SUPPLY35" gate="GND" pin="GND"/>
<wire x1="157.48" y1="182.88" x2="154.94" y2="182.88" width="0.1524" layer="91"/>
<wire x1="154.94" y1="182.88" x2="154.94" y2="180.34" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U7" gate="G$1" pin="32"/>
<wire x1="243.84" y1="154.94" x2="246.38" y2="154.94" width="0.1524" layer="91"/>
<wire x1="246.38" y1="154.94" x2="246.38" y2="152.4" width="0.1524" layer="91"/>
<pinref part="U7" gate="G$1" pin="31"/>
<wire x1="246.38" y1="152.4" x2="246.38" y2="139.7" width="0.1524" layer="91"/>
<wire x1="246.38" y1="139.7" x2="246.38" y2="137.16" width="0.1524" layer="91"/>
<wire x1="246.38" y1="137.16" x2="246.38" y2="104.14" width="0.1524" layer="91"/>
<wire x1="243.84" y1="152.4" x2="246.38" y2="152.4" width="0.1524" layer="91"/>
<pinref part="U7" gate="G$1" pin="26"/>
<wire x1="243.84" y1="139.7" x2="246.38" y2="139.7" width="0.1524" layer="91"/>
<pinref part="U7" gate="G$1" pin="25"/>
<wire x1="243.84" y1="137.16" x2="246.38" y2="137.16" width="0.1524" layer="91"/>
<junction x="246.38" y="152.4"/>
<junction x="246.38" y="139.7"/>
<junction x="246.38" y="137.16"/>
<pinref part="SUPPLY38" gate="GND" pin="GND"/>
<pinref part="U7" gate="G$1" pin="33"/>
<wire x1="243.84" y1="157.48" x2="246.38" y2="157.48" width="0.1524" layer="91"/>
<wire x1="246.38" y1="157.48" x2="246.38" y2="154.94" width="0.1524" layer="91"/>
<junction x="246.38" y="154.94"/>
</segment>
<segment>
<pinref part="U7" gate="G$1" pin="11"/>
<wire x1="208.28" y1="129.54" x2="203.2" y2="129.54" width="0.1524" layer="91"/>
<wire x1="203.2" y1="129.54" x2="203.2" y2="127" width="0.1524" layer="91"/>
<pinref part="SUPPLY40" gate="GND" pin="GND"/>
<pinref part="U7" gate="G$1" pin="12"/>
<wire x1="203.2" y1="127" x2="203.2" y2="121.92" width="0.1524" layer="91"/>
<wire x1="203.2" y1="121.92" x2="203.2" y2="106.68" width="0.1524" layer="91"/>
<wire x1="203.2" y1="106.68" x2="203.2" y2="104.14" width="0.1524" layer="91"/>
<wire x1="208.28" y1="127" x2="203.2" y2="127" width="0.1524" layer="91"/>
<junction x="203.2" y="127"/>
<pinref part="U7" gate="G$1" pin="14"/>
<wire x1="208.28" y1="121.92" x2="203.2" y2="121.92" width="0.1524" layer="91"/>
<junction x="203.2" y="121.92"/>
<pinref part="C119" gate="G$1" pin="1"/>
<wire x1="198.12" y1="106.68" x2="203.2" y2="106.68" width="0.1524" layer="91"/>
<junction x="203.2" y="106.68"/>
</segment>
<segment>
<pinref part="SUPPLY41" gate="GND" pin="GND"/>
<pinref part="C49" gate="G$1" pin="2"/>
<wire x1="180.34" y1="104.14" x2="180.34" y2="106.68" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C50" gate="G$1" pin="2"/>
<pinref part="SUPPLY42" gate="GND" pin="GND"/>
<wire x1="175.26" y1="129.54" x2="175.26" y2="127" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R40" gate="G$1" pin="1"/>
<pinref part="SUPPLY43" gate="GND" pin="GND"/>
<wire x1="157.48" y1="137.16" x2="154.94" y2="137.16" width="0.1524" layer="91"/>
<wire x1="154.94" y1="137.16" x2="154.94" y2="134.62" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R41" gate="G$1" pin="1"/>
<pinref part="SUPPLY44" gate="GND" pin="GND"/>
<wire x1="157.48" y1="114.3" x2="154.94" y2="114.3" width="0.1524" layer="91"/>
<wire x1="154.94" y1="114.3" x2="154.94" y2="111.76" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U9" gate="G$1" pin="32"/>
<wire x1="243.84" y1="86.36" x2="246.38" y2="86.36" width="0.1524" layer="91"/>
<wire x1="246.38" y1="86.36" x2="246.38" y2="83.82" width="0.1524" layer="91"/>
<pinref part="U9" gate="G$1" pin="31"/>
<wire x1="246.38" y1="83.82" x2="246.38" y2="71.12" width="0.1524" layer="91"/>
<wire x1="246.38" y1="71.12" x2="246.38" y2="68.58" width="0.1524" layer="91"/>
<wire x1="246.38" y1="68.58" x2="246.38" y2="35.56" width="0.1524" layer="91"/>
<wire x1="243.84" y1="83.82" x2="246.38" y2="83.82" width="0.1524" layer="91"/>
<pinref part="U9" gate="G$1" pin="26"/>
<wire x1="243.84" y1="71.12" x2="246.38" y2="71.12" width="0.1524" layer="91"/>
<pinref part="U9" gate="G$1" pin="25"/>
<wire x1="243.84" y1="68.58" x2="246.38" y2="68.58" width="0.1524" layer="91"/>
<junction x="246.38" y="83.82"/>
<junction x="246.38" y="71.12"/>
<junction x="246.38" y="68.58"/>
<pinref part="SUPPLY45" gate="GND" pin="GND"/>
<pinref part="U9" gate="G$1" pin="33"/>
<wire x1="243.84" y1="88.9" x2="246.38" y2="88.9" width="0.1524" layer="91"/>
<wire x1="246.38" y1="88.9" x2="246.38" y2="86.36" width="0.1524" layer="91"/>
<junction x="246.38" y="86.36"/>
</segment>
<segment>
<pinref part="U9" gate="G$1" pin="11"/>
<wire x1="208.28" y1="60.96" x2="203.2" y2="60.96" width="0.1524" layer="91"/>
<wire x1="203.2" y1="60.96" x2="203.2" y2="58.42" width="0.1524" layer="91"/>
<pinref part="SUPPLY46" gate="GND" pin="GND"/>
<pinref part="U9" gate="G$1" pin="12"/>
<wire x1="203.2" y1="58.42" x2="203.2" y2="53.34" width="0.1524" layer="91"/>
<wire x1="203.2" y1="53.34" x2="203.2" y2="38.1" width="0.1524" layer="91"/>
<wire x1="203.2" y1="38.1" x2="203.2" y2="35.56" width="0.1524" layer="91"/>
<wire x1="208.28" y1="58.42" x2="203.2" y2="58.42" width="0.1524" layer="91"/>
<junction x="203.2" y="58.42"/>
<pinref part="U9" gate="G$1" pin="14"/>
<wire x1="208.28" y1="53.34" x2="203.2" y2="53.34" width="0.1524" layer="91"/>
<junction x="203.2" y="53.34"/>
<pinref part="C118" gate="G$1" pin="1"/>
<wire x1="198.12" y1="38.1" x2="203.2" y2="38.1" width="0.1524" layer="91"/>
<junction x="203.2" y="38.1"/>
</segment>
<segment>
<pinref part="SUPPLY47" gate="GND" pin="GND"/>
<pinref part="C53" gate="G$1" pin="2"/>
<wire x1="180.34" y1="35.56" x2="180.34" y2="38.1" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C54" gate="G$1" pin="2"/>
<pinref part="SUPPLY48" gate="GND" pin="GND"/>
<wire x1="175.26" y1="60.96" x2="175.26" y2="58.42" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R49" gate="G$1" pin="1"/>
<pinref part="SUPPLY49" gate="GND" pin="GND"/>
<wire x1="157.48" y1="68.58" x2="154.94" y2="68.58" width="0.1524" layer="91"/>
<wire x1="154.94" y1="68.58" x2="154.94" y2="66.04" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R50" gate="G$1" pin="1"/>
<pinref part="SUPPLY50" gate="GND" pin="GND"/>
<wire x1="157.48" y1="45.72" x2="154.94" y2="45.72" width="0.1524" layer="91"/>
<wire x1="154.94" y1="45.72" x2="154.94" y2="43.18" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J5" gate="G$6" pin="1"/>
<wire x1="340.36" y1="208.28" x2="299.72" y2="208.28" width="0.1524" layer="91"/>
<junction x="299.72" y="208.28"/>
<label x="302.26" y="208.28" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="J6" gate="G$6" pin="1"/>
<wire x1="340.36" y1="182.88" x2="299.72" y2="182.88" width="0.1524" layer="91"/>
<junction x="299.72" y="182.88"/>
<label x="302.26" y="182.88" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="J7" gate="G$6" pin="1"/>
<wire x1="340.36" y1="157.48" x2="299.72" y2="157.48" width="0.1524" layer="91"/>
<junction x="299.72" y="157.48"/>
<label x="302.26" y="157.48" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="J8" gate="G$6" pin="1"/>
<wire x1="340.36" y1="132.08" x2="299.72" y2="132.08" width="0.1524" layer="91"/>
<junction x="299.72" y="132.08"/>
<label x="302.26" y="132.08" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="J9" gate="G$6" pin="1"/>
<wire x1="340.36" y1="106.68" x2="299.72" y2="106.68" width="0.1524" layer="91"/>
<junction x="299.72" y="106.68"/>
<label x="302.26" y="106.68" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="J10" gate="G$6" pin="1"/>
<wire x1="340.36" y1="81.28" x2="299.72" y2="81.28" width="0.1524" layer="91"/>
<junction x="299.72" y="81.28"/>
<label x="302.26" y="81.28" size="1.27" layer="95"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="17"/>
<pinref part="C45" gate="G$1" pin="1"/>
<wire x1="109.22" y1="185.42" x2="114.3" y2="185.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="18"/>
<wire x1="109.22" y1="187.96" x2="127" y2="187.96" width="0.1524" layer="91"/>
<wire x1="127" y1="187.96" x2="127" y2="185.42" width="0.1524" layer="91"/>
<pinref part="C45" gate="G$1" pin="2"/>
<wire x1="127" y1="185.42" x2="124.46" y2="185.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="19"/>
<pinref part="C48" gate="G$1" pin="1"/>
<wire x1="109.22" y1="190.5" x2="114.3" y2="190.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="13"/>
<wire x1="73.66" y1="193.04" x2="71.12" y2="193.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="7"/>
<wire x1="73.66" y1="208.28" x2="71.12" y2="208.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SDA1_5V" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="8"/>
<wire x1="73.66" y1="205.74" x2="58.42" y2="205.74" width="0.1524" layer="91"/>
<label x="58.42" y="205.74" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="8"/>
<wire x1="73.66" y1="137.16" x2="58.42" y2="137.16" width="0.1524" layer="91"/>
<label x="58.42" y="137.16" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="8"/>
<wire x1="73.66" y1="68.58" x2="58.42" y2="68.58" width="0.1524" layer="91"/>
<label x="58.42" y="68.58" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="10"/>
<wire x1="73.66" y1="200.66" x2="63.5" y2="200.66" width="0.1524" layer="91"/>
<label x="58.42" y="200.66" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="C116" gate="G$1" pin="2"/>
<wire x1="63.5" y1="200.66" x2="58.42" y2="200.66" width="0.1524" layer="91"/>
<wire x1="63.5" y1="185.42" x2="63.5" y2="200.66" width="0.1524" layer="91"/>
<junction x="63.5" y="200.66"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="10"/>
<wire x1="73.66" y1="132.08" x2="63.5" y2="132.08" width="0.1524" layer="91"/>
<label x="58.42" y="132.08" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="C115" gate="G$1" pin="2"/>
<wire x1="63.5" y1="132.08" x2="58.42" y2="132.08" width="0.1524" layer="91"/>
<wire x1="63.5" y1="116.84" x2="63.5" y2="132.08" width="0.1524" layer="91"/>
<junction x="63.5" y="132.08"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="10"/>
<wire x1="73.66" y1="63.5" x2="63.5" y2="63.5" width="0.1524" layer="91"/>
<label x="58.42" y="63.5" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="C117" gate="G$1" pin="2"/>
<wire x1="63.5" y1="63.5" x2="58.42" y2="63.5" width="0.1524" layer="91"/>
<wire x1="63.5" y1="48.26" x2="63.5" y2="63.5" width="0.1524" layer="91"/>
<junction x="63.5" y="63.5"/>
</segment>
<segment>
<pinref part="U6" gate="G$1" pin="10"/>
<wire x1="208.28" y1="200.66" x2="198.12" y2="200.66" width="0.1524" layer="91"/>
<label x="193.04" y="200.66" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="C120" gate="G$1" pin="2"/>
<wire x1="198.12" y1="200.66" x2="193.04" y2="200.66" width="0.1524" layer="91"/>
<wire x1="198.12" y1="185.42" x2="198.12" y2="200.66" width="0.1524" layer="91"/>
<junction x="198.12" y="200.66"/>
</segment>
<segment>
<pinref part="U7" gate="G$1" pin="10"/>
<wire x1="208.28" y1="132.08" x2="198.12" y2="132.08" width="0.1524" layer="91"/>
<label x="195.58" y="132.08" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="C119" gate="G$1" pin="2"/>
<wire x1="198.12" y1="132.08" x2="195.58" y2="132.08" width="0.1524" layer="91"/>
<wire x1="198.12" y1="116.84" x2="198.12" y2="132.08" width="0.1524" layer="91"/>
<junction x="198.12" y="132.08"/>
</segment>
<segment>
<pinref part="U9" gate="G$1" pin="10"/>
<wire x1="208.28" y1="63.5" x2="198.12" y2="63.5" width="0.1524" layer="91"/>
<label x="193.04" y="63.5" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="C118" gate="G$1" pin="2"/>
<wire x1="198.12" y1="63.5" x2="193.04" y2="63.5" width="0.1524" layer="91"/>
<wire x1="198.12" y1="48.26" x2="198.12" y2="63.5" width="0.1524" layer="91"/>
<junction x="198.12" y="63.5"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="15"/>
<pinref part="R44" gate="G$1" pin="2"/>
<wire x1="73.66" y1="187.96" x2="60.96" y2="187.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="6"/>
<wire x1="73.66" y1="210.82" x2="55.88" y2="210.82" width="0.1524" layer="91"/>
<pinref part="R37" gate="G$1" pin="2"/>
</segment>
</net>
<net name="HB1_MOTXP" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="2"/>
<wire x1="208.28" y1="220.98" x2="205.74" y2="220.98" width="0.1524" layer="91"/>
<wire x1="205.74" y1="220.98" x2="205.74" y2="223.52" width="0.1524" layer="91"/>
<wire x1="205.74" y1="223.52" x2="190.5" y2="223.52" width="0.1524" layer="91"/>
<pinref part="U6" gate="G$1" pin="1"/>
<wire x1="208.28" y1="223.52" x2="205.74" y2="223.52" width="0.1524" layer="91"/>
<junction x="205.74" y="223.52"/>
<label x="193.04" y="223.52" size="1.27" layer="95"/>
<junction x="190.5" y="223.52"/>
</segment>
<segment>
<pinref part="J8" gate="G$4" pin="1"/>
<wire x1="340.36" y1="137.16" x2="299.72" y2="137.16" width="0.1524" layer="91"/>
<junction x="299.72" y="137.16"/>
<label x="302.26" y="137.16" size="1.27" layer="95"/>
</segment>
</net>
<net name="HB1_MOTXN" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="30"/>
<wire x1="243.84" y1="218.44" x2="248.92" y2="218.44" width="0.1524" layer="91"/>
<pinref part="U6" gate="G$1" pin="29"/>
<wire x1="248.92" y1="218.44" x2="264.16" y2="218.44" width="0.1524" layer="91"/>
<wire x1="243.84" y1="215.9" x2="248.92" y2="215.9" width="0.1524" layer="91"/>
<wire x1="248.92" y1="215.9" x2="248.92" y2="218.44" width="0.1524" layer="91"/>
<junction x="248.92" y="218.44"/>
<label x="251.46" y="218.44" size="1.27" layer="95"/>
<junction x="264.16" y="218.44"/>
</segment>
<segment>
<pinref part="J8" gate="G$3" pin="1"/>
<wire x1="340.36" y1="139.7" x2="299.72" y2="139.7" width="0.1524" layer="91"/>
<junction x="299.72" y="139.7"/>
<label x="302.26" y="139.7" size="1.27" layer="95"/>
</segment>
</net>
<net name="HB1_MOTYP" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="28"/>
<wire x1="243.84" y1="213.36" x2="248.92" y2="213.36" width="0.1524" layer="91"/>
<pinref part="U6" gate="G$1" pin="27"/>
<wire x1="248.92" y1="213.36" x2="264.16" y2="213.36" width="0.1524" layer="91"/>
<wire x1="243.84" y1="210.82" x2="248.92" y2="210.82" width="0.1524" layer="91"/>
<wire x1="248.92" y1="210.82" x2="248.92" y2="213.36" width="0.1524" layer="91"/>
<junction x="248.92" y="213.36"/>
<label x="251.46" y="213.36" size="1.27" layer="95"/>
<junction x="264.16" y="213.36"/>
</segment>
<segment>
<pinref part="J8" gate="G$2" pin="1"/>
<wire x1="340.36" y1="142.24" x2="299.72" y2="142.24" width="0.1524" layer="91"/>
<junction x="299.72" y="142.24"/>
<label x="302.26" y="142.24" size="1.27" layer="95"/>
</segment>
</net>
<net name="HB1_MOTYN" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="24"/>
<wire x1="243.84" y1="203.2" x2="248.92" y2="203.2" width="0.1524" layer="91"/>
<pinref part="U6" gate="G$1" pin="23"/>
<wire x1="248.92" y1="203.2" x2="264.16" y2="203.2" width="0.1524" layer="91"/>
<wire x1="243.84" y1="200.66" x2="248.92" y2="200.66" width="0.1524" layer="91"/>
<wire x1="248.92" y1="200.66" x2="248.92" y2="203.2" width="0.1524" layer="91"/>
<junction x="248.92" y="203.2"/>
<label x="251.46" y="203.2" size="1.27" layer="95"/>
<junction x="264.16" y="203.2"/>
</segment>
<segment>
<pinref part="J8" gate="G$1" pin="1"/>
<wire x1="340.36" y1="144.78" x2="299.72" y2="144.78" width="0.1524" layer="91"/>
<junction x="299.72" y="144.78"/>
<label x="302.26" y="144.78" size="1.27" layer="95"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="16"/>
<wire x1="71.12" y1="185.42" x2="73.66" y2="185.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$29" class="0">
<segment>
<pinref part="R38" gate="G$1" pin="2"/>
<pinref part="R37" gate="G$1" pin="1"/>
<wire x1="33.02" y1="210.82" x2="35.56" y2="210.82" width="0.1524" layer="91"/>
<pinref part="C34" gate="G$1" pin="1"/>
<wire x1="35.56" y1="210.82" x2="40.64" y2="210.82" width="0.1524" layer="91"/>
<wire x1="40.64" y1="210.82" x2="45.72" y2="210.82" width="0.1524" layer="91"/>
<wire x1="40.64" y1="208.28" x2="40.64" y2="210.82" width="0.1524" layer="91"/>
<pinref part="R35" gate="G$1" pin="2"/>
<wire x1="33.02" y1="205.74" x2="35.56" y2="205.74" width="0.1524" layer="91"/>
<wire x1="35.56" y1="205.74" x2="35.56" y2="210.82" width="0.1524" layer="91"/>
<junction x="35.56" y="210.82"/>
<junction x="40.64" y="210.82"/>
</segment>
</net>
<net name="N$31" class="0">
<segment>
<pinref part="R47" gate="G$1" pin="2"/>
<pinref part="R44" gate="G$1" pin="1"/>
<wire x1="33.02" y1="187.96" x2="35.56" y2="187.96" width="0.1524" layer="91"/>
<pinref part="C43" gate="G$1" pin="1"/>
<wire x1="35.56" y1="187.96" x2="45.72" y2="187.96" width="0.1524" layer="91"/>
<wire x1="45.72" y1="187.96" x2="50.8" y2="187.96" width="0.1524" layer="91"/>
<wire x1="45.72" y1="185.42" x2="45.72" y2="187.96" width="0.1524" layer="91"/>
<pinref part="R46" gate="G$1" pin="2"/>
<wire x1="33.02" y1="182.88" x2="35.56" y2="182.88" width="0.1524" layer="91"/>
<wire x1="35.56" y1="182.88" x2="35.56" y2="187.96" width="0.1524" layer="91"/>
<junction x="35.56" y="187.96"/>
<junction x="45.72" y="187.96"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="17"/>
<pinref part="C31" gate="G$1" pin="1"/>
<wire x1="109.22" y1="116.84" x2="114.3" y2="116.84" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="18"/>
<wire x1="109.22" y1="119.38" x2="127" y2="119.38" width="0.1524" layer="91"/>
<wire x1="127" y1="119.38" x2="127" y2="116.84" width="0.1524" layer="91"/>
<pinref part="C31" gate="G$1" pin="2"/>
<wire x1="127" y1="116.84" x2="124.46" y2="116.84" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="19"/>
<pinref part="C32" gate="G$1" pin="1"/>
<wire x1="109.22" y1="121.92" x2="114.3" y2="121.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="13"/>
<wire x1="73.66" y1="124.46" x2="71.12" y2="124.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="7"/>
<wire x1="73.66" y1="139.7" x2="71.12" y2="139.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="15"/>
<pinref part="R15" gate="G$1" pin="2"/>
<wire x1="73.66" y1="119.38" x2="60.96" y2="119.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="6"/>
<wire x1="73.66" y1="142.24" x2="55.88" y2="142.24" width="0.1524" layer="91"/>
<pinref part="R16" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="16"/>
<wire x1="71.12" y1="116.84" x2="73.66" y2="116.84" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$26" class="0">
<segment>
<pinref part="R17" gate="G$1" pin="2"/>
<pinref part="R16" gate="G$1" pin="1"/>
<wire x1="33.02" y1="142.24" x2="35.56" y2="142.24" width="0.1524" layer="91"/>
<pinref part="C35" gate="G$1" pin="1"/>
<wire x1="35.56" y1="142.24" x2="40.64" y2="142.24" width="0.1524" layer="91"/>
<wire x1="40.64" y1="142.24" x2="45.72" y2="142.24" width="0.1524" layer="91"/>
<wire x1="40.64" y1="139.7" x2="40.64" y2="142.24" width="0.1524" layer="91"/>
<pinref part="R19" gate="G$1" pin="2"/>
<wire x1="33.02" y1="137.16" x2="35.56" y2="137.16" width="0.1524" layer="91"/>
<wire x1="35.56" y1="137.16" x2="35.56" y2="142.24" width="0.1524" layer="91"/>
<junction x="35.56" y="142.24"/>
<junction x="40.64" y="142.24"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<pinref part="R18" gate="G$1" pin="2"/>
<pinref part="R15" gate="G$1" pin="1"/>
<wire x1="33.02" y1="119.38" x2="35.56" y2="119.38" width="0.1524" layer="91"/>
<pinref part="C33" gate="G$1" pin="1"/>
<wire x1="35.56" y1="119.38" x2="45.72" y2="119.38" width="0.1524" layer="91"/>
<wire x1="45.72" y1="119.38" x2="50.8" y2="119.38" width="0.1524" layer="91"/>
<wire x1="45.72" y1="116.84" x2="45.72" y2="119.38" width="0.1524" layer="91"/>
<pinref part="R20" gate="G$1" pin="2"/>
<wire x1="33.02" y1="114.3" x2="35.56" y2="114.3" width="0.1524" layer="91"/>
<wire x1="35.56" y1="114.3" x2="35.56" y2="119.38" width="0.1524" layer="91"/>
<junction x="35.56" y="119.38"/>
<junction x="45.72" y="119.38"/>
</segment>
</net>
<net name="N$28" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="17"/>
<pinref part="C36" gate="G$1" pin="1"/>
<wire x1="109.22" y1="48.26" x2="114.3" y2="48.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$30" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="18"/>
<wire x1="109.22" y1="50.8" x2="127" y2="50.8" width="0.1524" layer="91"/>
<wire x1="127" y1="50.8" x2="127" y2="48.26" width="0.1524" layer="91"/>
<pinref part="C36" gate="G$1" pin="2"/>
<wire x1="127" y1="48.26" x2="124.46" y2="48.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$32" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="19"/>
<pinref part="C37" gate="G$1" pin="1"/>
<wire x1="109.22" y1="53.34" x2="114.3" y2="53.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$33" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="13"/>
<wire x1="73.66" y1="55.88" x2="71.12" y2="55.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$34" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="7"/>
<wire x1="73.66" y1="71.12" x2="71.12" y2="71.12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$35" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="15"/>
<pinref part="R21" gate="G$1" pin="2"/>
<wire x1="73.66" y1="50.8" x2="60.96" y2="50.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$36" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="6"/>
<wire x1="73.66" y1="73.66" x2="55.88" y2="73.66" width="0.1524" layer="91"/>
<pinref part="R22" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$37" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="16"/>
<wire x1="71.12" y1="48.26" x2="73.66" y2="48.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$38" class="0">
<segment>
<pinref part="R23" gate="G$1" pin="2"/>
<pinref part="R22" gate="G$1" pin="1"/>
<wire x1="33.02" y1="73.66" x2="35.56" y2="73.66" width="0.1524" layer="91"/>
<pinref part="C39" gate="G$1" pin="1"/>
<wire x1="35.56" y1="73.66" x2="40.64" y2="73.66" width="0.1524" layer="91"/>
<wire x1="40.64" y1="73.66" x2="45.72" y2="73.66" width="0.1524" layer="91"/>
<wire x1="40.64" y1="71.12" x2="40.64" y2="73.66" width="0.1524" layer="91"/>
<pinref part="R25" gate="G$1" pin="2"/>
<wire x1="33.02" y1="68.58" x2="35.56" y2="68.58" width="0.1524" layer="91"/>
<wire x1="35.56" y1="68.58" x2="35.56" y2="73.66" width="0.1524" layer="91"/>
<junction x="35.56" y="73.66"/>
<junction x="40.64" y="73.66"/>
</segment>
</net>
<net name="N$39" class="0">
<segment>
<pinref part="R24" gate="G$1" pin="2"/>
<pinref part="R21" gate="G$1" pin="1"/>
<wire x1="33.02" y1="50.8" x2="35.56" y2="50.8" width="0.1524" layer="91"/>
<pinref part="C38" gate="G$1" pin="1"/>
<wire x1="35.56" y1="50.8" x2="45.72" y2="50.8" width="0.1524" layer="91"/>
<wire x1="45.72" y1="50.8" x2="50.8" y2="50.8" width="0.1524" layer="91"/>
<wire x1="45.72" y1="48.26" x2="45.72" y2="50.8" width="0.1524" layer="91"/>
<pinref part="R26" gate="G$1" pin="2"/>
<wire x1="33.02" y1="45.72" x2="35.56" y2="45.72" width="0.1524" layer="91"/>
<wire x1="35.56" y1="45.72" x2="35.56" y2="50.8" width="0.1524" layer="91"/>
<junction x="35.56" y="50.8"/>
<junction x="45.72" y="50.8"/>
</segment>
</net>
<net name="N$40" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="17"/>
<pinref part="C40" gate="G$1" pin="1"/>
<wire x1="243.84" y1="185.42" x2="248.92" y2="185.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$41" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="18"/>
<wire x1="243.84" y1="187.96" x2="261.62" y2="187.96" width="0.1524" layer="91"/>
<wire x1="261.62" y1="187.96" x2="261.62" y2="185.42" width="0.1524" layer="91"/>
<pinref part="C40" gate="G$1" pin="2"/>
<wire x1="261.62" y1="185.42" x2="259.08" y2="185.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$42" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="19"/>
<pinref part="C41" gate="G$1" pin="1"/>
<wire x1="243.84" y1="190.5" x2="248.92" y2="190.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$43" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="13"/>
<wire x1="208.28" y1="193.04" x2="205.74" y2="193.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$44" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="7"/>
<wire x1="208.28" y1="208.28" x2="205.74" y2="208.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$45" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="15"/>
<pinref part="R27" gate="G$1" pin="2"/>
<wire x1="208.28" y1="187.96" x2="195.58" y2="187.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$46" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="6"/>
<wire x1="208.28" y1="210.82" x2="190.5" y2="210.82" width="0.1524" layer="91"/>
<pinref part="R28" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$47" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="16"/>
<wire x1="205.74" y1="185.42" x2="208.28" y2="185.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$48" class="0">
<segment>
<pinref part="R29" gate="G$1" pin="2"/>
<pinref part="R28" gate="G$1" pin="1"/>
<wire x1="167.64" y1="210.82" x2="170.18" y2="210.82" width="0.1524" layer="91"/>
<pinref part="C44" gate="G$1" pin="1"/>
<wire x1="170.18" y1="210.82" x2="175.26" y2="210.82" width="0.1524" layer="91"/>
<wire x1="175.26" y1="210.82" x2="180.34" y2="210.82" width="0.1524" layer="91"/>
<wire x1="175.26" y1="208.28" x2="175.26" y2="210.82" width="0.1524" layer="91"/>
<pinref part="R31" gate="G$1" pin="2"/>
<wire x1="167.64" y1="205.74" x2="170.18" y2="205.74" width="0.1524" layer="91"/>
<wire x1="170.18" y1="205.74" x2="170.18" y2="210.82" width="0.1524" layer="91"/>
<junction x="170.18" y="210.82"/>
<junction x="175.26" y="210.82"/>
</segment>
</net>
<net name="N$49" class="0">
<segment>
<pinref part="R30" gate="G$1" pin="2"/>
<pinref part="R27" gate="G$1" pin="1"/>
<wire x1="167.64" y1="187.96" x2="170.18" y2="187.96" width="0.1524" layer="91"/>
<pinref part="C42" gate="G$1" pin="1"/>
<wire x1="170.18" y1="187.96" x2="180.34" y2="187.96" width="0.1524" layer="91"/>
<wire x1="180.34" y1="187.96" x2="185.42" y2="187.96" width="0.1524" layer="91"/>
<wire x1="180.34" y1="185.42" x2="180.34" y2="187.96" width="0.1524" layer="91"/>
<pinref part="R32" gate="G$1" pin="2"/>
<wire x1="167.64" y1="182.88" x2="170.18" y2="182.88" width="0.1524" layer="91"/>
<wire x1="170.18" y1="182.88" x2="170.18" y2="187.96" width="0.1524" layer="91"/>
<junction x="170.18" y="187.96"/>
<junction x="180.34" y="187.96"/>
</segment>
</net>
<net name="N$50" class="0">
<segment>
<pinref part="U7" gate="G$1" pin="17"/>
<pinref part="C46" gate="G$1" pin="1"/>
<wire x1="243.84" y1="116.84" x2="248.92" y2="116.84" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$51" class="0">
<segment>
<pinref part="U7" gate="G$1" pin="18"/>
<wire x1="243.84" y1="119.38" x2="261.62" y2="119.38" width="0.1524" layer="91"/>
<wire x1="261.62" y1="119.38" x2="261.62" y2="116.84" width="0.1524" layer="91"/>
<pinref part="C46" gate="G$1" pin="2"/>
<wire x1="261.62" y1="116.84" x2="259.08" y2="116.84" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$52" class="0">
<segment>
<pinref part="U7" gate="G$1" pin="19"/>
<pinref part="C47" gate="G$1" pin="1"/>
<wire x1="243.84" y1="121.92" x2="248.92" y2="121.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$53" class="0">
<segment>
<pinref part="U7" gate="G$1" pin="13"/>
<wire x1="208.28" y1="124.46" x2="205.74" y2="124.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$54" class="0">
<segment>
<pinref part="U7" gate="G$1" pin="7"/>
<wire x1="208.28" y1="139.7" x2="205.74" y2="139.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$55" class="0">
<segment>
<pinref part="U7" gate="G$1" pin="15"/>
<pinref part="R33" gate="G$1" pin="2"/>
<wire x1="208.28" y1="119.38" x2="195.58" y2="119.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$56" class="0">
<segment>
<pinref part="U7" gate="G$1" pin="6"/>
<wire x1="208.28" y1="142.24" x2="190.5" y2="142.24" width="0.1524" layer="91"/>
<pinref part="R34" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$57" class="0">
<segment>
<pinref part="U7" gate="G$1" pin="16"/>
<wire x1="205.74" y1="116.84" x2="208.28" y2="116.84" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$58" class="0">
<segment>
<pinref part="R36" gate="G$1" pin="2"/>
<pinref part="R34" gate="G$1" pin="1"/>
<wire x1="167.64" y1="142.24" x2="170.18" y2="142.24" width="0.1524" layer="91"/>
<pinref part="C50" gate="G$1" pin="1"/>
<wire x1="170.18" y1="142.24" x2="175.26" y2="142.24" width="0.1524" layer="91"/>
<wire x1="175.26" y1="142.24" x2="180.34" y2="142.24" width="0.1524" layer="91"/>
<wire x1="175.26" y1="139.7" x2="175.26" y2="142.24" width="0.1524" layer="91"/>
<pinref part="R40" gate="G$1" pin="2"/>
<wire x1="167.64" y1="137.16" x2="170.18" y2="137.16" width="0.1524" layer="91"/>
<wire x1="170.18" y1="137.16" x2="170.18" y2="142.24" width="0.1524" layer="91"/>
<junction x="170.18" y="142.24"/>
<junction x="175.26" y="142.24"/>
</segment>
</net>
<net name="N$59" class="0">
<segment>
<pinref part="R39" gate="G$1" pin="2"/>
<pinref part="R33" gate="G$1" pin="1"/>
<wire x1="167.64" y1="119.38" x2="170.18" y2="119.38" width="0.1524" layer="91"/>
<pinref part="C49" gate="G$1" pin="1"/>
<wire x1="170.18" y1="119.38" x2="180.34" y2="119.38" width="0.1524" layer="91"/>
<wire x1="180.34" y1="119.38" x2="185.42" y2="119.38" width="0.1524" layer="91"/>
<wire x1="180.34" y1="116.84" x2="180.34" y2="119.38" width="0.1524" layer="91"/>
<pinref part="R41" gate="G$1" pin="2"/>
<wire x1="167.64" y1="114.3" x2="170.18" y2="114.3" width="0.1524" layer="91"/>
<wire x1="170.18" y1="114.3" x2="170.18" y2="119.38" width="0.1524" layer="91"/>
<junction x="170.18" y="119.38"/>
<junction x="180.34" y="119.38"/>
</segment>
</net>
<net name="N$60" class="0">
<segment>
<pinref part="U9" gate="G$1" pin="17"/>
<pinref part="C51" gate="G$1" pin="1"/>
<wire x1="243.84" y1="48.26" x2="248.92" y2="48.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$61" class="0">
<segment>
<pinref part="U9" gate="G$1" pin="18"/>
<wire x1="243.84" y1="50.8" x2="261.62" y2="50.8" width="0.1524" layer="91"/>
<wire x1="261.62" y1="50.8" x2="261.62" y2="48.26" width="0.1524" layer="91"/>
<pinref part="C51" gate="G$1" pin="2"/>
<wire x1="261.62" y1="48.26" x2="259.08" y2="48.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$62" class="0">
<segment>
<pinref part="U9" gate="G$1" pin="19"/>
<pinref part="C52" gate="G$1" pin="1"/>
<wire x1="243.84" y1="53.34" x2="248.92" y2="53.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$63" class="0">
<segment>
<pinref part="U9" gate="G$1" pin="13"/>
<wire x1="208.28" y1="55.88" x2="205.74" y2="55.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$64" class="0">
<segment>
<pinref part="U9" gate="G$1" pin="7"/>
<wire x1="208.28" y1="71.12" x2="205.74" y2="71.12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$65" class="0">
<segment>
<pinref part="U9" gate="G$1" pin="15"/>
<pinref part="R42" gate="G$1" pin="2"/>
<wire x1="208.28" y1="50.8" x2="195.58" y2="50.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$66" class="0">
<segment>
<pinref part="U9" gate="G$1" pin="6"/>
<wire x1="208.28" y1="73.66" x2="190.5" y2="73.66" width="0.1524" layer="91"/>
<pinref part="R43" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$67" class="0">
<segment>
<pinref part="U9" gate="G$1" pin="16"/>
<wire x1="205.74" y1="48.26" x2="208.28" y2="48.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$68" class="0">
<segment>
<pinref part="R45" gate="G$1" pin="2"/>
<pinref part="R43" gate="G$1" pin="1"/>
<wire x1="167.64" y1="73.66" x2="170.18" y2="73.66" width="0.1524" layer="91"/>
<pinref part="C54" gate="G$1" pin="1"/>
<wire x1="170.18" y1="73.66" x2="175.26" y2="73.66" width="0.1524" layer="91"/>
<wire x1="175.26" y1="73.66" x2="180.34" y2="73.66" width="0.1524" layer="91"/>
<wire x1="175.26" y1="71.12" x2="175.26" y2="73.66" width="0.1524" layer="91"/>
<pinref part="R49" gate="G$1" pin="2"/>
<wire x1="167.64" y1="68.58" x2="170.18" y2="68.58" width="0.1524" layer="91"/>
<wire x1="170.18" y1="68.58" x2="170.18" y2="73.66" width="0.1524" layer="91"/>
<junction x="170.18" y="73.66"/>
<junction x="175.26" y="73.66"/>
</segment>
</net>
<net name="N$69" class="0">
<segment>
<pinref part="R48" gate="G$1" pin="2"/>
<pinref part="R42" gate="G$1" pin="1"/>
<wire x1="167.64" y1="50.8" x2="170.18" y2="50.8" width="0.1524" layer="91"/>
<pinref part="C53" gate="G$1" pin="1"/>
<wire x1="170.18" y1="50.8" x2="180.34" y2="50.8" width="0.1524" layer="91"/>
<wire x1="180.34" y1="50.8" x2="185.42" y2="50.8" width="0.1524" layer="91"/>
<wire x1="180.34" y1="48.26" x2="180.34" y2="50.8" width="0.1524" layer="91"/>
<pinref part="R50" gate="G$1" pin="2"/>
<wire x1="167.64" y1="45.72" x2="170.18" y2="45.72" width="0.1524" layer="91"/>
<wire x1="170.18" y1="45.72" x2="170.18" y2="50.8" width="0.1524" layer="91"/>
<junction x="170.18" y="50.8"/>
<junction x="180.34" y="50.8"/>
</segment>
</net>
<net name="SCL1_5V" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="9"/>
<wire x1="73.66" y1="203.2" x2="58.42" y2="203.2" width="0.1524" layer="91"/>
<label x="58.42" y="203.2" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="9"/>
<wire x1="73.66" y1="66.04" x2="58.42" y2="66.04" width="0.1524" layer="91"/>
<label x="58.42" y="66.04" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="9"/>
<wire x1="73.66" y1="134.62" x2="58.42" y2="134.62" width="0.1524" layer="91"/>
<label x="58.42" y="134.62" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SDA2_5V" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="8"/>
<wire x1="208.28" y1="205.74" x2="193.04" y2="205.74" width="0.1524" layer="91"/>
<label x="193.04" y="205.74" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U7" gate="G$1" pin="8"/>
<wire x1="208.28" y1="137.16" x2="195.58" y2="137.16" width="0.1524" layer="91"/>
<label x="195.58" y="137.16" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U9" gate="G$1" pin="8"/>
<wire x1="208.28" y1="68.58" x2="193.04" y2="68.58" width="0.1524" layer="91"/>
<label x="193.04" y="68.58" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SCL2_5V" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="9"/>
<wire x1="208.28" y1="203.2" x2="193.04" y2="203.2" width="0.1524" layer="91"/>
<label x="193.04" y="203.2" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U7" gate="G$1" pin="9"/>
<wire x1="208.28" y1="134.62" x2="195.58" y2="134.62" width="0.1524" layer="91"/>
<label x="195.58" y="134.62" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U9" gate="G$1" pin="9"/>
<wire x1="208.28" y1="66.04" x2="193.04" y2="66.04" width="0.1524" layer="91"/>
<label x="193.04" y="66.04" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="LB1_MOTXN" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="30"/>
<wire x1="109.22" y1="218.44" x2="114.3" y2="218.44" width="0.1524" layer="91"/>
<pinref part="U8" gate="G$1" pin="29"/>
<wire x1="114.3" y1="218.44" x2="129.54" y2="218.44" width="0.1524" layer="91"/>
<wire x1="109.22" y1="215.9" x2="114.3" y2="215.9" width="0.1524" layer="91"/>
<wire x1="114.3" y1="215.9" x2="114.3" y2="218.44" width="0.1524" layer="91"/>
<junction x="114.3" y="218.44"/>
<label x="116.84" y="218.44" size="1.27" layer="95"/>
<junction x="129.54" y="218.44"/>
</segment>
<segment>
<pinref part="J5" gate="G$3" pin="1"/>
<wire x1="340.36" y1="215.9" x2="299.72" y2="215.9" width="0.1524" layer="91"/>
<junction x="299.72" y="215.9"/>
<label x="302.26" y="215.9" size="1.27" layer="95"/>
</segment>
</net>
<net name="LB1_MOTYP" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="28"/>
<wire x1="109.22" y1="213.36" x2="114.3" y2="213.36" width="0.1524" layer="91"/>
<pinref part="U8" gate="G$1" pin="27"/>
<wire x1="114.3" y1="213.36" x2="129.54" y2="213.36" width="0.1524" layer="91"/>
<wire x1="109.22" y1="210.82" x2="114.3" y2="210.82" width="0.1524" layer="91"/>
<wire x1="114.3" y1="210.82" x2="114.3" y2="213.36" width="0.1524" layer="91"/>
<junction x="114.3" y="213.36"/>
<label x="116.84" y="213.36" size="1.27" layer="95"/>
<junction x="129.54" y="213.36"/>
</segment>
<segment>
<pinref part="J5" gate="G$2" pin="1"/>
<wire x1="340.36" y1="218.44" x2="299.72" y2="218.44" width="0.1524" layer="91"/>
<junction x="299.72" y="218.44"/>
<label x="302.26" y="218.44" size="1.27" layer="95"/>
</segment>
</net>
<net name="LB1_MOTYN" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="24"/>
<wire x1="109.22" y1="203.2" x2="114.3" y2="203.2" width="0.1524" layer="91"/>
<pinref part="U8" gate="G$1" pin="23"/>
<wire x1="114.3" y1="203.2" x2="129.54" y2="203.2" width="0.1524" layer="91"/>
<wire x1="109.22" y1="200.66" x2="114.3" y2="200.66" width="0.1524" layer="91"/>
<wire x1="114.3" y1="200.66" x2="114.3" y2="203.2" width="0.1524" layer="91"/>
<junction x="114.3" y="203.2"/>
<label x="116.84" y="203.2" size="1.27" layer="95"/>
<junction x="129.54" y="203.2"/>
</segment>
<segment>
<pinref part="J5" gate="G$1" pin="1"/>
<wire x1="340.36" y1="220.98" x2="299.72" y2="220.98" width="0.1524" layer="91"/>
<junction x="299.72" y="220.98"/>
<label x="302.26" y="220.98" size="1.27" layer="95"/>
</segment>
</net>
<net name="LB1_MOTXP" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="2"/>
<wire x1="73.66" y1="220.98" x2="71.12" y2="220.98" width="0.1524" layer="91"/>
<wire x1="71.12" y1="220.98" x2="71.12" y2="223.52" width="0.1524" layer="91"/>
<wire x1="71.12" y1="223.52" x2="55.88" y2="223.52" width="0.1524" layer="91"/>
<pinref part="U8" gate="G$1" pin="1"/>
<wire x1="73.66" y1="223.52" x2="71.12" y2="223.52" width="0.1524" layer="91"/>
<junction x="71.12" y="223.52"/>
<label x="58.42" y="223.52" size="1.27" layer="95"/>
<junction x="55.88" y="223.52"/>
</segment>
<segment>
<pinref part="J5" gate="G$4" pin="1"/>
<wire x1="340.36" y1="213.36" x2="299.72" y2="213.36" width="0.1524" layer="91"/>
<junction x="299.72" y="213.36"/>
<label x="302.26" y="213.36" size="1.27" layer="95"/>
</segment>
</net>
<net name="LB2_MOTXN" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="30"/>
<wire x1="109.22" y1="149.86" x2="114.3" y2="149.86" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="29"/>
<wire x1="114.3" y1="149.86" x2="129.54" y2="149.86" width="0.1524" layer="91"/>
<wire x1="109.22" y1="147.32" x2="114.3" y2="147.32" width="0.1524" layer="91"/>
<wire x1="114.3" y1="147.32" x2="114.3" y2="149.86" width="0.1524" layer="91"/>
<junction x="114.3" y="149.86"/>
<label x="116.84" y="149.86" size="1.27" layer="95"/>
<junction x="129.54" y="149.86"/>
</segment>
<segment>
<pinref part="J6" gate="G$3" pin="1"/>
<wire x1="340.36" y1="190.5" x2="299.72" y2="190.5" width="0.1524" layer="91"/>
<junction x="299.72" y="190.5"/>
<label x="302.26" y="190.5" size="1.27" layer="95"/>
</segment>
</net>
<net name="LB2_MOTYP" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="28"/>
<wire x1="109.22" y1="144.78" x2="114.3" y2="144.78" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="27"/>
<wire x1="114.3" y1="144.78" x2="129.54" y2="144.78" width="0.1524" layer="91"/>
<wire x1="109.22" y1="142.24" x2="114.3" y2="142.24" width="0.1524" layer="91"/>
<wire x1="114.3" y1="142.24" x2="114.3" y2="144.78" width="0.1524" layer="91"/>
<junction x="114.3" y="144.78"/>
<label x="116.84" y="144.78" size="1.27" layer="95"/>
<junction x="129.54" y="144.78"/>
</segment>
<segment>
<pinref part="J6" gate="G$2" pin="1"/>
<wire x1="340.36" y1="193.04" x2="299.72" y2="193.04" width="0.1524" layer="91"/>
<junction x="299.72" y="193.04"/>
<label x="302.26" y="193.04" size="1.27" layer="95"/>
</segment>
</net>
<net name="LB2_MOTYN" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="24"/>
<wire x1="109.22" y1="134.62" x2="114.3" y2="134.62" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="23"/>
<wire x1="114.3" y1="134.62" x2="129.54" y2="134.62" width="0.1524" layer="91"/>
<wire x1="109.22" y1="132.08" x2="114.3" y2="132.08" width="0.1524" layer="91"/>
<wire x1="114.3" y1="132.08" x2="114.3" y2="134.62" width="0.1524" layer="91"/>
<junction x="114.3" y="134.62"/>
<label x="116.84" y="134.62" size="1.27" layer="95"/>
<junction x="129.54" y="134.62"/>
</segment>
<segment>
<pinref part="J6" gate="G$1" pin="1"/>
<wire x1="340.36" y1="195.58" x2="299.72" y2="195.58" width="0.1524" layer="91"/>
<junction x="299.72" y="195.58"/>
<label x="302.26" y="195.58" size="1.27" layer="95"/>
</segment>
</net>
<net name="LB2_MOTXP" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="2"/>
<wire x1="73.66" y1="152.4" x2="71.12" y2="152.4" width="0.1524" layer="91"/>
<wire x1="71.12" y1="152.4" x2="71.12" y2="154.94" width="0.1524" layer="91"/>
<wire x1="71.12" y1="154.94" x2="53.34" y2="154.94" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="1"/>
<wire x1="73.66" y1="154.94" x2="71.12" y2="154.94" width="0.1524" layer="91"/>
<junction x="71.12" y="154.94"/>
<label x="58.42" y="154.94" size="1.27" layer="95"/>
<junction x="53.34" y="154.94"/>
</segment>
<segment>
<pinref part="J6" gate="G$4" pin="1"/>
<wire x1="340.36" y1="187.96" x2="299.72" y2="187.96" width="0.1524" layer="91"/>
<junction x="299.72" y="187.96"/>
<label x="302.26" y="187.96" size="1.27" layer="95"/>
</segment>
</net>
<net name="LB3_MOTXP" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="2"/>
<wire x1="73.66" y1="83.82" x2="71.12" y2="83.82" width="0.1524" layer="91"/>
<wire x1="71.12" y1="83.82" x2="71.12" y2="86.36" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="1"/>
<wire x1="73.66" y1="86.36" x2="71.12" y2="86.36" width="0.1524" layer="91"/>
<junction x="71.12" y="86.36"/>
<label x="58.42" y="86.36" size="1.27" layer="95"/>
<wire x1="71.12" y1="86.36" x2="55.88" y2="86.36" width="0.1524" layer="91"/>
<junction x="55.88" y="86.36"/>
</segment>
<segment>
<pinref part="J7" gate="G$4" pin="1"/>
<wire x1="340.36" y1="162.56" x2="299.72" y2="162.56" width="0.1524" layer="91"/>
<junction x="299.72" y="162.56"/>
<label x="302.26" y="162.56" size="1.27" layer="95"/>
</segment>
</net>
<net name="LB3_MOTXN" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="30"/>
<wire x1="109.22" y1="81.28" x2="114.3" y2="81.28" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="29"/>
<wire x1="114.3" y1="81.28" x2="129.54" y2="81.28" width="0.1524" layer="91"/>
<wire x1="109.22" y1="78.74" x2="114.3" y2="78.74" width="0.1524" layer="91"/>
<wire x1="114.3" y1="78.74" x2="114.3" y2="81.28" width="0.1524" layer="91"/>
<junction x="114.3" y="81.28"/>
<label x="116.84" y="81.28" size="1.27" layer="95"/>
<junction x="129.54" y="81.28"/>
</segment>
<segment>
<pinref part="J7" gate="G$3" pin="1"/>
<wire x1="340.36" y1="165.1" x2="299.72" y2="165.1" width="0.1524" layer="91"/>
<junction x="299.72" y="165.1"/>
<label x="302.26" y="165.1" size="1.27" layer="95"/>
</segment>
</net>
<net name="LB3_MOTYP" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="28"/>
<wire x1="109.22" y1="76.2" x2="114.3" y2="76.2" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="27"/>
<wire x1="114.3" y1="76.2" x2="129.54" y2="76.2" width="0.1524" layer="91"/>
<wire x1="109.22" y1="73.66" x2="114.3" y2="73.66" width="0.1524" layer="91"/>
<wire x1="114.3" y1="73.66" x2="114.3" y2="76.2" width="0.1524" layer="91"/>
<junction x="114.3" y="76.2"/>
<label x="116.84" y="76.2" size="1.27" layer="95"/>
<junction x="129.54" y="76.2"/>
</segment>
<segment>
<pinref part="J7" gate="G$2" pin="1"/>
<wire x1="340.36" y1="167.64" x2="299.72" y2="167.64" width="0.1524" layer="91"/>
<junction x="299.72" y="167.64"/>
<label x="302.26" y="167.64" size="1.27" layer="95"/>
</segment>
</net>
<net name="LB3_MOTYN" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="24"/>
<wire x1="109.22" y1="66.04" x2="114.3" y2="66.04" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="23"/>
<wire x1="114.3" y1="66.04" x2="129.54" y2="66.04" width="0.1524" layer="91"/>
<wire x1="109.22" y1="63.5" x2="114.3" y2="63.5" width="0.1524" layer="91"/>
<wire x1="114.3" y1="63.5" x2="114.3" y2="66.04" width="0.1524" layer="91"/>
<junction x="114.3" y="66.04"/>
<label x="116.84" y="66.04" size="1.27" layer="95"/>
<junction x="129.54" y="66.04"/>
</segment>
<segment>
<pinref part="J7" gate="G$1" pin="1"/>
<wire x1="340.36" y1="170.18" x2="299.72" y2="170.18" width="0.1524" layer="91"/>
<junction x="299.72" y="170.18"/>
<label x="302.26" y="170.18" size="1.27" layer="95"/>
</segment>
</net>
<net name="HB3_MOTXP" class="0">
<segment>
<pinref part="U9" gate="G$1" pin="2"/>
<wire x1="208.28" y1="83.82" x2="205.74" y2="83.82" width="0.1524" layer="91"/>
<wire x1="205.74" y1="83.82" x2="205.74" y2="86.36" width="0.1524" layer="91"/>
<wire x1="205.74" y1="86.36" x2="190.5" y2="86.36" width="0.1524" layer="91"/>
<pinref part="U9" gate="G$1" pin="1"/>
<wire x1="208.28" y1="86.36" x2="205.74" y2="86.36" width="0.1524" layer="91"/>
<junction x="205.74" y="86.36"/>
<label x="193.04" y="86.36" size="1.27" layer="95"/>
<junction x="190.5" y="86.36"/>
</segment>
<segment>
<pinref part="J10" gate="G$4" pin="1"/>
<wire x1="340.36" y1="86.36" x2="299.72" y2="86.36" width="0.1524" layer="91"/>
<junction x="299.72" y="86.36"/>
<label x="302.26" y="86.36" size="1.27" layer="95"/>
</segment>
</net>
<net name="HB3_MOTXN" class="0">
<segment>
<pinref part="U9" gate="G$1" pin="30"/>
<wire x1="243.84" y1="81.28" x2="248.92" y2="81.28" width="0.1524" layer="91"/>
<pinref part="U9" gate="G$1" pin="29"/>
<wire x1="248.92" y1="81.28" x2="264.16" y2="81.28" width="0.1524" layer="91"/>
<wire x1="243.84" y1="78.74" x2="248.92" y2="78.74" width="0.1524" layer="91"/>
<wire x1="248.92" y1="78.74" x2="248.92" y2="81.28" width="0.1524" layer="91"/>
<junction x="248.92" y="81.28"/>
<label x="251.46" y="81.28" size="1.27" layer="95"/>
<junction x="264.16" y="81.28"/>
</segment>
<segment>
<pinref part="J10" gate="G$3" pin="1"/>
<wire x1="340.36" y1="88.9" x2="299.72" y2="88.9" width="0.1524" layer="91"/>
<junction x="299.72" y="88.9"/>
<label x="302.26" y="88.9" size="1.27" layer="95"/>
</segment>
</net>
<net name="HB3_MOTYP" class="0">
<segment>
<pinref part="U9" gate="G$1" pin="28"/>
<wire x1="243.84" y1="76.2" x2="248.92" y2="76.2" width="0.1524" layer="91"/>
<pinref part="U9" gate="G$1" pin="27"/>
<wire x1="248.92" y1="76.2" x2="264.16" y2="76.2" width="0.1524" layer="91"/>
<wire x1="243.84" y1="73.66" x2="248.92" y2="73.66" width="0.1524" layer="91"/>
<wire x1="248.92" y1="73.66" x2="248.92" y2="76.2" width="0.1524" layer="91"/>
<junction x="248.92" y="76.2"/>
<label x="251.46" y="76.2" size="1.27" layer="95"/>
<junction x="264.16" y="76.2"/>
</segment>
<segment>
<pinref part="J10" gate="G$2" pin="1"/>
<wire x1="340.36" y1="91.44" x2="299.72" y2="91.44" width="0.1524" layer="91"/>
<junction x="299.72" y="91.44"/>
<label x="302.26" y="91.44" size="1.27" layer="95"/>
</segment>
</net>
<net name="HB3_MOTYN" class="0">
<segment>
<pinref part="U9" gate="G$1" pin="24"/>
<wire x1="243.84" y1="66.04" x2="248.92" y2="66.04" width="0.1524" layer="91"/>
<pinref part="U9" gate="G$1" pin="23"/>
<wire x1="248.92" y1="66.04" x2="264.16" y2="66.04" width="0.1524" layer="91"/>
<wire x1="243.84" y1="63.5" x2="248.92" y2="63.5" width="0.1524" layer="91"/>
<wire x1="248.92" y1="63.5" x2="248.92" y2="66.04" width="0.1524" layer="91"/>
<junction x="248.92" y="66.04"/>
<label x="251.46" y="66.04" size="1.27" layer="95"/>
<junction x="264.16" y="66.04"/>
</segment>
<segment>
<pinref part="J10" gate="G$1" pin="1"/>
<wire x1="340.36" y1="93.98" x2="299.72" y2="93.98" width="0.1524" layer="91"/>
<junction x="299.72" y="93.98"/>
<label x="302.26" y="93.98" size="1.27" layer="95"/>
</segment>
</net>
<net name="HB2_MOTXP" class="0">
<segment>
<pinref part="U7" gate="G$1" pin="2"/>
<wire x1="208.28" y1="152.4" x2="205.74" y2="152.4" width="0.1524" layer="91"/>
<wire x1="205.74" y1="152.4" x2="205.74" y2="154.94" width="0.1524" layer="91"/>
<wire x1="205.74" y1="154.94" x2="190.5" y2="154.94" width="0.1524" layer="91"/>
<pinref part="U7" gate="G$1" pin="1"/>
<wire x1="208.28" y1="154.94" x2="205.74" y2="154.94" width="0.1524" layer="91"/>
<junction x="205.74" y="154.94"/>
<label x="193.04" y="154.94" size="1.27" layer="95"/>
<junction x="190.5" y="154.94"/>
</segment>
<segment>
<pinref part="J9" gate="G$4" pin="1"/>
<wire x1="340.36" y1="111.76" x2="299.72" y2="111.76" width="0.1524" layer="91"/>
<junction x="299.72" y="111.76"/>
<label x="302.26" y="111.76" size="1.27" layer="95"/>
</segment>
</net>
<net name="HB2_MOTXN" class="0">
<segment>
<pinref part="U7" gate="G$1" pin="30"/>
<wire x1="243.84" y1="149.86" x2="248.92" y2="149.86" width="0.1524" layer="91"/>
<pinref part="U7" gate="G$1" pin="29"/>
<wire x1="248.92" y1="149.86" x2="264.16" y2="149.86" width="0.1524" layer="91"/>
<wire x1="243.84" y1="147.32" x2="248.92" y2="147.32" width="0.1524" layer="91"/>
<wire x1="248.92" y1="147.32" x2="248.92" y2="149.86" width="0.1524" layer="91"/>
<junction x="248.92" y="149.86"/>
<label x="251.46" y="149.86" size="1.27" layer="95"/>
<junction x="264.16" y="149.86"/>
</segment>
<segment>
<pinref part="J9" gate="G$3" pin="1"/>
<wire x1="340.36" y1="114.3" x2="299.72" y2="114.3" width="0.1524" layer="91"/>
<junction x="299.72" y="114.3"/>
<label x="302.26" y="114.3" size="1.27" layer="95"/>
</segment>
</net>
<net name="HB2_MOTYP" class="0">
<segment>
<pinref part="U7" gate="G$1" pin="28"/>
<wire x1="243.84" y1="144.78" x2="248.92" y2="144.78" width="0.1524" layer="91"/>
<pinref part="U7" gate="G$1" pin="27"/>
<wire x1="248.92" y1="144.78" x2="264.16" y2="144.78" width="0.1524" layer="91"/>
<wire x1="243.84" y1="142.24" x2="248.92" y2="142.24" width="0.1524" layer="91"/>
<wire x1="248.92" y1="142.24" x2="248.92" y2="144.78" width="0.1524" layer="91"/>
<junction x="248.92" y="144.78"/>
<label x="251.46" y="144.78" size="1.27" layer="95"/>
<junction x="264.16" y="144.78"/>
</segment>
<segment>
<pinref part="J9" gate="G$2" pin="1"/>
<wire x1="340.36" y1="116.84" x2="299.72" y2="116.84" width="0.1524" layer="91"/>
<junction x="299.72" y="116.84"/>
<label x="302.26" y="116.84" size="1.27" layer="95"/>
</segment>
</net>
<net name="HB2_MOTYN" class="0">
<segment>
<pinref part="U7" gate="G$1" pin="24"/>
<wire x1="243.84" y1="134.62" x2="248.92" y2="134.62" width="0.1524" layer="91"/>
<pinref part="U7" gate="G$1" pin="23"/>
<wire x1="248.92" y1="134.62" x2="264.16" y2="134.62" width="0.1524" layer="91"/>
<wire x1="243.84" y1="132.08" x2="248.92" y2="132.08" width="0.1524" layer="91"/>
<wire x1="248.92" y1="132.08" x2="248.92" y2="134.62" width="0.1524" layer="91"/>
<junction x="248.92" y="134.62"/>
<label x="251.46" y="134.62" size="1.27" layer="95"/>
<junction x="264.16" y="134.62"/>
</segment>
<segment>
<pinref part="J9" gate="G$1" pin="1"/>
<wire x1="340.36" y1="119.38" x2="299.72" y2="119.38" width="0.1524" layer="91"/>
<junction x="299.72" y="119.38"/>
<label x="302.26" y="119.38" size="1.27" layer="95"/>
</segment>
</net>
<net name="LB_9V" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="4"/>
<wire x1="73.66" y1="215.9" x2="71.12" y2="215.9" width="0.1524" layer="91"/>
<pinref part="U8" gate="G$1" pin="3"/>
<wire x1="73.66" y1="218.44" x2="71.12" y2="218.44" width="0.1524" layer="91"/>
<pinref part="U8" gate="G$1" pin="5"/>
<wire x1="71.12" y1="218.44" x2="63.5" y2="218.44" width="0.1524" layer="91"/>
<wire x1="73.66" y1="213.36" x2="71.12" y2="213.36" width="0.1524" layer="91"/>
<wire x1="71.12" y1="213.36" x2="71.12" y2="215.9" width="0.1524" layer="91"/>
<junction x="71.12" y="218.44"/>
<junction x="71.12" y="215.9"/>
<wire x1="71.12" y1="215.9" x2="71.12" y2="218.44" width="0.1524" layer="91"/>
<label x="63.5" y="218.44" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R38" gate="G$1" pin="1"/>
<wire x1="22.86" y1="210.82" x2="20.32" y2="210.82" width="0.1524" layer="91"/>
<label x="20.32" y="210.82" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R47" gate="G$1" pin="1"/>
<wire x1="22.86" y1="187.96" x2="20.32" y2="187.96" width="0.1524" layer="91"/>
<label x="20.32" y="187.96" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="C48" gate="G$1" pin="2"/>
<wire x1="124.46" y1="190.5" x2="127" y2="190.5" width="0.1524" layer="91"/>
<wire x1="127" y1="190.5" x2="127" y2="193.04" width="0.1524" layer="91"/>
<pinref part="U8" gate="G$1" pin="20"/>
<wire x1="109.22" y1="193.04" x2="127" y2="193.04" width="0.1524" layer="91"/>
<pinref part="U8" gate="G$1" pin="22"/>
<wire x1="109.22" y1="198.12" x2="127" y2="198.12" width="0.1524" layer="91"/>
<wire x1="127" y1="193.04" x2="127" y2="195.58" width="0.1524" layer="91"/>
<wire x1="127" y1="195.58" x2="127" y2="198.12" width="0.1524" layer="91"/>
<wire x1="127" y1="198.12" x2="129.54" y2="198.12" width="0.1524" layer="91"/>
<junction x="127" y="198.12"/>
<pinref part="U8" gate="G$1" pin="21"/>
<wire x1="109.22" y1="195.58" x2="127" y2="195.58" width="0.1524" layer="91"/>
<junction x="127" y="195.58"/>
<junction x="127" y="193.04"/>
<label x="129.54" y="198.12" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="4"/>
<wire x1="73.66" y1="147.32" x2="71.12" y2="147.32" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="3"/>
<wire x1="73.66" y1="149.86" x2="71.12" y2="149.86" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="5"/>
<wire x1="71.12" y1="149.86" x2="63.5" y2="149.86" width="0.1524" layer="91"/>
<wire x1="73.66" y1="144.78" x2="71.12" y2="144.78" width="0.1524" layer="91"/>
<wire x1="71.12" y1="144.78" x2="71.12" y2="147.32" width="0.1524" layer="91"/>
<junction x="71.12" y="149.86"/>
<junction x="71.12" y="147.32"/>
<wire x1="71.12" y1="147.32" x2="71.12" y2="149.86" width="0.1524" layer="91"/>
<label x="63.5" y="149.86" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R17" gate="G$1" pin="1"/>
<wire x1="22.86" y1="142.24" x2="20.32" y2="142.24" width="0.1524" layer="91"/>
<label x="20.32" y="142.24" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R18" gate="G$1" pin="1"/>
<wire x1="22.86" y1="119.38" x2="20.32" y2="119.38" width="0.1524" layer="91"/>
<label x="20.32" y="119.38" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="C32" gate="G$1" pin="2"/>
<wire x1="124.46" y1="121.92" x2="127" y2="121.92" width="0.1524" layer="91"/>
<wire x1="127" y1="121.92" x2="127" y2="124.46" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="20"/>
<wire x1="109.22" y1="124.46" x2="127" y2="124.46" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="22"/>
<wire x1="109.22" y1="129.54" x2="127" y2="129.54" width="0.1524" layer="91"/>
<wire x1="127" y1="124.46" x2="127" y2="127" width="0.1524" layer="91"/>
<wire x1="127" y1="127" x2="127" y2="129.54" width="0.1524" layer="91"/>
<wire x1="127" y1="129.54" x2="129.54" y2="129.54" width="0.1524" layer="91"/>
<junction x="127" y="129.54"/>
<pinref part="U3" gate="G$1" pin="21"/>
<wire x1="109.22" y1="127" x2="127" y2="127" width="0.1524" layer="91"/>
<junction x="127" y="127"/>
<junction x="127" y="124.46"/>
<label x="129.54" y="129.54" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R23" gate="G$1" pin="1"/>
<wire x1="22.86" y1="73.66" x2="20.32" y2="73.66" width="0.1524" layer="91"/>
<label x="20.32" y="73.66" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="4"/>
<wire x1="73.66" y1="78.74" x2="71.12" y2="78.74" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="3"/>
<wire x1="73.66" y1="81.28" x2="71.12" y2="81.28" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="5"/>
<wire x1="71.12" y1="81.28" x2="63.5" y2="81.28" width="0.1524" layer="91"/>
<wire x1="73.66" y1="76.2" x2="71.12" y2="76.2" width="0.1524" layer="91"/>
<wire x1="71.12" y1="76.2" x2="71.12" y2="78.74" width="0.1524" layer="91"/>
<junction x="71.12" y="81.28"/>
<junction x="71.12" y="78.74"/>
<wire x1="71.12" y1="78.74" x2="71.12" y2="81.28" width="0.1524" layer="91"/>
<label x="63.5" y="81.28" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R24" gate="G$1" pin="1"/>
<wire x1="22.86" y1="50.8" x2="20.32" y2="50.8" width="0.1524" layer="91"/>
<label x="20.32" y="50.8" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="C37" gate="G$1" pin="2"/>
<wire x1="124.46" y1="53.34" x2="127" y2="53.34" width="0.1524" layer="91"/>
<wire x1="127" y1="53.34" x2="127" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="20"/>
<wire x1="109.22" y1="55.88" x2="127" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="22"/>
<wire x1="109.22" y1="60.96" x2="127" y2="60.96" width="0.1524" layer="91"/>
<wire x1="127" y1="55.88" x2="127" y2="58.42" width="0.1524" layer="91"/>
<wire x1="127" y1="58.42" x2="127" y2="60.96" width="0.1524" layer="91"/>
<wire x1="127" y1="60.96" x2="129.54" y2="60.96" width="0.1524" layer="91"/>
<junction x="127" y="60.96"/>
<pinref part="U5" gate="G$1" pin="21"/>
<wire x1="109.22" y1="58.42" x2="127" y2="58.42" width="0.1524" layer="91"/>
<junction x="127" y="58.42"/>
<junction x="127" y="55.88"/>
<label x="129.54" y="60.96" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="J5" gate="G$5" pin="1"/>
<wire x1="340.36" y1="210.82" x2="299.72" y2="210.82" width="0.1524" layer="91"/>
<junction x="299.72" y="210.82"/>
<label x="302.26" y="210.82" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="J6" gate="G$5" pin="1"/>
<wire x1="340.36" y1="185.42" x2="299.72" y2="185.42" width="0.1524" layer="91"/>
<junction x="299.72" y="185.42"/>
<label x="302.26" y="185.42" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="J7" gate="G$5" pin="1"/>
<wire x1="340.36" y1="160.02" x2="299.72" y2="160.02" width="0.1524" layer="91"/>
<junction x="299.72" y="160.02"/>
<label x="302.26" y="160.02" size="1.27" layer="95"/>
</segment>
</net>
<net name="HB_9V" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="4"/>
<wire x1="208.28" y1="215.9" x2="205.74" y2="215.9" width="0.1524" layer="91"/>
<pinref part="U6" gate="G$1" pin="3"/>
<wire x1="208.28" y1="218.44" x2="205.74" y2="218.44" width="0.1524" layer="91"/>
<pinref part="U6" gate="G$1" pin="5"/>
<wire x1="205.74" y1="218.44" x2="198.12" y2="218.44" width="0.1524" layer="91"/>
<wire x1="208.28" y1="213.36" x2="205.74" y2="213.36" width="0.1524" layer="91"/>
<wire x1="205.74" y1="213.36" x2="205.74" y2="215.9" width="0.1524" layer="91"/>
<junction x="205.74" y="218.44"/>
<junction x="205.74" y="215.9"/>
<wire x1="205.74" y1="215.9" x2="205.74" y2="218.44" width="0.1524" layer="91"/>
<label x="198.12" y="218.44" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R29" gate="G$1" pin="1"/>
<wire x1="157.48" y1="210.82" x2="154.94" y2="210.82" width="0.1524" layer="91"/>
<label x="154.94" y="210.82" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R30" gate="G$1" pin="1"/>
<wire x1="157.48" y1="187.96" x2="154.94" y2="187.96" width="0.1524" layer="91"/>
<label x="154.94" y="187.96" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="C41" gate="G$1" pin="2"/>
<wire x1="259.08" y1="190.5" x2="261.62" y2="190.5" width="0.1524" layer="91"/>
<wire x1="261.62" y1="190.5" x2="261.62" y2="193.04" width="0.1524" layer="91"/>
<pinref part="U6" gate="G$1" pin="20"/>
<wire x1="243.84" y1="193.04" x2="261.62" y2="193.04" width="0.1524" layer="91"/>
<pinref part="U6" gate="G$1" pin="22"/>
<wire x1="243.84" y1="198.12" x2="261.62" y2="198.12" width="0.1524" layer="91"/>
<wire x1="261.62" y1="193.04" x2="261.62" y2="195.58" width="0.1524" layer="91"/>
<wire x1="261.62" y1="195.58" x2="261.62" y2="198.12" width="0.1524" layer="91"/>
<wire x1="261.62" y1="198.12" x2="264.16" y2="198.12" width="0.1524" layer="91"/>
<junction x="261.62" y="198.12"/>
<pinref part="U6" gate="G$1" pin="21"/>
<wire x1="243.84" y1="195.58" x2="261.62" y2="195.58" width="0.1524" layer="91"/>
<junction x="261.62" y="195.58"/>
<junction x="261.62" y="193.04"/>
<label x="264.16" y="198.12" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R36" gate="G$1" pin="1"/>
<wire x1="157.48" y1="142.24" x2="154.94" y2="142.24" width="0.1524" layer="91"/>
<label x="154.94" y="142.24" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R39" gate="G$1" pin="1"/>
<wire x1="157.48" y1="119.38" x2="154.94" y2="119.38" width="0.1524" layer="91"/>
<label x="154.94" y="119.38" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U7" gate="G$1" pin="4"/>
<wire x1="208.28" y1="147.32" x2="205.74" y2="147.32" width="0.1524" layer="91"/>
<pinref part="U7" gate="G$1" pin="3"/>
<wire x1="208.28" y1="149.86" x2="205.74" y2="149.86" width="0.1524" layer="91"/>
<pinref part="U7" gate="G$1" pin="5"/>
<wire x1="205.74" y1="149.86" x2="198.12" y2="149.86" width="0.1524" layer="91"/>
<wire x1="208.28" y1="144.78" x2="205.74" y2="144.78" width="0.1524" layer="91"/>
<wire x1="205.74" y1="144.78" x2="205.74" y2="147.32" width="0.1524" layer="91"/>
<junction x="205.74" y="149.86"/>
<junction x="205.74" y="147.32"/>
<wire x1="205.74" y1="147.32" x2="205.74" y2="149.86" width="0.1524" layer="91"/>
<label x="198.12" y="149.86" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="C47" gate="G$1" pin="2"/>
<wire x1="259.08" y1="121.92" x2="261.62" y2="121.92" width="0.1524" layer="91"/>
<wire x1="261.62" y1="121.92" x2="261.62" y2="124.46" width="0.1524" layer="91"/>
<pinref part="U7" gate="G$1" pin="20"/>
<wire x1="243.84" y1="124.46" x2="261.62" y2="124.46" width="0.1524" layer="91"/>
<pinref part="U7" gate="G$1" pin="22"/>
<wire x1="243.84" y1="129.54" x2="261.62" y2="129.54" width="0.1524" layer="91"/>
<wire x1="261.62" y1="124.46" x2="261.62" y2="127" width="0.1524" layer="91"/>
<wire x1="261.62" y1="127" x2="261.62" y2="129.54" width="0.1524" layer="91"/>
<wire x1="261.62" y1="129.54" x2="264.16" y2="129.54" width="0.1524" layer="91"/>
<junction x="261.62" y="129.54"/>
<pinref part="U7" gate="G$1" pin="21"/>
<wire x1="243.84" y1="127" x2="261.62" y2="127" width="0.1524" layer="91"/>
<junction x="261.62" y="127"/>
<junction x="261.62" y="124.46"/>
<label x="264.16" y="129.54" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R45" gate="G$1" pin="1"/>
<wire x1="157.48" y1="73.66" x2="154.94" y2="73.66" width="0.1524" layer="91"/>
<label x="154.94" y="73.66" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R48" gate="G$1" pin="1"/>
<wire x1="157.48" y1="50.8" x2="154.94" y2="50.8" width="0.1524" layer="91"/>
<label x="154.94" y="50.8" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U9" gate="G$1" pin="4"/>
<wire x1="208.28" y1="78.74" x2="205.74" y2="78.74" width="0.1524" layer="91"/>
<pinref part="U9" gate="G$1" pin="3"/>
<wire x1="208.28" y1="81.28" x2="205.74" y2="81.28" width="0.1524" layer="91"/>
<pinref part="U9" gate="G$1" pin="5"/>
<wire x1="205.74" y1="81.28" x2="198.12" y2="81.28" width="0.1524" layer="91"/>
<wire x1="208.28" y1="76.2" x2="205.74" y2="76.2" width="0.1524" layer="91"/>
<wire x1="205.74" y1="76.2" x2="205.74" y2="78.74" width="0.1524" layer="91"/>
<junction x="205.74" y="81.28"/>
<junction x="205.74" y="78.74"/>
<wire x1="205.74" y1="78.74" x2="205.74" y2="81.28" width="0.1524" layer="91"/>
<label x="198.12" y="81.28" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="C52" gate="G$1" pin="2"/>
<wire x1="259.08" y1="53.34" x2="261.62" y2="53.34" width="0.1524" layer="91"/>
<wire x1="261.62" y1="53.34" x2="261.62" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U9" gate="G$1" pin="20"/>
<wire x1="243.84" y1="55.88" x2="261.62" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U9" gate="G$1" pin="22"/>
<wire x1="243.84" y1="60.96" x2="261.62" y2="60.96" width="0.1524" layer="91"/>
<wire x1="261.62" y1="55.88" x2="261.62" y2="58.42" width="0.1524" layer="91"/>
<wire x1="261.62" y1="58.42" x2="261.62" y2="60.96" width="0.1524" layer="91"/>
<wire x1="261.62" y1="60.96" x2="264.16" y2="60.96" width="0.1524" layer="91"/>
<junction x="261.62" y="60.96"/>
<pinref part="U9" gate="G$1" pin="21"/>
<wire x1="243.84" y1="58.42" x2="261.62" y2="58.42" width="0.1524" layer="91"/>
<junction x="261.62" y="58.42"/>
<junction x="261.62" y="55.88"/>
<label x="264.16" y="60.96" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="J8" gate="G$5" pin="1"/>
<wire x1="340.36" y1="134.62" x2="299.72" y2="134.62" width="0.1524" layer="91"/>
<junction x="299.72" y="134.62"/>
<label x="302.26" y="134.62" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="J9" gate="G$5" pin="1"/>
<wire x1="340.36" y1="109.22" x2="299.72" y2="109.22" width="0.1524" layer="91"/>
<junction x="299.72" y="109.22"/>
<label x="302.26" y="109.22" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="J10" gate="G$5" pin="1"/>
<wire x1="340.36" y1="83.82" x2="299.72" y2="83.82" width="0.1524" layer="91"/>
<junction x="299.72" y="83.82"/>
<label x="302.26" y="83.82" size="1.27" layer="95"/>
</segment>
</net>
<net name="LB1_DIR" class="0">
<segment>
<pinref part="J5" gate="G$7" pin="1"/>
<wire x1="340.36" y1="205.74" x2="299.72" y2="205.74" width="0.1524" layer="91"/>
<junction x="299.72" y="205.74"/>
<label x="302.26" y="205.74" size="1.27" layer="95"/>
</segment>
</net>
<net name="LB1_SPEED" class="0">
<segment>
<pinref part="J5" gate="G$8" pin="1"/>
<wire x1="340.36" y1="203.2" x2="299.72" y2="203.2" width="0.1524" layer="91"/>
<junction x="299.72" y="203.2"/>
<label x="302.26" y="203.2" size="1.27" layer="95"/>
</segment>
</net>
<net name="LB2_DIR" class="0">
<segment>
<pinref part="J6" gate="G$7" pin="1"/>
<wire x1="340.36" y1="180.34" x2="299.72" y2="180.34" width="0.1524" layer="91"/>
<junction x="299.72" y="180.34"/>
<label x="302.26" y="180.34" size="1.27" layer="95"/>
</segment>
</net>
<net name="LB2_SPEED" class="0">
<segment>
<pinref part="J6" gate="G$8" pin="1"/>
<wire x1="340.36" y1="177.8" x2="299.72" y2="177.8" width="0.1524" layer="91"/>
<junction x="299.72" y="177.8"/>
<label x="302.26" y="177.8" size="1.27" layer="95"/>
</segment>
</net>
<net name="LB3_DIR" class="0">
<segment>
<pinref part="J7" gate="G$7" pin="1"/>
<wire x1="340.36" y1="154.94" x2="299.72" y2="154.94" width="0.1524" layer="91"/>
<junction x="299.72" y="154.94"/>
<label x="302.26" y="154.94" size="1.27" layer="95"/>
</segment>
</net>
<net name="LB3_SPEED" class="0">
<segment>
<pinref part="J7" gate="G$8" pin="1"/>
<wire x1="340.36" y1="152.4" x2="299.72" y2="152.4" width="0.1524" layer="91"/>
<junction x="299.72" y="152.4"/>
<label x="302.26" y="152.4" size="1.27" layer="95"/>
</segment>
</net>
<net name="HB1_DIR" class="0">
<segment>
<pinref part="J8" gate="G$7" pin="1"/>
<wire x1="340.36" y1="129.54" x2="299.72" y2="129.54" width="0.1524" layer="91"/>
<junction x="299.72" y="129.54"/>
<label x="302.26" y="129.54" size="1.27" layer="95"/>
</segment>
</net>
<net name="HB1_SPEED" class="0">
<segment>
<pinref part="J8" gate="G$8" pin="1"/>
<wire x1="340.36" y1="127" x2="299.72" y2="127" width="0.1524" layer="91"/>
<junction x="299.72" y="127"/>
<label x="302.26" y="127" size="1.27" layer="95"/>
</segment>
</net>
<net name="HB2_DIR" class="0">
<segment>
<pinref part="J9" gate="G$7" pin="1"/>
<wire x1="340.36" y1="104.14" x2="299.72" y2="104.14" width="0.1524" layer="91"/>
<junction x="299.72" y="104.14"/>
<label x="302.26" y="104.14" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="J9" gate="G$8" pin="1"/>
<wire x1="340.36" y1="101.6" x2="299.72" y2="101.6" width="0.1524" layer="91"/>
<junction x="299.72" y="101.6"/>
<label x="302.26" y="101.6" size="1.27" layer="95"/>
</segment>
</net>
<net name="HB3_SPEED" class="0">
<segment>
<pinref part="J10" gate="G$8" pin="1"/>
<wire x1="340.36" y1="76.2" x2="299.72" y2="76.2" width="0.1524" layer="91"/>
<junction x="299.72" y="76.2"/>
<label x="302.26" y="76.2" size="1.27" layer="95"/>
</segment>
</net>
<net name="HB3_DIR" class="0">
<segment>
<pinref part="J10" gate="G$7" pin="1"/>
<wire x1="340.36" y1="78.74" x2="299.72" y2="78.74" width="0.1524" layer="91"/>
<junction x="299.72" y="78.74"/>
<label x="302.26" y="78.74" size="1.27" layer="95"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
<text x="50.8" y="154.94" size="1.27" layer="97" rot="MR180">AISG_LB Connector</text>
<text x="55.88" y="200.66" size="1.27" layer="97" rot="MR180">AISG_HB Connector</text>
<text x="50.8" y="167.64" size="1.27" layer="97" rot="MR180">Modem_LB Connector</text>
<text x="53.34" y="215.9" size="1.27" layer="97" rot="MR180">Modem_HB Connector</text>
<text x="127" y="228.6" size="1.27" layer="97">SBRT3U45SAF-13/SBRT3U45SA-13</text>
</plain>
<instances>
<instance part="U$5" gate="G$1" x="0" y="0" smashed="yes">
<attribute name="DRAWING_NAME" x="317.5" y="10.16" size="1.778" layer="94" font="vector"/>
<attribute name="SHEET" x="332.486" y="5.842" size="1.27" layer="94" font="vector"/>
</instance>
<instance part="C65" gate="G$1" x="195.58" y="81.28" smashed="yes" rot="R90">
<attribute name="NAME" x="194.31" y="80.01" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="198.12" y="80.01" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C66" gate="G$1" x="200.66" y="81.28" smashed="yes" rot="R90">
<attribute name="NAME" x="199.39" y="80.01" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="203.2" y="80.01" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C67" gate="G$1" x="284.48" y="81.28" smashed="yes" rot="R90">
<attribute name="NAME" x="283.21" y="80.01" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="287.02" y="80.01" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C68" gate="G$1" x="292.1" y="81.28" smashed="yes" rot="R90">
<attribute name="NAME" x="290.83" y="80.01" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="294.64" y="80.01" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="L2" gate="G$1" x="304.8" y="88.9" smashed="yes">
<attribute name="NAME" x="302.26" y="90.17" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="302.26" y="86.36" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="L3" gate="G$1" x="185.42" y="88.9" smashed="yes">
<attribute name="NAME" x="182.88" y="90.17" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="182.88" y="86.36" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="R99" gate="G$1" x="241.3" y="81.28" smashed="yes" rot="R90">
<attribute name="NAME" x="239.8014" y="80.01" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="244.602" y="80.01" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R100" gate="G$1" x="213.36" y="63.5" smashed="yes" rot="R180">
<attribute name="NAME" x="214.63" y="62.0014" size="1.27" layer="95" rot="R180"/>
<attribute name="VALUE" x="214.63" y="66.802" size="1.27" layer="96" rot="R180"/>
</instance>
<instance part="J1" gate="G$1" x="12.7" y="149.86" smashed="yes" rot="MR0">
<attribute name="NAME" x="16.002" y="150.368" size="1.27" layer="95" ratio="15" rot="MR0"/>
</instance>
<instance part="J1" gate="G$2" x="12.7" y="147.32" smashed="yes" rot="MR0">
<attribute name="NAME" x="16.002" y="147.828" size="1.27" layer="95" ratio="15" rot="MR0"/>
</instance>
<instance part="J1" gate="G$3" x="12.7" y="144.78" smashed="yes" rot="MR0">
<attribute name="NAME" x="16.002" y="145.288" size="1.27" layer="95" ratio="15" rot="MR0"/>
</instance>
<instance part="J1" gate="G$4" x="12.7" y="142.24" smashed="yes" rot="MR0">
<attribute name="NAME" x="16.002" y="142.748" size="1.27" layer="95" ratio="15" rot="MR0"/>
</instance>
<instance part="J2" gate="G$1" x="12.7" y="177.8" smashed="yes" rot="MR0">
<attribute name="NAME" x="16.002" y="178.308" size="1.27" layer="95" ratio="15" rot="MR0"/>
</instance>
<instance part="J2" gate="G$2" x="12.7" y="175.26" smashed="yes" rot="MR0">
<attribute name="NAME" x="16.002" y="175.768" size="1.27" layer="95" ratio="15" rot="MR0"/>
</instance>
<instance part="J2" gate="G$3" x="12.7" y="172.72" smashed="yes" rot="MR0">
<attribute name="NAME" x="16.002" y="173.228" size="1.27" layer="95" ratio="15" rot="MR0"/>
</instance>
<instance part="J2" gate="G$4" x="12.7" y="170.18" smashed="yes" rot="MR0">
<attribute name="NAME" x="16.002" y="170.688" size="1.27" layer="95" ratio="15" rot="MR0"/>
</instance>
<instance part="J3" gate="G$1" x="12.7" y="195.58" smashed="yes" rot="MR0">
<attribute name="NAME" x="16.002" y="196.088" size="1.27" layer="95" ratio="15" rot="MR0"/>
</instance>
<instance part="J3" gate="G$2" x="12.7" y="193.04" smashed="yes" rot="MR0">
<attribute name="NAME" x="16.002" y="193.548" size="1.27" layer="95" ratio="15" rot="MR0"/>
</instance>
<instance part="J3" gate="G$3" x="12.7" y="190.5" smashed="yes" rot="MR0">
<attribute name="NAME" x="16.002" y="191.008" size="1.27" layer="95" ratio="15" rot="MR0"/>
</instance>
<instance part="J3" gate="G$4" x="12.7" y="187.96" smashed="yes" rot="MR0">
<attribute name="NAME" x="16.002" y="188.468" size="1.27" layer="95" ratio="15" rot="MR0"/>
</instance>
<instance part="J4" gate="G$1" x="12.7" y="226.06" smashed="yes" rot="MR0">
<attribute name="NAME" x="16.002" y="226.568" size="1.27" layer="95" ratio="15" rot="MR0"/>
</instance>
<instance part="J4" gate="G$2" x="12.7" y="223.52" smashed="yes" rot="MR0">
<attribute name="NAME" x="16.002" y="224.028" size="1.27" layer="95" ratio="15" rot="MR0"/>
</instance>
<instance part="J4" gate="G$3" x="12.7" y="220.98" smashed="yes" rot="MR0">
<attribute name="NAME" x="16.002" y="221.488" size="1.27" layer="95" ratio="15" rot="MR0"/>
</instance>
<instance part="J4" gate="G$4" x="12.7" y="218.44" smashed="yes" rot="MR0">
<attribute name="NAME" x="16.002" y="218.948" size="1.27" layer="95" ratio="15" rot="MR0"/>
</instance>
<instance part="R87" gate="G$1" x="45.72" y="172.72" smashed="yes" rot="MR0">
<attribute name="NAME" x="46.99" y="174.2186" size="1.27" layer="95" rot="MR0"/>
<attribute name="VALUE" x="46.99" y="169.418" size="1.27" layer="96" rot="MR0"/>
</instance>
<instance part="R88" gate="G$1" x="45.72" y="170.18" smashed="yes" rot="MR0">
<attribute name="NAME" x="46.99" y="171.6786" size="1.27" layer="95" rot="MR0"/>
<attribute name="VALUE" x="46.99" y="166.878" size="1.27" layer="96" rot="MR0"/>
</instance>
<instance part="R89" gate="G$1" x="45.72" y="144.78" smashed="yes" rot="MR0">
<attribute name="NAME" x="46.99" y="146.2786" size="1.27" layer="95" rot="MR0"/>
<attribute name="VALUE" x="46.99" y="141.478" size="1.27" layer="96" rot="MR0"/>
</instance>
<instance part="R90" gate="G$1" x="45.72" y="142.24" smashed="yes" rot="MR0">
<attribute name="NAME" x="46.99" y="143.7386" size="1.27" layer="95" rot="MR0"/>
<attribute name="VALUE" x="46.99" y="138.938" size="1.27" layer="96" rot="MR0"/>
</instance>
<instance part="R91" gate="G$1" x="38.1" y="160.02" smashed="yes" rot="MR90">
<attribute name="NAME" x="39.5986" y="158.75" size="1.27" layer="95" rot="MR90"/>
<attribute name="VALUE" x="34.798" y="158.75" size="1.27" layer="96" rot="MR90"/>
</instance>
<instance part="R92" gate="G$1" x="33.02" y="160.02" smashed="yes" rot="MR90">
<attribute name="NAME" x="34.5186" y="158.75" size="1.27" layer="95" rot="MR90"/>
<attribute name="VALUE" x="29.718" y="158.75" size="1.27" layer="96" rot="MR90"/>
</instance>
<instance part="R93" gate="G$1" x="48.26" y="190.5" smashed="yes" rot="MR0">
<attribute name="NAME" x="49.53" y="191.9986" size="1.27" layer="95" rot="MR0"/>
<attribute name="VALUE" x="49.53" y="187.198" size="1.27" layer="96" rot="MR0"/>
</instance>
<instance part="R94" gate="G$1" x="48.26" y="187.96" smashed="yes" rot="MR0">
<attribute name="NAME" x="49.53" y="189.4586" size="1.27" layer="95" rot="MR0"/>
<attribute name="VALUE" x="49.53" y="184.658" size="1.27" layer="96" rot="MR0"/>
</instance>
<instance part="R95" gate="G$1" x="48.26" y="220.98" smashed="yes" rot="MR0">
<attribute name="NAME" x="49.53" y="222.4786" size="1.27" layer="95" rot="MR0"/>
<attribute name="VALUE" x="49.53" y="217.678" size="1.27" layer="96" rot="MR0"/>
</instance>
<instance part="R96" gate="G$1" x="48.26" y="218.44" smashed="yes" rot="MR0">
<attribute name="NAME" x="49.53" y="219.9386" size="1.27" layer="95" rot="MR0"/>
<attribute name="VALUE" x="49.53" y="215.138" size="1.27" layer="96" rot="MR0"/>
</instance>
<instance part="R97" gate="G$1" x="33.02" y="205.74" smashed="yes" rot="MR270">
<attribute name="NAME" x="31.5214" y="207.01" size="1.27" layer="95" rot="MR270"/>
<attribute name="VALUE" x="36.322" y="207.01" size="1.27" layer="96" rot="MR270"/>
</instance>
<instance part="R98" gate="G$1" x="38.1" y="205.74" smashed="yes" rot="MR270">
<attribute name="NAME" x="36.6014" y="207.01" size="1.27" layer="95" rot="MR270"/>
<attribute name="VALUE" x="41.402" y="207.01" size="1.27" layer="96" rot="MR270"/>
</instance>
<instance part="D2" gate="G$1" x="142.24" y="223.52" smashed="yes">
<attribute name="NAME" x="134.62" y="224.79" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="137.16" y="218.44" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="D3" gate="G$1" x="142.24" y="215.9" smashed="yes">
<attribute name="NAME" x="134.62" y="217.17" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="137.16" y="210.82" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="D4" gate="G$1" x="142.24" y="208.28" smashed="yes">
<attribute name="NAME" x="134.62" y="209.55" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="137.16" y="203.2" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="D5" gate="G$1" x="142.24" y="200.66" smashed="yes">
<attribute name="NAME" x="134.62" y="201.93" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="137.16" y="195.58" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="C81" gate="G$1" x="177.8" y="215.9" smashed="yes" rot="R90">
<attribute name="NAME" x="176.53" y="214.63" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="180.34" y="214.63" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C82" gate="G$1" x="233.68" y="215.9" smashed="yes" rot="R90">
<attribute name="NAME" x="232.41" y="214.63" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="236.22" y="214.63" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C83" gate="G$1" x="256.54" y="215.9" smashed="yes" rot="R90">
<attribute name="NAME" x="255.27" y="214.63" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="259.08" y="214.63" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="R125" gate="G$1" x="213.36" y="215.9" smashed="yes" rot="R90">
<attribute name="NAME" x="211.8614" y="214.63" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="216.662" y="214.63" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R126" gate="G$1" x="218.44" y="200.66" smashed="yes" rot="R90">
<attribute name="NAME" x="216.9414" y="199.39" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="221.742" y="199.39" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="L4" gate="G$1" x="165.1" y="223.52" smashed="yes">
<attribute name="NAME" x="162.56" y="224.79" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="162.56" y="220.98" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="R127" gate="G$1" x="165.1" y="228.6" smashed="yes" rot="R180">
<attribute name="NAME" x="166.37" y="227.1014" size="1.27" layer="95" rot="R180"/>
<attribute name="VALUE" x="166.37" y="231.902" size="1.27" layer="96" rot="R180"/>
</instance>
<instance part="Q2" gate="G$1" x="246.38" y="220.98" smashed="yes" rot="R90">
<attribute name="NAME" x="246.38" y="226.06" size="1.27" layer="95" ratio="15" rot="MR90"/>
<attribute name="VALUE" x="243.84" y="226.06" size="1.27" layer="96" ratio="15" rot="MR90"/>
</instance>
<instance part="Q3" gate="G$1" x="187.96" y="220.98" smashed="yes" rot="MR90">
<attribute name="NAME" x="187.96" y="226.06" size="1.27" layer="95" ratio="15" rot="R90"/>
<attribute name="VALUE" x="190.5" y="226.06" size="1.27" layer="96" ratio="15" rot="R90"/>
</instance>
<instance part="Q4" gate="G$1" x="215.9" y="187.96" smashed="yes">
<attribute name="NAME" x="220.98" y="187.96" size="1.27" layer="95" ratio="15"/>
<attribute name="VALUE" x="220.98" y="185.42" size="1.27" layer="96" ratio="15"/>
</instance>
<instance part="R129" gate="G$1" x="210.82" y="180.34" smashed="yes">
<attribute name="NAME" x="209.55" y="181.8386" size="1.27" layer="95"/>
<attribute name="VALUE" x="209.55" y="177.038" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY63" gate="GND" x="218.44" y="175.26" smashed="yes">
<attribute name="VALUE" x="216.535" y="172.085" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY64" gate="GND" x="205.74" y="58.42" smashed="yes">
<attribute name="VALUE" x="203.835" y="55.245" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY65" gate="GND" x="195.58" y="71.12" smashed="yes">
<attribute name="VALUE" x="193.675" y="67.945" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY66" gate="GND" x="200.66" y="71.12" smashed="yes">
<attribute name="VALUE" x="198.755" y="67.945" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY67" gate="GND" x="284.48" y="71.12" smashed="yes">
<attribute name="VALUE" x="282.575" y="67.945" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY68" gate="GND" x="292.1" y="71.12" smashed="yes">
<attribute name="VALUE" x="290.195" y="67.945" size="1.778" layer="96"/>
</instance>
<instance part="D6" gate="G$1" x="198.12" y="215.9" smashed="yes" rot="R90">
<attribute name="NAME" x="196.85" y="208.28" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="203.2" y="210.82" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="R128" gate="G$1" x="185.42" y="200.66" smashed="yes" rot="R90">
<attribute name="NAME" x="183.9214" y="199.39" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="188.722" y="199.39" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="SUPPLY69" gate="GND" x="185.42" y="190.5" smashed="yes">
<attribute name="VALUE" x="183.515" y="187.325" size="1.778" layer="96"/>
</instance>
<instance part="R130" gate="G$1" x="205.74" y="208.28" smashed="yes" rot="R180">
<attribute name="NAME" x="207.01" y="206.7814" size="1.27" layer="95" rot="R180"/>
<attribute name="VALUE" x="207.01" y="211.582" size="1.27" layer="96" rot="R180"/>
</instance>
<instance part="D7" gate="G$1" x="223.52" y="215.9" smashed="yes" rot="R90">
<attribute name="NAME" x="222.25" y="208.28" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="228.6" y="210.82" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="L5" gate="G$1" x="274.32" y="223.52" smashed="yes">
<attribute name="NAME" x="271.78" y="224.79" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="271.78" y="220.98" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="C84" gate="G$1" x="264.16" y="215.9" smashed="yes" rot="R90">
<attribute name="NAME" x="262.89" y="214.63" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="266.7" y="214.63" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C85" gate="G$1" x="284.48" y="215.9" smashed="yes" rot="R90">
<attribute name="NAME" x="283.21" y="214.63" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="287.02" y="214.63" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C86" gate="G$1" x="292.1" y="215.9" smashed="yes" rot="R90">
<attribute name="NAME" x="290.83" y="214.63" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="294.64" y="214.63" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="SUPPLY78" gate="GND" x="22.86" y="134.62" smashed="yes">
<attribute name="VALUE" x="20.955" y="131.445" size="1.778" layer="96"/>
</instance>
<instance part="C111" gate="G$1" x="205.74" y="132.08" smashed="yes" rot="R270">
<attribute name="NAME" x="207.01" y="133.35" size="1.27" layer="95" ratio="10" rot="R270"/>
<attribute name="VALUE" x="203.2" y="133.35" size="1.27" layer="96" ratio="10" rot="R270"/>
</instance>
<instance part="L10" gate="G$1" x="162.56" y="154.94" smashed="yes">
<attribute name="NAME" x="160.02" y="156.21" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="160.02" y="152.4" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="R160" gate="G$1" x="162.56" y="162.56" smashed="yes">
<attribute name="NAME" x="161.29" y="164.0586" size="1.27" layer="95"/>
<attribute name="VALUE" x="161.29" y="159.258" size="1.27" layer="96"/>
</instance>
<instance part="C112" gate="G$1" x="182.88" y="147.32" smashed="yes" rot="R90">
<attribute name="NAME" x="181.61" y="146.05" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="185.42" y="146.05" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C113" gate="G$1" x="190.5" y="147.32" smashed="yes" rot="R90">
<attribute name="NAME" x="189.23" y="146.05" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="193.04" y="146.05" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="U22" gate="A" x="215.9" y="137.16" smashed="yes">
<attribute name="NAME" x="220.98" y="160.02" size="2.0828" layer="95" ratio="6" rot="SR0"/>
<attribute name="VALUE" x="220.98" y="157.734" size="2.0828" layer="96" ratio="6" rot="SR0"/>
</instance>
<instance part="R161" gate="G$1" x="198.12" y="132.08" smashed="yes" rot="R90">
<attribute name="NAME" x="196.6214" y="130.81" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="201.422" y="130.81" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="SUPPLY89" gate="GND" x="175.26" y="134.62" smashed="yes">
<attribute name="VALUE" x="173.355" y="131.445" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY90" gate="GND" x="198.12" y="119.38" smashed="yes">
<attribute name="VALUE" x="196.215" y="116.205" size="1.778" layer="96"/>
</instance>
<instance part="R162" gate="G$1" x="325.12" y="144.78" smashed="yes" rot="R90">
<attribute name="NAME" x="323.6214" y="143.51" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="328.422" y="143.51" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R163" gate="G$1" x="332.74" y="121.92" smashed="yes" rot="R180">
<attribute name="NAME" x="334.01" y="120.4214" size="1.27" layer="95" rot="R180"/>
<attribute name="VALUE" x="334.01" y="125.222" size="1.27" layer="96" rot="R180"/>
</instance>
<instance part="L11" gate="G$1" x="294.64" y="154.94" smashed="yes">
<attribute name="NAME" x="292.1" y="156.21" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="292.1" y="152.4" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="C114" gate="G$1" x="266.7" y="149.86" smashed="yes">
<attribute name="NAME" x="265.43" y="151.13" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="265.43" y="147.32" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="SUPPLY91" gate="GND" x="340.36" y="116.84" smashed="yes">
<attribute name="VALUE" x="338.455" y="113.665" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY92" gate="GND" x="246.38" y="134.62" smashed="yes">
<attribute name="VALUE" x="244.475" y="131.445" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY93" gate="GND" x="302.26" y="134.62" smashed="yes">
<attribute name="VALUE" x="300.355" y="131.445" size="1.778" layer="96"/>
</instance>
<instance part="D22" gate="A" x="287.02" y="144.78" smashed="yes" rot="R90">
<attribute name="NAME" x="285.75" y="137.16" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="292.1" y="139.7" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="R164" gate="G$1" x="185.42" y="93.98" smashed="yes" rot="R180">
<attribute name="NAME" x="186.69" y="92.4814" size="1.27" layer="95" rot="R180"/>
<attribute name="VALUE" x="186.69" y="97.282" size="1.27" layer="96" rot="R180"/>
</instance>
<instance part="U13" gate="A" x="213.36" y="91.44" smashed="yes">
<attribute name="NAME" x="213.36" y="91.694" size="1.27" layer="95" font="vector"/>
<attribute name="VALUE" x="220.98" y="91.694" size="1.27" layer="96" font="vector"/>
</instance>
<instance part="R165" gate="G$1" x="304.8" y="93.98" smashed="yes" rot="R180">
<attribute name="NAME" x="306.07" y="92.4814" size="1.27" layer="95" rot="R180"/>
<attribute name="VALUE" x="306.07" y="97.282" size="1.27" layer="96" rot="R180"/>
</instance>
<instance part="+3V8" gate="G$1" x="317.5" y="104.14" smashed="yes">
<attribute name="VALUE" x="314.96" y="99.06" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="C121" gate="G$1" x="302.26" y="147.32" smashed="yes" rot="R90">
<attribute name="NAME" x="300.99" y="146.05" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="304.8" y="146.05" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C122" gate="G$1" x="175.26" y="147.32" smashed="yes" rot="R90">
<attribute name="NAME" x="173.99" y="146.05" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="177.8" y="146.05" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C123" gate="G$1" x="309.88" y="147.32" smashed="yes" rot="R90">
<attribute name="NAME" x="308.61" y="146.05" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="312.42" y="146.05" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C124" gate="G$1" x="317.5" y="147.32" smashed="yes" rot="R90">
<attribute name="NAME" x="316.23" y="146.05" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="320.04" y="146.05" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="R166" gate="G$1" x="251.46" y="149.86" smashed="yes" rot="R180">
<attribute name="NAME" x="252.73" y="148.3614" size="1.27" layer="95" rot="R180"/>
<attribute name="VALUE" x="252.73" y="153.162" size="1.27" layer="96" rot="R180"/>
</instance>
<instance part="R167" gate="G$1" x="276.86" y="142.24" smashed="yes" rot="R270">
<attribute name="NAME" x="278.3586" y="143.51" size="1.27" layer="95" rot="R270"/>
<attribute name="VALUE" x="273.558" y="143.51" size="1.27" layer="96" rot="R270"/>
</instance>
<instance part="C125" gate="G$1" x="276.86" y="129.54" smashed="yes" rot="R90">
<attribute name="NAME" x="275.59" y="128.27" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="279.4" y="128.27" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="D23" gate="G$1" x="256.54" y="81.28" smashed="yes" rot="R90">
<attribute name="NAME" x="255.27" y="73.66" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="261.62" y="76.2" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="R168" gate="G$1" x="248.92" y="73.66" smashed="yes" rot="R180">
<attribute name="NAME" x="250.19" y="72.1614" size="1.27" layer="95" rot="R180"/>
<attribute name="VALUE" x="250.19" y="76.962" size="1.27" layer="96" rot="R180"/>
</instance>
<instance part="Q17" gate="G$1" x="256.54" y="66.04" smashed="yes" rot="MR90">
<attribute name="NAME" x="248.92" y="76.2" size="1.778" layer="95" rot="MR270"/>
<attribute name="VALUE" x="251.46" y="76.2" size="1.778" layer="96" rot="MR270"/>
</instance>
<instance part="C126" gate="G$1" x="264.16" y="71.12" smashed="yes" rot="R180">
<attribute name="NAME" x="265.43" y="69.85" size="1.27" layer="95" ratio="10" rot="R180"/>
<attribute name="VALUE" x="265.43" y="73.66" size="1.27" layer="96" ratio="10" rot="R180"/>
</instance>
<instance part="SUPPLY94" gate="GND" x="271.78" y="58.42" smashed="yes">
<attribute name="VALUE" x="269.875" y="55.245" size="1.778" layer="96"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="N$116" class="0">
<segment>
<pinref part="L3" gate="G$1" pin="2"/>
<wire x1="190.5" y1="88.9" x2="193.04" y2="88.9" width="0.1524" layer="91"/>
<pinref part="C66" gate="G$1" pin="2"/>
<wire x1="193.04" y1="88.9" x2="195.58" y2="88.9" width="0.1524" layer="91"/>
<wire x1="195.58" y1="88.9" x2="200.66" y2="88.9" width="0.1524" layer="91"/>
<wire x1="200.66" y1="86.36" x2="200.66" y2="88.9" width="0.1524" layer="91"/>
<pinref part="C65" gate="G$1" pin="2"/>
<wire x1="195.58" y1="86.36" x2="195.58" y2="88.9" width="0.1524" layer="91"/>
<junction x="195.58" y="88.9"/>
<pinref part="R164" gate="G$1" pin="1"/>
<wire x1="190.5" y1="93.98" x2="193.04" y2="93.98" width="0.1524" layer="91"/>
<wire x1="193.04" y1="93.98" x2="193.04" y2="88.9" width="0.1524" layer="91"/>
<junction x="193.04" y="88.9"/>
<pinref part="U13" gate="A" pin="3"/>
<wire x1="208.28" y1="88.9" x2="200.66" y2="88.9" width="0.1524" layer="91"/>
<junction x="200.66" y="88.9"/>
</segment>
</net>
<net name="N$117" class="0">
<segment>
<pinref part="L2" gate="G$1" pin="1"/>
<pinref part="C68" gate="G$1" pin="2"/>
<wire x1="241.3" y1="88.9" x2="256.54" y2="88.9" width="0.1524" layer="91"/>
<wire x1="256.54" y1="88.9" x2="284.48" y2="88.9" width="0.1524" layer="91"/>
<wire x1="284.48" y1="88.9" x2="292.1" y2="88.9" width="0.1524" layer="91"/>
<wire x1="292.1" y1="88.9" x2="297.18" y2="88.9" width="0.1524" layer="91"/>
<wire x1="297.18" y1="88.9" x2="299.72" y2="88.9" width="0.1524" layer="91"/>
<wire x1="292.1" y1="86.36" x2="292.1" y2="88.9" width="0.1524" layer="91"/>
<pinref part="C67" gate="G$1" pin="2"/>
<wire x1="284.48" y1="86.36" x2="284.48" y2="88.9" width="0.1524" layer="91"/>
<pinref part="R99" gate="G$1" pin="2"/>
<wire x1="241.3" y1="86.36" x2="241.3" y2="88.9" width="0.1524" layer="91"/>
<junction x="284.48" y="88.9"/>
<junction x="292.1" y="88.9"/>
<pinref part="U13" gate="A" pin="2"/>
<wire x1="233.68" y1="88.9" x2="241.3" y2="88.9" width="0.1524" layer="91"/>
<junction x="241.3" y="88.9"/>
<pinref part="R165" gate="G$1" pin="2"/>
<wire x1="299.72" y1="93.98" x2="297.18" y2="93.98" width="0.1524" layer="91"/>
<wire x1="297.18" y1="93.98" x2="297.18" y2="88.9" width="0.1524" layer="91"/>
<junction x="297.18" y="88.9"/>
<pinref part="D23" gate="G$1" pin="-"/>
<wire x1="256.54" y1="86.36" x2="256.54" y2="88.9" width="0.1524" layer="91"/>
<junction x="256.54" y="88.9"/>
</segment>
</net>
<net name="N$119" class="0">
<segment>
<pinref part="R100" gate="G$1" pin="1"/>
<pinref part="R99" gate="G$1" pin="1"/>
<wire x1="241.3" y1="76.2" x2="241.3" y2="73.66" width="0.1524" layer="91"/>
<wire x1="241.3" y1="73.66" x2="241.3" y2="63.5" width="0.1524" layer="91"/>
<wire x1="241.3" y1="63.5" x2="220.98" y2="63.5" width="0.1524" layer="91"/>
<wire x1="220.98" y1="63.5" x2="218.44" y2="63.5" width="0.1524" layer="91"/>
<junction x="220.98" y="63.5"/>
<pinref part="U13" gate="A" pin="1"/>
<wire x1="220.98" y1="76.2" x2="220.98" y2="63.5" width="0.1524" layer="91"/>
<pinref part="R168" gate="G$1" pin="2"/>
<wire x1="243.84" y1="73.66" x2="241.3" y2="73.66" width="0.1524" layer="91"/>
<junction x="241.3" y="73.66"/>
<pinref part="Q17" gate="G$1" pin="E"/>
<wire x1="251.46" y1="63.5" x2="241.3" y2="63.5" width="0.1524" layer="91"/>
<junction x="241.3" y="63.5"/>
</segment>
</net>
<net name="AISG_1_VIN" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="1"/>
<wire x1="17.78" y1="149.86" x2="71.12" y2="149.86" width="0.1524" layer="91"/>
<label x="66.04" y="149.86" size="1.27" layer="95" rot="MR0"/>
<label x="71.12" y="149.86" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="D5" gate="G$1" pin="+"/>
<wire x1="137.16" y1="200.66" x2="119.38" y2="200.66" width="0.1524" layer="91"/>
<label x="119.38" y="200.66" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="AISG_1_RS485_N" class="0">
<segment>
<label x="66.04" y="144.78" size="1.27" layer="95" rot="MR0"/>
<pinref part="R89" gate="G$1" pin="1"/>
<wire x1="50.8" y1="144.78" x2="71.12" y2="144.78" width="0.1524" layer="91"/>
<label x="71.12" y="144.78" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="AISG_1_RS485_P" class="0">
<segment>
<label x="66.04" y="142.24" size="1.27" layer="95" rot="MR0"/>
<pinref part="R90" gate="G$1" pin="1"/>
<wire x1="50.8" y1="142.24" x2="71.12" y2="142.24" width="0.1524" layer="91"/>
<label x="71.12" y="142.24" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="AISG_2_VIN" class="0">
<segment>
<pinref part="J2" gate="G$1" pin="1"/>
<wire x1="17.78" y1="177.8" x2="71.12" y2="177.8" width="0.1524" layer="91"/>
<label x="66.04" y="177.8" size="1.27" layer="95" rot="MR0"/>
<label x="71.12" y="177.8" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="D4" gate="G$1" pin="+"/>
<wire x1="137.16" y1="208.28" x2="119.38" y2="208.28" width="0.1524" layer="91"/>
<label x="119.38" y="208.28" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="AISG_2_RS485_N" class="0">
<segment>
<label x="66.04" y="172.72" size="1.27" layer="95" rot="MR0"/>
<pinref part="R87" gate="G$1" pin="1"/>
<wire x1="50.8" y1="172.72" x2="71.12" y2="172.72" width="0.1524" layer="91"/>
<label x="71.12" y="172.72" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="AISG_2_RS485_P" class="0">
<segment>
<label x="66.04" y="170.18" size="1.27" layer="95" rot="MR0"/>
<pinref part="R88" gate="G$1" pin="1"/>
<wire x1="50.8" y1="170.18" x2="71.12" y2="170.18" width="0.1524" layer="91"/>
<label x="71.12" y="170.18" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="AISG_3_VIN" class="0">
<segment>
<pinref part="J3" gate="G$1" pin="1"/>
<wire x1="17.78" y1="195.58" x2="71.12" y2="195.58" width="0.1524" layer="91"/>
<label x="68.58" y="195.58" size="1.27" layer="95" rot="MR0"/>
<label x="71.12" y="195.58" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="D3" gate="G$1" pin="+"/>
<wire x1="137.16" y1="215.9" x2="119.38" y2="215.9" width="0.1524" layer="91"/>
<label x="119.38" y="215.9" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="AISG_3_RS485_N" class="0">
<segment>
<label x="68.58" y="190.5" size="1.27" layer="95" rot="MR0"/>
<pinref part="R93" gate="G$1" pin="1"/>
<wire x1="53.34" y1="190.5" x2="71.12" y2="190.5" width="0.1524" layer="91"/>
<label x="71.12" y="190.5" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="AISG_3_RS485_P" class="0">
<segment>
<label x="68.58" y="187.96" size="1.27" layer="95" rot="MR0"/>
<pinref part="R94" gate="G$1" pin="1"/>
<wire x1="53.34" y1="187.96" x2="71.12" y2="187.96" width="0.1524" layer="91"/>
<label x="71.12" y="187.96" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="AISG_4_RS485_N" class="0">
<segment>
<label x="68.58" y="220.98" size="1.27" layer="95" rot="MR0"/>
<pinref part="R95" gate="G$1" pin="1"/>
<wire x1="53.34" y1="220.98" x2="71.12" y2="220.98" width="0.1524" layer="91"/>
<label x="71.12" y="220.98" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="AISG_4_RS485_P" class="0">
<segment>
<label x="68.58" y="218.44" size="1.27" layer="95" rot="MR0"/>
<pinref part="R96" gate="G$1" pin="1"/>
<wire x1="53.34" y1="218.44" x2="71.12" y2="218.44" width="0.1524" layer="91"/>
<label x="71.12" y="218.44" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$106" class="0">
<segment>
<pinref part="J2" gate="G$3" pin="1"/>
<wire x1="17.78" y1="172.72" x2="33.02" y2="172.72" width="0.1524" layer="91"/>
<pinref part="R87" gate="G$1" pin="2"/>
<pinref part="R92" gate="G$1" pin="2"/>
<wire x1="33.02" y1="172.72" x2="40.64" y2="172.72" width="0.1524" layer="91"/>
<wire x1="33.02" y1="165.1" x2="33.02" y2="172.72" width="0.1524" layer="91"/>
<junction x="33.02" y="172.72"/>
</segment>
</net>
<net name="N$107" class="0">
<segment>
<pinref part="J2" gate="G$4" pin="1"/>
<wire x1="17.78" y1="170.18" x2="38.1" y2="170.18" width="0.1524" layer="91"/>
<pinref part="R88" gate="G$1" pin="2"/>
<pinref part="R91" gate="G$1" pin="2"/>
<wire x1="38.1" y1="170.18" x2="40.64" y2="170.18" width="0.1524" layer="91"/>
<wire x1="38.1" y1="165.1" x2="38.1" y2="170.18" width="0.1524" layer="91"/>
<junction x="38.1" y="170.18"/>
</segment>
</net>
<net name="N$109" class="0">
<segment>
<pinref part="J1" gate="G$4" pin="1"/>
<wire x1="17.78" y1="142.24" x2="38.1" y2="142.24" width="0.1524" layer="91"/>
<pinref part="R90" gate="G$1" pin="2"/>
<pinref part="R91" gate="G$1" pin="1"/>
<wire x1="38.1" y1="142.24" x2="40.64" y2="142.24" width="0.1524" layer="91"/>
<wire x1="38.1" y1="154.94" x2="38.1" y2="142.24" width="0.1524" layer="91"/>
<junction x="38.1" y="142.24"/>
</segment>
</net>
<net name="N$111" class="0">
<segment>
<pinref part="J1" gate="G$3" pin="1"/>
<wire x1="17.78" y1="144.78" x2="33.02" y2="144.78" width="0.1524" layer="91"/>
<pinref part="R89" gate="G$1" pin="2"/>
<pinref part="R92" gate="G$1" pin="1"/>
<wire x1="33.02" y1="144.78" x2="40.64" y2="144.78" width="0.1524" layer="91"/>
<wire x1="33.02" y1="154.94" x2="33.02" y2="144.78" width="0.1524" layer="91"/>
<junction x="33.02" y="144.78"/>
</segment>
</net>
<net name="N$112" class="0">
<segment>
<pinref part="J3" gate="G$3" pin="1"/>
<wire x1="17.78" y1="190.5" x2="38.1" y2="190.5" width="0.1524" layer="91"/>
<pinref part="R93" gate="G$1" pin="2"/>
<pinref part="R98" gate="G$1" pin="2"/>
<wire x1="38.1" y1="190.5" x2="43.18" y2="190.5" width="0.1524" layer="91"/>
<wire x1="38.1" y1="200.66" x2="38.1" y2="190.5" width="0.1524" layer="91"/>
<junction x="38.1" y="190.5"/>
</segment>
</net>
<net name="N$113" class="0">
<segment>
<pinref part="J3" gate="G$4" pin="1"/>
<wire x1="17.78" y1="187.96" x2="33.02" y2="187.96" width="0.1524" layer="91"/>
<pinref part="R94" gate="G$1" pin="2"/>
<pinref part="R97" gate="G$1" pin="2"/>
<wire x1="33.02" y1="187.96" x2="43.18" y2="187.96" width="0.1524" layer="91"/>
<wire x1="33.02" y1="200.66" x2="33.02" y2="187.96" width="0.1524" layer="91"/>
<junction x="33.02" y="187.96"/>
</segment>
</net>
<net name="N$114" class="0">
<segment>
<pinref part="J4" gate="G$3" pin="1"/>
<wire x1="17.78" y1="220.98" x2="38.1" y2="220.98" width="0.1524" layer="91"/>
<pinref part="R95" gate="G$1" pin="2"/>
<pinref part="R98" gate="G$1" pin="1"/>
<wire x1="38.1" y1="220.98" x2="43.18" y2="220.98" width="0.1524" layer="91"/>
<wire x1="38.1" y1="210.82" x2="38.1" y2="220.98" width="0.1524" layer="91"/>
<junction x="38.1" y="220.98"/>
</segment>
</net>
<net name="N$115" class="0">
<segment>
<pinref part="J4" gate="G$4" pin="1"/>
<wire x1="17.78" y1="218.44" x2="33.02" y2="218.44" width="0.1524" layer="91"/>
<pinref part="R96" gate="G$1" pin="2"/>
<pinref part="R97" gate="G$1" pin="1"/>
<wire x1="33.02" y1="218.44" x2="43.18" y2="218.44" width="0.1524" layer="91"/>
<wire x1="33.02" y1="210.82" x2="33.02" y2="218.44" width="0.1524" layer="91"/>
<junction x="33.02" y="218.44"/>
</segment>
</net>
<net name="AISG_4_VIN" class="0">
<segment>
<pinref part="D2" gate="G$1" pin="+"/>
<wire x1="137.16" y1="223.52" x2="119.38" y2="223.52" width="0.1524" layer="91"/>
<label x="119.38" y="223.52" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="J4" gate="G$1" pin="1"/>
<wire x1="17.78" y1="226.06" x2="71.12" y2="226.06" width="0.1524" layer="91"/>
<label x="68.58" y="226.06" size="1.27" layer="95" rot="MR0"/>
<label x="71.12" y="226.06" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$121" class="0">
<segment>
<pinref part="D2" gate="G$1" pin="-"/>
<wire x1="147.32" y1="223.52" x2="152.4" y2="223.52" width="0.1524" layer="91"/>
<wire x1="152.4" y1="223.52" x2="152.4" y2="215.9" width="0.1524" layer="91"/>
<pinref part="D5" gate="G$1" pin="-"/>
<wire x1="152.4" y1="215.9" x2="152.4" y2="208.28" width="0.1524" layer="91"/>
<wire x1="152.4" y1="208.28" x2="152.4" y2="200.66" width="0.1524" layer="91"/>
<wire x1="152.4" y1="200.66" x2="147.32" y2="200.66" width="0.1524" layer="91"/>
<pinref part="D4" gate="G$1" pin="-"/>
<wire x1="147.32" y1="208.28" x2="152.4" y2="208.28" width="0.1524" layer="91"/>
<junction x="152.4" y="208.28"/>
<pinref part="D3" gate="G$1" pin="-"/>
<wire x1="147.32" y1="215.9" x2="152.4" y2="215.9" width="0.1524" layer="91"/>
<junction x="152.4" y="215.9"/>
<wire x1="152.4" y1="223.52" x2="157.48" y2="223.52" width="0.1524" layer="91"/>
<junction x="152.4" y="223.52"/>
<pinref part="L4" gate="G$1" pin="1"/>
<pinref part="R127" gate="G$1" pin="2"/>
<wire x1="157.48" y1="223.52" x2="160.02" y2="223.52" width="0.1524" layer="91"/>
<wire x1="160.02" y1="228.6" x2="157.48" y2="228.6" width="0.1524" layer="91"/>
<wire x1="157.48" y1="228.6" x2="157.48" y2="223.52" width="0.1524" layer="91"/>
<junction x="157.48" y="223.52"/>
</segment>
</net>
<net name="N$123" class="0">
<segment>
<pinref part="L4" gate="G$1" pin="2"/>
<wire x1="170.18" y1="223.52" x2="172.72" y2="223.52" width="0.1524" layer="91"/>
<pinref part="R127" gate="G$1" pin="1"/>
<wire x1="170.18" y1="228.6" x2="172.72" y2="228.6" width="0.1524" layer="91"/>
<wire x1="172.72" y1="228.6" x2="172.72" y2="223.52" width="0.1524" layer="91"/>
<junction x="172.72" y="223.52"/>
<pinref part="C81" gate="G$1" pin="2"/>
<wire x1="177.8" y1="223.52" x2="172.72" y2="223.52" width="0.1524" layer="91"/>
<wire x1="177.8" y1="220.98" x2="177.8" y2="223.52" width="0.1524" layer="91"/>
<pinref part="Q3" gate="G$1" pin="D"/>
<wire x1="182.88" y1="223.52" x2="177.8" y2="223.52" width="0.1524" layer="91"/>
<junction x="177.8" y="223.52"/>
</segment>
</net>
<net name="N$142" class="0">
<segment>
<pinref part="Q2" gate="G$1" pin="S"/>
<pinref part="R125" gate="G$1" pin="2"/>
<wire x1="213.36" y1="223.52" x2="223.52" y2="223.52" width="0.1524" layer="91"/>
<wire x1="223.52" y1="223.52" x2="233.68" y2="223.52" width="0.1524" layer="91"/>
<wire x1="233.68" y1="223.52" x2="241.3" y2="223.52" width="0.1524" layer="91"/>
<wire x1="213.36" y1="220.98" x2="213.36" y2="223.52" width="0.1524" layer="91"/>
<pinref part="C82" gate="G$1" pin="2"/>
<wire x1="233.68" y1="220.98" x2="233.68" y2="223.52" width="0.1524" layer="91"/>
<junction x="233.68" y="223.52"/>
<pinref part="Q3" gate="G$1" pin="S"/>
<wire x1="193.04" y1="223.52" x2="198.12" y2="223.52" width="0.1524" layer="91"/>
<junction x="213.36" y="223.52"/>
<pinref part="D6" gate="G$1" pin="-"/>
<wire x1="198.12" y1="223.52" x2="213.36" y2="223.52" width="0.1524" layer="91"/>
<wire x1="198.12" y1="220.98" x2="198.12" y2="223.52" width="0.1524" layer="91"/>
<junction x="198.12" y="223.52"/>
<pinref part="D7" gate="G$1" pin="-"/>
<wire x1="223.52" y1="220.98" x2="223.52" y2="223.52" width="0.1524" layer="91"/>
<junction x="223.52" y="223.52"/>
</segment>
</net>
<net name="N$143" class="0">
<segment>
<pinref part="Q2" gate="G$1" pin="D"/>
<pinref part="C83" gate="G$1" pin="2"/>
<wire x1="251.46" y1="223.52" x2="256.54" y2="223.52" width="0.1524" layer="91"/>
<wire x1="256.54" y1="223.52" x2="256.54" y2="220.98" width="0.1524" layer="91"/>
<wire x1="256.54" y1="223.52" x2="264.16" y2="223.52" width="0.1524" layer="91"/>
<junction x="256.54" y="223.52"/>
<pinref part="L5" gate="G$1" pin="1"/>
<pinref part="C84" gate="G$1" pin="2"/>
<wire x1="264.16" y1="223.52" x2="269.24" y2="223.52" width="0.1524" layer="91"/>
<wire x1="264.16" y1="220.98" x2="264.16" y2="223.52" width="0.1524" layer="91"/>
<junction x="264.16" y="223.52"/>
</segment>
</net>
<net name="N$144" class="0">
<segment>
<pinref part="R125" gate="G$1" pin="1"/>
<wire x1="210.82" y1="208.28" x2="213.36" y2="208.28" width="0.1524" layer="91"/>
<wire x1="213.36" y1="208.28" x2="213.36" y2="210.82" width="0.1524" layer="91"/>
<pinref part="C82" gate="G$1" pin="1"/>
<wire x1="213.36" y1="208.28" x2="218.44" y2="208.28" width="0.1524" layer="91"/>
<wire x1="218.44" y1="208.28" x2="223.52" y2="208.28" width="0.1524" layer="91"/>
<wire x1="223.52" y1="208.28" x2="233.68" y2="208.28" width="0.1524" layer="91"/>
<wire x1="233.68" y1="208.28" x2="233.68" y2="210.82" width="0.1524" layer="91"/>
<junction x="213.36" y="208.28"/>
<pinref part="C83" gate="G$1" pin="1"/>
<wire x1="233.68" y1="208.28" x2="243.84" y2="208.28" width="0.1524" layer="91"/>
<wire x1="243.84" y1="208.28" x2="256.54" y2="208.28" width="0.1524" layer="91"/>
<wire x1="256.54" y1="208.28" x2="256.54" y2="210.82" width="0.1524" layer="91"/>
<junction x="233.68" y="208.28"/>
<pinref part="Q2" gate="G$1" pin="G"/>
<wire x1="243.84" y1="218.44" x2="243.84" y2="208.28" width="0.1524" layer="91"/>
<junction x="243.84" y="208.28"/>
<pinref part="R126" gate="G$1" pin="2"/>
<wire x1="218.44" y1="205.74" x2="218.44" y2="208.28" width="0.1524" layer="91"/>
<junction x="218.44" y="208.28"/>
<pinref part="R130" gate="G$1" pin="1"/>
<pinref part="D7" gate="G$1" pin="+"/>
<wire x1="223.52" y1="210.82" x2="223.52" y2="208.28" width="0.1524" layer="91"/>
<junction x="223.52" y="208.28"/>
<wire x1="256.54" y1="208.28" x2="264.16" y2="208.28" width="0.1524" layer="91"/>
<junction x="256.54" y="208.28"/>
<pinref part="C84" gate="G$1" pin="1"/>
<wire x1="264.16" y1="208.28" x2="264.16" y2="210.82" width="0.1524" layer="91"/>
<pinref part="C85" gate="G$1" pin="1"/>
<wire x1="264.16" y1="208.28" x2="284.48" y2="208.28" width="0.1524" layer="91"/>
<wire x1="284.48" y1="208.28" x2="284.48" y2="210.82" width="0.1524" layer="91"/>
<junction x="264.16" y="208.28"/>
<pinref part="C86" gate="G$1" pin="1"/>
<wire x1="284.48" y1="208.28" x2="292.1" y2="208.28" width="0.1524" layer="91"/>
<wire x1="292.1" y1="208.28" x2="292.1" y2="210.82" width="0.1524" layer="91"/>
<junction x="284.48" y="208.28"/>
</segment>
</net>
<net name="N$122" class="0">
<segment>
<pinref part="R126" gate="G$1" pin="1"/>
<pinref part="Q4" gate="G$1" pin="D"/>
<wire x1="218.44" y1="195.58" x2="218.44" y2="193.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$141" class="0">
<segment>
<pinref part="Q4" gate="G$1" pin="G"/>
<wire x1="213.36" y1="185.42" x2="203.2" y2="185.42" width="0.1524" layer="91"/>
<pinref part="R129" gate="G$1" pin="1"/>
<wire x1="205.74" y1="180.34" x2="203.2" y2="180.34" width="0.1524" layer="91"/>
<wire x1="203.2" y1="180.34" x2="203.2" y2="185.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="R129" gate="G$1" pin="2"/>
<wire x1="215.9" y1="180.34" x2="218.44" y2="180.34" width="0.1524" layer="91"/>
<pinref part="Q4" gate="G$1" pin="S"/>
<wire x1="218.44" y1="180.34" x2="218.44" y2="182.88" width="0.1524" layer="91"/>
<wire x1="218.44" y1="180.34" x2="218.44" y2="177.8" width="0.1524" layer="91"/>
<junction x="218.44" y="180.34"/>
<pinref part="SUPPLY63" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="R100" gate="G$1" pin="2"/>
<wire x1="208.28" y1="63.5" x2="205.74" y2="63.5" width="0.1524" layer="91"/>
<wire x1="205.74" y1="63.5" x2="205.74" y2="60.96" width="0.1524" layer="91"/>
<pinref part="SUPPLY64" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C65" gate="G$1" pin="1"/>
<pinref part="SUPPLY65" gate="GND" pin="GND"/>
<wire x1="195.58" y1="76.2" x2="195.58" y2="73.66" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUPPLY66" gate="GND" pin="GND"/>
<pinref part="C66" gate="G$1" pin="1"/>
<wire x1="200.66" y1="73.66" x2="200.66" y2="76.2" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C67" gate="G$1" pin="1"/>
<pinref part="SUPPLY67" gate="GND" pin="GND"/>
<wire x1="284.48" y1="76.2" x2="284.48" y2="73.66" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C68" gate="G$1" pin="1"/>
<pinref part="SUPPLY68" gate="GND" pin="GND"/>
<wire x1="292.1" y1="76.2" x2="292.1" y2="73.66" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R128" gate="G$1" pin="1"/>
<wire x1="185.42" y1="195.58" x2="185.42" y2="193.04" width="0.1524" layer="91"/>
<pinref part="SUPPLY69" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="J4" gate="G$2" pin="1"/>
<wire x1="17.78" y1="223.52" x2="25.4" y2="223.52" width="0.1524" layer="91"/>
<pinref part="J3" gate="G$2" pin="1"/>
<wire x1="17.78" y1="193.04" x2="22.86" y2="193.04" width="0.1524" layer="91"/>
<wire x1="22.86" y1="193.04" x2="25.4" y2="193.04" width="0.1524" layer="91"/>
<wire x1="25.4" y1="223.52" x2="25.4" y2="193.04" width="0.1524" layer="91"/>
<pinref part="J2" gate="G$2" pin="1"/>
<wire x1="17.78" y1="175.26" x2="22.86" y2="175.26" width="0.1524" layer="91"/>
<wire x1="22.86" y1="175.26" x2="25.4" y2="175.26" width="0.1524" layer="91"/>
<wire x1="22.86" y1="193.04" x2="22.86" y2="175.26" width="0.1524" layer="91"/>
<junction x="22.86" y="193.04"/>
<junction x="22.86" y="175.26"/>
<pinref part="J1" gate="G$2" pin="1"/>
<wire x1="17.78" y1="147.32" x2="22.86" y2="147.32" width="0.1524" layer="91"/>
<wire x1="22.86" y1="147.32" x2="25.4" y2="147.32" width="0.1524" layer="91"/>
<wire x1="25.4" y1="175.26" x2="25.4" y2="147.32" width="0.1524" layer="91"/>
<wire x1="22.86" y1="147.32" x2="22.86" y2="137.16" width="0.1524" layer="91"/>
<junction x="22.86" y="147.32"/>
<pinref part="SUPPLY78" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C113" gate="G$1" pin="1"/>
<wire x1="190.5" y1="142.24" x2="190.5" y2="139.7" width="0.1524" layer="91"/>
<wire x1="190.5" y1="139.7" x2="182.88" y2="139.7" width="0.1524" layer="91"/>
<wire x1="182.88" y1="139.7" x2="175.26" y2="139.7" width="0.1524" layer="91"/>
<wire x1="175.26" y1="139.7" x2="175.26" y2="142.24" width="0.1524" layer="91"/>
<pinref part="C112" gate="G$1" pin="1"/>
<wire x1="182.88" y1="142.24" x2="182.88" y2="139.7" width="0.1524" layer="91"/>
<junction x="182.88" y="139.7"/>
<wire x1="175.26" y1="139.7" x2="175.26" y2="137.16" width="0.1524" layer="91"/>
<junction x="175.26" y="139.7"/>
<pinref part="SUPPLY89" gate="GND" pin="GND"/>
<pinref part="C122" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="R161" gate="G$1" pin="1"/>
<wire x1="198.12" y1="127" x2="198.12" y2="124.46" width="0.1524" layer="91"/>
<wire x1="198.12" y1="124.46" x2="205.74" y2="124.46" width="0.1524" layer="91"/>
<pinref part="C111" gate="G$1" pin="2"/>
<wire x1="205.74" y1="124.46" x2="205.74" y2="127" width="0.1524" layer="91"/>
<pinref part="SUPPLY90" gate="GND" pin="GND"/>
<wire x1="198.12" y1="124.46" x2="198.12" y2="121.92" width="0.1524" layer="91"/>
<junction x="198.12" y="124.46"/>
</segment>
<segment>
<pinref part="R163" gate="G$1" pin="1"/>
<pinref part="SUPPLY91" gate="GND" pin="GND"/>
<wire x1="337.82" y1="121.92" x2="340.36" y2="121.92" width="0.1524" layer="91"/>
<wire x1="340.36" y1="121.92" x2="340.36" y2="119.38" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUPPLY93" gate="GND" pin="GND"/>
<wire x1="302.26" y1="142.24" x2="302.26" y2="139.7" width="0.1524" layer="91"/>
<pinref part="C121" gate="G$1" pin="1"/>
<pinref part="C123" gate="G$1" pin="1"/>
<wire x1="302.26" y1="139.7" x2="302.26" y2="137.16" width="0.1524" layer="91"/>
<wire x1="302.26" y1="139.7" x2="309.88" y2="139.7" width="0.1524" layer="91"/>
<wire x1="309.88" y1="139.7" x2="309.88" y2="142.24" width="0.1524" layer="91"/>
<junction x="302.26" y="139.7"/>
<pinref part="C124" gate="G$1" pin="1"/>
<wire x1="309.88" y1="139.7" x2="317.5" y2="139.7" width="0.1524" layer="91"/>
<wire x1="317.5" y1="139.7" x2="317.5" y2="142.24" width="0.1524" layer="91"/>
<junction x="309.88" y="139.7"/>
</segment>
<segment>
<pinref part="U22" gate="A" pin="GND"/>
<wire x1="241.3" y1="142.24" x2="246.38" y2="142.24" width="0.1524" layer="91"/>
<wire x1="246.38" y1="142.24" x2="246.38" y2="139.7" width="0.1524" layer="91"/>
<pinref part="U22" gate="A" pin="DAP"/>
<wire x1="246.38" y1="139.7" x2="246.38" y2="137.16" width="0.1524" layer="91"/>
<wire x1="241.3" y1="139.7" x2="246.38" y2="139.7" width="0.1524" layer="91"/>
<junction x="246.38" y="139.7"/>
<pinref part="SUPPLY92" gate="GND" pin="GND"/>
</segment>
<segment>
<wire x1="271.78" y1="60.96" x2="271.78" y2="63.5" width="0.1524" layer="91"/>
<pinref part="C126" gate="G$1" pin="1"/>
<wire x1="271.78" y1="63.5" x2="271.78" y2="71.12" width="0.1524" layer="91"/>
<wire x1="271.78" y1="71.12" x2="269.24" y2="71.12" width="0.1524" layer="91"/>
<pinref part="SUPPLY94" gate="GND" pin="GND"/>
<pinref part="Q17" gate="G$1" pin="C"/>
<wire x1="261.62" y1="63.5" x2="271.78" y2="63.5" width="0.1524" layer="91"/>
<junction x="271.78" y="63.5"/>
</segment>
</net>
<net name="N$145" class="0">
<segment>
<pinref part="R130" gate="G$1" pin="2"/>
<wire x1="198.12" y1="208.28" x2="200.66" y2="208.28" width="0.1524" layer="91"/>
<pinref part="D6" gate="G$1" pin="+"/>
<wire x1="198.12" y1="210.82" x2="198.12" y2="208.28" width="0.1524" layer="91"/>
<junction x="198.12" y="208.28"/>
<wire x1="190.5" y1="208.28" x2="198.12" y2="208.28" width="0.1524" layer="91"/>
<pinref part="Q3" gate="G$1" pin="G"/>
<wire x1="190.5" y1="218.44" x2="190.5" y2="208.28" width="0.1524" layer="91"/>
<junction x="190.5" y="208.28"/>
<wire x1="185.42" y1="208.28" x2="190.5" y2="208.28" width="0.1524" layer="91"/>
<pinref part="R128" gate="G$1" pin="2"/>
<wire x1="185.42" y1="205.74" x2="185.42" y2="208.28" width="0.1524" layer="91"/>
<junction x="185.42" y="208.28"/>
<pinref part="C81" gate="G$1" pin="1"/>
<wire x1="177.8" y1="210.82" x2="177.8" y2="208.28" width="0.1524" layer="91"/>
<wire x1="177.8" y1="208.28" x2="185.42" y2="208.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="AISG_VIN" class="0">
<segment>
<pinref part="L5" gate="G$1" pin="2"/>
<pinref part="C86" gate="G$1" pin="2"/>
<wire x1="279.4" y1="223.52" x2="284.48" y2="223.52" width="0.1524" layer="91"/>
<wire x1="284.48" y1="223.52" x2="292.1" y2="223.52" width="0.1524" layer="91"/>
<wire x1="292.1" y1="223.52" x2="292.1" y2="220.98" width="0.1524" layer="91"/>
<pinref part="C85" gate="G$1" pin="2"/>
<wire x1="284.48" y1="220.98" x2="284.48" y2="223.52" width="0.1524" layer="91"/>
<junction x="284.48" y="223.52"/>
<wire x1="292.1" y1="223.52" x2="299.72" y2="223.52" width="0.1524" layer="91"/>
<junction x="292.1" y="223.52"/>
<label x="299.72" y="223.52" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="149.86" y1="154.94" x2="154.94" y2="154.94" width="0.1524" layer="91"/>
<pinref part="R160" gate="G$1" pin="1"/>
<wire x1="157.48" y1="162.56" x2="154.94" y2="162.56" width="0.1524" layer="91"/>
<wire x1="154.94" y1="162.56" x2="154.94" y2="154.94" width="0.1524" layer="91"/>
<pinref part="L10" gate="G$1" pin="1"/>
<wire x1="157.48" y1="154.94" x2="154.94" y2="154.94" width="0.1524" layer="91"/>
<junction x="154.94" y="154.94"/>
<label x="149.86" y="154.94" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$176" class="0">
<segment>
<pinref part="R160" gate="G$1" pin="2"/>
<wire x1="167.64" y1="162.56" x2="170.18" y2="162.56" width="0.1524" layer="91"/>
<wire x1="170.18" y1="162.56" x2="170.18" y2="154.94" width="0.1524" layer="91"/>
<pinref part="L10" gate="G$1" pin="2"/>
<wire x1="170.18" y1="154.94" x2="167.64" y2="154.94" width="0.1524" layer="91"/>
<wire x1="170.18" y1="154.94" x2="175.26" y2="154.94" width="0.1524" layer="91"/>
<junction x="170.18" y="154.94"/>
<pinref part="C113" gate="G$1" pin="2"/>
<wire x1="175.26" y1="154.94" x2="182.88" y2="154.94" width="0.1524" layer="91"/>
<wire x1="182.88" y1="154.94" x2="190.5" y2="154.94" width="0.1524" layer="91"/>
<wire x1="190.5" y1="154.94" x2="190.5" y2="152.4" width="0.1524" layer="91"/>
<pinref part="C112" gate="G$1" pin="2"/>
<wire x1="182.88" y1="152.4" x2="182.88" y2="154.94" width="0.1524" layer="91"/>
<junction x="182.88" y="154.94"/>
<wire x1="175.26" y1="152.4" x2="175.26" y2="154.94" width="0.1524" layer="91"/>
<junction x="175.26" y="154.94"/>
<pinref part="U22" gate="A" pin="VIN"/>
<wire x1="190.5" y1="154.94" x2="210.82" y2="154.94" width="0.1524" layer="91"/>
<junction x="190.5" y="154.94"/>
<pinref part="C122" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$180" class="0">
<segment>
<pinref part="U22" gate="A" pin="RT/SYNC"/>
<pinref part="R161" gate="G$1" pin="2"/>
<wire x1="210.82" y1="144.78" x2="198.12" y2="144.78" width="0.1524" layer="91"/>
<wire x1="198.12" y1="144.78" x2="198.12" y2="137.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$182" class="0">
<segment>
<pinref part="U22" gate="A" pin="SS"/>
<pinref part="C111" gate="G$1" pin="1"/>
<wire x1="210.82" y1="139.7" x2="205.74" y2="139.7" width="0.1524" layer="91"/>
<wire x1="205.74" y1="139.7" x2="205.74" y2="137.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$183" class="0">
<segment>
<pinref part="C114" gate="G$1" pin="1"/>
<pinref part="R166" gate="G$1" pin="1"/>
<wire x1="256.54" y1="149.86" x2="261.62" y2="149.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$184" class="0">
<segment>
<pinref part="C114" gate="G$1" pin="2"/>
<wire x1="271.78" y1="149.86" x2="276.86" y2="149.86" width="0.1524" layer="91"/>
<pinref part="U22" gate="A" pin="SW"/>
<wire x1="276.86" y1="149.86" x2="276.86" y2="154.94" width="0.1524" layer="91"/>
<wire x1="276.86" y1="154.94" x2="241.3" y2="154.94" width="0.1524" layer="91"/>
<pinref part="L11" gate="G$1" pin="1"/>
<wire x1="276.86" y1="154.94" x2="287.02" y2="154.94" width="0.1524" layer="91"/>
<junction x="276.86" y="154.94"/>
<pinref part="D22" gate="A" pin="C"/>
<wire x1="287.02" y1="154.94" x2="289.56" y2="154.94" width="0.1524" layer="91"/>
<wire x1="287.02" y1="149.86" x2="287.02" y2="154.94" width="0.1524" layer="91"/>
<junction x="287.02" y="154.94"/>
<pinref part="R167" gate="G$1" pin="1"/>
<wire x1="276.86" y1="147.32" x2="276.86" y2="149.86" width="0.1524" layer="91"/>
<junction x="276.86" y="149.86"/>
</segment>
</net>
<net name="N$185" class="0">
<segment>
<pinref part="R162" gate="G$1" pin="1"/>
<pinref part="R163" gate="G$1" pin="2"/>
<wire x1="325.12" y1="139.7" x2="325.12" y2="121.92" width="0.1524" layer="91"/>
<pinref part="U22" gate="A" pin="FB"/>
<wire x1="325.12" y1="121.92" x2="327.66" y2="121.92" width="0.1524" layer="91"/>
<wire x1="241.3" y1="147.32" x2="254" y2="147.32" width="0.1524" layer="91"/>
<wire x1="254" y1="147.32" x2="254" y2="121.92" width="0.1524" layer="91"/>
<wire x1="254" y1="121.92" x2="276.86" y2="121.92" width="0.1524" layer="91"/>
<junction x="325.12" y="121.92"/>
<pinref part="D22" gate="A" pin="A"/>
<wire x1="276.86" y1="121.92" x2="287.02" y2="121.92" width="0.1524" layer="91"/>
<wire x1="287.02" y1="121.92" x2="325.12" y2="121.92" width="0.1524" layer="91"/>
<wire x1="287.02" y1="139.7" x2="287.02" y2="121.92" width="0.1524" layer="91"/>
<junction x="287.02" y="121.92"/>
<pinref part="C125" gate="G$1" pin="1"/>
<wire x1="276.86" y1="124.46" x2="276.86" y2="121.92" width="0.1524" layer="91"/>
<junction x="276.86" y="121.92"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<pinref part="L11" gate="G$1" pin="2"/>
<wire x1="299.72" y1="154.94" x2="302.26" y2="154.94" width="0.1524" layer="91"/>
<wire x1="302.26" y1="154.94" x2="302.26" y2="152.4" width="0.1524" layer="91"/>
<pinref part="R162" gate="G$1" pin="2"/>
<wire x1="325.12" y1="149.86" x2="325.12" y2="154.94" width="0.1524" layer="91"/>
<wire x1="325.12" y1="154.94" x2="317.5" y2="154.94" width="0.1524" layer="91"/>
<junction x="302.26" y="154.94"/>
<wire x1="317.5" y1="154.94" x2="309.88" y2="154.94" width="0.1524" layer="91"/>
<wire x1="309.88" y1="154.94" x2="302.26" y2="154.94" width="0.1524" layer="91"/>
<wire x1="325.12" y1="154.94" x2="330.2" y2="154.94" width="0.1524" layer="91"/>
<junction x="325.12" y="154.94"/>
<label x="330.2" y="154.94" size="1.27" layer="95" xref="yes"/>
<pinref part="C121" gate="G$1" pin="2"/>
<pinref part="C123" gate="G$1" pin="2"/>
<wire x1="309.88" y1="152.4" x2="309.88" y2="154.94" width="0.1524" layer="91"/>
<junction x="309.88" y="154.94"/>
<pinref part="C124" gate="G$1" pin="2"/>
<wire x1="317.5" y1="152.4" x2="317.5" y2="154.94" width="0.1524" layer="91"/>
<junction x="317.5" y="154.94"/>
</segment>
<segment>
<pinref part="L3" gate="G$1" pin="1"/>
<wire x1="180.34" y1="88.9" x2="177.8" y2="88.9" width="0.1524" layer="91"/>
<pinref part="R164" gate="G$1" pin="2"/>
<wire x1="177.8" y1="88.9" x2="172.72" y2="88.9" width="0.1524" layer="91"/>
<wire x1="180.34" y1="93.98" x2="177.8" y2="93.98" width="0.1524" layer="91"/>
<wire x1="177.8" y1="93.98" x2="177.8" y2="88.9" width="0.1524" layer="91"/>
<junction x="177.8" y="88.9"/>
<label x="172.72" y="88.9" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<pinref part="+3V8" gate="G$1" pin="+3V3"/>
<pinref part="R165" gate="G$1" pin="1"/>
<wire x1="309.88" y1="93.98" x2="312.42" y2="93.98" width="0.1524" layer="91"/>
<wire x1="312.42" y1="93.98" x2="312.42" y2="88.9" width="0.1524" layer="91"/>
<pinref part="L2" gate="G$1" pin="2"/>
<wire x1="309.88" y1="88.9" x2="312.42" y2="88.9" width="0.1524" layer="91"/>
<wire x1="312.42" y1="88.9" x2="317.5" y2="88.9" width="0.1524" layer="91"/>
<junction x="312.42" y="88.9"/>
<label x="322.58" y="88.9" size="1.27" layer="95" xref="yes"/>
<wire x1="317.5" y1="88.9" x2="322.58" y2="88.9" width="0.1524" layer="91"/>
<wire x1="317.5" y1="101.6" x2="317.5" y2="88.9" width="0.1524" layer="91"/>
<junction x="317.5" y="88.9"/>
</segment>
</net>
<net name="N$186" class="0">
<segment>
<pinref part="R166" gate="G$1" pin="2"/>
<pinref part="U22" gate="A" pin="BOOT"/>
<wire x1="241.3" y1="152.4" x2="243.84" y2="152.4" width="0.1524" layer="91"/>
<wire x1="243.84" y1="152.4" x2="243.84" y2="149.86" width="0.1524" layer="91"/>
<wire x1="243.84" y1="149.86" x2="246.38" y2="149.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$187" class="0">
<segment>
<pinref part="C125" gate="G$1" pin="2"/>
<pinref part="R167" gate="G$1" pin="2"/>
<wire x1="276.86" y1="134.62" x2="276.86" y2="137.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$188" class="0">
<segment>
<pinref part="D23" gate="G$1" pin="+"/>
<wire x1="256.54" y1="76.2" x2="256.54" y2="73.66" width="0.1524" layer="91"/>
<pinref part="R168" gate="G$1" pin="1"/>
<wire x1="256.54" y1="73.66" x2="254" y2="73.66" width="0.1524" layer="91"/>
<pinref part="Q17" gate="G$1" pin="B"/>
<wire x1="256.54" y1="73.66" x2="256.54" y2="71.12" width="0.1524" layer="91"/>
<junction x="256.54" y="73.66"/>
<pinref part="C126" gate="G$1" pin="2"/>
<wire x1="256.54" y1="71.12" x2="256.54" y2="68.58" width="0.1524" layer="91"/>
<wire x1="259.08" y1="71.12" x2="256.54" y2="71.12" width="0.1524" layer="91"/>
<junction x="256.54" y="71.12"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
