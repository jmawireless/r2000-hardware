<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.4.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting keepoldvectorfont="yes"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="60" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="2" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="5" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="no" active="no"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="no" active="no"/>
<layer number="103" name="tMap" color="7" fill="1" visible="no" active="no"/>
<layer number="104" name="Name" color="7" fill="1" visible="no" active="no"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="no" active="no"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="no" active="no"/>
<layer number="107" name="Crop" color="7" fill="1" visible="no" active="no"/>
<layer number="108" name="fp8" color="7" fill="1" visible="yes" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="yes" active="yes"/>
<layer number="110" name="110" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="111" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="7" fill="1" visible="yes" active="yes"/>
<layer number="114" name="FRNTMAAT1" color="7" fill="1" visible="no" active="no"/>
<layer number="115" name="FRNTMAAT2" color="7" fill="1" visible="no" active="no"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="no" active="no"/>
<layer number="117" name="BACKMAAT1" color="7" fill="1" visible="no" active="no"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="yes" active="yes"/>
<layer number="119" name="KAP_TEKEN" color="7" fill="1" visible="no" active="no"/>
<layer number="120" name="KAP_MAAT1" color="7" fill="1" visible="no" active="no"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="no" active="no"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="no" active="no"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="top_silk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="130" name="SMDSTROOK" color="7" fill="1" visible="no" active="no"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="133" name="bottom_silk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="DrillLegend" color="7" fill="1" visible="no" active="no"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="14" fill="1" visible="no" active="no"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="no"/>
<layer number="201" name="201bmp" color="2" fill="1" visible="no" active="no"/>
<layer number="202" name="202bmp" color="3" fill="1" visible="no" active="no"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="no" active="no"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="no" active="no"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="no" active="no"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="no" active="no"/>
<layer number="207" name="207bmp" color="15" fill="10" visible="no" active="no"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="no" active="no"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="Eagle3D_PG1" color="7" fill="1" visible="no" active="no"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="no"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="OrgLBR" color="13" fill="1" visible="no" active="no"/>
<layer number="255" name="Accent" color="7" fill="1" visible="no" active="no"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="JMA_LBR">
<packages>
<package name="RGT16_1P45X1P45">
<smd name="1" x="-1.475" y="0.75" dx="0.28" dy="0.85" layer="1" rot="R270"/>
<smd name="2" x="-1.475" y="0.25" dx="0.28" dy="0.85" layer="1" rot="R270"/>
<smd name="3" x="-1.475" y="-0.25" dx="0.28" dy="0.85" layer="1" rot="R270"/>
<smd name="4" x="-1.475" y="-0.75" dx="0.28" dy="0.85" layer="1" rot="R270"/>
<smd name="5" x="-0.7501875" y="-1.475" dx="0.28" dy="0.85" layer="1" rot="R180"/>
<smd name="6" x="-0.2500625" y="-1.475" dx="0.28" dy="0.85" layer="1" rot="R180"/>
<smd name="7" x="0.2500625" y="-1.475" dx="0.28" dy="0.85" layer="1" rot="R180"/>
<smd name="8" x="0.7501875" y="-1.475" dx="0.28" dy="0.85" layer="1" rot="R180"/>
<smd name="9" x="1.475" y="-0.75" dx="0.28" dy="0.85" layer="1" rot="R270"/>
<smd name="10" x="1.475" y="-0.25" dx="0.28" dy="0.85" layer="1" rot="R270"/>
<smd name="11" x="1.475" y="0.25" dx="0.28" dy="0.85" layer="1" rot="R270"/>
<smd name="12" x="1.475" y="0.75" dx="0.28" dy="0.85" layer="1" rot="R270"/>
<smd name="13" x="0.7501875" y="1.475" dx="0.28" dy="0.85" layer="1" rot="R180"/>
<smd name="14" x="0.2500625" y="1.475" dx="0.28" dy="0.85" layer="1" rot="R180"/>
<smd name="15" x="-0.2500625" y="1.475" dx="0.28" dy="0.85" layer="1" rot="R180"/>
<smd name="16" x="-0.7501875" y="1.475" dx="0.28" dy="0.85" layer="1" rot="R180"/>
<smd name="17" x="0" y="0" dx="1.25" dy="1.25" layer="1"/>
<wire x1="-1.9558" y1="-1.9558" x2="-1.2192" y2="-1.9558" width="0.1524" layer="21"/>
<wire x1="1.9558" y1="-1.9558" x2="1.9558" y2="-1.2192" width="0.1524" layer="21"/>
<wire x1="1.9558" y1="1.9558" x2="1.2192" y2="1.9558" width="0.1524" layer="21"/>
<wire x1="-1.9558" y1="-1.2192" x2="-1.9558" y2="-1.9558" width="0.1524" layer="21"/>
<wire x1="1.2192" y1="-1.9558" x2="1.9558" y2="-1.9558" width="0.1524" layer="21"/>
<wire x1="1.9558" y1="1.2192" x2="1.9558" y2="1.9558" width="0.1524" layer="21"/>
<wire x1="-1.2192" y1="1.9558" x2="-1.9558" y2="1.9558" width="0.1524" layer="21"/>
<text x="-2.54" y="3.429" size="1.27" layer="25" ratio="6" rot="SR0">&gt;Name</text>
<rectangle x1="-2.0066" y1="2.0828" x2="-1.4986" y2="2.5908" layer="21"/>
<wire x1="-1.9558" y1="1.9558" x2="-1.9558" y2="1.2192" width="0.1524" layer="21"/>
<wire x1="-1.2192" y1="1.9558" x2="-1.9558" y2="1.9558" width="0.1524" layer="51"/>
<wire x1="-1.9558" y1="1.9558" x2="-1.9558" y2="1.2192" width="0.1524" layer="51"/>
<wire x1="1.9558" y1="1.9558" x2="1.2192" y2="1.9558" width="0.1524" layer="51"/>
<wire x1="1.9558" y1="1.2192" x2="1.9558" y2="1.9558" width="0.1524" layer="51"/>
<wire x1="1.9558" y1="-1.9558" x2="1.9558" y2="-1.2192" width="0.1524" layer="51"/>
<wire x1="1.2192" y1="-1.9558" x2="1.9558" y2="-1.9558" width="0.1524" layer="51"/>
<wire x1="-1.9558" y1="-1.9558" x2="-1.2192" y2="-1.9558" width="0.1524" layer="51"/>
<wire x1="-1.9558" y1="-1.2192" x2="-1.9558" y2="-1.9558" width="0.1524" layer="51"/>
<rectangle x1="-2.0066" y1="2.0828" x2="-1.4986" y2="2.5908" layer="51"/>
</package>
<package name="CAPC1005X55N">
<wire x1="-0.48" y1="-0.93" x2="-0.48" y2="0.93" width="0.127" layer="21"/>
<wire x1="-0.48" y1="0.93" x2="0.48" y2="0.93" width="0.127" layer="21"/>
<wire x1="0.48" y1="0.93" x2="0.48" y2="-0.93" width="0.127" layer="21"/>
<wire x1="0.48" y1="-0.93" x2="-0.48" y2="-0.93" width="0.127" layer="21" style="shortdash"/>
<smd name="1" x="0" y="0.45" dx="0.62" dy="0.62" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-0.45" dx="0.62" dy="0.62" layer="1" roundness="25" rot="R270"/>
<text x="0.635" y="1.27" size="0.889" layer="25" ratio="11" rot="R270">&gt;NAME</text>
<text x="-1.524" y="1.397" size="0.635" layer="27" font="vector" ratio="10" rot="R270">&gt;VALUE</text>
<wire x1="-0.48" y1="-0.93" x2="-0.48" y2="0.93" width="0.127" layer="51"/>
<wire x1="-0.48" y1="0.93" x2="0.48" y2="0.93" width="0.127" layer="51"/>
<wire x1="0.48" y1="0.93" x2="0.48" y2="-0.93" width="0.127" layer="51"/>
<wire x1="0.48" y1="-0.93" x2="-0.48" y2="-0.93" width="0.127" layer="51" style="shortdash"/>
</package>
<package name="CAPC1608X55N">
<wire x1="0.675" y1="1.47" x2="0.675" y2="-1.47" width="0.127" layer="21"/>
<wire x1="0.675" y1="-1.47" x2="-0.675" y2="-1.47" width="0.127" layer="21"/>
<wire x1="-0.675" y1="-1.47" x2="-0.675" y2="1.47" width="0.127" layer="21"/>
<wire x1="-0.675" y1="1.47" x2="0.675" y2="1.47" width="0.127" layer="21"/>
<smd name="1" x="0" y="0.762" dx="0.9" dy="0.95" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-0.762" dx="0.9" dy="0.95" layer="1" roundness="25" rot="R270"/>
<text x="0.889" y="1.27" size="0.889" layer="25" ratio="11" rot="R270">&gt;NAME</text>
<text x="-1.5875" y="1.27" size="0.635" layer="27" ratio="10" rot="R270">&gt;VALUE</text>
<rectangle x1="-1.27" y1="-0.508" x2="1.27" y2="0.508" layer="39" rot="R270"/>
<wire x1="0.675" y1="1.47" x2="0.675" y2="-1.47" width="0.127" layer="51"/>
<wire x1="0.675" y1="-1.47" x2="-0.675" y2="-1.47" width="0.127" layer="51"/>
<wire x1="-0.675" y1="-1.47" x2="-0.675" y2="1.47" width="0.127" layer="51"/>
<wire x1="-0.675" y1="1.47" x2="0.675" y2="1.47" width="0.127" layer="51"/>
</package>
<package name="CAPC2013X145N">
<wire x1="0.925" y1="1.72" x2="0.925" y2="-1.72" width="0.127" layer="21"/>
<wire x1="0.925" y1="-1.72" x2="-0.925" y2="-1.72" width="0.127" layer="21"/>
<wire x1="-0.925" y1="-1.72" x2="-0.925" y2="1.72" width="0.127" layer="21"/>
<wire x1="-0.925" y1="1.72" x2="0.925" y2="1.72" width="0.127" layer="21"/>
<smd name="1" x="0" y="0.9" dx="1.15" dy="1.45" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-0.9" dx="1.15" dy="1.45" layer="1" roundness="25" rot="R270"/>
<text x="2.111" y="-2.02" size="0.889" layer="25" ratio="11" rot="R90">&gt;NAME</text>
<text x="-1.1625" y="-1.52" size="0.635" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.27" y1="-0.508" x2="1.27" y2="0.508" layer="39" rot="R270"/>
<wire x1="0.925" y1="1.72" x2="0.925" y2="-1.72" width="0.127" layer="51"/>
<wire x1="0.925" y1="-1.72" x2="-0.925" y2="-1.72" width="0.127" layer="51"/>
<wire x1="-0.925" y1="-1.72" x2="-0.925" y2="1.72" width="0.127" layer="51"/>
<wire x1="-0.925" y1="1.72" x2="0.925" y2="1.72" width="0.127" layer="51"/>
</package>
<package name="CAPC3216X190N">
<wire x1="1.27" y1="2.54" x2="1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="1.27" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-2.54" x2="-1.27" y2="2.54" width="0.127" layer="21"/>
<wire x1="-1.27" y1="2.54" x2="1.27" y2="2.54" width="0.127" layer="21"/>
<smd name="1" x="0" y="1.5" dx="1.15" dy="1.8" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-1.5" dx="1.15" dy="1.8" layer="1" roundness="25" rot="R270"/>
<text x="2.361" y="-2.02" size="0.889" layer="25" ratio="11" rot="R90">&gt;NAME</text>
<text x="-1.4125" y="-1.52" size="0.635" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.27" y1="-0.75" x2="1.27" y2="0.75" layer="39" rot="R270"/>
<wire x1="0.925" y1="1.72" x2="0.925" y2="-1.72" width="0.127" layer="51"/>
<wire x1="0.925" y1="-1.72" x2="-0.925" y2="-1.72" width="0.127" layer="51"/>
<wire x1="-0.925" y1="-1.72" x2="-0.925" y2="1.72" width="0.127" layer="51"/>
<wire x1="-0.925" y1="1.72" x2="0.925" y2="1.72" width="0.127" layer="51"/>
</package>
<package name="CAPC4532X279N">
<wire x1="2.175" y1="3.22" x2="2.175" y2="-3.22" width="0.127" layer="21"/>
<wire x1="2.175" y1="-3.22" x2="-2.175" y2="-3.22" width="0.127" layer="21"/>
<wire x1="-2.175" y1="-3.22" x2="-2.175" y2="3.22" width="0.127" layer="21"/>
<wire x1="-2.175" y1="3.22" x2="2.175" y2="3.22" width="0.127" layer="21"/>
<smd name="1" x="0" y="2.05" dx="1.4" dy="3.4" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-2.05" dx="1.4" dy="3.4" layer="1" roundness="25" rot="R270"/>
<text x="3.361" y="-2.02" size="0.889" layer="25" ratio="11" rot="R90">&gt;NAME</text>
<text x="-2.6625" y="-1.52" size="0.635" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-2.264" y1="-1.494" x2="2.256" y2="1.514" layer="39" rot="R270"/>
<wire x1="1.56" y1="2.331" x2="1.56" y2="-2.355" width="0.127" layer="51"/>
<wire x1="1.56" y1="-2.355" x2="-1.56" y2="-2.355" width="0.127" layer="51"/>
<wire x1="-1.56" y1="-2.355" x2="-1.56" y2="2.331" width="0.127" layer="51"/>
<wire x1="-1.56" y1="2.331" x2="1.56" y2="2.331" width="0.127" layer="51"/>
</package>
<package name="RESC1005X40N">
<wire x1="-0.48" y1="-0.94" x2="-0.48" y2="0.94" width="0.127" layer="21"/>
<wire x1="-0.48" y1="0.94" x2="0.48" y2="0.94" width="0.127" layer="21"/>
<wire x1="0.48" y1="0.94" x2="0.48" y2="-0.94" width="0.127" layer="21"/>
<wire x1="0.48" y1="-0.94" x2="-0.48" y2="-0.94" width="0.127" layer="21" style="shortdash"/>
<smd name="1" x="0" y="0.48" dx="0.57" dy="0.62" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-0.48" dx="0.57" dy="0.62" layer="1" roundness="25" rot="R270"/>
<text x="0.635" y="1.27" size="0.889" layer="25" ratio="11" rot="R270">&gt;NAME</text>
<text x="-1.524" y="1.397" size="0.635" layer="27" font="vector" ratio="10" rot="R270">&gt;VALUE</text>
<wire x1="-0.48" y1="-0.93" x2="-0.48" y2="0.93" width="0.127" layer="51"/>
<wire x1="-0.48" y1="0.93" x2="0.48" y2="0.93" width="0.127" layer="51"/>
<wire x1="0.48" y1="0.93" x2="0.48" y2="-0.93" width="0.127" layer="51"/>
<wire x1="0.48" y1="-0.93" x2="-0.48" y2="-0.93" width="0.127" layer="51" style="shortdash"/>
</package>
<package name="RESC1608X50N">
<wire x1="0.675" y1="1.47" x2="0.675" y2="-1.47" width="0.127" layer="21"/>
<wire x1="0.675" y1="-1.47" x2="-0.675" y2="-1.47" width="0.127" layer="21"/>
<wire x1="-0.675" y1="-1.47" x2="-0.675" y2="1.47" width="0.127" layer="21"/>
<wire x1="-0.675" y1="1.47" x2="0.675" y2="1.47" width="0.127" layer="21"/>
<smd name="1" x="0" y="0.8" dx="0.95" dy="1" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-0.8" dx="0.95" dy="1" layer="1" roundness="25" rot="R270"/>
<text x="0.889" y="1.27" size="0.889" layer="25" ratio="11" rot="R270">&gt;NAME</text>
<text x="-1.5875" y="1.27" size="0.635" layer="27" ratio="10" rot="R270">&gt;VALUE</text>
<rectangle x1="-1.27" y1="-0.508" x2="1.27" y2="0.508" layer="39" rot="R270"/>
<wire x1="0.675" y1="1.47" x2="0.675" y2="-1.47" width="0.127" layer="51"/>
<wire x1="0.675" y1="-1.47" x2="-0.675" y2="-1.47" width="0.127" layer="51"/>
<wire x1="-0.675" y1="-1.47" x2="-0.675" y2="1.47" width="0.127" layer="51"/>
<wire x1="-0.675" y1="1.47" x2="0.675" y2="1.47" width="0.127" layer="51"/>
</package>
<package name="RESC2013X70N">
<wire x1="1" y1="1.8" x2="1" y2="-1.8" width="0.127" layer="21"/>
<wire x1="1" y1="-1.8" x2="-1" y2="-1.8" width="0.127" layer="21"/>
<wire x1="-1" y1="-1.8" x2="-1" y2="1.8" width="0.127" layer="21"/>
<wire x1="-1" y1="1.8" x2="1" y2="1.8" width="0.127" layer="21"/>
<smd name="1" x="0" y="0.95" dx="1.05" dy="1.4" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-0.95" dx="1.05" dy="1.4" layer="1" roundness="25" rot="R270"/>
<text x="1.511" y="-1.47" size="0.4064" layer="25" ratio="11" rot="R90">&gt;NAME</text>
<text x="-1.2125" y="-1.47" size="0.4064" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.27" y1="-0.508" x2="1.27" y2="0.508" layer="39" rot="R270"/>
<wire x1="0.675" y1="1.47" x2="0.675" y2="-1.47" width="0.127" layer="51"/>
<wire x1="0.675" y1="-1.47" x2="-0.675" y2="-1.47" width="0.127" layer="51"/>
<wire x1="-0.675" y1="-1.47" x2="-0.675" y2="1.47" width="0.127" layer="51"/>
<wire x1="-0.675" y1="1.47" x2="0.675" y2="1.47" width="0.127" layer="51"/>
</package>
<package name="RESC3216X84N">
<wire x1="1.275" y1="2.27" x2="1.275" y2="-2.27" width="0.127" layer="21"/>
<wire x1="1.275" y1="-2.27" x2="-1.275" y2="-2.27" width="0.127" layer="21"/>
<wire x1="-1.275" y1="-2.27" x2="-1.275" y2="2.27" width="0.127" layer="21"/>
<wire x1="-1.275" y1="2.27" x2="1.275" y2="2.27" width="0.127" layer="21"/>
<smd name="1" x="0" y="1.5" dx="1.05" dy="1.75" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-1.5" dx="1.05" dy="1.75" layer="1" roundness="25" rot="R270"/>
<text x="1.95" y="-2" size="0.4064" layer="25" ratio="11" rot="R90">&gt;NAME</text>
<text x="-1.5" y="-2" size="0.4064" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.27" y1="-0.75" x2="1.27" y2="0.75" layer="39" rot="R270"/>
<wire x1="0.675" y1="1.47" x2="0.675" y2="-1.47" width="0.127" layer="51"/>
<wire x1="0.675" y1="-1.47" x2="-0.675" y2="-1.47" width="0.127" layer="51"/>
<wire x1="-0.675" y1="-1.47" x2="-0.675" y2="1.47" width="0.127" layer="51"/>
<wire x1="-0.675" y1="1.47" x2="0.675" y2="1.47" width="0.127" layer="51"/>
</package>
<package name="RES2013X38N">
<wire x1="0.925" y1="1.72" x2="0.925" y2="-1.72" width="0.127" layer="21"/>
<wire x1="0.925" y1="-1.72" x2="-0.925" y2="-1.72" width="0.127" layer="21"/>
<wire x1="-0.925" y1="-1.72" x2="-0.925" y2="1.72" width="0.127" layer="21"/>
<wire x1="-0.925" y1="1.72" x2="0.925" y2="1.72" width="0.127" layer="21"/>
<smd name="1" x="0" y="1" dx="0.95" dy="1.45" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-1" dx="0.95" dy="1.45" layer="1" roundness="25" rot="R270"/>
<text x="2" y="-2" size="0.889" layer="25" ratio="11" rot="R90">&gt;NAME</text>
<text x="-1" y="-2" size="0.635" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.27" y1="-0.508" x2="1.27" y2="0.508" layer="39" rot="R270"/>
<wire x1="0.925" y1="1.72" x2="0.925" y2="-1.72" width="0.127" layer="51"/>
<wire x1="0.925" y1="-1.72" x2="-0.925" y2="-1.72" width="0.127" layer="51"/>
<wire x1="-0.925" y1="-1.72" x2="-0.925" y2="1.72" width="0.127" layer="51"/>
<wire x1="-0.925" y1="1.72" x2="0.925" y2="1.72" width="0.127" layer="51"/>
</package>
<package name="RESC5325X84N">
<wire x1="1.775" y1="3.52" x2="1.775" y2="-3.52" width="0.127" layer="21"/>
<wire x1="1.775" y1="-3.52" x2="-1.775" y2="-3.52" width="0.127" layer="21"/>
<wire x1="-1.775" y1="-3.52" x2="-1.775" y2="3.52" width="0.127" layer="21"/>
<wire x1="-1.775" y1="3.52" x2="1.775" y2="3.52" width="0.127" layer="21"/>
<smd name="1" x="0" y="2.55" dx="1.1" dy="2.65" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-2.55" dx="1.1" dy="2.65" layer="1" roundness="25" rot="R270"/>
<text x="2.45" y="-2" size="0.4064" layer="25" ratio="11" rot="R90">&gt;NAME</text>
<text x="-2" y="-2" size="0.4064" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-3" y1="-1.27" x2="3" y2="1.27" layer="39" rot="R270"/>
</package>
<package name="RESC6332X65N">
<wire x1="2" y1="4" x2="2" y2="-4" width="0.127" layer="21"/>
<wire x1="2" y1="-4" x2="-2" y2="-4" width="0.127" layer="21"/>
<wire x1="-2" y1="-4" x2="-2" y2="4" width="0.127" layer="21"/>
<wire x1="-2" y1="4" x2="2" y2="4" width="0.127" layer="21"/>
<smd name="1" x="0" y="3" dx="1.25" dy="3.35" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-3" dx="1.25" dy="3.35" layer="1" roundness="25" rot="R270"/>
<text x="2" y="-2" size="0.889" layer="25" ratio="11" rot="R90">&gt;NAME</text>
<text x="-1" y="-2" size="0.635" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<wire x1="2" y1="4" x2="2" y2="-4" width="0.127" layer="51"/>
<wire x1="2" y1="-4" x2="-2" y2="-4" width="0.127" layer="51"/>
<wire x1="-2" y1="-4" x2="-2" y2="4" width="0.127" layer="51"/>
<wire x1="-2" y1="4" x2="2" y2="4" width="0.127" layer="51"/>
<rectangle x1="-1.5" y1="-3" x2="1.5" y2="3" layer="39"/>
</package>
<package name="DO-214AB,SMC">
<smd name="P$1" x="-3.272" y="0" dx="1.54" dy="3.14" layer="1"/>
<smd name="P$2" x="3.378" y="0" dx="1.54" dy="3.14" layer="1"/>
<wire x1="-4.318" y1="1.8" x2="-4.318" y2="-1.8" width="0.127" layer="21"/>
<wire x1="-4.318" y1="1.8" x2="-4.318" y2="-1.8" width="0.127" layer="51"/>
<text x="-4.064" y="3.556" size="1.27" layer="25">&gt;NAME</text>
<text x="-4.064" y="-4.572" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-3.556" y1="-3.048" x2="3.594" y2="-3.048" width="0.127" layer="21"/>
<wire x1="-3.556" y1="3.202" x2="3.594" y2="3.202" width="0.127" layer="21"/>
<wire x1="-3.556" y1="2.032" x2="-3.556" y2="3.175" width="0.127" layer="21"/>
<wire x1="3.6195" y1="3.175" x2="3.6195" y2="2.032" width="0.127" layer="21"/>
<wire x1="-3.556" y1="-2.032" x2="-3.556" y2="-3.048" width="0.127" layer="21"/>
<wire x1="-3.556" y1="2.032" x2="-3.556" y2="3.175" width="0.127" layer="51"/>
<wire x1="-3.556" y1="3.202" x2="3.594" y2="3.202" width="0.127" layer="51"/>
<wire x1="3.6195" y1="3.175" x2="3.6195" y2="2.032" width="0.127" layer="51"/>
<wire x1="3.6195" y1="-3.048" x2="3.6195" y2="-2.032" width="0.127" layer="21"/>
<wire x1="-3.556" y1="-3.048" x2="3.594" y2="-3.048" width="0.127" layer="51"/>
<wire x1="-3.556" y1="-2.032" x2="-3.556" y2="-3.048" width="0.127" layer="51"/>
<wire x1="3.6195" y1="-3.048" x2="3.6195" y2="-2.032" width="0.127" layer="51"/>
</package>
<package name="SOIC127P600X173-8N">
<smd name="1" x="-2.7" y="1.905" dx="1.6" dy="0.6" layer="1"/>
<smd name="2" x="-2.7" y="0.635" dx="1.6" dy="0.6" layer="1"/>
<smd name="3" x="-2.7" y="-0.635" dx="1.6" dy="0.6" layer="1"/>
<smd name="4" x="-2.7" y="-1.905" dx="1.6" dy="0.6" layer="1"/>
<smd name="5" x="2.7" y="-1.905" dx="1.6" dy="0.6" layer="1"/>
<smd name="6" x="2.7" y="-0.635" dx="1.6" dy="0.6" layer="1"/>
<smd name="7" x="2.7" y="0.635" dx="1.6" dy="0.6" layer="1"/>
<smd name="8" x="2.7" y="1.905" dx="1.6" dy="0.6" layer="1"/>
<wire x1="2.5" y1="3" x2="2.5" y2="2.5" width="0.127" layer="21"/>
<wire x1="-2.5" y1="-2.5" x2="-2.5" y2="-3" width="0.127" layer="21"/>
<wire x1="-2.5" y1="-3" x2="2.5" y2="-3" width="0.127" layer="21"/>
<wire x1="2.5" y1="-3" x2="2.5" y2="-2.5" width="0.127" layer="21"/>
<wire x1="2.5" y1="3" x2="-2.5" y2="3" width="0.127" layer="21"/>
<wire x1="-2.5" y1="3" x2="-2.5" y2="2.5" width="0.127" layer="21"/>
<wire x1="-2.5" y1="2.5" x2="-2.5" y2="3" width="0.127" layer="51"/>
<wire x1="-2.5" y1="3" x2="2.5" y2="3" width="0.127" layer="51"/>
<wire x1="2.5" y1="3" x2="2.5" y2="2.5" width="0.127" layer="51"/>
<wire x1="-2.5" y1="-2.5" x2="-2.5" y2="-3" width="0.127" layer="51"/>
<wire x1="-2.5" y1="-3" x2="2.5" y2="-3" width="0.127" layer="51"/>
<wire x1="2.5" y1="-3" x2="2.5" y2="-2.5" width="0.127" layer="51"/>
<rectangle x1="-3.1985" y1="2.738" x2="-2.6985" y2="3.238" layer="51"/>
<rectangle x1="-3.1985" y1="2.738" x2="-2.6985" y2="3.238" layer="21"/>
<text x="-1.27" y="3.81" size="1.27" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="-1.27" y="5.08" size="1.27" layer="27" font="vector" ratio="15">&gt;VALUE</text>
<wire x1="-2.54" y1="2.54" x2="-3.81" y2="2.54" width="0.127" layer="51"/>
<wire x1="-3.81" y1="2.54" x2="-3.81" y2="-2.54" width="0.127" layer="51"/>
<wire x1="-3.81" y1="-2.54" x2="-2.54" y2="-2.54" width="0.127" layer="51"/>
<wire x1="2.54" y1="2.54" x2="3.81" y2="2.54" width="0.127" layer="51"/>
<wire x1="3.81" y1="2.54" x2="3.81" y2="-2.54" width="0.127" layer="51"/>
<wire x1="3.81" y1="-2.54" x2="2.54" y2="-2.54" width="0.127" layer="51"/>
</package>
<package name="TP_1.5MM">
<pad name="1" x="0" y="0" drill="1" diameter="1.5" rot="R180"/>
<text x="-1.905" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<rectangle x1="-0.35" y1="-0.35" x2="0.35" y2="0.35" layer="51"/>
</package>
<package name="C120H40">
<pad name="1" x="0" y="0" drill="0.6" diameter="1.2" thermals="no"/>
<text x="0.9" y="-0.1" size="0.254" layer="25">&gt;NAME</text>
</package>
<package name="C131H51">
<pad name="1" x="0" y="0" drill="0.71" diameter="1.31" thermals="no"/>
<text x="1.2" y="0.1" size="0.254" layer="25">&gt;NAME</text>
<text x="1.2" y="-0.2" size="0.254" layer="21">&gt;LABEL</text>
</package>
<package name="C406H326">
<pad name="1" x="0" y="0" drill="3.26" diameter="4.06" thermals="no"/>
<text x="2.2" y="1.1" size="0.254" layer="25">&gt;NAME</text>
<text x="2.2" y="0.8" size="0.254" layer="21">&gt;LABEL</text>
</package>
<package name="C491H411">
<pad name="1" x="0" y="0" drill="4.31" diameter="4.91" thermals="no"/>
<text x="3.2" y="1.1" size="0.254" layer="25">&gt;NAME</text>
<text x="3.2" y="0.8" size="0.254" layer="21">&gt;LABEL</text>
</package>
<package name="TP_1.5MM_PAD">
<pad name="1" x="0" y="0" drill="1" diameter="1.5" rot="R180"/>
<text x="-1.27" y="4.445" size="1.27" layer="25">&gt;NAME</text>
<rectangle x1="-0.35" y1="-0.35" x2="0.35" y2="0.35" layer="51"/>
<rectangle x1="-2" y1="-2.5" x2="8" y2="2.5" layer="1"/>
</package>
<package name="TP_1.7MM">
<pad name="1" x="0" y="0" drill="1" diameter="1.6764" rot="R180"/>
<text x="-1.905" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<rectangle x1="-0.35" y1="-0.35" x2="0.35" y2="0.35" layer="51"/>
</package>
<package name="GDT_2029-XX-SMLF">
<smd name="1" x="-3.016" y="-0.254" dx="8.25" dy="1.78" layer="1" rot="R90" thermals="no"/>
<smd name="2" x="2.824" y="-0.254" dx="8.25" dy="1.78" layer="1" rot="R90" thermals="no"/>
<wire x1="-4.116" y1="4.146" x2="3.884" y2="4.146" width="0.127" layer="21"/>
<wire x1="3.884" y1="4.146" x2="3.884" y2="-4.654" width="0.127" layer="21"/>
<wire x1="3.884" y1="-4.654" x2="-4.116" y2="-4.654" width="0.127" layer="21"/>
<wire x1="-4.116" y1="-4.654" x2="-4.116" y2="4.146" width="0.127" layer="21"/>
<wire x1="-4.116" y1="4.146" x2="3.884" y2="4.146" width="0.127" layer="51"/>
<wire x1="3.884" y1="4.146" x2="3.884" y2="-4.654" width="0.127" layer="51"/>
<wire x1="3.884" y1="-4.654" x2="-4.116" y2="-4.654" width="0.127" layer="51"/>
<wire x1="-4.116" y1="-4.654" x2="-4.116" y2="4.146" width="0.127" layer="51"/>
<text x="-4.016" y="4.746" size="1.27" layer="25">&gt;NAME</text>
<text x="-4.016" y="6.746" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="0.5MM_FIDUCIAL">
<wire x1="0" y1="1" x2="0" y2="-1" width="0.0508" layer="51"/>
<wire x1="-1" y1="0" x2="1" y2="0" width="0.0508" layer="51"/>
<circle x="0" y="0" radius="0.5" width="0" layer="1"/>
<circle x="0" y="0" radius="0.889" width="0" layer="29"/>
<circle x="0" y="0" radius="1" width="0.0508" layer="51"/>
<circle x="0" y="0" radius="1.0307" width="0" layer="41"/>
<circle x="0" y="0" radius="0.5" width="0.0508" layer="51"/>
</package>
<package name="TRANSITION_CLIP_.141">
<wire x1="1.53" y1="3.165" x2="0.006" y2="4.689" width="0" layer="20" curve="-90"/>
<wire x1="1.53" y1="3.165" x2="5.34" y2="3.165" width="0" layer="20"/>
<wire x1="5.34" y1="3.165" x2="5.975" y2="3.8" width="0" layer="20" curve="-90"/>
<wire x1="5.975" y1="3.8" x2="6.604" y2="3.165" width="0" layer="20" curve="-90"/>
<wire x1="6.604" y1="3.165" x2="6.604" y2="-3.185" width="0" layer="20"/>
<wire x1="1.534" y1="-3.185" x2="0.01" y2="-4.709" width="0" layer="20" curve="90"/>
<wire x1="1.534" y1="-3.185" x2="5.344" y2="-3.185" width="0" layer="20"/>
<wire x1="5.344" y1="-3.185" x2="5.979" y2="-3.82" width="0" layer="20" curve="90"/>
<wire x1="5.979" y1="-3.82" x2="6.604" y2="-3.185" width="0" layer="20" curve="90"/>
<smd name="1" x="10.29" y="0" dx="5.334" dy="1.7526" layer="1" cream="no"/>
<wire x1="0" y1="4.689" x2="0" y2="-4.689" width="0" layer="20"/>
<smd name="2" x="4.7625" y="0" dx="8.509" dy="12.192" layer="16" thermals="no" cream="no"/>
<rectangle x1="0.508" y1="-3.175" x2="7.112" y2="3.175" layer="41"/>
<rectangle x1="0.508" y1="-3.175" x2="7.112" y2="3.175" layer="42"/>
<polygon width="0.1524" layer="30">
<vertex x="0" y="5.08"/>
<vertex x="0" y="-5.08"/>
<vertex x="0.508" y="-5.08"/>
<vertex x="0.508" y="-4.572" curve="-90"/>
<vertex x="1.524" y="-3.556"/>
<vertex x="5.08" y="-3.556" curve="90"/>
<vertex x="5.588" y="-4.064"/>
<vertex x="6.604" y="-4.064" curve="90"/>
<vertex x="7.112" y="-3.556"/>
<vertex x="7.112" y="3.556" curve="90"/>
<vertex x="6.604" y="4.064"/>
<vertex x="5.588" y="4.064" curve="90"/>
<vertex x="5.08" y="3.556"/>
<vertex x="1.524" y="3.556" curve="-90"/>
<vertex x="0.508" y="4.572"/>
<vertex x="0.508" y="5.08"/>
</polygon>
<polygon width="0.1524" layer="29">
<vertex x="0" y="5.08"/>
<vertex x="0" y="-5.08"/>
<vertex x="0.508" y="-5.08"/>
<vertex x="0.508" y="-4.572" curve="-90"/>
<vertex x="1.524" y="-3.556"/>
<vertex x="5.08" y="-3.556" curve="90"/>
<vertex x="5.588" y="-4.064"/>
<vertex x="6.604" y="-4.064" curve="90"/>
<vertex x="7.112" y="-3.556"/>
<vertex x="7.112" y="3.556" curve="90"/>
<vertex x="6.604" y="4.064"/>
<vertex x="5.588" y="4.064" curve="90"/>
<vertex x="5.08" y="3.556"/>
<vertex x="1.524" y="3.556" curve="-90"/>
<vertex x="0.508" y="4.572"/>
<vertex x="0.508" y="5.08"/>
</polygon>
</package>
<package name="MRA08B-M">
<smd name="1" x="-2.7051" y="1.905" dx="1.905" dy="0.5842" layer="1"/>
<smd name="2" x="-2.7051" y="0.635" dx="1.905" dy="0.5842" layer="1"/>
<smd name="3" x="-2.7051" y="-0.635" dx="1.905" dy="0.5842" layer="1"/>
<smd name="4" x="-2.7051" y="-1.905" dx="1.905" dy="0.5842" layer="1"/>
<smd name="5" x="2.7051" y="-1.905" dx="1.905" dy="0.5842" layer="1"/>
<smd name="6" x="2.7051" y="-0.635" dx="1.905" dy="0.5842" layer="1"/>
<smd name="7" x="2.7051" y="0.635" dx="1.905" dy="0.5842" layer="1"/>
<smd name="8" x="2.7051" y="1.905" dx="1.905" dy="0.5842" layer="1"/>
<smd name="EPAD" x="0" y="0" dx="2.413" dy="3.0988" layer="1" cream="no"/>
<wire x1="-2.0066" y1="-2.4892" x2="2.0066" y2="-2.4892" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="2.4892" x2="0.2794" y2="2.4892" width="0.1524" layer="51"/>
<wire x1="0.2794" y1="2.4892" x2="-0.3048" y2="2.4892" width="0.1524" layer="51"/>
<wire x1="-0.3048" y1="2.4892" x2="-2.0066" y2="2.4892" width="0.1524" layer="51"/>
<wire x1="0.2794" y1="2.4892" x2="-0.3048" y2="2.4892" width="0.1524" layer="51" curve="-180"/>
<polygon width="0.0254" layer="31">
<vertex x="-0.9795" y="1.3224"/>
<vertex x="-0.9795" y="0.227"/>
<vertex x="0.9795" y="0.227"/>
<vertex x="0.9795" y="1.3224"/>
</polygon>
<polygon width="0.0254" layer="31">
<vertex x="-0.9795" y="-0.227"/>
<vertex x="-0.9795" y="-1.3224"/>
<vertex x="0.9795" y="-1.3224"/>
<vertex x="0.9795" y="-0.227"/>
</polygon>
<text x="-3.2766" y="-4.191" size="1.27" layer="25" ratio="6" rot="SR0">&gt;Name</text>
<rectangle x1="-3.048" y1="2.54" x2="-2.54" y2="2.9972" layer="21"/>
<wire x1="-2.0066" y1="-2.4892" x2="2.0066" y2="-2.4892" width="0.1524" layer="21"/>
<wire x1="2.0066" y1="2.4892" x2="0.2794" y2="2.4892" width="0.1524" layer="21"/>
<wire x1="0.2794" y1="2.4892" x2="-0.3048" y2="2.4892" width="0.1524" layer="21"/>
<wire x1="-0.3048" y1="2.4892" x2="-2.0066" y2="2.4892" width="0.1524" layer="21"/>
<wire x1="0.2794" y1="2.4892" x2="-0.3048" y2="2.4892" width="0.1524" layer="21" curve="-180"/>
<rectangle x1="-3.048" y1="2.54" x2="-2.54" y2="2.9972" layer="51"/>
</package>
<package name="MRA08B-L">
<smd name="1" x="-2.6035" y="1.905" dx="1.2954" dy="0.4826" layer="1"/>
<smd name="2" x="-2.6035" y="0.635" dx="1.2954" dy="0.4826" layer="1"/>
<smd name="3" x="-2.6035" y="-0.635" dx="1.2954" dy="0.4826" layer="1"/>
<smd name="4" x="-2.6035" y="-1.905" dx="1.2954" dy="0.4826" layer="1"/>
<smd name="5" x="2.6035" y="-1.905" dx="1.2954" dy="0.4826" layer="1"/>
<smd name="6" x="2.6035" y="-0.635" dx="1.2954" dy="0.4826" layer="1"/>
<smd name="7" x="2.6035" y="0.635" dx="1.2954" dy="0.4826" layer="1"/>
<smd name="8" x="2.6035" y="1.905" dx="1.2954" dy="0.4826" layer="1"/>
<smd name="EPAD" x="0" y="0" dx="2.413" dy="3.0988" layer="1" cream="no"/>
<wire x1="-2.0066" y1="1.6764" x2="-2.0066" y2="2.1336" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="2.1336" x2="-3.0988" y2="2.1336" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="2.1336" x2="-3.0988" y2="1.651" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="1.651" x2="-2.0066" y2="1.6764" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="0.4064" x2="-2.0066" y2="0.8636" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="0.8636" x2="-3.0988" y2="0.8636" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="0.8636" x2="-3.0988" y2="0.381" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="0.381" x2="-2.0066" y2="0.4064" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="-0.8636" x2="-2.0066" y2="-0.4064" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="-0.4064" x2="-3.0988" y2="-0.4064" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="-0.4064" x2="-3.0988" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="-0.889" x2="-2.0066" y2="-0.8636" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="-2.1336" x2="-2.0066" y2="-1.6764" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="-1.6764" x2="-3.0988" y2="-1.6764" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="-1.6764" x2="-3.0988" y2="-2.159" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="-2.159" x2="-2.0066" y2="-2.1336" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="-1.6764" x2="2.0066" y2="-2.1336" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="-2.1336" x2="3.0988" y2="-2.1336" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="-2.1336" x2="3.0988" y2="-1.651" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="-1.651" x2="2.0066" y2="-1.6764" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="-0.4064" x2="2.0066" y2="-0.8636" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="-0.8636" x2="3.0988" y2="-0.8636" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="-0.8636" x2="3.0988" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="-0.381" x2="2.0066" y2="-0.4064" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="0.8636" x2="2.0066" y2="0.4064" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="0.4064" x2="3.0988" y2="0.4064" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="0.4064" x2="3.0988" y2="0.889" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="0.889" x2="2.0066" y2="0.8636" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="2.1336" x2="2.0066" y2="1.6764" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="1.6764" x2="3.0988" y2="1.6764" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="1.6764" x2="3.0988" y2="2.159" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="2.159" x2="2.0066" y2="2.1336" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="-2.4892" x2="2.0066" y2="-2.4892" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="-2.4892" x2="2.0066" y2="2.4892" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="2.4892" x2="-0.3048" y2="2.4892" width="0.1524" layer="51"/>
<wire x1="-0.3048" y1="2.4892" x2="-2.0066" y2="2.4892" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="2.4892" x2="-2.0066" y2="-2.4892" width="0.1524" layer="51"/>
<wire x1="0.3048" y1="2.5146" x2="-0.3048" y2="2.4892" width="0.1524" layer="51" curve="-180"/>
<text x="-2.2098" y="1.1684" size="1.27" layer="51" ratio="6" rot="SR0">*</text>
<polygon width="0.0254" layer="31">
<vertex x="-1.1065" y="1.4494"/>
<vertex x="-1.1065" y="0.1"/>
<vertex x="1.1065" y="0.1"/>
<vertex x="1.1065" y="1.4494"/>
</polygon>
<polygon width="0.0254" layer="31">
<vertex x="-1.1065" y="-0.1"/>
<vertex x="-1.1065" y="-1.4494"/>
<vertex x="1.1065" y="-1.4494"/>
<vertex x="1.1065" y="-0.1"/>
</polygon>
<wire x1="-2.1336" y1="-2.6416" x2="2.1336" y2="-2.6416" width="0.1524" layer="21"/>
<wire x1="2.1336" y1="2.6416" x2="-2.1336" y2="2.6416" width="0.1524" layer="21"/>
<text x="-3.429" y="2.2606" size="1.27" layer="21" ratio="6" rot="SR0">*</text>
<text x="-3.2766" y="-0.635" size="1.27" layer="25" ratio="6" rot="SR0">&gt;Name</text>
</package>
<package name="DO-214AC_475X290">
<wire x1="-2.02" y1="-3.43" x2="-2.02" y2="3.43" width="0.127" layer="21"/>
<wire x1="-2.02" y1="3.43" x2="2.02" y2="3.43" width="0.127" layer="21"/>
<wire x1="2.02" y1="3.43" x2="2.02" y2="-3.43" width="0.127" layer="21"/>
<wire x1="2.02" y1="-3.43" x2="-2.02" y2="-3.43" width="0.127" layer="21" style="shortdash"/>
<smd name="1" x="0" y="2.25" dx="1.7" dy="2.5" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-2.25" dx="1.7" dy="2.5" layer="1" roundness="25" rot="R270"/>
<text x="3" y="-2" size="0.635" layer="21" ratio="11" rot="R90">&gt;NAME</text>
<text x="-2.25" y="-2" size="0.635" layer="27" font="vector" ratio="10" rot="R90">&gt;VALUE</text>
<wire x1="-2.02" y1="-3.43" x2="-2.02" y2="3.43" width="0.127" layer="21"/>
<wire x1="-2.02" y1="3.43" x2="2.02" y2="3.43" width="0.127" layer="21"/>
<wire x1="2.02" y1="3.43" x2="2.02" y2="-3.43" width="0.127" layer="21"/>
<wire x1="2.02" y1="-3.43" x2="-2.02" y2="-3.43" width="0.127" layer="21" style="shortdash"/>
<wire x1="-2" y1="-3.65" x2="2" y2="-3.65" width="0.127" layer="21"/>
<wire x1="-2" y1="-3.65" x2="2" y2="-3.65" width="0.127" layer="21"/>
<wire x1="-0.5" y1="-3.43" x2="-2.02" y2="-3.43" width="0.127" layer="51"/>
<wire x1="-2.02" y1="-3.43" x2="-2.02" y2="3.43" width="0.127" layer="51"/>
<wire x1="-2.02" y1="3.43" x2="2.02" y2="3.43" width="0.127" layer="51"/>
<wire x1="2.01" y1="3.42" x2="2.02" y2="3.42" width="0.127" layer="51"/>
<wire x1="2.02" y1="3.42" x2="2.02" y2="-3.43" width="0.127" layer="51"/>
<wire x1="2.02" y1="-3.43" x2="0.5" y2="-3.43" width="0.127" layer="51"/>
<wire x1="1.74" y1="-3.65" x2="2" y2="-3.65" width="0.127" layer="51"/>
<wire x1="1.69" y1="-3.65" x2="1.74" y2="-3.65" width="0.127" layer="51"/>
<wire x1="1.72" y1="-3.65" x2="-2" y2="-3.65" width="0.127" layer="51"/>
</package>
<package name="SOT-23-3">
<smd name="3" x="0" y="-1.1" dx="1.4" dy="1" layer="1" rot="R90"/>
<smd name="2" x="-0.95" y="1.1" dx="1.4" dy="1" layer="1" rot="R90"/>
<smd name="1" x="0.95" y="1.1" dx="1.4" dy="1" layer="1" rot="R90"/>
<wire x1="-2" y1="1" x2="-2" y2="-1" width="0.127" layer="21"/>
<wire x1="-2" y1="-1" x2="-1" y2="-1" width="0.127" layer="21"/>
<wire x1="1" y1="-1" x2="2" y2="-1" width="0.127" layer="21"/>
<wire x1="2" y1="-1" x2="2" y2="1" width="0.127" layer="21"/>
<wire x1="-0.25" y1="1" x2="0.25" y2="1" width="0.127" layer="21"/>
<wire x1="-2" y1="1" x2="-2" y2="-1" width="0.127" layer="51"/>
<wire x1="-2" y1="-1" x2="-1" y2="-1" width="0.127" layer="51"/>
<wire x1="2" y1="1" x2="2" y2="-1" width="0.127" layer="51"/>
<wire x1="2" y1="-1" x2="1" y2="-1" width="0.127" layer="51"/>
<wire x1="-0.25" y1="1" x2="0.25" y2="1" width="0.127" layer="51"/>
<text x="-2" y="2" size="1.27" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="-2" y="4" size="1.27" layer="27" font="vector" ratio="15">&gt;VALUE</text>
</package>
<package name="BLM18XXXXXXH1D">
<smd name="1" x="-0.725" y="0" dx="0.75" dy="0.7" layer="1" rot="R90"/>
<smd name="2" x="0.725" y="0" dx="0.75" dy="0.7" layer="1" rot="R90"/>
<wire x1="-1.25" y1="-0.625" x2="-1.25" y2="0.625" width="0.127" layer="21"/>
<wire x1="-1.25" y1="0.625" x2="1.25" y2="0.625" width="0.127" layer="21"/>
<wire x1="1.25" y1="0.625" x2="1.25" y2="-0.625" width="0.127" layer="21"/>
<wire x1="1.25" y1="-0.625" x2="-1.25" y2="-0.625" width="0.127" layer="21"/>
<text x="2" y="0" size="1.27" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="2" y="-2" size="1.27" layer="27" font="vector" ratio="15">&gt;VALUE</text>
</package>
<package name="DO-214AB_SMC">
<smd name="1" x="-3.5685" y="0" dx="2.794" dy="3.81" layer="1"/>
<smd name="2" x="3.5685" y="0" dx="2.794" dy="3.81" layer="1"/>
<wire x1="-4" y1="2.5" x2="-4" y2="3" width="0.127" layer="21"/>
<wire x1="-4" y1="3" x2="4" y2="3" width="0.127" layer="21"/>
<wire x1="4" y1="3" x2="4" y2="2.5" width="0.127" layer="21"/>
<wire x1="-4" y1="-2.5" x2="-4" y2="-3" width="0.127" layer="21"/>
<wire x1="-4" y1="-3" x2="4" y2="-3" width="0.127" layer="21"/>
<wire x1="4" y1="-3" x2="4" y2="-2.5" width="0.127" layer="21"/>
<wire x1="-4" y1="3" x2="4" y2="3" width="0.127" layer="51"/>
<wire x1="-4" y1="-3" x2="4" y2="-3" width="0.127" layer="51"/>
<wire x1="-4" y1="3" x2="-4" y2="2" width="0.127" layer="51"/>
<wire x1="-4" y1="2" x2="-5" y2="2" width="0.127" layer="51"/>
<wire x1="-5" y1="2" x2="-5" y2="-2" width="0.127" layer="51"/>
<wire x1="-5" y1="-2" x2="-4" y2="-2" width="0.127" layer="51"/>
<wire x1="-4" y1="-2" x2="-4" y2="-3" width="0.127" layer="51"/>
<wire x1="4" y1="3" x2="4" y2="2" width="0.127" layer="51"/>
<wire x1="4" y1="2" x2="5" y2="2" width="0.127" layer="51"/>
<wire x1="5" y1="2" x2="5" y2="-2" width="0.127" layer="51"/>
<wire x1="5" y1="-2" x2="4" y2="-2" width="0.127" layer="51"/>
<wire x1="4" y1="-2" x2="4" y2="-3" width="0.127" layer="51"/>
<wire x1="-5.5" y1="3" x2="-5.5" y2="-3" width="0.127" layer="51"/>
<wire x1="-5.5" y1="3" x2="-5.5" y2="-3" width="0.127" layer="21"/>
<text x="-5.08" y="3.81" size="0.8128" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="0" y="3.81" size="0.8128" layer="27" font="vector" ratio="15">&gt;VALUE</text>
<wire x1="-5.5" y1="3" x2="-5.5" y2="-3" width="0.127" layer="51"/>
</package>
<package name="CIR_PAD_1MM">
<text x="-1.905" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<smd name="P$1" x="0" y="0" dx="1" dy="1" layer="1" roundness="100"/>
</package>
<package name="CIR_PAD_2MM">
<text x="-1.905" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<smd name="P$1" x="0" y="0" dx="2" dy="2" layer="1" roundness="100"/>
</package>
<package name="XTAL1150X480X430N">
<wire x1="3.429" y1="-2.032" x2="5.109" y2="-1.1429" width="0.0508" layer="21" curve="55.771157"/>
<wire x1="5.715" y1="-1.1938" x2="5.715" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="-5.1091" y1="-1.143" x2="-3.429" y2="-2.032" width="0.0508" layer="21" curve="55.772485"/>
<wire x1="-5.715" y1="-1.1938" x2="-5.715" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="1.27" x2="3.429" y2="1.27" width="0.0508" layer="21"/>
<wire x1="-3.429" y1="2.032" x2="3.429" y2="2.032" width="0.0508" layer="21"/>
<wire x1="3.429" y1="-1.27" x2="-3.429" y2="-1.27" width="0.0508" layer="21"/>
<wire x1="-5.461" y1="2.413" x2="5.461" y2="2.413" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="0.381" x2="-6.477" y2="0.381" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="-0.381" x2="-6.477" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="-6.477" y1="0.381" x2="-6.477" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="2.159" x2="-5.461" y2="2.413" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.715" y1="1.1938" x2="-5.715" y2="-1.1938" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="2.159" x2="-5.715" y2="1.1938" width="0.1524" layer="21"/>
<wire x1="-3.9826" y1="1.143" x2="-3.429" y2="1.27" width="0.0508" layer="21" curve="-25.842828"/>
<wire x1="-3.9826" y1="-1.143" x2="-3.429" y2="-1.27" width="0.0508" layer="21" curve="25.842828"/>
<wire x1="-5.1091" y1="1.143" x2="-3.429" y2="2.0321" width="0.0508" layer="21" curve="-55.770993"/>
<wire x1="-3.9826" y1="1.143" x2="-3.9826" y2="-1.143" width="0.0508" layer="51" curve="128.314524"/>
<wire x1="-5.1091" y1="1.143" x2="-5.1091" y2="-1.143" width="0.0508" layer="51" curve="68.456213"/>
<wire x1="3.429" y1="2.032" x2="5.1091" y2="1.143" width="0.0508" layer="21" curve="-55.772485"/>
<wire x1="3.9826" y1="1.143" x2="3.9826" y2="-1.143" width="0.0508" layer="51" curve="-128.314524"/>
<wire x1="3.429" y1="1.27" x2="3.9826" y2="1.143" width="0.0508" layer="21" curve="-25.842828"/>
<wire x1="3.429" y1="-1.27" x2="3.9826" y2="-1.143" width="0.0508" layer="21" curve="25.842828"/>
<wire x1="6.477" y1="0.381" x2="6.477" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="5.1091" y1="1.143" x2="5.1091" y2="-1.143" width="0.0508" layer="51" curve="-68.456213"/>
<wire x1="5.715" y1="1.1938" x2="5.715" y2="0.381" width="0.1524" layer="51"/>
<wire x1="5.715" y1="0.381" x2="5.715" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="5.715" y1="-0.381" x2="5.715" y2="-1.1938" width="0.1524" layer="51"/>
<wire x1="5.715" y1="2.159" x2="5.715" y2="1.1938" width="0.1524" layer="21"/>
<wire x1="5.461" y1="2.413" x2="5.715" y2="2.159" width="0.1524" layer="21" curve="-90"/>
<wire x1="5.715" y1="0.381" x2="6.477" y2="0.381" width="0.1524" layer="51"/>
<wire x1="5.715" y1="-0.381" x2="6.477" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="3.429" y1="-2.032" x2="-3.429" y2="-2.032" width="0.0508" layer="21"/>
<wire x1="-5.461" y1="-2.413" x2="5.461" y2="-2.413" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="-2.159" x2="-5.461" y2="-2.413" width="0.1524" layer="21" curve="90"/>
<wire x1="5.461" y1="-2.413" x2="5.715" y2="-2.159" width="0.1524" layer="21" curve="90"/>
<wire x1="-0.254" y1="0.635" x2="-0.254" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="-0.635" x2="0.254" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0.254" y1="-0.635" x2="0.254" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0.635" x2="-0.254" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0.635" x2="-0.635" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0" x2="-0.635" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0" x2="-1.143" y2="0" width="0.0508" layer="21"/>
<wire x1="0.635" y1="0.635" x2="0.635" y2="0" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="0.635" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="1.143" y2="0" width="0.0508" layer="21"/>
<smd name="1" x="-4.75" y="0" dx="5.5" dy="2" layer="1"/>
<smd name="2" x="4.75" y="0" dx="5.5" dy="2" layer="1"/>
<text x="-5.715" y="2.667" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.715" y="-4.064" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="IND_SMD_5.0X5.0">
<smd name="P$1" x="-1.79" y="-0.01" dx="1.5" dy="4" layer="1"/>
<smd name="P$2" x="1.81" y="-0.01" dx="1.5" dy="4" layer="1"/>
<wire x1="-2.5" y1="2.5" x2="2.5" y2="2.5" width="0.127" layer="21"/>
<wire x1="-2.5" y1="-2.5" x2="2.5" y2="-2.5" width="0.127" layer="21"/>
<wire x1="-2.5" y1="2.5" x2="2.5" y2="2.5" width="0.127" layer="51"/>
<wire x1="-2.5" y1="-2.5" x2="2.5" y2="-2.5" width="0.127" layer="51"/>
<text x="-3.4" y="2.7" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.4" y="-4" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="RESC4532X279N">
<wire x1="2.175" y1="3.22" x2="2.175" y2="-3.22" width="0.127" layer="21"/>
<wire x1="2.175" y1="-3.22" x2="-2.175" y2="-3.22" width="0.127" layer="21"/>
<wire x1="-2.175" y1="-3.22" x2="-2.175" y2="3.22" width="0.127" layer="21"/>
<wire x1="-2.175" y1="3.22" x2="2.175" y2="3.22" width="0.127" layer="21"/>
<smd name="1" x="0" y="2.05" dx="1.4" dy="3.4" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-2.05" dx="1.4" dy="3.4" layer="1" roundness="25" rot="R270"/>
<text x="3.361" y="-2.02" size="0.889" layer="25" ratio="11" rot="R90">&gt;NAME</text>
<text x="-2.6625" y="-1.52" size="0.635" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-2.264" y1="-1.494" x2="2.256" y2="1.514" layer="39" rot="R270"/>
<wire x1="1.56" y1="2.331" x2="1.56" y2="-2.355" width="0.127" layer="51"/>
<wire x1="1.56" y1="-2.355" x2="-1.56" y2="-2.355" width="0.127" layer="51"/>
<wire x1="-1.56" y1="-2.355" x2="-1.56" y2="2.331" width="0.127" layer="51"/>
<wire x1="-1.56" y1="2.331" x2="1.56" y2="2.331" width="0.127" layer="51"/>
</package>
<package name="SOP50P490X110-10AN">
<smd name="3" x="-2.2" y="0" dx="0.3" dy="1.45" layer="1" rot="R90"/>
<smd name="2" x="-2.2" y="0.5" dx="0.3" dy="1.45" layer="1" rot="R90"/>
<smd name="1" x="-2.2" y="1" dx="0.3" dy="1.45" layer="1" rot="R90"/>
<smd name="4" x="-2.2" y="-0.5" dx="0.3" dy="1.45" layer="1" rot="R90"/>
<smd name="5" x="-2.2" y="-1" dx="0.3" dy="1.45" layer="1" rot="R90"/>
<smd name="8" x="2.2" y="0" dx="0.3" dy="1.45" layer="1" rot="R90"/>
<smd name="9" x="2.2" y="0.5" dx="0.3" dy="1.45" layer="1" rot="R90"/>
<smd name="10" x="2.2" y="1" dx="0.3" dy="1.45" layer="1" rot="R90"/>
<smd name="7" x="2.2" y="-0.5" dx="0.3" dy="1.45" layer="1" rot="R90"/>
<smd name="6" x="2.2" y="-1" dx="0.3" dy="1.45" layer="1" rot="R90"/>
<wire x1="-1.5" y1="1.5" x2="1.5" y2="1.5" width="0.127" layer="51"/>
<wire x1="-1.5" y1="-1.5" x2="1.5" y2="-1.5" width="0.127" layer="51"/>
<wire x1="-1.5" y1="1.5" x2="-1.5" y2="1.32" width="0.127" layer="51"/>
<wire x1="-1.5" y1="-1.5" x2="-1.5" y2="-1.32" width="0.127" layer="51"/>
<wire x1="1.5" y1="-1.5" x2="1.5" y2="-1.32" width="0.127" layer="51"/>
<wire x1="1.5" y1="1.5" x2="1.5" y2="1.32" width="0.127" layer="51"/>
<wire x1="-1.5" y1="1.5" x2="1.5" y2="1.5" width="0.127" layer="21"/>
<wire x1="-1.5" y1="-1.5" x2="1.5" y2="-1.5" width="0.127" layer="21"/>
<wire x1="-1.5" y1="1.5" x2="-1.5" y2="1.32" width="0.127" layer="21"/>
<wire x1="-1.5" y1="-1.5" x2="-1.5" y2="-1.32" width="0.127" layer="21"/>
<wire x1="1.5" y1="-1.5" x2="1.5" y2="-1.32" width="0.127" layer="21"/>
<wire x1="1.5" y1="1.5" x2="1.5" y2="1.32" width="0.127" layer="21"/>
<text x="-2.25" y="1.75" size="0.8128" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="-2.2" y="-2.45" size="0.8128" layer="27" font="vector" ratio="15">&gt;VALUE</text>
<circle x="-2" y="1.5" radius="0.1" width="0.127" layer="51"/>
<wire x1="-2" y1="1.5" x2="-1.9" y2="1.5" width="0.127" layer="51"/>
<circle x="-2" y="1.5" radius="0.1" width="0.127" layer="21"/>
<wire x1="-2" y1="1.5" x2="-1.9" y2="1.5" width="0.127" layer="21"/>
</package>
<package name="MRA08B">
<smd name="1" x="-2.6543" y="1.905" dx="1.6002" dy="0.5334" layer="1"/>
<smd name="2" x="-2.6543" y="0.635" dx="1.6002" dy="0.5334" layer="1"/>
<smd name="3" x="-2.6543" y="-0.635" dx="1.6002" dy="0.5334" layer="1"/>
<smd name="4" x="-2.6543" y="-1.905" dx="1.6002" dy="0.5334" layer="1"/>
<smd name="5" x="2.6543" y="-1.905" dx="1.6002" dy="0.5334" layer="1"/>
<smd name="6" x="2.6543" y="-0.635" dx="1.6002" dy="0.5334" layer="1"/>
<smd name="7" x="2.6543" y="0.635" dx="1.6002" dy="0.5334" layer="1"/>
<smd name="8" x="2.6543" y="1.905" dx="1.6002" dy="0.5334" layer="1"/>
<smd name="9" x="0" y="0" dx="2.413" dy="3.0988" layer="1" cream="no"/>
<wire x1="-2.0066" y1="1.6764" x2="-2.0066" y2="2.1336" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="2.1336" x2="-3.0988" y2="2.1336" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="2.1336" x2="-3.0988" y2="1.651" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="1.651" x2="-2.0066" y2="1.6764" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="0.4064" x2="-2.0066" y2="0.8636" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="0.8636" x2="-3.0988" y2="0.8636" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="0.8636" x2="-3.0988" y2="0.381" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="0.381" x2="-2.0066" y2="0.4064" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="-0.8636" x2="-2.0066" y2="-0.4064" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="-0.4064" x2="-3.0988" y2="-0.4064" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="-0.4064" x2="-3.0988" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="-0.889" x2="-2.0066" y2="-0.8636" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="-2.1336" x2="-2.0066" y2="-1.6764" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="-1.6764" x2="-3.0988" y2="-1.6764" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="-1.6764" x2="-3.0988" y2="-2.159" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="-2.159" x2="-2.0066" y2="-2.1336" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="-1.6764" x2="2.0066" y2="-2.1336" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="-2.1336" x2="3.0988" y2="-2.1336" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="-2.1336" x2="3.0988" y2="-1.651" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="-1.651" x2="2.0066" y2="-1.6764" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="-0.4064" x2="2.0066" y2="-0.8636" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="-0.8636" x2="3.0988" y2="-0.8636" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="-0.8636" x2="3.0988" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="-0.381" x2="2.0066" y2="-0.4064" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="0.8636" x2="2.0066" y2="0.4064" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="0.4064" x2="3.0988" y2="0.4064" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="0.4064" x2="3.0988" y2="0.889" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="0.889" x2="2.0066" y2="0.8636" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="2.1336" x2="2.0066" y2="1.6764" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="1.6764" x2="3.0988" y2="1.6764" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="1.6764" x2="3.0988" y2="2.159" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="2.159" x2="2.0066" y2="2.1336" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="-2.4892" x2="2.0066" y2="-2.4892" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="-2.4892" x2="2.0066" y2="2.4892" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="2.4892" x2="-0.3048" y2="2.4892" width="0.1524" layer="51"/>
<wire x1="-0.3048" y1="2.4892" x2="-2.0066" y2="2.4892" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="2.4892" x2="-2.0066" y2="-2.4892" width="0.1524" layer="51"/>
<wire x1="0.3048" y1="2.5146" x2="-0.3048" y2="2.4892" width="0.1524" layer="51" curve="-180"/>
<text x="-2.2098" y="1.1684" size="1.27" layer="51" ratio="6" rot="SR0"></text>
<polygon width="0.0254" layer="31">
<vertex x="-1.1065" y="1.4494"/>
<vertex x="-1.1065" y="0.1"/>
<vertex x="1.1065" y="0.1"/>
<vertex x="1.1065" y="1.4494"/>
</polygon>
<polygon width="0.0254" layer="31">
<vertex x="-1.1065" y="-0.1"/>
<vertex x="-1.1065" y="-1.4494"/>
<vertex x="1.1065" y="-1.4494"/>
<vertex x="1.1065" y="-0.1"/>
</polygon>
<wire x1="-2.1336" y1="-2.6416" x2="2.1336" y2="-2.6416" width="0.1524" layer="21"/>
<wire x1="2.1336" y1="2.6416" x2="-2.1336" y2="2.6416" width="0.1524" layer="21"/>
<text x="-3.4798" y="2.3114" size="1.27" layer="21" ratio="6" rot="SR0"></text>
<text x="-3.2766" y="-0.635" size="1.27" layer="25" ratio="6" rot="SR0">&gt;Name</text>
<rectangle x1="-3.048" y1="2.54" x2="-2.54" y2="3.048" layer="21"/>
<rectangle x1="-3.048" y1="2.54" x2="-2.54" y2="3.048" layer="51"/>
</package>
<package name="TO228P991X238-3N">
<smd name="3" x="2" y="0" dx="7.1" dy="5.55" layer="1"/>
<smd name="1" x="-4.5" y="2.28" dx="1" dy="2.15" layer="1" rot="R90"/>
<smd name="2" x="-4.5" y="-2.28" dx="1" dy="2.15" layer="1" rot="R90"/>
<wire x1="-1.6" y1="3.365" x2="4.2" y2="3.365" width="0.127" layer="21"/>
<wire x1="-1.6" y1="-3.365" x2="4.2" y2="-3.365" width="0.127" layer="21"/>
<wire x1="-1.6" y1="3.365" x2="-1.6" y2="2.95" width="0.127" layer="21"/>
<wire x1="-1.6" y1="-2.95" x2="-1.6" y2="-3.365" width="0.127" layer="21"/>
<wire x1="4.2" y1="3.365" x2="4.2" y2="2.95" width="0.127" layer="21"/>
<wire x1="4.2" y1="-2.95" x2="4.2" y2="-3.365" width="0.127" layer="21"/>
<wire x1="-1.6" y1="3.365" x2="4.2" y2="3.365" width="0.127" layer="51"/>
<wire x1="-1.6" y1="-3.365" x2="4.2" y2="-3.365" width="0.127" layer="51"/>
<wire x1="-1.6" y1="3.365" x2="-1.6" y2="2.95" width="0.127" layer="51"/>
<wire x1="-1.6" y1="-2.95" x2="-1.6" y2="-3.365" width="0.127" layer="51"/>
<wire x1="4.2" y1="3.365" x2="4.2" y2="2.95" width="0.127" layer="51"/>
<wire x1="4.2" y1="-2.95" x2="4.2" y2="-3.365" width="0.127" layer="51"/>
<text x="-1.5" y="3.5" size="1.016" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="-1.5" y="4.75" size="1.016" layer="27" font="vector" ratio="15">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="SN65HVD62RGTR">
<pin name="SYNCOUT" x="-5.08" y="22.86" visible="pad" length="middle" direction="in"/>
<pin name="TXIN" x="-5.08" y="20.32" visible="pad" length="middle" direction="in"/>
<pin name="VL" x="-5.08" y="17.78" visible="pad" length="middle" direction="in"/>
<pin name="RXOUT" x="-5.08" y="15.24" visible="pad" length="middle" direction="in"/>
<pin name="DIR" x="-5.08" y="12.7" visible="pad" length="middle" direction="in"/>
<pin name="DIRSET2" x="-5.08" y="10.16" visible="pad" length="middle" direction="in"/>
<pin name="DIRSET1" x="-5.08" y="7.62" visible="pad" length="middle" direction="in"/>
<pin name="GND_2" x="-5.08" y="5.08" visible="pad" length="middle" direction="in"/>
<pin name="RES" x="30.48" y="2.54" visible="pad" length="middle" direction="in" rot="R180"/>
<pin name="BIAS" x="30.48" y="5.08" visible="pad" length="middle" direction="in" rot="R180"/>
<pin name="RXIN" x="30.48" y="7.62" visible="pad" length="middle" direction="in" rot="R180"/>
<pin name="TXOUT" x="30.48" y="10.16" visible="pad" length="middle" direction="in" rot="R180"/>
<pin name="VCC" x="30.48" y="12.7" visible="pad" length="middle" direction="in" rot="R180"/>
<pin name="XTAL1" x="30.48" y="15.24" visible="pad" length="middle" direction="in" rot="R180"/>
<pin name="XTAL2" x="30.48" y="17.78" visible="pad" length="middle" direction="in" rot="R180"/>
<pin name="GND" x="30.48" y="20.32" visible="pad" length="middle" direction="in" rot="R180"/>
<pin name="EPAD" x="30.48" y="22.86" visible="pad" length="middle" direction="in" rot="R180"/>
<wire x1="0" y1="25.4" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="25.4" y2="0" width="0.1524" layer="94"/>
<wire x1="25.4" y1="0" x2="25.4" y2="25.4" width="0.1524" layer="94"/>
<wire x1="25.4" y1="25.4" x2="0" y2="25.4" width="0.1524" layer="94"/>
<text x="7.62" y="30.48" size="2.0828" layer="95" ratio="6" rot="SR0">&gt;Name</text>
<text x="7.62" y="27.94" size="2.0828" layer="96" ratio="6" rot="SR0">&gt;Value</text>
<text x="1.016" y="21.844" size="1.778" layer="94" ratio="15">SYNCOUT</text>
<text x="1.016" y="19.304" size="1.778" layer="94" ratio="15">TXIN</text>
<text x="1.016" y="16.764" size="1.778" layer="94" ratio="15">VL</text>
<text x="1.016" y="14.224" size="1.778" layer="94" ratio="15">RXOUT</text>
<text x="1.016" y="11.684" size="1.778" layer="94" ratio="15">DIR</text>
<text x="1.016" y="9.144" size="1.778" layer="94" ratio="15">DIRSET2</text>
<text x="1.016" y="6.604" size="1.778" layer="94" ratio="15">DIRSET1</text>
<text x="1.016" y="4.064" size="1.778" layer="94" ratio="15">GND_2</text>
<text x="24.13" y="23.876" size="1.778" layer="94" ratio="15" rot="R180">EPAD</text>
<text x="24.13" y="21.336" size="1.778" layer="94" ratio="15" rot="R180">GND</text>
<text x="24.13" y="18.796" size="1.778" layer="94" ratio="15" rot="R180">XTAL2</text>
<text x="24.13" y="16.256" size="1.778" layer="94" ratio="15" rot="R180">XTAL1</text>
<text x="24.13" y="13.462" size="1.778" layer="94" ratio="15" rot="R180">VCC</text>
<text x="24.13" y="10.922" size="1.778" layer="94" ratio="15" rot="R180">TXOUT</text>
<text x="24.13" y="8.382" size="1.778" layer="94" ratio="15" rot="R180">RXIN</text>
<text x="24.13" y="5.842" size="1.778" layer="94" ratio="15" rot="R180">BIAS</text>
<text x="24.13" y="3.302" size="1.778" layer="94" ratio="15" rot="R180">RES</text>
</symbol>
<symbol name="CAPACITOR">
<wire x1="-0.635" y1="-1.016" x2="-0.635" y2="0" width="0.254" layer="94"/>
<wire x1="-0.635" y1="0" x2="-0.635" y2="1.016" width="0.254" layer="94"/>
<wire x1="0.635" y1="1.016" x2="0.635" y2="0" width="0.254" layer="94"/>
<wire x1="0.635" y1="0" x2="0.635" y2="-1.016" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-0.635" y2="0" width="0.1524" layer="94"/>
<wire x1="0.635" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<text x="-1.27" y="1.27" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="RESISTER">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.2032" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.2032" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.2032" layer="94"/>
<text x="-1.27" y="1.4986" size="1.27" layer="95">&gt;NAME</text>
<text x="-1.27" y="-3.302" size="1.27" layer="96" distance="49">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="ZENER_BIDIRECTIONAL">
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="0" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-2.54" y2="-2.54" width="0.254" layer="94"/>
<text x="-8.89" y="1.27" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="-6.35" y="-5.08" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
<pin name="+" x="-5.08" y="0" visible="pin" length="short" direction="pas"/>
<pin name="-" x="5.08" y="0" visible="pin" length="short" direction="pas" rot="R180"/>
<wire x1="-2.54" y1="0" x2="-3.81" y2="0" width="0.15875" layer="94"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="3.81" y2="0" width="0.15875" layer="94"/>
<wire x1="0" y1="1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-1.905" width="0.254" layer="94"/>
<wire x1="0" y1="1.905" x2="0.635" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-1.905" x2="-0.635" y2="-2.54" width="0.254" layer="94"/>
</symbol>
<symbol name="CRYSTAL_2PIN">
<wire x1="1.016" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.016" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.381" y1="1.524" x2="-0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="-0.381" y1="-1.524" x2="0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="-1.524" x2="0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="1.524" x2="-0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="1.016" y1="1.778" x2="1.016" y2="-1.778" width="0.254" layer="94"/>
<wire x1="-1.016" y1="1.778" x2="-1.016" y2="-1.778" width="0.254" layer="94"/>
<text x="2.54" y="1.016" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<text x="-2.159" y="-1.143" size="0.8636" layer="93">1</text>
<text x="1.524" y="-1.143" size="0.8636" layer="93">2</text>
<pin name="2" x="2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="RS485">
<text x="1.27" y="-7.62" size="1.778" layer="94" ratio="15">R0</text>
<text x="1.27" y="-12.7" size="1.778" layer="94" ratio="15">!RE</text>
<text x="1.27" y="-17.78" size="1.778" layer="94" ratio="15">DE</text>
<text x="1.27" y="-22.86" size="1.778" layer="94" ratio="15">DI</text>
<text x="19.05" y="-22.86" size="1.778" layer="94" ratio="15" align="bottom-right">GND</text>
<text x="19.05" y="-17.78" size="1.778" layer="94" ratio="15" align="bottom-right">A(D0/RI)</text>
<text x="19.05" y="-12.7" size="1.778" layer="94" ratio="15" align="bottom-right">B(!D0!/!RI!)</text>
<text x="19.05" y="-7.62" size="1.778" layer="94" ratio="15" align="bottom-right">VCC</text>
<wire x1="0" y1="-2.54" x2="0" y2="-25.4" width="0.254" layer="94"/>
<wire x1="0" y1="-25.4" x2="20.32" y2="-25.4" width="0.254" layer="94"/>
<wire x1="20.32" y1="-25.4" x2="20.32" y2="0" width="0.254" layer="94"/>
<wire x1="20.32" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="0" y2="-2.54" width="0.254" layer="94"/>
<pin name="1" x="-2.54" y="-7.62" visible="pad" length="short"/>
<pin name="2" x="-2.54" y="-12.7" visible="pad" length="short"/>
<pin name="3" x="-2.54" y="-17.78" visible="pad" length="short"/>
<pin name="4" x="-2.54" y="-22.86" visible="pad" length="short"/>
<pin name="5" x="22.86" y="-22.86" visible="pad" length="short" rot="R180"/>
<pin name="6" x="22.86" y="-17.78" visible="pad" length="short" rot="R180"/>
<pin name="7" x="22.86" y="-12.7" visible="pad" length="short" rot="R180"/>
<pin name="8" x="22.86" y="-7.62" visible="pad" length="short" rot="R180"/>
<text x="6.35" y="-2.54" size="1.778" layer="94" ratio="15">RS485</text>
<text x="0" y="2.54" size="1.778" layer="95" ratio="15">&gt;NAME</text>
<text x="0" y="-27.94" size="1.778" layer="96" ratio="15">&gt;VALUE</text>
</symbol>
<symbol name="GDT">
<polygon width="0.254" layer="94">
<vertex x="-0.635" y="1.27"/>
<vertex x="0.635" y="1.27"/>
<vertex x="0.635" y="0.635"/>
<vertex x="-0.635" y="0.635"/>
</polygon>
<wire x1="-0.635" y1="-0.635" x2="-0.635" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-0.635" y1="-1.27" x2="0.635" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0.635" y1="-1.27" x2="0.635" y2="-0.635" width="0.254" layer="94"/>
<wire x1="0.635" y1="-0.635" x2="-0.635" y2="-0.635" width="0.254" layer="94"/>
<pin name="1" x="0" y="5.08" visible="pad" length="short" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="pad" length="short" rot="R90"/>
<text x="5.08" y="-2.54" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<wire x1="0" y1="-2.54" x2="2.54" y2="0" width="0.254" layer="94" curve="90"/>
<wire x1="2.54" y1="0" x2="0" y2="2.54" width="0.254" layer="94" curve="90"/>
<wire x1="0" y1="2.54" x2="-2.54" y2="0" width="0.254" layer="94" curve="90"/>
<wire x1="-2.54" y1="0" x2="0" y2="-2.54" width="0.254" layer="94" curve="90"/>
</symbol>
<symbol name="TP">
<wire x1="0.762" y1="1.778" x2="0" y2="0.254" width="0.254" layer="94"/>
<wire x1="0" y1="0.254" x2="-0.762" y2="1.778" width="0.254" layer="94"/>
<wire x1="-0.762" y1="1.778" x2="0.762" y2="1.778" width="0.254" layer="94" curve="-180"/>
<text x="-1.27" y="3.81" size="1.778" layer="95">&gt;NAME</text>
<pin name="PP" x="0" y="0" visible="off" length="short" direction="in" rot="R90"/>
</symbol>
<symbol name="FIDUCIAL">
<circle x="0" y="0" radius="3.81" width="0" layer="94"/>
<circle x="0" y="0" radius="6.35" width="0.254" layer="94"/>
<text x="-4.826" y="8.89" size="1.778" layer="95">&gt;NAME</text>
</symbol>
<symbol name="SMA_CONN">
<wire x1="2.54" y1="-2.54" x2="1.778" y2="-1.778" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="2.032" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="2.54" x2="2.54" y2="0.508" width="0.3048" layer="94" curve="-79.611142" cap="flat"/>
<wire x1="0" y1="-2.54" x2="2.54" y2="-0.508" width="0.3048" layer="94" curve="79.611142" cap="flat"/>
<text x="0" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<text x="0" y="3.302" size="1.778" layer="95">&gt;NAME</text>
<rectangle x1="0" y1="-0.254" x2="2.286" y2="0.254" layer="94"/>
<pin name="1" x="5.08" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<pin name="2" x="5.08" y="-2.54" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="LM22672MRX-ADJ">
<pin name="BOOT" x="25.4" y="15.24" length="middle" rot="R180"/>
<pin name="SS" x="-5.08" y="2.54" length="middle"/>
<pin name="RT/SYNC" x="-5.08" y="7.62" length="middle"/>
<pin name="FB" x="25.4" y="10.16" length="middle" rot="R180"/>
<pin name="EN" x="-5.08" y="12.7" length="middle"/>
<pin name="GND" x="25.4" y="5.08" length="middle" rot="R180"/>
<pin name="VIN" x="-5.08" y="17.78" length="middle"/>
<pin name="SW" x="25.4" y="17.78" length="middle" rot="R180"/>
<pin name="PAD" x="25.4" y="2.54" length="middle" rot="R180"/>
<wire x1="0" y1="20.32" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="20.32" y2="0" width="0.1524" layer="94"/>
<wire x1="20.32" y1="0" x2="20.32" y2="20.32" width="0.1524" layer="94"/>
<wire x1="20.32" y1="20.32" x2="0" y2="20.32" width="0.1524" layer="94"/>
<text x="5.08" y="22.86" size="2.0828" layer="95" ratio="6" rot="SR0">&gt;Name</text>
<text x="5.08" y="20.574" size="2.0828" layer="96" ratio="6" rot="SR0">&gt;Value</text>
</symbol>
<symbol name="SCHOTTKY_DIODE">
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="1.524" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.524" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.778" x2="1.27" y2="0" width="0.254" layer="94"/>
<text x="-7.62" y="1.27" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-5.08" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
<pin name="A" x="-5.08" y="0" visible="off" length="short" direction="pas"/>
<pin name="C" x="5.08" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.524" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0" x2="-2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="1.27" y1="1.778" x2="0.508" y2="1.778" width="0.254" layer="94"/>
<wire x1="0.508" y1="1.778" x2="0.508" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="-1.524" x2="2.032" y2="-1.524" width="0.254" layer="94"/>
<wire x1="2.032" y1="-1.524" x2="2.032" y2="-1.016" width="0.254" layer="94"/>
</symbol>
<symbol name="NMOS_FET">
<pin name="G" x="-2.54" y="-2.54" visible="pad" length="short"/>
<pin name="S" x="2.54" y="-5.08" visible="pad" length="short" rot="R90"/>
<pin name="D" x="2.54" y="5.08" visible="pad" length="short" rot="R270"/>
<wire x1="0" y1="2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<text x="5.08" y="0" size="1.27" layer="95" ratio="15">&gt;NAME</text>
<text x="5.08" y="-2.54" size="1.27" layer="96" ratio="15">&gt;VALUE</text>
<wire x1="0.508" y1="2.54" x2="0.508" y2="2.032" width="0.254" layer="94"/>
<wire x1="0.508" y1="2.032" x2="0.508" y2="1.524" width="0.254" layer="94"/>
<wire x1="0.508" y1="0.762" x2="0.508" y2="0" width="0.254" layer="94"/>
<wire x1="0.508" y1="0" x2="0.508" y2="-0.762" width="0.254" layer="94"/>
<wire x1="0.508" y1="-1.524" x2="0.508" y2="-2.032" width="0.254" layer="94"/>
<wire x1="0.508" y1="-2.032" x2="0.508" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0.508" y1="2.032" x2="2.54" y2="2.032" width="0.254" layer="94"/>
<wire x1="2.54" y1="2.032" x2="2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="0.508" y1="-2.032" x2="2.54" y2="-2.032" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.032" x2="2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0.508" y1="0" x2="0.762" y2="0" width="0.254" layer="94"/>
<wire x1="0.762" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-2.032" width="0.254" layer="94"/>
<wire x1="0.762" y1="0" x2="1.524" y2="0.254" width="0.254" layer="94"/>
<wire x1="0.762" y1="0" x2="1.524" y2="-0.254" width="0.254" layer="94"/>
<wire x1="1.524" y1="-0.254" x2="1.524" y2="0.254" width="0.254" layer="94"/>
<wire x1="2.54" y1="2.54" x2="4.064" y2="2.54" width="0.254" layer="94"/>
<wire x1="4.064" y1="2.54" x2="4.064" y2="0.508" width="0.254" layer="94"/>
<wire x1="4.064" y1="0.508" x2="3.556" y2="0.508" width="0.254" layer="94"/>
<wire x1="4.572" y1="0.508" x2="4.064" y2="0.508" width="0.254" layer="94"/>
<wire x1="4.064" y1="-0.508" x2="4.572" y2="-0.508" width="0.254" layer="94"/>
<wire x1="4.572" y1="-0.508" x2="4.064" y2="0.508" width="0.254" layer="94"/>
<wire x1="4.064" y1="0.508" x2="3.556" y2="-0.508" width="0.254" layer="94"/>
<wire x1="3.556" y1="-0.508" x2="4.064" y2="-0.508" width="0.254" layer="94"/>
<wire x1="4.064" y1="-0.508" x2="4.064" y2="-2.54" width="0.254" layer="94"/>
<wire x1="4.064" y1="-2.54" x2="2.54" y2="-2.54" width="0.254" layer="94"/>
</symbol>
<symbol name="FERRITE_BEAD">
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="-1.27" y1="0" x2="0" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="0" y1="0" x2="1.27" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.254" layer="94" curve="-180"/>
<text x="-2.54" y="3.81" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<wire x1="-2.54" y1="1.27" x2="2.54" y2="1.27" width="0.254" layer="94"/>
</symbol>
<symbol name="INDUCTOR">
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="-1.27" y1="0" x2="0" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="0" y1="0" x2="1.27" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.254" layer="94" curve="-180"/>
<text x="-2.54" y="1.27" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="ZENER">
<wire x1="-1.27" y1="-1.524" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="1.524" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.524" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="-1.524" width="0.254" layer="94"/>
<text x="-7.62" y="1.27" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-5.08" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
<pin name="+" x="-5.08" y="0" visible="pin" length="short" direction="pas"/>
<pin name="-" x="5.08" y="0" visible="pin" length="short" direction="pas" rot="R180"/>
<wire x1="-1.27" y1="0" x2="-2.54" y2="0" width="0.15748125" layer="94"/>
<wire x1="1.27" y1="-1.397" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="1.397" width="0.254" layer="94"/>
<wire x1="1.27" y1="-1.397" x2="0.889" y2="-1.778" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.397" x2="1.651" y2="1.778" width="0.254" layer="94"/>
<wire x1="2.7178" y1="0" x2="1.1938" y2="0" width="0.15748125" layer="94"/>
</symbol>
<symbol name="TI_TPS2491DGSR">
<wire x1="10.16" y1="5.08" x2="10.16" y2="-5.08" width="0.254" layer="94"/>
<wire x1="10.16" y1="-5.08" x2="-10.16" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-5.08" x2="-10.16" y2="5.08" width="0.254" layer="94"/>
<wire x1="-10.16" y1="5.08" x2="10.16" y2="5.08" width="0.254" layer="94"/>
<pin name="10_VCC" x="-12.7" y="2.54" visible="pad" length="short"/>
<pin name="1_EN" x="-12.7" y="0" visible="pad" length="short"/>
<pin name="2_VREF" x="-12.7" y="-2.54" visible="pad" length="short"/>
<pin name="3_PROG" x="-5.08" y="-7.62" visible="pad" length="short" rot="R90"/>
<pin name="4_TIMER" x="0" y="-7.62" visible="pad" length="short" rot="R90"/>
<pin name="5_GND" x="5.08" y="-7.62" visible="pad" length="short" rot="R90"/>
<pin name="6_PG" x="12.7" y="2.54" visible="pad" length="short" rot="R180"/>
<pin name="9_SENSE" x="-5.08" y="7.62" visible="pad" length="short" rot="R270"/>
<pin name="8_GATE" x="0" y="7.62" visible="pad" length="short" rot="R270"/>
<pin name="7_OUT" x="5.08" y="7.62" visible="pad" length="short" rot="R270"/>
<text x="-3.048" y="0" size="1.27" layer="94">TPS2491</text>
<text x="-6.604" y="3.81" size="0.8128" layer="94">SENSE</text>
<text x="-1.27" y="3.81" size="0.8128" layer="94">GATE</text>
<text x="4.064" y="3.81" size="0.8128" layer="94">OUT</text>
<text x="8.382" y="2.286" size="0.8128" layer="94">PG</text>
<text x="4.064" y="-4.572" size="0.8128" layer="94">GND</text>
<text x="-1.27" y="-4.572" size="0.8128" layer="94">TIMER</text>
<text x="-6.35" y="-4.572" size="0.8128" layer="94">PROG</text>
<text x="-9.652" y="-3.048" size="0.8128" layer="94">VREF</text>
<text x="-9.652" y="-0.508" size="0.8128" layer="94">EN</text>
<text x="-9.652" y="2.032" size="0.8128" layer="94">VCC</text>
<text x="-12.7" y="7.62" size="1.27" layer="95" ratio="15">&gt;NAME</text>
<text x="-12.7" y="10.16" size="1.27" layer="95" ratio="15">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="TI_SN65HVD62RGTR" prefix="U" uservalue="yes">
<gates>
<gate name="A" symbol="SN65HVD62RGTR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="RGT16_1P45X1P45">
<connects>
<connect gate="A" pin="BIAS" pad="10"/>
<connect gate="A" pin="DIR" pad="5"/>
<connect gate="A" pin="DIRSET1" pad="7"/>
<connect gate="A" pin="DIRSET2" pad="6"/>
<connect gate="A" pin="EPAD" pad="17"/>
<connect gate="A" pin="GND" pad="16"/>
<connect gate="A" pin="GND_2" pad="8"/>
<connect gate="A" pin="RES" pad="9"/>
<connect gate="A" pin="RXIN" pad="11"/>
<connect gate="A" pin="RXOUT" pad="4"/>
<connect gate="A" pin="SYNCOUT" pad="1"/>
<connect gate="A" pin="TXIN" pad="2"/>
<connect gate="A" pin="TXOUT" pad="12"/>
<connect gate="A" pin="VCC" pad="13"/>
<connect gate="A" pin="VL" pad="3"/>
<connect gate="A" pin="XTAL1" pad="14"/>
<connect gate="A" pin="XTAL2" pad="15"/>
</connects>
<technologies>
<technology name="">
<attribute name="MANUFACTURER_PART_NUMBER" value="SN65HVD62RGTR" constant="no"/>
<attribute name="VENDOR" value="Texas Instruments" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAP_SMD_" prefix="C" uservalue="yes">
<gates>
<gate name="G$1" symbol="CAPACITOR" x="0" y="0"/>
</gates>
<devices>
<device name="0402" package="CAPC1005X55N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="CAPC1608X55N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="CAPC2013X145N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="CAPC3216X190N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1812" package="CAPC4532X279N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RES_SMD_" prefix="R" uservalue="yes">
<gates>
<gate name="G$1" symbol="RESISTER" x="0" y="0"/>
</gates>
<devices>
<device name="0402" package="RESC1005X40N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="RESC1608X50N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="RESC2013X70N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="RESC3216X84N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805_SH" package="RES2013X38N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2010" package="RESC5325X84N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2512" package="RESC6332X65N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1812" package="RESC4532X279N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ZENER_UNIDIRECTIONAL_SMT">
<gates>
<gate name="G$1" symbol="ZENER" x="0" y="0"/>
</gates>
<devices>
<device name="DO-214AB,SMC" package="DO-214AB,SMC">
<connects>
<connect gate="G$1" pin="+" pad="P$2"/>
<connect gate="G$1" pin="-" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="STM_SM15T" prefix="D" uservalue="yes">
<gates>
<gate name="A" symbol="ZENER_BIDIRECTIONAL" x="0" y="0"/>
</gates>
<devices>
<device name="" package="DO-214AB,SMC">
<connects>
<connect gate="A" pin="+" pad="P$2"/>
<connect gate="A" pin="-" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TI_DS3695AX" prefix="U" uservalue="yes">
<gates>
<gate name="G$1" symbol="RS485" x="10.16" y="-12.7"/>
</gates>
<devices>
<device name="" package="SOIC127P600X173-8N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="1.5SMC480CA" prefix="D" uservalue="yes">
<gates>
<gate name="A" symbol="ZENER" x="0" y="0"/>
</gates>
<devices>
<device name="" package="DO-214AB,SMC">
<connects>
<connect gate="A" pin="+" pad="P$2"/>
<connect gate="A" pin="-" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TP_CLASSB_" prefix="TP" uservalue="yes">
<gates>
<gate name="G$1" symbol="TP" x="0" y="0"/>
</gates>
<devices>
<device name="1.5MM" package="TP_1.5MM">
<connects>
<connect gate="G$1" pin="PP" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0.4MM" package="C120H40">
<connects>
<connect gate="G$1" pin="PP" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0.51MM" package="C131H51">
<connects>
<connect gate="G$1" pin="PP" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3.26MM" package="C406H326">
<connects>
<connect gate="G$1" pin="PP" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="4.11MM" package="C491H411">
<connects>
<connect gate="G$1" pin="PP" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1.5MM_PAD" package="TP_1.5MM_PAD">
<connects>
<connect gate="G$1" pin="PP" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1.7MM" package="TP_1.7MM">
<connects>
<connect gate="G$1" pin="PP" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1MM_CIR" package="CIR_PAD_1MM">
<connects>
<connect gate="G$1" pin="PP" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2MM_CIR" package="CIR_PAD_2MM">
<connects>
<connect gate="G$1" pin="PP" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GDT_2029-XX-SMLF" prefix="F" uservalue="yes">
<gates>
<gate name="G$1" symbol="GDT" x="0" y="0"/>
</gates>
<devices>
<device name="" package="GDT_2029-XX-SMLF">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="0.5MM_FIDUCIAL" prefix="F">
<gates>
<gate name="G$1" symbol="FIDUCIAL" x="0" y="0"/>
</gates>
<devices>
<device name="" package="0.5MM_FIDUCIAL">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TRANSISTION_CLIP" prefix="J" uservalue="yes">
<gates>
<gate name="G$1" symbol="SMA_CONN" x="0" y="0"/>
</gates>
<devices>
<device name="TRANSITION_CLIP_.141" package="TRANSITION_CLIP_.141">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TI_LM22672MRX-ADJ" prefix="U" uservalue="yes">
<gates>
<gate name="A" symbol="LM22672MRX-ADJ" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MRA08B">
<connects>
<connect gate="A" pin="BOOT" pad="1"/>
<connect gate="A" pin="EN" pad="5"/>
<connect gate="A" pin="FB" pad="4"/>
<connect gate="A" pin="GND" pad="6"/>
<connect gate="A" pin="PAD" pad="9"/>
<connect gate="A" pin="RT/SYNC" pad="3"/>
<connect gate="A" pin="SS" pad="2"/>
<connect gate="A" pin="SW" pad="8"/>
<connect gate="A" pin="VIN" pad="7"/>
</connects>
<technologies>
<technology name="">
<attribute name="MANUFACTURER_PART_NUMBER" value="lm22672mrxadj" constant="no"/>
<attribute name="VENDOR" value="Texas Instruments" constant="no"/>
</technology>
</technologies>
</device>
<device name="MRA08B-M" package="MRA08B-M">
<connects>
<connect gate="A" pin="BOOT" pad="1"/>
<connect gate="A" pin="EN" pad="5"/>
<connect gate="A" pin="FB" pad="4"/>
<connect gate="A" pin="GND" pad="6"/>
<connect gate="A" pin="PAD" pad="EPAD"/>
<connect gate="A" pin="RT/SYNC" pad="3"/>
<connect gate="A" pin="SS" pad="2"/>
<connect gate="A" pin="SW" pad="8"/>
<connect gate="A" pin="VIN" pad="7"/>
</connects>
<technologies>
<technology name="">
<attribute name="MANUFACTURER_PART_NUMBER" value="lm22672mrxadj" constant="no"/>
<attribute name="VENDOR" value="Texas Instruments" constant="no"/>
</technology>
</technologies>
</device>
<device name="MRA08B-L" package="MRA08B-L">
<connects>
<connect gate="A" pin="BOOT" pad="1"/>
<connect gate="A" pin="EN" pad="5"/>
<connect gate="A" pin="FB" pad="4"/>
<connect gate="A" pin="GND" pad="6"/>
<connect gate="A" pin="PAD" pad="EPAD"/>
<connect gate="A" pin="RT/SYNC" pad="3"/>
<connect gate="A" pin="SS" pad="2"/>
<connect gate="A" pin="SW" pad="8"/>
<connect gate="A" pin="VIN" pad="7"/>
</connects>
<technologies>
<technology name="">
<attribute name="MANUFACTURER_PART_NUMBER" value="lm22672mrxadj" constant="no"/>
<attribute name="VENDOR" value="Texas Instruments" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DIODE_SCHOTTKY_DO-214AC" prefix="D" uservalue="yes">
<gates>
<gate name="A" symbol="SCHOTTKY_DIODE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="DO-214AC_475X290">
<connects>
<connect gate="A" pin="A" pad="1"/>
<connect gate="A" pin="C" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MOSFET_NMOS" prefix="Q" uservalue="yes">
<gates>
<gate name="G$1" symbol="NMOS_FET" x="-2.54" y="0"/>
</gates>
<devices>
<device name="SOT-23-3" package="SOT-23-3">
<connects>
<connect gate="G$1" pin="D" pad="3"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="DPAK/TO-252-3" package="TO228P991X238-3N">
<connects>
<connect gate="G$1" pin="D" pad="3"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="FERRITE_BEAD" prefix="FB" uservalue="yes">
<gates>
<gate name="G$1" symbol="FERRITE_BEAD" x="0" y="0"/>
</gates>
<devices>
<device name="BLM18XXXXXXH1D" package="BLM18XXXXXXH1D">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="CAPC1608X55N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ZENER_ONSEMI_1SMC5.0AT3G" prefix="D" uservalue="yes">
<gates>
<gate name="G$1" symbol="ZENER" x="0" y="-2.54"/>
</gates>
<devices>
<device name="" package="DO-214AB_SMC">
<connects>
<connect gate="G$1" pin="+" pad="2"/>
<connect gate="G$1" pin="-" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="XTAL_HC49/US" prefix="Y" uservalue="yes">
<gates>
<gate name="G$1" symbol="CRYSTAL_2PIN" x="0" y="0"/>
</gates>
<devices>
<device name="" package="XTAL1150X480X430N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="IND_SMD_5.0X5.0" prefix="L" uservalue="yes">
<gates>
<gate name="G$1" symbol="INDUCTOR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="IND_SMD_5.0X5.0">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TI_TPS2491DGSR" prefix="U" uservalue="yes">
<gates>
<gate name="G$1" symbol="TI_TPS2491DGSR" x="0" y="0"/>
</gates>
<devices>
<device name="10-TFSOP/10-MSOP" package="SOP50P490X110-10AN">
<connects>
<connect gate="G$1" pin="10_VCC" pad="10"/>
<connect gate="G$1" pin="1_EN" pad="1"/>
<connect gate="G$1" pin="2_VREF" pad="2"/>
<connect gate="G$1" pin="3_PROG" pad="3"/>
<connect gate="G$1" pin="4_TIMER" pad="4"/>
<connect gate="G$1" pin="5_GND" pad="5"/>
<connect gate="G$1" pin="6_PG" pad="6"/>
<connect gate="G$1" pin="7_OUT" pad="7"/>
<connect gate="G$1" pin="8_GATE" pad="8"/>
<connect gate="G$1" pin="9_SENSE" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply2" urn="urn:adsk.eagle:library:372">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
Please keep in mind, that these devices are necessary for the
automatic wiring of the supply signals.&lt;p&gt;
The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND" urn="urn:adsk.eagle:symbol:26990/1" library_version="2">
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="-1.27" y2="0" width="0.254" layer="94"/>
<text x="-1.905" y="-3.175" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" urn="urn:adsk.eagle:component:27037/1" prefix="SUPPLY" library_version="2">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="GND" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1" urn="urn:adsk.eagle:library:371">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="+5V" urn="urn:adsk.eagle:symbol:26929/1" library_version="1">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+5V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="+5V" urn="urn:adsk.eagle:component:26963/1" prefix="P+" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="+5V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="frames">
<description>&lt;b&gt;Frames for Sheet and Layout&lt;/b&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="A3_LOC_JMA_RELEASE">
<wire x1="0" y1="241.3" x2="63.5" y2="241.3" width="0.025" layer="94"/>
<wire x1="63.5" y1="241.3" x2="127" y2="241.3" width="0.025" layer="94"/>
<wire x1="127" y1="241.3" x2="190.5" y2="241.3" width="0.025" layer="94"/>
<wire x1="190.5" y1="241.3" x2="254" y2="241.3" width="0.025" layer="94"/>
<wire x1="254" y1="241.3" x2="317.5" y2="241.3" width="0.025" layer="94"/>
<wire x1="317.5" y1="241.3" x2="380.97883125" y2="241.3" width="0.025" layer="94"/>
<wire x1="380.97883125" y1="241.3" x2="380.97883125" y2="0.02116875" width="0.025" layer="94"/>
<wire x1="380.97883125" y1="0.02116875" x2="0" y2="0.02116875" width="0.025" layer="94"/>
<wire x1="0" y1="0.02116875" x2="0" y2="60.325" width="0.025" layer="94"/>
<wire x1="0" y1="60.325" x2="0" y2="120.65" width="0.025" layer="94"/>
<wire x1="0" y1="120.65" x2="0" y2="180.975" width="0.025" layer="94"/>
<wire x1="0" y1="180.975" x2="0" y2="241.3" width="0.025" layer="94"/>
<wire x1="5.079890625" y1="236.22041875" x2="375.898909375" y2="236.22041875" width="0.025" layer="94"/>
<wire x1="375.898909375" y1="236.22041875" x2="375.898909375" y2="5.10075" width="0.025" layer="94"/>
<wire x1="375.898909375" y1="5.10075" x2="5.079890625" y2="5.10075" width="0.025" layer="94"/>
<wire x1="5.079890625" y1="5.10075" x2="5.079890625" y2="236.22041875" width="0.025" layer="94"/>
<wire x1="317.5" y1="5.08" x2="317.5" y2="0" width="0.025" layer="94"/>
<wire x1="317.5" y1="236.22" x2="317.5" y2="241.3" width="0.025" layer="94"/>
<wire x1="254" y1="5.08" x2="254" y2="0" width="0.025" layer="94"/>
<wire x1="254" y1="236.22" x2="254" y2="241.3" width="0.025" layer="94"/>
<wire x1="190.5" y1="5.08" x2="190.5" y2="0" width="0.025" layer="94"/>
<wire x1="190.5" y1="236.22" x2="190.5" y2="241.3" width="0.025" layer="94"/>
<wire x1="127" y1="5.08" x2="127" y2="0" width="0.025" layer="94"/>
<wire x1="127" y1="236.22" x2="127" y2="241.3" width="0.025" layer="94"/>
<wire x1="63.5" y1="5.08" x2="63.5" y2="0" width="0.025" layer="94"/>
<wire x1="63.5" y1="236.22" x2="63.5" y2="241.3" width="0.025" layer="94"/>
<wire x1="5.08" y1="180.975" x2="0" y2="180.975" width="0.025" layer="94"/>
<wire x1="375.92" y1="180.975" x2="381" y2="180.975" width="0.025" layer="94"/>
<wire x1="5.08" y1="120.65" x2="0" y2="120.65" width="0.025" layer="94"/>
<wire x1="375.92" y1="120.65" x2="381" y2="120.65" width="0.025" layer="94"/>
<wire x1="5.08" y1="60.325" x2="0" y2="60.325" width="0.025" layer="94"/>
<wire x1="375.92" y1="60.325" x2="381" y2="60.325" width="0.025" layer="94"/>
<wire x1="271.78" y1="5.08" x2="271.78" y2="21.59" width="0.025" layer="94"/>
<wire x1="271.78" y1="21.59" x2="271.78" y2="27.94" width="0.025" layer="94"/>
<wire x1="271.78" y1="27.94" x2="271.78" y2="34.29" width="0.025" layer="94"/>
<wire x1="271.78" y1="34.29" x2="271.78" y2="40.64" width="0.025" layer="94"/>
<wire x1="271.78" y1="40.64" x2="271.78" y2="45.72" width="0.025" layer="94"/>
<wire x1="271.78" y1="45.72" x2="285.75" y2="45.72" width="0.025" layer="94"/>
<wire x1="285.75" y1="45.72" x2="293.37" y2="45.72" width="0.025" layer="94"/>
<wire x1="293.37" y1="45.72" x2="304.8" y2="45.72" width="0.025" layer="94"/>
<wire x1="304.8" y1="45.72" x2="375.92" y2="45.72" width="0.025" layer="94"/>
<wire x1="304.8" y1="5.08" x2="304.8" y2="8.89" width="0.025" layer="94"/>
<wire x1="304.8" y1="8.89" x2="304.8" y2="15.24" width="0.025" layer="94"/>
<wire x1="304.8" y1="15.24" x2="304.8" y2="21.59" width="0.025" layer="94"/>
<wire x1="304.8" y1="21.59" x2="304.8" y2="27.94" width="0.025" layer="94"/>
<wire x1="304.8" y1="27.94" x2="304.8" y2="34.29" width="0.025" layer="94"/>
<wire x1="304.8" y1="34.29" x2="304.8" y2="40.64" width="0.025" layer="94"/>
<wire x1="304.8" y1="40.64" x2="304.8" y2="45.72" width="0.025" layer="94"/>
<wire x1="271.78" y1="40.64" x2="304.8" y2="40.64" width="0.025" layer="94"/>
<wire x1="271.78" y1="34.29" x2="304.8" y2="34.29" width="0.025" layer="94"/>
<wire x1="271.78" y1="27.94" x2="304.8" y2="27.94" width="0.025" layer="94"/>
<wire x1="271.78" y1="21.59" x2="285.75" y2="21.59" width="0.025" layer="94"/>
<wire x1="285.75" y1="21.59" x2="293.37" y2="21.59" width="0.025" layer="94"/>
<wire x1="293.37" y1="21.59" x2="304.8" y2="21.59" width="0.025" layer="94"/>
<wire x1="304.8" y1="8.89" x2="322.58" y2="8.89" width="0.025" layer="94"/>
<wire x1="322.58" y1="8.89" x2="359.41" y2="8.89" width="0.025" layer="94"/>
<wire x1="359.41" y1="8.89" x2="369.57" y2="8.89" width="0.025" layer="94"/>
<wire x1="369.57" y1="8.89" x2="375.92" y2="8.89" width="0.025" layer="94"/>
<wire x1="322.58" y1="8.89" x2="322.58" y2="5.08" width="0.025" layer="94"/>
<wire x1="359.41" y1="8.89" x2="359.41" y2="5.08" width="0.025" layer="94"/>
<wire x1="304.8" y1="15.24" x2="369.57" y2="15.24" width="0.025" layer="94"/>
<wire x1="369.57" y1="15.24" x2="375.92" y2="15.24" width="0.025" layer="94"/>
<wire x1="369.57" y1="15.24" x2="369.57" y2="8.89" width="0.025" layer="94"/>
<wire x1="304.8" y1="21.59" x2="375.92" y2="21.59" width="0.025" layer="94"/>
<wire x1="285.75" y1="45.72" x2="285.75" y2="21.59" width="0.025" layer="94"/>
<wire x1="293.37" y1="45.72" x2="293.37" y2="21.59" width="0.025" layer="94"/>
<wire x1="307.34" y1="31.75" x2="373.38" y2="31.75" width="0.025" layer="94"/>
<wire x1="329.29703125" y1="44.90211875" x2="329.35418125" y2="44.890690625" width="0.025" layer="94"/>
<wire x1="329.35418125" y1="44.890690625" x2="329.3999" y2="44.879259375" width="0.025" layer="94"/>
<wire x1="329.81138125" y1="44.776390625" x2="329.84566875" y2="44.764959375" width="0.025" layer="94"/>
<wire x1="329.84566875" y1="44.764959375" x2="329.891390625" y2="44.75353125" width="0.025" layer="94"/>
<wire x1="331.137259375" y1="44.1706" x2="331.365859375" y2="43.99915" width="0.025" layer="94"/>
<wire x1="331.365859375" y1="43.99915" x2="331.451590625" y2="43.91341875" width="0.025" layer="94"/>
<wire x1="331.92593125" y1="43.164759375" x2="331.937359375" y2="43.09618125" width="0.025" layer="94"/>
<wire x1="331.937359375" y1="43.09618125" x2="331.948790625" y2="42.993309375" width="0.025" layer="94"/>
<wire x1="331.891640625" y1="42.5704" x2="331.90306875" y2="42.604690625" width="0.025" layer="94"/>
<wire x1="331.84591875" y1="42.4561" x2="331.891640625" y2="42.5704" width="0.025" layer="94"/>
<wire x1="329.262740625" y1="40.947340625" x2="329.319890625" y2="40.95876875" width="0.025" layer="94"/>
<wire x1="329.194159375" y1="40.935909375" x2="329.262740625" y2="40.947340625" width="0.025" layer="94"/>
<wire x1="329.194159375" y1="40.935909375" x2="331.44586875" y2="32.512" width="0.025" layer="94"/>
<wire x1="323.764909375" y1="32.512" x2="324.9625" y2="37.03828125" width="0.025" layer="94"/>
<wire x1="324.9625" y1="37.03828125" x2="325.993759375" y2="40.935909375" width="0.025" layer="94"/>
<wire x1="325.719440625" y1="40.993059375" x2="325.78801875" y2="40.98163125" width="0.025" layer="94"/>
<wire x1="325.63943125" y1="41.01591875" x2="325.719440625" y2="40.993059375" width="0.025" layer="94"/>
<wire x1="325.45655" y1="41.061640625" x2="325.536559375" y2="41.03878125" width="0.025" layer="94"/>
<wire x1="325.3994" y1="41.07306875" x2="325.45655" y2="41.061640625" width="0.025" layer="94"/>
<wire x1="325.136509375" y1="41.15308125" x2="325.21651875" y2="41.13021875" width="0.025" layer="94"/>
<wire x1="325.06793125" y1="41.175940625" x2="325.136509375" y2="41.15308125" width="0.025" layer="94"/>
<wire x1="324.976490625" y1="41.21023125" x2="325.06793125" y2="41.175940625" width="0.025" layer="94"/>
<wire x1="324.97076875" y1="41.21251875" x2="324.976490625" y2="41.21023125" width="0.025" layer="94"/>
<wire x1="324.64501875" y1="41.347390625" x2="324.747890625" y2="41.30166875" width="0.025" layer="94"/>
<wire x1="324.565009375" y1="41.38168125" x2="324.64501875" y2="41.347390625" width="0.025" layer="94"/>
<wire x1="324.450709375" y1="41.43883125" x2="324.45185" y2="41.438259375" width="0.025" layer="94"/>
<wire x1="324.393559375" y1="41.47311875" x2="324.450709375" y2="41.43883125" width="0.025" layer="94"/>
<wire x1="324.26783125" y1="41.5417" x2="324.347840625" y2="41.49598125" width="0.025" layer="94"/>
<wire x1="324.233540625" y1="41.564559375" x2="324.26783125" y2="41.5417" width="0.025" layer="94"/>
<wire x1="324.18781875" y1="41.58741875" x2="324.233540625" y2="41.564559375" width="0.025" layer="94"/>
<wire x1="324.1421" y1="41.621709375" x2="324.18781875" y2="41.58741875" width="0.025" layer="94"/>
<wire x1="324.08495" y1="41.656" x2="324.1421" y2="41.621709375" width="0.025" layer="94"/>
<wire x1="323.822059375" y1="41.861740625" x2="323.85635" y2="41.82745" width="0.025" layer="94"/>
<wire x1="323.776340625" y1="41.89603125" x2="323.822059375" y2="41.861740625" width="0.025" layer="94"/>
<wire x1="323.62775" y1="42.05605" x2="323.662040625" y2="42.01033125" width="0.025" layer="94"/>
<wire x1="323.593459375" y1="42.090340625" x2="323.62775" y2="42.05605" width="0.025" layer="94"/>
<wire x1="323.35343125" y1="42.46753125" x2="323.354" y2="42.466390625" width="0.025" layer="94"/>
<wire x1="323.33056875" y1="42.52468125" x2="323.35343125" y2="42.46753125" width="0.025" layer="94"/>
<wire x1="323.536309375" y1="43.690540625" x2="323.58203125" y2="43.736259375" width="0.025" layer="94"/>
<wire x1="323.58203125" y1="43.736259375" x2="323.62775" y2="43.793409375" width="0.025" layer="94"/>
<wire x1="323.78776875" y1="43.95343125" x2="323.84491875" y2="43.99915" width="0.025" layer="94"/>
<wire x1="323.84491875" y1="43.99915" x2="323.87806875" y2="44.0323" width="0.025" layer="94"/>
<wire x1="323.87806875" y1="44.0323" x2="323.879209375" y2="44.033440625" width="0.025" layer="94"/>
<wire x1="323.879209375" y1="44.033440625" x2="323.936359375" y2="44.079159375" width="0.025" layer="94"/>
<wire x1="323.936359375" y1="44.079159375" x2="324.0278" y2="44.147740625" width="0.025" layer="94"/>
<wire x1="324.164959375" y1="44.23918125" x2="324.222109375" y2="44.27346875" width="0.025" layer="94"/>
<wire x1="324.222109375" y1="44.27346875" x2="324.290690625" y2="44.319190625" width="0.025" layer="94"/>
<wire x1="324.290690625" y1="44.319190625" x2="324.336409375" y2="44.34205" width="0.025" layer="94"/>
<wire x1="324.336409375" y1="44.34205" x2="324.393559375" y2="44.376340625" width="0.025" layer="94"/>
<wire x1="324.62101875" y1="44.49006875" x2="324.622159375" y2="44.490640625" width="0.025" layer="94"/>
<wire x1="324.622159375" y1="44.490640625" x2="324.679309375" y2="44.5135" width="0.025" layer="94"/>
<wire x1="324.679309375" y1="44.5135" x2="324.72503125" y2="44.536359375" width="0.025" layer="94"/>
<wire x1="324.72503125" y1="44.536359375" x2="325.01078125" y2="44.650659375" width="0.025" layer="94"/>
<wire x1="325.01078125" y1="44.650659375" x2="325.35368125" y2="44.764959375" width="0.025" layer="94"/>
<wire x1="325.35368125" y1="44.764959375" x2="325.433690625" y2="44.78781875" width="0.025" layer="94"/>
<wire x1="325.433690625" y1="44.78781875" x2="325.479409375" y2="44.79925" width="0.025" layer="94"/>
<wire x1="325.479409375" y1="44.79925" x2="325.5137" y2="44.81068125" width="0.025" layer="94"/>
<wire x1="325.5137" y1="44.81068125" x2="325.7423" y2="44.86783125" width="0.025" layer="94"/>
<wire x1="325.7423" y1="44.86783125" x2="325.79945" y2="44.879259375" width="0.025" layer="94"/>
<wire x1="325.79945" y1="44.879259375" x2="325.84516875" y2="44.890690625" width="0.025" layer="94"/>
<wire x1="326.393809375" y1="44.69638125" x2="326.450959375" y2="44.707809375" width="0.025" layer="94"/>
<wire x1="326.32523125" y1="44.68495" x2="326.393809375" y2="44.69638125" width="0.025" layer="94"/>
<wire x1="325.936609375" y1="44.604940625" x2="325.993759375" y2="44.61636875" width="0.025" layer="94"/>
<wire x1="325.84516875" y1="44.58208125" x2="325.936609375" y2="44.604940625" width="0.025" layer="94"/>
<wire x1="325.765159375" y1="44.55921875" x2="325.84516875" y2="44.58208125" width="0.025" layer="94"/>
<wire x1="325.67371875" y1="44.536359375" x2="325.765159375" y2="44.55921875" width="0.025" layer="94"/>
<wire x1="325.5137" y1="44.490640625" x2="325.520559375" y2="44.4926" width="0.025" layer="94"/>
<wire x1="325.23938125" y1="44.3992" x2="325.5137" y2="44.490640625" width="0.025" layer="94"/>
<wire x1="324.98791875" y1="44.29633125" x2="325.06793125" y2="44.33061875" width="0.025" layer="94"/>
<wire x1="324.93076875" y1="44.27346875" x2="324.98791875" y2="44.29633125" width="0.025" layer="94"/>
<wire x1="324.61073125" y1="44.10201875" x2="324.679309375" y2="44.147740625" width="0.025" layer="94"/>
<wire x1="324.53071875" y1="44.0563" x2="324.531859375" y2="44.05695" width="0.025" layer="94"/>
<wire x1="324.531859375" y1="44.05695" x2="324.61073125" y2="44.10201875" width="0.025" layer="94"/>
<wire x1="324.462140625" y1="44.01058125" x2="324.53071875" y2="44.0563" width="0.025" layer="94"/>
<wire x1="324.233540625" y1="43.8277" x2="324.279259375" y2="43.87341875" width="0.025" layer="94"/>
<wire x1="324.164959375" y1="43.77055" x2="324.233540625" y2="43.8277" width="0.025" layer="94"/>
<wire x1="323.73061875" y1="42.51325" x2="323.74205" y2="42.478959375" width="0.025" layer="94"/>
<wire x1="323.74205" y1="42.478959375" x2="323.75348125" y2="42.433240625" width="0.025" layer="94"/>
<wire x1="323.75348125" y1="42.433240625" x2="323.776340625" y2="42.38751875" width="0.025" layer="94"/>
<wire x1="323.776340625" y1="42.38751875" x2="323.7992" y2="42.33036875" width="0.025" layer="94"/>
<wire x1="324.08495" y1="41.95318125" x2="324.1421" y2="41.907459375" width="0.025" layer="94"/>
<wire x1="324.1421" y1="41.907459375" x2="324.176390625" y2="41.87316875" width="0.025" layer="94"/>
<wire x1="324.41641875" y1="41.690290625" x2="324.462140625" y2="41.656" width="0.025" layer="94"/>
<wire x1="324.462140625" y1="41.656" x2="324.53071875" y2="41.61028125" width="0.025" layer="94"/>
<wire x1="324.64501875" y1="41.5417" x2="324.690740625" y2="41.518840625" width="0.025" layer="94"/>
<wire x1="324.690740625" y1="41.518840625" x2="324.72503125" y2="41.49598125" width="0.025" layer="94"/>
<wire x1="324.72503125" y1="41.49598125" x2="324.78218125" y2="41.461690625" width="0.025" layer="94"/>
<wire x1="324.919340625" y1="41.393109375" x2="324.976490625" y2="41.37025" width="0.025" layer="94"/>
<wire x1="324.976490625" y1="41.37025" x2="325.022209375" y2="41.347390625" width="0.025" layer="94"/>
<wire x1="325.46798125" y1="41.18736875" x2="325.536559375" y2="41.164509375" width="0.025" layer="94"/>
<wire x1="325.536559375" y1="41.164509375" x2="325.58228125" y2="41.15308125" width="0.025" layer="94"/>
<wire x1="325.58228125" y1="41.15308125" x2="325.61656875" y2="41.14165" width="0.025" layer="94"/>
<wire x1="325.61656875" y1="41.14165" x2="325.708009375" y2="41.118790625" width="0.025" layer="94"/>
<wire x1="325.708009375" y1="41.118790625" x2="325.7423" y2="41.107359375" width="0.025" layer="94"/>
<wire x1="325.7423" y1="41.107359375" x2="326.01661875" y2="41.03878125" width="0.025" layer="94"/>
<wire x1="325.45655" y1="41.38168125" x2="325.5137" y2="41.35881875" width="0.025" layer="94"/>
<wire x1="325.433690625" y1="41.393109375" x2="325.45655" y2="41.38168125" width="0.025" layer="94"/>
<wire x1="325.15936875" y1="41.518840625" x2="325.319390625" y2="41.43883125" width="0.025" layer="94"/>
<wire x1="325.10221875" y1="41.55313125" x2="325.15936875" y2="41.518840625" width="0.025" layer="94"/>
<wire x1="324.70216875" y1="43.4848" x2="324.747890625" y2="43.519090625" width="0.025" layer="94"/>
<wire x1="324.747890625" y1="43.519090625" x2="324.77075" y2="43.54195" width="0.025" layer="94"/>
<wire x1="324.98678125" y1="43.68978125" x2="324.98791875" y2="43.690540625" width="0.025" layer="94"/>
<wire x1="324.98791875" y1="43.690540625" x2="325.10221875" y2="43.75911875" width="0.025" layer="94"/>
<wire x1="325.10221875" y1="43.75911875" x2="325.177659375" y2="43.796840625" width="0.025" layer="94"/>
<wire x1="325.45655" y1="43.93056875" x2="325.68515" y2="44.022009375" width="0.025" layer="94"/>
<wire x1="325.82116875" y1="44.06735" x2="325.822309375" y2="44.06773125" width="0.025" layer="94"/>
<wire x1="325.822309375" y1="44.06773125" x2="326.062340625" y2="44.136309375" width="0.025" layer="94"/>
<wire x1="326.062340625" y1="44.136309375" x2="326.15378125" y2="44.15916875" width="0.025" layer="94"/>
<wire x1="326.15378125" y1="44.15916875" x2="326.35951875" y2="44.204890625" width="0.025" layer="94"/>
<wire x1="326.279509375" y1="43.919140625" x2="326.462390625" y2="43.964859375" width="0.025" layer="94"/>
<wire x1="326.119490625" y1="43.87341875" x2="326.279509375" y2="43.919140625" width="0.025" layer="94"/>
<wire x1="326.02805" y1="43.850559375" x2="326.119490625" y2="43.87341875" width="0.025" layer="94"/>
<wire x1="325.68515" y1="43.72483125" x2="325.8566" y2="43.793409375" width="0.025" layer="94"/>
<wire x1="325.605140625" y1="43.690540625" x2="325.68515" y2="43.72483125" width="0.025" layer="94"/>
<wire x1="325.604" y1="43.68996875" x2="325.605140625" y2="43.690540625" width="0.025" layer="94"/>
<wire x1="325.34225" y1="43.54195" x2="325.45655" y2="43.61053125" width="0.025" layer="94"/>
<wire x1="325.341109375" y1="43.541190625" x2="325.34225" y2="43.54195" width="0.025" layer="94"/>
<wire x1="324.862190625" y1="43.15333125" x2="325.033640625" y2="43.32478125" width="0.025" layer="94"/>
<wire x1="324.8279" y1="43.107609375" x2="324.862190625" y2="43.15333125" width="0.025" layer="94"/>
<wire x1="324.75931875" y1="43.004740625" x2="324.8279" y2="43.107609375" width="0.025" layer="94"/>
<wire x1="324.633590625" y1="42.62755" x2="324.65645" y2="42.74185" width="0.025" layer="94"/>
<wire x1="324.633590625" y1="42.626409375" x2="324.633590625" y2="42.62755" width="0.025" layer="94"/>
<wire x1="324.690740625" y1="42.261790625" x2="324.70216875" y2="42.23893125" width="0.025" layer="94"/>
<wire x1="324.70216875" y1="42.23893125" x2="324.7136" y2="42.204640625" width="0.025" layer="94"/>
<wire x1="324.7136" y1="42.204640625" x2="324.72503125" y2="42.18178125" width="0.025" layer="94"/>
<wire x1="324.72503125" y1="42.18178125" x2="324.747890625" y2="42.147490625" width="0.025" layer="94"/>
<wire x1="324.793609375" y1="42.06748125" x2="324.87361875" y2="41.964609375" width="0.025" layer="94"/>
<wire x1="324.87361875" y1="41.964609375" x2="324.874759375" y2="41.96346875" width="0.025" layer="94"/>
<wire x1="325.84516875" y1="41.35881875" x2="325.969759375" y2="41.317290625" width="0.025" layer="94"/>
<wire x1="326.084059375" y1="41.279190625" x2="326.0852" y2="41.278809375" width="0.025" layer="94"/>
<wire x1="326.0852" y1="41.278809375" x2="326.119490625" y2="41.43883125" width="0.025" layer="94"/>
<wire x1="326.0852" y1="41.461690625" x2="326.119490625" y2="41.43883125" width="0.025" layer="94"/>
<wire x1="325.993759375" y1="41.49598125" x2="326.0852" y2="41.461690625" width="0.025" layer="94"/>
<wire x1="325.99261875" y1="41.49646875" x2="325.993759375" y2="41.49598125" width="0.025" layer="94"/>
<wire x1="325.650859375" y1="41.66743125" x2="325.652" y2="41.66678125" width="0.025" layer="94"/>
<wire x1="325.55941875" y1="41.72458125" x2="325.650859375" y2="41.66743125" width="0.025" layer="94"/>
<wire x1="325.44511875" y1="42.95901875" x2="325.536559375" y2="43.0276" width="0.025" layer="94"/>
<wire x1="325.536559375" y1="43.0276" x2="325.5377" y2="43.028359375" width="0.025" layer="94"/>
<wire x1="325.605140625" y1="43.07331875" x2="325.776590625" y2="43.176190625" width="0.025" layer="94"/>
<wire x1="326.49668125" y1="43.43908125" x2="326.64526875" y2="43.47336875" width="0.025" layer="94"/>
<wire x1="326.75956875" y1="43.30191875" x2="326.81671875" y2="43.31335" width="0.025" layer="94"/>
<wire x1="326.71385" y1="43.290490625" x2="326.75956875" y2="43.30191875" width="0.025" layer="94"/>
<wire x1="326.6567" y1="43.279059375" x2="326.71385" y2="43.290490625" width="0.025" layer="94"/>
<wire x1="326.565259375" y1="43.2562" x2="326.6567" y2="43.279059375" width="0.025" layer="94"/>
<wire x1="326.35951875" y1="43.18761875" x2="326.41666875" y2="43.21048125" width="0.025" layer="94"/>
<wire x1="326.32523125" y1="43.176190625" x2="326.35951875" y2="43.18761875" width="0.025" layer="94"/>
<wire x1="326.21093125" y1="43.13046875" x2="326.32523125" y2="43.176190625" width="0.025" layer="94"/>
<wire x1="326.005190625" y1="43.0276" x2="326.21093125" y2="43.13046875" width="0.025" layer="94"/>
<wire x1="325.547609375" y1="42.09148125" x2="325.547990625" y2="42.090340625" width="0.025" layer="94"/>
<wire x1="325.547990625" y1="42.090340625" x2="325.57085" y2="42.04461875" width="0.025" layer="94"/>
<wire x1="325.84516875" y1="41.747440625" x2="325.95946875" y2="41.678859375" width="0.025" layer="94"/>
<wire x1="325.95946875" y1="41.678859375" x2="326.03948125" y2="41.633140625" width="0.025" layer="94"/>
<wire x1="326.03948125" y1="41.633140625" x2="326.108059375" y2="41.59885" width="0.025" layer="94"/>
<wire x1="326.108059375" y1="41.59885" x2="326.165209375" y2="41.575990625" width="0.025" layer="94"/>
<wire x1="326.165209375" y1="41.575990625" x2="326.35951875" y2="42.307509375" width="0.025" layer="94"/>
<wire x1="329.022709375" y1="41.575990625" x2="329.04556875" y2="41.575990625" width="0.025" layer="94"/>
<wire x1="329.04556875" y1="41.575990625" x2="329.251309375" y2="41.678859375" width="0.025" layer="94"/>
<wire x1="329.251309375" y1="41.678859375" x2="329.25245" y2="41.67961875" width="0.025" layer="94"/>
<wire x1="329.42161875" y1="41.7924" x2="329.422759375" y2="41.793159375" width="0.025" layer="94"/>
<wire x1="329.422759375" y1="41.793159375" x2="329.548490625" y2="41.918890625" width="0.025" layer="94"/>
<wire x1="329.548490625" y1="41.918890625" x2="329.58278125" y2="41.964609375" width="0.025" layer="94"/>
<wire x1="329.58278125" y1="41.964609375" x2="329.6285" y2="42.033190625" width="0.025" layer="94"/>
<wire x1="329.662790625" y1="42.1132" x2="329.68565" y2="42.18178125" width="0.025" layer="94"/>
<wire x1="329.68565" y1="42.18178125" x2="329.69708125" y2="42.2275" width="0.025" layer="94"/>
<wire x1="329.57135" y1="42.718990625" x2="329.61706875" y2="42.650409375" width="0.025" layer="94"/>
<wire x1="329.537059375" y1="42.764709375" x2="329.57135" y2="42.718990625" width="0.025" layer="94"/>
<wire x1="329.434190625" y1="42.86758125" x2="329.43533125" y2="42.866440625" width="0.025" layer="94"/>
<wire x1="329.377040625" y1="42.9133" x2="329.434190625" y2="42.86758125" width="0.025" layer="94"/>
<wire x1="329.01128125" y1="43.119040625" x2="329.06843125" y2="43.09618125" width="0.025" layer="94"/>
<wire x1="328.965559375" y1="43.1419" x2="329.01128125" y2="43.119040625" width="0.025" layer="94"/>
<wire x1="328.32548125" y1="43.32478125" x2="328.43978125" y2="43.30191875" width="0.025" layer="94"/>
<wire x1="328.2569" y1="43.336209375" x2="328.32548125" y2="43.32478125" width="0.025" layer="94"/>
<wire x1="328.15403125" y1="43.347640625" x2="328.2569" y2="43.336209375" width="0.025" layer="94"/>
<wire x1="328.005440625" y1="43.35906875" x2="328.15403125" y2="43.347640625" width="0.025" layer="94"/>
<wire x1="328.005440625" y1="43.35906875" x2="328.005440625" y2="43.53051875" width="0.025" layer="94"/>
<wire x1="328.78268125" y1="43.41621875" x2="329.010140625" y2="43.34798125" width="0.025" layer="94"/>
<wire x1="329.010140625" y1="43.34798125" x2="329.01128125" y2="43.347640625" width="0.025" layer="94"/>
<wire x1="329.01128125" y1="43.347640625" x2="329.194159375" y2="43.279059375" width="0.025" layer="94"/>
<wire x1="329.194159375" y1="43.279059375" x2="329.308459375" y2="43.233340625" width="0.025" layer="94"/>
<wire x1="329.308459375" y1="43.233340625" x2="329.3096" y2="43.23276875" width="0.025" layer="94"/>
<wire x1="329.81138125" y1="42.9133" x2="329.891390625" y2="42.833290625" width="0.025" layer="94"/>
<wire x1="329.891390625" y1="42.833290625" x2="329.937109375" y2="42.776140625" width="0.025" layer="94"/>
<wire x1="329.95996875" y1="42.74185" x2="329.994259375" y2="42.6847" width="0.025" layer="94"/>
<wire x1="329.994259375" y1="42.6847" x2="330.03998125" y2="42.5704" width="0.025" layer="94"/>
<wire x1="330.03998125" y1="42.5704" x2="330.051409375" y2="42.51325" width="0.025" layer="94"/>
<wire x1="330.051409375" y1="42.51325" x2="330.062840625" y2="42.421809375" width="0.025" layer="94"/>
<wire x1="329.55991875" y1="41.66743125" x2="329.6285" y2="41.71315" width="0.025" layer="94"/>
<wire x1="329.45705" y1="41.61028125" x2="329.55991875" y2="41.66743125" width="0.025" layer="94"/>
<wire x1="329.15986875" y1="41.47311875" x2="329.1633" y2="41.474590625" width="0.025" layer="94"/>
<wire x1="329.06843125" y1="41.43883125" x2="329.15986875" y2="41.47311875" width="0.025" layer="94"/>
<wire x1="329.137009375" y1="41.290240625" x2="329.18273125" y2="41.30166875" width="0.025" layer="94"/>
<wire x1="329.18273125" y1="41.30166875" x2="329.21701875" y2="41.3131" width="0.025" layer="94"/>
<wire x1="329.21701875" y1="41.3131" x2="329.262740625" y2="41.32453125" width="0.025" layer="94"/>
<wire x1="329.262740625" y1="41.32453125" x2="329.3999" y2="41.37025" width="0.025" layer="94"/>
<wire x1="329.57135" y1="41.43883125" x2="329.605640625" y2="41.450259375" width="0.025" layer="94"/>
<wire x1="329.605640625" y1="41.450259375" x2="329.834240625" y2="41.564559375" width="0.025" layer="94"/>
<wire x1="329.834240625" y1="41.564559375" x2="329.86853125" y2="41.58741875" width="0.025" layer="94"/>
<wire x1="329.86853125" y1="41.58741875" x2="329.891390625" y2="41.59885" width="0.025" layer="94"/>
<wire x1="330.30286875" y1="41.93031875" x2="330.348590625" y2="41.98746875" width="0.025" layer="94"/>
<wire x1="330.348590625" y1="41.98746875" x2="330.37145" y2="42.01033125" width="0.025" layer="94"/>
<wire x1="330.405740625" y1="42.05605" x2="330.47431875" y2="42.17035" width="0.025" layer="94"/>
<wire x1="330.47431875" y1="42.17035" x2="330.520040625" y2="42.28465" width="0.025" layer="94"/>
<wire x1="330.54251875" y1="42.352090625" x2="330.5429" y2="42.35323125" width="0.025" layer="94"/>
<wire x1="330.5429" y1="42.35323125" x2="330.55433125" y2="42.41038125" width="0.025" layer="94"/>
<wire x1="330.55433125" y1="42.41038125" x2="330.565759375" y2="42.478959375" width="0.025" layer="94"/>
<wire x1="328.72553125" y1="44.22775" x2="328.95413125" y2="44.18203125" width="0.025" layer="94"/>
<wire x1="328.95413125" y1="44.18203125" x2="329.22845" y2="44.11345" width="0.025" layer="94"/>
<wire x1="329.69708125" y1="43.95343125" x2="329.7428" y2="43.93056875" width="0.025" layer="94"/>
<wire x1="329.7428" y1="43.93056875" x2="329.79995" y2="43.907709375" width="0.025" layer="94"/>
<wire x1="329.79995" y1="43.907709375" x2="329.84566875" y2="43.88485" width="0.025" layer="94"/>
<wire x1="329.84566875" y1="43.88485" x2="329.90281875" y2="43.861990625" width="0.025" layer="94"/>
<wire x1="329.90281875" y1="43.861990625" x2="330.03998125" y2="43.793409375" width="0.025" layer="94"/>
<wire x1="330.03998125" y1="43.793409375" x2="330.09713125" y2="43.75911875" width="0.025" layer="94"/>
<wire x1="330.09713125" y1="43.75911875" x2="330.14285" y2="43.736259375" width="0.025" layer="94"/>
<wire x1="330.55433125" y1="43.43908125" x2="330.60005" y2="43.393359375" width="0.025" layer="94"/>
<wire x1="330.60005" y1="43.393359375" x2="330.66863125" y2="43.31335" width="0.025" layer="94"/>
<wire x1="330.66863125" y1="43.31335" x2="330.70291875" y2="43.279059375" width="0.025" layer="94"/>
<wire x1="330.805790625" y1="42.193209375" x2="330.82865" y2="42.23893125" width="0.025" layer="94"/>
<wire x1="330.7715" y1="42.136059375" x2="330.805790625" y2="42.193209375" width="0.025" layer="94"/>
<wire x1="330.70291875" y1="42.04461875" x2="330.7715" y2="42.136059375" width="0.025" layer="94"/>
<wire x1="329.29703125" y1="41.233090625" x2="329.594209375" y2="41.32453125" width="0.025" layer="94"/>
<wire x1="329.1713" y1="41.03878125" x2="329.18273125" y2="41.03878125" width="0.025" layer="94"/>
<wire x1="329.18273125" y1="41.03878125" x2="329.26616875" y2="41.059640625" width="0.025" layer="94"/>
<wire x1="329.26616875" y1="41.059640625" x2="329.50276875" y2="41.118790625" width="0.025" layer="94"/>
<wire x1="329.50276875" y1="41.118790625" x2="329.7428" y2="41.18736875" width="0.025" layer="94"/>
<wire x1="329.7428" y1="41.18736875" x2="329.743940625" y2="41.18775" width="0.025" layer="94"/>
<wire x1="329.948540625" y1="41.25595" x2="330.005690625" y2="41.278809375" width="0.025" layer="94"/>
<wire x1="330.005690625" y1="41.278809375" x2="330.03998125" y2="41.290240625" width="0.025" layer="94"/>
<wire x1="330.60005" y1="41.564559375" x2="330.634340625" y2="41.58741875" width="0.025" layer="94"/>
<wire x1="330.634340625" y1="41.58741875" x2="330.691490625" y2="41.621709375" width="0.025" layer="94"/>
<wire x1="330.691490625" y1="41.621709375" x2="330.737209375" y2="41.656" width="0.025" layer="94"/>
<wire x1="331.2287" y1="42.06748125" x2="331.27441875" y2="42.12463125" width="0.025" layer="94"/>
<wire x1="331.27441875" y1="42.12463125" x2="331.308709375" y2="42.17035" width="0.025" layer="94"/>
<wire x1="331.33156875" y1="42.204640625" x2="331.343" y2="42.2275" width="0.025" layer="94"/>
<wire x1="331.343" y1="42.2275" x2="331.365859375" y2="42.261790625" width="0.025" layer="94"/>
<wire x1="331.50301875" y1="42.97045" x2="331.51445" y2="42.84471875" width="0.025" layer="94"/>
<wire x1="331.365859375" y1="43.3705" x2="331.36643125" y2="43.369359375" width="0.025" layer="94"/>
<wire x1="331.27441875" y1="43.507659375" x2="331.365859375" y2="43.3705" width="0.025" layer="94"/>
<wire x1="331.24013125" y1="43.55338125" x2="331.27441875" y2="43.507659375" width="0.025" layer="94"/>
<wire x1="330.61148125" y1="44.090590625" x2="330.66863125" y2="44.0563" width="0.025" layer="94"/>
<wire x1="330.610340625" y1="44.09135" x2="330.61148125" y2="44.090590625" width="0.025" layer="94"/>
<wire x1="329.95996875" y1="44.3992" x2="330.24571875" y2="44.2849" width="0.025" layer="94"/>
<wire x1="329.651359375" y1="44.50206875" x2="329.95996875" y2="44.3992" width="0.025" layer="94"/>
<wire x1="329.491340625" y1="44.547790625" x2="329.651359375" y2="44.50206875" width="0.025" layer="94"/>
<wire x1="329.21701875" y1="44.61636875" x2="329.491340625" y2="44.547790625" width="0.025" layer="94"/>
<wire x1="328.3712" y1="44.75353125" x2="328.47406875" y2="44.7421" width="0.025" layer="94"/>
<wire x1="328.2569" y1="44.764959375" x2="328.3712" y2="44.75353125" width="0.025" layer="94"/>
<wire x1="328.222609375" y1="44.764959375" x2="328.2569" y2="44.764959375" width="0.025" layer="94"/>
<wire x1="327.58253125" y1="40.78731875" x2="328.519790625" y2="36.21531875" width="0.025" layer="94"/>
<wire x1="326.690990625" y1="36.21531875" x2="327.58253125" y2="40.78731875" width="0.025" layer="94"/>
<wire x1="326.690990625" y1="36.21531875" x2="326.86038125" y2="37.084" width="0.025" layer="94"/>
<wire x1="324.9746" y1="37.084" x2="323.764909375" y2="32.512" width="0.025" layer="94"/>
<wire x1="323.764909375" y1="32.512" x2="325.8566" y2="32.512" width="0.025" layer="94"/>
<wire x1="325.8566" y1="32.512" x2="326.336659375" y2="34.5694" width="0.025" layer="94"/>
<wire x1="326.336659375" y1="34.5694" x2="328.862690625" y2="34.5694" width="0.025" layer="94"/>
<wire x1="328.862690625" y1="34.5694" x2="329.319890625" y2="32.512" width="0.025" layer="94"/>
<wire x1="329.319890625" y1="32.512" x2="331.44586875" y2="32.512" width="0.025" layer="94"/>
<wire x1="331.44586875" y1="32.512" x2="330.22376875" y2="37.084" width="0.025" layer="94"/>
<wire x1="328.341709375" y1="37.084" x2="328.35108125" y2="37.03828125" width="0.025" layer="94"/>
<wire x1="328.35108125" y1="37.03828125" x2="328.519790625" y2="36.21531875" width="0.025" layer="94"/>
<wire x1="328.519790625" y1="36.21531875" x2="326.690990625" y2="36.21531875" width="0.025" layer="94"/>
<wire x1="314.41516875" y1="42.29608125" x2="314.41516875" y2="32.512" width="0.025" layer="94"/>
<wire x1="314.41516875" y1="32.512" x2="316.26683125" y2="32.512" width="0.025" layer="94"/>
<wire x1="316.26683125" y1="32.512" x2="316.26683125" y2="40.21581875" width="0.025" layer="94"/>
<wire x1="316.26683125" y1="40.21581875" x2="317.98133125" y2="32.512" width="0.025" layer="94"/>
<wire x1="317.98133125" y1="32.512" x2="319.55866875" y2="32.512" width="0.025" layer="94"/>
<wire x1="319.55866875" y1="32.512" x2="321.250309375" y2="40.21581875" width="0.025" layer="94"/>
<wire x1="321.250309375" y1="40.21581875" x2="321.250309375" y2="32.512" width="0.025" layer="94"/>
<wire x1="321.250309375" y1="32.512" x2="323.12483125" y2="32.512" width="0.025" layer="94"/>
<wire x1="323.12483125" y1="32.512" x2="323.12483125" y2="42.29608125" width="0.025" layer="94"/>
<wire x1="323.12483125" y1="42.29608125" x2="320.15303125" y2="42.29608125" width="0.025" layer="94"/>
<wire x1="320.15303125" y1="42.29608125" x2="318.767990625" y2="35.575240625" width="0.025" layer="94"/>
<wire x1="318.767990625" y1="35.575240625" x2="317.364109375" y2="42.29608125" width="0.025" layer="94"/>
<wire x1="317.364109375" y1="42.29608125" x2="314.41516875" y2="42.29608125" width="0.025" layer="94"/>
<wire x1="312.83783125" y1="35.392359375" x2="312.83783125" y2="42.3418" width="0.025" layer="94"/>
<wire x1="312.83783125" y1="42.3418" x2="310.78043125" y2="42.3418" width="0.025" layer="94"/>
<wire x1="310.78043125" y1="42.3418" x2="310.78043125" y2="35.1409" width="0.025" layer="94"/>
<wire x1="310.78043125" y1="35.1409" x2="310.75756875" y2="35.0266" width="0.025" layer="94"/>
<wire x1="310.75756875" y1="35.0266" x2="310.71185" y2="34.889440625" width="0.025" layer="94"/>
<wire x1="310.71185" y1="34.889440625" x2="310.63346875" y2="34.75228125" width="0.025" layer="94"/>
<wire x1="310.63346875" y1="34.75228125" x2="310.620409375" y2="34.72941875" width="0.025" layer="94"/>
<wire x1="310.620409375" y1="34.72941875" x2="310.55183125" y2="34.660840625" width="0.025" layer="94"/>
<wire x1="310.55183125" y1="34.660840625" x2="310.506109375" y2="34.619690625" width="0.025" layer="94"/>
<wire x1="310.506109375" y1="34.619690625" x2="310.41466875" y2="34.546540625" width="0.025" layer="94"/>
<wire x1="310.41466875" y1="34.546540625" x2="310.30036875" y2="34.477959375" width="0.025" layer="94"/>
<wire x1="310.30036875" y1="34.477959375" x2="310.18606875" y2="34.432240625" width="0.025" layer="94"/>
<wire x1="310.18606875" y1="34.432240625" x2="310.02605" y2="34.38651875" width="0.025" layer="94"/>
<wire x1="310.02605" y1="34.38651875" x2="309.84316875" y2="34.363659375" width="0.025" layer="94"/>
<wire x1="309.84316875" y1="34.363659375" x2="309.660290625" y2="34.363659375" width="0.025" layer="94"/>
<wire x1="309.660290625" y1="34.363659375" x2="309.56885" y2="34.3789" width="0.025" layer="94"/>
<wire x1="309.56885" y1="34.3789" x2="309.52313125" y2="34.38651875" width="0.025" layer="94"/>
<wire x1="309.52313125" y1="34.38651875" x2="309.477409375" y2="34.39958125" width="0.025" layer="94"/>
<wire x1="309.477409375" y1="34.39958125" x2="309.431690625" y2="34.41265" width="0.025" layer="94"/>
<wire x1="309.431690625" y1="34.41265" x2="309.363109375" y2="34.432240625" width="0.025" layer="94"/>
<wire x1="309.363109375" y1="34.432240625" x2="309.22595" y2="34.50081875" width="0.025" layer="94"/>
<wire x1="309.22595" y1="34.50081875" x2="309.088790625" y2="34.592259375" width="0.025" layer="94"/>
<wire x1="309.088790625" y1="34.592259375" x2="308.95163125" y2="34.72941875" width="0.025" layer="94"/>
<wire x1="308.95163125" y1="34.72941875" x2="308.70733125" y2="35.05515" width="0.025" layer="94"/>
<wire x1="308.70733125" y1="35.05515" x2="307.145690625" y2="33.81501875" width="0.025" layer="94"/>
<wire x1="307.145690625" y1="33.81501875" x2="307.259990625" y2="33.58641875" width="0.025" layer="94"/>
<wire x1="307.259990625" y1="33.58641875" x2="307.305709375" y2="33.525459375" width="0.025" layer="94"/>
<wire x1="307.305709375" y1="33.525459375" x2="307.34571875" y2="33.47211875" width="0.025" layer="94"/>
<wire x1="307.34571875" y1="33.47211875" x2="307.39715" y2="33.403540625" width="0.025" layer="94"/>
<wire x1="307.39715" y1="33.403540625" x2="307.43935" y2="33.35781875" width="0.025" layer="94"/>
<wire x1="307.43935" y1="33.35781875" x2="307.488590625" y2="33.30448125" width="0.025" layer="94"/>
<wire x1="307.488590625" y1="33.30448125" x2="307.523759375" y2="33.26638125" width="0.025" layer="94"/>
<wire x1="307.523759375" y1="33.26638125" x2="307.55716875" y2="33.23018125" width="0.025" layer="94"/>
<wire x1="307.55716875" y1="33.23018125" x2="307.587059375" y2="33.1978" width="0.025" layer="94"/>
<wire x1="307.587059375" y1="33.1978" x2="307.62575" y2="33.155890625" width="0.025" layer="94"/>
<wire x1="307.62575" y1="33.155890625" x2="307.65036875" y2="33.12921875" width="0.025" layer="94"/>
<wire x1="307.65036875" y1="33.12921875" x2="307.67146875" y2="33.106359375" width="0.025" layer="94"/>
<wire x1="307.67146875" y1="33.106359375" x2="307.762909375" y2="33.02323125" width="0.025" layer="94"/>
<wire x1="307.762909375" y1="33.02323125" x2="307.7972" y2="32.992059375" width="0.025" layer="94"/>
<wire x1="307.7972" y1="32.992059375" x2="307.85435" y2="32.940109375" width="0.025" layer="94"/>
<wire x1="307.85435" y1="32.940109375" x2="307.89778125" y2="32.90061875" width="0.025" layer="94"/>
<wire x1="307.89778125" y1="32.90061875" x2="307.92293125" y2="32.877759375" width="0.025" layer="94"/>
<wire x1="307.92293125" y1="32.877759375" x2="308.12866875" y2="32.7406" width="0.025" layer="94"/>
<wire x1="308.12866875" y1="32.7406" x2="308.174390625" y2="32.71011875" width="0.025" layer="94"/>
<wire x1="308.174390625" y1="32.71011875" x2="308.24296875" y2="32.6705" width="0.025" layer="94"/>
<wire x1="308.24296875" y1="32.6705" x2="308.288690625" y2="32.646109375" width="0.025" layer="94"/>
<wire x1="308.288690625" y1="32.646109375" x2="308.3687" y2="32.603440625" width="0.025" layer="94"/>
<wire x1="308.3687" y1="32.603440625" x2="308.42585" y2="32.572959375" width="0.025" layer="94"/>
<wire x1="308.42585" y1="32.572959375" x2="308.54015" y2="32.512" width="0.025" layer="94"/>
<wire x1="308.54015" y1="32.512" x2="308.66206875" y2="32.46628125" width="0.025" layer="94"/>
<wire x1="308.66206875" y1="32.46628125" x2="308.72303125" y2="32.44341875" width="0.025" layer="94"/>
<wire x1="308.72303125" y1="32.44341875" x2="308.905909375" y2="32.374840625" width="0.025" layer="94"/>
<wire x1="308.905909375" y1="32.374840625" x2="308.99735" y2="32.353740625" width="0.025" layer="94"/>
<wire x1="308.99735" y1="32.353740625" x2="309.203090625" y2="32.306259375" width="0.025" layer="94"/>
<wire x1="309.203090625" y1="32.306259375" x2="309.27166875" y2="32.295709375" width="0.025" layer="94"/>
<wire x1="309.27166875" y1="32.295709375" x2="309.35168125" y2="32.2834" width="0.025" layer="94"/>
<wire x1="309.35168125" y1="32.2834" x2="309.40883125" y2="32.274609375" width="0.025" layer="94"/>
<wire x1="309.40883125" y1="32.274609375" x2="309.477409375" y2="32.264059375" width="0.025" layer="94"/>
<wire x1="309.477409375" y1="32.264059375" x2="309.50026875" y2="32.260540625" width="0.025" layer="94"/>
<wire x1="309.50026875" y1="32.260540625" x2="309.68315" y2="32.2453" width="0.025" layer="94"/>
<wire x1="309.68315" y1="32.2453" x2="309.774590625" y2="32.23768125" width="0.025" layer="94"/>
<wire x1="309.774590625" y1="32.23768125" x2="310.048909375" y2="32.260540625" width="0.025" layer="94"/>
<wire x1="310.048909375" y1="32.260540625" x2="310.231790625" y2="32.2834" width="0.025" layer="94"/>
<wire x1="310.231790625" y1="32.2834" x2="310.460390625" y2="32.32911875" width="0.025" layer="94"/>
<wire x1="310.460390625" y1="32.32911875" x2="310.620409375" y2="32.374840625" width="0.025" layer="94"/>
<wire x1="310.620409375" y1="32.374840625" x2="310.82615" y2="32.44341875" width="0.025" layer="94"/>
<wire x1="310.82615" y1="32.44341875" x2="311.05475" y2="32.534859375" width="0.025" layer="94"/>
<wire x1="311.05475" y1="32.534859375" x2="311.374790625" y2="32.69488125" width="0.025" layer="94"/>
<wire x1="311.374790625" y1="32.69488125" x2="311.58053125" y2="32.832040625" width="0.025" layer="94"/>
<wire x1="311.58053125" y1="32.832040625" x2="311.649109375" y2="32.889190625" width="0.025" layer="94"/>
<wire x1="311.649109375" y1="32.889190625" x2="311.69483125" y2="32.927290625" width="0.025" layer="94"/>
<wire x1="311.69483125" y1="32.927290625" x2="311.763409375" y2="32.984440625" width="0.025" layer="94"/>
<wire x1="311.763409375" y1="32.984440625" x2="311.85485" y2="33.060640625" width="0.025" layer="94"/>
<wire x1="311.85485" y1="33.060640625" x2="312.106309375" y2="33.3121" width="0.025" layer="94"/>
<wire x1="312.106309375" y1="33.3121" x2="312.15203125" y2="33.373059375" width="0.025" layer="94"/>
<wire x1="312.15203125" y1="33.373059375" x2="312.174890625" y2="33.403540625" width="0.025" layer="94"/>
<wire x1="312.174890625" y1="33.403540625" x2="312.220609375" y2="33.4645" width="0.025" layer="94"/>
<wire x1="312.220609375" y1="33.4645" x2="312.258709375" y2="33.517840625" width="0.025" layer="94"/>
<wire x1="312.258709375" y1="33.517840625" x2="312.30443125" y2="33.58641875" width="0.025" layer="94"/>
<wire x1="312.30443125" y1="33.58641875" x2="312.334909375" y2="33.632140625" width="0.025" layer="94"/>
<wire x1="312.334909375" y1="33.632140625" x2="312.39586875" y2="33.72358125" width="0.025" layer="94"/>
<wire x1="312.39586875" y1="33.72358125" x2="312.42635" y2="33.7693" width="0.025" layer="94"/>
<wire x1="312.42635" y1="33.7693" x2="312.60923125" y2="34.135059375" width="0.025" layer="94"/>
<wire x1="312.60923125" y1="34.135059375" x2="312.65495" y2="34.249359375" width="0.025" layer="94"/>
<wire x1="312.65495" y1="34.249359375" x2="312.68108125" y2="34.3408" width="0.025" layer="94"/>
<wire x1="312.68108125" y1="34.3408" x2="312.7121" y2="34.4551" width="0.025" layer="94"/>
<wire x1="312.7121" y1="34.4551" x2="312.746390625" y2="34.61511875" width="0.025" layer="94"/>
<wire x1="312.746390625" y1="34.61511875" x2="312.76925" y2="34.72941875" width="0.025" layer="94"/>
<wire x1="312.76925" y1="34.72941875" x2="312.792109375" y2="34.889440625" width="0.025" layer="94"/>
<wire x1="312.792109375" y1="34.889440625" x2="312.809890625" y2="35.049459375" width="0.025" layer="94"/>
<wire x1="312.809890625" y1="35.049459375" x2="312.81673125" y2="35.118040625" width="0.025" layer="94"/>
<wire x1="312.81673125" y1="35.118040625" x2="312.823759375" y2="35.20948125" width="0.025" layer="94"/>
<wire x1="312.823759375" y1="35.20948125" x2="312.829040625" y2="35.278059375" width="0.025" layer="94"/>
<wire x1="312.829040625" y1="35.278059375" x2="312.83783125" y2="35.392359375" width="0.025" layer="94"/>
<wire x1="324.9625" y1="37.03828125" x2="325.251490625" y2="38.13625" width="0.025" layer="94"/>
<wire x1="325.251490625" y1="38.13625" x2="325.993759375" y2="40.935909375" width="0.025" layer="94"/>
<wire x1="325.993759375" y1="40.935909375" x2="325.90231875" y2="40.95876875" width="0.025" layer="94"/>
<wire x1="325.90231875" y1="40.95876875" x2="325.78801875" y2="40.98163125" width="0.025" layer="94"/>
<wire x1="325.78801875" y1="40.98163125" x2="325.63943125" y2="41.01591875" width="0.025" layer="94"/>
<wire x1="325.63943125" y1="41.01591875" x2="325.536559375" y2="41.03878125" width="0.025" layer="94"/>
<wire x1="325.536559375" y1="41.03878125" x2="325.3994" y2="41.07306875" width="0.025" layer="94"/>
<wire x1="325.3994" y1="41.07306875" x2="325.319390625" y2="41.09593125" width="0.025" layer="94"/>
<wire x1="325.319390625" y1="41.09593125" x2="325.21651875" y2="41.13021875" width="0.025" layer="94"/>
<wire x1="325.21651875" y1="41.13021875" x2="325.06793125" y2="41.175940625" width="0.025" layer="94"/>
<wire x1="325.06793125" y1="41.175940625" x2="324.97076875" y2="41.21251875" width="0.025" layer="94"/>
<wire x1="324.97076875" y1="41.21251875" x2="324.747890625" y2="41.30166875" width="0.025" layer="94"/>
<wire x1="324.747890625" y1="41.30166875" x2="324.565009375" y2="41.38168125" width="0.025" layer="94"/>
<wire x1="324.565009375" y1="41.38168125" x2="324.45185" y2="41.438259375" width="0.025" layer="94"/>
<wire x1="324.45185" y1="41.438259375" x2="324.393559375" y2="41.47311875" width="0.025" layer="94"/>
<wire x1="324.393559375" y1="41.47311875" x2="324.347840625" y2="41.49598125" width="0.025" layer="94"/>
<wire x1="324.347840625" y1="41.49598125" x2="324.18781875" y2="41.58741875" width="0.025" layer="94"/>
<wire x1="324.18781875" y1="41.58741875" x2="324.08495" y2="41.656" width="0.025" layer="94"/>
<wire x1="324.08495" y1="41.656" x2="323.85635" y2="41.82745" width="0.025" layer="94"/>
<wire x1="323.85635" y1="41.82745" x2="323.776340625" y2="41.89603125" width="0.025" layer="94"/>
<wire x1="323.776340625" y1="41.89603125" x2="323.662040625" y2="42.01033125" width="0.025" layer="94"/>
<wire x1="323.662040625" y1="42.01033125" x2="323.593459375" y2="42.090340625" width="0.025" layer="94"/>
<wire x1="323.593459375" y1="42.090340625" x2="323.547740625" y2="42.147490625" width="0.025" layer="94"/>
<wire x1="323.547740625" y1="42.147490625" x2="323.52488125" y2="42.18178125" width="0.025" layer="94"/>
<wire x1="323.52488125" y1="42.18178125" x2="323.490590625" y2="42.2275" width="0.025" layer="94"/>
<wire x1="323.490590625" y1="42.2275" x2="323.46773125" y2="42.261790625" width="0.025" layer="94"/>
<wire x1="323.46773125" y1="42.261790625" x2="323.433440625" y2="42.318940625" width="0.025" layer="94"/>
<wire x1="323.433440625" y1="42.318940625" x2="323.41058125" y2="42.35323125" width="0.025" layer="94"/>
<wire x1="323.41058125" y1="42.35323125" x2="323.354" y2="42.466390625" width="0.025" layer="94"/>
<wire x1="323.354" y1="42.466390625" x2="323.33056875" y2="42.52468125" width="0.025" layer="94"/>
<wire x1="323.33056875" y1="42.52468125" x2="323.29628125" y2="42.62755" width="0.025" layer="94"/>
<wire x1="323.29628125" y1="42.62755" x2="323.28485" y2="42.67326875" width="0.025" layer="94"/>
<wire x1="323.28485" y1="42.67326875" x2="323.27341875" y2="42.73041875" width="0.025" layer="94"/>
<wire x1="323.27341875" y1="42.73041875" x2="323.261990625" y2="42.81043125" width="0.025" layer="94"/>
<wire x1="323.261990625" y1="42.81043125" x2="323.250559375" y2="42.936159375" width="0.025" layer="94"/>
<wire x1="323.250559375" y1="42.936159375" x2="323.261990625" y2="43.061890625" width="0.025" layer="94"/>
<wire x1="323.261990625" y1="43.061890625" x2="323.27341875" y2="43.13046875" width="0.025" layer="94"/>
<wire x1="323.27341875" y1="43.13046875" x2="323.28485" y2="43.18761875" width="0.025" layer="94"/>
<wire x1="323.28485" y1="43.18761875" x2="323.29628125" y2="43.233340625" width="0.025" layer="94"/>
<wire x1="323.29628125" y1="43.233340625" x2="323.33056875" y2="43.336209375" width="0.025" layer="94"/>
<wire x1="323.33056875" y1="43.336209375" x2="323.35343125" y2="43.393359375" width="0.025" layer="94"/>
<wire x1="323.35343125" y1="43.393359375" x2="323.41058125" y2="43.507659375" width="0.025" layer="94"/>
<wire x1="323.41058125" y1="43.507659375" x2="323.44486875" y2="43.564809375" width="0.025" layer="94"/>
<wire x1="323.44486875" y1="43.564809375" x2="323.479159375" y2="43.61053125" width="0.025" layer="94"/>
<wire x1="323.479159375" y1="43.61053125" x2="323.536309375" y2="43.690540625" width="0.025" layer="94"/>
<wire x1="323.536309375" y1="43.690540625" x2="323.62775" y2="43.793409375" width="0.025" layer="94"/>
<wire x1="323.62775" y1="43.793409375" x2="323.78776875" y2="43.95343125" width="0.025" layer="94"/>
<wire x1="323.78776875" y1="43.95343125" x2="323.87806875" y2="44.0323" width="0.025" layer="94"/>
<wire x1="323.87806875" y1="44.0323" x2="323.9375" y2="44.08001875" width="0.025" layer="94"/>
<wire x1="323.9375" y1="44.08001875" x2="324.0278" y2="44.147740625" width="0.025" layer="94"/>
<wire x1="324.0278" y1="44.147740625" x2="324.164959375" y2="44.23918125" width="0.025" layer="94"/>
<wire x1="324.164959375" y1="44.23918125" x2="324.290690625" y2="44.319190625" width="0.025" layer="94"/>
<wire x1="324.290690625" y1="44.319190625" x2="324.393559375" y2="44.376340625" width="0.025" layer="94"/>
<wire x1="324.393559375" y1="44.376340625" x2="324.62101875" y2="44.49006875" width="0.025" layer="94"/>
<wire x1="324.62101875" y1="44.49006875" x2="324.72503125" y2="44.536359375" width="0.025" layer="94"/>
<wire x1="324.72503125" y1="44.536359375" x2="325.009640625" y2="44.6502" width="0.025" layer="94"/>
<wire x1="325.009640625" y1="44.6502" x2="325.10908125" y2="44.68343125" width="0.025" layer="94"/>
<wire x1="325.10908125" y1="44.68343125" x2="325.287390625" y2="44.742859375" width="0.025" layer="94"/>
<wire x1="325.287390625" y1="44.742859375" x2="325.35368125" y2="44.764959375" width="0.025" layer="94"/>
<wire x1="325.35368125" y1="44.764959375" x2="325.43483125" y2="44.7881" width="0.025" layer="94"/>
<wire x1="325.43483125" y1="44.7881" x2="325.5137" y2="44.81068125" width="0.025" layer="94"/>
<wire x1="325.5137" y1="44.81068125" x2="325.741159375" y2="44.867540625" width="0.025" layer="94"/>
<wire x1="325.741159375" y1="44.867540625" x2="325.84516875" y2="44.890690625" width="0.025" layer="94"/>
<wire x1="325.84516875" y1="44.890690625" x2="326.07376875" y2="44.936409375" width="0.025" layer="94"/>
<wire x1="326.07376875" y1="44.936409375" x2="326.14235" y2="44.947840625" width="0.025" layer="94"/>
<wire x1="326.14235" y1="44.947840625" x2="326.222359375" y2="44.95926875" width="0.025" layer="94"/>
<wire x1="326.222359375" y1="44.95926875" x2="326.35951875" y2="44.98213125" width="0.025" layer="94"/>
<wire x1="326.35951875" y1="44.98213125" x2="326.43953125" y2="44.993559375" width="0.025" layer="94"/>
<wire x1="326.43953125" y1="44.993559375" x2="326.622409375" y2="45.01641875" width="0.025" layer="94"/>
<wire x1="326.622409375" y1="45.01641875" x2="326.72528125" y2="45.02785" width="0.025" layer="94"/>
<wire x1="326.72528125" y1="45.02785" x2="326.862440625" y2="45.03928125" width="0.025" layer="94"/>
<wire x1="326.862440625" y1="45.03928125" x2="326.862440625" y2="44.764959375" width="0.025" layer="94"/>
<wire x1="326.862440625" y1="44.764959375" x2="326.75956875" y2="44.75353125" width="0.025" layer="94"/>
<wire x1="326.75956875" y1="44.75353125" x2="326.519540625" y2="44.719240625" width="0.025" layer="94"/>
<wire x1="326.519540625" y1="44.719240625" x2="326.450959375" y2="44.707809375" width="0.025" layer="94"/>
<wire x1="326.450959375" y1="44.707809375" x2="326.32523125" y2="44.68495" width="0.025" layer="94"/>
<wire x1="326.32523125" y1="44.68495" x2="326.09663125" y2="44.63923125" width="0.025" layer="94"/>
<wire x1="326.09663125" y1="44.63923125" x2="325.993759375" y2="44.61636875" width="0.025" layer="94"/>
<wire x1="325.993759375" y1="44.61636875" x2="325.84516875" y2="44.58208125" width="0.025" layer="94"/>
<wire x1="325.84516875" y1="44.58208125" x2="325.67371875" y2="44.536359375" width="0.025" layer="94"/>
<wire x1="325.67371875" y1="44.536359375" x2="325.520559375" y2="44.4926" width="0.025" layer="94"/>
<wire x1="325.520559375" y1="44.4926" x2="325.4154" y2="44.45786875" width="0.025" layer="94"/>
<wire x1="325.4154" y1="44.45786875" x2="325.23938125" y2="44.3992" width="0.025" layer="94"/>
<wire x1="325.23938125" y1="44.3992" x2="325.06793125" y2="44.33061875" width="0.025" layer="94"/>
<wire x1="325.06793125" y1="44.33061875" x2="324.93076875" y2="44.27346875" width="0.025" layer="94"/>
<wire x1="324.93076875" y1="44.27346875" x2="324.679309375" y2="44.147740625" width="0.025" layer="94"/>
<wire x1="324.679309375" y1="44.147740625" x2="324.531859375" y2="44.05695" width="0.025" layer="94"/>
<wire x1="324.531859375" y1="44.05695" x2="324.462140625" y2="44.01058125" width="0.025" layer="94"/>
<wire x1="324.462140625" y1="44.01058125" x2="324.279259375" y2="43.87341875" width="0.025" layer="94"/>
<wire x1="324.279259375" y1="43.87341875" x2="324.164959375" y2="43.77055" width="0.025" layer="94"/>
<wire x1="324.164959375" y1="43.77055" x2="324.03923125" y2="43.64481875" width="0.025" layer="94"/>
<wire x1="324.03923125" y1="43.64481875" x2="323.97065" y2="43.564809375" width="0.025" layer="94"/>
<wire x1="323.97065" y1="43.564809375" x2="323.90206875" y2="43.47336875" width="0.025" layer="94"/>
<wire x1="323.90206875" y1="43.47336875" x2="323.86778125" y2="43.41621875" width="0.025" layer="94"/>
<wire x1="323.86778125" y1="43.41621875" x2="323.822059375" y2="43.336209375" width="0.025" layer="94"/>
<wire x1="323.822059375" y1="43.336209375" x2="323.78776875" y2="43.26763125" width="0.025" layer="94"/>
<wire x1="323.78776875" y1="43.26763125" x2="323.75348125" y2="43.176190625" width="0.025" layer="94"/>
<wire x1="323.75348125" y1="43.176190625" x2="323.73061875" y2="43.107609375" width="0.025" layer="94"/>
<wire x1="323.73061875" y1="43.107609375" x2="323.707759375" y2="43.004740625" width="0.025" layer="94"/>
<wire x1="323.707759375" y1="43.004740625" x2="323.69633125" y2="42.90186875" width="0.025" layer="94"/>
<wire x1="323.69633125" y1="42.90186875" x2="323.6849" y2="42.821859375" width="0.025" layer="94"/>
<wire x1="323.6849" y1="42.821859375" x2="323.6849" y2="42.776140625" width="0.025" layer="94"/>
<wire x1="323.6849" y1="42.776140625" x2="323.69633125" y2="42.650409375" width="0.025" layer="94"/>
<wire x1="323.69633125" y1="42.650409375" x2="323.73061875" y2="42.51325" width="0.025" layer="94"/>
<wire x1="323.73061875" y1="42.51325" x2="323.75348125" y2="42.433240625" width="0.025" layer="94"/>
<wire x1="323.75348125" y1="42.433240625" x2="323.7992" y2="42.33036875" width="0.025" layer="94"/>
<wire x1="323.7992" y1="42.33036875" x2="323.822059375" y2="42.28465" width="0.025" layer="94"/>
<wire x1="323.822059375" y1="42.28465" x2="323.890640625" y2="42.18178125" width="0.025" layer="94"/>
<wire x1="323.890640625" y1="42.18178125" x2="323.95921875" y2="42.090340625" width="0.025" layer="94"/>
<wire x1="323.95921875" y1="42.090340625" x2="324.004940625" y2="42.033190625" width="0.025" layer="94"/>
<wire x1="324.004940625" y1="42.033190625" x2="324.08495" y2="41.95318125" width="0.025" layer="94"/>
<wire x1="324.08495" y1="41.95318125" x2="324.176390625" y2="41.87316875" width="0.025" layer="94"/>
<wire x1="324.176390625" y1="41.87316875" x2="324.24496875" y2="41.81601875" width="0.025" layer="94"/>
<wire x1="324.24496875" y1="41.81601875" x2="324.30211875" y2="41.7703" width="0.025" layer="94"/>
<wire x1="324.30211875" y1="41.7703" x2="324.347840625" y2="41.736009375" width="0.025" layer="94"/>
<wire x1="324.347840625" y1="41.736009375" x2="324.41641875" y2="41.690290625" width="0.025" layer="94"/>
<wire x1="324.41641875" y1="41.690290625" x2="324.53071875" y2="41.61028125" width="0.025" layer="94"/>
<wire x1="324.53071875" y1="41.61028125" x2="324.64501875" y2="41.5417" width="0.025" layer="94"/>
<wire x1="324.64501875" y1="41.5417" x2="324.78218125" y2="41.461690625" width="0.025" layer="94"/>
<wire x1="324.78218125" y1="41.461690625" x2="324.919340625" y2="41.393109375" width="0.025" layer="94"/>
<wire x1="324.919340625" y1="41.393109375" x2="325.022209375" y2="41.347390625" width="0.025" layer="94"/>
<wire x1="325.022209375" y1="41.347390625" x2="325.079359375" y2="41.32453125" width="0.025" layer="94"/>
<wire x1="325.079359375" y1="41.32453125" x2="325.15936875" y2="41.290240625" width="0.025" layer="94"/>
<wire x1="325.15936875" y1="41.290240625" x2="325.250809375" y2="41.25595" width="0.025" layer="94"/>
<wire x1="325.250809375" y1="41.25595" x2="325.38796875" y2="41.21023125" width="0.025" layer="94"/>
<wire x1="325.38796875" y1="41.21023125" x2="325.46798125" y2="41.18736875" width="0.025" layer="94"/>
<wire x1="325.46798125" y1="41.18736875" x2="325.58228125" y2="41.15308125" width="0.025" layer="94"/>
<wire x1="325.58228125" y1="41.15308125" x2="325.708009375" y2="41.118790625" width="0.025" layer="94"/>
<wire x1="325.708009375" y1="41.118790625" x2="325.82116875" y2="41.087640625" width="0.025" layer="94"/>
<wire x1="325.82116875" y1="41.087640625" x2="326.01661875" y2="41.03878125" width="0.025" layer="94"/>
<wire x1="326.01661875" y1="41.03878125" x2="326.062340625" y2="41.18736875" width="0.025" layer="94"/>
<wire x1="326.062340625" y1="41.18736875" x2="326.02805" y2="41.1988" width="0.025" layer="94"/>
<wire x1="326.02805" y1="41.1988" x2="325.78801875" y2="41.26738125" width="0.025" layer="94"/>
<wire x1="325.78801875" y1="41.26738125" x2="325.5137" y2="41.35881875" width="0.025" layer="94"/>
<wire x1="325.5137" y1="41.35881875" x2="325.433690625" y2="41.393109375" width="0.025" layer="94"/>
<wire x1="325.433690625" y1="41.393109375" x2="325.319390625" y2="41.43883125" width="0.025" layer="94"/>
<wire x1="325.319390625" y1="41.43883125" x2="325.233659375" y2="41.481690625" width="0.025" layer="94"/>
<wire x1="325.233659375" y1="41.481690625" x2="325.10221875" y2="41.55313125" width="0.025" layer="94"/>
<wire x1="325.10221875" y1="41.55313125" x2="325.01078125" y2="41.614090625" width="0.025" layer="94"/>
<wire x1="325.01078125" y1="41.614090625" x2="324.93076875" y2="41.66743125" width="0.025" layer="94"/>
<wire x1="324.93076875" y1="41.66743125" x2="324.88505" y2="41.70171875" width="0.025" layer="94"/>
<wire x1="324.88505" y1="41.70171875" x2="324.8279" y2="41.747440625" width="0.025" layer="94"/>
<wire x1="324.8279" y1="41.747440625" x2="324.78218125" y2="41.78173125" width="0.025" layer="94"/>
<wire x1="324.78218125" y1="41.78173125" x2="324.66788125" y2="41.87316875" width="0.025" layer="94"/>
<wire x1="324.66788125" y1="41.87316875" x2="324.507859375" y2="42.033190625" width="0.025" layer="94"/>
<wire x1="324.507859375" y1="42.033190625" x2="324.47356875" y2="42.078909375" width="0.025" layer="94"/>
<wire x1="324.47356875" y1="42.078909375" x2="324.42785" y2="42.147490625" width="0.025" layer="94"/>
<wire x1="324.42785" y1="42.147490625" x2="324.393559375" y2="42.204640625" width="0.025" layer="94"/>
<wire x1="324.393559375" y1="42.204640625" x2="324.3707" y2="42.250359375" width="0.025" layer="94"/>
<wire x1="324.3707" y1="42.250359375" x2="324.336409375" y2="42.33036875" width="0.025" layer="94"/>
<wire x1="324.336409375" y1="42.33036875" x2="324.31355" y2="42.41038125" width="0.025" layer="94"/>
<wire x1="324.31355" y1="42.41038125" x2="324.30211875" y2="42.44466875" width="0.025" layer="94"/>
<wire x1="324.30211875" y1="42.44466875" x2="324.290690625" y2="42.50181875" width="0.025" layer="94"/>
<wire x1="324.290690625" y1="42.50181875" x2="324.279259375" y2="42.604690625" width="0.025" layer="94"/>
<wire x1="324.279259375" y1="42.604690625" x2="324.279259375" y2="42.764709375" width="0.025" layer="94"/>
<wire x1="324.279259375" y1="42.764709375" x2="324.290690625" y2="42.84471875" width="0.025" layer="94"/>
<wire x1="324.290690625" y1="42.84471875" x2="324.30211875" y2="42.90186875" width="0.025" layer="94"/>
<wire x1="324.30211875" y1="42.90186875" x2="324.347840625" y2="43.03903125" width="0.025" layer="94"/>
<wire x1="324.347840625" y1="43.03903125" x2="324.38213125" y2="43.107609375" width="0.025" layer="94"/>
<wire x1="324.38213125" y1="43.107609375" x2="324.41641875" y2="43.164759375" width="0.025" layer="94"/>
<wire x1="324.41641875" y1="43.164759375" x2="324.519290625" y2="43.30191875" width="0.025" layer="94"/>
<wire x1="324.519290625" y1="43.30191875" x2="324.70216875" y2="43.4848" width="0.025" layer="94"/>
<wire x1="324.70216875" y1="43.4848" x2="324.77075" y2="43.54195" width="0.025" layer="94"/>
<wire x1="324.77075" y1="43.54195" x2="324.81646875" y2="43.576240625" width="0.025" layer="94"/>
<wire x1="324.81646875" y1="43.576240625" x2="324.98678125" y2="43.68978125" width="0.025" layer="94"/>
<wire x1="324.98678125" y1="43.68978125" x2="325.10108125" y2="43.75843125" width="0.025" layer="94"/>
<wire x1="325.10108125" y1="43.75843125" x2="325.177659375" y2="43.796840625" width="0.025" layer="94"/>
<wire x1="325.177659375" y1="43.796840625" x2="325.376540625" y2="43.89628125" width="0.025" layer="94"/>
<wire x1="325.376540625" y1="43.89628125" x2="325.45655" y2="43.93056875" width="0.025" layer="94"/>
<wire x1="325.45655" y1="43.93056875" x2="325.53426875" y2="43.96143125" width="0.025" layer="94"/>
<wire x1="325.53426875" y1="43.96143125" x2="325.68515" y2="44.022009375" width="0.025" layer="94"/>
<wire x1="325.68515" y1="44.022009375" x2="325.82116875" y2="44.06735" width="0.025" layer="94"/>
<wire x1="325.82116875" y1="44.06735" x2="325.94346875" y2="44.10235" width="0.025" layer="94"/>
<wire x1="325.94346875" y1="44.10235" x2="326.06348125" y2="44.136590625" width="0.025" layer="94"/>
<wire x1="326.06348125" y1="44.136590625" x2="326.15378125" y2="44.15916875" width="0.025" layer="94"/>
<wire x1="326.15378125" y1="44.15916875" x2="326.230359375" y2="44.17631875" width="0.025" layer="94"/>
<wire x1="326.230359375" y1="44.17631875" x2="326.35951875" y2="44.204890625" width="0.025" layer="94"/>
<wire x1="326.35951875" y1="44.204890625" x2="326.47381875" y2="44.22775" width="0.025" layer="94"/>
<wire x1="326.47381875" y1="44.22775" x2="326.622409375" y2="44.250609375" width="0.025" layer="94"/>
<wire x1="326.622409375" y1="44.250609375" x2="326.78243125" y2="44.27346875" width="0.025" layer="94"/>
<wire x1="326.78243125" y1="44.27346875" x2="326.89673125" y2="44.2849" width="0.025" layer="94"/>
<wire x1="326.89673125" y1="44.2849" x2="327.091040625" y2="44.29633125" width="0.025" layer="94"/>
<wire x1="327.091040625" y1="44.29633125" x2="327.091040625" y2="44.033440625" width="0.025" layer="94"/>
<wire x1="327.091040625" y1="44.033440625" x2="326.919590625" y2="44.033440625" width="0.025" layer="94"/>
<wire x1="326.919590625" y1="44.033440625" x2="326.793859375" y2="44.022009375" width="0.025" layer="94"/>
<wire x1="326.793859375" y1="44.022009375" x2="326.71385" y2="44.01058125" width="0.025" layer="94"/>
<wire x1="326.71385" y1="44.01058125" x2="326.576690625" y2="43.98771875" width="0.025" layer="94"/>
<wire x1="326.576690625" y1="43.98771875" x2="326.462390625" y2="43.964859375" width="0.025" layer="94"/>
<wire x1="326.462390625" y1="43.964859375" x2="326.28065" y2="43.91941875" width="0.025" layer="94"/>
<wire x1="326.28065" y1="43.91941875" x2="326.12063125" y2="43.87375" width="0.025" layer="94"/>
<wire x1="326.12063125" y1="43.87375" x2="326.02805" y2="43.850559375" width="0.025" layer="94"/>
<wire x1="326.02805" y1="43.850559375" x2="325.8566" y2="43.793409375" width="0.025" layer="94"/>
<wire x1="325.8566" y1="43.793409375" x2="325.708009375" y2="43.73396875" width="0.025" layer="94"/>
<wire x1="325.708009375" y1="43.73396875" x2="325.604" y2="43.68996875" width="0.025" layer="94"/>
<wire x1="325.604" y1="43.68996875" x2="325.536559375" y2="43.65625" width="0.025" layer="94"/>
<wire x1="325.536559375" y1="43.65625" x2="325.45655" y2="43.61053125" width="0.025" layer="94"/>
<wire x1="325.45655" y1="43.61053125" x2="325.341109375" y2="43.541190625" width="0.025" layer="94"/>
<wire x1="325.341109375" y1="43.541190625" x2="325.205090625" y2="43.450509375" width="0.025" layer="94"/>
<wire x1="325.205090625" y1="43.450509375" x2="325.11365" y2="43.38193125" width="0.025" layer="94"/>
<wire x1="325.11365" y1="43.38193125" x2="325.033640625" y2="43.32478125" width="0.025" layer="94"/>
<wire x1="325.033640625" y1="43.32478125" x2="324.86133125" y2="43.152190625" width="0.025" layer="94"/>
<wire x1="324.86133125" y1="43.152190625" x2="324.828759375" y2="43.10875" width="0.025" layer="94"/>
<wire x1="324.828759375" y1="43.10875" x2="324.75931875" y2="43.004740625" width="0.025" layer="94"/>
<wire x1="324.75931875" y1="43.004740625" x2="324.70216875" y2="42.890440625" width="0.025" layer="94"/>
<wire x1="324.70216875" y1="42.890440625" x2="324.66788125" y2="42.78756875" width="0.025" layer="94"/>
<wire x1="324.66788125" y1="42.78756875" x2="324.65645" y2="42.74185" width="0.025" layer="94"/>
<wire x1="324.65645" y1="42.74185" x2="324.633590625" y2="42.626409375" width="0.025" layer="94"/>
<wire x1="324.633590625" y1="42.626409375" x2="324.633590625" y2="42.50181875" width="0.025" layer="94"/>
<wire x1="324.633590625" y1="42.50181875" x2="324.64501875" y2="42.421809375" width="0.025" layer="94"/>
<wire x1="324.64501875" y1="42.421809375" x2="324.66788125" y2="42.33036875" width="0.025" layer="94"/>
<wire x1="324.66788125" y1="42.33036875" x2="324.690740625" y2="42.261790625" width="0.025" layer="94"/>
<wire x1="324.690740625" y1="42.261790625" x2="324.7136" y2="42.204640625" width="0.025" layer="94"/>
<wire x1="324.7136" y1="42.204640625" x2="324.747890625" y2="42.147490625" width="0.025" layer="94"/>
<wire x1="324.747890625" y1="42.147490625" x2="324.793609375" y2="42.06748125" width="0.025" layer="94"/>
<wire x1="324.793609375" y1="42.06748125" x2="324.874759375" y2="41.96346875" width="0.025" layer="94"/>
<wire x1="324.874759375" y1="41.96346875" x2="324.976490625" y2="41.861740625" width="0.025" layer="94"/>
<wire x1="324.976490625" y1="41.861740625" x2="325.090790625" y2="41.75886875" width="0.025" layer="94"/>
<wire x1="325.090790625" y1="41.75886875" x2="325.193659375" y2="41.678859375" width="0.025" layer="94"/>
<wire x1="325.193659375" y1="41.678859375" x2="325.27366875" y2="41.621709375" width="0.025" layer="94"/>
<wire x1="325.27366875" y1="41.621709375" x2="325.433690625" y2="41.53026875" width="0.025" layer="94"/>
<wire x1="325.433690625" y1="41.53026875" x2="325.58228125" y2="41.461690625" width="0.025" layer="94"/>
<wire x1="325.58228125" y1="41.461690625" x2="325.68515" y2="41.41596875" width="0.025" layer="94"/>
<wire x1="325.68515" y1="41.41596875" x2="325.84516875" y2="41.35881875" width="0.025" layer="94"/>
<wire x1="325.84516875" y1="41.35881875" x2="325.91718125" y2="41.33481875" width="0.025" layer="94"/>
<wire x1="325.91718125" y1="41.33481875" x2="325.969759375" y2="41.317290625" width="0.025" layer="94"/>
<wire x1="325.969759375" y1="41.317290625" x2="326.084059375" y2="41.279190625" width="0.025" layer="94"/>
<wire x1="326.084059375" y1="41.279190625" x2="326.11925" y2="41.437690625" width="0.025" layer="94"/>
<wire x1="326.11925" y1="41.437690625" x2="325.99261875" y2="41.49646875" width="0.025" layer="94"/>
<wire x1="325.99261875" y1="41.49646875" x2="325.91375" y2="41.53026875" width="0.025" layer="94"/>
<wire x1="325.91375" y1="41.53026875" x2="325.81088125" y2="41.575990625" width="0.025" layer="94"/>
<wire x1="325.81088125" y1="41.575990625" x2="325.652" y2="41.66678125" width="0.025" layer="94"/>
<wire x1="325.652" y1="41.66678125" x2="325.55941875" y2="41.72458125" width="0.025" layer="94"/>
<wire x1="325.55941875" y1="41.72458125" x2="325.422259375" y2="41.83888125" width="0.025" layer="94"/>
<wire x1="325.422259375" y1="41.83888125" x2="325.365109375" y2="41.89603125" width="0.025" layer="94"/>
<wire x1="325.365109375" y1="41.89603125" x2="325.307959375" y2="41.964609375" width="0.025" layer="94"/>
<wire x1="325.307959375" y1="41.964609375" x2="325.21651875" y2="42.10176875" width="0.025" layer="94"/>
<wire x1="325.21651875" y1="42.10176875" x2="325.18223125" y2="42.18178125" width="0.025" layer="94"/>
<wire x1="325.18223125" y1="42.18178125" x2="325.15936875" y2="42.250359375" width="0.025" layer="94"/>
<wire x1="325.15936875" y1="42.250359375" x2="325.147940625" y2="42.318940625" width="0.025" layer="94"/>
<wire x1="325.147940625" y1="42.318940625" x2="325.147940625" y2="42.478959375" width="0.025" layer="94"/>
<wire x1="325.147940625" y1="42.478959375" x2="325.15936875" y2="42.547540625" width="0.025" layer="94"/>
<wire x1="325.15936875" y1="42.547540625" x2="325.1708" y2="42.593259375" width="0.025" layer="94"/>
<wire x1="325.1708" y1="42.593259375" x2="325.193659375" y2="42.650409375" width="0.025" layer="94"/>
<wire x1="325.193659375" y1="42.650409375" x2="325.22795" y2="42.718990625" width="0.025" layer="94"/>
<wire x1="325.22795" y1="42.718990625" x2="325.250809375" y2="42.75328125" width="0.025" layer="94"/>
<wire x1="325.250809375" y1="42.75328125" x2="325.2851" y2="42.799" width="0.025" layer="94"/>
<wire x1="325.2851" y1="42.799" x2="325.44511875" y2="42.95901875" width="0.025" layer="94"/>
<wire x1="325.44511875" y1="42.95901875" x2="325.5377" y2="43.028359375" width="0.025" layer="94"/>
<wire x1="325.5377" y1="43.028359375" x2="325.605140625" y2="43.07331875" width="0.025" layer="94"/>
<wire x1="325.605140625" y1="43.07331875" x2="325.68858125" y2="43.123609375" width="0.025" layer="94"/>
<wire x1="325.68858125" y1="43.123609375" x2="325.776590625" y2="43.176190625" width="0.025" layer="94"/>
<wire x1="325.776590625" y1="43.176190625" x2="325.84516875" y2="43.21048125" width="0.025" layer="94"/>
<wire x1="325.84516875" y1="43.21048125" x2="326.005190625" y2="43.279059375" width="0.025" layer="94"/>
<wire x1="326.005190625" y1="43.279059375" x2="326.09663125" y2="43.31335" width="0.025" layer="94"/>
<wire x1="326.09663125" y1="43.31335" x2="326.37095" y2="43.404790625" width="0.025" layer="94"/>
<wire x1="326.37095" y1="43.404790625" x2="326.49668125" y2="43.43908125" width="0.025" layer="94"/>
<wire x1="326.49668125" y1="43.43908125" x2="326.56411875" y2="43.454640625" width="0.025" layer="94"/>
<wire x1="326.56411875" y1="43.454640625" x2="326.64526875" y2="43.47336875" width="0.025" layer="94"/>
<wire x1="326.64526875" y1="43.47336875" x2="326.75956875" y2="43.49623125" width="0.025" layer="94"/>
<wire x1="326.75956875" y1="43.49623125" x2="326.93101875" y2="43.519090625" width="0.025" layer="94"/>
<wire x1="326.93101875" y1="43.519090625" x2="327.06818125" y2="43.53051875" width="0.025" layer="94"/>
<wire x1="327.06818125" y1="43.53051875" x2="327.205340625" y2="43.53051875" width="0.025" layer="94"/>
<wire x1="327.205340625" y1="43.53051875" x2="327.205340625" y2="43.35906875" width="0.025" layer="94"/>
<wire x1="327.205340625" y1="43.35906875" x2="327.17105" y2="43.35906875" width="0.025" layer="94"/>
<wire x1="327.17105" y1="43.35906875" x2="327.04531875" y2="43.347640625" width="0.025" layer="94"/>
<wire x1="327.04531875" y1="43.347640625" x2="326.95388125" y2="43.336209375" width="0.025" layer="94"/>
<wire x1="326.95388125" y1="43.336209375" x2="326.81671875" y2="43.31335" width="0.025" layer="94"/>
<wire x1="326.81671875" y1="43.31335" x2="326.71385" y2="43.290490625" width="0.025" layer="94"/>
<wire x1="326.71385" y1="43.290490625" x2="326.565259375" y2="43.2562" width="0.025" layer="94"/>
<wire x1="326.565259375" y1="43.2562" x2="326.48525" y2="43.233340625" width="0.025" layer="94"/>
<wire x1="326.48525" y1="43.233340625" x2="326.41666875" y2="43.21048125" width="0.025" layer="94"/>
<wire x1="326.41666875" y1="43.21048125" x2="326.32523125" y2="43.176190625" width="0.025" layer="94"/>
<wire x1="326.32523125" y1="43.176190625" x2="326.21206875" y2="43.13093125" width="0.025" layer="94"/>
<wire x1="326.21206875" y1="43.13093125" x2="326.005190625" y2="43.0276" width="0.025" layer="94"/>
<wire x1="326.005190625" y1="43.0276" x2="325.948040625" y2="42.993309375" width="0.025" layer="94"/>
<wire x1="325.948040625" y1="42.993309375" x2="325.84516875" y2="42.92473125" width="0.025" layer="94"/>
<wire x1="325.84516875" y1="42.92473125" x2="325.79945" y2="42.890440625" width="0.025" layer="94"/>
<wire x1="325.79945" y1="42.890440625" x2="325.7423" y2="42.84471875" width="0.025" layer="94"/>
<wire x1="325.7423" y1="42.84471875" x2="325.708009375" y2="42.81043125" width="0.025" layer="94"/>
<wire x1="325.708009375" y1="42.81043125" x2="325.63943125" y2="42.73041875" width="0.025" layer="94"/>
<wire x1="325.63943125" y1="42.73041875" x2="325.593709375" y2="42.661840625" width="0.025" layer="94"/>
<wire x1="325.593709375" y1="42.661840625" x2="325.55941875" y2="42.593259375" width="0.025" layer="94"/>
<wire x1="325.55941875" y1="42.593259375" x2="325.536559375" y2="42.536109375" width="0.025" layer="94"/>
<wire x1="325.536559375" y1="42.536109375" x2="325.5137" y2="42.4561" width="0.025" layer="94"/>
<wire x1="325.5137" y1="42.4561" x2="325.50226875" y2="42.39895" width="0.025" layer="94"/>
<wire x1="325.50226875" y1="42.39895" x2="325.50226875" y2="42.261790625" width="0.025" layer="94"/>
<wire x1="325.50226875" y1="42.261790625" x2="325.5137" y2="42.204640625" width="0.025" layer="94"/>
<wire x1="325.5137" y1="42.204640625" x2="325.52513125" y2="42.15891875" width="0.025" layer="94"/>
<wire x1="325.52513125" y1="42.15891875" x2="325.547609375" y2="42.09148125" width="0.025" layer="94"/>
<wire x1="325.547609375" y1="42.09148125" x2="325.57085" y2="42.04461875" width="0.025" layer="94"/>
<wire x1="325.57085" y1="42.04461875" x2="325.605140625" y2="41.98746875" width="0.025" layer="94"/>
<wire x1="325.605140625" y1="41.98746875" x2="325.67371875" y2="41.89603125" width="0.025" layer="94"/>
<wire x1="325.67371875" y1="41.89603125" x2="325.75373125" y2="41.81601875" width="0.025" layer="94"/>
<wire x1="325.75373125" y1="41.81601875" x2="325.84516875" y2="41.747440625" width="0.025" layer="94"/>
<wire x1="325.84516875" y1="41.747440625" x2="325.95833125" y2="41.67955" width="0.025" layer="94"/>
<wire x1="325.95833125" y1="41.67955" x2="326.04061875" y2="41.63256875" width="0.025" layer="94"/>
<wire x1="326.04061875" y1="41.63256875" x2="326.108059375" y2="41.59885" width="0.025" layer="94"/>
<wire x1="326.108059375" y1="41.59885" x2="326.165509375" y2="41.57713125" width="0.025" layer="94"/>
<wire x1="326.165509375" y1="41.57713125" x2="326.35951875" y2="42.307509375" width="0.025" layer="94"/>
<wire x1="326.35951875" y1="42.307509375" x2="328.8284" y2="42.307509375" width="0.025" layer="94"/>
<wire x1="328.8284" y1="42.307509375" x2="329.022709375" y2="41.575990625" width="0.025" layer="94"/>
<wire x1="329.022709375" y1="41.575990625" x2="329.087859375" y2="41.597140625" width="0.025" layer="94"/>
<wire x1="329.087859375" y1="41.597140625" x2="329.14158125" y2="41.624" width="0.025" layer="94"/>
<wire x1="329.14158125" y1="41.624" x2="329.25245" y2="41.67961875" width="0.025" layer="94"/>
<wire x1="329.25245" y1="41.67961875" x2="329.42161875" y2="41.7924" width="0.025" layer="94"/>
<wire x1="329.42161875" y1="41.7924" x2="329.548490625" y2="41.918890625" width="0.025" layer="94"/>
<wire x1="329.548490625" y1="41.918890625" x2="329.6285" y2="42.033190625" width="0.025" layer="94"/>
<wire x1="329.6285" y1="42.033190625" x2="329.662790625" y2="42.1132" width="0.025" layer="94"/>
<wire x1="329.662790625" y1="42.1132" x2="329.68593125" y2="42.18291875" width="0.025" layer="94"/>
<wire x1="329.68593125" y1="42.18291875" x2="329.69708125" y2="42.2275" width="0.025" layer="94"/>
<wire x1="329.69708125" y1="42.2275" x2="329.708509375" y2="42.307509375" width="0.025" layer="94"/>
<wire x1="329.708509375" y1="42.307509375" x2="329.708509375" y2="42.35323125" width="0.025" layer="94"/>
<wire x1="329.708509375" y1="42.35323125" x2="329.69708125" y2="42.433240625" width="0.025" layer="94"/>
<wire x1="329.69708125" y1="42.433240625" x2="329.67421875" y2="42.52468125" width="0.025" layer="94"/>
<wire x1="329.67421875" y1="42.52468125" x2="329.651359375" y2="42.58183125" width="0.025" layer="94"/>
<wire x1="329.651359375" y1="42.58183125" x2="329.61706875" y2="42.650409375" width="0.025" layer="94"/>
<wire x1="329.61706875" y1="42.650409375" x2="329.570490625" y2="42.72013125" width="0.025" layer="94"/>
<wire x1="329.570490625" y1="42.72013125" x2="329.537059375" y2="42.764709375" width="0.025" layer="94"/>
<wire x1="329.537059375" y1="42.764709375" x2="329.43533125" y2="42.866440625" width="0.025" layer="94"/>
<wire x1="329.43533125" y1="42.866440625" x2="329.377040625" y2="42.9133" width="0.025" layer="94"/>
<wire x1="329.377040625" y1="42.9133" x2="329.33131875" y2="42.947590625" width="0.025" layer="94"/>
<wire x1="329.33131875" y1="42.947590625" x2="329.21701875" y2="43.01616875" width="0.025" layer="94"/>
<wire x1="329.21701875" y1="43.01616875" x2="329.137009375" y2="43.061890625" width="0.025" layer="94"/>
<wire x1="329.137009375" y1="43.061890625" x2="329.06843125" y2="43.09618125" width="0.025" layer="94"/>
<wire x1="329.06843125" y1="43.09618125" x2="328.965559375" y2="43.1419" width="0.025" layer="94"/>
<wire x1="328.965559375" y1="43.1419" x2="328.908409375" y2="43.164759375" width="0.025" layer="94"/>
<wire x1="328.908409375" y1="43.164759375" x2="328.81696875" y2="43.19905" width="0.025" layer="94"/>
<wire x1="328.81696875" y1="43.19905" x2="328.7141" y2="43.233340625" width="0.025" layer="94"/>
<wire x1="328.7141" y1="43.233340625" x2="328.634090625" y2="43.2562" width="0.025" layer="94"/>
<wire x1="328.634090625" y1="43.2562" x2="328.54265" y2="43.279059375" width="0.025" layer="94"/>
<wire x1="328.54265" y1="43.279059375" x2="328.43978125" y2="43.30191875" width="0.025" layer="94"/>
<wire x1="328.43978125" y1="43.30191875" x2="328.32661875" y2="43.32478125" width="0.025" layer="94"/>
<wire x1="328.32661875" y1="43.32478125" x2="328.255759375" y2="43.336209375" width="0.025" layer="94"/>
<wire x1="328.255759375" y1="43.336209375" x2="328.15516875" y2="43.347640625" width="0.025" layer="94"/>
<wire x1="328.15516875" y1="43.347640625" x2="328.075159375" y2="43.353709375" width="0.025" layer="94"/>
<wire x1="328.075159375" y1="43.353709375" x2="328.00658125" y2="43.35906875" width="0.025" layer="94"/>
<wire x1="328.00658125" y1="43.35906875" x2="328.005440625" y2="43.53051875" width="0.025" layer="94"/>
<wire x1="328.005440625" y1="43.53051875" x2="328.176890625" y2="43.53051875" width="0.025" layer="94"/>
<wire x1="328.176890625" y1="43.53051875" x2="328.291190625" y2="43.519090625" width="0.025" layer="94"/>
<wire x1="328.291190625" y1="43.519090625" x2="328.3712" y2="43.507659375" width="0.025" layer="94"/>
<wire x1="328.3712" y1="43.507659375" x2="328.43978125" y2="43.49623125" width="0.025" layer="94"/>
<wire x1="328.43978125" y1="43.49623125" x2="328.61123125" y2="43.461940625" width="0.025" layer="94"/>
<wire x1="328.61123125" y1="43.461940625" x2="328.70266875" y2="43.43908125" width="0.025" layer="94"/>
<wire x1="328.70266875" y1="43.43908125" x2="328.78268125" y2="43.41621875" width="0.025" layer="94"/>
<wire x1="328.78268125" y1="43.41621875" x2="328.892409375" y2="43.38306875" width="0.025" layer="94"/>
<wire x1="328.892409375" y1="43.38306875" x2="329.010140625" y2="43.34798125" width="0.025" layer="94"/>
<wire x1="329.010140625" y1="43.34798125" x2="329.079859375" y2="43.32191875" width="0.025" layer="94"/>
<wire x1="329.079859375" y1="43.32191875" x2="329.194159375" y2="43.279059375" width="0.025" layer="94"/>
<wire x1="329.194159375" y1="43.279059375" x2="329.3096" y2="43.23276875" width="0.025" layer="94"/>
<wire x1="329.3096" y1="43.23276875" x2="329.44561875" y2="43.164759375" width="0.025" layer="94"/>
<wire x1="329.44561875" y1="43.164759375" x2="329.50276875" y2="43.13046875" width="0.025" layer="94"/>
<wire x1="329.50276875" y1="43.13046875" x2="329.548490625" y2="43.107609375" width="0.025" layer="94"/>
<wire x1="329.548490625" y1="43.107609375" x2="329.61706875" y2="43.061890625" width="0.025" layer="94"/>
<wire x1="329.61706875" y1="43.061890625" x2="329.75423125" y2="42.95901875" width="0.025" layer="94"/>
<wire x1="329.75423125" y1="42.95901875" x2="329.81138125" y2="42.9133" width="0.025" layer="94"/>
<wire x1="329.891390625" y1="42.833290625" x2="329.89253125" y2="42.83215" width="0.025" layer="94"/>
<wire x1="329.89253125" y1="42.83215" x2="329.937109375" y2="42.776140625" width="0.025" layer="94"/>
<wire x1="329.937109375" y1="42.776140625" x2="329.95996875" y2="42.74185" width="0.025" layer="94"/>
<wire x1="329.95996875" y1="42.74185" x2="329.9954" y2="42.682409375" width="0.025" layer="94"/>
<wire x1="329.9954" y1="42.682409375" x2="330.03998125" y2="42.5704" width="0.025" layer="94"/>
<wire x1="330.03998125" y1="42.5704" x2="330.051409375" y2="42.512109375" width="0.025" layer="94"/>
<wire x1="330.051409375" y1="42.512109375" x2="330.062840625" y2="42.421809375" width="0.025" layer="94"/>
<wire x1="330.062840625" y1="42.421809375" x2="330.062840625" y2="42.35323125" width="0.025" layer="94"/>
<wire x1="330.062840625" y1="42.35323125" x2="330.051409375" y2="42.261790625" width="0.025" layer="94"/>
<wire x1="330.051409375" y1="42.261790625" x2="330.03998125" y2="42.21606875" width="0.025" layer="94"/>
<wire x1="330.03998125" y1="42.21606875" x2="330.01711875" y2="42.15891875" width="0.025" layer="94"/>
<wire x1="330.01711875" y1="42.15891875" x2="329.98283125" y2="42.090340625" width="0.025" layer="94"/>
<wire x1="329.98283125" y1="42.090340625" x2="329.95996875" y2="42.05605" width="0.025" layer="94"/>
<wire x1="329.95996875" y1="42.05605" x2="329.90281875" y2="41.976040625" width="0.025" layer="94"/>
<wire x1="329.90281875" y1="41.976040625" x2="329.834240625" y2="41.89603125" width="0.025" layer="94"/>
<wire x1="329.834240625" y1="41.89603125" x2="329.7428" y2="41.804590625" width="0.025" layer="94"/>
<wire x1="329.7428" y1="41.804590625" x2="329.6285" y2="41.71315" width="0.025" layer="94"/>
<wire x1="329.6285" y1="41.71315" x2="329.55878125" y2="41.666790625" width="0.025" layer="94"/>
<wire x1="329.55878125" y1="41.666790625" x2="329.45705" y2="41.61028125" width="0.025" layer="94"/>
<wire x1="329.45705" y1="41.61028125" x2="329.319890625" y2="41.5417" width="0.025" layer="94"/>
<wire x1="329.319890625" y1="41.5417" x2="329.1633" y2="41.474590625" width="0.025" layer="94"/>
<wire x1="329.1633" y1="41.474590625" x2="329.06843125" y2="41.43883125" width="0.025" layer="94"/>
<wire x1="329.06843125" y1="41.43883125" x2="329.10271875" y2="41.278809375" width="0.025" layer="94"/>
<wire x1="329.10271875" y1="41.278809375" x2="329.137009375" y2="41.290240625" width="0.025" layer="94"/>
<wire x1="329.137009375" y1="41.290240625" x2="329.21701875" y2="41.3131" width="0.025" layer="94"/>
<wire x1="329.21701875" y1="41.3131" x2="329.3999" y2="41.37025" width="0.025" layer="94"/>
<wire x1="329.3999" y1="41.37025" x2="329.57135" y2="41.43883125" width="0.025" layer="94"/>
<wire x1="329.57135" y1="41.43883125" x2="329.834240625" y2="41.564559375" width="0.025" layer="94"/>
<wire x1="329.834240625" y1="41.564559375" x2="329.891390625" y2="41.59885" width="0.025" layer="94"/>
<wire x1="329.891390625" y1="41.59885" x2="329.994259375" y2="41.66743125" width="0.025" layer="94"/>
<wire x1="329.994259375" y1="41.66743125" x2="330.051409375" y2="41.71315" width="0.025" layer="94"/>
<wire x1="330.051409375" y1="41.71315" x2="330.14285" y2="41.78173125" width="0.025" layer="94"/>
<wire x1="330.14285" y1="41.78173125" x2="330.177140625" y2="41.81601875" width="0.025" layer="94"/>
<wire x1="330.177140625" y1="41.81601875" x2="330.25715" y2="41.8846" width="0.025" layer="94"/>
<wire x1="330.25715" y1="41.8846" x2="330.30286875" y2="41.93031875" width="0.025" layer="94"/>
<wire x1="330.30286875" y1="41.93031875" x2="330.37145" y2="42.01033125" width="0.025" layer="94"/>
<wire x1="330.37145" y1="42.01033125" x2="330.405740625" y2="42.05605" width="0.025" layer="94"/>
<wire x1="330.405740625" y1="42.05605" x2="330.47478125" y2="42.171490625" width="0.025" layer="94"/>
<wire x1="330.47478125" y1="42.171490625" x2="330.520040625" y2="42.28465" width="0.025" layer="94"/>
<wire x1="330.520040625" y1="42.28465" x2="330.54251875" y2="42.352090625" width="0.025" layer="94"/>
<wire x1="330.54251875" y1="42.352090625" x2="330.565759375" y2="42.478959375" width="0.025" layer="94"/>
<wire x1="330.565759375" y1="42.478959375" x2="330.565759375" y2="42.650409375" width="0.025" layer="94"/>
<wire x1="330.565759375" y1="42.650409375" x2="330.55433125" y2="42.718990625" width="0.025" layer="94"/>
<wire x1="330.55433125" y1="42.718990625" x2="330.5429" y2="42.776140625" width="0.025" layer="94"/>
<wire x1="330.5429" y1="42.776140625" x2="330.520040625" y2="42.84471875" width="0.025" layer="94"/>
<wire x1="330.520040625" y1="42.84471875" x2="330.49718125" y2="42.90186875" width="0.025" layer="94"/>
<wire x1="330.49718125" y1="42.90186875" x2="330.451459375" y2="42.993309375" width="0.025" layer="94"/>
<wire x1="330.451459375" y1="42.993309375" x2="330.41716875" y2="43.050459375" width="0.025" layer="94"/>
<wire x1="330.41716875" y1="43.050459375" x2="330.394309375" y2="43.08475" width="0.025" layer="94"/>
<wire x1="330.394309375" y1="43.08475" x2="330.36001875" y2="43.13046875" width="0.025" layer="94"/>
<wire x1="330.36001875" y1="43.13046875" x2="330.291440625" y2="43.21048125" width="0.025" layer="94"/>
<wire x1="330.291440625" y1="43.21048125" x2="330.234290625" y2="43.26763125" width="0.025" layer="94"/>
<wire x1="330.234290625" y1="43.26763125" x2="330.15428125" y2="43.336209375" width="0.025" layer="94"/>
<wire x1="330.15428125" y1="43.336209375" x2="330.01711875" y2="43.43908125" width="0.025" layer="94"/>
<wire x1="330.01711875" y1="43.43908125" x2="329.879959375" y2="43.53051875" width="0.025" layer="94"/>
<wire x1="329.879959375" y1="43.53051875" x2="329.765659375" y2="43.5991" width="0.025" layer="94"/>
<wire x1="329.765659375" y1="43.5991" x2="329.68565" y2="43.64481875" width="0.025" layer="94"/>
<wire x1="329.68565" y1="43.64481875" x2="329.52563125" y2="43.72483125" width="0.025" layer="94"/>
<wire x1="329.52563125" y1="43.72483125" x2="329.44561875" y2="43.75911875" width="0.025" layer="94"/>
<wire x1="329.44561875" y1="43.75911875" x2="329.1713" y2="43.850559375" width="0.025" layer="94"/>
<wire x1="329.1713" y1="43.850559375" x2="329.04556875" y2="43.88485" width="0.025" layer="94"/>
<wire x1="329.04556875" y1="43.88485" x2="328.965559375" y2="43.907709375" width="0.025" layer="94"/>
<wire x1="328.965559375" y1="43.907709375" x2="328.691240625" y2="43.976290625" width="0.025" layer="94"/>
<wire x1="328.691240625" y1="43.976290625" x2="328.634090625" y2="43.98771875" width="0.025" layer="94"/>
<wire x1="328.634090625" y1="43.98771875" x2="328.565509375" y2="43.99915" width="0.025" layer="94"/>
<wire x1="328.565509375" y1="43.99915" x2="328.394059375" y2="44.022009375" width="0.025" layer="94"/>
<wire x1="328.394059375" y1="44.022009375" x2="328.2569" y2="44.033440625" width="0.025" layer="94"/>
<wire x1="328.2569" y1="44.033440625" x2="328.119740625" y2="44.033440625" width="0.025" layer="94"/>
<wire x1="328.119740625" y1="44.033440625" x2="328.119740625" y2="44.29633125" width="0.025" layer="94"/>
<wire x1="328.119740625" y1="44.29633125" x2="328.165459375" y2="44.29633125" width="0.025" layer="94"/>
<wire x1="328.165459375" y1="44.29633125" x2="328.32548125" y2="44.2849" width="0.025" layer="94"/>
<wire x1="328.32548125" y1="44.2849" x2="328.42835" y2="44.27346875" width="0.025" layer="94"/>
<wire x1="328.42835" y1="44.27346875" x2="328.519790625" y2="44.262040625" width="0.025" layer="94"/>
<wire x1="328.519790625" y1="44.262040625" x2="328.5998" y2="44.250609375" width="0.025" layer="94"/>
<wire x1="328.5998" y1="44.250609375" x2="328.72553125" y2="44.22775" width="0.025" layer="94"/>
<wire x1="328.72553125" y1="44.22775" x2="328.952990625" y2="44.18203125" width="0.025" layer="94"/>
<wire x1="328.952990625" y1="44.18203125" x2="329.22845" y2="44.11345" width="0.025" layer="94"/>
<wire x1="329.22845" y1="44.11345" x2="329.38846875" y2="44.06773125" width="0.025" layer="94"/>
<wire x1="329.38846875" y1="44.06773125" x2="329.52563125" y2="44.022009375" width="0.025" layer="94"/>
<wire x1="329.52563125" y1="44.022009375" x2="329.69708125" y2="43.95343125" width="0.025" layer="94"/>
<wire x1="329.69708125" y1="43.95343125" x2="329.90281875" y2="43.861990625" width="0.025" layer="94"/>
<wire x1="329.90281875" y1="43.861990625" x2="330.04111875" y2="43.79271875" width="0.025" layer="94"/>
<wire x1="330.04111875" y1="43.79271875" x2="330.14285" y2="43.736259375" width="0.025" layer="94"/>
<wire x1="330.14285" y1="43.736259375" x2="330.2" y2="43.70196875" width="0.025" layer="94"/>
<wire x1="330.2" y1="43.70196875" x2="330.337159375" y2="43.61053125" width="0.025" layer="94"/>
<wire x1="330.337159375" y1="43.61053125" x2="330.38288125" y2="43.576240625" width="0.025" layer="94"/>
<wire x1="330.38288125" y1="43.576240625" x2="330.55433125" y2="43.43908125" width="0.025" layer="94"/>
<wire x1="330.55433125" y1="43.43908125" x2="330.601190625" y2="43.39221875" width="0.025" layer="94"/>
<wire x1="330.601190625" y1="43.39221875" x2="330.644140625" y2="43.34193125" width="0.025" layer="94"/>
<wire x1="330.644140625" y1="43.34193125" x2="330.70291875" y2="43.279059375" width="0.025" layer="94"/>
<wire x1="330.70291875" y1="43.279059375" x2="330.7715" y2="43.18761875" width="0.025" layer="94"/>
<wire x1="330.7715" y1="43.18761875" x2="330.81721875" y2="43.119040625" width="0.025" layer="94"/>
<wire x1="330.81721875" y1="43.119040625" x2="330.862940625" y2="43.0276" width="0.025" layer="94"/>
<wire x1="330.862940625" y1="43.0276" x2="330.8858" y2="42.95901875" width="0.025" layer="94"/>
<wire x1="330.8858" y1="42.95901875" x2="330.908659375" y2="42.879009375" width="0.025" layer="94"/>
<wire x1="330.908659375" y1="42.879009375" x2="330.920090625" y2="42.81043125" width="0.025" layer="94"/>
<wire x1="330.920090625" y1="42.81043125" x2="330.93151875" y2="42.69613125" width="0.025" layer="94"/>
<wire x1="330.93151875" y1="42.69613125" x2="330.93151875" y2="42.63898125" width="0.025" layer="94"/>
<wire x1="330.93151875" y1="42.63898125" x2="330.920090625" y2="42.536109375" width="0.025" layer="94"/>
<wire x1="330.920090625" y1="42.536109375" x2="330.908659375" y2="42.478959375" width="0.025" layer="94"/>
<wire x1="330.908659375" y1="42.478959375" x2="330.8858" y2="42.38751875" width="0.025" layer="94"/>
<wire x1="330.8858" y1="42.38751875" x2="330.87436875" y2="42.35323125" width="0.025" layer="94"/>
<wire x1="330.87436875" y1="42.35323125" x2="330.82865" y2="42.23893125" width="0.025" layer="94"/>
<wire x1="330.82865" y1="42.23893125" x2="330.8051" y2="42.19206875" width="0.025" layer="94"/>
<wire x1="330.8051" y1="42.19206875" x2="330.770640625" y2="42.13491875" width="0.025" layer="94"/>
<wire x1="330.770640625" y1="42.13491875" x2="330.70291875" y2="42.04461875" width="0.025" layer="94"/>
<wire x1="330.70291875" y1="42.04461875" x2="330.6572" y2="41.98746875" width="0.025" layer="94"/>
<wire x1="330.6572" y1="41.98746875" x2="330.577190625" y2="41.907459375" width="0.025" layer="94"/>
<wire x1="330.577190625" y1="41.907459375" x2="330.49718125" y2="41.83888125" width="0.025" layer="94"/>
<wire x1="330.49718125" y1="41.83888125" x2="330.44003125" y2="41.793159375" width="0.025" layer="94"/>
<wire x1="330.44003125" y1="41.793159375" x2="330.394309375" y2="41.75886875" width="0.025" layer="94"/>
<wire x1="330.394309375" y1="41.75886875" x2="330.337159375" y2="41.71315" width="0.025" layer="94"/>
<wire x1="330.337159375" y1="41.71315" x2="330.291440625" y2="41.678859375" width="0.025" layer="94"/>
<wire x1="330.291440625" y1="41.678859375" x2="330.18856875" y2="41.61028125" width="0.025" layer="94"/>
<wire x1="330.18856875" y1="41.61028125" x2="330.14285" y2="41.575990625" width="0.025" layer="94"/>
<wire x1="330.14285" y1="41.575990625" x2="330.0857" y2="41.5417" width="0.025" layer="94"/>
<wire x1="330.0857" y1="41.5417" x2="329.834240625" y2="41.41596875" width="0.025" layer="94"/>
<wire x1="329.834240625" y1="41.41596875" x2="329.719940625" y2="41.37025" width="0.025" layer="94"/>
<wire x1="329.719940625" y1="41.37025" x2="329.594209375" y2="41.32453125" width="0.025" layer="94"/>
<wire x1="329.594209375" y1="41.32453125" x2="329.44333125" y2="41.278109375" width="0.025" layer="94"/>
<wire x1="329.44333125" y1="41.278109375" x2="329.29703125" y2="41.233090625" width="0.025" layer="94"/>
<wire x1="329.29703125" y1="41.233090625" x2="329.137009375" y2="41.18736875" width="0.025" layer="94"/>
<wire x1="329.137009375" y1="41.18736875" x2="329.1713" y2="41.03878125" width="0.025" layer="94"/>
<wire x1="329.1713" y1="41.03878125" x2="329.26616875" y2="41.059640625" width="0.025" layer="94"/>
<wire x1="329.26616875" y1="41.059640625" x2="329.50163125" y2="41.1185" width="0.025" layer="94"/>
<wire x1="329.50163125" y1="41.1185" x2="329.6045" y2="41.147859375" width="0.025" layer="94"/>
<wire x1="329.6045" y1="41.147859375" x2="329.743940625" y2="41.18775" width="0.025" layer="94"/>
<wire x1="329.743940625" y1="41.18775" x2="329.948540625" y2="41.25595" width="0.025" layer="94"/>
<wire x1="329.948540625" y1="41.25595" x2="330.03998125" y2="41.290240625" width="0.025" layer="94"/>
<wire x1="330.03998125" y1="41.290240625" x2="330.21143125" y2="41.35881875" width="0.025" layer="94"/>
<wire x1="330.21143125" y1="41.35881875" x2="330.48575" y2="41.49598125" width="0.025" layer="94"/>
<wire x1="330.48575" y1="41.49598125" x2="330.60005" y2="41.564559375" width="0.025" layer="94"/>
<wire x1="330.60005" y1="41.564559375" x2="330.69035" y2="41.62101875" width="0.025" layer="94"/>
<wire x1="330.69035" y1="41.62101875" x2="330.737209375" y2="41.656" width="0.025" layer="94"/>
<wire x1="330.737209375" y1="41.656" x2="330.805790625" y2="41.70171875" width="0.025" layer="94"/>
<wire x1="330.805790625" y1="41.70171875" x2="330.851509375" y2="41.736009375" width="0.025" layer="94"/>
<wire x1="330.851509375" y1="41.736009375" x2="331.080109375" y2="41.918890625" width="0.025" layer="94"/>
<wire x1="331.080109375" y1="41.918890625" x2="331.2287" y2="42.06748125" width="0.025" layer="94"/>
<wire x1="331.2287" y1="42.06748125" x2="331.308709375" y2="42.17035" width="0.025" layer="94"/>
<wire x1="331.308709375" y1="42.17035" x2="331.33156875" y2="42.204640625" width="0.025" layer="94"/>
<wire x1="331.33156875" y1="42.204640625" x2="331.365859375" y2="42.261790625" width="0.025" layer="94"/>
<wire x1="331.365859375" y1="42.261790625" x2="331.434440625" y2="42.39895" width="0.025" layer="94"/>
<wire x1="331.434440625" y1="42.39895" x2="331.4573" y2="42.4561" width="0.025" layer="94"/>
<wire x1="331.4573" y1="42.4561" x2="331.46873125" y2="42.490390625" width="0.025" layer="94"/>
<wire x1="331.46873125" y1="42.490390625" x2="331.491590625" y2="42.58183125" width="0.025" layer="94"/>
<wire x1="331.491590625" y1="42.58183125" x2="331.50301875" y2="42.650409375" width="0.025" layer="94"/>
<wire x1="331.50301875" y1="42.650409375" x2="331.51445" y2="42.764709375" width="0.025" layer="94"/>
<wire x1="331.51445" y1="42.764709375" x2="331.51445" y2="42.84471875" width="0.025" layer="94"/>
<wire x1="331.51445" y1="42.84471875" x2="331.50988125" y2="42.89615" width="0.025" layer="94"/>
<wire x1="331.50988125" y1="42.89615" x2="331.50301875" y2="42.97045" width="0.025" layer="94"/>
<wire x1="331.50301875" y1="42.97045" x2="331.480159375" y2="43.08475" width="0.025" layer="94"/>
<wire x1="331.480159375" y1="43.08475" x2="331.434440625" y2="43.221909375" width="0.025" layer="94"/>
<wire x1="331.434440625" y1="43.221909375" x2="331.41158125" y2="43.279059375" width="0.025" layer="94"/>
<wire x1="331.41158125" y1="43.279059375" x2="331.36643125" y2="43.369359375" width="0.025" layer="94"/>
<wire x1="331.36643125" y1="43.369359375" x2="331.27518125" y2="43.50651875" width="0.025" layer="94"/>
<wire x1="331.27518125" y1="43.50651875" x2="331.24013125" y2="43.55338125" width="0.025" layer="94"/>
<wire x1="331.24013125" y1="43.55338125" x2="331.194409375" y2="43.61053125" width="0.025" layer="94"/>
<wire x1="331.194409375" y1="43.61053125" x2="330.98866875" y2="43.81626875" width="0.025" layer="94"/>
<wire x1="330.98866875" y1="43.81626875" x2="330.920090625" y2="43.87341875" width="0.025" layer="94"/>
<wire x1="330.920090625" y1="43.87341875" x2="330.862940625" y2="43.919140625" width="0.025" layer="94"/>
<wire x1="330.862940625" y1="43.919140625" x2="330.7715" y2="43.98771875" width="0.025" layer="94"/>
<wire x1="330.7715" y1="43.98771875" x2="330.66863125" y2="44.0563" width="0.025" layer="94"/>
<wire x1="330.66863125" y1="44.0563" x2="330.610340625" y2="44.09135" width="0.025" layer="94"/>
<wire x1="330.610340625" y1="44.09135" x2="330.577190625" y2="44.11345" width="0.025" layer="94"/>
<wire x1="330.577190625" y1="44.11345" x2="330.520040625" y2="44.147740625" width="0.025" layer="94"/>
<wire x1="330.520040625" y1="44.147740625" x2="330.24571875" y2="44.2849" width="0.025" layer="94"/>
<wire x1="330.24571875" y1="44.2849" x2="329.95768125" y2="44.399959375" width="0.025" layer="94"/>
<wire x1="329.95768125" y1="44.399959375" x2="329.651359375" y2="44.50206875" width="0.025" layer="94"/>
<wire x1="329.651359375" y1="44.50206875" x2="329.4902" y2="44.54806875" width="0.025" layer="94"/>
<wire x1="329.4902" y1="44.54806875" x2="329.21701875" y2="44.61636875" width="0.025" layer="94"/>
<wire x1="329.21701875" y1="44.61636875" x2="328.87411875" y2="44.68495" width="0.025" layer="94"/>
<wire x1="328.87411875" y1="44.68495" x2="328.736959375" y2="44.707809375" width="0.025" layer="94"/>
<wire x1="328.736959375" y1="44.707809375" x2="328.65695" y2="44.719240625" width="0.025" layer="94"/>
<wire x1="328.65695" y1="44.719240625" x2="328.47406875" y2="44.7421" width="0.025" layer="94"/>
<wire x1="328.47406875" y1="44.7421" x2="328.370059375" y2="44.75353125" width="0.025" layer="94"/>
<wire x1="328.370059375" y1="44.75353125" x2="328.222609375" y2="44.764959375" width="0.025" layer="94"/>
<wire x1="328.222609375" y1="44.764959375" x2="328.222609375" y2="45.03928125" width="0.025" layer="94"/>
<wire x1="328.222609375" y1="45.03928125" x2="328.279759375" y2="45.03928125" width="0.025" layer="94"/>
<wire x1="328.279759375" y1="45.03928125" x2="328.42835" y2="45.02785" width="0.025" layer="94"/>
<wire x1="328.42835" y1="45.02785" x2="328.55408125" y2="45.01641875" width="0.025" layer="94"/>
<wire x1="328.55408125" y1="45.01641875" x2="328.65695" y2="45.004990625" width="0.025" layer="94"/>
<wire x1="328.65695" y1="45.004990625" x2="328.736959375" y2="44.994990625" width="0.025" layer="94"/>
<wire x1="328.736959375" y1="44.994990625" x2="328.79983125" y2="44.98713125" width="0.025" layer="94"/>
<wire x1="328.79983125" y1="44.98713125" x2="328.93126875" y2="44.9707" width="0.025" layer="94"/>
<wire x1="328.93126875" y1="44.9707" x2="329.137009375" y2="44.936409375" width="0.025" layer="94"/>
<wire x1="329.137009375" y1="44.936409375" x2="329.251309375" y2="44.91355" width="0.025" layer="94"/>
<wire x1="329.251309375" y1="44.91355" x2="329.29703125" y2="44.90211875" width="0.025" layer="94"/>
<wire x1="329.29703125" y1="44.90211875" x2="329.3999" y2="44.879259375" width="0.025" layer="94"/>
<wire x1="329.3999" y1="44.879259375" x2="329.45705" y2="44.86783125" width="0.025" layer="94"/>
<wire x1="329.45705" y1="44.86783125" x2="329.63993125" y2="44.822109375" width="0.025" layer="94"/>
<wire x1="329.63993125" y1="44.822109375" x2="329.719940625" y2="44.79925" width="0.025" layer="94"/>
<wire x1="329.719940625" y1="44.79925" x2="329.81138125" y2="44.776390625" width="0.025" layer="94"/>
<wire x1="329.81138125" y1="44.776390625" x2="329.891390625" y2="44.75353125" width="0.025" layer="94"/>
<wire x1="329.891390625" y1="44.75353125" x2="330.165709375" y2="44.662090625" width="0.025" layer="94"/>
<wire x1="330.165709375" y1="44.662090625" x2="330.25715" y2="44.6278" width="0.025" layer="94"/>
<wire x1="330.25715" y1="44.6278" x2="330.3143" y2="44.604940625" width="0.025" layer="94"/>
<wire x1="330.3143" y1="44.604940625" x2="330.394309375" y2="44.57065" width="0.025" layer="94"/>
<wire x1="330.394309375" y1="44.57065" x2="330.508609375" y2="44.52493125" width="0.025" layer="94"/>
<wire x1="330.508609375" y1="44.52493125" x2="330.87436875" y2="44.34205" width="0.025" layer="94"/>
<wire x1="330.87436875" y1="44.34205" x2="331.04581875" y2="44.23918125" width="0.025" layer="94"/>
<wire x1="331.04581875" y1="44.23918125" x2="331.080109375" y2="44.21631875" width="0.025" layer="94"/>
<wire x1="331.080109375" y1="44.21631875" x2="331.137259375" y2="44.1706" width="0.025" layer="94"/>
<wire x1="331.137259375" y1="44.1706" x2="331.36471875" y2="44.000009375" width="0.025" layer="94"/>
<wire x1="331.36471875" y1="44.000009375" x2="331.451590625" y2="43.91341875" width="0.025" layer="94"/>
<wire x1="331.451590625" y1="43.91341875" x2="331.62875" y2="43.736259375" width="0.025" layer="94"/>
<wire x1="331.62875" y1="43.736259375" x2="331.67446875" y2="43.679109375" width="0.025" layer="94"/>
<wire x1="331.67446875" y1="43.679109375" x2="331.74305" y2="43.58766875" width="0.025" layer="94"/>
<wire x1="331.74305" y1="43.58766875" x2="331.777340625" y2="43.53051875" width="0.025" layer="94"/>
<wire x1="331.777340625" y1="43.53051875" x2="331.823059375" y2="43.450509375" width="0.025" layer="94"/>
<wire x1="331.823059375" y1="43.450509375" x2="331.85735" y2="43.38193125" width="0.025" layer="94"/>
<wire x1="331.85735" y1="43.38193125" x2="331.880209375" y2="43.32478125" width="0.025" layer="94"/>
<wire x1="331.880209375" y1="43.32478125" x2="331.90306875" y2="43.2562" width="0.025" layer="94"/>
<wire x1="331.90306875" y1="43.2562" x2="331.92593125" y2="43.164759375" width="0.025" layer="94"/>
<wire x1="331.92593125" y1="43.164759375" x2="331.937359375" y2="43.095040625" width="0.025" layer="94"/>
<wire x1="331.937359375" y1="43.095040625" x2="331.948790625" y2="42.993309375" width="0.025" layer="94"/>
<wire x1="331.948790625" y1="42.993309375" x2="331.948790625" y2="42.84471875" width="0.025" layer="94"/>
<wire x1="331.948790625" y1="42.84471875" x2="331.937359375" y2="42.75328125" width="0.025" layer="94"/>
<wire x1="331.937359375" y1="42.75328125" x2="331.92593125" y2="42.69613125" width="0.025" layer="94"/>
<wire x1="331.92593125" y1="42.69613125" x2="331.90306875" y2="42.604690625" width="0.025" layer="94"/>
<wire x1="331.90306875" y1="42.604690625" x2="331.886609375" y2="42.55783125" width="0.025" layer="94"/>
<wire x1="331.886609375" y1="42.55783125" x2="331.84591875" y2="42.4561" width="0.025" layer="94"/>
<wire x1="331.84591875" y1="42.4561" x2="331.777340625" y2="42.318940625" width="0.025" layer="94"/>
<wire x1="331.777340625" y1="42.318940625" x2="331.708759375" y2="42.21606875" width="0.025" layer="94"/>
<wire x1="331.708759375" y1="42.21606875" x2="331.67446875" y2="42.17035" width="0.025" layer="94"/>
<wire x1="331.67446875" y1="42.17035" x2="331.56016875" y2="42.033190625" width="0.025" layer="94"/>
<wire x1="331.56016875" y1="42.033190625" x2="331.40015" y2="41.87316875" width="0.025" layer="94"/>
<wire x1="331.40015" y1="41.87316875" x2="331.262990625" y2="41.75886875" width="0.025" layer="94"/>
<wire x1="331.262990625" y1="41.75886875" x2="331.17155" y2="41.690290625" width="0.025" layer="94"/>
<wire x1="331.17155" y1="41.690290625" x2="331.034390625" y2="41.59885" width="0.025" layer="94"/>
<wire x1="331.034390625" y1="41.59885" x2="330.977240625" y2="41.564559375" width="0.025" layer="94"/>
<wire x1="330.977240625" y1="41.564559375" x2="330.89723125" y2="41.518840625" width="0.025" layer="94"/>
<wire x1="330.89723125" y1="41.518840625" x2="330.84008125" y2="41.48455" width="0.025" layer="94"/>
<wire x1="330.84008125" y1="41.48455" x2="330.5429" y2="41.335959375" width="0.025" layer="94"/>
<wire x1="330.5429" y1="41.335959375" x2="330.462890625" y2="41.30166875" width="0.025" layer="94"/>
<wire x1="330.462890625" y1="41.30166875" x2="330.348590625" y2="41.25595" width="0.025" layer="94"/>
<wire x1="330.348590625" y1="41.25595" x2="330.25715" y2="41.221659375" width="0.025" layer="94"/>
<wire x1="330.25715" y1="41.221659375" x2="330.13141875" y2="41.175940625" width="0.025" layer="94"/>
<wire x1="330.13141875" y1="41.175940625" x2="329.994259375" y2="41.13021875" width="0.025" layer="94"/>
<wire x1="329.994259375" y1="41.13021875" x2="329.879959375" y2="41.09593125" width="0.025" layer="94"/>
<wire x1="329.879959375" y1="41.09593125" x2="329.75423125" y2="41.061640625" width="0.025" layer="94"/>
<wire x1="329.75423125" y1="41.061640625" x2="329.67421875" y2="41.03878125" width="0.025" layer="94"/>
<wire x1="329.67421875" y1="41.03878125" x2="329.52563125" y2="41.004490625" width="0.025" layer="94"/>
<wire x1="329.52563125" y1="41.004490625" x2="329.319890625" y2="40.95876875" width="0.025" layer="94"/>
<wire x1="329.319890625" y1="40.95876875" x2="329.194159375" y2="40.935909375" width="0.025" layer="94"/>
<wire x1="328.35108125" y1="37.03828125" x2="327.58253125" y2="40.78731875" width="0.025" layer="94"/>
<wire x1="327.58253125" y1="40.78731875" x2="326.86038125" y2="37.084" width="0.025" layer="94"/>
<wire x1="326.86038125" y1="37.084" x2="326.805290625" y2="37.01541875" width="0.025" layer="94"/>
<wire x1="271.78" y1="236.22" x2="271.78" y2="231.14" width="0.025" layer="94"/>
<wire x1="271.78" y1="231.14" x2="271.78" y2="226.06" width="0.025" layer="94"/>
<wire x1="271.78" y1="226.06" x2="279.4" y2="226.06" width="0.025" layer="94"/>
<wire x1="279.4" y1="226.06" x2="289.56" y2="226.06" width="0.025" layer="94"/>
<wire x1="289.56" y1="226.06" x2="299.72" y2="226.06" width="0.025" layer="94"/>
<wire x1="299.72" y1="226.06" x2="309.88" y2="226.06" width="0.025" layer="94"/>
<wire x1="309.88" y1="226.06" x2="375.92" y2="226.06" width="0.025" layer="94"/>
<wire x1="271.78" y1="231.14" x2="375.92" y2="231.14" width="0.025" layer="94"/>
<wire x1="279.4" y1="236.22" x2="279.4" y2="226.06" width="0.025" layer="94"/>
<wire x1="289.56" y1="236.22" x2="289.56" y2="226.06" width="0.025" layer="94"/>
<wire x1="299.72" y1="236.22" x2="299.72" y2="226.06" width="0.025" layer="94"/>
<wire x1="309.88" y1="236.22" x2="309.88" y2="226.06" width="0.025" layer="94"/>
<text x="272.796" y="35.306" size="1.778" layer="94" font="vector" ratio="12">DRAWN
BY</text>
<text x="272.542" y="28.702" size="1.778" layer="94" font="vector" ratio="12">CHECKED
BY</text>
<text x="272.542" y="22.352" size="1.778" layer="94" font="vector" ratio="12">DESIGN
APPROVAL</text>
<text x="287.02" y="42.164" size="1.778" layer="94" font="vector" ratio="12">INIT</text>
<text x="294.386" y="42.164" size="1.778" layer="94" font="vector" ratio="12">DATE</text>
<text x="333.502" y="33.528" size="1.6764" layer="94" font="vector" ratio="12">JOHN MEZZALINGUA ASSOCIATES
 7645 HENRY CLAY BOULEVARD
  LIVERPOOL, NEW YORK 13088
       TEL. 315-431-7100</text>
<text x="307.34" y="22.86" size="1.27" layer="94" font="vector" ratio="12">THIS DRAWING AND SPECIFICATIONS ARE THE PROPERTY OF
JOHN MEZZALINGUA ASSOCIATES AND SHALL NOT BE 
REPRODICED, COPIED, OR USED AS A BASIS FOR MANUFACTURING OR
SALE OF EQUIPMENT OR DEVICES WITHOUT WRITTEN PERMISSION.</text>
<text x="305.562" y="19.812" size="1.27" layer="94" font="vector" ratio="12">TITLE: </text>
<text x="305.562" y="13.462" size="1.27" layer="94" font="vector" ratio="12">DWG. NO.</text>
<text x="370.332" y="13.462" size="1.27" layer="94" font="vector" ratio="12">REV.</text>
<text x="360.172" y="7.112" size="1.27" layer="94" font="vector" ratio="12">SIZE:   B</text>
<text x="323.342" y="7.112" size="1.27" layer="94" font="vector" ratio="12">SHEET</text>
<text x="305.562" y="7.112" size="1.27" layer="94" font="vector" ratio="12">SCALE: NONE</text>
<text x="317.5" y="10.16" size="1.778" layer="94" font="vector">&gt;DRAWING_NAME</text>
<text x="332.486" y="5.842" size="1.27" layer="94" font="vector">&gt;SHEET</text>
<text x="273.304" y="233.68" size="1.778" layer="94" font="vector" ratio="12">REV</text>
<text x="281.178" y="233.68" size="1.778" layer="94" font="vector" ratio="12">ZONE</text>
<text x="291.592" y="233.68" size="1.778" layer="94" font="vector" ratio="12">DATE</text>
<text x="302.26" y="233.68" size="1.778" layer="94" font="vector" ratio="12">ECO</text>
<text x="333.502" y="233.426" size="1.778" layer="94" font="vector" ratio="12">DESCRIPTION</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="A3_LOC_JMA_RELEASE">
<gates>
<gate name="G$1" symbol="A3_LOC_JMA_RELEASE" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="con-samtec" urn="urn:adsk.eagle:library:184">
<description>&lt;b&gt;Samtec Connectors&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="TSW-104-XX-G-D" library_version="2">
<description>&lt;b&gt;THROUGH-HOLE .025" SQ POST HEADER&lt;/b&gt;&lt;p&gt;
Source: Samtec TSW.pdf</description>
<wire x1="-5.209" y1="2.425" x2="5.209" y2="2.425" width="0.2032" layer="21"/>
<wire x1="5.209" y1="2.425" x2="5.209" y2="-2.425" width="0.2032" layer="21"/>
<wire x1="5.209" y1="-2.425" x2="-5.209" y2="-2.425" width="0.2032" layer="21"/>
<wire x1="-5.209" y1="-2.425" x2="-5.209" y2="2.425" width="0.2032" layer="21"/>
<pad name="1" x="3.81" y="-1.27" drill="1" diameter="1.5" shape="octagon" rot="R180"/>
<pad name="2" x="3.81" y="1.27" drill="1" diameter="1.5" shape="octagon" rot="R180"/>
<pad name="3" x="1.27" y="-1.27" drill="1" diameter="1.5" shape="octagon" rot="R180"/>
<pad name="4" x="1.27" y="1.27" drill="1" diameter="1.5" shape="octagon" rot="R180"/>
<pad name="5" x="-1.27" y="-1.27" drill="1" diameter="1.5" shape="octagon" rot="R180"/>
<pad name="6" x="-1.27" y="1.27" drill="1" diameter="1.5" shape="octagon" rot="R180"/>
<pad name="7" x="-3.81" y="-1.27" drill="1" diameter="1.5" shape="octagon" rot="R180"/>
<pad name="8" x="-3.81" y="1.27" drill="1" diameter="1.5" shape="octagon" rot="R180"/>
<text x="3.602" y="-3.818" size="1.1" layer="21" font="vector" rot="SR0">1</text>
<text x="3.427" y="2.744" size="1.1" layer="21" font="vector" rot="SR0">2</text>
<text x="-5.715" y="-2.54" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="6.985" y="-2.54" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-4.16" y1="-1.62" x2="-3.46" y2="-0.92" layer="51"/>
<rectangle x1="-4.16" y1="0.92" x2="-3.46" y2="1.62" layer="51"/>
<rectangle x1="-1.62" y1="-1.62" x2="-0.92" y2="-0.92" layer="51"/>
<rectangle x1="-1.62" y1="0.92" x2="-0.92" y2="1.62" layer="51"/>
<rectangle x1="0.92" y1="-1.62" x2="1.62" y2="-0.92" layer="51"/>
<rectangle x1="0.92" y1="0.92" x2="1.62" y2="1.62" layer="51"/>
<rectangle x1="3.46" y1="-1.62" x2="4.16" y2="-0.92" layer="51"/>
<rectangle x1="3.46" y1="0.92" x2="4.16" y2="1.62" layer="51"/>
</package>
<package name="TSW-104-08-G-D-RA" library_version="2">
<description>&lt;b&gt;THROUGH-HOLE .025" SQ POST HEADER&lt;/b&gt;&lt;p&gt;
Source: Samtec TSW.pdf</description>
<wire x1="-5.209" y1="-2.046" x2="5.209" y2="-2.046" width="0.2032" layer="21"/>
<wire x1="5.209" y1="-2.046" x2="5.209" y2="-0.106" width="0.2032" layer="21"/>
<wire x1="5.209" y1="-0.106" x2="-5.209" y2="-0.106" width="0.2032" layer="21"/>
<wire x1="-5.209" y1="-0.106" x2="-5.209" y2="-2.046" width="0.2032" layer="21"/>
<pad name="1" x="3.81" y="1.524" drill="1" diameter="1.5" shape="octagon" rot="R180"/>
<pad name="2" x="3.81" y="4.064" drill="1" diameter="1.5" shape="octagon" rot="R180"/>
<pad name="3" x="1.27" y="1.524" drill="1" diameter="1.5" shape="octagon" rot="R180"/>
<pad name="4" x="1.27" y="4.064" drill="1" diameter="1.5" shape="octagon" rot="R180"/>
<pad name="5" x="-1.27" y="1.524" drill="1" diameter="1.5" shape="octagon" rot="R180"/>
<pad name="6" x="-1.27" y="4.064" drill="1" diameter="1.5" shape="octagon" rot="R180"/>
<pad name="7" x="-3.81" y="1.524" drill="1" diameter="1.5" shape="octagon" rot="R180"/>
<pad name="8" x="-3.81" y="4.064" drill="1" diameter="1.5" shape="octagon" rot="R180"/>
<text x="-5.715" y="-7.62" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="6.985" y="-7.62" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<text x="5.095" y="0.775" size="1.1" layer="21" font="vector" rot="SR0">1</text>
<text x="5.06" y="3.29" size="1.1" layer="21" font="vector" rot="SR0">2</text>
<rectangle x1="-4.064" y1="0" x2="-3.556" y2="4.318" layer="51"/>
<rectangle x1="-1.524" y1="0" x2="-1.016" y2="4.318" layer="51"/>
<rectangle x1="1.016" y1="0" x2="1.524" y2="4.318" layer="51"/>
<rectangle x1="3.556" y1="0" x2="4.064" y2="4.318" layer="51"/>
<rectangle x1="-4.064" y1="-7.89" x2="-3.556" y2="-2.04" layer="21"/>
<rectangle x1="-1.524" y1="-7.89" x2="-1.016" y2="-2.04" layer="21"/>
<rectangle x1="1.016" y1="-7.89" x2="1.524" y2="-2.04" layer="21"/>
<rectangle x1="3.556" y1="-7.89" x2="4.064" y2="-2.04" layer="21"/>
</package>
<package name="TSW-104-XX-G-Q" library_version="2">
<description>&lt;b&gt;THROUGH-HOLE .025" SQ POST HEADER&lt;/b&gt;&lt;p&gt;
Source: Samtec TSW.pdf</description>
<wire x1="-5.209" y1="3.695" x2="5.209" y2="3.695" width="0.2032" layer="21"/>
<wire x1="5.209" y1="3.695" x2="5.209" y2="-3.695" width="0.2032" layer="21"/>
<wire x1="5.209" y1="-3.695" x2="-5.209" y2="-3.695" width="0.2032" layer="21"/>
<wire x1="-5.209" y1="-3.695" x2="-5.209" y2="3.695" width="0.2032" layer="21"/>
<pad name="1" x="3.81" y="-2.54" drill="1" diameter="1.5" shape="octagon" rot="R180"/>
<pad name="2" x="3.81" y="2.54" drill="1" diameter="1.5" shape="octagon" rot="R180"/>
<pad name="3" x="1.27" y="-2.54" drill="1" diameter="1.5" shape="octagon" rot="R180"/>
<pad name="4" x="1.27" y="2.54" drill="1" diameter="1.5" shape="octagon" rot="R180"/>
<pad name="5" x="-1.27" y="-2.54" drill="1" diameter="1.5" shape="octagon" rot="R180"/>
<pad name="6" x="-1.27" y="2.54" drill="1" diameter="1.5" shape="octagon" rot="R180"/>
<pad name="7" x="-3.81" y="-2.54" drill="1" diameter="1.5" shape="octagon" rot="R180"/>
<pad name="8" x="-3.81" y="2.54" drill="1" diameter="1.5" shape="octagon" rot="R180"/>
<text x="3.652" y="-5.138" size="1.1" layer="21" font="vector" rot="SR0">1</text>
<text x="3.552" y="3.989" size="1.1" layer="21" font="vector" rot="SR0">2</text>
<text x="-5.715" y="-3.81" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="6.985" y="-3.81" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-4.16" y1="-2.89" x2="-3.46" y2="-2.19" layer="51"/>
<rectangle x1="-4.16" y1="2.19" x2="-3.46" y2="2.89" layer="51"/>
<rectangle x1="-1.62" y1="-2.89" x2="-0.92" y2="-2.19" layer="51"/>
<rectangle x1="-1.62" y1="2.19" x2="-0.92" y2="2.89" layer="51"/>
<rectangle x1="0.92" y1="-2.89" x2="1.62" y2="-2.19" layer="51"/>
<rectangle x1="0.92" y1="2.19" x2="1.62" y2="2.89" layer="51"/>
<rectangle x1="3.46" y1="-2.89" x2="4.16" y2="-2.19" layer="51"/>
<rectangle x1="3.46" y1="2.19" x2="4.16" y2="2.89" layer="51"/>
</package>
<package name="TSW-104-08-G-Q-RA" library_version="2">
<description>&lt;b&gt;THROUGH-HOLE .025" SQ POST HEADER&lt;/b&gt;&lt;p&gt;
Source: Samtec TSW.pdf</description>
<wire x1="-5.209" y1="-2.046" x2="5.209" y2="-2.046" width="0.2032" layer="21"/>
<wire x1="5.209" y1="-2.046" x2="5.209" y2="-0.106" width="0.2032" layer="21"/>
<wire x1="5.209" y1="-0.106" x2="-5.209" y2="-0.106" width="0.2032" layer="21"/>
<wire x1="-5.209" y1="-0.106" x2="-5.209" y2="-2.046" width="0.2032" layer="21"/>
<pad name="1" x="3.81" y="1.524" drill="1" diameter="1.5" shape="octagon" rot="R180"/>
<pad name="2" x="3.81" y="6.604" drill="1" diameter="1.5" shape="octagon" rot="R180"/>
<pad name="3" x="1.27" y="1.524" drill="1" diameter="1.5" shape="octagon" rot="R180"/>
<pad name="4" x="1.27" y="6.604" drill="1" diameter="1.5" shape="octagon" rot="R180"/>
<pad name="5" x="-1.27" y="1.524" drill="1" diameter="1.5" shape="octagon" rot="R180"/>
<pad name="6" x="-1.27" y="6.604" drill="1" diameter="1.5" shape="octagon" rot="R180"/>
<pad name="7" x="-3.81" y="1.524" drill="1" diameter="1.5" shape="octagon" rot="R180"/>
<pad name="8" x="-3.81" y="6.604" drill="1" diameter="1.5" shape="octagon" rot="R180"/>
<text x="5.17" y="1.1" size="1.1" layer="21" font="vector" rot="SR0">1</text>
<text x="5.11" y="6.165" size="1.1" layer="21" font="vector" rot="SR0">2</text>
<text x="-5.715" y="-7.62" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="6.985" y="-7.62" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-4.064" y1="0" x2="-3.556" y2="6.858" layer="51"/>
<rectangle x1="-1.524" y1="0" x2="-1.016" y2="6.858" layer="51"/>
<rectangle x1="1.016" y1="0" x2="1.524" y2="6.858" layer="51"/>
<rectangle x1="3.556" y1="0" x2="4.064" y2="6.858" layer="51"/>
<rectangle x1="-4.064" y1="-7.89" x2="-3.556" y2="-2.04" layer="21"/>
<rectangle x1="-1.524" y1="-7.89" x2="-1.016" y2="-2.04" layer="21"/>
<rectangle x1="1.016" y1="-7.89" x2="1.524" y2="-2.04" layer="21"/>
<rectangle x1="3.556" y1="-7.89" x2="4.064" y2="-2.04" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="MPINV" library_version="2">
<text x="-1.27" y="1.27" size="1.778" layer="96">&gt;VALUE</text>
<text x="2.54" y="-0.762" size="1.524" layer="95">&gt;NAME</text>
<rectangle x1="0" y1="-0.254" x2="1.778" y2="0.254" layer="94"/>
<pin name="1" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
</symbol>
<symbol name="MPIN" library_version="2">
<text x="2.54" y="-0.762" size="1.524" layer="95">&gt;NAME</text>
<rectangle x1="0" y1="-0.254" x2="1.778" y2="0.254" layer="94"/>
<pin name="1" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="TSW-104-*-G" prefix="X" library_version="2">
<description>&lt;b&gt;THROUGH-HOLE .025" SQ POST HEADER&lt;/b&gt;&lt;p&gt;
Source: Samtec TSW.pdf</description>
<gates>
<gate name="-1" symbol="MPINV" x="-7.62" y="2.54" addlevel="always"/>
<gate name="-2" symbol="MPIN" x="10.16" y="2.54" addlevel="always"/>
<gate name="-3" symbol="MPIN" x="-7.62" y="0" addlevel="always"/>
<gate name="-4" symbol="MPIN" x="10.16" y="0" addlevel="always"/>
<gate name="-5" symbol="MPIN" x="-7.62" y="-2.54" addlevel="always"/>
<gate name="-6" symbol="MPIN" x="10.16" y="-2.54" addlevel="always"/>
<gate name="-7" symbol="MPIN" x="-7.62" y="-5.08" addlevel="always"/>
<gate name="-8" symbol="MPIN" x="10.16" y="-5.08" addlevel="always"/>
</gates>
<devices>
<device name="-D" package="TSW-104-XX-G-D">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-2" pin="1" pad="2"/>
<connect gate="-3" pin="1" pad="3"/>
<connect gate="-4" pin="1" pad="4"/>
<connect gate="-5" pin="1" pad="5"/>
<connect gate="-6" pin="1" pad="6"/>
<connect gate="-7" pin="1" pad="7"/>
<connect gate="-8" pin="1" pad="8"/>
</connects>
<technologies>
<technology name="07">
<attribute name="MF" value="Samtec Inc." constant="no"/>
<attribute name="MPN" value="TSW-104-07-G-D" constant="no"/>
<attribute name="OC_FARNELL" value="" constant="no"/>
<attribute name="OC_NEWARK" value="" constant="no"/>
</technology>
<technology name="08">
<attribute name="MF" value="Samtec Inc." constant="no"/>
<attribute name="MPN" value="TSW-104-07-G-D" constant="no"/>
<attribute name="OC_FARNELL" value="" constant="no"/>
<attribute name="OC_NEWARK" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="-D-RA" package="TSW-104-08-G-D-RA">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-2" pin="1" pad="2"/>
<connect gate="-3" pin="1" pad="3"/>
<connect gate="-4" pin="1" pad="4"/>
<connect gate="-5" pin="1" pad="5"/>
<connect gate="-6" pin="1" pad="6"/>
<connect gate="-7" pin="1" pad="7"/>
<connect gate="-8" pin="1" pad="8"/>
</connects>
<technologies>
<technology name="08">
<attribute name="MF" value="Samtec Inc." constant="no"/>
<attribute name="MPN" value="TSW-104-08-G-D-RA" constant="no"/>
</technology>
</technologies>
</device>
<device name="-Q" package="TSW-104-XX-G-Q">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-2" pin="1" pad="2"/>
<connect gate="-3" pin="1" pad="3"/>
<connect gate="-4" pin="1" pad="4"/>
<connect gate="-5" pin="1" pad="5"/>
<connect gate="-6" pin="1" pad="6"/>
<connect gate="-7" pin="1" pad="7"/>
<connect gate="-8" pin="1" pad="8"/>
</connects>
<technologies>
<technology name="07">
<attribute name="MF" value="Samtec Inc." constant="no"/>
<attribute name="MPN" value="TSW-104-07-G-Q" constant="no"/>
</technology>
<technology name="08">
<attribute name="MF" value="Samtec Inc." constant="no"/>
<attribute name="MPN" value="TSW-104-07-G-Q" constant="no"/>
</technology>
</technologies>
</device>
<device name="-Q-RA" package="TSW-104-08-G-Q-RA">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-2" pin="1" pad="2"/>
<connect gate="-3" pin="1" pad="3"/>
<connect gate="-4" pin="1" pad="4"/>
<connect gate="-5" pin="1" pad="5"/>
<connect gate="-6" pin="1" pad="6"/>
<connect gate="-7" pin="1" pad="7"/>
<connect gate="-8" pin="1" pad="8"/>
</connects>
<technologies>
<technology name="08">
<attribute name="MF" value="Samtec Inc." constant="no"/>
<attribute name="MPN" value="TSW-104-08-G-Q-RA" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
<class number="1" name="RF" width="0.237490625" drill="0.254">
<clearance class="0" value="0.508"/>
<clearance class="1" value="0.508"/>
</class>
</classes>
<parts>
<part name="U1" library="JMA_LBR" deviceset="TI_SN65HVD62RGTR" device="" value="SN65HVD63RGTT"/>
<part name="C11" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="39pF"/>
<part name="C10" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="39pF"/>
<part name="R9" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="49.9"/>
<part name="C2" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="470pF"/>
<part name="C7" library="JMA_LBR" deviceset="CAP_SMD_" device="0805" value="0.22uF 5% , 100V"/>
<part name="R4" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="4.12K"/>
<part name="R1" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="10K"/>
<part name="SUPPLY2" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="R6" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="0"/>
<part name="R5" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="0"/>
<part name="R13" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="120"/>
<part name="D2" library="JMA_LBR" deviceset="STM_SM15T" device="" value="SM15T10CA"/>
<part name="D3" library="JMA_LBR" deviceset="STM_SM15T" device="" value="SM15T10CA"/>
<part name="P+1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="P+2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="P+3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="R7" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="0"/>
<part name="R8" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="0"/>
<part name="R11" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="0"/>
<part name="SUPPLY4" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="U2" library="JMA_LBR" deviceset="TI_DS3695AX" device="" value="SN65176BDR"/>
<part name="R15" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="DNP"/>
<part name="C5" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="0.1uF"/>
<part name="R14" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="DNP"/>
<part name="C3" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="0.1uF"/>
<part name="C8" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="0.1uF"/>
<part name="D100" library="JMA_LBR" deviceset="1.5SMC480CA" device="" value="1.5SMC480A"/>
<part name="C6" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="1uF"/>
<part name="C4" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="1uF"/>
<part name="C9" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="1uF"/>
<part name="F102" library="JMA_LBR" deviceset="GDT_2029-XX-SMLF" device="" value="B88069X1630T602"/>
<part name="F101" library="JMA_LBR" deviceset="GDT_2029-XX-SMLF" device="" value="B88069X1630T602"/>
<part name="F5" library="JMA_LBR" deviceset="GDT_2029-XX-SMLF" device="" value="B88069X1630T602"/>
<part name="F2" library="JMA_LBR" deviceset="GDT_2029-XX-SMLF" device="" value="B88069X1630T602"/>
<part name="C101" library="JMA_LBR" deviceset="CAP_SMD_" device="0805" value="47pF, 100V"/>
<part name="D1" library="JMA_LBR" deviceset="ZENER_UNIDIRECTIONAL_SMT" device="DO-214AB,SMC" value="SM15T6V8A"/>
<part name="C1" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="1uF"/>
<part name="R2" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="DNP"/>
<part name="R3" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="DNP"/>
<part name="R12" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="DNP"/>
<part name="TP2" library="JMA_LBR" deviceset="TP_CLASSB_" device="1MM_CIR" value="TP_OOK"/>
<part name="TP4" library="JMA_LBR" deviceset="TP_CLASSB_" device="1MM_CIR" value="TP_OOK_OUT"/>
<part name="DP" library="JMA_LBR" deviceset="TP_CLASSB_" device="1MM_CIR" value="TP_DN"/>
<part name="DN" library="JMA_LBR" deviceset="TP_CLASSB_" device="1MM_CIR" value="TP_DP"/>
<part name="TP5" library="JMA_LBR" deviceset="TP_CLASSB_" device="1MM_CIR" value="TP_RX"/>
<part name="TP1" library="JMA_LBR" deviceset="TP_CLASSB_" device="1MM_CIR" value="TP_DIR"/>
<part name="TP3" library="JMA_LBR" deviceset="TP_CLASSB_" device="1MM_CIR" value="TP_TX"/>
<part name="F1" library="JMA_LBR" deviceset="0.5MM_FIDUCIAL" device=""/>
<part name="F4" library="JMA_LBR" deviceset="0.5MM_FIDUCIAL" device=""/>
<part name="F104" library="JMA_LBR" deviceset="0.5MM_FIDUCIAL" device=""/>
<part name="F100" library="JMA_LBR" deviceset="0.5MM_FIDUCIAL" device=""/>
<part name="F103" library="JMA_LBR" deviceset="0.5MM_FIDUCIAL" device=""/>
<part name="F3" library="JMA_LBR" deviceset="0.5MM_FIDUCIAL" device=""/>
<part name="C102" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="DNP"/>
<part name="SUPPLY1" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="U$2" library="frames" deviceset="A3_LOC_JMA_RELEASE" device=""/>
<part name="J100" library="JMA_LBR" deviceset="TRANSISTION_CLIP" device="TRANSITION_CLIP_.141" value="415-0029-mm750"/>
<part name="U101" library="JMA_LBR" deviceset="TI_LM22672MRX-ADJ" device="MRA08B-M" value="LM22672MRX-ADJ/NOPB"/>
<part name="D101" library="JMA_LBR" deviceset="DIODE_SCHOTTKY_DO-214AC" device="" value="CDBA140-G"/>
<part name="C109" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="10uF"/>
<part name="C110" library="JMA_LBR" deviceset="CAP_SMD_" device="0805" value="22uF"/>
<part name="C111" library="JMA_LBR" deviceset="CAP_SMD_" device="0805" value="22uF"/>
<part name="R111" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="1K"/>
<part name="R109" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="2.87K"/>
<part name="SUPPLY6" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY7" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="C112" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="1uF"/>
<part name="R115" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="DNP"/>
<part name="P+5" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="SUPPLY8" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="FB100" library="JMA_LBR" deviceset="FERRITE_BEAD" device="0603" value="MPZ1608S300ATAH0"/>
<part name="D102" library="JMA_LBR" deviceset="ZENER_ONSEMI_1SMC5.0AT3G" device="" value="SMCJ33A-13-F"/>
<part name="C105" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="DNP"/>
<part name="C104" library="JMA_LBR" deviceset="CAP_SMD_" device="0805" value="3.3uF, 50V"/>
<part name="L200" library="JMA_LBR" deviceset="IND_SMD_5.0X5.0" device="" value="SRN5040-220M"/>
<part name="Y1" library="JMA_LBR" deviceset="XTAL_HC49/US" device="" value="FF40BI1-8.704MHz-T "/>
<part name="C106" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="10nF"/>
<part name="R114" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="3"/>
<part name="R112" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="5.1"/>
<part name="C108" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="330pF"/>
<part name="U100" library="JMA_LBR" deviceset="TI_TPS2491DGSR" device="10-TFSOP/10-MSOP"/>
<part name="R104" library="JMA_LBR" deviceset="RES_SMD_" device="0805" value="0.2"/>
<part name="R102" library="JMA_LBR" deviceset="RES_SMD_" device="0805" value="0"/>
<part name="R103" library="JMA_LBR" deviceset="RES_SMD_" device="0805" value="DNP"/>
<part name="Q100" library="JMA_LBR" deviceset="MOSFET_NMOS" device="DPAK/TO-252-3" value="BUK72150-55A,118"/>
<part name="R110" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="10K"/>
<part name="R113" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="8.87K"/>
<part name="R106" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="56K"/>
<part name="R107" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="10K"/>
<part name="R101" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="10"/>
<part name="R105" library="JMA_LBR" deviceset="RES_SMD_" device="0805" value="470K"/>
<part name="R100" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="1K"/>
<part name="C100" library="JMA_LBR" deviceset="CAP_SMD_" device="0805" value="22nF"/>
<part name="C103" library="JMA_LBR" deviceset="CAP_SMD_" device="0805" value="DNP"/>
<part name="C107" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="6.8nF"/>
<part name="SUPPLY11" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="R108" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="0"/>
<part name="R10" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="20"/>
<part name="L100" library="JMA_LBR" deviceset="IND_SMD_5.0X5.0" device="" value="SRN5040-100M"/>
<part name="L1" library="JMA_LBR" deviceset="IND_SMD_5.0X5.0" device="" value="SRN5040-100M"/>
<part name="U7" library="JMA_LBR" deviceset="TI_SN65HVD62RGTR" device="" value="SN65HVD63RGTT"/>
<part name="C36" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="39pF"/>
<part name="C37" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="39pF"/>
<part name="R47" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="49.9"/>
<part name="C38" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="470pF"/>
<part name="C39" library="JMA_LBR" deviceset="CAP_SMD_" device="0805" value="0.22uF 5% , 100V"/>
<part name="R48" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="4.12K"/>
<part name="R49" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="10K"/>
<part name="SUPPLY15" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="R50" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="0"/>
<part name="R51" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="0"/>
<part name="R52" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="120"/>
<part name="D10" library="JMA_LBR" deviceset="STM_SM15T" device="" value="SM15T10CA"/>
<part name="D11" library="JMA_LBR" deviceset="STM_SM15T" device="" value="SM15T10CA"/>
<part name="P+9" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="P+10" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="P+11" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="R53" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="0"/>
<part name="R54" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="0"/>
<part name="R55" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="0"/>
<part name="SUPPLY16" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="U8" library="JMA_LBR" deviceset="TI_DS3695AX" device="" value="SN65176BDR"/>
<part name="R56" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="DNP"/>
<part name="C40" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="0.1uF"/>
<part name="R57" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="DNP"/>
<part name="C41" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="0.1uF"/>
<part name="C42" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="0.1uF"/>
<part name="D12" library="JMA_LBR" deviceset="1.5SMC480CA" device="" value="1.5SMC480A"/>
<part name="C43" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="1uF"/>
<part name="C44" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="1uF"/>
<part name="C45" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="1uF"/>
<part name="F16" library="JMA_LBR" deviceset="GDT_2029-XX-SMLF" device="" value="B88069X1630T602"/>
<part name="F17" library="JMA_LBR" deviceset="GDT_2029-XX-SMLF" device="" value="B88069X1630T602"/>
<part name="F18" library="JMA_LBR" deviceset="GDT_2029-XX-SMLF" device="" value="B88069X1630T602"/>
<part name="F19" library="JMA_LBR" deviceset="GDT_2029-XX-SMLF" device="" value="B88069X1630T602"/>
<part name="C46" library="JMA_LBR" deviceset="CAP_SMD_" device="0805" value="47pF, 100V"/>
<part name="D13" library="JMA_LBR" deviceset="ZENER_UNIDIRECTIONAL_SMT" device="DO-214AB,SMC" value="SM15T6V8A"/>
<part name="C47" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="1uF"/>
<part name="R58" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="DNP"/>
<part name="R59" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="DNP"/>
<part name="R60" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="DNP"/>
<part name="TP11" library="JMA_LBR" deviceset="TP_CLASSB_" device="1MM_CIR" value="TP_OOK"/>
<part name="TP12" library="JMA_LBR" deviceset="TP_CLASSB_" device="1MM_CIR" value="TP_OOK_OUT"/>
<part name="DP2" library="JMA_LBR" deviceset="TP_CLASSB_" device="1MM_CIR" value="TP_DN"/>
<part name="DN2" library="JMA_LBR" deviceset="TP_CLASSB_" device="1MM_CIR" value="TP_DP"/>
<part name="TP13" library="JMA_LBR" deviceset="TP_CLASSB_" device="1MM_CIR" value="TP_RX"/>
<part name="TP14" library="JMA_LBR" deviceset="TP_CLASSB_" device="1MM_CIR" value="TP_DIR"/>
<part name="TP15" library="JMA_LBR" deviceset="TP_CLASSB_" device="1MM_CIR" value="TP_TX"/>
<part name="F20" library="JMA_LBR" deviceset="0.5MM_FIDUCIAL" device=""/>
<part name="F21" library="JMA_LBR" deviceset="0.5MM_FIDUCIAL" device=""/>
<part name="F22" library="JMA_LBR" deviceset="0.5MM_FIDUCIAL" device=""/>
<part name="F23" library="JMA_LBR" deviceset="0.5MM_FIDUCIAL" device=""/>
<part name="F24" library="JMA_LBR" deviceset="0.5MM_FIDUCIAL" device=""/>
<part name="F25" library="JMA_LBR" deviceset="0.5MM_FIDUCIAL" device=""/>
<part name="C48" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="DNP"/>
<part name="SUPPLY17" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="U$3" library="frames" deviceset="A3_LOC_JMA_RELEASE" device=""/>
<part name="J4" library="JMA_LBR" deviceset="TRANSISTION_CLIP" device="TRANSITION_CLIP_.141" value="415-0029-mm750"/>
<part name="U9" library="JMA_LBR" deviceset="TI_LM22672MRX-ADJ" device="MRA08B-M" value="LM22672MRX-ADJ/NOPB"/>
<part name="D14" library="JMA_LBR" deviceset="DIODE_SCHOTTKY_DO-214AC" device="" value="CDBA140-G"/>
<part name="C49" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="10uF"/>
<part name="C50" library="JMA_LBR" deviceset="CAP_SMD_" device="0805" value="22uF"/>
<part name="C51" library="JMA_LBR" deviceset="CAP_SMD_" device="0805" value="22uF"/>
<part name="R61" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="1K"/>
<part name="R62" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="2.87K"/>
<part name="SUPPLY18" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY19" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="C52" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="1uF"/>
<part name="R63" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="DNP"/>
<part name="P+12" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="SUPPLY20" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="FB2" library="JMA_LBR" deviceset="FERRITE_BEAD" device="0603" value="MPZ1608S300ATAH0"/>
<part name="D15" library="JMA_LBR" deviceset="ZENER_ONSEMI_1SMC5.0AT3G" device="" value="SMCJ33A-13-F"/>
<part name="C53" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="DNP"/>
<part name="C54" library="JMA_LBR" deviceset="CAP_SMD_" device="0805" value="3.3uF, 50V"/>
<part name="L5" library="JMA_LBR" deviceset="IND_SMD_5.0X5.0" device="" value="SRN5040-220M"/>
<part name="Y3" library="JMA_LBR" deviceset="XTAL_HC49/US" device="" value="FF40BI1-8.704MHz-T "/>
<part name="C55" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="10nF"/>
<part name="R64" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="3"/>
<part name="R65" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="5.1"/>
<part name="C56" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="330pF"/>
<part name="U10" library="JMA_LBR" deviceset="TI_TPS2491DGSR" device="10-TFSOP/10-MSOP"/>
<part name="R66" library="JMA_LBR" deviceset="RES_SMD_" device="0805" value="0.2"/>
<part name="R67" library="JMA_LBR" deviceset="RES_SMD_" device="0805" value="0"/>
<part name="R68" library="JMA_LBR" deviceset="RES_SMD_" device="0805" value="DNP"/>
<part name="Q2" library="JMA_LBR" deviceset="MOSFET_NMOS" device="DPAK/TO-252-3" value="BUK72150-55A,118"/>
<part name="R69" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="10K"/>
<part name="R70" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="8.87K"/>
<part name="R71" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="56K"/>
<part name="R72" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="10K"/>
<part name="R73" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="10"/>
<part name="R74" library="JMA_LBR" deviceset="RES_SMD_" device="0805" value="470K"/>
<part name="R75" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="1K"/>
<part name="C57" library="JMA_LBR" deviceset="CAP_SMD_" device="0805" value="22nF"/>
<part name="C58" library="JMA_LBR" deviceset="CAP_SMD_" device="0805" value="DNP"/>
<part name="C59" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="6.8nF"/>
<part name="SUPPLY21" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="R76" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="0"/>
<part name="R77" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="20"/>
<part name="L6" library="JMA_LBR" deviceset="IND_SMD_5.0X5.0" device="" value="SRN5040-100M"/>
<part name="L7" library="JMA_LBR" deviceset="IND_SMD_5.0X5.0" device="" value="SRN5040-100M"/>
<part name="X2" library="con-samtec" library_urn="urn:adsk.eagle:library:184" deviceset="TSW-104-*-G" device="-D-RA" technology="08"/>
</parts>
<sheets>
<sheet>
<plain>
<text x="7.62" y="208.788" size="1.778" layer="95" ratio="15">Modulated RF Input</text>
<text x="312.42" y="17.78" size="1.778" layer="94">AISG 3.0 Modem</text>
<text x="370.84" y="10.16" size="1.778" layer="94">01</text>
<text x="281.94" y="228.6" size="1.778" layer="94">01</text>
<text x="289.56" y="228.6" size="1.778" layer="94">12/28/17</text>
<text x="317.5" y="228.6" size="1.778" layer="94">AISG 3.0 Modem Initial Release</text>
<text x="355.6" y="121.92" size="1.778" layer="95">10V - 30V</text>
<text x="353.06" y="99.06" size="1.778" layer="95">DATA_P</text>
<text x="353.06" y="66.04" size="1.778" layer="95">DATA_N</text>
<text x="353.06" y="50.8" size="1.778" layer="95">GND</text>
</plain>
<instances>
<instance part="U1" gate="A" x="137.16" y="68.58" smashed="yes" rot="MR0">
<attribute name="NAME" x="137.16" y="96.52" size="1.778" layer="95" ratio="15" rot="SMR0"/>
<attribute name="VALUE" x="137.16" y="94.234" size="1.778" layer="96" ratio="15" rot="SMR0"/>
</instance>
<instance part="C11" gate="G$1" x="53.34" y="91.44" smashed="yes" rot="MR180">
<attribute name="NAME" x="52.07" y="91.948" size="1.778" layer="95" ratio="15" rot="MR0"/>
<attribute name="VALUE" x="54.61" y="90.932" size="1.778" layer="96" ratio="15" rot="MR180"/>
</instance>
<instance part="C10" gate="G$1" x="53.34" y="81.28" smashed="yes" rot="MR180">
<attribute name="NAME" x="52.07" y="81.788" size="1.778" layer="95" ratio="15" rot="MR0"/>
<attribute name="VALUE" x="54.61" y="80.772" size="1.778" layer="96" ratio="15" rot="MR180"/>
</instance>
<instance part="R9" gate="G$1" x="86.36" y="78.74" smashed="yes">
<attribute name="NAME" x="81.28" y="78.994" size="1.778" layer="95" ratio="15"/>
<attribute name="VALUE" x="88.9" y="78.994" size="1.778" layer="96" ratio="15"/>
</instance>
<instance part="C2" gate="G$1" x="50.8" y="66.04" smashed="yes" rot="MR90">
<attribute name="NAME" x="50.546" y="69.342" size="1.778" layer="95" ratio="15" rot="MR0"/>
<attribute name="VALUE" x="50.546" y="61.468" size="1.778" layer="96" ratio="15" rot="MR0"/>
</instance>
<instance part="C7" gate="G$1" x="35.56" y="104.14" smashed="yes" rot="R90">
<attribute name="NAME" x="35.306" y="109.22" size="1.778" layer="95" ratio="15" rot="R180"/>
<attribute name="VALUE" x="35.306" y="100.838" size="1.778" layer="96" ratio="15" rot="R180"/>
</instance>
<instance part="R4" gate="G$1" x="86.36" y="73.66" smashed="yes">
<attribute name="NAME" x="80.264" y="73.914" size="1.778" layer="95" ratio="15"/>
<attribute name="VALUE" x="88.9" y="73.914" size="1.778" layer="96" ratio="15"/>
</instance>
<instance part="R1" gate="G$1" x="78.74" y="63.5" smashed="yes" rot="R90">
<attribute name="NAME" x="78.486" y="68.58" size="1.778" layer="95" ratio="15" rot="R180"/>
<attribute name="VALUE" x="78.486" y="60.198" size="1.778" layer="96" ratio="15" rot="R180"/>
</instance>
<instance part="SUPPLY2" gate="GND" x="190.5" y="48.26" smashed="yes">
<attribute name="VALUE" x="190.246" y="50.292" size="1.778" layer="96" ratio="15" rot="R180"/>
</instance>
<instance part="R6" gate="G$1" x="182.88" y="78.74" smashed="yes">
<attribute name="NAME" x="176.022" y="78.994" size="1.778" layer="95" ratio="15"/>
<attribute name="VALUE" x="187.198" y="78.994" size="1.778" layer="96" ratio="15"/>
</instance>
<instance part="R5" gate="G$1" x="182.88" y="76.2" smashed="yes">
<attribute name="NAME" x="176.022" y="76.454" size="1.778" layer="95" ratio="15"/>
<attribute name="VALUE" x="187.198" y="76.454" size="1.778" layer="96" ratio="15"/>
</instance>
<instance part="R13" gate="G$1" x="261.62" y="83.82" smashed="yes" rot="R90">
<attribute name="NAME" x="261.366" y="88.9" size="1.778" layer="95" ratio="15" rot="R180"/>
<attribute name="VALUE" x="261.366" y="80.518" size="1.778" layer="96" ratio="15" rot="R180"/>
</instance>
<instance part="D2" gate="A" x="276.86" y="76.2" smashed="yes" rot="R90">
<attribute name="NAME" x="276.606" y="81.28" size="1.778" layer="95" ratio="15" rot="R180"/>
<attribute name="VALUE" x="276.606" y="72.898" size="1.778" layer="96" ratio="15" rot="R180"/>
</instance>
<instance part="D3" gate="A" x="276.86" y="91.44" smashed="yes" rot="R270">
<attribute name="NAME" x="276.606" y="96.52" size="1.778" layer="95" ratio="15" rot="R180"/>
<attribute name="VALUE" x="276.606" y="88.138" size="1.778" layer="96" ratio="15" rot="R180"/>
</instance>
<instance part="P+1" gate="1" x="96.52" y="104.14" smashed="yes">
<attribute name="VALUE" x="99.568" y="105.918" size="1.778" layer="96" ratio="15" rot="R180"/>
</instance>
<instance part="P+2" gate="1" x="160.02" y="109.22" smashed="yes">
<attribute name="VALUE" x="160.02" y="109.22" size="1.778" layer="96" ratio="15"/>
</instance>
<instance part="P+3" gate="1" x="248.92" y="119.38" smashed="yes">
<attribute name="VALUE" x="248.92" y="119.38" size="1.778" layer="96" ratio="15"/>
</instance>
<instance part="R7" gate="G$1" x="182.88" y="81.28" smashed="yes">
<attribute name="NAME" x="176.022" y="81.534" size="1.778" layer="95" ratio="15"/>
<attribute name="VALUE" x="187.198" y="81.534" size="1.778" layer="96" ratio="15"/>
</instance>
<instance part="R8" gate="G$1" x="182.88" y="83.82" smashed="yes">
<attribute name="NAME" x="176.022" y="84.074" size="1.778" layer="95" ratio="15"/>
<attribute name="VALUE" x="187.198" y="84.074" size="1.778" layer="96" ratio="15"/>
</instance>
<instance part="R11" gate="G$1" x="182.88" y="88.9" smashed="yes">
<attribute name="NAME" x="176.276" y="89.154" size="1.778" layer="95" ratio="15"/>
<attribute name="VALUE" x="187.198" y="89.154" size="1.778" layer="96" ratio="15"/>
</instance>
<instance part="SUPPLY4" gate="GND" x="233.68" y="48.26" smashed="yes">
<attribute name="VALUE" x="233.426" y="50.292" size="1.778" layer="96" ratio="15" rot="R180"/>
</instance>
<instance part="U2" gate="G$1" x="208.28" y="96.52" smashed="yes">
<attribute name="NAME" x="210.82" y="99.06" size="1.778" layer="95" ratio="15"/>
<attribute name="VALUE" x="210.82" y="97.028" size="1.778" layer="96" ratio="15"/>
</instance>
<instance part="R15" gate="G$1" x="256.54" y="106.68" smashed="yes" rot="R270">
<attribute name="NAME" x="256.286" y="111.76" size="1.778" layer="95" ratio="15" rot="R180"/>
<attribute name="VALUE" x="256.286" y="103.378" size="1.778" layer="96" ratio="15" rot="R180"/>
</instance>
<instance part="C5" gate="G$1" x="96.52" y="60.96" smashed="yes" rot="MR90">
<attribute name="NAME" x="96.266" y="64.262" size="1.778" layer="95" ratio="15" rot="MR0"/>
<attribute name="VALUE" x="96.266" y="55.88" size="1.778" layer="96" ratio="15" rot="MR0"/>
</instance>
<instance part="R14" gate="G$1" x="256.54" y="58.42" smashed="yes" rot="R270">
<attribute name="NAME" x="256.54" y="63.5" size="1.778" layer="95" ratio="15" rot="R180"/>
<attribute name="VALUE" x="256.286" y="55.118" size="1.778" layer="96" ratio="15" rot="R180"/>
</instance>
<instance part="C3" gate="G$1" x="160.02" y="60.96" smashed="yes" rot="MR90">
<attribute name="NAME" x="159.766" y="64.262" size="1.778" layer="95" ratio="15" rot="MR0"/>
<attribute name="VALUE" x="159.766" y="56.134" size="1.778" layer="96" ratio="15" rot="MR0"/>
</instance>
<instance part="C8" gate="G$1" x="248.92" y="60.96" smashed="yes" rot="MR90">
<attribute name="NAME" x="248.666" y="64.262" size="1.778" layer="95" ratio="15" rot="MR0"/>
<attribute name="VALUE" x="248.666" y="56.134" size="1.778" layer="96" ratio="15" rot="MR0"/>
</instance>
<instance part="D100" gate="A" x="274.32" y="124.46" smashed="yes">
<attribute name="NAME" x="266.192" y="124.968" size="1.778" layer="95" ratio="15"/>
<attribute name="VALUE" x="277.622" y="124.714" size="1.778" layer="96" ratio="15"/>
</instance>
<instance part="C6" gate="G$1" x="86.36" y="60.96" smashed="yes" rot="MR90">
<attribute name="NAME" x="86.106" y="64.262" size="1.778" layer="95" ratio="15" rot="MR0"/>
<attribute name="VALUE" x="86.106" y="55.88" size="1.778" layer="96" ratio="15" rot="MR0"/>
</instance>
<instance part="C4" gate="G$1" x="152.4" y="60.96" smashed="yes" rot="MR90">
<attribute name="NAME" x="152.146" y="64.262" size="1.778" layer="95" ratio="15" rot="MR0"/>
<attribute name="VALUE" x="152.146" y="56.134" size="1.778" layer="96" ratio="15" rot="MR0"/>
</instance>
<instance part="C9" gate="G$1" x="238.76" y="60.96" smashed="yes" rot="MR90">
<attribute name="NAME" x="238.506" y="64.262" size="1.778" layer="95" ratio="15" rot="MR0"/>
<attribute name="VALUE" x="238.506" y="56.134" size="1.778" layer="96" ratio="15" rot="MR0"/>
</instance>
<instance part="F102" gate="G$1" x="38.1" y="190.5" smashed="yes">
<attribute name="NAME" x="37.846" y="196.596" size="1.778" layer="95" ratio="15" rot="R180"/>
<attribute name="VALUE" x="37.846" y="186.182" size="1.778" layer="96" ratio="15" rot="R180"/>
</instance>
<instance part="F101" gate="G$1" x="304.8" y="83.82" smashed="yes">
<attribute name="NAME" x="304.546" y="89.662" size="1.778" layer="95" ratio="15" rot="R180"/>
<attribute name="VALUE" x="304.546" y="79.502" size="1.778" layer="96" ratio="15" rot="R180"/>
</instance>
<instance part="F5" gate="G$1" x="330.2" y="88.9" smashed="yes">
<attribute name="NAME" x="329.946" y="94.996" size="1.778" layer="95" ratio="15" rot="R180"/>
<attribute name="VALUE" x="329.946" y="84.582" size="1.778" layer="96" ratio="15" rot="R180"/>
</instance>
<instance part="F2" gate="G$1" x="330.2" y="73.66" smashed="yes" rot="R180">
<attribute name="NAME" x="329.946" y="79.756" size="1.778" layer="95" ratio="15" rot="R180"/>
<attribute name="VALUE" x="329.946" y="69.596" size="1.778" layer="96" ratio="15" rot="R180"/>
</instance>
<instance part="C101" gate="G$1" x="53.34" y="190.5" smashed="yes" rot="R90">
<attribute name="NAME" x="53.086" y="193.802" size="1.778" layer="95" ratio="15" align="bottom-right"/>
<attribute name="VALUE" x="53.086" y="187.198" size="1.778" layer="96" ratio="15" rot="R180"/>
</instance>
<instance part="D1" gate="G$1" x="35.56" y="66.04" smashed="yes" rot="R90">
<attribute name="NAME" x="35.052" y="71.12" size="1.778" layer="95" ratio="15" rot="R180"/>
<attribute name="VALUE" x="35.052" y="62.738" size="1.778" layer="96" ratio="15" rot="R180"/>
</instance>
<instance part="C1" gate="G$1" x="104.14" y="60.96" smashed="yes" rot="MR90">
<attribute name="NAME" x="103.886" y="64.262" size="1.778" layer="95" ratio="15" rot="MR0"/>
<attribute name="VALUE" x="103.886" y="55.88" size="1.778" layer="96" ratio="15" rot="MR0"/>
</instance>
<instance part="R2" gate="G$1" x="165.1" y="96.52" smashed="yes" rot="R90">
<attribute name="NAME" x="164.846" y="101.6" size="1.778" layer="95" ratio="15" rot="R180"/>
<attribute name="VALUE" x="164.846" y="93.218" size="1.778" layer="96" ratio="15" rot="R180"/>
</instance>
<instance part="R3" gate="G$1" x="172.72" y="96.52" smashed="yes" rot="R90">
<attribute name="NAME" x="172.466" y="101.6" size="1.778" layer="95" ratio="15" rot="R180"/>
<attribute name="VALUE" x="172.466" y="93.218" size="1.778" layer="96" ratio="15" rot="R180"/>
</instance>
<instance part="R12" gate="G$1" x="144.78" y="60.96" smashed="yes" rot="R90">
<attribute name="NAME" x="144.526" y="66.04" size="1.778" layer="95" ratio="15" rot="R180"/>
<attribute name="VALUE" x="144.526" y="57.658" size="1.778" layer="96" ratio="15" rot="R180"/>
</instance>
<instance part="TP2" gate="G$1" x="33.02" y="83.82" smashed="yes" rot="R90">
<attribute name="NAME" x="33.02" y="82.55" size="1.778" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="TP4" gate="G$1" x="101.6" y="93.98" smashed="yes">
<attribute name="NAME" x="100.076" y="97.028" size="1.778" layer="95" ratio="15"/>
</instance>
<instance part="DP" gate="G$1" x="236.22" y="84.836" smashed="yes">
<attribute name="NAME" x="235.458" y="86.614" size="1.778" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="DN" gate="G$1" x="236.22" y="79.756" smashed="yes">
<attribute name="NAME" x="235.458" y="81.534" size="1.778" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="TP5" gate="G$1" x="203.2" y="89.916" smashed="yes">
<attribute name="NAME" x="202.438" y="91.694" size="1.778" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="TP1" gate="G$1" x="203.2" y="84.836" smashed="yes">
<attribute name="NAME" x="202.438" y="86.614" size="1.778" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="TP3" gate="G$1" x="203.2" y="74.422" smashed="yes">
<attribute name="NAME" x="202.438" y="76.2" size="1.778" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="F1" gate="G$1" x="15.24" y="20.32" smashed="yes">
<attribute name="NAME" x="15.24" y="27.178" size="1.778" layer="95"/>
</instance>
<instance part="F4" gate="G$1" x="30.48" y="20.32" smashed="yes">
<attribute name="NAME" x="30.48" y="27.178" size="1.778" layer="95"/>
</instance>
<instance part="F104" gate="G$1" x="45.72" y="20.32" smashed="yes">
<attribute name="NAME" x="45.72" y="27.178" size="1.778" layer="95"/>
</instance>
<instance part="F100" gate="G$1" x="60.96" y="20.32" smashed="yes">
<attribute name="NAME" x="60.96" y="27.178" size="1.778" layer="95"/>
</instance>
<instance part="F103" gate="G$1" x="76.2" y="20.32" smashed="yes">
<attribute name="NAME" x="76.2" y="27.178" size="1.778" layer="95"/>
</instance>
<instance part="F3" gate="G$1" x="91.44" y="20.32" smashed="yes">
<attribute name="NAME" x="91.44" y="27.178" size="1.778" layer="95"/>
</instance>
<instance part="C102" gate="G$1" x="78.74" y="190.5" smashed="yes" rot="MR90">
<attribute name="NAME" x="78.74" y="193.802" size="1.778" layer="95" ratio="15" rot="MR0"/>
<attribute name="VALUE" x="78.486" y="185.674" size="1.778" layer="96" ratio="15" rot="MR0"/>
</instance>
<instance part="SUPPLY1" gate="GND" x="205.74" y="165.1" smashed="yes">
<attribute name="VALUE" x="205.994" y="165.608" size="1.778" layer="96" ratio="15"/>
</instance>
<instance part="U$2" gate="G$1" x="0" y="0" smashed="yes">
<attribute name="DRAWING_NAME" x="317.5" y="10.16" size="1.778" layer="94"/>
<attribute name="SHEET" x="332.486" y="5.842" size="1.778" layer="94"/>
</instance>
<instance part="J100" gate="G$1" x="7.62" y="205.74" smashed="yes">
<attribute name="VALUE" x="7.62" y="211.074" size="1.778" layer="96" ratio="15"/>
<attribute name="NAME" x="7.62" y="213.106" size="1.778" layer="95" ratio="15"/>
</instance>
<instance part="U101" gate="A" x="238.76" y="187.96" smashed="yes">
<attribute name="NAME" x="259.08" y="213.36" size="1.778" layer="95" ratio="15" rot="SR180"/>
<attribute name="VALUE" x="259.08" y="210.82" size="1.778" layer="96" ratio="15" rot="SR180"/>
</instance>
<instance part="D101" gate="A" x="309.88" y="190.5" smashed="yes" rot="R90">
<attribute name="NAME" x="309.88" y="195.58" size="1.778" layer="95" ratio="15" rot="R180"/>
<attribute name="VALUE" x="296.926" y="185.42" size="1.778" layer="96" ratio="15"/>
</instance>
<instance part="C109" gate="G$1" x="332.74" y="198.12" smashed="yes" rot="R90">
<attribute name="NAME" x="332.486" y="203.2" size="1.778" layer="95" ratio="10" rot="R180"/>
<attribute name="VALUE" x="332.486" y="194.818" size="1.778" layer="96" ratio="10" rot="R180"/>
</instance>
<instance part="C110" gate="G$1" x="340.36" y="198.12" smashed="yes" rot="R90">
<attribute name="NAME" x="340.106" y="203.2" size="1.778" layer="95" ratio="10" rot="R180"/>
<attribute name="VALUE" x="340.106" y="194.818" size="1.778" layer="96" ratio="10" rot="R180"/>
</instance>
<instance part="C111" gate="G$1" x="347.98" y="198.12" smashed="yes" rot="R90">
<attribute name="NAME" x="347.726" y="203.2" size="1.778" layer="95" ratio="10" rot="R180"/>
<attribute name="VALUE" x="347.726" y="194.818" size="1.778" layer="96" ratio="10" rot="R180"/>
</instance>
<instance part="R111" gate="G$1" x="294.64" y="213.36" smashed="yes" rot="R180">
<attribute name="NAME" x="292.1" y="214.884" size="1.778" layer="95" ratio="15"/>
<attribute name="VALUE" x="293.116" y="210.058" size="1.778" layer="96" ratio="15"/>
</instance>
<instance part="R109" gate="G$1" x="312.42" y="213.36" smashed="yes">
<attribute name="NAME" x="309.88" y="214.884" size="1.778" layer="95" ratio="15"/>
<attribute name="VALUE" x="314.452" y="212.09" size="1.778" layer="96" ratio="15" rot="R180"/>
</instance>
<instance part="SUPPLY6" gate="GND" x="287.02" y="208.28" smashed="yes">
<attribute name="VALUE" x="286.766" y="210.312" size="1.778" layer="96" ratio="15" rot="R180"/>
</instance>
<instance part="SUPPLY7" gate="GND" x="322.58" y="165.1" smashed="yes">
<attribute name="VALUE" x="322.58" y="166.878" size="1.778" layer="96" ratio="15" rot="R180"/>
</instance>
<instance part="C112" gate="G$1" x="231.14" y="182.88" smashed="yes" rot="R90">
<attribute name="NAME" x="230.886" y="187.96" size="1.778" layer="95" ratio="10" rot="R180"/>
<attribute name="VALUE" x="230.886" y="179.578" size="1.778" layer="96" ratio="10" rot="R180"/>
</instance>
<instance part="R115" gate="G$1" x="223.52" y="185.42" smashed="yes" rot="R90">
<attribute name="NAME" x="223.266" y="190.5" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="223.266" y="182.118" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="P+5" gate="1" x="373.38" y="218.44" smashed="yes">
<attribute name="VALUE" x="368.3" y="218.44" size="1.778" layer="96" ratio="15"/>
</instance>
<instance part="SUPPLY8" gate="GND" x="48.26" y="165.1" smashed="yes">
<attribute name="VALUE" x="48.514" y="165.608" size="1.778" layer="96" ratio="15"/>
</instance>
<instance part="FB100" gate="G$1" x="358.14" y="205.74" smashed="yes">
<attribute name="NAME" x="355.6" y="209.55" size="1.778" layer="95" ratio="15"/>
<attribute name="VALUE" x="350.52" y="203.2" size="1.778" layer="96" ratio="15"/>
</instance>
<instance part="D102" gate="G$1" x="71.12" y="190.5" smashed="yes" rot="R90">
<attribute name="NAME" x="71.12" y="195.58" size="1.778" layer="95" ratio="15" rot="R180"/>
<attribute name="VALUE" x="70.866" y="187.198" size="1.778" layer="96" ratio="15" rot="R180"/>
</instance>
<instance part="C105" gate="G$1" x="213.36" y="187.96" smashed="yes" rot="R90">
<attribute name="NAME" x="213.106" y="193.04" size="1.778" layer="95" ratio="10" rot="R180"/>
<attribute name="VALUE" x="213.106" y="184.658" size="1.778" layer="96" ratio="10" rot="R180"/>
</instance>
<instance part="C104" gate="G$1" x="187.96" y="187.96" smashed="yes" rot="R90">
<attribute name="NAME" x="187.706" y="192.786" size="1.778" layer="95" ratio="10" rot="R180"/>
<attribute name="VALUE" x="187.706" y="184.658" size="1.778" layer="96" ratio="10" rot="R180"/>
</instance>
<instance part="L200" gate="G$1" x="322.58" y="205.74" smashed="yes">
<attribute name="NAME" x="320.04" y="208.788" size="1.778" layer="95" ratio="15"/>
<attribute name="VALUE" x="315.214" y="206.756" size="1.778" layer="96" ratio="15"/>
</instance>
<instance part="Y1" gate="G$1" x="81.28" y="86.36" smashed="yes" rot="R90">
<attribute name="NAME" x="81.28" y="89.662" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="81.28" y="84.836" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="C106" gate="G$1" x="287.02" y="203.2" smashed="yes">
<attribute name="NAME" x="280.162" y="203.454" size="1.778" layer="95" ratio="10"/>
<attribute name="VALUE" x="287.782" y="203.454" size="1.778" layer="96" ratio="10"/>
</instance>
<instance part="R114" gate="G$1" x="274.32" y="203.2" smashed="yes">
<attribute name="NAME" x="265.938" y="203.4286" size="1.778" layer="95"/>
<attribute name="VALUE" x="277.114" y="203.454" size="1.778" layer="96"/>
</instance>
<instance part="R112" gate="G$1" x="294.64" y="190.5" smashed="yes" rot="R90">
<attribute name="NAME" x="294.386" y="195.3514" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="294.386" y="186.69" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="C108" gate="G$1" x="294.64" y="177.8" smashed="yes" rot="R90">
<attribute name="NAME" x="294.386" y="182.88" size="1.778" layer="95" ratio="10" rot="R180"/>
<attribute name="VALUE" x="294.386" y="174.752" size="1.778" layer="96" ratio="10" rot="R180"/>
</instance>
<instance part="U100" gate="G$1" x="134.62" y="172.72" smashed="yes">
<attribute name="NAME" x="141.986" y="178.308" size="1.778" layer="95" ratio="15"/>
<attribute name="VALUE" x="121.92" y="182.88" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="R104" gate="G$1" x="114.3" y="205.74" smashed="yes">
<attribute name="NAME" x="116.84" y="209.296" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="116.84" y="203.962" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R102" gate="G$1" x="124.46" y="193.04" smashed="yes" rot="R90">
<attribute name="NAME" x="123.19" y="196.6214" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="123.19" y="191.262" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R103" gate="G$1" x="114.3" y="185.42" smashed="yes" rot="R180">
<attribute name="NAME" x="116.078" y="189.0014" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="116.332" y="183.642" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="Q100" gate="G$1" x="132.08" y="203.2" smashed="yes" rot="R90">
<attribute name="NAME" x="129.286" y="210.82" size="1.778" layer="95" ratio="15"/>
<attribute name="VALUE" x="129.032" y="208.28" size="1.778" layer="96" ratio="15"/>
</instance>
<instance part="R110" gate="G$1" x="119.38" y="162.56" smashed="yes" rot="R90">
<attribute name="NAME" x="118.11" y="166.1414" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="118.11" y="160.782" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R113" gate="G$1" x="119.38" y="147.32" smashed="yes" rot="R90">
<attribute name="NAME" x="118.11" y="150.9014" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="118.11" y="145.542" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R106" gate="G$1" x="99.06" y="193.04" smashed="yes" rot="R90">
<attribute name="NAME" x="97.79" y="196.6214" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="97.79" y="191.262" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R107" gate="G$1" x="99.06" y="162.56" smashed="yes" rot="R90">
<attribute name="NAME" x="97.79" y="166.1414" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="97.79" y="160.782" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R101" gate="G$1" x="134.62" y="193.04" smashed="yes" rot="R90">
<attribute name="NAME" x="133.35" y="196.6214" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="133.35" y="191.262" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R105" gate="G$1" x="149.86" y="193.04" smashed="yes" rot="R90">
<attribute name="NAME" x="148.59" y="196.6214" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="148.59" y="191.262" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R100" gate="G$1" x="152.4" y="162.56" smashed="yes" rot="R90">
<attribute name="NAME" x="151.13" y="166.1414" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="151.13" y="160.782" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="C100" gate="G$1" x="152.4" y="147.32" smashed="yes" rot="R90">
<attribute name="NAME" x="151.13" y="151.13" size="1.778" layer="95" ratio="10" rot="R180"/>
<attribute name="VALUE" x="151.13" y="144.78" size="1.778" layer="96" ratio="10" rot="R180"/>
</instance>
<instance part="C103" gate="G$1" x="200.66" y="187.96" smashed="yes" rot="R90">
<attribute name="NAME" x="200.406" y="192.786" size="1.778" layer="95" ratio="10" rot="R180"/>
<attribute name="VALUE" x="200.406" y="184.658" size="1.778" layer="96" ratio="10" rot="R180"/>
</instance>
<instance part="C107" gate="G$1" x="134.62" y="157.48" smashed="yes" rot="R90">
<attribute name="NAME" x="134.62" y="162.56" size="1.778" layer="95" ratio="10" rot="R180"/>
<attribute name="VALUE" x="134.62" y="152.4" size="1.778" layer="96" ratio="10" rot="R180"/>
</instance>
<instance part="SUPPLY11" gate="GND" x="129.54" y="134.62" smashed="yes">
<attribute name="VALUE" x="129.794" y="135.128" size="1.778" layer="96" ratio="15"/>
</instance>
<instance part="R108" gate="G$1" x="167.64" y="175.26" smashed="yes" rot="R180">
<attribute name="NAME" x="166.37" y="176.7586" size="1.778" layer="95"/>
<attribute name="VALUE" x="166.37" y="171.958" size="1.778" layer="96"/>
</instance>
<instance part="R10" gate="G$1" x="86.36" y="76.2" smashed="yes">
<attribute name="NAME" x="78.74" y="76.454" size="1.778" layer="95" ratio="15"/>
<attribute name="VALUE" x="88.9" y="76.454" size="1.778" layer="96" ratio="15"/>
</instance>
<instance part="L100" gate="G$1" x="170.18" y="205.74" smashed="yes">
<attribute name="NAME" x="167.64" y="208.788" size="1.778" layer="95" ratio="15"/>
<attribute name="VALUE" x="162.814" y="206.756" size="1.778" layer="96" ratio="15"/>
</instance>
<instance part="L1" gate="G$1" x="167.64" y="124.46" smashed="yes">
<attribute name="NAME" x="165.1" y="127.508" size="1.778" layer="95" ratio="15"/>
<attribute name="VALUE" x="160.274" y="125.476" size="1.778" layer="96" ratio="15"/>
</instance>
<instance part="X2" gate="-1" x="345.44" y="124.46" smashed="yes">
<attribute name="VALUE" x="344.17" y="125.73" size="1.778" layer="96"/>
<attribute name="NAME" x="347.98" y="123.698" size="1.524" layer="95"/>
</instance>
<instance part="X2" gate="-2" x="345.44" y="66.04" smashed="yes">
<attribute name="NAME" x="347.98" y="65.278" size="1.524" layer="95"/>
</instance>
<instance part="X2" gate="-3" x="345.44" y="50.8" smashed="yes">
<attribute name="NAME" x="347.98" y="50.038" size="1.524" layer="95"/>
</instance>
<instance part="X2" gate="-4" x="345.44" y="99.06" smashed="yes">
<attribute name="NAME" x="347.98" y="98.298" size="1.524" layer="95"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="N$1" class="0">
<segment>
<wire x1="58.42" y1="81.28" x2="81.28" y2="81.28" width="0.1524" layer="91"/>
<wire x1="81.28" y1="81.28" x2="88.9" y2="81.28" width="0.1524" layer="91"/>
<wire x1="88.9" y1="81.28" x2="88.9" y2="83.82" width="0.1524" layer="91"/>
<pinref part="C10" gate="G$1" pin="2"/>
<pinref part="U1" gate="A" pin="XTAL1"/>
<wire x1="88.9" y1="83.82" x2="106.68" y2="83.82" width="0.1524" layer="91"/>
<wire x1="81.28" y1="83.82" x2="81.28" y2="81.28" width="0.1524" layer="91"/>
<junction x="81.28" y="81.28"/>
<pinref part="Y1" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="C11" gate="G$1" pin="2"/>
<wire x1="88.9" y1="86.36" x2="88.9" y2="91.44" width="0.1524" layer="91"/>
<wire x1="88.9" y1="91.44" x2="81.28" y2="91.44" width="0.1524" layer="91"/>
<pinref part="U1" gate="A" pin="XTAL2"/>
<wire x1="81.28" y1="91.44" x2="58.42" y2="91.44" width="0.1524" layer="91"/>
<wire x1="88.9" y1="86.36" x2="106.68" y2="86.36" width="0.1524" layer="91"/>
<wire x1="81.28" y1="88.9" x2="81.28" y2="91.44" width="0.1524" layer="91"/>
<junction x="81.28" y="91.44"/>
<pinref part="Y1" gate="G$1" pin="2"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<wire x1="335.28" y1="81.28" x2="335.28" y2="50.8" width="0.1524" layer="91"/>
<wire x1="233.68" y1="50.8" x2="238.76" y2="50.8" width="0.1524" layer="91"/>
<junction x="233.68" y="50.8"/>
<pinref part="SUPPLY4" gate="GND" pin="GND"/>
<wire x1="238.76" y1="50.8" x2="248.92" y2="50.8" width="0.1524" layer="91"/>
<wire x1="248.92" y1="50.8" x2="256.54" y2="50.8" width="0.1524" layer="91"/>
<wire x1="256.54" y1="50.8" x2="281.94" y2="50.8" width="0.1524" layer="91"/>
<wire x1="281.94" y1="50.8" x2="304.8" y2="50.8" width="0.1524" layer="91"/>
<wire x1="304.8" y1="50.8" x2="335.28" y2="50.8" width="0.1524" layer="91"/>
<wire x1="231.14" y1="73.66" x2="233.68" y2="73.66" width="0.1524" layer="91"/>
<wire x1="233.68" y1="73.66" x2="233.68" y2="50.8" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="5"/>
<wire x1="281.94" y1="50.8" x2="281.94" y2="83.82" width="0.1524" layer="91"/>
<junction x="281.94" y="50.8"/>
<pinref part="D3" gate="A" pin="-"/>
<pinref part="D2" gate="A" pin="-"/>
<wire x1="276.86" y1="86.36" x2="276.86" y2="83.82" width="0.1524" layer="91"/>
<wire x1="276.86" y1="83.82" x2="276.86" y2="81.28" width="0.1524" layer="91"/>
<wire x1="281.94" y1="83.82" x2="276.86" y2="83.82" width="0.1524" layer="91"/>
<junction x="276.86" y="83.82"/>
<wire x1="304.8" y1="78.74" x2="304.8" y2="50.8" width="0.1524" layer="91"/>
<junction x="304.8" y="50.8"/>
<pinref part="R14" gate="G$1" pin="2"/>
<wire x1="256.54" y1="53.34" x2="256.54" y2="50.8" width="0.1524" layer="91"/>
<junction x="256.54" y="50.8"/>
<pinref part="C8" gate="G$1" pin="1"/>
<junction x="248.92" y="50.8"/>
<wire x1="248.92" y1="55.88" x2="248.92" y2="50.8" width="0.1524" layer="91"/>
<pinref part="F101" gate="G$1" pin="2"/>
<pinref part="F5" gate="G$1" pin="2"/>
<pinref part="F2" gate="G$1" pin="2"/>
<wire x1="330.2" y1="83.82" x2="330.2" y2="81.28" width="0.1524" layer="91"/>
<wire x1="330.2" y1="81.28" x2="330.2" y2="78.74" width="0.1524" layer="91"/>
<wire x1="335.28" y1="81.28" x2="330.2" y2="81.28" width="0.1524" layer="91"/>
<junction x="330.2" y="81.28"/>
<pinref part="C9" gate="G$1" pin="1"/>
<wire x1="238.76" y1="55.88" x2="238.76" y2="50.8" width="0.1524" layer="91"/>
<junction x="238.76" y="50.8"/>
<junction x="335.28" y="50.8"/>
<pinref part="X2" gate="-3" pin="1"/>
<wire x1="342.9" y1="50.8" x2="335.28" y2="50.8" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R6" gate="G$1" pin="2"/>
<wire x1="187.96" y1="78.74" x2="190.5" y2="78.74" width="0.1524" layer="91"/>
<wire x1="190.5" y1="78.74" x2="190.5" y2="76.2" width="0.1524" layer="91"/>
<pinref part="R5" gate="G$1" pin="2"/>
<wire x1="190.5" y1="76.2" x2="187.96" y2="76.2" width="0.1524" layer="91"/>
<wire x1="190.5" y1="76.2" x2="190.5" y2="73.66" width="0.1524" layer="91"/>
<junction x="190.5" y="76.2"/>
<pinref part="SUPPLY2" gate="GND" pin="GND"/>
<pinref part="U1" gate="A" pin="GND_2"/>
<wire x1="190.5" y1="73.66" x2="190.5" y2="50.8" width="0.1524" layer="91"/>
<wire x1="190.5" y1="73.66" x2="142.24" y2="73.66" width="0.1524" layer="91"/>
<junction x="190.5" y="73.66"/>
<junction x="190.5" y="50.8"/>
<wire x1="99.06" y1="50.8" x2="104.14" y2="50.8" width="0.1524" layer="91"/>
<pinref part="U1" gate="A" pin="EPAD"/>
<wire x1="104.14" y1="50.8" x2="144.78" y2="50.8" width="0.1524" layer="91"/>
<wire x1="144.78" y1="50.8" x2="152.4" y2="50.8" width="0.1524" layer="91"/>
<wire x1="152.4" y1="50.8" x2="160.02" y2="50.8" width="0.1524" layer="91"/>
<wire x1="160.02" y1="50.8" x2="190.5" y2="50.8" width="0.1524" layer="91"/>
<wire x1="106.68" y1="91.44" x2="99.06" y2="91.44" width="0.1524" layer="91"/>
<wire x1="99.06" y1="91.44" x2="99.06" y2="88.9" width="0.1524" layer="91"/>
<pinref part="U1" gate="A" pin="GND"/>
<wire x1="99.06" y1="88.9" x2="99.06" y2="50.8" width="0.1524" layer="91"/>
<wire x1="106.68" y1="88.9" x2="99.06" y2="88.9" width="0.1524" layer="91"/>
<junction x="99.06" y="88.9"/>
<pinref part="C2" gate="G$1" pin="1"/>
<wire x1="50.8" y1="60.96" x2="50.8" y2="50.8" width="0.1524" layer="91"/>
<pinref part="C11" gate="G$1" pin="1"/>
<wire x1="48.26" y1="91.44" x2="43.18" y2="91.44" width="0.1524" layer="91"/>
<wire x1="43.18" y1="91.44" x2="43.18" y2="81.28" width="0.1524" layer="91"/>
<pinref part="C10" gate="G$1" pin="1"/>
<wire x1="48.26" y1="81.28" x2="43.18" y2="81.28" width="0.1524" layer="91"/>
<junction x="43.18" y="81.28"/>
<wire x1="43.18" y1="50.8" x2="43.18" y2="81.28" width="0.1524" layer="91"/>
<wire x1="43.18" y1="50.8" x2="50.8" y2="50.8" width="0.1524" layer="91"/>
<wire x1="99.06" y1="50.8" x2="96.52" y2="50.8" width="0.1524" layer="91"/>
<pinref part="R1" gate="G$1" pin="1"/>
<wire x1="96.52" y1="50.8" x2="86.36" y2="50.8" width="0.1524" layer="91"/>
<wire x1="86.36" y1="50.8" x2="78.74" y2="50.8" width="0.1524" layer="91"/>
<wire x1="50.8" y1="50.8" x2="78.74" y2="50.8" width="0.1524" layer="91"/>
<wire x1="78.74" y1="50.8" x2="78.74" y2="58.42" width="0.1524" layer="91"/>
<junction x="50.8" y="50.8"/>
<junction x="78.74" y="50.8"/>
<junction x="99.06" y="50.8"/>
<pinref part="C5" gate="G$1" pin="1"/>
<junction x="96.52" y="50.8"/>
<pinref part="C3" gate="G$1" pin="1"/>
<wire x1="96.52" y1="55.88" x2="96.52" y2="50.8" width="0.1524" layer="91"/>
<junction x="160.02" y="50.8"/>
<wire x1="160.02" y1="55.88" x2="160.02" y2="50.8" width="0.1524" layer="91"/>
<wire x1="35.56" y1="50.8" x2="43.18" y2="50.8" width="0.1524" layer="91"/>
<junction x="43.18" y="50.8"/>
<pinref part="C1" gate="G$1" pin="1"/>
<wire x1="104.14" y1="55.88" x2="104.14" y2="50.8" width="0.1524" layer="91"/>
<junction x="104.14" y="50.8"/>
<pinref part="C6" gate="G$1" pin="1"/>
<wire x1="86.36" y1="55.88" x2="86.36" y2="50.8" width="0.1524" layer="91"/>
<junction x="86.36" y="50.8"/>
<pinref part="C4" gate="G$1" pin="1"/>
<wire x1="152.4" y1="55.88" x2="152.4" y2="50.8" width="0.1524" layer="91"/>
<junction x="152.4" y="50.8"/>
<pinref part="R12" gate="G$1" pin="1"/>
<wire x1="144.78" y1="55.88" x2="144.78" y2="50.8" width="0.1524" layer="91"/>
<junction x="144.78" y="50.8"/>
<pinref part="D1" gate="G$1" pin="+"/>
<wire x1="35.56" y1="60.96" x2="35.56" y2="50.8" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R111" gate="G$1" pin="2"/>
<pinref part="SUPPLY6" gate="GND" pin="GND"/>
<wire x1="289.56" y1="213.36" x2="287.02" y2="213.36" width="0.1524" layer="91"/>
<wire x1="287.02" y1="213.36" x2="287.02" y2="210.82" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U101" gate="A" pin="GND"/>
<wire x1="264.16" y1="193.04" x2="269.24" y2="193.04" width="0.1524" layer="91"/>
<wire x1="269.24" y1="193.04" x2="269.24" y2="190.5" width="0.1524" layer="91"/>
<pinref part="U101" gate="A" pin="PAD"/>
<wire x1="269.24" y1="190.5" x2="264.16" y2="190.5" width="0.1524" layer="91"/>
<pinref part="D101" gate="A" pin="A"/>
<wire x1="309.88" y1="185.42" x2="309.88" y2="170.18" width="0.1524" layer="91"/>
<wire x1="309.88" y1="170.18" x2="294.64" y2="170.18" width="0.1524" layer="91"/>
<wire x1="294.64" y1="170.18" x2="269.24" y2="170.18" width="0.1524" layer="91"/>
<wire x1="269.24" y1="170.18" x2="269.24" y2="190.5" width="0.1524" layer="91"/>
<junction x="269.24" y="190.5"/>
<junction x="309.88" y="170.18"/>
<pinref part="C109" gate="G$1" pin="1"/>
<wire x1="332.74" y1="193.04" x2="332.74" y2="170.18" width="0.1524" layer="91"/>
<wire x1="332.74" y1="170.18" x2="322.58" y2="170.18" width="0.1524" layer="91"/>
<pinref part="C110" gate="G$1" pin="1"/>
<wire x1="322.58" y1="170.18" x2="309.88" y2="170.18" width="0.1524" layer="91"/>
<wire x1="340.36" y1="193.04" x2="340.36" y2="170.18" width="0.1524" layer="91"/>
<wire x1="340.36" y1="170.18" x2="332.74" y2="170.18" width="0.1524" layer="91"/>
<junction x="332.74" y="170.18"/>
<pinref part="C111" gate="G$1" pin="1"/>
<wire x1="347.98" y1="193.04" x2="347.98" y2="170.18" width="0.1524" layer="91"/>
<wire x1="347.98" y1="170.18" x2="340.36" y2="170.18" width="0.1524" layer="91"/>
<junction x="340.36" y="170.18"/>
<pinref part="SUPPLY7" gate="GND" pin="GND"/>
<wire x1="322.58" y1="167.64" x2="322.58" y2="170.18" width="0.1524" layer="91"/>
<junction x="322.58" y="170.18"/>
<pinref part="C108" gate="G$1" pin="1"/>
<wire x1="294.64" y1="172.72" x2="294.64" y2="170.18" width="0.1524" layer="91"/>
<junction x="294.64" y="170.18"/>
</segment>
<segment>
<pinref part="C112" gate="G$1" pin="1"/>
<wire x1="231.14" y1="177.8" x2="231.14" y2="170.18" width="0.1524" layer="91"/>
<pinref part="R115" gate="G$1" pin="1"/>
<wire x1="223.52" y1="180.34" x2="223.52" y2="170.18" width="0.1524" layer="91"/>
<wire x1="223.52" y1="170.18" x2="231.14" y2="170.18" width="0.1524" layer="91"/>
<wire x1="223.52" y1="170.18" x2="213.36" y2="170.18" width="0.1524" layer="91"/>
<junction x="223.52" y="170.18"/>
<pinref part="SUPPLY1" gate="GND" pin="GND"/>
<wire x1="213.36" y1="170.18" x2="205.74" y2="170.18" width="0.1524" layer="91"/>
<wire x1="205.74" y1="170.18" x2="205.74" y2="167.64" width="0.1524" layer="91"/>
<junction x="205.74" y="170.18"/>
<wire x1="187.96" y1="170.18" x2="200.66" y2="170.18" width="0.1524" layer="91"/>
<pinref part="C104" gate="G$1" pin="1"/>
<wire x1="200.66" y1="170.18" x2="205.74" y2="170.18" width="0.1524" layer="91"/>
<wire x1="187.96" y1="182.88" x2="187.96" y2="170.18" width="0.1524" layer="91"/>
<pinref part="C103" gate="G$1" pin="1"/>
<wire x1="200.66" y1="182.88" x2="200.66" y2="170.18" width="0.1524" layer="91"/>
<junction x="200.66" y="170.18"/>
<pinref part="C105" gate="G$1" pin="1"/>
<wire x1="213.36" y1="182.88" x2="213.36" y2="170.18" width="0.1524" layer="91"/>
<junction x="213.36" y="170.18"/>
</segment>
<segment>
<wire x1="53.34" y1="170.18" x2="71.12" y2="170.18" width="0.1524" layer="91"/>
<pinref part="C102" gate="G$1" pin="1"/>
<wire x1="71.12" y1="170.18" x2="78.74" y2="170.18" width="0.1524" layer="91"/>
<wire x1="78.74" y1="185.42" x2="78.74" y2="170.18" width="0.1524" layer="91"/>
<pinref part="C101" gate="G$1" pin="1"/>
<wire x1="53.34" y1="185.42" x2="53.34" y2="170.18" width="0.1524" layer="91"/>
<junction x="53.34" y="170.18"/>
<pinref part="F102" gate="G$1" pin="2"/>
<wire x1="53.34" y1="170.18" x2="48.26" y2="170.18" width="0.1524" layer="91"/>
<wire x1="48.26" y1="170.18" x2="38.1" y2="170.18" width="0.1524" layer="91"/>
<wire x1="38.1" y1="170.18" x2="38.1" y2="185.42" width="0.1524" layer="91"/>
<wire x1="15.24" y1="203.2" x2="15.24" y2="170.18" width="0.1524" layer="91"/>
<wire x1="15.24" y1="170.18" x2="38.1" y2="170.18" width="0.1524" layer="91"/>
<junction x="38.1" y="170.18"/>
<pinref part="J100" gate="G$1" pin="2"/>
<wire x1="15.24" y1="203.2" x2="12.7" y2="203.2" width="0.1524" layer="91"/>
<pinref part="D102" gate="G$1" pin="+"/>
<wire x1="71.12" y1="185.42" x2="71.12" y2="170.18" width="0.1524" layer="91"/>
<junction x="71.12" y="170.18"/>
<pinref part="SUPPLY8" gate="GND" pin="GND"/>
<wire x1="48.26" y1="167.64" x2="48.26" y2="170.18" width="0.1524" layer="91"/>
<junction x="48.26" y="170.18"/>
</segment>
<segment>
<pinref part="U100" gate="G$1" pin="5_GND"/>
<wire x1="139.7" y1="165.1" x2="139.7" y2="144.78" width="0.1524" layer="91"/>
<pinref part="C107" gate="G$1" pin="1"/>
<wire x1="134.62" y1="152.4" x2="134.62" y2="144.78" width="0.1524" layer="91"/>
<wire x1="134.62" y1="144.78" x2="139.7" y2="144.78" width="0.1524" layer="91"/>
<pinref part="R107" gate="G$1" pin="1"/>
<wire x1="99.06" y1="157.48" x2="99.06" y2="139.7" width="0.1524" layer="91"/>
<pinref part="C100" gate="G$1" pin="1"/>
<wire x1="99.06" y1="139.7" x2="119.38" y2="139.7" width="0.1524" layer="91"/>
<wire x1="119.38" y1="139.7" x2="129.54" y2="139.7" width="0.1524" layer="91"/>
<wire x1="129.54" y1="139.7" x2="139.7" y2="139.7" width="0.1524" layer="91"/>
<wire x1="139.7" y1="139.7" x2="152.4" y2="139.7" width="0.1524" layer="91"/>
<wire x1="152.4" y1="139.7" x2="152.4" y2="142.24" width="0.1524" layer="91"/>
<wire x1="139.7" y1="144.78" x2="139.7" y2="139.7" width="0.1524" layer="91"/>
<junction x="139.7" y="144.78"/>
<junction x="139.7" y="139.7"/>
<pinref part="R113" gate="G$1" pin="1"/>
<wire x1="119.38" y1="142.24" x2="119.38" y2="139.7" width="0.1524" layer="91"/>
<junction x="119.38" y="139.7"/>
<pinref part="SUPPLY11" gate="GND" pin="GND"/>
<wire x1="129.54" y1="137.16" x2="129.54" y2="139.7" width="0.1524" layer="91"/>
<junction x="129.54" y="139.7"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="U1" gate="A" pin="TXOUT"/>
<pinref part="R9" gate="G$1" pin="2"/>
<wire x1="106.68" y1="78.74" x2="101.6" y2="78.74" width="0.1524" layer="91"/>
<wire x1="101.6" y1="78.74" x2="91.44" y2="78.74" width="0.1524" layer="91"/>
<wire x1="101.6" y1="93.98" x2="101.6" y2="78.74" width="0.1524" layer="91"/>
<junction x="101.6" y="78.74"/>
<pinref part="TP4" gate="G$1" pin="PP"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="U1" gate="A" pin="BIAS"/>
<pinref part="R4" gate="G$1" pin="2"/>
<wire x1="106.68" y1="73.66" x2="104.14" y2="73.66" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="2"/>
<wire x1="104.14" y1="73.66" x2="91.44" y2="73.66" width="0.1524" layer="91"/>
<wire x1="104.14" y1="66.04" x2="104.14" y2="73.66" width="0.1524" layer="91"/>
<junction x="104.14" y="73.66"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="U1" gate="A" pin="DIRSET2"/>
<wire x1="142.24" y1="78.74" x2="172.72" y2="78.74" width="0.1524" layer="91"/>
<pinref part="R6" gate="G$1" pin="1"/>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="172.72" y1="78.74" x2="177.8" y2="78.74" width="0.1524" layer="91"/>
<wire x1="172.72" y1="91.44" x2="172.72" y2="78.74" width="0.1524" layer="91"/>
<junction x="172.72" y="78.74"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="U1" gate="A" pin="DIRSET1"/>
<wire x1="142.24" y1="76.2" x2="165.1" y2="76.2" width="0.1524" layer="91"/>
<pinref part="R5" gate="G$1" pin="1"/>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="165.1" y1="76.2" x2="177.8" y2="76.2" width="0.1524" layer="91"/>
<wire x1="165.1" y1="91.44" x2="165.1" y2="76.2" width="0.1524" layer="91"/>
<junction x="165.1" y="76.2"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<wire x1="193.04" y1="83.82" x2="193.04" y2="73.66" width="0.1524" layer="91"/>
<wire x1="193.04" y1="73.66" x2="203.2" y2="73.66" width="0.1524" layer="91"/>
<wire x1="203.2" y1="73.66" x2="205.74" y2="73.66" width="0.1524" layer="91"/>
<pinref part="R8" gate="G$1" pin="2"/>
<wire x1="187.96" y1="83.82" x2="193.04" y2="83.82" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="4"/>
<wire x1="203.2" y1="74.422" x2="203.2" y2="73.66" width="0.1524" layer="91"/>
<junction x="203.2" y="73.66"/>
<pinref part="TP3" gate="G$1" pin="PP"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<wire x1="195.58" y1="81.28" x2="195.58" y2="83.82" width="0.1524" layer="91"/>
<pinref part="R7" gate="G$1" pin="2"/>
<wire x1="195.58" y1="83.82" x2="200.66" y2="83.82" width="0.1524" layer="91"/>
<wire x1="187.96" y1="81.28" x2="195.58" y2="81.28" width="0.1524" layer="91"/>
<wire x1="200.66" y1="83.82" x2="200.66" y2="78.74" width="0.1524" layer="91"/>
<wire x1="200.66" y1="78.74" x2="205.74" y2="78.74" width="0.1524" layer="91"/>
<junction x="200.66" y="83.82"/>
<pinref part="U2" gate="G$1" pin="2"/>
<pinref part="U2" gate="G$1" pin="3"/>
<wire x1="200.66" y1="83.82" x2="203.2" y2="83.82" width="0.1524" layer="91"/>
<wire x1="203.2" y1="83.82" x2="205.74" y2="83.82" width="0.1524" layer="91"/>
<wire x1="203.2" y1="84.836" x2="203.2" y2="83.82" width="0.1524" layer="91"/>
<junction x="203.2" y="83.82"/>
<pinref part="TP1" gate="G$1" pin="PP"/>
</segment>
</net>
<net name="DATA_P" class="0">
<segment>
<wire x1="231.14" y1="83.82" x2="236.22" y2="83.82" width="0.1524" layer="91"/>
<wire x1="236.22" y1="83.82" x2="251.46" y2="83.82" width="0.1524" layer="91"/>
<wire x1="261.62" y1="99.06" x2="256.54" y2="99.06" width="0.1524" layer="91"/>
<wire x1="256.54" y1="99.06" x2="251.46" y2="99.06" width="0.1524" layer="91"/>
<wire x1="251.46" y1="99.06" x2="251.46" y2="83.82" width="0.1524" layer="91"/>
<pinref part="R13" gate="G$1" pin="2"/>
<wire x1="261.62" y1="88.9" x2="261.62" y2="99.06" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="7"/>
<wire x1="261.62" y1="99.06" x2="276.86" y2="99.06" width="0.1524" layer="91"/>
<junction x="261.62" y="99.06"/>
<pinref part="D3" gate="A" pin="+"/>
<wire x1="276.86" y1="99.06" x2="330.2" y2="99.06" width="0.1524" layer="91"/>
<wire x1="276.86" y1="96.52" x2="276.86" y2="99.06" width="0.1524" layer="91"/>
<junction x="276.86" y="99.06"/>
<wire x1="236.22" y1="84.836" x2="236.22" y2="83.82" width="0.1524" layer="91"/>
<junction x="236.22" y="83.82"/>
<pinref part="F5" gate="G$1" pin="1"/>
<wire x1="330.2" y1="93.98" x2="330.2" y2="99.06" width="0.1524" layer="91"/>
<pinref part="DP" gate="G$1" pin="PP"/>
<pinref part="R15" gate="G$1" pin="2"/>
<wire x1="256.54" y1="101.6" x2="256.54" y2="99.06" width="0.1524" layer="91"/>
<junction x="256.54" y="99.06"/>
<junction x="330.2" y="99.06"/>
<pinref part="X2" gate="-4" pin="1"/>
<wire x1="342.9" y1="99.06" x2="330.2" y2="99.06" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DATA_N" class="0">
<segment>
<wire x1="261.62" y1="66.04" x2="256.54" y2="66.04" width="0.1524" layer="91"/>
<wire x1="256.54" y1="66.04" x2="251.46" y2="66.04" width="0.1524" layer="91"/>
<pinref part="R13" gate="G$1" pin="1"/>
<wire x1="231.14" y1="78.74" x2="236.22" y2="78.74" width="0.1524" layer="91"/>
<wire x1="236.22" y1="78.74" x2="251.46" y2="78.74" width="0.1524" layer="91"/>
<wire x1="261.62" y1="78.74" x2="261.62" y2="66.04" width="0.1524" layer="91"/>
<wire x1="251.46" y1="66.04" x2="251.46" y2="78.74" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="6"/>
<pinref part="R14" gate="G$1" pin="1"/>
<wire x1="256.54" y1="63.5" x2="256.54" y2="66.04" width="0.1524" layer="91"/>
<junction x="256.54" y="66.04"/>
<wire x1="261.62" y1="66.04" x2="276.86" y2="66.04" width="0.1524" layer="91"/>
<junction x="261.62" y="66.04"/>
<pinref part="D2" gate="A" pin="+"/>
<wire x1="276.86" y1="66.04" x2="330.2" y2="66.04" width="0.1524" layer="91"/>
<wire x1="276.86" y1="71.12" x2="276.86" y2="66.04" width="0.1524" layer="91"/>
<junction x="276.86" y="66.04"/>
<wire x1="236.22" y1="79.756" x2="236.22" y2="78.74" width="0.1524" layer="91"/>
<junction x="236.22" y="78.74"/>
<pinref part="F2" gate="G$1" pin="1"/>
<wire x1="330.2" y1="68.58" x2="330.2" y2="66.04" width="0.1524" layer="91"/>
<pinref part="DN" gate="G$1" pin="PP"/>
<junction x="330.2" y="66.04"/>
<pinref part="X2" gate="-2" pin="1"/>
<wire x1="342.9" y1="66.04" x2="330.2" y2="66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<pinref part="U1" gate="A" pin="VL"/>
<wire x1="142.24" y1="86.36" x2="160.02" y2="86.36" width="0.1524" layer="91"/>
<wire x1="160.02" y1="86.36" x2="160.02" y2="104.14" width="0.1524" layer="91"/>
<pinref part="P+2" gate="1" pin="+5V"/>
<pinref part="C3" gate="G$1" pin="2"/>
<wire x1="160.02" y1="104.14" x2="160.02" y2="106.68" width="0.1524" layer="91"/>
<wire x1="160.02" y1="86.36" x2="160.02" y2="68.58" width="0.1524" layer="91"/>
<junction x="160.02" y="86.36"/>
<pinref part="C4" gate="G$1" pin="2"/>
<wire x1="160.02" y1="68.58" x2="160.02" y2="66.04" width="0.1524" layer="91"/>
<wire x1="152.4" y1="66.04" x2="152.4" y2="68.58" width="0.1524" layer="91"/>
<wire x1="152.4" y1="68.58" x2="160.02" y2="68.58" width="0.1524" layer="91"/>
<junction x="160.02" y="68.58"/>
<pinref part="R3" gate="G$1" pin="2"/>
<wire x1="172.72" y1="101.6" x2="172.72" y2="104.14" width="0.1524" layer="91"/>
<wire x1="172.72" y1="104.14" x2="165.1" y2="104.14" width="0.1524" layer="91"/>
<pinref part="R2" gate="G$1" pin="2"/>
<wire x1="165.1" y1="104.14" x2="165.1" y2="101.6" width="0.1524" layer="91"/>
<wire x1="160.02" y1="104.14" x2="165.1" y2="104.14" width="0.1524" layer="91"/>
<junction x="160.02" y="104.14"/>
<junction x="165.1" y="104.14"/>
</segment>
<segment>
<wire x1="231.14" y1="88.9" x2="248.92" y2="88.9" width="0.1524" layer="91"/>
<wire x1="248.92" y1="88.9" x2="248.92" y2="114.3" width="0.1524" layer="91"/>
<pinref part="P+3" gate="1" pin="+5V"/>
<pinref part="U2" gate="G$1" pin="8"/>
<pinref part="R15" gate="G$1" pin="1"/>
<wire x1="248.92" y1="114.3" x2="248.92" y2="116.84" width="0.1524" layer="91"/>
<wire x1="256.54" y1="111.76" x2="256.54" y2="114.3" width="0.1524" layer="91"/>
<wire x1="256.54" y1="114.3" x2="248.92" y2="114.3" width="0.1524" layer="91"/>
<junction x="248.92" y="114.3"/>
<pinref part="C8" gate="G$1" pin="2"/>
<wire x1="248.92" y1="88.9" x2="248.92" y2="68.58" width="0.1524" layer="91"/>
<junction x="248.92" y="88.9"/>
<pinref part="C9" gate="G$1" pin="2"/>
<wire x1="248.92" y1="68.58" x2="248.92" y2="66.04" width="0.1524" layer="91"/>
<wire x1="238.76" y1="66.04" x2="238.76" y2="68.58" width="0.1524" layer="91"/>
<wire x1="238.76" y1="68.58" x2="248.92" y2="68.58" width="0.1524" layer="91"/>
<junction x="248.92" y="68.58"/>
</segment>
<segment>
<pinref part="U1" gate="A" pin="VCC"/>
<wire x1="106.68" y1="81.28" x2="96.52" y2="81.28" width="0.1524" layer="91"/>
<wire x1="96.52" y1="81.28" x2="96.52" y2="101.6" width="0.1524" layer="91"/>
<pinref part="P+1" gate="1" pin="+5V"/>
<pinref part="C5" gate="G$1" pin="2"/>
<wire x1="96.52" y1="81.28" x2="96.52" y2="68.58" width="0.1524" layer="91"/>
<junction x="96.52" y="81.28"/>
<pinref part="C6" gate="G$1" pin="2"/>
<wire x1="96.52" y1="68.58" x2="96.52" y2="66.04" width="0.1524" layer="91"/>
<wire x1="86.36" y1="66.04" x2="86.36" y2="68.58" width="0.1524" layer="91"/>
<wire x1="86.36" y1="68.58" x2="96.52" y2="68.58" width="0.1524" layer="91"/>
<junction x="96.52" y="68.58"/>
</segment>
<segment>
<wire x1="363.22" y1="205.74" x2="365.76" y2="205.74" width="0.1524" layer="91"/>
<pinref part="P+5" gate="1" pin="+5V"/>
<wire x1="365.76" y1="205.74" x2="373.38" y2="205.74" width="0.1524" layer="91"/>
<wire x1="373.38" y1="205.74" x2="373.38" y2="215.9" width="0.1524" layer="91"/>
<pinref part="FB100" gate="G$1" pin="2"/>
<pinref part="R109" gate="G$1" pin="2"/>
<wire x1="317.5" y1="213.36" x2="365.76" y2="213.36" width="0.1524" layer="91"/>
<wire x1="365.76" y1="213.36" x2="365.76" y2="205.74" width="0.1524" layer="91"/>
<junction x="365.76" y="205.74"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="U1" gate="A" pin="DIR"/>
<pinref part="R7" gate="G$1" pin="1"/>
<wire x1="142.24" y1="81.28" x2="177.8" y2="81.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="U1" gate="A" pin="RXOUT"/>
<pinref part="R8" gate="G$1" pin="1"/>
<wire x1="142.24" y1="83.82" x2="177.8" y2="83.82" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="U1" gate="A" pin="TXIN"/>
<pinref part="R11" gate="G$1" pin="1"/>
<wire x1="142.24" y1="88.9" x2="177.8" y2="88.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<pinref part="R11" gate="G$1" pin="2"/>
<wire x1="187.96" y1="88.9" x2="203.2" y2="88.9" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="1"/>
<wire x1="203.2" y1="88.9" x2="205.74" y2="88.9" width="0.1524" layer="91"/>
<wire x1="203.2" y1="89.916" x2="203.2" y2="88.9" width="0.1524" layer="91"/>
<junction x="203.2" y="88.9"/>
<pinref part="TP5" gate="G$1" pin="PP"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<wire x1="78.74" y1="71.12" x2="106.68" y2="71.12" width="0.1524" layer="91"/>
<pinref part="R1" gate="G$1" pin="2"/>
<wire x1="78.74" y1="68.58" x2="78.74" y2="71.12" width="0.1524" layer="91"/>
<pinref part="R4" gate="G$1" pin="1"/>
<wire x1="81.28" y1="73.66" x2="78.74" y2="73.66" width="0.1524" layer="91"/>
<wire x1="78.74" y1="73.66" x2="78.74" y2="71.12" width="0.1524" layer="91"/>
<junction x="78.74" y="71.12"/>
<pinref part="U1" gate="A" pin="RES"/>
</segment>
</net>
<net name="N$25" class="1">
<segment>
<pinref part="R9" gate="G$1" pin="1"/>
<wire x1="81.28" y1="78.74" x2="78.74" y2="78.74" width="0.1524" layer="91"/>
<pinref part="C2" gate="G$1" pin="2"/>
<wire x1="78.74" y1="78.74" x2="50.8" y2="78.74" width="0.1524" layer="91"/>
<wire x1="50.8" y1="71.12" x2="50.8" y2="78.74" width="0.1524" layer="91"/>
<junction x="50.8" y="78.74"/>
<wire x1="50.8" y1="78.74" x2="35.56" y2="78.74" width="0.1524" layer="91"/>
<wire x1="78.74" y1="78.74" x2="78.74" y2="76.2" width="0.1524" layer="91"/>
<junction x="78.74" y="78.74"/>
<wire x1="35.56" y1="83.82" x2="35.56" y2="78.74" width="0.1524" layer="91"/>
<wire x1="33.02" y1="83.82" x2="35.56" y2="83.82" width="0.1524" layer="91"/>
<pinref part="TP2" gate="G$1" pin="PP"/>
<pinref part="D1" gate="G$1" pin="-"/>
<wire x1="35.56" y1="71.12" x2="35.56" y2="78.74" width="0.1524" layer="91"/>
<junction x="35.56" y="78.74"/>
<pinref part="R10" gate="G$1" pin="1"/>
<wire x1="81.28" y1="76.2" x2="78.74" y2="76.2" width="0.1524" layer="91"/>
<pinref part="C7" gate="G$1" pin="1"/>
<wire x1="35.56" y1="99.06" x2="35.56" y2="83.82" width="0.1524" layer="91"/>
<junction x="35.56" y="83.82"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<wire x1="304.8" y1="88.9" x2="304.8" y2="124.46" width="0.1524" layer="91"/>
<pinref part="F101" gate="G$1" pin="1"/>
<wire x1="279.4" y1="124.46" x2="304.8" y2="124.46" width="0.1524" layer="91"/>
<pinref part="D100" gate="A" pin="-"/>
<junction x="304.8" y="124.46"/>
<pinref part="X2" gate="-1" pin="1"/>
<wire x1="342.9" y1="124.46" x2="304.8" y2="124.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$30" class="0">
<segment>
<pinref part="R12" gate="G$1" pin="2"/>
<wire x1="144.78" y1="66.04" x2="144.78" y2="91.44" width="0.1524" layer="91"/>
<pinref part="U1" gate="A" pin="SYNCOUT"/>
<wire x1="144.78" y1="91.44" x2="142.24" y2="91.44" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="U101" gate="A" pin="BOOT"/>
<pinref part="R114" gate="G$1" pin="1"/>
<wire x1="264.16" y1="203.2" x2="269.24" y2="203.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<pinref part="U101" gate="A" pin="FB"/>
<wire x1="264.16" y1="198.12" x2="302.26" y2="198.12" width="0.1524" layer="91"/>
<wire x1="302.26" y1="198.12" x2="302.26" y2="213.36" width="0.1524" layer="91"/>
<pinref part="R111" gate="G$1" pin="1"/>
<pinref part="R109" gate="G$1" pin="1"/>
<wire x1="299.72" y1="213.36" x2="302.26" y2="213.36" width="0.1524" layer="91"/>
<wire x1="302.26" y1="213.36" x2="307.34" y2="213.36" width="0.1524" layer="91"/>
<junction x="302.26" y="213.36"/>
</segment>
</net>
<net name="N$43" class="0">
<segment>
<pinref part="U101" gate="A" pin="SS"/>
<pinref part="C112" gate="G$1" pin="2"/>
<wire x1="233.68" y1="190.5" x2="231.14" y2="190.5" width="0.1524" layer="91"/>
<wire x1="231.14" y1="190.5" x2="231.14" y2="187.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$46" class="0">
<segment>
<pinref part="U101" gate="A" pin="RT/SYNC"/>
<pinref part="R115" gate="G$1" pin="2"/>
<wire x1="233.68" y1="195.58" x2="223.52" y2="195.58" width="0.1524" layer="91"/>
<wire x1="223.52" y1="195.58" x2="223.52" y2="190.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$28" class="0">
<segment>
<pinref part="U101" gate="A" pin="VIN"/>
<wire x1="187.96" y1="205.74" x2="200.66" y2="205.74" width="0.1524" layer="91"/>
<wire x1="200.66" y1="205.74" x2="213.36" y2="205.74" width="0.1524" layer="91"/>
<wire x1="213.36" y1="205.74" x2="233.68" y2="205.74" width="0.1524" layer="91"/>
<pinref part="C105" gate="G$1" pin="2"/>
<wire x1="213.36" y1="193.04" x2="213.36" y2="205.74" width="0.1524" layer="91"/>
<junction x="213.36" y="205.74"/>
<pinref part="C103" gate="G$1" pin="2"/>
<wire x1="200.66" y1="193.04" x2="200.66" y2="205.74" width="0.1524" layer="91"/>
<junction x="200.66" y="205.74"/>
<pinref part="C104" gate="G$1" pin="2"/>
<wire x1="187.96" y1="193.04" x2="187.96" y2="205.74" width="0.1524" layer="91"/>
<pinref part="L100" gate="G$1" pin="2"/>
<wire x1="175.26" y1="205.74" x2="187.96" y2="205.74" width="0.1524" layer="91"/>
<junction x="187.96" y="205.74"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="FB100" gate="G$1" pin="1"/>
<pinref part="C111" gate="G$1" pin="2"/>
<wire x1="347.98" y1="203.2" x2="347.98" y2="205.74" width="0.1524" layer="91"/>
<junction x="347.98" y="205.74"/>
<wire x1="347.98" y1="205.74" x2="353.06" y2="205.74" width="0.1524" layer="91"/>
<wire x1="340.36" y1="205.74" x2="347.98" y2="205.74" width="0.1524" layer="91"/>
<pinref part="C110" gate="G$1" pin="2"/>
<wire x1="340.36" y1="203.2" x2="340.36" y2="205.74" width="0.1524" layer="91"/>
<junction x="340.36" y="205.74"/>
<wire x1="332.74" y1="205.74" x2="340.36" y2="205.74" width="0.1524" layer="91"/>
<pinref part="C109" gate="G$1" pin="2"/>
<wire x1="332.74" y1="203.2" x2="332.74" y2="205.74" width="0.1524" layer="91"/>
<pinref part="L200" gate="G$1" pin="2"/>
<wire x1="327.66" y1="205.74" x2="332.74" y2="205.74" width="0.1524" layer="91"/>
<junction x="332.74" y="205.74"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<pinref part="R114" gate="G$1" pin="2"/>
<pinref part="C106" gate="G$1" pin="1"/>
<wire x1="279.4" y1="203.2" x2="281.94" y2="203.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<pinref part="C106" gate="G$1" pin="2"/>
<pinref part="R112" gate="G$1" pin="2"/>
<wire x1="292.1" y1="203.2" x2="294.64" y2="203.2" width="0.1524" layer="91"/>
<wire x1="294.64" y1="203.2" x2="294.64" y2="195.58" width="0.1524" layer="91"/>
<pinref part="U101" gate="A" pin="SW"/>
<wire x1="264.16" y1="205.74" x2="294.64" y2="205.74" width="0.1524" layer="91"/>
<wire x1="294.64" y1="205.74" x2="309.88" y2="205.74" width="0.1524" layer="91"/>
<pinref part="D101" gate="A" pin="C"/>
<wire x1="309.88" y1="205.74" x2="317.5" y2="205.74" width="0.1524" layer="91"/>
<wire x1="309.88" y1="195.58" x2="309.88" y2="205.74" width="0.1524" layer="91"/>
<junction x="309.88" y="205.74"/>
<pinref part="L200" gate="G$1" pin="1"/>
<wire x1="294.64" y1="203.2" x2="294.64" y2="205.74" width="0.1524" layer="91"/>
<junction x="294.64" y="203.2"/>
<junction x="294.64" y="205.74"/>
</segment>
</net>
<net name="N$26" class="0">
<segment>
<pinref part="R112" gate="G$1" pin="1"/>
<pinref part="C108" gate="G$1" pin="2"/>
<wire x1="294.64" y1="185.42" x2="294.64" y2="182.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="R102" gate="G$1" pin="2"/>
<wire x1="124.46" y1="198.12" x2="124.46" y2="205.74" width="0.1524" layer="91"/>
<pinref part="R104" gate="G$1" pin="2"/>
<wire x1="124.46" y1="205.74" x2="119.38" y2="205.74" width="0.1524" layer="91"/>
<pinref part="Q100" gate="G$1" pin="D"/>
<wire x1="127" y1="205.74" x2="124.46" y2="205.74" width="0.1524" layer="91"/>
<junction x="124.46" y="205.74"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<pinref part="R102" gate="G$1" pin="1"/>
<wire x1="124.46" y1="187.96" x2="124.46" y2="185.42" width="0.1524" layer="91"/>
<pinref part="R103" gate="G$1" pin="1"/>
<wire x1="124.46" y1="185.42" x2="119.38" y2="185.42" width="0.1524" layer="91"/>
<pinref part="U100" gate="G$1" pin="9_SENSE"/>
<wire x1="129.54" y1="180.34" x2="129.54" y2="182.88" width="0.1524" layer="91"/>
<junction x="124.46" y="185.42"/>
<wire x1="129.54" y1="182.88" x2="124.46" y2="182.88" width="0.1524" layer="91"/>
<wire x1="124.46" y1="182.88" x2="124.46" y2="185.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$33" class="0">
<segment>
<pinref part="R103" gate="G$1" pin="2"/>
<wire x1="109.22" y1="185.42" x2="104.14" y2="185.42" width="0.1524" layer="91"/>
<wire x1="104.14" y1="185.42" x2="104.14" y2="205.74" width="0.1524" layer="91"/>
<pinref part="R104" gate="G$1" pin="1"/>
<wire x1="104.14" y1="205.74" x2="109.22" y2="205.74" width="0.1524" layer="91"/>
<pinref part="U100" gate="G$1" pin="10_VCC"/>
<wire x1="121.92" y1="175.26" x2="104.14" y2="175.26" width="0.1524" layer="91"/>
<wire x1="104.14" y1="175.26" x2="104.14" y2="185.42" width="0.1524" layer="91"/>
<junction x="104.14" y="185.42"/>
<pinref part="C102" gate="G$1" pin="2"/>
<wire x1="162.56" y1="124.46" x2="88.9" y2="124.46" width="0.1524" layer="91"/>
<wire x1="78.74" y1="195.58" x2="78.74" y2="205.74" width="0.1524" layer="91"/>
<pinref part="D102" gate="G$1" pin="-"/>
<wire x1="71.12" y1="195.58" x2="71.12" y2="205.74" width="0.1524" layer="91"/>
<pinref part="C101" gate="G$1" pin="2"/>
<wire x1="53.34" y1="195.58" x2="53.34" y2="205.74" width="0.1524" layer="91"/>
<junction x="53.34" y="205.74"/>
<pinref part="J100" gate="G$1" pin="1"/>
<pinref part="F102" gate="G$1" pin="1"/>
<wire x1="38.1" y1="195.58" x2="38.1" y2="205.74" width="0.1524" layer="91"/>
<junction x="38.1" y="205.74"/>
<wire x1="38.1" y1="205.74" x2="12.7" y2="205.74" width="0.1524" layer="91"/>
<wire x1="38.1" y1="205.74" x2="53.34" y2="205.74" width="0.1524" layer="91"/>
<wire x1="53.34" y1="205.74" x2="71.12" y2="205.74" width="0.1524" layer="91"/>
<junction x="71.12" y="205.74"/>
<wire x1="71.12" y1="205.74" x2="78.74" y2="205.74" width="0.1524" layer="91"/>
<junction x="78.74" y="205.74"/>
<wire x1="78.74" y1="205.74" x2="88.9" y2="205.74" width="0.1524" layer="91"/>
<wire x1="88.9" y1="124.46" x2="88.9" y2="205.74" width="0.1524" layer="91"/>
<wire x1="104.14" y1="205.74" x2="99.06" y2="205.74" width="0.1524" layer="91"/>
<junction x="104.14" y="205.74"/>
<junction x="88.9" y="205.74"/>
<pinref part="R106" gate="G$1" pin="2"/>
<wire x1="99.06" y1="205.74" x2="88.9" y2="205.74" width="0.1524" layer="91"/>
<wire x1="99.06" y1="198.12" x2="99.06" y2="205.74" width="0.1524" layer="91"/>
<junction x="99.06" y="205.74"/>
<pinref part="L1" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$37" class="0">
<segment>
<pinref part="U100" gate="G$1" pin="2_VREF"/>
<pinref part="R110" gate="G$1" pin="2"/>
<wire x1="121.92" y1="170.18" x2="119.38" y2="170.18" width="0.1524" layer="91"/>
<wire x1="119.38" y1="170.18" x2="119.38" y2="167.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$38" class="0">
<segment>
<pinref part="R110" gate="G$1" pin="1"/>
<pinref part="R113" gate="G$1" pin="2"/>
<wire x1="119.38" y1="157.48" x2="119.38" y2="154.94" width="0.1524" layer="91"/>
<pinref part="U100" gate="G$1" pin="3_PROG"/>
<wire x1="119.38" y1="154.94" x2="119.38" y2="152.4" width="0.1524" layer="91"/>
<wire x1="129.54" y1="165.1" x2="129.54" y2="154.94" width="0.1524" layer="91"/>
<wire x1="129.54" y1="154.94" x2="119.38" y2="154.94" width="0.1524" layer="91"/>
<junction x="119.38" y="154.94"/>
</segment>
</net>
<net name="N$40" class="0">
<segment>
<pinref part="U100" gate="G$1" pin="1_EN"/>
<wire x1="121.92" y1="172.72" x2="99.06" y2="172.72" width="0.1524" layer="91"/>
<pinref part="R107" gate="G$1" pin="2"/>
<wire x1="99.06" y1="167.64" x2="99.06" y2="172.72" width="0.1524" layer="91"/>
<pinref part="R106" gate="G$1" pin="1"/>
<wire x1="99.06" y1="187.96" x2="99.06" y2="172.72" width="0.1524" layer="91"/>
<junction x="99.06" y="172.72"/>
</segment>
</net>
<net name="N$39" class="0">
<segment>
<pinref part="R101" gate="G$1" pin="2"/>
<pinref part="Q100" gate="G$1" pin="G"/>
<wire x1="134.62" y1="198.12" x2="134.62" y2="200.66" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$42" class="0">
<segment>
<pinref part="U100" gate="G$1" pin="6_PG"/>
<pinref part="R105" gate="G$1" pin="1"/>
<wire x1="147.32" y1="175.26" x2="149.86" y2="175.26" width="0.1524" layer="91"/>
<wire x1="149.86" y1="175.26" x2="149.86" y2="187.96" width="0.1524" layer="91"/>
<wire x1="149.86" y1="175.26" x2="162.56" y2="175.26" width="0.1524" layer="91"/>
<junction x="149.86" y="175.26"/>
<pinref part="R108" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$47" class="0">
<segment>
<pinref part="R100" gate="G$1" pin="1"/>
<pinref part="C100" gate="G$1" pin="2"/>
<wire x1="152.4" y1="157.48" x2="152.4" y2="152.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$44" class="0">
<segment>
<pinref part="U100" gate="G$1" pin="4_TIMER"/>
<pinref part="C107" gate="G$1" pin="2"/>
<wire x1="134.62" y1="165.1" x2="134.62" y2="162.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$45" class="0">
<segment>
<pinref part="R108" gate="G$1" pin="1"/>
<wire x1="172.72" y1="175.26" x2="175.26" y2="175.26" width="0.1524" layer="91"/>
<wire x1="175.26" y1="175.26" x2="175.26" y2="200.66" width="0.1524" layer="91"/>
<pinref part="U101" gate="A" pin="EN"/>
<wire x1="175.26" y1="200.66" x2="233.68" y2="200.66" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$31" class="0">
<segment>
<pinref part="R100" gate="G$1" pin="2"/>
<wire x1="152.4" y1="167.64" x2="152.4" y2="182.88" width="0.1524" layer="91"/>
<pinref part="R101" gate="G$1" pin="1"/>
<pinref part="U100" gate="G$1" pin="8_GATE"/>
<wire x1="134.62" y1="187.96" x2="134.62" y2="182.88" width="0.1524" layer="91"/>
<wire x1="134.62" y1="182.88" x2="134.62" y2="180.34" width="0.1524" layer="91"/>
<wire x1="152.4" y1="182.88" x2="134.62" y2="182.88" width="0.1524" layer="91"/>
<junction x="134.62" y="182.88"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="R10" gate="G$1" pin="2"/>
<pinref part="U1" gate="A" pin="RXIN"/>
<wire x1="91.44" y1="76.2" x2="106.68" y2="76.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$29" class="1">
<segment>
<pinref part="C7" gate="G$1" pin="2"/>
<wire x1="35.56" y1="109.22" x2="35.56" y2="119.38" width="0.1524" layer="91"/>
<pinref part="R105" gate="G$1" pin="2"/>
<wire x1="149.86" y1="198.12" x2="149.86" y2="205.74" width="0.1524" layer="91"/>
<pinref part="Q100" gate="G$1" pin="S"/>
<wire x1="137.16" y1="205.74" x2="139.7" y2="205.74" width="0.1524" layer="91"/>
<wire x1="139.7" y1="205.74" x2="149.86" y2="205.74" width="0.1524" layer="91"/>
<junction x="139.7" y="205.74"/>
<pinref part="U100" gate="G$1" pin="7_OUT"/>
<wire x1="139.7" y1="180.34" x2="139.7" y2="205.74" width="0.1524" layer="91"/>
<pinref part="L100" gate="G$1" pin="1"/>
<wire x1="165.1" y1="205.74" x2="157.48" y2="205.74" width="0.1524" layer="91"/>
<junction x="149.86" y="205.74"/>
<wire x1="157.48" y1="205.74" x2="149.86" y2="205.74" width="0.1524" layer="91"/>
<wire x1="35.56" y1="119.38" x2="157.48" y2="119.38" width="0.1524" layer="91"/>
<wire x1="157.48" y1="119.38" x2="157.48" y2="205.74" width="0.1524" layer="91"/>
<junction x="157.48" y="205.74"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<pinref part="D100" gate="A" pin="+"/>
<wire x1="269.24" y1="124.46" x2="172.72" y2="124.46" width="0.1524" layer="91"/>
<pinref part="L1" gate="G$1" pin="2"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
<text x="7.62" y="208.788" size="1.778" layer="95" ratio="15">Modulated RF Input</text>
<text x="312.42" y="17.78" size="1.778" layer="94">AISG 3.0 Modem</text>
<text x="370.84" y="10.16" size="1.778" layer="94">01</text>
<text x="281.94" y="228.6" size="1.778" layer="94">01</text>
<text x="289.56" y="228.6" size="1.778" layer="94">12/28/17</text>
<text x="317.5" y="228.6" size="1.778" layer="94">AISG 3.0 Modem Initial Release</text>
<text x="353.06" y="124.46" size="1.778" layer="95">10V - 30V</text>
<text x="353.06" y="99.06" size="1.778" layer="95">DATA_P</text>
<text x="353.06" y="66.04" size="1.778" layer="95">DATA_N</text>
<text x="353.06" y="50.8" size="1.778" layer="95">GND</text>
</plain>
<instances>
<instance part="U7" gate="A" x="137.16" y="68.58" smashed="yes" rot="MR0">
<attribute name="NAME" x="137.16" y="96.52" size="1.778" layer="95" ratio="15" rot="SMR0"/>
<attribute name="VALUE" x="137.16" y="94.234" size="1.778" layer="96" ratio="15" rot="SMR0"/>
</instance>
<instance part="C36" gate="G$1" x="53.34" y="91.44" smashed="yes" rot="MR180">
<attribute name="NAME" x="52.07" y="91.948" size="1.778" layer="95" ratio="15" rot="MR0"/>
<attribute name="VALUE" x="54.61" y="90.932" size="1.778" layer="96" ratio="15" rot="MR180"/>
</instance>
<instance part="C37" gate="G$1" x="53.34" y="81.28" smashed="yes" rot="MR180">
<attribute name="NAME" x="52.07" y="81.788" size="1.778" layer="95" ratio="15" rot="MR0"/>
<attribute name="VALUE" x="54.61" y="80.772" size="1.778" layer="96" ratio="15" rot="MR180"/>
</instance>
<instance part="R47" gate="G$1" x="86.36" y="78.74" smashed="yes">
<attribute name="NAME" x="81.28" y="78.994" size="1.778" layer="95" ratio="15"/>
<attribute name="VALUE" x="88.9" y="78.994" size="1.778" layer="96" ratio="15"/>
</instance>
<instance part="C38" gate="G$1" x="50.8" y="66.04" smashed="yes" rot="MR90">
<attribute name="NAME" x="50.546" y="69.342" size="1.778" layer="95" ratio="15" rot="MR0"/>
<attribute name="VALUE" x="50.546" y="61.468" size="1.778" layer="96" ratio="15" rot="MR0"/>
</instance>
<instance part="C39" gate="G$1" x="35.56" y="104.14" smashed="yes" rot="R90">
<attribute name="NAME" x="35.306" y="109.22" size="1.778" layer="95" ratio="15" rot="R180"/>
<attribute name="VALUE" x="35.306" y="100.838" size="1.778" layer="96" ratio="15" rot="R180"/>
</instance>
<instance part="R48" gate="G$1" x="86.36" y="73.66" smashed="yes">
<attribute name="NAME" x="80.264" y="73.914" size="1.778" layer="95" ratio="15"/>
<attribute name="VALUE" x="88.9" y="73.914" size="1.778" layer="96" ratio="15"/>
</instance>
<instance part="R49" gate="G$1" x="78.74" y="63.5" smashed="yes" rot="R90">
<attribute name="NAME" x="78.486" y="68.58" size="1.778" layer="95" ratio="15" rot="R180"/>
<attribute name="VALUE" x="78.486" y="60.198" size="1.778" layer="96" ratio="15" rot="R180"/>
</instance>
<instance part="SUPPLY15" gate="GND" x="190.5" y="48.26" smashed="yes">
<attribute name="VALUE" x="190.246" y="50.292" size="1.778" layer="96" ratio="15" rot="R180"/>
</instance>
<instance part="R50" gate="G$1" x="182.88" y="78.74" smashed="yes">
<attribute name="NAME" x="176.022" y="78.994" size="1.778" layer="95" ratio="15"/>
<attribute name="VALUE" x="187.198" y="78.994" size="1.778" layer="96" ratio="15"/>
</instance>
<instance part="R51" gate="G$1" x="182.88" y="76.2" smashed="yes">
<attribute name="NAME" x="176.022" y="76.454" size="1.778" layer="95" ratio="15"/>
<attribute name="VALUE" x="187.198" y="76.454" size="1.778" layer="96" ratio="15"/>
</instance>
<instance part="R52" gate="G$1" x="261.62" y="83.82" smashed="yes" rot="R90">
<attribute name="NAME" x="261.366" y="88.9" size="1.778" layer="95" ratio="15" rot="R180"/>
<attribute name="VALUE" x="261.366" y="80.518" size="1.778" layer="96" ratio="15" rot="R180"/>
</instance>
<instance part="D10" gate="A" x="276.86" y="76.2" smashed="yes" rot="R90">
<attribute name="NAME" x="276.606" y="81.28" size="1.778" layer="95" ratio="15" rot="R180"/>
<attribute name="VALUE" x="276.606" y="72.898" size="1.778" layer="96" ratio="15" rot="R180"/>
</instance>
<instance part="D11" gate="A" x="276.86" y="91.44" smashed="yes" rot="R270">
<attribute name="NAME" x="276.606" y="96.52" size="1.778" layer="95" ratio="15" rot="R180"/>
<attribute name="VALUE" x="276.606" y="88.138" size="1.778" layer="96" ratio="15" rot="R180"/>
</instance>
<instance part="P+9" gate="1" x="96.52" y="104.14" smashed="yes">
<attribute name="VALUE" x="99.568" y="105.918" size="1.778" layer="96" ratio="15" rot="R180"/>
</instance>
<instance part="P+10" gate="1" x="160.02" y="109.22" smashed="yes">
<attribute name="VALUE" x="160.02" y="109.22" size="1.778" layer="96" ratio="15"/>
</instance>
<instance part="P+11" gate="1" x="248.92" y="119.38" smashed="yes">
<attribute name="VALUE" x="248.92" y="119.38" size="1.778" layer="96" ratio="15"/>
</instance>
<instance part="R53" gate="G$1" x="182.88" y="81.28" smashed="yes">
<attribute name="NAME" x="176.022" y="81.534" size="1.778" layer="95" ratio="15"/>
<attribute name="VALUE" x="187.198" y="81.534" size="1.778" layer="96" ratio="15"/>
</instance>
<instance part="R54" gate="G$1" x="182.88" y="83.82" smashed="yes">
<attribute name="NAME" x="176.022" y="84.074" size="1.778" layer="95" ratio="15"/>
<attribute name="VALUE" x="187.198" y="84.074" size="1.778" layer="96" ratio="15"/>
</instance>
<instance part="R55" gate="G$1" x="182.88" y="88.9" smashed="yes">
<attribute name="NAME" x="176.276" y="89.154" size="1.778" layer="95" ratio="15"/>
<attribute name="VALUE" x="187.198" y="89.154" size="1.778" layer="96" ratio="15"/>
</instance>
<instance part="SUPPLY16" gate="GND" x="233.68" y="48.26" smashed="yes">
<attribute name="VALUE" x="233.426" y="50.292" size="1.778" layer="96" ratio="15" rot="R180"/>
</instance>
<instance part="U8" gate="G$1" x="208.28" y="96.52" smashed="yes">
<attribute name="NAME" x="210.82" y="99.06" size="1.778" layer="95" ratio="15"/>
<attribute name="VALUE" x="210.82" y="97.028" size="1.778" layer="96" ratio="15"/>
</instance>
<instance part="R56" gate="G$1" x="256.54" y="106.68" smashed="yes" rot="R270">
<attribute name="NAME" x="256.286" y="111.76" size="1.778" layer="95" ratio="15" rot="R180"/>
<attribute name="VALUE" x="256.286" y="103.378" size="1.778" layer="96" ratio="15" rot="R180"/>
</instance>
<instance part="C40" gate="G$1" x="96.52" y="60.96" smashed="yes" rot="MR90">
<attribute name="NAME" x="96.266" y="64.262" size="1.778" layer="95" ratio="15" rot="MR0"/>
<attribute name="VALUE" x="96.266" y="55.88" size="1.778" layer="96" ratio="15" rot="MR0"/>
</instance>
<instance part="R57" gate="G$1" x="256.54" y="58.42" smashed="yes" rot="R270">
<attribute name="NAME" x="256.54" y="63.5" size="1.778" layer="95" ratio="15" rot="R180"/>
<attribute name="VALUE" x="256.286" y="55.118" size="1.778" layer="96" ratio="15" rot="R180"/>
</instance>
<instance part="C41" gate="G$1" x="160.02" y="60.96" smashed="yes" rot="MR90">
<attribute name="NAME" x="159.766" y="64.262" size="1.778" layer="95" ratio="15" rot="MR0"/>
<attribute name="VALUE" x="159.766" y="56.134" size="1.778" layer="96" ratio="15" rot="MR0"/>
</instance>
<instance part="C42" gate="G$1" x="248.92" y="60.96" smashed="yes" rot="MR90">
<attribute name="NAME" x="248.666" y="64.262" size="1.778" layer="95" ratio="15" rot="MR0"/>
<attribute name="VALUE" x="248.666" y="56.134" size="1.778" layer="96" ratio="15" rot="MR0"/>
</instance>
<instance part="D12" gate="A" x="274.32" y="124.46" smashed="yes">
<attribute name="NAME" x="266.192" y="124.968" size="1.778" layer="95" ratio="15"/>
<attribute name="VALUE" x="277.622" y="124.714" size="1.778" layer="96" ratio="15"/>
</instance>
<instance part="C43" gate="G$1" x="86.36" y="60.96" smashed="yes" rot="MR90">
<attribute name="NAME" x="86.106" y="64.262" size="1.778" layer="95" ratio="15" rot="MR0"/>
<attribute name="VALUE" x="86.106" y="55.88" size="1.778" layer="96" ratio="15" rot="MR0"/>
</instance>
<instance part="C44" gate="G$1" x="152.4" y="60.96" smashed="yes" rot="MR90">
<attribute name="NAME" x="152.146" y="64.262" size="1.778" layer="95" ratio="15" rot="MR0"/>
<attribute name="VALUE" x="152.146" y="56.134" size="1.778" layer="96" ratio="15" rot="MR0"/>
</instance>
<instance part="C45" gate="G$1" x="238.76" y="60.96" smashed="yes" rot="MR90">
<attribute name="NAME" x="238.506" y="64.262" size="1.778" layer="95" ratio="15" rot="MR0"/>
<attribute name="VALUE" x="238.506" y="56.134" size="1.778" layer="96" ratio="15" rot="MR0"/>
</instance>
<instance part="F16" gate="G$1" x="38.1" y="190.5" smashed="yes">
<attribute name="NAME" x="37.846" y="196.596" size="1.778" layer="95" ratio="15" rot="R180"/>
<attribute name="VALUE" x="37.846" y="186.182" size="1.778" layer="96" ratio="15" rot="R180"/>
</instance>
<instance part="F17" gate="G$1" x="304.8" y="83.82" smashed="yes">
<attribute name="NAME" x="304.546" y="89.662" size="1.778" layer="95" ratio="15" rot="R180"/>
<attribute name="VALUE" x="304.546" y="79.502" size="1.778" layer="96" ratio="15" rot="R180"/>
</instance>
<instance part="F18" gate="G$1" x="330.2" y="88.9" smashed="yes">
<attribute name="NAME" x="329.946" y="94.996" size="1.778" layer="95" ratio="15" rot="R180"/>
<attribute name="VALUE" x="329.946" y="84.582" size="1.778" layer="96" ratio="15" rot="R180"/>
</instance>
<instance part="F19" gate="G$1" x="330.2" y="73.66" smashed="yes" rot="R180">
<attribute name="NAME" x="329.946" y="79.756" size="1.778" layer="95" ratio="15" rot="R180"/>
<attribute name="VALUE" x="329.946" y="69.596" size="1.778" layer="96" ratio="15" rot="R180"/>
</instance>
<instance part="C46" gate="G$1" x="53.34" y="190.5" smashed="yes" rot="R90">
<attribute name="NAME" x="53.086" y="193.802" size="1.778" layer="95" ratio="15" align="bottom-right"/>
<attribute name="VALUE" x="53.086" y="187.198" size="1.778" layer="96" ratio="15" rot="R180"/>
</instance>
<instance part="D13" gate="G$1" x="35.56" y="66.04" smashed="yes" rot="R90">
<attribute name="NAME" x="35.052" y="71.12" size="1.778" layer="95" ratio="15" rot="R180"/>
<attribute name="VALUE" x="35.052" y="62.738" size="1.778" layer="96" ratio="15" rot="R180"/>
</instance>
<instance part="C47" gate="G$1" x="104.14" y="60.96" smashed="yes" rot="MR90">
<attribute name="NAME" x="103.886" y="64.262" size="1.778" layer="95" ratio="15" rot="MR0"/>
<attribute name="VALUE" x="103.886" y="55.88" size="1.778" layer="96" ratio="15" rot="MR0"/>
</instance>
<instance part="R58" gate="G$1" x="165.1" y="96.52" smashed="yes" rot="R90">
<attribute name="NAME" x="164.846" y="101.6" size="1.778" layer="95" ratio="15" rot="R180"/>
<attribute name="VALUE" x="164.846" y="93.218" size="1.778" layer="96" ratio="15" rot="R180"/>
</instance>
<instance part="R59" gate="G$1" x="172.72" y="96.52" smashed="yes" rot="R90">
<attribute name="NAME" x="172.466" y="101.6" size="1.778" layer="95" ratio="15" rot="R180"/>
<attribute name="VALUE" x="172.466" y="93.218" size="1.778" layer="96" ratio="15" rot="R180"/>
</instance>
<instance part="R60" gate="G$1" x="144.78" y="60.96" smashed="yes" rot="R90">
<attribute name="NAME" x="144.526" y="66.04" size="1.778" layer="95" ratio="15" rot="R180"/>
<attribute name="VALUE" x="144.526" y="57.658" size="1.778" layer="96" ratio="15" rot="R180"/>
</instance>
<instance part="TP11" gate="G$1" x="33.02" y="83.82" smashed="yes" rot="R90">
<attribute name="NAME" x="33.02" y="82.55" size="1.778" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="TP12" gate="G$1" x="101.6" y="93.98" smashed="yes">
<attribute name="NAME" x="100.076" y="97.028" size="1.778" layer="95" ratio="15"/>
</instance>
<instance part="DP2" gate="G$1" x="236.22" y="84.836" smashed="yes">
<attribute name="NAME" x="235.458" y="86.614" size="1.778" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="DN2" gate="G$1" x="236.22" y="79.756" smashed="yes">
<attribute name="NAME" x="235.458" y="81.534" size="1.778" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="TP13" gate="G$1" x="203.2" y="89.916" smashed="yes">
<attribute name="NAME" x="202.438" y="91.694" size="1.778" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="TP14" gate="G$1" x="203.2" y="84.836" smashed="yes">
<attribute name="NAME" x="202.438" y="86.614" size="1.778" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="TP15" gate="G$1" x="203.2" y="74.422" smashed="yes">
<attribute name="NAME" x="202.438" y="76.2" size="1.778" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="F20" gate="G$1" x="15.24" y="20.32" smashed="yes">
<attribute name="NAME" x="15.24" y="27.178" size="1.778" layer="95"/>
</instance>
<instance part="F21" gate="G$1" x="30.48" y="20.32" smashed="yes">
<attribute name="NAME" x="30.48" y="27.178" size="1.778" layer="95"/>
</instance>
<instance part="F22" gate="G$1" x="45.72" y="20.32" smashed="yes">
<attribute name="NAME" x="45.72" y="27.178" size="1.778" layer="95"/>
</instance>
<instance part="F23" gate="G$1" x="60.96" y="20.32" smashed="yes">
<attribute name="NAME" x="60.96" y="27.178" size="1.778" layer="95"/>
</instance>
<instance part="F24" gate="G$1" x="76.2" y="20.32" smashed="yes">
<attribute name="NAME" x="76.2" y="27.178" size="1.778" layer="95"/>
</instance>
<instance part="F25" gate="G$1" x="91.44" y="20.32" smashed="yes">
<attribute name="NAME" x="91.44" y="27.178" size="1.778" layer="95"/>
</instance>
<instance part="C48" gate="G$1" x="78.74" y="190.5" smashed="yes" rot="MR90">
<attribute name="NAME" x="78.74" y="193.802" size="1.778" layer="95" ratio="15" rot="MR0"/>
<attribute name="VALUE" x="78.486" y="185.674" size="1.778" layer="96" ratio="15" rot="MR0"/>
</instance>
<instance part="SUPPLY17" gate="GND" x="205.74" y="165.1" smashed="yes">
<attribute name="VALUE" x="205.994" y="165.608" size="1.778" layer="96" ratio="15"/>
</instance>
<instance part="U$3" gate="G$1" x="0" y="0" smashed="yes">
<attribute name="DRAWING_NAME" x="317.5" y="10.16" size="1.778" layer="94"/>
<attribute name="SHEET" x="332.486" y="5.842" size="1.778" layer="94"/>
</instance>
<instance part="J4" gate="G$1" x="7.62" y="205.74" smashed="yes">
<attribute name="VALUE" x="7.62" y="211.074" size="1.778" layer="96" ratio="15"/>
<attribute name="NAME" x="7.62" y="213.106" size="1.778" layer="95" ratio="15"/>
</instance>
<instance part="U9" gate="A" x="238.76" y="187.96" smashed="yes">
<attribute name="NAME" x="259.08" y="213.36" size="1.778" layer="95" ratio="15" rot="SR180"/>
<attribute name="VALUE" x="259.08" y="210.82" size="1.778" layer="96" ratio="15" rot="SR180"/>
</instance>
<instance part="D14" gate="A" x="309.88" y="190.5" smashed="yes" rot="R90">
<attribute name="NAME" x="309.88" y="195.58" size="1.778" layer="95" ratio="15" rot="R180"/>
<attribute name="VALUE" x="296.926" y="185.42" size="1.778" layer="96" ratio="15"/>
</instance>
<instance part="C49" gate="G$1" x="332.74" y="198.12" smashed="yes" rot="R90">
<attribute name="NAME" x="332.486" y="203.2" size="1.778" layer="95" ratio="10" rot="R180"/>
<attribute name="VALUE" x="332.486" y="194.818" size="1.778" layer="96" ratio="10" rot="R180"/>
</instance>
<instance part="C50" gate="G$1" x="340.36" y="198.12" smashed="yes" rot="R90">
<attribute name="NAME" x="340.106" y="203.2" size="1.778" layer="95" ratio="10" rot="R180"/>
<attribute name="VALUE" x="340.106" y="194.818" size="1.778" layer="96" ratio="10" rot="R180"/>
</instance>
<instance part="C51" gate="G$1" x="347.98" y="198.12" smashed="yes" rot="R90">
<attribute name="NAME" x="347.726" y="203.2" size="1.778" layer="95" ratio="10" rot="R180"/>
<attribute name="VALUE" x="347.726" y="194.818" size="1.778" layer="96" ratio="10" rot="R180"/>
</instance>
<instance part="R61" gate="G$1" x="294.64" y="213.36" smashed="yes" rot="R180">
<attribute name="NAME" x="292.1" y="214.884" size="1.778" layer="95" ratio="15"/>
<attribute name="VALUE" x="293.116" y="210.058" size="1.778" layer="96" ratio="15"/>
</instance>
<instance part="R62" gate="G$1" x="312.42" y="213.36" smashed="yes">
<attribute name="NAME" x="309.88" y="214.884" size="1.778" layer="95" ratio="15"/>
<attribute name="VALUE" x="314.452" y="212.09" size="1.778" layer="96" ratio="15" rot="R180"/>
</instance>
<instance part="SUPPLY18" gate="GND" x="287.02" y="208.28" smashed="yes">
<attribute name="VALUE" x="286.766" y="210.312" size="1.778" layer="96" ratio="15" rot="R180"/>
</instance>
<instance part="SUPPLY19" gate="GND" x="322.58" y="165.1" smashed="yes">
<attribute name="VALUE" x="322.58" y="166.878" size="1.778" layer="96" ratio="15" rot="R180"/>
</instance>
<instance part="C52" gate="G$1" x="231.14" y="182.88" smashed="yes" rot="R90">
<attribute name="NAME" x="230.886" y="187.96" size="1.778" layer="95" ratio="10" rot="R180"/>
<attribute name="VALUE" x="230.886" y="179.578" size="1.778" layer="96" ratio="10" rot="R180"/>
</instance>
<instance part="R63" gate="G$1" x="223.52" y="185.42" smashed="yes" rot="R90">
<attribute name="NAME" x="223.266" y="190.5" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="223.266" y="182.118" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="P+12" gate="1" x="373.38" y="218.44" smashed="yes">
<attribute name="VALUE" x="368.3" y="218.44" size="1.778" layer="96" ratio="15"/>
</instance>
<instance part="SUPPLY20" gate="GND" x="48.26" y="165.1" smashed="yes">
<attribute name="VALUE" x="48.514" y="165.608" size="1.778" layer="96" ratio="15"/>
</instance>
<instance part="FB2" gate="G$1" x="358.14" y="205.74" smashed="yes">
<attribute name="NAME" x="355.6" y="209.55" size="1.778" layer="95" ratio="15"/>
<attribute name="VALUE" x="350.52" y="203.2" size="1.778" layer="96" ratio="15"/>
</instance>
<instance part="D15" gate="G$1" x="71.12" y="190.5" smashed="yes" rot="R90">
<attribute name="NAME" x="71.12" y="195.58" size="1.778" layer="95" ratio="15" rot="R180"/>
<attribute name="VALUE" x="70.866" y="187.198" size="1.778" layer="96" ratio="15" rot="R180"/>
</instance>
<instance part="C53" gate="G$1" x="213.36" y="187.96" smashed="yes" rot="R90">
<attribute name="NAME" x="213.106" y="193.04" size="1.778" layer="95" ratio="10" rot="R180"/>
<attribute name="VALUE" x="213.106" y="184.658" size="1.778" layer="96" ratio="10" rot="R180"/>
</instance>
<instance part="C54" gate="G$1" x="187.96" y="187.96" smashed="yes" rot="R90">
<attribute name="NAME" x="187.706" y="192.786" size="1.778" layer="95" ratio="10" rot="R180"/>
<attribute name="VALUE" x="187.706" y="184.658" size="1.778" layer="96" ratio="10" rot="R180"/>
</instance>
<instance part="L5" gate="G$1" x="322.58" y="205.74" smashed="yes">
<attribute name="NAME" x="320.04" y="208.788" size="1.778" layer="95" ratio="15"/>
<attribute name="VALUE" x="315.214" y="206.756" size="1.778" layer="96" ratio="15"/>
</instance>
<instance part="Y3" gate="G$1" x="81.28" y="86.36" smashed="yes" rot="R90">
<attribute name="NAME" x="81.28" y="89.662" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="81.28" y="84.836" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="C55" gate="G$1" x="287.02" y="203.2" smashed="yes">
<attribute name="NAME" x="280.162" y="203.454" size="1.778" layer="95" ratio="10"/>
<attribute name="VALUE" x="287.782" y="203.454" size="1.778" layer="96" ratio="10"/>
</instance>
<instance part="R64" gate="G$1" x="274.32" y="203.2" smashed="yes">
<attribute name="NAME" x="265.938" y="203.4286" size="1.778" layer="95"/>
<attribute name="VALUE" x="277.114" y="203.454" size="1.778" layer="96"/>
</instance>
<instance part="R65" gate="G$1" x="294.64" y="190.5" smashed="yes" rot="R90">
<attribute name="NAME" x="294.386" y="195.3514" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="294.386" y="186.69" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="C56" gate="G$1" x="294.64" y="177.8" smashed="yes" rot="R90">
<attribute name="NAME" x="294.386" y="182.88" size="1.778" layer="95" ratio="10" rot="R180"/>
<attribute name="VALUE" x="294.386" y="174.752" size="1.778" layer="96" ratio="10" rot="R180"/>
</instance>
<instance part="U10" gate="G$1" x="134.62" y="172.72" smashed="yes">
<attribute name="NAME" x="141.986" y="178.308" size="1.778" layer="95" ratio="15"/>
<attribute name="VALUE" x="121.92" y="182.88" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="R66" gate="G$1" x="114.3" y="205.74" smashed="yes">
<attribute name="NAME" x="116.84" y="209.296" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="116.84" y="203.962" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R67" gate="G$1" x="124.46" y="193.04" smashed="yes" rot="R90">
<attribute name="NAME" x="123.19" y="196.6214" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="123.19" y="191.262" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R68" gate="G$1" x="114.3" y="185.42" smashed="yes" rot="R180">
<attribute name="NAME" x="116.078" y="189.0014" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="116.332" y="183.642" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="Q2" gate="G$1" x="132.08" y="203.2" smashed="yes" rot="R90">
<attribute name="NAME" x="129.286" y="210.82" size="1.778" layer="95" ratio="15"/>
<attribute name="VALUE" x="129.032" y="208.28" size="1.778" layer="96" ratio="15"/>
</instance>
<instance part="R69" gate="G$1" x="119.38" y="162.56" smashed="yes" rot="R90">
<attribute name="NAME" x="118.11" y="166.1414" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="118.11" y="160.782" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R70" gate="G$1" x="119.38" y="147.32" smashed="yes" rot="R90">
<attribute name="NAME" x="118.11" y="150.9014" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="118.11" y="145.542" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R71" gate="G$1" x="99.06" y="193.04" smashed="yes" rot="R90">
<attribute name="NAME" x="97.79" y="196.6214" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="97.79" y="191.262" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R72" gate="G$1" x="99.06" y="162.56" smashed="yes" rot="R90">
<attribute name="NAME" x="97.79" y="166.1414" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="97.79" y="160.782" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R73" gate="G$1" x="134.62" y="193.04" smashed="yes" rot="R90">
<attribute name="NAME" x="133.35" y="196.6214" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="133.35" y="191.262" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R74" gate="G$1" x="149.86" y="193.04" smashed="yes" rot="R90">
<attribute name="NAME" x="148.59" y="196.6214" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="148.59" y="191.262" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R75" gate="G$1" x="152.4" y="162.56" smashed="yes" rot="R90">
<attribute name="NAME" x="151.13" y="166.1414" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="151.13" y="160.782" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="C57" gate="G$1" x="152.4" y="147.32" smashed="yes" rot="R90">
<attribute name="NAME" x="151.13" y="151.13" size="1.778" layer="95" ratio="10" rot="R180"/>
<attribute name="VALUE" x="151.13" y="144.78" size="1.778" layer="96" ratio="10" rot="R180"/>
</instance>
<instance part="C58" gate="G$1" x="200.66" y="187.96" smashed="yes" rot="R90">
<attribute name="NAME" x="200.406" y="192.786" size="1.778" layer="95" ratio="10" rot="R180"/>
<attribute name="VALUE" x="200.406" y="184.658" size="1.778" layer="96" ratio="10" rot="R180"/>
</instance>
<instance part="C59" gate="G$1" x="134.62" y="157.48" smashed="yes" rot="R90">
<attribute name="NAME" x="134.62" y="162.56" size="1.778" layer="95" ratio="10" rot="R180"/>
<attribute name="VALUE" x="134.62" y="152.4" size="1.778" layer="96" ratio="10" rot="R180"/>
</instance>
<instance part="SUPPLY21" gate="GND" x="129.54" y="134.62" smashed="yes">
<attribute name="VALUE" x="129.794" y="135.128" size="1.778" layer="96" ratio="15"/>
</instance>
<instance part="R76" gate="G$1" x="167.64" y="175.26" smashed="yes" rot="R180">
<attribute name="NAME" x="166.37" y="176.7586" size="1.778" layer="95"/>
<attribute name="VALUE" x="166.37" y="171.958" size="1.778" layer="96"/>
</instance>
<instance part="R77" gate="G$1" x="86.36" y="76.2" smashed="yes">
<attribute name="NAME" x="78.74" y="76.454" size="1.778" layer="95" ratio="15"/>
<attribute name="VALUE" x="88.9" y="76.454" size="1.778" layer="96" ratio="15"/>
</instance>
<instance part="L6" gate="G$1" x="170.18" y="205.74" smashed="yes">
<attribute name="NAME" x="167.64" y="208.788" size="1.778" layer="95" ratio="15"/>
<attribute name="VALUE" x="162.814" y="206.756" size="1.778" layer="96" ratio="15"/>
</instance>
<instance part="L7" gate="G$1" x="167.64" y="124.46" smashed="yes">
<attribute name="NAME" x="165.1" y="127.508" size="1.778" layer="95" ratio="15"/>
<attribute name="VALUE" x="160.274" y="125.476" size="1.778" layer="96" ratio="15"/>
</instance>
<instance part="X2" gate="-5" x="342.9" y="50.8" smashed="yes">
<attribute name="NAME" x="345.44" y="50.038" size="1.524" layer="95"/>
</instance>
<instance part="X2" gate="-6" x="342.9" y="66.04" smashed="yes">
<attribute name="NAME" x="345.44" y="65.278" size="1.524" layer="95"/>
</instance>
<instance part="X2" gate="-7" x="345.44" y="124.46" smashed="yes">
<attribute name="NAME" x="347.98" y="123.698" size="1.524" layer="95"/>
</instance>
<instance part="X2" gate="-8" x="342.9" y="99.06" smashed="yes">
<attribute name="NAME" x="345.44" y="98.298" size="1.524" layer="95"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="N$89" class="0">
<segment>
<wire x1="58.42" y1="81.28" x2="81.28" y2="81.28" width="0.1524" layer="91"/>
<wire x1="81.28" y1="81.28" x2="88.9" y2="81.28" width="0.1524" layer="91"/>
<wire x1="88.9" y1="81.28" x2="88.9" y2="83.82" width="0.1524" layer="91"/>
<pinref part="C37" gate="G$1" pin="2"/>
<pinref part="U7" gate="A" pin="XTAL1"/>
<wire x1="88.9" y1="83.82" x2="106.68" y2="83.82" width="0.1524" layer="91"/>
<wire x1="81.28" y1="83.82" x2="81.28" y2="81.28" width="0.1524" layer="91"/>
<junction x="81.28" y="81.28"/>
<pinref part="Y3" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$90" class="0">
<segment>
<pinref part="C36" gate="G$1" pin="2"/>
<wire x1="88.9" y1="86.36" x2="88.9" y2="91.44" width="0.1524" layer="91"/>
<wire x1="88.9" y1="91.44" x2="81.28" y2="91.44" width="0.1524" layer="91"/>
<pinref part="U7" gate="A" pin="XTAL2"/>
<wire x1="81.28" y1="91.44" x2="58.42" y2="91.44" width="0.1524" layer="91"/>
<wire x1="88.9" y1="86.36" x2="106.68" y2="86.36" width="0.1524" layer="91"/>
<wire x1="81.28" y1="88.9" x2="81.28" y2="91.44" width="0.1524" layer="91"/>
<junction x="81.28" y="91.44"/>
<pinref part="Y3" gate="G$1" pin="2"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<wire x1="335.28" y1="81.28" x2="335.28" y2="50.8" width="0.1524" layer="91"/>
<wire x1="233.68" y1="50.8" x2="238.76" y2="50.8" width="0.1524" layer="91"/>
<junction x="233.68" y="50.8"/>
<pinref part="SUPPLY16" gate="GND" pin="GND"/>
<wire x1="238.76" y1="50.8" x2="248.92" y2="50.8" width="0.1524" layer="91"/>
<wire x1="248.92" y1="50.8" x2="256.54" y2="50.8" width="0.1524" layer="91"/>
<wire x1="256.54" y1="50.8" x2="281.94" y2="50.8" width="0.1524" layer="91"/>
<wire x1="281.94" y1="50.8" x2="304.8" y2="50.8" width="0.1524" layer="91"/>
<wire x1="304.8" y1="50.8" x2="335.28" y2="50.8" width="0.1524" layer="91"/>
<wire x1="231.14" y1="73.66" x2="233.68" y2="73.66" width="0.1524" layer="91"/>
<wire x1="233.68" y1="73.66" x2="233.68" y2="50.8" width="0.1524" layer="91"/>
<pinref part="U8" gate="G$1" pin="5"/>
<wire x1="281.94" y1="50.8" x2="281.94" y2="83.82" width="0.1524" layer="91"/>
<junction x="281.94" y="50.8"/>
<pinref part="D11" gate="A" pin="-"/>
<pinref part="D10" gate="A" pin="-"/>
<wire x1="276.86" y1="86.36" x2="276.86" y2="83.82" width="0.1524" layer="91"/>
<wire x1="276.86" y1="83.82" x2="276.86" y2="81.28" width="0.1524" layer="91"/>
<wire x1="281.94" y1="83.82" x2="276.86" y2="83.82" width="0.1524" layer="91"/>
<junction x="276.86" y="83.82"/>
<wire x1="304.8" y1="78.74" x2="304.8" y2="50.8" width="0.1524" layer="91"/>
<junction x="304.8" y="50.8"/>
<pinref part="R57" gate="G$1" pin="2"/>
<wire x1="256.54" y1="53.34" x2="256.54" y2="50.8" width="0.1524" layer="91"/>
<junction x="256.54" y="50.8"/>
<pinref part="C42" gate="G$1" pin="1"/>
<junction x="248.92" y="50.8"/>
<wire x1="248.92" y1="55.88" x2="248.92" y2="50.8" width="0.1524" layer="91"/>
<pinref part="F17" gate="G$1" pin="2"/>
<pinref part="F18" gate="G$1" pin="2"/>
<pinref part="F19" gate="G$1" pin="2"/>
<wire x1="330.2" y1="83.82" x2="330.2" y2="81.28" width="0.1524" layer="91"/>
<wire x1="330.2" y1="81.28" x2="330.2" y2="78.74" width="0.1524" layer="91"/>
<wire x1="335.28" y1="81.28" x2="330.2" y2="81.28" width="0.1524" layer="91"/>
<junction x="330.2" y="81.28"/>
<pinref part="C45" gate="G$1" pin="1"/>
<wire x1="238.76" y1="55.88" x2="238.76" y2="50.8" width="0.1524" layer="91"/>
<junction x="238.76" y="50.8"/>
<junction x="335.28" y="50.8"/>
<pinref part="X2" gate="-5" pin="1"/>
<wire x1="340.36" y1="50.8" x2="335.28" y2="50.8" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R50" gate="G$1" pin="2"/>
<wire x1="187.96" y1="78.74" x2="190.5" y2="78.74" width="0.1524" layer="91"/>
<wire x1="190.5" y1="78.74" x2="190.5" y2="76.2" width="0.1524" layer="91"/>
<pinref part="R51" gate="G$1" pin="2"/>
<wire x1="190.5" y1="76.2" x2="187.96" y2="76.2" width="0.1524" layer="91"/>
<wire x1="190.5" y1="76.2" x2="190.5" y2="73.66" width="0.1524" layer="91"/>
<junction x="190.5" y="76.2"/>
<pinref part="SUPPLY15" gate="GND" pin="GND"/>
<pinref part="U7" gate="A" pin="GND_2"/>
<wire x1="190.5" y1="73.66" x2="190.5" y2="50.8" width="0.1524" layer="91"/>
<wire x1="190.5" y1="73.66" x2="142.24" y2="73.66" width="0.1524" layer="91"/>
<junction x="190.5" y="73.66"/>
<junction x="190.5" y="50.8"/>
<wire x1="99.06" y1="50.8" x2="104.14" y2="50.8" width="0.1524" layer="91"/>
<pinref part="U7" gate="A" pin="EPAD"/>
<wire x1="104.14" y1="50.8" x2="144.78" y2="50.8" width="0.1524" layer="91"/>
<wire x1="144.78" y1="50.8" x2="152.4" y2="50.8" width="0.1524" layer="91"/>
<wire x1="152.4" y1="50.8" x2="160.02" y2="50.8" width="0.1524" layer="91"/>
<wire x1="160.02" y1="50.8" x2="190.5" y2="50.8" width="0.1524" layer="91"/>
<wire x1="106.68" y1="91.44" x2="99.06" y2="91.44" width="0.1524" layer="91"/>
<wire x1="99.06" y1="91.44" x2="99.06" y2="88.9" width="0.1524" layer="91"/>
<pinref part="U7" gate="A" pin="GND"/>
<wire x1="99.06" y1="88.9" x2="99.06" y2="50.8" width="0.1524" layer="91"/>
<wire x1="106.68" y1="88.9" x2="99.06" y2="88.9" width="0.1524" layer="91"/>
<junction x="99.06" y="88.9"/>
<pinref part="C38" gate="G$1" pin="1"/>
<wire x1="50.8" y1="60.96" x2="50.8" y2="50.8" width="0.1524" layer="91"/>
<pinref part="C36" gate="G$1" pin="1"/>
<wire x1="48.26" y1="91.44" x2="43.18" y2="91.44" width="0.1524" layer="91"/>
<wire x1="43.18" y1="91.44" x2="43.18" y2="81.28" width="0.1524" layer="91"/>
<pinref part="C37" gate="G$1" pin="1"/>
<wire x1="48.26" y1="81.28" x2="43.18" y2="81.28" width="0.1524" layer="91"/>
<junction x="43.18" y="81.28"/>
<wire x1="43.18" y1="50.8" x2="43.18" y2="81.28" width="0.1524" layer="91"/>
<wire x1="43.18" y1="50.8" x2="50.8" y2="50.8" width="0.1524" layer="91"/>
<wire x1="99.06" y1="50.8" x2="96.52" y2="50.8" width="0.1524" layer="91"/>
<pinref part="R49" gate="G$1" pin="1"/>
<wire x1="96.52" y1="50.8" x2="86.36" y2="50.8" width="0.1524" layer="91"/>
<wire x1="86.36" y1="50.8" x2="78.74" y2="50.8" width="0.1524" layer="91"/>
<wire x1="50.8" y1="50.8" x2="78.74" y2="50.8" width="0.1524" layer="91"/>
<wire x1="78.74" y1="50.8" x2="78.74" y2="58.42" width="0.1524" layer="91"/>
<junction x="50.8" y="50.8"/>
<junction x="78.74" y="50.8"/>
<junction x="99.06" y="50.8"/>
<pinref part="C40" gate="G$1" pin="1"/>
<junction x="96.52" y="50.8"/>
<pinref part="C41" gate="G$1" pin="1"/>
<wire x1="96.52" y1="55.88" x2="96.52" y2="50.8" width="0.1524" layer="91"/>
<junction x="160.02" y="50.8"/>
<wire x1="160.02" y1="55.88" x2="160.02" y2="50.8" width="0.1524" layer="91"/>
<wire x1="35.56" y1="50.8" x2="43.18" y2="50.8" width="0.1524" layer="91"/>
<junction x="43.18" y="50.8"/>
<pinref part="C47" gate="G$1" pin="1"/>
<wire x1="104.14" y1="55.88" x2="104.14" y2="50.8" width="0.1524" layer="91"/>
<junction x="104.14" y="50.8"/>
<pinref part="C43" gate="G$1" pin="1"/>
<wire x1="86.36" y1="55.88" x2="86.36" y2="50.8" width="0.1524" layer="91"/>
<junction x="86.36" y="50.8"/>
<pinref part="C44" gate="G$1" pin="1"/>
<wire x1="152.4" y1="55.88" x2="152.4" y2="50.8" width="0.1524" layer="91"/>
<junction x="152.4" y="50.8"/>
<pinref part="R60" gate="G$1" pin="1"/>
<wire x1="144.78" y1="55.88" x2="144.78" y2="50.8" width="0.1524" layer="91"/>
<junction x="144.78" y="50.8"/>
<pinref part="D13" gate="G$1" pin="+"/>
<wire x1="35.56" y1="60.96" x2="35.56" y2="50.8" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R61" gate="G$1" pin="2"/>
<pinref part="SUPPLY18" gate="GND" pin="GND"/>
<wire x1="289.56" y1="213.36" x2="287.02" y2="213.36" width="0.1524" layer="91"/>
<wire x1="287.02" y1="213.36" x2="287.02" y2="210.82" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U9" gate="A" pin="GND"/>
<wire x1="264.16" y1="193.04" x2="269.24" y2="193.04" width="0.1524" layer="91"/>
<wire x1="269.24" y1="193.04" x2="269.24" y2="190.5" width="0.1524" layer="91"/>
<pinref part="U9" gate="A" pin="PAD"/>
<wire x1="269.24" y1="190.5" x2="264.16" y2="190.5" width="0.1524" layer="91"/>
<pinref part="D14" gate="A" pin="A"/>
<wire x1="309.88" y1="185.42" x2="309.88" y2="170.18" width="0.1524" layer="91"/>
<wire x1="309.88" y1="170.18" x2="294.64" y2="170.18" width="0.1524" layer="91"/>
<wire x1="294.64" y1="170.18" x2="269.24" y2="170.18" width="0.1524" layer="91"/>
<wire x1="269.24" y1="170.18" x2="269.24" y2="190.5" width="0.1524" layer="91"/>
<junction x="269.24" y="190.5"/>
<junction x="309.88" y="170.18"/>
<pinref part="C49" gate="G$1" pin="1"/>
<wire x1="332.74" y1="193.04" x2="332.74" y2="170.18" width="0.1524" layer="91"/>
<wire x1="332.74" y1="170.18" x2="322.58" y2="170.18" width="0.1524" layer="91"/>
<pinref part="C50" gate="G$1" pin="1"/>
<wire x1="322.58" y1="170.18" x2="309.88" y2="170.18" width="0.1524" layer="91"/>
<wire x1="340.36" y1="193.04" x2="340.36" y2="170.18" width="0.1524" layer="91"/>
<wire x1="340.36" y1="170.18" x2="332.74" y2="170.18" width="0.1524" layer="91"/>
<junction x="332.74" y="170.18"/>
<pinref part="C51" gate="G$1" pin="1"/>
<wire x1="347.98" y1="193.04" x2="347.98" y2="170.18" width="0.1524" layer="91"/>
<wire x1="347.98" y1="170.18" x2="340.36" y2="170.18" width="0.1524" layer="91"/>
<junction x="340.36" y="170.18"/>
<pinref part="SUPPLY19" gate="GND" pin="GND"/>
<wire x1="322.58" y1="167.64" x2="322.58" y2="170.18" width="0.1524" layer="91"/>
<junction x="322.58" y="170.18"/>
<pinref part="C56" gate="G$1" pin="1"/>
<wire x1="294.64" y1="172.72" x2="294.64" y2="170.18" width="0.1524" layer="91"/>
<junction x="294.64" y="170.18"/>
</segment>
<segment>
<pinref part="C52" gate="G$1" pin="1"/>
<wire x1="231.14" y1="177.8" x2="231.14" y2="170.18" width="0.1524" layer="91"/>
<pinref part="R63" gate="G$1" pin="1"/>
<wire x1="223.52" y1="180.34" x2="223.52" y2="170.18" width="0.1524" layer="91"/>
<wire x1="223.52" y1="170.18" x2="231.14" y2="170.18" width="0.1524" layer="91"/>
<wire x1="223.52" y1="170.18" x2="213.36" y2="170.18" width="0.1524" layer="91"/>
<junction x="223.52" y="170.18"/>
<pinref part="SUPPLY17" gate="GND" pin="GND"/>
<wire x1="213.36" y1="170.18" x2="205.74" y2="170.18" width="0.1524" layer="91"/>
<wire x1="205.74" y1="170.18" x2="205.74" y2="167.64" width="0.1524" layer="91"/>
<junction x="205.74" y="170.18"/>
<wire x1="187.96" y1="170.18" x2="200.66" y2="170.18" width="0.1524" layer="91"/>
<pinref part="C54" gate="G$1" pin="1"/>
<wire x1="200.66" y1="170.18" x2="205.74" y2="170.18" width="0.1524" layer="91"/>
<wire x1="187.96" y1="182.88" x2="187.96" y2="170.18" width="0.1524" layer="91"/>
<pinref part="C58" gate="G$1" pin="1"/>
<wire x1="200.66" y1="182.88" x2="200.66" y2="170.18" width="0.1524" layer="91"/>
<junction x="200.66" y="170.18"/>
<pinref part="C53" gate="G$1" pin="1"/>
<wire x1="213.36" y1="182.88" x2="213.36" y2="170.18" width="0.1524" layer="91"/>
<junction x="213.36" y="170.18"/>
</segment>
<segment>
<wire x1="53.34" y1="170.18" x2="71.12" y2="170.18" width="0.1524" layer="91"/>
<pinref part="C48" gate="G$1" pin="1"/>
<wire x1="71.12" y1="170.18" x2="78.74" y2="170.18" width="0.1524" layer="91"/>
<wire x1="78.74" y1="185.42" x2="78.74" y2="170.18" width="0.1524" layer="91"/>
<pinref part="C46" gate="G$1" pin="1"/>
<wire x1="53.34" y1="185.42" x2="53.34" y2="170.18" width="0.1524" layer="91"/>
<junction x="53.34" y="170.18"/>
<pinref part="F16" gate="G$1" pin="2"/>
<wire x1="53.34" y1="170.18" x2="48.26" y2="170.18" width="0.1524" layer="91"/>
<wire x1="48.26" y1="170.18" x2="38.1" y2="170.18" width="0.1524" layer="91"/>
<wire x1="38.1" y1="170.18" x2="38.1" y2="185.42" width="0.1524" layer="91"/>
<wire x1="15.24" y1="203.2" x2="15.24" y2="170.18" width="0.1524" layer="91"/>
<wire x1="15.24" y1="170.18" x2="38.1" y2="170.18" width="0.1524" layer="91"/>
<junction x="38.1" y="170.18"/>
<pinref part="J4" gate="G$1" pin="2"/>
<wire x1="15.24" y1="203.2" x2="12.7" y2="203.2" width="0.1524" layer="91"/>
<pinref part="D15" gate="G$1" pin="+"/>
<wire x1="71.12" y1="185.42" x2="71.12" y2="170.18" width="0.1524" layer="91"/>
<junction x="71.12" y="170.18"/>
<pinref part="SUPPLY20" gate="GND" pin="GND"/>
<wire x1="48.26" y1="167.64" x2="48.26" y2="170.18" width="0.1524" layer="91"/>
<junction x="48.26" y="170.18"/>
</segment>
<segment>
<pinref part="U10" gate="G$1" pin="5_GND"/>
<wire x1="139.7" y1="165.1" x2="139.7" y2="144.78" width="0.1524" layer="91"/>
<pinref part="C59" gate="G$1" pin="1"/>
<wire x1="134.62" y1="152.4" x2="134.62" y2="144.78" width="0.1524" layer="91"/>
<wire x1="134.62" y1="144.78" x2="139.7" y2="144.78" width="0.1524" layer="91"/>
<pinref part="R72" gate="G$1" pin="1"/>
<wire x1="99.06" y1="157.48" x2="99.06" y2="139.7" width="0.1524" layer="91"/>
<pinref part="C57" gate="G$1" pin="1"/>
<wire x1="99.06" y1="139.7" x2="119.38" y2="139.7" width="0.1524" layer="91"/>
<wire x1="119.38" y1="139.7" x2="129.54" y2="139.7" width="0.1524" layer="91"/>
<wire x1="129.54" y1="139.7" x2="139.7" y2="139.7" width="0.1524" layer="91"/>
<wire x1="139.7" y1="139.7" x2="152.4" y2="139.7" width="0.1524" layer="91"/>
<wire x1="152.4" y1="139.7" x2="152.4" y2="142.24" width="0.1524" layer="91"/>
<wire x1="139.7" y1="144.78" x2="139.7" y2="139.7" width="0.1524" layer="91"/>
<junction x="139.7" y="144.78"/>
<junction x="139.7" y="139.7"/>
<pinref part="R70" gate="G$1" pin="1"/>
<wire x1="119.38" y1="142.24" x2="119.38" y2="139.7" width="0.1524" layer="91"/>
<junction x="119.38" y="139.7"/>
<pinref part="SUPPLY21" gate="GND" pin="GND"/>
<wire x1="129.54" y1="137.16" x2="129.54" y2="139.7" width="0.1524" layer="91"/>
<junction x="129.54" y="139.7"/>
</segment>
</net>
<net name="N$91" class="0">
<segment>
<pinref part="U7" gate="A" pin="TXOUT"/>
<pinref part="R47" gate="G$1" pin="2"/>
<wire x1="106.68" y1="78.74" x2="101.6" y2="78.74" width="0.1524" layer="91"/>
<wire x1="101.6" y1="78.74" x2="91.44" y2="78.74" width="0.1524" layer="91"/>
<wire x1="101.6" y1="93.98" x2="101.6" y2="78.74" width="0.1524" layer="91"/>
<junction x="101.6" y="78.74"/>
<pinref part="TP12" gate="G$1" pin="PP"/>
</segment>
</net>
<net name="N$92" class="0">
<segment>
<pinref part="U7" gate="A" pin="BIAS"/>
<pinref part="R48" gate="G$1" pin="2"/>
<wire x1="106.68" y1="73.66" x2="104.14" y2="73.66" width="0.1524" layer="91"/>
<pinref part="C47" gate="G$1" pin="2"/>
<wire x1="104.14" y1="73.66" x2="91.44" y2="73.66" width="0.1524" layer="91"/>
<wire x1="104.14" y1="66.04" x2="104.14" y2="73.66" width="0.1524" layer="91"/>
<junction x="104.14" y="73.66"/>
</segment>
</net>
<net name="N$93" class="0">
<segment>
<pinref part="U7" gate="A" pin="DIRSET2"/>
<wire x1="142.24" y1="78.74" x2="172.72" y2="78.74" width="0.1524" layer="91"/>
<pinref part="R50" gate="G$1" pin="1"/>
<pinref part="R59" gate="G$1" pin="1"/>
<wire x1="172.72" y1="78.74" x2="177.8" y2="78.74" width="0.1524" layer="91"/>
<wire x1="172.72" y1="91.44" x2="172.72" y2="78.74" width="0.1524" layer="91"/>
<junction x="172.72" y="78.74"/>
</segment>
</net>
<net name="N$94" class="0">
<segment>
<pinref part="U7" gate="A" pin="DIRSET1"/>
<wire x1="142.24" y1="76.2" x2="165.1" y2="76.2" width="0.1524" layer="91"/>
<pinref part="R51" gate="G$1" pin="1"/>
<pinref part="R58" gate="G$1" pin="1"/>
<wire x1="165.1" y1="76.2" x2="177.8" y2="76.2" width="0.1524" layer="91"/>
<wire x1="165.1" y1="91.44" x2="165.1" y2="76.2" width="0.1524" layer="91"/>
<junction x="165.1" y="76.2"/>
</segment>
</net>
<net name="N$95" class="0">
<segment>
<wire x1="193.04" y1="83.82" x2="193.04" y2="73.66" width="0.1524" layer="91"/>
<wire x1="193.04" y1="73.66" x2="203.2" y2="73.66" width="0.1524" layer="91"/>
<wire x1="203.2" y1="73.66" x2="205.74" y2="73.66" width="0.1524" layer="91"/>
<pinref part="R54" gate="G$1" pin="2"/>
<wire x1="187.96" y1="83.82" x2="193.04" y2="83.82" width="0.1524" layer="91"/>
<pinref part="U8" gate="G$1" pin="4"/>
<wire x1="203.2" y1="74.422" x2="203.2" y2="73.66" width="0.1524" layer="91"/>
<junction x="203.2" y="73.66"/>
<pinref part="TP15" gate="G$1" pin="PP"/>
</segment>
</net>
<net name="N$96" class="0">
<segment>
<wire x1="195.58" y1="81.28" x2="195.58" y2="83.82" width="0.1524" layer="91"/>
<pinref part="R53" gate="G$1" pin="2"/>
<wire x1="195.58" y1="83.82" x2="200.66" y2="83.82" width="0.1524" layer="91"/>
<wire x1="187.96" y1="81.28" x2="195.58" y2="81.28" width="0.1524" layer="91"/>
<wire x1="200.66" y1="83.82" x2="200.66" y2="78.74" width="0.1524" layer="91"/>
<wire x1="200.66" y1="78.74" x2="205.74" y2="78.74" width="0.1524" layer="91"/>
<junction x="200.66" y="83.82"/>
<pinref part="U8" gate="G$1" pin="2"/>
<pinref part="U8" gate="G$1" pin="3"/>
<wire x1="200.66" y1="83.82" x2="203.2" y2="83.82" width="0.1524" layer="91"/>
<wire x1="203.2" y1="83.82" x2="205.74" y2="83.82" width="0.1524" layer="91"/>
<wire x1="203.2" y1="84.836" x2="203.2" y2="83.82" width="0.1524" layer="91"/>
<junction x="203.2" y="83.82"/>
<pinref part="TP14" gate="G$1" pin="PP"/>
</segment>
</net>
<net name="DATA2_P" class="0">
<segment>
<wire x1="231.14" y1="83.82" x2="236.22" y2="83.82" width="0.1524" layer="91"/>
<wire x1="236.22" y1="83.82" x2="251.46" y2="83.82" width="0.1524" layer="91"/>
<wire x1="261.62" y1="99.06" x2="256.54" y2="99.06" width="0.1524" layer="91"/>
<wire x1="256.54" y1="99.06" x2="251.46" y2="99.06" width="0.1524" layer="91"/>
<wire x1="251.46" y1="99.06" x2="251.46" y2="83.82" width="0.1524" layer="91"/>
<pinref part="R52" gate="G$1" pin="2"/>
<wire x1="261.62" y1="88.9" x2="261.62" y2="99.06" width="0.1524" layer="91"/>
<pinref part="U8" gate="G$1" pin="7"/>
<wire x1="261.62" y1="99.06" x2="276.86" y2="99.06" width="0.1524" layer="91"/>
<junction x="261.62" y="99.06"/>
<pinref part="D11" gate="A" pin="+"/>
<wire x1="276.86" y1="99.06" x2="330.2" y2="99.06" width="0.1524" layer="91"/>
<wire x1="276.86" y1="96.52" x2="276.86" y2="99.06" width="0.1524" layer="91"/>
<junction x="276.86" y="99.06"/>
<wire x1="236.22" y1="84.836" x2="236.22" y2="83.82" width="0.1524" layer="91"/>
<junction x="236.22" y="83.82"/>
<pinref part="F18" gate="G$1" pin="1"/>
<wire x1="330.2" y1="93.98" x2="330.2" y2="99.06" width="0.1524" layer="91"/>
<pinref part="DP2" gate="G$1" pin="PP"/>
<pinref part="R56" gate="G$1" pin="2"/>
<wire x1="256.54" y1="101.6" x2="256.54" y2="99.06" width="0.1524" layer="91"/>
<junction x="256.54" y="99.06"/>
<wire x1="340.36" y1="99.06" x2="330.2" y2="99.06" width="0.1524" layer="91"/>
<junction x="330.2" y="99.06"/>
<pinref part="X2" gate="-8" pin="1"/>
</segment>
</net>
<net name="DATA2_N" class="0">
<segment>
<wire x1="261.62" y1="66.04" x2="256.54" y2="66.04" width="0.1524" layer="91"/>
<wire x1="256.54" y1="66.04" x2="251.46" y2="66.04" width="0.1524" layer="91"/>
<pinref part="R52" gate="G$1" pin="1"/>
<wire x1="231.14" y1="78.74" x2="236.22" y2="78.74" width="0.1524" layer="91"/>
<wire x1="236.22" y1="78.74" x2="251.46" y2="78.74" width="0.1524" layer="91"/>
<wire x1="261.62" y1="78.74" x2="261.62" y2="66.04" width="0.1524" layer="91"/>
<wire x1="251.46" y1="66.04" x2="251.46" y2="78.74" width="0.1524" layer="91"/>
<pinref part="U8" gate="G$1" pin="6"/>
<pinref part="R57" gate="G$1" pin="1"/>
<wire x1="256.54" y1="63.5" x2="256.54" y2="66.04" width="0.1524" layer="91"/>
<junction x="256.54" y="66.04"/>
<wire x1="261.62" y1="66.04" x2="276.86" y2="66.04" width="0.1524" layer="91"/>
<junction x="261.62" y="66.04"/>
<pinref part="D10" gate="A" pin="+"/>
<wire x1="276.86" y1="66.04" x2="330.2" y2="66.04" width="0.1524" layer="91"/>
<wire x1="276.86" y1="71.12" x2="276.86" y2="66.04" width="0.1524" layer="91"/>
<junction x="276.86" y="66.04"/>
<wire x1="236.22" y1="79.756" x2="236.22" y2="78.74" width="0.1524" layer="91"/>
<junction x="236.22" y="78.74"/>
<pinref part="F19" gate="G$1" pin="1"/>
<wire x1="330.2" y1="68.58" x2="330.2" y2="66.04" width="0.1524" layer="91"/>
<pinref part="DN2" gate="G$1" pin="PP"/>
<wire x1="340.36" y1="66.04" x2="330.2" y2="66.04" width="0.1524" layer="91"/>
<junction x="330.2" y="66.04"/>
<pinref part="X2" gate="-6" pin="1"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<pinref part="U7" gate="A" pin="VL"/>
<wire x1="142.24" y1="86.36" x2="160.02" y2="86.36" width="0.1524" layer="91"/>
<wire x1="160.02" y1="86.36" x2="160.02" y2="104.14" width="0.1524" layer="91"/>
<pinref part="P+10" gate="1" pin="+5V"/>
<pinref part="C41" gate="G$1" pin="2"/>
<wire x1="160.02" y1="104.14" x2="160.02" y2="106.68" width="0.1524" layer="91"/>
<wire x1="160.02" y1="86.36" x2="160.02" y2="68.58" width="0.1524" layer="91"/>
<junction x="160.02" y="86.36"/>
<pinref part="C44" gate="G$1" pin="2"/>
<wire x1="160.02" y1="68.58" x2="160.02" y2="66.04" width="0.1524" layer="91"/>
<wire x1="152.4" y1="66.04" x2="152.4" y2="68.58" width="0.1524" layer="91"/>
<wire x1="152.4" y1="68.58" x2="160.02" y2="68.58" width="0.1524" layer="91"/>
<junction x="160.02" y="68.58"/>
<pinref part="R59" gate="G$1" pin="2"/>
<wire x1="172.72" y1="101.6" x2="172.72" y2="104.14" width="0.1524" layer="91"/>
<wire x1="172.72" y1="104.14" x2="165.1" y2="104.14" width="0.1524" layer="91"/>
<pinref part="R58" gate="G$1" pin="2"/>
<wire x1="165.1" y1="104.14" x2="165.1" y2="101.6" width="0.1524" layer="91"/>
<wire x1="160.02" y1="104.14" x2="165.1" y2="104.14" width="0.1524" layer="91"/>
<junction x="160.02" y="104.14"/>
<junction x="165.1" y="104.14"/>
</segment>
<segment>
<wire x1="231.14" y1="88.9" x2="248.92" y2="88.9" width="0.1524" layer="91"/>
<wire x1="248.92" y1="88.9" x2="248.92" y2="114.3" width="0.1524" layer="91"/>
<pinref part="P+11" gate="1" pin="+5V"/>
<pinref part="U8" gate="G$1" pin="8"/>
<pinref part="R56" gate="G$1" pin="1"/>
<wire x1="248.92" y1="114.3" x2="248.92" y2="116.84" width="0.1524" layer="91"/>
<wire x1="256.54" y1="111.76" x2="256.54" y2="114.3" width="0.1524" layer="91"/>
<wire x1="256.54" y1="114.3" x2="248.92" y2="114.3" width="0.1524" layer="91"/>
<junction x="248.92" y="114.3"/>
<pinref part="C42" gate="G$1" pin="2"/>
<wire x1="248.92" y1="88.9" x2="248.92" y2="68.58" width="0.1524" layer="91"/>
<junction x="248.92" y="88.9"/>
<pinref part="C45" gate="G$1" pin="2"/>
<wire x1="248.92" y1="68.58" x2="248.92" y2="66.04" width="0.1524" layer="91"/>
<wire x1="238.76" y1="66.04" x2="238.76" y2="68.58" width="0.1524" layer="91"/>
<wire x1="238.76" y1="68.58" x2="248.92" y2="68.58" width="0.1524" layer="91"/>
<junction x="248.92" y="68.58"/>
</segment>
<segment>
<pinref part="U7" gate="A" pin="VCC"/>
<wire x1="106.68" y1="81.28" x2="96.52" y2="81.28" width="0.1524" layer="91"/>
<wire x1="96.52" y1="81.28" x2="96.52" y2="101.6" width="0.1524" layer="91"/>
<pinref part="P+9" gate="1" pin="+5V"/>
<pinref part="C40" gate="G$1" pin="2"/>
<wire x1="96.52" y1="81.28" x2="96.52" y2="68.58" width="0.1524" layer="91"/>
<junction x="96.52" y="81.28"/>
<pinref part="C43" gate="G$1" pin="2"/>
<wire x1="96.52" y1="68.58" x2="96.52" y2="66.04" width="0.1524" layer="91"/>
<wire x1="86.36" y1="66.04" x2="86.36" y2="68.58" width="0.1524" layer="91"/>
<wire x1="86.36" y1="68.58" x2="96.52" y2="68.58" width="0.1524" layer="91"/>
<junction x="96.52" y="68.58"/>
</segment>
<segment>
<wire x1="363.22" y1="205.74" x2="365.76" y2="205.74" width="0.1524" layer="91"/>
<pinref part="P+12" gate="1" pin="+5V"/>
<wire x1="365.76" y1="205.74" x2="373.38" y2="205.74" width="0.1524" layer="91"/>
<wire x1="373.38" y1="205.74" x2="373.38" y2="215.9" width="0.1524" layer="91"/>
<pinref part="FB2" gate="G$1" pin="2"/>
<pinref part="R62" gate="G$1" pin="2"/>
<wire x1="317.5" y1="213.36" x2="365.76" y2="213.36" width="0.1524" layer="91"/>
<wire x1="365.76" y1="213.36" x2="365.76" y2="205.74" width="0.1524" layer="91"/>
<junction x="365.76" y="205.74"/>
</segment>
</net>
<net name="N$97" class="0">
<segment>
<pinref part="U7" gate="A" pin="DIR"/>
<pinref part="R53" gate="G$1" pin="1"/>
<wire x1="142.24" y1="81.28" x2="177.8" y2="81.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$98" class="0">
<segment>
<pinref part="U7" gate="A" pin="RXOUT"/>
<pinref part="R54" gate="G$1" pin="1"/>
<wire x1="142.24" y1="83.82" x2="177.8" y2="83.82" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$99" class="0">
<segment>
<pinref part="U7" gate="A" pin="TXIN"/>
<pinref part="R55" gate="G$1" pin="1"/>
<wire x1="142.24" y1="88.9" x2="177.8" y2="88.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$100" class="0">
<segment>
<pinref part="R55" gate="G$1" pin="2"/>
<wire x1="187.96" y1="88.9" x2="203.2" y2="88.9" width="0.1524" layer="91"/>
<pinref part="U8" gate="G$1" pin="1"/>
<wire x1="203.2" y1="88.9" x2="205.74" y2="88.9" width="0.1524" layer="91"/>
<wire x1="203.2" y1="89.916" x2="203.2" y2="88.9" width="0.1524" layer="91"/>
<junction x="203.2" y="88.9"/>
<pinref part="TP13" gate="G$1" pin="PP"/>
</segment>
</net>
<net name="N$101" class="0">
<segment>
<wire x1="78.74" y1="71.12" x2="106.68" y2="71.12" width="0.1524" layer="91"/>
<pinref part="R49" gate="G$1" pin="2"/>
<wire x1="78.74" y1="68.58" x2="78.74" y2="71.12" width="0.1524" layer="91"/>
<pinref part="R48" gate="G$1" pin="1"/>
<wire x1="81.28" y1="73.66" x2="78.74" y2="73.66" width="0.1524" layer="91"/>
<wire x1="78.74" y1="73.66" x2="78.74" y2="71.12" width="0.1524" layer="91"/>
<junction x="78.74" y="71.12"/>
<pinref part="U7" gate="A" pin="RES"/>
</segment>
</net>
<net name="N$102" class="1">
<segment>
<pinref part="R47" gate="G$1" pin="1"/>
<wire x1="81.28" y1="78.74" x2="78.74" y2="78.74" width="0.1524" layer="91"/>
<pinref part="C38" gate="G$1" pin="2"/>
<wire x1="78.74" y1="78.74" x2="50.8" y2="78.74" width="0.1524" layer="91"/>
<wire x1="50.8" y1="71.12" x2="50.8" y2="78.74" width="0.1524" layer="91"/>
<junction x="50.8" y="78.74"/>
<wire x1="50.8" y1="78.74" x2="35.56" y2="78.74" width="0.1524" layer="91"/>
<wire x1="78.74" y1="78.74" x2="78.74" y2="76.2" width="0.1524" layer="91"/>
<junction x="78.74" y="78.74"/>
<wire x1="35.56" y1="83.82" x2="35.56" y2="78.74" width="0.1524" layer="91"/>
<wire x1="33.02" y1="83.82" x2="35.56" y2="83.82" width="0.1524" layer="91"/>
<pinref part="TP11" gate="G$1" pin="PP"/>
<pinref part="D13" gate="G$1" pin="-"/>
<wire x1="35.56" y1="71.12" x2="35.56" y2="78.74" width="0.1524" layer="91"/>
<junction x="35.56" y="78.74"/>
<pinref part="R77" gate="G$1" pin="1"/>
<wire x1="81.28" y1="76.2" x2="78.74" y2="76.2" width="0.1524" layer="91"/>
<pinref part="C39" gate="G$1" pin="1"/>
<wire x1="35.56" y1="99.06" x2="35.56" y2="83.82" width="0.1524" layer="91"/>
<junction x="35.56" y="83.82"/>
</segment>
</net>
<net name="N$103" class="0">
<segment>
<wire x1="304.8" y1="88.9" x2="304.8" y2="124.46" width="0.1524" layer="91"/>
<pinref part="F17" gate="G$1" pin="1"/>
<wire x1="279.4" y1="124.46" x2="304.8" y2="124.46" width="0.1524" layer="91"/>
<pinref part="D12" gate="A" pin="-"/>
<wire x1="342.9" y1="124.46" x2="304.8" y2="124.46" width="0.1524" layer="91"/>
<junction x="304.8" y="124.46"/>
<pinref part="X2" gate="-7" pin="1"/>
</segment>
</net>
<net name="N$104" class="0">
<segment>
<pinref part="R60" gate="G$1" pin="2"/>
<wire x1="144.78" y1="66.04" x2="144.78" y2="91.44" width="0.1524" layer="91"/>
<pinref part="U7" gate="A" pin="SYNCOUT"/>
<wire x1="144.78" y1="91.44" x2="142.24" y2="91.44" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$105" class="0">
<segment>
<pinref part="U9" gate="A" pin="BOOT"/>
<pinref part="R64" gate="G$1" pin="1"/>
<wire x1="264.16" y1="203.2" x2="269.24" y2="203.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$106" class="0">
<segment>
<pinref part="U9" gate="A" pin="FB"/>
<wire x1="264.16" y1="198.12" x2="302.26" y2="198.12" width="0.1524" layer="91"/>
<wire x1="302.26" y1="198.12" x2="302.26" y2="213.36" width="0.1524" layer="91"/>
<pinref part="R61" gate="G$1" pin="1"/>
<pinref part="R62" gate="G$1" pin="1"/>
<wire x1="299.72" y1="213.36" x2="302.26" y2="213.36" width="0.1524" layer="91"/>
<wire x1="302.26" y1="213.36" x2="307.34" y2="213.36" width="0.1524" layer="91"/>
<junction x="302.26" y="213.36"/>
</segment>
</net>
<net name="N$107" class="0">
<segment>
<pinref part="U9" gate="A" pin="SS"/>
<pinref part="C52" gate="G$1" pin="2"/>
<wire x1="233.68" y1="190.5" x2="231.14" y2="190.5" width="0.1524" layer="91"/>
<wire x1="231.14" y1="190.5" x2="231.14" y2="187.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$108" class="0">
<segment>
<pinref part="U9" gate="A" pin="RT/SYNC"/>
<pinref part="R63" gate="G$1" pin="2"/>
<wire x1="233.68" y1="195.58" x2="223.52" y2="195.58" width="0.1524" layer="91"/>
<wire x1="223.52" y1="195.58" x2="223.52" y2="190.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$109" class="0">
<segment>
<pinref part="U9" gate="A" pin="VIN"/>
<wire x1="187.96" y1="205.74" x2="200.66" y2="205.74" width="0.1524" layer="91"/>
<wire x1="200.66" y1="205.74" x2="213.36" y2="205.74" width="0.1524" layer="91"/>
<wire x1="213.36" y1="205.74" x2="233.68" y2="205.74" width="0.1524" layer="91"/>
<pinref part="C53" gate="G$1" pin="2"/>
<wire x1="213.36" y1="193.04" x2="213.36" y2="205.74" width="0.1524" layer="91"/>
<junction x="213.36" y="205.74"/>
<pinref part="C58" gate="G$1" pin="2"/>
<wire x1="200.66" y1="193.04" x2="200.66" y2="205.74" width="0.1524" layer="91"/>
<junction x="200.66" y="205.74"/>
<pinref part="C54" gate="G$1" pin="2"/>
<wire x1="187.96" y1="193.04" x2="187.96" y2="205.74" width="0.1524" layer="91"/>
<pinref part="L6" gate="G$1" pin="2"/>
<wire x1="175.26" y1="205.74" x2="187.96" y2="205.74" width="0.1524" layer="91"/>
<junction x="187.96" y="205.74"/>
</segment>
</net>
<net name="N$110" class="0">
<segment>
<pinref part="FB2" gate="G$1" pin="1"/>
<pinref part="C51" gate="G$1" pin="2"/>
<wire x1="347.98" y1="203.2" x2="347.98" y2="205.74" width="0.1524" layer="91"/>
<junction x="347.98" y="205.74"/>
<wire x1="347.98" y1="205.74" x2="353.06" y2="205.74" width="0.1524" layer="91"/>
<wire x1="340.36" y1="205.74" x2="347.98" y2="205.74" width="0.1524" layer="91"/>
<pinref part="C50" gate="G$1" pin="2"/>
<wire x1="340.36" y1="203.2" x2="340.36" y2="205.74" width="0.1524" layer="91"/>
<junction x="340.36" y="205.74"/>
<wire x1="332.74" y1="205.74" x2="340.36" y2="205.74" width="0.1524" layer="91"/>
<pinref part="C49" gate="G$1" pin="2"/>
<wire x1="332.74" y1="203.2" x2="332.74" y2="205.74" width="0.1524" layer="91"/>
<pinref part="L5" gate="G$1" pin="2"/>
<wire x1="327.66" y1="205.74" x2="332.74" y2="205.74" width="0.1524" layer="91"/>
<junction x="332.74" y="205.74"/>
</segment>
</net>
<net name="N$111" class="0">
<segment>
<pinref part="R64" gate="G$1" pin="2"/>
<pinref part="C55" gate="G$1" pin="1"/>
<wire x1="279.4" y1="203.2" x2="281.94" y2="203.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$112" class="0">
<segment>
<pinref part="C55" gate="G$1" pin="2"/>
<pinref part="R65" gate="G$1" pin="2"/>
<wire x1="292.1" y1="203.2" x2="294.64" y2="203.2" width="0.1524" layer="91"/>
<wire x1="294.64" y1="203.2" x2="294.64" y2="195.58" width="0.1524" layer="91"/>
<pinref part="U9" gate="A" pin="SW"/>
<wire x1="264.16" y1="205.74" x2="294.64" y2="205.74" width="0.1524" layer="91"/>
<wire x1="294.64" y1="205.74" x2="309.88" y2="205.74" width="0.1524" layer="91"/>
<pinref part="D14" gate="A" pin="C"/>
<wire x1="309.88" y1="205.74" x2="317.5" y2="205.74" width="0.1524" layer="91"/>
<wire x1="309.88" y1="195.58" x2="309.88" y2="205.74" width="0.1524" layer="91"/>
<junction x="309.88" y="205.74"/>
<pinref part="L5" gate="G$1" pin="1"/>
<wire x1="294.64" y1="203.2" x2="294.64" y2="205.74" width="0.1524" layer="91"/>
<junction x="294.64" y="203.2"/>
<junction x="294.64" y="205.74"/>
</segment>
</net>
<net name="N$113" class="0">
<segment>
<pinref part="R65" gate="G$1" pin="1"/>
<pinref part="C56" gate="G$1" pin="2"/>
<wire x1="294.64" y1="185.42" x2="294.64" y2="182.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$114" class="0">
<segment>
<pinref part="R67" gate="G$1" pin="2"/>
<wire x1="124.46" y1="198.12" x2="124.46" y2="205.74" width="0.1524" layer="91"/>
<pinref part="R66" gate="G$1" pin="2"/>
<wire x1="124.46" y1="205.74" x2="119.38" y2="205.74" width="0.1524" layer="91"/>
<pinref part="Q2" gate="G$1" pin="D"/>
<wire x1="127" y1="205.74" x2="124.46" y2="205.74" width="0.1524" layer="91"/>
<junction x="124.46" y="205.74"/>
</segment>
</net>
<net name="N$115" class="0">
<segment>
<pinref part="R67" gate="G$1" pin="1"/>
<wire x1="124.46" y1="187.96" x2="124.46" y2="185.42" width="0.1524" layer="91"/>
<pinref part="R68" gate="G$1" pin="1"/>
<wire x1="124.46" y1="185.42" x2="119.38" y2="185.42" width="0.1524" layer="91"/>
<pinref part="U10" gate="G$1" pin="9_SENSE"/>
<wire x1="129.54" y1="180.34" x2="129.54" y2="182.88" width="0.1524" layer="91"/>
<junction x="124.46" y="185.42"/>
<wire x1="129.54" y1="182.88" x2="124.46" y2="182.88" width="0.1524" layer="91"/>
<wire x1="124.46" y1="182.88" x2="124.46" y2="185.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$116" class="0">
<segment>
<pinref part="R68" gate="G$1" pin="2"/>
<wire x1="109.22" y1="185.42" x2="104.14" y2="185.42" width="0.1524" layer="91"/>
<wire x1="104.14" y1="185.42" x2="104.14" y2="205.74" width="0.1524" layer="91"/>
<pinref part="R66" gate="G$1" pin="1"/>
<wire x1="104.14" y1="205.74" x2="109.22" y2="205.74" width="0.1524" layer="91"/>
<pinref part="U10" gate="G$1" pin="10_VCC"/>
<wire x1="121.92" y1="175.26" x2="104.14" y2="175.26" width="0.1524" layer="91"/>
<wire x1="104.14" y1="175.26" x2="104.14" y2="185.42" width="0.1524" layer="91"/>
<junction x="104.14" y="185.42"/>
<pinref part="C48" gate="G$1" pin="2"/>
<wire x1="162.56" y1="124.46" x2="88.9" y2="124.46" width="0.1524" layer="91"/>
<wire x1="78.74" y1="195.58" x2="78.74" y2="205.74" width="0.1524" layer="91"/>
<pinref part="D15" gate="G$1" pin="-"/>
<wire x1="71.12" y1="195.58" x2="71.12" y2="205.74" width="0.1524" layer="91"/>
<pinref part="C46" gate="G$1" pin="2"/>
<wire x1="53.34" y1="195.58" x2="53.34" y2="205.74" width="0.1524" layer="91"/>
<junction x="53.34" y="205.74"/>
<pinref part="J4" gate="G$1" pin="1"/>
<pinref part="F16" gate="G$1" pin="1"/>
<wire x1="38.1" y1="195.58" x2="38.1" y2="205.74" width="0.1524" layer="91"/>
<junction x="38.1" y="205.74"/>
<wire x1="38.1" y1="205.74" x2="12.7" y2="205.74" width="0.1524" layer="91"/>
<wire x1="38.1" y1="205.74" x2="53.34" y2="205.74" width="0.1524" layer="91"/>
<wire x1="53.34" y1="205.74" x2="71.12" y2="205.74" width="0.1524" layer="91"/>
<junction x="71.12" y="205.74"/>
<wire x1="71.12" y1="205.74" x2="78.74" y2="205.74" width="0.1524" layer="91"/>
<junction x="78.74" y="205.74"/>
<wire x1="78.74" y1="205.74" x2="88.9" y2="205.74" width="0.1524" layer="91"/>
<wire x1="88.9" y1="124.46" x2="88.9" y2="205.74" width="0.1524" layer="91"/>
<wire x1="104.14" y1="205.74" x2="99.06" y2="205.74" width="0.1524" layer="91"/>
<junction x="104.14" y="205.74"/>
<junction x="88.9" y="205.74"/>
<pinref part="R71" gate="G$1" pin="2"/>
<wire x1="99.06" y1="205.74" x2="88.9" y2="205.74" width="0.1524" layer="91"/>
<wire x1="99.06" y1="198.12" x2="99.06" y2="205.74" width="0.1524" layer="91"/>
<junction x="99.06" y="205.74"/>
<pinref part="L7" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$117" class="0">
<segment>
<pinref part="U10" gate="G$1" pin="2_VREF"/>
<pinref part="R69" gate="G$1" pin="2"/>
<wire x1="121.92" y1="170.18" x2="119.38" y2="170.18" width="0.1524" layer="91"/>
<wire x1="119.38" y1="170.18" x2="119.38" y2="167.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$118" class="0">
<segment>
<pinref part="R69" gate="G$1" pin="1"/>
<pinref part="R70" gate="G$1" pin="2"/>
<wire x1="119.38" y1="157.48" x2="119.38" y2="154.94" width="0.1524" layer="91"/>
<pinref part="U10" gate="G$1" pin="3_PROG"/>
<wire x1="119.38" y1="154.94" x2="119.38" y2="152.4" width="0.1524" layer="91"/>
<wire x1="129.54" y1="165.1" x2="129.54" y2="154.94" width="0.1524" layer="91"/>
<wire x1="129.54" y1="154.94" x2="119.38" y2="154.94" width="0.1524" layer="91"/>
<junction x="119.38" y="154.94"/>
</segment>
</net>
<net name="N$119" class="0">
<segment>
<pinref part="U10" gate="G$1" pin="1_EN"/>
<wire x1="121.92" y1="172.72" x2="99.06" y2="172.72" width="0.1524" layer="91"/>
<pinref part="R72" gate="G$1" pin="2"/>
<wire x1="99.06" y1="167.64" x2="99.06" y2="172.72" width="0.1524" layer="91"/>
<pinref part="R71" gate="G$1" pin="1"/>
<wire x1="99.06" y1="187.96" x2="99.06" y2="172.72" width="0.1524" layer="91"/>
<junction x="99.06" y="172.72"/>
</segment>
</net>
<net name="N$120" class="0">
<segment>
<pinref part="R73" gate="G$1" pin="2"/>
<pinref part="Q2" gate="G$1" pin="G"/>
<wire x1="134.62" y1="198.12" x2="134.62" y2="200.66" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$121" class="0">
<segment>
<pinref part="U10" gate="G$1" pin="6_PG"/>
<pinref part="R74" gate="G$1" pin="1"/>
<wire x1="147.32" y1="175.26" x2="149.86" y2="175.26" width="0.1524" layer="91"/>
<wire x1="149.86" y1="175.26" x2="149.86" y2="187.96" width="0.1524" layer="91"/>
<wire x1="149.86" y1="175.26" x2="162.56" y2="175.26" width="0.1524" layer="91"/>
<junction x="149.86" y="175.26"/>
<pinref part="R76" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$122" class="0">
<segment>
<pinref part="R75" gate="G$1" pin="1"/>
<pinref part="C57" gate="G$1" pin="2"/>
<wire x1="152.4" y1="157.48" x2="152.4" y2="152.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$123" class="0">
<segment>
<pinref part="U10" gate="G$1" pin="4_TIMER"/>
<pinref part="C59" gate="G$1" pin="2"/>
<wire x1="134.62" y1="165.1" x2="134.62" y2="162.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$124" class="0">
<segment>
<pinref part="R76" gate="G$1" pin="1"/>
<wire x1="172.72" y1="175.26" x2="175.26" y2="175.26" width="0.1524" layer="91"/>
<wire x1="175.26" y1="175.26" x2="175.26" y2="200.66" width="0.1524" layer="91"/>
<pinref part="U9" gate="A" pin="EN"/>
<wire x1="175.26" y1="200.66" x2="233.68" y2="200.66" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$125" class="0">
<segment>
<pinref part="R75" gate="G$1" pin="2"/>
<wire x1="152.4" y1="167.64" x2="152.4" y2="182.88" width="0.1524" layer="91"/>
<pinref part="R73" gate="G$1" pin="1"/>
<pinref part="U10" gate="G$1" pin="8_GATE"/>
<wire x1="134.62" y1="187.96" x2="134.62" y2="182.88" width="0.1524" layer="91"/>
<wire x1="134.62" y1="182.88" x2="134.62" y2="180.34" width="0.1524" layer="91"/>
<wire x1="152.4" y1="182.88" x2="134.62" y2="182.88" width="0.1524" layer="91"/>
<junction x="134.62" y="182.88"/>
</segment>
</net>
<net name="N$126" class="0">
<segment>
<pinref part="R77" gate="G$1" pin="2"/>
<pinref part="U7" gate="A" pin="RXIN"/>
<wire x1="91.44" y1="76.2" x2="106.68" y2="76.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$127" class="1">
<segment>
<pinref part="C39" gate="G$1" pin="2"/>
<wire x1="35.56" y1="109.22" x2="35.56" y2="119.38" width="0.1524" layer="91"/>
<pinref part="R74" gate="G$1" pin="2"/>
<wire x1="149.86" y1="198.12" x2="149.86" y2="205.74" width="0.1524" layer="91"/>
<pinref part="Q2" gate="G$1" pin="S"/>
<wire x1="137.16" y1="205.74" x2="139.7" y2="205.74" width="0.1524" layer="91"/>
<wire x1="139.7" y1="205.74" x2="149.86" y2="205.74" width="0.1524" layer="91"/>
<junction x="139.7" y="205.74"/>
<pinref part="U10" gate="G$1" pin="7_OUT"/>
<wire x1="139.7" y1="180.34" x2="139.7" y2="205.74" width="0.1524" layer="91"/>
<pinref part="L6" gate="G$1" pin="1"/>
<wire x1="165.1" y1="205.74" x2="157.48" y2="205.74" width="0.1524" layer="91"/>
<junction x="149.86" y="205.74"/>
<wire x1="157.48" y1="205.74" x2="149.86" y2="205.74" width="0.1524" layer="91"/>
<wire x1="35.56" y1="119.38" x2="157.48" y2="119.38" width="0.1524" layer="91"/>
<wire x1="157.48" y1="119.38" x2="157.48" y2="205.74" width="0.1524" layer="91"/>
<junction x="157.48" y="205.74"/>
</segment>
</net>
<net name="N$128" class="0">
<segment>
<pinref part="D12" gate="A" pin="+"/>
<wire x1="269.24" y1="124.46" x2="172.72" y2="124.46" width="0.1524" layer="91"/>
<pinref part="L7" gate="G$1" pin="2"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
