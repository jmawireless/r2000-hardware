<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.2.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting keepoldvectorfont="yes"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="yes" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="5" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="no" active="no"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="no" active="no"/>
<layer number="103" name="tMap" color="7" fill="1" visible="no" active="no"/>
<layer number="104" name="Name" color="7" fill="1" visible="no" active="no"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="no" active="no"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="no" active="no"/>
<layer number="107" name="Crop" color="7" fill="1" visible="no" active="no"/>
<layer number="108" name="fp8" color="7" fill="1" visible="yes" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="yes" active="yes"/>
<layer number="110" name="110" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="111" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="7" fill="1" visible="yes" active="yes"/>
<layer number="114" name="FRNTMAAT1" color="7" fill="1" visible="no" active="no"/>
<layer number="115" name="FRNTMAAT2" color="7" fill="1" visible="no" active="no"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="no" active="no"/>
<layer number="117" name="BACKMAAT1" color="7" fill="1" visible="no" active="no"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="yes" active="yes"/>
<layer number="119" name="KAP_TEKEN" color="7" fill="1" visible="no" active="no"/>
<layer number="120" name="KAP_MAAT1" color="7" fill="1" visible="no" active="no"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="no" active="no"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="no" active="no"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="top_silk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="130" name="SMDSTROOK" color="7" fill="1" visible="no" active="no"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="133" name="bottom_silk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="DrillLegend" color="7" fill="1" visible="no" active="no"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="14" fill="1" visible="no" active="no"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="no"/>
<layer number="201" name="201bmp" color="2" fill="1" visible="no" active="no"/>
<layer number="202" name="202bmp" color="3" fill="1" visible="no" active="no"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="no" active="no"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="no" active="no"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="no" active="no"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="no" active="no"/>
<layer number="207" name="207bmp" color="15" fill="10" visible="no" active="no"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="no" active="no"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="Eagle3D_PG1" color="7" fill="1" visible="no" active="no"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="no"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="OrgLBR" color="13" fill="1" visible="no" active="no"/>
<layer number="255" name="Accent" color="7" fill="1" visible="no" active="no"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="frames">
<description>&lt;b&gt;Frames for Sheet and Layout&lt;/b&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="A3L-LOC_JMA">
<wire x1="288.29" y1="3.81" x2="342.265" y2="3.81" width="0.1016" layer="94"/>
<wire x1="342.265" y1="3.81" x2="373.38" y2="3.81" width="0.1016" layer="94"/>
<wire x1="373.38" y1="3.81" x2="383.54" y2="3.81" width="0.1016" layer="94"/>
<wire x1="383.54" y1="3.81" x2="383.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="383.54" y1="8.89" x2="383.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="383.54" y1="13.97" x2="383.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="383.54" y1="19.05" x2="383.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="288.29" y1="3.81" x2="288.29" y2="24.13" width="0.1016" layer="94"/>
<wire x1="288.29" y1="24.13" x2="342.265" y2="24.13" width="0.1016" layer="94"/>
<wire x1="342.265" y1="24.13" x2="383.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="373.38" y1="3.81" x2="373.38" y2="8.89" width="0.1016" layer="94"/>
<wire x1="373.38" y1="8.89" x2="383.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="373.38" y1="8.89" x2="342.265" y2="8.89" width="0.1016" layer="94"/>
<wire x1="342.265" y1="8.89" x2="342.265" y2="3.81" width="0.1016" layer="94"/>
<wire x1="342.265" y1="8.89" x2="342.265" y2="13.97" width="0.1016" layer="94"/>
<wire x1="342.265" y1="13.97" x2="383.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="342.265" y1="13.97" x2="342.265" y2="19.05" width="0.1016" layer="94"/>
<wire x1="342.265" y1="19.05" x2="383.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="342.265" y1="19.05" x2="342.265" y2="24.13" width="0.1016" layer="94"/>
<text x="344.17" y="15.24" size="2.54" layer="94">&gt;DRAWING_NAME</text>
<text x="344.17" y="10.16" size="2.286" layer="94">&gt;LAST_DATE_TIME</text>
<text x="357.505" y="5.08" size="2.54" layer="94">&gt;SHEET</text>
<text x="343.916" y="4.953" size="2.54" layer="94">Sheet:</text>
<frame x1="0" y1="0" x2="387.35" y2="260.35" columns="8" rows="5" layer="94"/>
<text x="292.1" y="15.24" size="2.54" layer="94" ratio="15">Proprietory Information
JMA WIRELESS </text>
<text x="344.424" y="20.32" size="2.54" layer="95" ratio="15">&gt;AUTHER</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="A3L-LOC_JMA">
<gates>
<gate name="G$1" symbol="A3L-LOC_JMA" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="JMA_LBR">
<packages>
<package name="JST_B8B-PHDSS">
<pad name="1" x="3" y="-1" drill="0.7"/>
<pad name="2" x="3" y="1" drill="0.7"/>
<pad name="3" x="1" y="-1" drill="0.7"/>
<pad name="4" x="1" y="1" drill="0.7"/>
<pad name="5" x="-1" y="-1" drill="0.7"/>
<pad name="6" x="-1" y="1" drill="0.7"/>
<pad name="7" x="-3" y="-1" drill="0.7"/>
<pad name="8" x="-3" y="1" drill="0.7"/>
<wire x1="-5.5" y1="3.5" x2="5.5" y2="3.5" width="0.127" layer="21"/>
<wire x1="5.5" y1="3.5" x2="5.5" y2="-3.5" width="0.127" layer="21"/>
<wire x1="5.5" y1="-3.5" x2="-5.5" y2="-3.5" width="0.127" layer="21"/>
<wire x1="-5.5" y1="-3.5" x2="-5.5" y2="3.5" width="0.127" layer="21"/>
<wire x1="-5.5" y1="3.5" x2="5.5" y2="3.5" width="0.127" layer="51"/>
<wire x1="5.5" y1="3.5" x2="5.5" y2="-3.5" width="0.127" layer="51"/>
<wire x1="5.5" y1="-3.5" x2="-5.5" y2="-3.5" width="0.127" layer="51"/>
<wire x1="-5.5" y1="-3.5" x2="-5.5" y2="3.5" width="0.127" layer="51"/>
<wire x1="4" y1="-4" x2="6" y2="-4" width="0.127" layer="51"/>
<wire x1="6" y1="-4" x2="6" y2="-2" width="0.127" layer="51"/>
<wire x1="4" y1="-4" x2="6" y2="-4" width="0.127" layer="21"/>
<wire x1="6" y1="-4" x2="6" y2="-2" width="0.127" layer="21"/>
<text x="6.35" y="2.54" size="0.8128" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="6.35" y="1.27" size="0.8128" layer="27" font="vector" ratio="15">&gt;VALUE</text>
</package>
<package name="JST_PHDR-08VS_FEMALE">
<text x="12" y="6" size="0.8128" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="12" y="5" size="0.8128" layer="27" font="vector" ratio="15">&gt;VALUE</text>
<circle x="0" y="-1.25" radius="12" width="0.1524" layer="21"/>
<wire x1="-3.426" y1="-12.827" x2="-3.426" y2="-15.184" width="0.1524" layer="21"/>
<wire x1="-3.426" y1="-15.184" x2="3.746" y2="-15.184" width="0.1524" layer="21"/>
<wire x1="3.746" y1="-15.184" x2="3.746" y2="-12.7" width="0.1524" layer="21"/>
<pad name="P$1" x="-3" y="-1" drill="0.7"/>
<pad name="P$2" x="-3" y="1" drill="0.7"/>
<pad name="P$3" x="-1" y="-1" drill="0.7"/>
<pad name="P$4" x="-1" y="1" drill="0.7"/>
<pad name="P$5" x="1" y="-1" drill="0.7"/>
<pad name="P$6" x="1" y="1" drill="0.7"/>
<pad name="P$7" x="3" y="-1" drill="0.7"/>
<pad name="P$8" x="3" y="1" drill="0.7"/>
</package>
</packages>
<symbols>
<symbol name="PIN">
<pin name="1" x="-5.08" y="0" visible="off"/>
<wire x1="-3.302" y1="0" x2="-1.27" y2="0" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="0" x2="2.54" y2="0" width="0.8128" layer="94"/>
<text x="-3.302" y="0.508" size="1.27" layer="95" ratio="15">&gt;NAME</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="JST_B8B-PHDSS" prefix="J" uservalue="yes">
<gates>
<gate name="-1" symbol="PIN" x="5.08" y="7.62"/>
<gate name="-2" symbol="PIN" x="5.08" y="5.08"/>
<gate name="-3" symbol="PIN" x="5.08" y="2.54"/>
<gate name="-4" symbol="PIN" x="5.08" y="0"/>
<gate name="-5" symbol="PIN" x="5.08" y="-2.54"/>
<gate name="-6" symbol="PIN" x="5.08" y="-5.08"/>
<gate name="-7" symbol="PIN" x="5.08" y="-7.62"/>
<gate name="-8" symbol="PIN" x="5.08" y="-10.16"/>
</gates>
<devices>
<device name="" package="JST_B8B-PHDSS">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-2" pin="1" pad="2"/>
<connect gate="-3" pin="1" pad="3"/>
<connect gate="-4" pin="1" pad="4"/>
<connect gate="-5" pin="1" pad="5"/>
<connect gate="-6" pin="1" pad="6"/>
<connect gate="-7" pin="1" pad="7"/>
<connect gate="-8" pin="1" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST_PHDR-08VS_FEMALE" package="JST_PHDR-08VS_FEMALE">
<connects>
<connect gate="-1" pin="1" pad="P$1"/>
<connect gate="-2" pin="1" pad="P$2"/>
<connect gate="-3" pin="1" pad="P$3"/>
<connect gate="-4" pin="1" pad="P$4"/>
<connect gate="-5" pin="1" pad="P$5"/>
<connect gate="-6" pin="1" pad="P$6"/>
<connect gate="-7" pin="1" pad="P$7"/>
<connect gate="-8" pin="1" pad="P$8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="con-samtec" urn="urn:adsk.eagle:library:184">
<description>&lt;b&gt;Samtec Connectors&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="TSW-104-XX-G-D" library_version="2">
<description>&lt;b&gt;THROUGH-HOLE .025" SQ POST HEADER&lt;/b&gt;&lt;p&gt;
Source: Samtec TSW.pdf</description>
<wire x1="-5.209" y1="2.425" x2="5.209" y2="2.425" width="0.2032" layer="21"/>
<wire x1="5.209" y1="2.425" x2="5.209" y2="-2.425" width="0.2032" layer="21"/>
<wire x1="5.209" y1="-2.425" x2="-5.209" y2="-2.425" width="0.2032" layer="21"/>
<wire x1="-5.209" y1="-2.425" x2="-5.209" y2="2.425" width="0.2032" layer="21"/>
<pad name="1" x="3.81" y="-1.27" drill="1" diameter="1.5" shape="octagon" rot="R180"/>
<pad name="2" x="3.81" y="1.27" drill="1" diameter="1.5" shape="octagon" rot="R180"/>
<pad name="3" x="1.27" y="-1.27" drill="1" diameter="1.5" shape="octagon" rot="R180"/>
<pad name="4" x="1.27" y="1.27" drill="1" diameter="1.5" shape="octagon" rot="R180"/>
<pad name="5" x="-1.27" y="-1.27" drill="1" diameter="1.5" shape="octagon" rot="R180"/>
<pad name="6" x="-1.27" y="1.27" drill="1" diameter="1.5" shape="octagon" rot="R180"/>
<pad name="7" x="-3.81" y="-1.27" drill="1" diameter="1.5" shape="octagon" rot="R180"/>
<pad name="8" x="-3.81" y="1.27" drill="1" diameter="1.5" shape="octagon" rot="R180"/>
<text x="3.602" y="-3.818" size="1.1" layer="21" font="vector" rot="SR0">1</text>
<text x="3.427" y="2.744" size="1.1" layer="21" font="vector" rot="SR0">2</text>
<text x="-5.715" y="-2.54" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="6.985" y="-2.54" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-4.16" y1="-1.62" x2="-3.46" y2="-0.92" layer="51"/>
<rectangle x1="-4.16" y1="0.92" x2="-3.46" y2="1.62" layer="51"/>
<rectangle x1="-1.62" y1="-1.62" x2="-0.92" y2="-0.92" layer="51"/>
<rectangle x1="-1.62" y1="0.92" x2="-0.92" y2="1.62" layer="51"/>
<rectangle x1="0.92" y1="-1.62" x2="1.62" y2="-0.92" layer="51"/>
<rectangle x1="0.92" y1="0.92" x2="1.62" y2="1.62" layer="51"/>
<rectangle x1="3.46" y1="-1.62" x2="4.16" y2="-0.92" layer="51"/>
<rectangle x1="3.46" y1="0.92" x2="4.16" y2="1.62" layer="51"/>
</package>
<package name="TSW-104-08-G-D-RA" library_version="2">
<description>&lt;b&gt;THROUGH-HOLE .025" SQ POST HEADER&lt;/b&gt;&lt;p&gt;
Source: Samtec TSW.pdf</description>
<wire x1="-5.209" y1="-2.046" x2="5.209" y2="-2.046" width="0.2032" layer="21"/>
<wire x1="5.209" y1="-2.046" x2="5.209" y2="-0.106" width="0.2032" layer="21"/>
<wire x1="5.209" y1="-0.106" x2="-5.209" y2="-0.106" width="0.2032" layer="21"/>
<wire x1="-5.209" y1="-0.106" x2="-5.209" y2="-2.046" width="0.2032" layer="21"/>
<pad name="1" x="3.81" y="1.524" drill="1" diameter="1.5" shape="octagon" rot="R180"/>
<pad name="2" x="3.81" y="4.064" drill="1" diameter="1.5" shape="octagon" rot="R180"/>
<pad name="3" x="1.27" y="1.524" drill="1" diameter="1.5" shape="octagon" rot="R180"/>
<pad name="4" x="1.27" y="4.064" drill="1" diameter="1.5" shape="octagon" rot="R180"/>
<pad name="5" x="-1.27" y="1.524" drill="1" diameter="1.5" shape="octagon" rot="R180"/>
<pad name="6" x="-1.27" y="4.064" drill="1" diameter="1.5" shape="octagon" rot="R180"/>
<pad name="7" x="-3.81" y="1.524" drill="1" diameter="1.5" shape="octagon" rot="R180"/>
<pad name="8" x="-3.81" y="4.064" drill="1" diameter="1.5" shape="octagon" rot="R180"/>
<text x="-5.715" y="-7.62" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="6.985" y="-7.62" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<text x="5.095" y="0.775" size="1.1" layer="21" font="vector" rot="SR0">1</text>
<text x="5.06" y="3.29" size="1.1" layer="21" font="vector" rot="SR0">2</text>
<rectangle x1="-4.064" y1="0" x2="-3.556" y2="4.318" layer="51"/>
<rectangle x1="-1.524" y1="0" x2="-1.016" y2="4.318" layer="51"/>
<rectangle x1="1.016" y1="0" x2="1.524" y2="4.318" layer="51"/>
<rectangle x1="3.556" y1="0" x2="4.064" y2="4.318" layer="51"/>
<rectangle x1="-4.064" y1="-7.89" x2="-3.556" y2="-2.04" layer="21"/>
<rectangle x1="-1.524" y1="-7.89" x2="-1.016" y2="-2.04" layer="21"/>
<rectangle x1="1.016" y1="-7.89" x2="1.524" y2="-2.04" layer="21"/>
<rectangle x1="3.556" y1="-7.89" x2="4.064" y2="-2.04" layer="21"/>
</package>
<package name="TSW-104-XX-G-Q" library_version="2">
<description>&lt;b&gt;THROUGH-HOLE .025" SQ POST HEADER&lt;/b&gt;&lt;p&gt;
Source: Samtec TSW.pdf</description>
<wire x1="-5.209" y1="3.695" x2="5.209" y2="3.695" width="0.2032" layer="21"/>
<wire x1="5.209" y1="3.695" x2="5.209" y2="-3.695" width="0.2032" layer="21"/>
<wire x1="5.209" y1="-3.695" x2="-5.209" y2="-3.695" width="0.2032" layer="21"/>
<wire x1="-5.209" y1="-3.695" x2="-5.209" y2="3.695" width="0.2032" layer="21"/>
<pad name="1" x="3.81" y="-2.54" drill="1" diameter="1.5" shape="octagon" rot="R180"/>
<pad name="2" x="3.81" y="2.54" drill="1" diameter="1.5" shape="octagon" rot="R180"/>
<pad name="3" x="1.27" y="-2.54" drill="1" diameter="1.5" shape="octagon" rot="R180"/>
<pad name="4" x="1.27" y="2.54" drill="1" diameter="1.5" shape="octagon" rot="R180"/>
<pad name="5" x="-1.27" y="-2.54" drill="1" diameter="1.5" shape="octagon" rot="R180"/>
<pad name="6" x="-1.27" y="2.54" drill="1" diameter="1.5" shape="octagon" rot="R180"/>
<pad name="7" x="-3.81" y="-2.54" drill="1" diameter="1.5" shape="octagon" rot="R180"/>
<pad name="8" x="-3.81" y="2.54" drill="1" diameter="1.5" shape="octagon" rot="R180"/>
<text x="3.652" y="-5.138" size="1.1" layer="21" font="vector" rot="SR0">1</text>
<text x="3.552" y="3.989" size="1.1" layer="21" font="vector" rot="SR0">2</text>
<text x="-5.715" y="-3.81" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="6.985" y="-3.81" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-4.16" y1="-2.89" x2="-3.46" y2="-2.19" layer="51"/>
<rectangle x1="-4.16" y1="2.19" x2="-3.46" y2="2.89" layer="51"/>
<rectangle x1="-1.62" y1="-2.89" x2="-0.92" y2="-2.19" layer="51"/>
<rectangle x1="-1.62" y1="2.19" x2="-0.92" y2="2.89" layer="51"/>
<rectangle x1="0.92" y1="-2.89" x2="1.62" y2="-2.19" layer="51"/>
<rectangle x1="0.92" y1="2.19" x2="1.62" y2="2.89" layer="51"/>
<rectangle x1="3.46" y1="-2.89" x2="4.16" y2="-2.19" layer="51"/>
<rectangle x1="3.46" y1="2.19" x2="4.16" y2="2.89" layer="51"/>
</package>
<package name="TSW-104-08-G-Q-RA" library_version="2">
<description>&lt;b&gt;THROUGH-HOLE .025" SQ POST HEADER&lt;/b&gt;&lt;p&gt;
Source: Samtec TSW.pdf</description>
<wire x1="-5.209" y1="-2.046" x2="5.209" y2="-2.046" width="0.2032" layer="21"/>
<wire x1="5.209" y1="-2.046" x2="5.209" y2="-0.106" width="0.2032" layer="21"/>
<wire x1="5.209" y1="-0.106" x2="-5.209" y2="-0.106" width="0.2032" layer="21"/>
<wire x1="-5.209" y1="-0.106" x2="-5.209" y2="-2.046" width="0.2032" layer="21"/>
<pad name="1" x="3.81" y="1.524" drill="1" diameter="1.5" shape="octagon" rot="R180"/>
<pad name="2" x="3.81" y="6.604" drill="1" diameter="1.5" shape="octagon" rot="R180"/>
<pad name="3" x="1.27" y="1.524" drill="1" diameter="1.5" shape="octagon" rot="R180"/>
<pad name="4" x="1.27" y="6.604" drill="1" diameter="1.5" shape="octagon" rot="R180"/>
<pad name="5" x="-1.27" y="1.524" drill="1" diameter="1.5" shape="octagon" rot="R180"/>
<pad name="6" x="-1.27" y="6.604" drill="1" diameter="1.5" shape="octagon" rot="R180"/>
<pad name="7" x="-3.81" y="1.524" drill="1" diameter="1.5" shape="octagon" rot="R180"/>
<pad name="8" x="-3.81" y="6.604" drill="1" diameter="1.5" shape="octagon" rot="R180"/>
<text x="5.17" y="1.1" size="1.1" layer="21" font="vector" rot="SR0">1</text>
<text x="5.11" y="6.165" size="1.1" layer="21" font="vector" rot="SR0">2</text>
<text x="-5.715" y="-7.62" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="6.985" y="-7.62" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-4.064" y1="0" x2="-3.556" y2="6.858" layer="51"/>
<rectangle x1="-1.524" y1="0" x2="-1.016" y2="6.858" layer="51"/>
<rectangle x1="1.016" y1="0" x2="1.524" y2="6.858" layer="51"/>
<rectangle x1="3.556" y1="0" x2="4.064" y2="6.858" layer="51"/>
<rectangle x1="-4.064" y1="-7.89" x2="-3.556" y2="-2.04" layer="21"/>
<rectangle x1="-1.524" y1="-7.89" x2="-1.016" y2="-2.04" layer="21"/>
<rectangle x1="1.016" y1="-7.89" x2="1.524" y2="-2.04" layer="21"/>
<rectangle x1="3.556" y1="-7.89" x2="4.064" y2="-2.04" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="MPINV" library_version="2">
<text x="-1.27" y="1.27" size="1.778" layer="96">&gt;VALUE</text>
<text x="2.54" y="-0.762" size="1.524" layer="95">&gt;NAME</text>
<rectangle x1="0" y1="-0.254" x2="1.778" y2="0.254" layer="94"/>
<pin name="1" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
</symbol>
<symbol name="MPIN" library_version="2">
<text x="2.54" y="-0.762" size="1.524" layer="95">&gt;NAME</text>
<rectangle x1="0" y1="-0.254" x2="1.778" y2="0.254" layer="94"/>
<pin name="1" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="TSW-104-*-G" prefix="X" library_version="2">
<description>&lt;b&gt;THROUGH-HOLE .025" SQ POST HEADER&lt;/b&gt;&lt;p&gt;
Source: Samtec TSW.pdf</description>
<gates>
<gate name="-1" symbol="MPINV" x="-7.62" y="2.54" addlevel="always"/>
<gate name="-2" symbol="MPIN" x="10.16" y="2.54" addlevel="always"/>
<gate name="-3" symbol="MPIN" x="-7.62" y="0" addlevel="always"/>
<gate name="-4" symbol="MPIN" x="10.16" y="0" addlevel="always"/>
<gate name="-5" symbol="MPIN" x="-7.62" y="-2.54" addlevel="always"/>
<gate name="-6" symbol="MPIN" x="10.16" y="-2.54" addlevel="always"/>
<gate name="-7" symbol="MPIN" x="-7.62" y="-5.08" addlevel="always"/>
<gate name="-8" symbol="MPIN" x="10.16" y="-5.08" addlevel="always"/>
</gates>
<devices>
<device name="-D" package="TSW-104-XX-G-D">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-2" pin="1" pad="2"/>
<connect gate="-3" pin="1" pad="3"/>
<connect gate="-4" pin="1" pad="4"/>
<connect gate="-5" pin="1" pad="5"/>
<connect gate="-6" pin="1" pad="6"/>
<connect gate="-7" pin="1" pad="7"/>
<connect gate="-8" pin="1" pad="8"/>
</connects>
<technologies>
<technology name="07">
<attribute name="MF" value="Samtec Inc." constant="no"/>
<attribute name="MPN" value="TSW-104-07-G-D" constant="no"/>
<attribute name="OC_FARNELL" value="" constant="no"/>
<attribute name="OC_NEWARK" value="" constant="no"/>
</technology>
<technology name="08">
<attribute name="MF" value="Samtec Inc." constant="no"/>
<attribute name="MPN" value="TSW-104-07-G-D" constant="no"/>
<attribute name="OC_FARNELL" value="" constant="no"/>
<attribute name="OC_NEWARK" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="-D-RA" package="TSW-104-08-G-D-RA">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-2" pin="1" pad="2"/>
<connect gate="-3" pin="1" pad="3"/>
<connect gate="-4" pin="1" pad="4"/>
<connect gate="-5" pin="1" pad="5"/>
<connect gate="-6" pin="1" pad="6"/>
<connect gate="-7" pin="1" pad="7"/>
<connect gate="-8" pin="1" pad="8"/>
</connects>
<technologies>
<technology name="08">
<attribute name="MF" value="Samtec Inc." constant="no"/>
<attribute name="MPN" value="TSW-104-08-G-D-RA" constant="no"/>
</technology>
</technologies>
</device>
<device name="-Q" package="TSW-104-XX-G-Q">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-2" pin="1" pad="2"/>
<connect gate="-3" pin="1" pad="3"/>
<connect gate="-4" pin="1" pad="4"/>
<connect gate="-5" pin="1" pad="5"/>
<connect gate="-6" pin="1" pad="6"/>
<connect gate="-7" pin="1" pad="7"/>
<connect gate="-8" pin="1" pad="8"/>
</connects>
<technologies>
<technology name="07">
<attribute name="MF" value="Samtec Inc." constant="no"/>
<attribute name="MPN" value="TSW-104-07-G-Q" constant="no"/>
</technology>
<technology name="08">
<attribute name="MF" value="Samtec Inc." constant="no"/>
<attribute name="MPN" value="TSW-104-07-G-Q" constant="no"/>
</technology>
</technologies>
</device>
<device name="-Q-RA" package="TSW-104-08-G-Q-RA">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-2" pin="1" pad="2"/>
<connect gate="-3" pin="1" pad="3"/>
<connect gate="-4" pin="1" pad="4"/>
<connect gate="-5" pin="1" pad="5"/>
<connect gate="-6" pin="1" pad="6"/>
<connect gate="-7" pin="1" pad="7"/>
<connect gate="-8" pin="1" pad="8"/>
</connects>
<technologies>
<technology name="08">
<attribute name="MF" value="Samtec Inc." constant="no"/>
<attribute name="MPN" value="TSW-104-08-G-Q-RA" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="U$2" library="frames" deviceset="A3L-LOC_JMA" device=""/>
<part name="J1" library="JMA_LBR" deviceset="JST_B8B-PHDSS" device="JST_PHDR-08VS_FEMALE" value="JST_PHDR-08VS"/>
<part name="J2" library="JMA_LBR" deviceset="JST_B8B-PHDSS" device="JST_PHDR-08VS_FEMALE" value="JST_PHDR-08VS"/>
<part name="J3" library="JMA_LBR" deviceset="JST_B8B-PHDSS" device="JST_PHDR-08VS_FEMALE" value="JST_PHDR-08VS"/>
<part name="J4" library="JMA_LBR" deviceset="JST_B8B-PHDSS" device="JST_PHDR-08VS_FEMALE" value="JST_PHDR-08VS"/>
<part name="X1" library="con-samtec" library_urn="urn:adsk.eagle:library:184" deviceset="TSW-104-*-G" device="-D-RA" technology="08"/>
<part name="X2" library="con-samtec" library_urn="urn:adsk.eagle:library:184" deviceset="TSW-104-*-G" device="-D-RA" technology="08"/>
<part name="X3" library="con-samtec" library_urn="urn:adsk.eagle:library:184" deviceset="TSW-104-*-G" device="-D-RA" technology="08"/>
<part name="X4" library="con-samtec" library_urn="urn:adsk.eagle:library:184" deviceset="TSW-104-*-G" device="-D-RA" technology="08"/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="U$2" gate="G$1" x="0" y="0" smashed="yes">
<attribute name="DRAWING_NAME" x="344.17" y="15.24" size="2.54" layer="94"/>
<attribute name="LAST_DATE_TIME" x="344.17" y="10.16" size="2.286" layer="94"/>
<attribute name="SHEET" x="357.505" y="5.08" size="2.54" layer="94"/>
</instance>
<instance part="J1" gate="-1" x="147.32" y="157.48" smashed="yes" rot="MR0">
<attribute name="NAME" x="150.622" y="157.988" size="1.016" layer="95" font="vector" rot="MR0"/>
</instance>
<instance part="J1" gate="-2" x="147.32" y="154.94" smashed="yes" rot="MR0">
<attribute name="NAME" x="150.622" y="155.448" size="1.016" layer="95" font="vector" rot="MR0"/>
</instance>
<instance part="J1" gate="-3" x="147.32" y="152.4" smashed="yes" rot="MR0">
<attribute name="NAME" x="150.622" y="152.908" size="1.016" layer="95" font="vector" rot="MR0"/>
</instance>
<instance part="J1" gate="-4" x="147.32" y="149.86" smashed="yes" rot="MR0">
<attribute name="NAME" x="150.622" y="150.368" size="1.016" layer="95" font="vector" rot="MR0"/>
</instance>
<instance part="J1" gate="-5" x="147.32" y="147.32" smashed="yes" rot="MR0">
<attribute name="NAME" x="150.622" y="147.828" size="1.016" layer="95" font="vector" rot="MR0"/>
</instance>
<instance part="J1" gate="-6" x="147.32" y="144.78" smashed="yes" rot="MR0">
<attribute name="NAME" x="150.622" y="145.288" size="1.016" layer="95" font="vector" rot="MR0"/>
</instance>
<instance part="J1" gate="-7" x="147.32" y="142.24" smashed="yes" rot="MR0">
<attribute name="NAME" x="150.622" y="142.748" size="1.016" layer="95" font="vector" rot="MR0"/>
</instance>
<instance part="J1" gate="-8" x="147.32" y="139.7" smashed="yes" rot="MR0">
<attribute name="NAME" x="150.622" y="140.208" size="1.016" layer="95" font="vector" rot="MR0"/>
</instance>
<instance part="J2" gate="-1" x="193.04" y="157.48" smashed="yes" rot="R180">
<attribute name="NAME" x="196.342" y="156.972" size="1.016" layer="95" font="vector" rot="R180"/>
</instance>
<instance part="J2" gate="-2" x="193.04" y="154.94" smashed="yes" rot="R180">
<attribute name="NAME" x="196.342" y="154.432" size="1.016" layer="95" font="vector" rot="R180"/>
</instance>
<instance part="J2" gate="-3" x="193.04" y="152.4" smashed="yes" rot="R180">
<attribute name="NAME" x="196.342" y="151.892" size="1.016" layer="95" font="vector" rot="R180"/>
</instance>
<instance part="J2" gate="-4" x="193.04" y="149.86" smashed="yes" rot="R180">
<attribute name="NAME" x="196.342" y="149.352" size="1.016" layer="95" font="vector" rot="R180"/>
</instance>
<instance part="J2" gate="-5" x="193.04" y="147.32" smashed="yes" rot="R180">
<attribute name="NAME" x="196.342" y="146.812" size="1.016" layer="95" font="vector" rot="R180"/>
</instance>
<instance part="J2" gate="-6" x="193.04" y="144.78" smashed="yes" rot="R180">
<attribute name="NAME" x="196.342" y="144.272" size="1.016" layer="95" font="vector" rot="R180"/>
</instance>
<instance part="J2" gate="-7" x="193.04" y="142.24" smashed="yes" rot="R180">
<attribute name="NAME" x="196.342" y="141.732" size="1.016" layer="95" font="vector" rot="R180"/>
</instance>
<instance part="J2" gate="-8" x="193.04" y="139.7" smashed="yes" rot="R180">
<attribute name="NAME" x="196.342" y="139.192" size="1.016" layer="95" font="vector" rot="R180"/>
</instance>
<instance part="J3" gate="-1" x="147.32" y="124.46" smashed="yes" rot="R180">
<attribute name="NAME" x="150.622" y="123.952" size="1.016" layer="95" font="vector" rot="R180"/>
</instance>
<instance part="J3" gate="-2" x="147.32" y="121.92" smashed="yes" rot="R180">
<attribute name="NAME" x="150.622" y="121.412" size="1.016" layer="95" font="vector" rot="R180"/>
</instance>
<instance part="J3" gate="-3" x="147.32" y="119.38" smashed="yes" rot="R180">
<attribute name="NAME" x="150.622" y="118.872" size="1.016" layer="95" font="vector" rot="R180"/>
</instance>
<instance part="J3" gate="-4" x="147.32" y="116.84" smashed="yes" rot="R180">
<attribute name="NAME" x="150.622" y="116.332" size="1.016" layer="95" font="vector" rot="R180"/>
</instance>
<instance part="J3" gate="-5" x="147.32" y="114.3" smashed="yes" rot="R180">
<attribute name="NAME" x="150.622" y="113.792" size="1.016" layer="95" font="vector" rot="R180"/>
</instance>
<instance part="J3" gate="-6" x="147.32" y="111.76" smashed="yes" rot="R180">
<attribute name="NAME" x="150.622" y="111.252" size="1.016" layer="95" font="vector" rot="R180"/>
</instance>
<instance part="J3" gate="-7" x="147.32" y="109.22" smashed="yes" rot="R180">
<attribute name="NAME" x="150.622" y="108.712" size="1.016" layer="95" font="vector" rot="R180"/>
</instance>
<instance part="J3" gate="-8" x="147.32" y="106.68" smashed="yes" rot="R180">
<attribute name="NAME" x="150.622" y="106.172" size="1.016" layer="95" font="vector" rot="R180"/>
</instance>
<instance part="J4" gate="-1" x="190.5" y="124.46" smashed="yes" rot="R180">
<attribute name="NAME" x="193.802" y="123.952" size="1.016" layer="95" font="vector" rot="R180"/>
</instance>
<instance part="J4" gate="-2" x="190.5" y="121.92" smashed="yes" rot="R180">
<attribute name="NAME" x="193.802" y="121.412" size="1.016" layer="95" font="vector" rot="R180"/>
</instance>
<instance part="J4" gate="-3" x="190.5" y="119.38" smashed="yes" rot="R180">
<attribute name="NAME" x="193.802" y="118.872" size="1.016" layer="95" font="vector" rot="R180"/>
</instance>
<instance part="J4" gate="-4" x="190.5" y="116.84" smashed="yes" rot="R180">
<attribute name="NAME" x="193.802" y="116.332" size="1.016" layer="95" font="vector" rot="R180"/>
</instance>
<instance part="J4" gate="-5" x="190.5" y="114.3" smashed="yes" rot="R180">
<attribute name="NAME" x="193.802" y="113.792" size="1.016" layer="95" font="vector" rot="R180"/>
</instance>
<instance part="J4" gate="-6" x="190.5" y="111.76" smashed="yes" rot="R180">
<attribute name="NAME" x="193.802" y="111.252" size="1.016" layer="95" font="vector" rot="R180"/>
</instance>
<instance part="J4" gate="-7" x="190.5" y="109.22" smashed="yes" rot="R180">
<attribute name="NAME" x="193.802" y="108.712" size="1.016" layer="95" font="vector" rot="R180"/>
</instance>
<instance part="J4" gate="-8" x="190.5" y="106.68" smashed="yes" rot="R180">
<attribute name="NAME" x="193.802" y="106.172" size="1.016" layer="95" font="vector" rot="R180"/>
</instance>
<instance part="X1" gate="-1" x="160.02" y="157.48" smashed="yes">
<attribute name="VALUE" x="158.75" y="158.75" size="1.016" layer="96" font="vector"/>
<attribute name="NAME" x="162.56" y="156.718" size="1.016" layer="95" font="vector"/>
</instance>
<instance part="X1" gate="-2" x="160.02" y="147.32" smashed="yes">
<attribute name="NAME" x="162.56" y="146.558" size="1.016" layer="95" font="vector"/>
</instance>
<instance part="X1" gate="-3" x="160.02" y="154.94" smashed="yes">
<attribute name="NAME" x="162.56" y="154.178" size="1.016" layer="95" font="vector"/>
</instance>
<instance part="X1" gate="-4" x="160.02" y="144.78" smashed="yes">
<attribute name="NAME" x="162.56" y="144.018" size="1.016" layer="95" font="vector"/>
</instance>
<instance part="X1" gate="-5" x="160.02" y="152.4" smashed="yes">
<attribute name="NAME" x="162.56" y="151.638" size="1.016" layer="95" font="vector"/>
</instance>
<instance part="X1" gate="-6" x="160.02" y="142.24" smashed="yes">
<attribute name="NAME" x="162.56" y="141.478" size="1.016" layer="95" font="vector"/>
</instance>
<instance part="X1" gate="-7" x="160.02" y="149.86" smashed="yes">
<attribute name="NAME" x="162.56" y="149.098" size="1.016" layer="95" font="vector"/>
</instance>
<instance part="X1" gate="-8" x="160.02" y="139.7" smashed="yes">
<attribute name="NAME" x="162.56" y="138.938" size="1.016" layer="95" font="vector"/>
</instance>
<instance part="X2" gate="-1" x="208.28" y="157.48" smashed="yes">
<attribute name="VALUE" x="207.01" y="158.75" size="1.016" layer="96" font="vector"/>
<attribute name="NAME" x="210.82" y="156.718" size="1.016" layer="95" font="vector"/>
</instance>
<instance part="X2" gate="-2" x="208.28" y="147.32" smashed="yes">
<attribute name="NAME" x="210.82" y="146.558" size="1.016" layer="95" font="vector"/>
</instance>
<instance part="X2" gate="-3" x="208.28" y="154.94" smashed="yes">
<attribute name="NAME" x="210.82" y="154.178" size="1.016" layer="95" font="vector"/>
</instance>
<instance part="X2" gate="-4" x="208.28" y="144.78" smashed="yes">
<attribute name="NAME" x="210.82" y="144.018" size="1.016" layer="95" font="vector"/>
</instance>
<instance part="X2" gate="-5" x="208.28" y="152.4" smashed="yes">
<attribute name="NAME" x="210.82" y="151.638" size="1.016" layer="95" font="vector"/>
</instance>
<instance part="X2" gate="-6" x="208.28" y="142.24" smashed="yes">
<attribute name="NAME" x="210.82" y="141.478" size="1.016" layer="95" font="vector"/>
</instance>
<instance part="X2" gate="-7" x="208.28" y="149.86" smashed="yes">
<attribute name="NAME" x="210.82" y="149.098" size="1.016" layer="95" font="vector"/>
</instance>
<instance part="X2" gate="-8" x="208.28" y="139.7" smashed="yes">
<attribute name="NAME" x="210.82" y="138.938" size="1.016" layer="95" font="vector"/>
</instance>
<instance part="X3" gate="-1" x="162.56" y="124.46" smashed="yes">
<attribute name="VALUE" x="161.29" y="125.73" size="1.016" layer="96" font="vector"/>
<attribute name="NAME" x="165.1" y="123.698" size="1.016" layer="95" font="vector"/>
</instance>
<instance part="X3" gate="-2" x="162.56" y="114.3" smashed="yes">
<attribute name="NAME" x="165.1" y="113.538" size="1.016" layer="95" font="vector"/>
</instance>
<instance part="X3" gate="-3" x="162.56" y="121.92" smashed="yes">
<attribute name="NAME" x="165.1" y="121.158" size="1.016" layer="95" font="vector"/>
</instance>
<instance part="X3" gate="-4" x="162.56" y="111.76" smashed="yes">
<attribute name="NAME" x="165.1" y="110.998" size="1.016" layer="95" font="vector"/>
</instance>
<instance part="X3" gate="-5" x="162.56" y="119.38" smashed="yes">
<attribute name="NAME" x="165.1" y="118.618" size="1.016" layer="95" font="vector"/>
</instance>
<instance part="X3" gate="-6" x="162.56" y="109.22" smashed="yes">
<attribute name="NAME" x="165.1" y="108.458" size="1.016" layer="95" font="vector"/>
</instance>
<instance part="X3" gate="-7" x="162.56" y="116.84" smashed="yes">
<attribute name="NAME" x="165.1" y="116.078" size="1.016" layer="95" font="vector"/>
</instance>
<instance part="X3" gate="-8" x="162.56" y="106.68" smashed="yes">
<attribute name="NAME" x="165.1" y="105.918" size="1.016" layer="95" font="vector"/>
</instance>
<instance part="X4" gate="-1" x="205.74" y="124.46" smashed="yes">
<attribute name="VALUE" x="204.47" y="125.73" size="1.016" layer="96" font="vector"/>
<attribute name="NAME" x="208.28" y="123.698" size="1.016" layer="95" font="vector"/>
</instance>
<instance part="X4" gate="-2" x="205.74" y="114.3" smashed="yes">
<attribute name="NAME" x="208.28" y="113.538" size="1.016" layer="95" font="vector"/>
</instance>
<instance part="X4" gate="-3" x="205.74" y="121.92" smashed="yes">
<attribute name="NAME" x="208.28" y="121.158" size="1.016" layer="95" font="vector"/>
</instance>
<instance part="X4" gate="-4" x="205.74" y="111.76" smashed="yes">
<attribute name="NAME" x="208.28" y="110.998" size="1.016" layer="95" font="vector"/>
</instance>
<instance part="X4" gate="-5" x="205.74" y="119.38" smashed="yes">
<attribute name="NAME" x="208.28" y="118.618" size="1.016" layer="95" font="vector"/>
</instance>
<instance part="X4" gate="-6" x="205.74" y="109.22" smashed="yes">
<attribute name="NAME" x="208.28" y="108.458" size="1.016" layer="95" font="vector"/>
</instance>
<instance part="X4" gate="-7" x="205.74" y="116.84" smashed="yes">
<attribute name="NAME" x="208.28" y="116.078" size="1.016" layer="95" font="vector"/>
</instance>
<instance part="X4" gate="-8" x="205.74" y="106.68" smashed="yes">
<attribute name="NAME" x="208.28" y="105.918" size="1.016" layer="95" font="vector"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="A_4" class="0">
<segment>
<pinref part="J3" gate="-1" pin="1"/>
<pinref part="X3" gate="-1" pin="1"/>
<wire x1="152.4" y1="124.46" x2="160.02" y2="124.46" width="0.1524" layer="91"/>
<label x="152.4" y="124.46" size="1.016" layer="95" font="vector"/>
</segment>
</net>
<net name="!A_4" class="0">
<segment>
<pinref part="J3" gate="-2" pin="1"/>
<pinref part="X3" gate="-3" pin="1"/>
<wire x1="152.4" y1="121.92" x2="160.02" y2="121.92" width="0.1524" layer="91"/>
<label x="152.4" y="121.92" size="1.016" layer="95" font="vector"/>
</segment>
</net>
<net name="B_4" class="0">
<segment>
<pinref part="J3" gate="-3" pin="1"/>
<pinref part="X3" gate="-5" pin="1"/>
<wire x1="152.4" y1="119.38" x2="160.02" y2="119.38" width="0.1524" layer="91"/>
<label x="152.4" y="119.38" size="1.016" layer="95" font="vector"/>
</segment>
</net>
<net name="!B_4" class="0">
<segment>
<pinref part="J3" gate="-4" pin="1"/>
<pinref part="X3" gate="-7" pin="1"/>
<wire x1="152.4" y1="116.84" x2="160.02" y2="116.84" width="0.1524" layer="91"/>
<label x="152.4" y="116.84" size="1.016" layer="95" font="vector"/>
</segment>
</net>
<net name="VCC_4" class="0">
<segment>
<pinref part="J3" gate="-5" pin="1"/>
<pinref part="X3" gate="-2" pin="1"/>
<wire x1="152.4" y1="114.3" x2="160.02" y2="114.3" width="0.1524" layer="91"/>
<label x="152.4" y="114.3" size="1.016" layer="95" font="vector"/>
</segment>
</net>
<net name="GND_4" class="0">
<segment>
<pinref part="J3" gate="-6" pin="1"/>
<pinref part="X3" gate="-4" pin="1"/>
<wire x1="152.4" y1="111.76" x2="160.02" y2="111.76" width="0.1524" layer="91"/>
<label x="152.4" y="111.76" size="1.016" layer="95" font="vector"/>
</segment>
</net>
<net name="DIR_4" class="0">
<segment>
<pinref part="J3" gate="-7" pin="1"/>
<pinref part="X3" gate="-6" pin="1"/>
<wire x1="152.4" y1="109.22" x2="160.02" y2="109.22" width="0.1524" layer="91"/>
<label x="152.4" y="109.22" size="1.016" layer="95" font="vector"/>
</segment>
</net>
<net name="SPEED_4" class="0">
<segment>
<pinref part="J3" gate="-8" pin="1"/>
<pinref part="X3" gate="-8" pin="1"/>
<wire x1="152.4" y1="106.68" x2="160.02" y2="106.68" width="0.1524" layer="91"/>
<label x="152.4" y="106.68" size="1.016" layer="95" font="vector"/>
</segment>
</net>
<net name="A_3" class="0">
<segment>
<pinref part="J2" gate="-1" pin="1"/>
<pinref part="X2" gate="-1" pin="1"/>
<wire x1="198.12" y1="157.48" x2="205.74" y2="157.48" width="0.1524" layer="91"/>
<label x="198.12" y="157.48" size="1.016" layer="95" font="vector"/>
</segment>
</net>
<net name="!A_3" class="0">
<segment>
<pinref part="J2" gate="-2" pin="1"/>
<pinref part="X2" gate="-3" pin="1"/>
<wire x1="198.12" y1="154.94" x2="205.74" y2="154.94" width="0.1524" layer="91"/>
<label x="198.12" y="154.94" size="1.016" layer="95" font="vector"/>
</segment>
</net>
<net name="B_3" class="0">
<segment>
<pinref part="J2" gate="-3" pin="1"/>
<pinref part="X2" gate="-5" pin="1"/>
<wire x1="198.12" y1="152.4" x2="205.74" y2="152.4" width="0.1524" layer="91"/>
<label x="198.12" y="152.4" size="1.016" layer="95" font="vector"/>
</segment>
</net>
<net name="!B_3" class="0">
<segment>
<pinref part="J2" gate="-4" pin="1"/>
<pinref part="X2" gate="-7" pin="1"/>
<wire x1="198.12" y1="149.86" x2="205.74" y2="149.86" width="0.1524" layer="91"/>
<label x="198.12" y="149.86" size="1.016" layer="95" font="vector"/>
</segment>
</net>
<net name="VCC_3" class="0">
<segment>
<pinref part="J2" gate="-5" pin="1"/>
<pinref part="X2" gate="-2" pin="1"/>
<wire x1="198.12" y1="147.32" x2="205.74" y2="147.32" width="0.1524" layer="91"/>
<label x="198.12" y="147.32" size="1.016" layer="95" font="vector"/>
</segment>
</net>
<net name="GND_3" class="0">
<segment>
<pinref part="J2" gate="-6" pin="1"/>
<pinref part="X2" gate="-4" pin="1"/>
<wire x1="198.12" y1="144.78" x2="205.74" y2="144.78" width="0.1524" layer="91"/>
<label x="198.12" y="144.78" size="1.016" layer="95" font="vector"/>
</segment>
</net>
<net name="DIR_3" class="0">
<segment>
<pinref part="J2" gate="-7" pin="1"/>
<pinref part="X2" gate="-6" pin="1"/>
<wire x1="198.12" y1="142.24" x2="205.74" y2="142.24" width="0.1524" layer="91"/>
<label x="198.12" y="142.24" size="1.016" layer="95" font="vector"/>
</segment>
</net>
<net name="SPEED_3" class="0">
<segment>
<pinref part="J2" gate="-8" pin="1"/>
<pinref part="X2" gate="-8" pin="1"/>
<wire x1="198.12" y1="139.7" x2="205.74" y2="139.7" width="0.1524" layer="91"/>
<label x="198.12" y="139.7" size="1.016" layer="95" font="vector"/>
</segment>
</net>
<net name="A_2" class="0">
<segment>
<pinref part="J1" gate="-1" pin="1"/>
<pinref part="X1" gate="-1" pin="1"/>
<wire x1="152.4" y1="157.48" x2="157.48" y2="157.48" width="0.1524" layer="91"/>
<label x="152.4" y="157.48" size="1.016" layer="95" font="vector"/>
</segment>
</net>
<net name="!A_2" class="0">
<segment>
<pinref part="J1" gate="-2" pin="1"/>
<pinref part="X1" gate="-3" pin="1"/>
<wire x1="152.4" y1="154.94" x2="157.48" y2="154.94" width="0.1524" layer="91"/>
<label x="152.4" y="154.94" size="1.016" layer="95" font="vector"/>
</segment>
</net>
<net name="B_2" class="0">
<segment>
<pinref part="J1" gate="-3" pin="1"/>
<pinref part="X1" gate="-5" pin="1"/>
<wire x1="152.4" y1="152.4" x2="157.48" y2="152.4" width="0.1524" layer="91"/>
<label x="152.4" y="152.4" size="1.016" layer="95" font="vector"/>
</segment>
</net>
<net name="!B_2" class="0">
<segment>
<pinref part="J1" gate="-4" pin="1"/>
<pinref part="X1" gate="-7" pin="1"/>
<wire x1="152.4" y1="149.86" x2="157.48" y2="149.86" width="0.1524" layer="91"/>
<label x="152.4" y="149.86" size="1.016" layer="95" font="vector"/>
</segment>
</net>
<net name="VCC_2" class="0">
<segment>
<pinref part="J1" gate="-5" pin="1"/>
<pinref part="X1" gate="-2" pin="1"/>
<wire x1="152.4" y1="147.32" x2="157.48" y2="147.32" width="0.1524" layer="91"/>
<label x="152.4" y="147.32" size="1.016" layer="95" font="vector"/>
</segment>
</net>
<net name="GND_2" class="0">
<segment>
<pinref part="J1" gate="-6" pin="1"/>
<pinref part="X1" gate="-4" pin="1"/>
<wire x1="152.4" y1="144.78" x2="157.48" y2="144.78" width="0.1524" layer="91"/>
<label x="152.4" y="144.78" size="1.016" layer="95" font="vector"/>
</segment>
</net>
<net name="DIR_2" class="0">
<segment>
<pinref part="J1" gate="-7" pin="1"/>
<pinref part="X1" gate="-6" pin="1"/>
<wire x1="152.4" y1="142.24" x2="157.48" y2="142.24" width="0.1524" layer="91"/>
<label x="152.4" y="142.24" size="1.016" layer="95" font="vector"/>
</segment>
</net>
<net name="SPEED_2" class="0">
<segment>
<pinref part="J1" gate="-8" pin="1"/>
<pinref part="X1" gate="-8" pin="1"/>
<wire x1="152.4" y1="139.7" x2="157.48" y2="139.7" width="0.1524" layer="91"/>
<label x="152.4" y="139.7" size="1.016" layer="95" font="vector"/>
</segment>
</net>
<net name="A_5" class="0">
<segment>
<pinref part="J4" gate="-1" pin="1"/>
<pinref part="X4" gate="-1" pin="1"/>
<wire x1="195.58" y1="124.46" x2="203.2" y2="124.46" width="0.1524" layer="91"/>
<label x="195.58" y="124.46" size="1.016" layer="95" font="vector"/>
</segment>
</net>
<net name="!A_5" class="0">
<segment>
<pinref part="J4" gate="-2" pin="1"/>
<pinref part="X4" gate="-3" pin="1"/>
<wire x1="195.58" y1="121.92" x2="203.2" y2="121.92" width="0.1524" layer="91"/>
<label x="195.58" y="121.92" size="1.016" layer="95" font="vector"/>
</segment>
</net>
<net name="B_5" class="0">
<segment>
<pinref part="J4" gate="-3" pin="1"/>
<pinref part="X4" gate="-5" pin="1"/>
<wire x1="195.58" y1="119.38" x2="203.2" y2="119.38" width="0.1524" layer="91"/>
<label x="195.58" y="119.38" size="1.016" layer="95" font="vector"/>
</segment>
</net>
<net name="!B_5" class="0">
<segment>
<pinref part="J4" gate="-4" pin="1"/>
<pinref part="X4" gate="-7" pin="1"/>
<wire x1="195.58" y1="116.84" x2="203.2" y2="116.84" width="0.1524" layer="91"/>
<label x="195.58" y="116.84" size="1.016" layer="95" font="vector"/>
</segment>
</net>
<net name="VCC_5" class="0">
<segment>
<pinref part="J4" gate="-5" pin="1"/>
<pinref part="X4" gate="-2" pin="1"/>
<wire x1="195.58" y1="114.3" x2="203.2" y2="114.3" width="0.1524" layer="91"/>
<label x="195.58" y="114.3" size="1.016" layer="95" font="vector"/>
</segment>
</net>
<net name="GND_5" class="0">
<segment>
<pinref part="J4" gate="-6" pin="1"/>
<pinref part="X4" gate="-4" pin="1"/>
<wire x1="195.58" y1="111.76" x2="203.2" y2="111.76" width="0.1524" layer="91"/>
<label x="195.58" y="111.76" size="1.016" layer="95" font="vector"/>
</segment>
</net>
<net name="DIR_5" class="0">
<segment>
<pinref part="J4" gate="-7" pin="1"/>
<pinref part="X4" gate="-6" pin="1"/>
<wire x1="195.58" y1="109.22" x2="203.2" y2="109.22" width="0.1524" layer="91"/>
<label x="195.58" y="109.22" size="1.016" layer="95" font="vector"/>
</segment>
</net>
<net name="SPEED_5" class="0">
<segment>
<pinref part="J4" gate="-8" pin="1"/>
<pinref part="X4" gate="-8" pin="1"/>
<wire x1="195.58" y1="106.68" x2="203.2" y2="106.68" width="0.1524" layer="91"/>
<label x="195.58" y="106.68" size="1.016" layer="95" font="vector"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
</compatibility>
</eagle>
