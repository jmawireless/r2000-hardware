<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.5.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting keepoldvectorfont="yes"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="yes" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="60" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="2" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="5" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="no" active="no"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="no" active="no"/>
<layer number="103" name="tMap" color="7" fill="1" visible="no" active="no"/>
<layer number="104" name="Name" color="7" fill="1" visible="no" active="no"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="no" active="no"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="no" active="no"/>
<layer number="107" name="Crop" color="7" fill="1" visible="no" active="no"/>
<layer number="108" name="fp8" color="7" fill="1" visible="yes" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="yes" active="yes"/>
<layer number="110" name="110" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="111" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="7" fill="1" visible="yes" active="yes"/>
<layer number="114" name="FRNTMAAT1" color="7" fill="1" visible="no" active="no"/>
<layer number="115" name="FRNTMAAT2" color="7" fill="1" visible="no" active="no"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="no" active="no"/>
<layer number="117" name="BACKMAAT1" color="7" fill="1" visible="no" active="no"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="yes" active="yes"/>
<layer number="119" name="KAP_TEKEN" color="7" fill="1" visible="no" active="no"/>
<layer number="120" name="KAP_MAAT1" color="7" fill="1" visible="no" active="no"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="no" active="no"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="no" active="no"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="top_silk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="130" name="SMDSTROOK" color="7" fill="1" visible="no" active="no"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="133" name="bottom_silk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="DrillLegend" color="7" fill="1" visible="no" active="no"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="14" fill="1" visible="no" active="no"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="no"/>
<layer number="201" name="201bmp" color="2" fill="1" visible="no" active="no"/>
<layer number="202" name="202bmp" color="3" fill="1" visible="no" active="no"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="no" active="no"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="no" active="no"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="no" active="no"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="no" active="no"/>
<layer number="207" name="207bmp" color="15" fill="10" visible="no" active="no"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="no" active="no"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="Eagle3D_PG1" color="7" fill="1" visible="no" active="no"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="no"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="OrgLBR" color="13" fill="1" visible="no" active="no"/>
<layer number="255" name="Accent" color="7" fill="1" visible="no" active="no"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="JMA_LBR" urn="urn:adsk.eagle:library:15620506">
<packages>
<package name="0.5MM_FIDUCIAL" urn="urn:adsk.eagle:footprint:15620619/1" library_version="1">
<wire x1="0" y1="1" x2="0" y2="-1" width="0.0508" layer="51"/>
<wire x1="-1" y1="0" x2="1" y2="0" width="0.0508" layer="51"/>
<circle x="0" y="0" radius="0.5" width="0" layer="1"/>
<circle x="0" y="0" radius="0.889" width="0" layer="29"/>
<circle x="0" y="0" radius="1" width="0.0508" layer="51"/>
<circle x="0" y="0" radius="1.0307" width="0" layer="41"/>
<circle x="0" y="0" radius="0.5" width="0.0508" layer="51"/>
</package>
</packages>
<packages3d>
<package3d name="0.5MM_FIDUCIAL" urn="urn:adsk.eagle:package:15620691/1" type="box" library_version="1">
<packageinstances>
<packageinstance name="0.5MM_FIDUCIAL"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="FIDUCIAL" urn="urn:adsk.eagle:symbol:15620531/1" library_version="1">
<circle x="0" y="0" radius="3.81" width="0" layer="94"/>
<circle x="0" y="0" radius="6.35" width="0.254" layer="94"/>
<text x="-4.826" y="13.97" size="1.778" layer="125">&gt;NAME</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="0.5MM_FIDUCIAL" urn="urn:adsk.eagle:component:15620763/1" prefix="F" library_version="1">
<gates>
<gate name="G$1" symbol="FIDUCIAL" x="0" y="0"/>
</gates>
<devices>
<device name="" package="0.5MM_FIDUCIAL">
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15620691/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply2" urn="urn:adsk.eagle:library:372">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
Please keep in mind, that these devices are necessary for the
automatic wiring of the supply signals.&lt;p&gt;
The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND" urn="urn:adsk.eagle:symbol:26990/1" library_version="2">
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="-1.27" y2="0" width="0.254" layer="94"/>
<text x="-1.905" y="-3.175" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" urn="urn:adsk.eagle:component:27037/1" prefix="SUPPLY" library_version="2">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="GND" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1" urn="urn:adsk.eagle:library:371">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="+5V" urn="urn:adsk.eagle:symbol:26929/1" library_version="1">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+5V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="+5V" urn="urn:adsk.eagle:component:26963/1" prefix="P+" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="+5V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="frames" urn="urn:adsk.eagle:library:229">
<description>&lt;b&gt;Frames for Sheet and Layout&lt;/b&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="A3_LOC_JMA_RELEASE">
<wire x1="0" y1="241.3" x2="63.5" y2="241.3" width="0.025" layer="94"/>
<wire x1="63.5" y1="241.3" x2="127" y2="241.3" width="0.025" layer="94"/>
<wire x1="127" y1="241.3" x2="190.5" y2="241.3" width="0.025" layer="94"/>
<wire x1="190.5" y1="241.3" x2="254" y2="241.3" width="0.025" layer="94"/>
<wire x1="254" y1="241.3" x2="317.5" y2="241.3" width="0.025" layer="94"/>
<wire x1="317.5" y1="241.3" x2="380.97883125" y2="241.3" width="0.025" layer="94"/>
<wire x1="380.97883125" y1="241.3" x2="380.97883125" y2="0.02116875" width="0.025" layer="94"/>
<wire x1="380.97883125" y1="0.02116875" x2="0" y2="0.02116875" width="0.025" layer="94"/>
<wire x1="0" y1="0.02116875" x2="0" y2="60.325" width="0.025" layer="94"/>
<wire x1="0" y1="60.325" x2="0" y2="120.65" width="0.025" layer="94"/>
<wire x1="0" y1="120.65" x2="0" y2="180.975" width="0.025" layer="94"/>
<wire x1="0" y1="180.975" x2="0" y2="241.3" width="0.025" layer="94"/>
<wire x1="5.079890625" y1="236.22041875" x2="375.898909375" y2="236.22041875" width="0.025" layer="94"/>
<wire x1="375.898909375" y1="236.22041875" x2="375.898909375" y2="5.10075" width="0.025" layer="94"/>
<wire x1="375.898909375" y1="5.10075" x2="5.079890625" y2="5.10075" width="0.025" layer="94"/>
<wire x1="5.079890625" y1="5.10075" x2="5.079890625" y2="236.22041875" width="0.025" layer="94"/>
<wire x1="317.5" y1="5.08" x2="317.5" y2="0" width="0.025" layer="94"/>
<wire x1="317.5" y1="236.22" x2="317.5" y2="241.3" width="0.025" layer="94"/>
<wire x1="254" y1="5.08" x2="254" y2="0" width="0.025" layer="94"/>
<wire x1="254" y1="236.22" x2="254" y2="241.3" width="0.025" layer="94"/>
<wire x1="190.5" y1="5.08" x2="190.5" y2="0" width="0.025" layer="94"/>
<wire x1="190.5" y1="236.22" x2="190.5" y2="241.3" width="0.025" layer="94"/>
<wire x1="127" y1="5.08" x2="127" y2="0" width="0.025" layer="94"/>
<wire x1="127" y1="236.22" x2="127" y2="241.3" width="0.025" layer="94"/>
<wire x1="63.5" y1="5.08" x2="63.5" y2="0" width="0.025" layer="94"/>
<wire x1="63.5" y1="236.22" x2="63.5" y2="241.3" width="0.025" layer="94"/>
<wire x1="5.08" y1="180.975" x2="0" y2="180.975" width="0.025" layer="94"/>
<wire x1="375.92" y1="180.975" x2="381" y2="180.975" width="0.025" layer="94"/>
<wire x1="5.08" y1="120.65" x2="0" y2="120.65" width="0.025" layer="94"/>
<wire x1="375.92" y1="120.65" x2="381" y2="120.65" width="0.025" layer="94"/>
<wire x1="5.08" y1="60.325" x2="0" y2="60.325" width="0.025" layer="94"/>
<wire x1="375.92" y1="60.325" x2="381" y2="60.325" width="0.025" layer="94"/>
<wire x1="271.78" y1="5.08" x2="271.78" y2="21.59" width="0.025" layer="94"/>
<wire x1="271.78" y1="21.59" x2="271.78" y2="27.94" width="0.025" layer="94"/>
<wire x1="271.78" y1="27.94" x2="271.78" y2="34.29" width="0.025" layer="94"/>
<wire x1="271.78" y1="34.29" x2="271.78" y2="40.64" width="0.025" layer="94"/>
<wire x1="271.78" y1="40.64" x2="271.78" y2="45.72" width="0.025" layer="94"/>
<wire x1="271.78" y1="45.72" x2="285.75" y2="45.72" width="0.025" layer="94"/>
<wire x1="285.75" y1="45.72" x2="293.37" y2="45.72" width="0.025" layer="94"/>
<wire x1="293.37" y1="45.72" x2="304.8" y2="45.72" width="0.025" layer="94"/>
<wire x1="304.8" y1="45.72" x2="375.92" y2="45.72" width="0.025" layer="94"/>
<wire x1="304.8" y1="5.08" x2="304.8" y2="8.89" width="0.025" layer="94"/>
<wire x1="304.8" y1="8.89" x2="304.8" y2="15.24" width="0.025" layer="94"/>
<wire x1="304.8" y1="15.24" x2="304.8" y2="21.59" width="0.025" layer="94"/>
<wire x1="304.8" y1="21.59" x2="304.8" y2="27.94" width="0.025" layer="94"/>
<wire x1="304.8" y1="27.94" x2="304.8" y2="34.29" width="0.025" layer="94"/>
<wire x1="304.8" y1="34.29" x2="304.8" y2="40.64" width="0.025" layer="94"/>
<wire x1="304.8" y1="40.64" x2="304.8" y2="45.72" width="0.025" layer="94"/>
<wire x1="271.78" y1="40.64" x2="304.8" y2="40.64" width="0.025" layer="94"/>
<wire x1="271.78" y1="34.29" x2="304.8" y2="34.29" width="0.025" layer="94"/>
<wire x1="271.78" y1="27.94" x2="304.8" y2="27.94" width="0.025" layer="94"/>
<wire x1="271.78" y1="21.59" x2="285.75" y2="21.59" width="0.025" layer="94"/>
<wire x1="285.75" y1="21.59" x2="293.37" y2="21.59" width="0.025" layer="94"/>
<wire x1="293.37" y1="21.59" x2="304.8" y2="21.59" width="0.025" layer="94"/>
<wire x1="304.8" y1="8.89" x2="322.58" y2="8.89" width="0.025" layer="94"/>
<wire x1="322.58" y1="8.89" x2="359.41" y2="8.89" width="0.025" layer="94"/>
<wire x1="359.41" y1="8.89" x2="369.57" y2="8.89" width="0.025" layer="94"/>
<wire x1="369.57" y1="8.89" x2="375.92" y2="8.89" width="0.025" layer="94"/>
<wire x1="322.58" y1="8.89" x2="322.58" y2="5.08" width="0.025" layer="94"/>
<wire x1="359.41" y1="8.89" x2="359.41" y2="5.08" width="0.025" layer="94"/>
<wire x1="304.8" y1="15.24" x2="369.57" y2="15.24" width="0.025" layer="94"/>
<wire x1="369.57" y1="15.24" x2="375.92" y2="15.24" width="0.025" layer="94"/>
<wire x1="369.57" y1="15.24" x2="369.57" y2="8.89" width="0.025" layer="94"/>
<wire x1="304.8" y1="21.59" x2="375.92" y2="21.59" width="0.025" layer="94"/>
<wire x1="285.75" y1="45.72" x2="285.75" y2="21.59" width="0.025" layer="94"/>
<wire x1="293.37" y1="45.72" x2="293.37" y2="21.59" width="0.025" layer="94"/>
<wire x1="307.34" y1="31.75" x2="373.38" y2="31.75" width="0.025" layer="94"/>
<wire x1="329.29703125" y1="44.90211875" x2="329.35418125" y2="44.890690625" width="0.025" layer="94"/>
<wire x1="329.35418125" y1="44.890690625" x2="329.3999" y2="44.879259375" width="0.025" layer="94"/>
<wire x1="329.81138125" y1="44.776390625" x2="329.84566875" y2="44.764959375" width="0.025" layer="94"/>
<wire x1="329.84566875" y1="44.764959375" x2="329.891390625" y2="44.75353125" width="0.025" layer="94"/>
<wire x1="331.137259375" y1="44.1706" x2="331.365859375" y2="43.99915" width="0.025" layer="94"/>
<wire x1="331.365859375" y1="43.99915" x2="331.451590625" y2="43.91341875" width="0.025" layer="94"/>
<wire x1="331.92593125" y1="43.164759375" x2="331.937359375" y2="43.09618125" width="0.025" layer="94"/>
<wire x1="331.937359375" y1="43.09618125" x2="331.948790625" y2="42.993309375" width="0.025" layer="94"/>
<wire x1="331.891640625" y1="42.5704" x2="331.90306875" y2="42.604690625" width="0.025" layer="94"/>
<wire x1="331.84591875" y1="42.4561" x2="331.891640625" y2="42.5704" width="0.025" layer="94"/>
<wire x1="329.262740625" y1="40.947340625" x2="329.319890625" y2="40.95876875" width="0.025" layer="94"/>
<wire x1="329.194159375" y1="40.935909375" x2="329.262740625" y2="40.947340625" width="0.025" layer="94"/>
<wire x1="329.194159375" y1="40.935909375" x2="331.44586875" y2="32.512" width="0.025" layer="94"/>
<wire x1="323.764909375" y1="32.512" x2="324.9625" y2="37.03828125" width="0.025" layer="94"/>
<wire x1="324.9625" y1="37.03828125" x2="325.993759375" y2="40.935909375" width="0.025" layer="94"/>
<wire x1="325.719440625" y1="40.993059375" x2="325.78801875" y2="40.98163125" width="0.025" layer="94"/>
<wire x1="325.63943125" y1="41.01591875" x2="325.719440625" y2="40.993059375" width="0.025" layer="94"/>
<wire x1="325.45655" y1="41.061640625" x2="325.536559375" y2="41.03878125" width="0.025" layer="94"/>
<wire x1="325.3994" y1="41.07306875" x2="325.45655" y2="41.061640625" width="0.025" layer="94"/>
<wire x1="325.136509375" y1="41.15308125" x2="325.21651875" y2="41.13021875" width="0.025" layer="94"/>
<wire x1="325.06793125" y1="41.175940625" x2="325.136509375" y2="41.15308125" width="0.025" layer="94"/>
<wire x1="324.976490625" y1="41.21023125" x2="325.06793125" y2="41.175940625" width="0.025" layer="94"/>
<wire x1="324.97076875" y1="41.21251875" x2="324.976490625" y2="41.21023125" width="0.025" layer="94"/>
<wire x1="324.64501875" y1="41.347390625" x2="324.747890625" y2="41.30166875" width="0.025" layer="94"/>
<wire x1="324.565009375" y1="41.38168125" x2="324.64501875" y2="41.347390625" width="0.025" layer="94"/>
<wire x1="324.450709375" y1="41.43883125" x2="324.45185" y2="41.438259375" width="0.025" layer="94"/>
<wire x1="324.393559375" y1="41.47311875" x2="324.450709375" y2="41.43883125" width="0.025" layer="94"/>
<wire x1="324.26783125" y1="41.5417" x2="324.347840625" y2="41.49598125" width="0.025" layer="94"/>
<wire x1="324.233540625" y1="41.564559375" x2="324.26783125" y2="41.5417" width="0.025" layer="94"/>
<wire x1="324.18781875" y1="41.58741875" x2="324.233540625" y2="41.564559375" width="0.025" layer="94"/>
<wire x1="324.1421" y1="41.621709375" x2="324.18781875" y2="41.58741875" width="0.025" layer="94"/>
<wire x1="324.08495" y1="41.656" x2="324.1421" y2="41.621709375" width="0.025" layer="94"/>
<wire x1="323.822059375" y1="41.861740625" x2="323.85635" y2="41.82745" width="0.025" layer="94"/>
<wire x1="323.776340625" y1="41.89603125" x2="323.822059375" y2="41.861740625" width="0.025" layer="94"/>
<wire x1="323.62775" y1="42.05605" x2="323.662040625" y2="42.01033125" width="0.025" layer="94"/>
<wire x1="323.593459375" y1="42.090340625" x2="323.62775" y2="42.05605" width="0.025" layer="94"/>
<wire x1="323.35343125" y1="42.46753125" x2="323.354" y2="42.466390625" width="0.025" layer="94"/>
<wire x1="323.33056875" y1="42.52468125" x2="323.35343125" y2="42.46753125" width="0.025" layer="94"/>
<wire x1="323.536309375" y1="43.690540625" x2="323.58203125" y2="43.736259375" width="0.025" layer="94"/>
<wire x1="323.58203125" y1="43.736259375" x2="323.62775" y2="43.793409375" width="0.025" layer="94"/>
<wire x1="323.78776875" y1="43.95343125" x2="323.84491875" y2="43.99915" width="0.025" layer="94"/>
<wire x1="323.84491875" y1="43.99915" x2="323.87806875" y2="44.0323" width="0.025" layer="94"/>
<wire x1="323.87806875" y1="44.0323" x2="323.879209375" y2="44.033440625" width="0.025" layer="94"/>
<wire x1="323.879209375" y1="44.033440625" x2="323.936359375" y2="44.079159375" width="0.025" layer="94"/>
<wire x1="323.936359375" y1="44.079159375" x2="324.0278" y2="44.147740625" width="0.025" layer="94"/>
<wire x1="324.164959375" y1="44.23918125" x2="324.222109375" y2="44.27346875" width="0.025" layer="94"/>
<wire x1="324.222109375" y1="44.27346875" x2="324.290690625" y2="44.319190625" width="0.025" layer="94"/>
<wire x1="324.290690625" y1="44.319190625" x2="324.336409375" y2="44.34205" width="0.025" layer="94"/>
<wire x1="324.336409375" y1="44.34205" x2="324.393559375" y2="44.376340625" width="0.025" layer="94"/>
<wire x1="324.62101875" y1="44.49006875" x2="324.622159375" y2="44.490640625" width="0.025" layer="94"/>
<wire x1="324.622159375" y1="44.490640625" x2="324.679309375" y2="44.5135" width="0.025" layer="94"/>
<wire x1="324.679309375" y1="44.5135" x2="324.72503125" y2="44.536359375" width="0.025" layer="94"/>
<wire x1="324.72503125" y1="44.536359375" x2="325.01078125" y2="44.650659375" width="0.025" layer="94"/>
<wire x1="325.01078125" y1="44.650659375" x2="325.35368125" y2="44.764959375" width="0.025" layer="94"/>
<wire x1="325.35368125" y1="44.764959375" x2="325.433690625" y2="44.78781875" width="0.025" layer="94"/>
<wire x1="325.433690625" y1="44.78781875" x2="325.479409375" y2="44.79925" width="0.025" layer="94"/>
<wire x1="325.479409375" y1="44.79925" x2="325.5137" y2="44.81068125" width="0.025" layer="94"/>
<wire x1="325.5137" y1="44.81068125" x2="325.7423" y2="44.86783125" width="0.025" layer="94"/>
<wire x1="325.7423" y1="44.86783125" x2="325.79945" y2="44.879259375" width="0.025" layer="94"/>
<wire x1="325.79945" y1="44.879259375" x2="325.84516875" y2="44.890690625" width="0.025" layer="94"/>
<wire x1="326.393809375" y1="44.69638125" x2="326.450959375" y2="44.707809375" width="0.025" layer="94"/>
<wire x1="326.32523125" y1="44.68495" x2="326.393809375" y2="44.69638125" width="0.025" layer="94"/>
<wire x1="325.936609375" y1="44.604940625" x2="325.993759375" y2="44.61636875" width="0.025" layer="94"/>
<wire x1="325.84516875" y1="44.58208125" x2="325.936609375" y2="44.604940625" width="0.025" layer="94"/>
<wire x1="325.765159375" y1="44.55921875" x2="325.84516875" y2="44.58208125" width="0.025" layer="94"/>
<wire x1="325.67371875" y1="44.536359375" x2="325.765159375" y2="44.55921875" width="0.025" layer="94"/>
<wire x1="325.5137" y1="44.490640625" x2="325.520559375" y2="44.4926" width="0.025" layer="94"/>
<wire x1="325.23938125" y1="44.3992" x2="325.5137" y2="44.490640625" width="0.025" layer="94"/>
<wire x1="324.98791875" y1="44.29633125" x2="325.06793125" y2="44.33061875" width="0.025" layer="94"/>
<wire x1="324.93076875" y1="44.27346875" x2="324.98791875" y2="44.29633125" width="0.025" layer="94"/>
<wire x1="324.61073125" y1="44.10201875" x2="324.679309375" y2="44.147740625" width="0.025" layer="94"/>
<wire x1="324.53071875" y1="44.0563" x2="324.531859375" y2="44.05695" width="0.025" layer="94"/>
<wire x1="324.531859375" y1="44.05695" x2="324.61073125" y2="44.10201875" width="0.025" layer="94"/>
<wire x1="324.462140625" y1="44.01058125" x2="324.53071875" y2="44.0563" width="0.025" layer="94"/>
<wire x1="324.233540625" y1="43.8277" x2="324.279259375" y2="43.87341875" width="0.025" layer="94"/>
<wire x1="324.164959375" y1="43.77055" x2="324.233540625" y2="43.8277" width="0.025" layer="94"/>
<wire x1="323.73061875" y1="42.51325" x2="323.74205" y2="42.478959375" width="0.025" layer="94"/>
<wire x1="323.74205" y1="42.478959375" x2="323.75348125" y2="42.433240625" width="0.025" layer="94"/>
<wire x1="323.75348125" y1="42.433240625" x2="323.776340625" y2="42.38751875" width="0.025" layer="94"/>
<wire x1="323.776340625" y1="42.38751875" x2="323.7992" y2="42.33036875" width="0.025" layer="94"/>
<wire x1="324.08495" y1="41.95318125" x2="324.1421" y2="41.907459375" width="0.025" layer="94"/>
<wire x1="324.1421" y1="41.907459375" x2="324.176390625" y2="41.87316875" width="0.025" layer="94"/>
<wire x1="324.41641875" y1="41.690290625" x2="324.462140625" y2="41.656" width="0.025" layer="94"/>
<wire x1="324.462140625" y1="41.656" x2="324.53071875" y2="41.61028125" width="0.025" layer="94"/>
<wire x1="324.64501875" y1="41.5417" x2="324.690740625" y2="41.518840625" width="0.025" layer="94"/>
<wire x1="324.690740625" y1="41.518840625" x2="324.72503125" y2="41.49598125" width="0.025" layer="94"/>
<wire x1="324.72503125" y1="41.49598125" x2="324.78218125" y2="41.461690625" width="0.025" layer="94"/>
<wire x1="324.919340625" y1="41.393109375" x2="324.976490625" y2="41.37025" width="0.025" layer="94"/>
<wire x1="324.976490625" y1="41.37025" x2="325.022209375" y2="41.347390625" width="0.025" layer="94"/>
<wire x1="325.46798125" y1="41.18736875" x2="325.536559375" y2="41.164509375" width="0.025" layer="94"/>
<wire x1="325.536559375" y1="41.164509375" x2="325.58228125" y2="41.15308125" width="0.025" layer="94"/>
<wire x1="325.58228125" y1="41.15308125" x2="325.61656875" y2="41.14165" width="0.025" layer="94"/>
<wire x1="325.61656875" y1="41.14165" x2="325.708009375" y2="41.118790625" width="0.025" layer="94"/>
<wire x1="325.708009375" y1="41.118790625" x2="325.7423" y2="41.107359375" width="0.025" layer="94"/>
<wire x1="325.7423" y1="41.107359375" x2="326.01661875" y2="41.03878125" width="0.025" layer="94"/>
<wire x1="325.45655" y1="41.38168125" x2="325.5137" y2="41.35881875" width="0.025" layer="94"/>
<wire x1="325.433690625" y1="41.393109375" x2="325.45655" y2="41.38168125" width="0.025" layer="94"/>
<wire x1="325.15936875" y1="41.518840625" x2="325.319390625" y2="41.43883125" width="0.025" layer="94"/>
<wire x1="325.10221875" y1="41.55313125" x2="325.15936875" y2="41.518840625" width="0.025" layer="94"/>
<wire x1="324.70216875" y1="43.4848" x2="324.747890625" y2="43.519090625" width="0.025" layer="94"/>
<wire x1="324.747890625" y1="43.519090625" x2="324.77075" y2="43.54195" width="0.025" layer="94"/>
<wire x1="324.98678125" y1="43.68978125" x2="324.98791875" y2="43.690540625" width="0.025" layer="94"/>
<wire x1="324.98791875" y1="43.690540625" x2="325.10221875" y2="43.75911875" width="0.025" layer="94"/>
<wire x1="325.10221875" y1="43.75911875" x2="325.177659375" y2="43.796840625" width="0.025" layer="94"/>
<wire x1="325.45655" y1="43.93056875" x2="325.68515" y2="44.022009375" width="0.025" layer="94"/>
<wire x1="325.82116875" y1="44.06735" x2="325.822309375" y2="44.06773125" width="0.025" layer="94"/>
<wire x1="325.822309375" y1="44.06773125" x2="326.062340625" y2="44.136309375" width="0.025" layer="94"/>
<wire x1="326.062340625" y1="44.136309375" x2="326.15378125" y2="44.15916875" width="0.025" layer="94"/>
<wire x1="326.15378125" y1="44.15916875" x2="326.35951875" y2="44.204890625" width="0.025" layer="94"/>
<wire x1="326.279509375" y1="43.919140625" x2="326.462390625" y2="43.964859375" width="0.025" layer="94"/>
<wire x1="326.119490625" y1="43.87341875" x2="326.279509375" y2="43.919140625" width="0.025" layer="94"/>
<wire x1="326.02805" y1="43.850559375" x2="326.119490625" y2="43.87341875" width="0.025" layer="94"/>
<wire x1="325.68515" y1="43.72483125" x2="325.8566" y2="43.793409375" width="0.025" layer="94"/>
<wire x1="325.605140625" y1="43.690540625" x2="325.68515" y2="43.72483125" width="0.025" layer="94"/>
<wire x1="325.604" y1="43.68996875" x2="325.605140625" y2="43.690540625" width="0.025" layer="94"/>
<wire x1="325.34225" y1="43.54195" x2="325.45655" y2="43.61053125" width="0.025" layer="94"/>
<wire x1="325.341109375" y1="43.541190625" x2="325.34225" y2="43.54195" width="0.025" layer="94"/>
<wire x1="324.862190625" y1="43.15333125" x2="325.033640625" y2="43.32478125" width="0.025" layer="94"/>
<wire x1="324.8279" y1="43.107609375" x2="324.862190625" y2="43.15333125" width="0.025" layer="94"/>
<wire x1="324.75931875" y1="43.004740625" x2="324.8279" y2="43.107609375" width="0.025" layer="94"/>
<wire x1="324.633590625" y1="42.62755" x2="324.65645" y2="42.74185" width="0.025" layer="94"/>
<wire x1="324.633590625" y1="42.626409375" x2="324.633590625" y2="42.62755" width="0.025" layer="94"/>
<wire x1="324.690740625" y1="42.261790625" x2="324.70216875" y2="42.23893125" width="0.025" layer="94"/>
<wire x1="324.70216875" y1="42.23893125" x2="324.7136" y2="42.204640625" width="0.025" layer="94"/>
<wire x1="324.7136" y1="42.204640625" x2="324.72503125" y2="42.18178125" width="0.025" layer="94"/>
<wire x1="324.72503125" y1="42.18178125" x2="324.747890625" y2="42.147490625" width="0.025" layer="94"/>
<wire x1="324.793609375" y1="42.06748125" x2="324.87361875" y2="41.964609375" width="0.025" layer="94"/>
<wire x1="324.87361875" y1="41.964609375" x2="324.874759375" y2="41.96346875" width="0.025" layer="94"/>
<wire x1="325.84516875" y1="41.35881875" x2="325.969759375" y2="41.317290625" width="0.025" layer="94"/>
<wire x1="326.084059375" y1="41.279190625" x2="326.0852" y2="41.278809375" width="0.025" layer="94"/>
<wire x1="326.0852" y1="41.278809375" x2="326.119490625" y2="41.43883125" width="0.025" layer="94"/>
<wire x1="326.0852" y1="41.461690625" x2="326.119490625" y2="41.43883125" width="0.025" layer="94"/>
<wire x1="325.993759375" y1="41.49598125" x2="326.0852" y2="41.461690625" width="0.025" layer="94"/>
<wire x1="325.99261875" y1="41.49646875" x2="325.993759375" y2="41.49598125" width="0.025" layer="94"/>
<wire x1="325.650859375" y1="41.66743125" x2="325.652" y2="41.66678125" width="0.025" layer="94"/>
<wire x1="325.55941875" y1="41.72458125" x2="325.650859375" y2="41.66743125" width="0.025" layer="94"/>
<wire x1="325.44511875" y1="42.95901875" x2="325.536559375" y2="43.0276" width="0.025" layer="94"/>
<wire x1="325.536559375" y1="43.0276" x2="325.5377" y2="43.028359375" width="0.025" layer="94"/>
<wire x1="325.605140625" y1="43.07331875" x2="325.776590625" y2="43.176190625" width="0.025" layer="94"/>
<wire x1="326.49668125" y1="43.43908125" x2="326.64526875" y2="43.47336875" width="0.025" layer="94"/>
<wire x1="326.75956875" y1="43.30191875" x2="326.81671875" y2="43.31335" width="0.025" layer="94"/>
<wire x1="326.71385" y1="43.290490625" x2="326.75956875" y2="43.30191875" width="0.025" layer="94"/>
<wire x1="326.6567" y1="43.279059375" x2="326.71385" y2="43.290490625" width="0.025" layer="94"/>
<wire x1="326.565259375" y1="43.2562" x2="326.6567" y2="43.279059375" width="0.025" layer="94"/>
<wire x1="326.35951875" y1="43.18761875" x2="326.41666875" y2="43.21048125" width="0.025" layer="94"/>
<wire x1="326.32523125" y1="43.176190625" x2="326.35951875" y2="43.18761875" width="0.025" layer="94"/>
<wire x1="326.21093125" y1="43.13046875" x2="326.32523125" y2="43.176190625" width="0.025" layer="94"/>
<wire x1="326.005190625" y1="43.0276" x2="326.21093125" y2="43.13046875" width="0.025" layer="94"/>
<wire x1="325.547609375" y1="42.09148125" x2="325.547990625" y2="42.090340625" width="0.025" layer="94"/>
<wire x1="325.547990625" y1="42.090340625" x2="325.57085" y2="42.04461875" width="0.025" layer="94"/>
<wire x1="325.84516875" y1="41.747440625" x2="325.95946875" y2="41.678859375" width="0.025" layer="94"/>
<wire x1="325.95946875" y1="41.678859375" x2="326.03948125" y2="41.633140625" width="0.025" layer="94"/>
<wire x1="326.03948125" y1="41.633140625" x2="326.108059375" y2="41.59885" width="0.025" layer="94"/>
<wire x1="326.108059375" y1="41.59885" x2="326.165209375" y2="41.575990625" width="0.025" layer="94"/>
<wire x1="326.165209375" y1="41.575990625" x2="326.35951875" y2="42.307509375" width="0.025" layer="94"/>
<wire x1="329.022709375" y1="41.575990625" x2="329.04556875" y2="41.575990625" width="0.025" layer="94"/>
<wire x1="329.04556875" y1="41.575990625" x2="329.251309375" y2="41.678859375" width="0.025" layer="94"/>
<wire x1="329.251309375" y1="41.678859375" x2="329.25245" y2="41.67961875" width="0.025" layer="94"/>
<wire x1="329.42161875" y1="41.7924" x2="329.422759375" y2="41.793159375" width="0.025" layer="94"/>
<wire x1="329.422759375" y1="41.793159375" x2="329.548490625" y2="41.918890625" width="0.025" layer="94"/>
<wire x1="329.548490625" y1="41.918890625" x2="329.58278125" y2="41.964609375" width="0.025" layer="94"/>
<wire x1="329.58278125" y1="41.964609375" x2="329.6285" y2="42.033190625" width="0.025" layer="94"/>
<wire x1="329.662790625" y1="42.1132" x2="329.68565" y2="42.18178125" width="0.025" layer="94"/>
<wire x1="329.68565" y1="42.18178125" x2="329.69708125" y2="42.2275" width="0.025" layer="94"/>
<wire x1="329.57135" y1="42.718990625" x2="329.61706875" y2="42.650409375" width="0.025" layer="94"/>
<wire x1="329.537059375" y1="42.764709375" x2="329.57135" y2="42.718990625" width="0.025" layer="94"/>
<wire x1="329.434190625" y1="42.86758125" x2="329.43533125" y2="42.866440625" width="0.025" layer="94"/>
<wire x1="329.377040625" y1="42.9133" x2="329.434190625" y2="42.86758125" width="0.025" layer="94"/>
<wire x1="329.01128125" y1="43.119040625" x2="329.06843125" y2="43.09618125" width="0.025" layer="94"/>
<wire x1="328.965559375" y1="43.1419" x2="329.01128125" y2="43.119040625" width="0.025" layer="94"/>
<wire x1="328.32548125" y1="43.32478125" x2="328.43978125" y2="43.30191875" width="0.025" layer="94"/>
<wire x1="328.2569" y1="43.336209375" x2="328.32548125" y2="43.32478125" width="0.025" layer="94"/>
<wire x1="328.15403125" y1="43.347640625" x2="328.2569" y2="43.336209375" width="0.025" layer="94"/>
<wire x1="328.005440625" y1="43.35906875" x2="328.15403125" y2="43.347640625" width="0.025" layer="94"/>
<wire x1="328.005440625" y1="43.35906875" x2="328.005440625" y2="43.53051875" width="0.025" layer="94"/>
<wire x1="328.78268125" y1="43.41621875" x2="329.010140625" y2="43.34798125" width="0.025" layer="94"/>
<wire x1="329.010140625" y1="43.34798125" x2="329.01128125" y2="43.347640625" width="0.025" layer="94"/>
<wire x1="329.01128125" y1="43.347640625" x2="329.194159375" y2="43.279059375" width="0.025" layer="94"/>
<wire x1="329.194159375" y1="43.279059375" x2="329.308459375" y2="43.233340625" width="0.025" layer="94"/>
<wire x1="329.308459375" y1="43.233340625" x2="329.3096" y2="43.23276875" width="0.025" layer="94"/>
<wire x1="329.81138125" y1="42.9133" x2="329.891390625" y2="42.833290625" width="0.025" layer="94"/>
<wire x1="329.891390625" y1="42.833290625" x2="329.937109375" y2="42.776140625" width="0.025" layer="94"/>
<wire x1="329.95996875" y1="42.74185" x2="329.994259375" y2="42.6847" width="0.025" layer="94"/>
<wire x1="329.994259375" y1="42.6847" x2="330.03998125" y2="42.5704" width="0.025" layer="94"/>
<wire x1="330.03998125" y1="42.5704" x2="330.051409375" y2="42.51325" width="0.025" layer="94"/>
<wire x1="330.051409375" y1="42.51325" x2="330.062840625" y2="42.421809375" width="0.025" layer="94"/>
<wire x1="329.55991875" y1="41.66743125" x2="329.6285" y2="41.71315" width="0.025" layer="94"/>
<wire x1="329.45705" y1="41.61028125" x2="329.55991875" y2="41.66743125" width="0.025" layer="94"/>
<wire x1="329.15986875" y1="41.47311875" x2="329.1633" y2="41.474590625" width="0.025" layer="94"/>
<wire x1="329.06843125" y1="41.43883125" x2="329.15986875" y2="41.47311875" width="0.025" layer="94"/>
<wire x1="329.137009375" y1="41.290240625" x2="329.18273125" y2="41.30166875" width="0.025" layer="94"/>
<wire x1="329.18273125" y1="41.30166875" x2="329.21701875" y2="41.3131" width="0.025" layer="94"/>
<wire x1="329.21701875" y1="41.3131" x2="329.262740625" y2="41.32453125" width="0.025" layer="94"/>
<wire x1="329.262740625" y1="41.32453125" x2="329.3999" y2="41.37025" width="0.025" layer="94"/>
<wire x1="329.57135" y1="41.43883125" x2="329.605640625" y2="41.450259375" width="0.025" layer="94"/>
<wire x1="329.605640625" y1="41.450259375" x2="329.834240625" y2="41.564559375" width="0.025" layer="94"/>
<wire x1="329.834240625" y1="41.564559375" x2="329.86853125" y2="41.58741875" width="0.025" layer="94"/>
<wire x1="329.86853125" y1="41.58741875" x2="329.891390625" y2="41.59885" width="0.025" layer="94"/>
<wire x1="330.30286875" y1="41.93031875" x2="330.348590625" y2="41.98746875" width="0.025" layer="94"/>
<wire x1="330.348590625" y1="41.98746875" x2="330.37145" y2="42.01033125" width="0.025" layer="94"/>
<wire x1="330.405740625" y1="42.05605" x2="330.47431875" y2="42.17035" width="0.025" layer="94"/>
<wire x1="330.47431875" y1="42.17035" x2="330.520040625" y2="42.28465" width="0.025" layer="94"/>
<wire x1="330.54251875" y1="42.352090625" x2="330.5429" y2="42.35323125" width="0.025" layer="94"/>
<wire x1="330.5429" y1="42.35323125" x2="330.55433125" y2="42.41038125" width="0.025" layer="94"/>
<wire x1="330.55433125" y1="42.41038125" x2="330.565759375" y2="42.478959375" width="0.025" layer="94"/>
<wire x1="328.72553125" y1="44.22775" x2="328.95413125" y2="44.18203125" width="0.025" layer="94"/>
<wire x1="328.95413125" y1="44.18203125" x2="329.22845" y2="44.11345" width="0.025" layer="94"/>
<wire x1="329.69708125" y1="43.95343125" x2="329.7428" y2="43.93056875" width="0.025" layer="94"/>
<wire x1="329.7428" y1="43.93056875" x2="329.79995" y2="43.907709375" width="0.025" layer="94"/>
<wire x1="329.79995" y1="43.907709375" x2="329.84566875" y2="43.88485" width="0.025" layer="94"/>
<wire x1="329.84566875" y1="43.88485" x2="329.90281875" y2="43.861990625" width="0.025" layer="94"/>
<wire x1="329.90281875" y1="43.861990625" x2="330.03998125" y2="43.793409375" width="0.025" layer="94"/>
<wire x1="330.03998125" y1="43.793409375" x2="330.09713125" y2="43.75911875" width="0.025" layer="94"/>
<wire x1="330.09713125" y1="43.75911875" x2="330.14285" y2="43.736259375" width="0.025" layer="94"/>
<wire x1="330.55433125" y1="43.43908125" x2="330.60005" y2="43.393359375" width="0.025" layer="94"/>
<wire x1="330.60005" y1="43.393359375" x2="330.66863125" y2="43.31335" width="0.025" layer="94"/>
<wire x1="330.66863125" y1="43.31335" x2="330.70291875" y2="43.279059375" width="0.025" layer="94"/>
<wire x1="330.805790625" y1="42.193209375" x2="330.82865" y2="42.23893125" width="0.025" layer="94"/>
<wire x1="330.7715" y1="42.136059375" x2="330.805790625" y2="42.193209375" width="0.025" layer="94"/>
<wire x1="330.70291875" y1="42.04461875" x2="330.7715" y2="42.136059375" width="0.025" layer="94"/>
<wire x1="329.29703125" y1="41.233090625" x2="329.594209375" y2="41.32453125" width="0.025" layer="94"/>
<wire x1="329.1713" y1="41.03878125" x2="329.18273125" y2="41.03878125" width="0.025" layer="94"/>
<wire x1="329.18273125" y1="41.03878125" x2="329.26616875" y2="41.059640625" width="0.025" layer="94"/>
<wire x1="329.26616875" y1="41.059640625" x2="329.50276875" y2="41.118790625" width="0.025" layer="94"/>
<wire x1="329.50276875" y1="41.118790625" x2="329.7428" y2="41.18736875" width="0.025" layer="94"/>
<wire x1="329.7428" y1="41.18736875" x2="329.743940625" y2="41.18775" width="0.025" layer="94"/>
<wire x1="329.948540625" y1="41.25595" x2="330.005690625" y2="41.278809375" width="0.025" layer="94"/>
<wire x1="330.005690625" y1="41.278809375" x2="330.03998125" y2="41.290240625" width="0.025" layer="94"/>
<wire x1="330.60005" y1="41.564559375" x2="330.634340625" y2="41.58741875" width="0.025" layer="94"/>
<wire x1="330.634340625" y1="41.58741875" x2="330.691490625" y2="41.621709375" width="0.025" layer="94"/>
<wire x1="330.691490625" y1="41.621709375" x2="330.737209375" y2="41.656" width="0.025" layer="94"/>
<wire x1="331.2287" y1="42.06748125" x2="331.27441875" y2="42.12463125" width="0.025" layer="94"/>
<wire x1="331.27441875" y1="42.12463125" x2="331.308709375" y2="42.17035" width="0.025" layer="94"/>
<wire x1="331.33156875" y1="42.204640625" x2="331.343" y2="42.2275" width="0.025" layer="94"/>
<wire x1="331.343" y1="42.2275" x2="331.365859375" y2="42.261790625" width="0.025" layer="94"/>
<wire x1="331.50301875" y1="42.97045" x2="331.51445" y2="42.84471875" width="0.025" layer="94"/>
<wire x1="331.365859375" y1="43.3705" x2="331.36643125" y2="43.369359375" width="0.025" layer="94"/>
<wire x1="331.27441875" y1="43.507659375" x2="331.365859375" y2="43.3705" width="0.025" layer="94"/>
<wire x1="331.24013125" y1="43.55338125" x2="331.27441875" y2="43.507659375" width="0.025" layer="94"/>
<wire x1="330.61148125" y1="44.090590625" x2="330.66863125" y2="44.0563" width="0.025" layer="94"/>
<wire x1="330.610340625" y1="44.09135" x2="330.61148125" y2="44.090590625" width="0.025" layer="94"/>
<wire x1="329.95996875" y1="44.3992" x2="330.24571875" y2="44.2849" width="0.025" layer="94"/>
<wire x1="329.651359375" y1="44.50206875" x2="329.95996875" y2="44.3992" width="0.025" layer="94"/>
<wire x1="329.491340625" y1="44.547790625" x2="329.651359375" y2="44.50206875" width="0.025" layer="94"/>
<wire x1="329.21701875" y1="44.61636875" x2="329.491340625" y2="44.547790625" width="0.025" layer="94"/>
<wire x1="328.3712" y1="44.75353125" x2="328.47406875" y2="44.7421" width="0.025" layer="94"/>
<wire x1="328.2569" y1="44.764959375" x2="328.3712" y2="44.75353125" width="0.025" layer="94"/>
<wire x1="328.222609375" y1="44.764959375" x2="328.2569" y2="44.764959375" width="0.025" layer="94"/>
<wire x1="327.58253125" y1="40.78731875" x2="328.519790625" y2="36.21531875" width="0.025" layer="94"/>
<wire x1="326.690990625" y1="36.21531875" x2="327.58253125" y2="40.78731875" width="0.025" layer="94"/>
<wire x1="326.690990625" y1="36.21531875" x2="326.86038125" y2="37.084" width="0.025" layer="94"/>
<wire x1="324.9746" y1="37.084" x2="323.764909375" y2="32.512" width="0.025" layer="94"/>
<wire x1="323.764909375" y1="32.512" x2="325.8566" y2="32.512" width="0.025" layer="94"/>
<wire x1="325.8566" y1="32.512" x2="326.336659375" y2="34.5694" width="0.025" layer="94"/>
<wire x1="326.336659375" y1="34.5694" x2="328.862690625" y2="34.5694" width="0.025" layer="94"/>
<wire x1="328.862690625" y1="34.5694" x2="329.319890625" y2="32.512" width="0.025" layer="94"/>
<wire x1="329.319890625" y1="32.512" x2="331.44586875" y2="32.512" width="0.025" layer="94"/>
<wire x1="331.44586875" y1="32.512" x2="330.22376875" y2="37.084" width="0.025" layer="94"/>
<wire x1="328.341709375" y1="37.084" x2="328.35108125" y2="37.03828125" width="0.025" layer="94"/>
<wire x1="328.35108125" y1="37.03828125" x2="328.519790625" y2="36.21531875" width="0.025" layer="94"/>
<wire x1="328.519790625" y1="36.21531875" x2="326.690990625" y2="36.21531875" width="0.025" layer="94"/>
<wire x1="314.41516875" y1="42.29608125" x2="314.41516875" y2="32.512" width="0.025" layer="94"/>
<wire x1="314.41516875" y1="32.512" x2="316.26683125" y2="32.512" width="0.025" layer="94"/>
<wire x1="316.26683125" y1="32.512" x2="316.26683125" y2="40.21581875" width="0.025" layer="94"/>
<wire x1="316.26683125" y1="40.21581875" x2="317.98133125" y2="32.512" width="0.025" layer="94"/>
<wire x1="317.98133125" y1="32.512" x2="319.55866875" y2="32.512" width="0.025" layer="94"/>
<wire x1="319.55866875" y1="32.512" x2="321.250309375" y2="40.21581875" width="0.025" layer="94"/>
<wire x1="321.250309375" y1="40.21581875" x2="321.250309375" y2="32.512" width="0.025" layer="94"/>
<wire x1="321.250309375" y1="32.512" x2="323.12483125" y2="32.512" width="0.025" layer="94"/>
<wire x1="323.12483125" y1="32.512" x2="323.12483125" y2="42.29608125" width="0.025" layer="94"/>
<wire x1="323.12483125" y1="42.29608125" x2="320.15303125" y2="42.29608125" width="0.025" layer="94"/>
<wire x1="320.15303125" y1="42.29608125" x2="318.767990625" y2="35.575240625" width="0.025" layer="94"/>
<wire x1="318.767990625" y1="35.575240625" x2="317.364109375" y2="42.29608125" width="0.025" layer="94"/>
<wire x1="317.364109375" y1="42.29608125" x2="314.41516875" y2="42.29608125" width="0.025" layer="94"/>
<wire x1="312.83783125" y1="35.392359375" x2="312.83783125" y2="42.3418" width="0.025" layer="94"/>
<wire x1="312.83783125" y1="42.3418" x2="310.78043125" y2="42.3418" width="0.025" layer="94"/>
<wire x1="310.78043125" y1="42.3418" x2="310.78043125" y2="35.1409" width="0.025" layer="94"/>
<wire x1="310.78043125" y1="35.1409" x2="310.75756875" y2="35.0266" width="0.025" layer="94"/>
<wire x1="310.75756875" y1="35.0266" x2="310.71185" y2="34.889440625" width="0.025" layer="94"/>
<wire x1="310.71185" y1="34.889440625" x2="310.63346875" y2="34.75228125" width="0.025" layer="94"/>
<wire x1="310.63346875" y1="34.75228125" x2="310.620409375" y2="34.72941875" width="0.025" layer="94"/>
<wire x1="310.620409375" y1="34.72941875" x2="310.55183125" y2="34.660840625" width="0.025" layer="94"/>
<wire x1="310.55183125" y1="34.660840625" x2="310.506109375" y2="34.619690625" width="0.025" layer="94"/>
<wire x1="310.506109375" y1="34.619690625" x2="310.41466875" y2="34.546540625" width="0.025" layer="94"/>
<wire x1="310.41466875" y1="34.546540625" x2="310.30036875" y2="34.477959375" width="0.025" layer="94"/>
<wire x1="310.30036875" y1="34.477959375" x2="310.18606875" y2="34.432240625" width="0.025" layer="94"/>
<wire x1="310.18606875" y1="34.432240625" x2="310.02605" y2="34.38651875" width="0.025" layer="94"/>
<wire x1="310.02605" y1="34.38651875" x2="309.84316875" y2="34.363659375" width="0.025" layer="94"/>
<wire x1="309.84316875" y1="34.363659375" x2="309.660290625" y2="34.363659375" width="0.025" layer="94"/>
<wire x1="309.660290625" y1="34.363659375" x2="309.56885" y2="34.3789" width="0.025" layer="94"/>
<wire x1="309.56885" y1="34.3789" x2="309.52313125" y2="34.38651875" width="0.025" layer="94"/>
<wire x1="309.52313125" y1="34.38651875" x2="309.477409375" y2="34.39958125" width="0.025" layer="94"/>
<wire x1="309.477409375" y1="34.39958125" x2="309.431690625" y2="34.41265" width="0.025" layer="94"/>
<wire x1="309.431690625" y1="34.41265" x2="309.363109375" y2="34.432240625" width="0.025" layer="94"/>
<wire x1="309.363109375" y1="34.432240625" x2="309.22595" y2="34.50081875" width="0.025" layer="94"/>
<wire x1="309.22595" y1="34.50081875" x2="309.088790625" y2="34.592259375" width="0.025" layer="94"/>
<wire x1="309.088790625" y1="34.592259375" x2="308.95163125" y2="34.72941875" width="0.025" layer="94"/>
<wire x1="308.95163125" y1="34.72941875" x2="308.70733125" y2="35.05515" width="0.025" layer="94"/>
<wire x1="308.70733125" y1="35.05515" x2="307.145690625" y2="33.81501875" width="0.025" layer="94"/>
<wire x1="307.145690625" y1="33.81501875" x2="307.259990625" y2="33.58641875" width="0.025" layer="94"/>
<wire x1="307.259990625" y1="33.58641875" x2="307.305709375" y2="33.525459375" width="0.025" layer="94"/>
<wire x1="307.305709375" y1="33.525459375" x2="307.34571875" y2="33.47211875" width="0.025" layer="94"/>
<wire x1="307.34571875" y1="33.47211875" x2="307.39715" y2="33.403540625" width="0.025" layer="94"/>
<wire x1="307.39715" y1="33.403540625" x2="307.43935" y2="33.35781875" width="0.025" layer="94"/>
<wire x1="307.43935" y1="33.35781875" x2="307.488590625" y2="33.30448125" width="0.025" layer="94"/>
<wire x1="307.488590625" y1="33.30448125" x2="307.523759375" y2="33.26638125" width="0.025" layer="94"/>
<wire x1="307.523759375" y1="33.26638125" x2="307.55716875" y2="33.23018125" width="0.025" layer="94"/>
<wire x1="307.55716875" y1="33.23018125" x2="307.587059375" y2="33.1978" width="0.025" layer="94"/>
<wire x1="307.587059375" y1="33.1978" x2="307.62575" y2="33.155890625" width="0.025" layer="94"/>
<wire x1="307.62575" y1="33.155890625" x2="307.65036875" y2="33.12921875" width="0.025" layer="94"/>
<wire x1="307.65036875" y1="33.12921875" x2="307.67146875" y2="33.106359375" width="0.025" layer="94"/>
<wire x1="307.67146875" y1="33.106359375" x2="307.762909375" y2="33.02323125" width="0.025" layer="94"/>
<wire x1="307.762909375" y1="33.02323125" x2="307.7972" y2="32.992059375" width="0.025" layer="94"/>
<wire x1="307.7972" y1="32.992059375" x2="307.85435" y2="32.940109375" width="0.025" layer="94"/>
<wire x1="307.85435" y1="32.940109375" x2="307.89778125" y2="32.90061875" width="0.025" layer="94"/>
<wire x1="307.89778125" y1="32.90061875" x2="307.92293125" y2="32.877759375" width="0.025" layer="94"/>
<wire x1="307.92293125" y1="32.877759375" x2="308.12866875" y2="32.7406" width="0.025" layer="94"/>
<wire x1="308.12866875" y1="32.7406" x2="308.174390625" y2="32.71011875" width="0.025" layer="94"/>
<wire x1="308.174390625" y1="32.71011875" x2="308.24296875" y2="32.6705" width="0.025" layer="94"/>
<wire x1="308.24296875" y1="32.6705" x2="308.288690625" y2="32.646109375" width="0.025" layer="94"/>
<wire x1="308.288690625" y1="32.646109375" x2="308.3687" y2="32.603440625" width="0.025" layer="94"/>
<wire x1="308.3687" y1="32.603440625" x2="308.42585" y2="32.572959375" width="0.025" layer="94"/>
<wire x1="308.42585" y1="32.572959375" x2="308.54015" y2="32.512" width="0.025" layer="94"/>
<wire x1="308.54015" y1="32.512" x2="308.66206875" y2="32.46628125" width="0.025" layer="94"/>
<wire x1="308.66206875" y1="32.46628125" x2="308.72303125" y2="32.44341875" width="0.025" layer="94"/>
<wire x1="308.72303125" y1="32.44341875" x2="308.905909375" y2="32.374840625" width="0.025" layer="94"/>
<wire x1="308.905909375" y1="32.374840625" x2="308.99735" y2="32.353740625" width="0.025" layer="94"/>
<wire x1="308.99735" y1="32.353740625" x2="309.203090625" y2="32.306259375" width="0.025" layer="94"/>
<wire x1="309.203090625" y1="32.306259375" x2="309.27166875" y2="32.295709375" width="0.025" layer="94"/>
<wire x1="309.27166875" y1="32.295709375" x2="309.35168125" y2="32.2834" width="0.025" layer="94"/>
<wire x1="309.35168125" y1="32.2834" x2="309.40883125" y2="32.274609375" width="0.025" layer="94"/>
<wire x1="309.40883125" y1="32.274609375" x2="309.477409375" y2="32.264059375" width="0.025" layer="94"/>
<wire x1="309.477409375" y1="32.264059375" x2="309.50026875" y2="32.260540625" width="0.025" layer="94"/>
<wire x1="309.50026875" y1="32.260540625" x2="309.68315" y2="32.2453" width="0.025" layer="94"/>
<wire x1="309.68315" y1="32.2453" x2="309.774590625" y2="32.23768125" width="0.025" layer="94"/>
<wire x1="309.774590625" y1="32.23768125" x2="310.048909375" y2="32.260540625" width="0.025" layer="94"/>
<wire x1="310.048909375" y1="32.260540625" x2="310.231790625" y2="32.2834" width="0.025" layer="94"/>
<wire x1="310.231790625" y1="32.2834" x2="310.460390625" y2="32.32911875" width="0.025" layer="94"/>
<wire x1="310.460390625" y1="32.32911875" x2="310.620409375" y2="32.374840625" width="0.025" layer="94"/>
<wire x1="310.620409375" y1="32.374840625" x2="310.82615" y2="32.44341875" width="0.025" layer="94"/>
<wire x1="310.82615" y1="32.44341875" x2="311.05475" y2="32.534859375" width="0.025" layer="94"/>
<wire x1="311.05475" y1="32.534859375" x2="311.374790625" y2="32.69488125" width="0.025" layer="94"/>
<wire x1="311.374790625" y1="32.69488125" x2="311.58053125" y2="32.832040625" width="0.025" layer="94"/>
<wire x1="311.58053125" y1="32.832040625" x2="311.649109375" y2="32.889190625" width="0.025" layer="94"/>
<wire x1="311.649109375" y1="32.889190625" x2="311.69483125" y2="32.927290625" width="0.025" layer="94"/>
<wire x1="311.69483125" y1="32.927290625" x2="311.763409375" y2="32.984440625" width="0.025" layer="94"/>
<wire x1="311.763409375" y1="32.984440625" x2="311.85485" y2="33.060640625" width="0.025" layer="94"/>
<wire x1="311.85485" y1="33.060640625" x2="312.106309375" y2="33.3121" width="0.025" layer="94"/>
<wire x1="312.106309375" y1="33.3121" x2="312.15203125" y2="33.373059375" width="0.025" layer="94"/>
<wire x1="312.15203125" y1="33.373059375" x2="312.174890625" y2="33.403540625" width="0.025" layer="94"/>
<wire x1="312.174890625" y1="33.403540625" x2="312.220609375" y2="33.4645" width="0.025" layer="94"/>
<wire x1="312.220609375" y1="33.4645" x2="312.258709375" y2="33.517840625" width="0.025" layer="94"/>
<wire x1="312.258709375" y1="33.517840625" x2="312.30443125" y2="33.58641875" width="0.025" layer="94"/>
<wire x1="312.30443125" y1="33.58641875" x2="312.334909375" y2="33.632140625" width="0.025" layer="94"/>
<wire x1="312.334909375" y1="33.632140625" x2="312.39586875" y2="33.72358125" width="0.025" layer="94"/>
<wire x1="312.39586875" y1="33.72358125" x2="312.42635" y2="33.7693" width="0.025" layer="94"/>
<wire x1="312.42635" y1="33.7693" x2="312.60923125" y2="34.135059375" width="0.025" layer="94"/>
<wire x1="312.60923125" y1="34.135059375" x2="312.65495" y2="34.249359375" width="0.025" layer="94"/>
<wire x1="312.65495" y1="34.249359375" x2="312.68108125" y2="34.3408" width="0.025" layer="94"/>
<wire x1="312.68108125" y1="34.3408" x2="312.7121" y2="34.4551" width="0.025" layer="94"/>
<wire x1="312.7121" y1="34.4551" x2="312.746390625" y2="34.61511875" width="0.025" layer="94"/>
<wire x1="312.746390625" y1="34.61511875" x2="312.76925" y2="34.72941875" width="0.025" layer="94"/>
<wire x1="312.76925" y1="34.72941875" x2="312.792109375" y2="34.889440625" width="0.025" layer="94"/>
<wire x1="312.792109375" y1="34.889440625" x2="312.809890625" y2="35.049459375" width="0.025" layer="94"/>
<wire x1="312.809890625" y1="35.049459375" x2="312.81673125" y2="35.118040625" width="0.025" layer="94"/>
<wire x1="312.81673125" y1="35.118040625" x2="312.823759375" y2="35.20948125" width="0.025" layer="94"/>
<wire x1="312.823759375" y1="35.20948125" x2="312.829040625" y2="35.278059375" width="0.025" layer="94"/>
<wire x1="312.829040625" y1="35.278059375" x2="312.83783125" y2="35.392359375" width="0.025" layer="94"/>
<wire x1="324.9625" y1="37.03828125" x2="325.251490625" y2="38.13625" width="0.025" layer="94"/>
<wire x1="325.251490625" y1="38.13625" x2="325.993759375" y2="40.935909375" width="0.025" layer="94"/>
<wire x1="325.993759375" y1="40.935909375" x2="325.90231875" y2="40.95876875" width="0.025" layer="94"/>
<wire x1="325.90231875" y1="40.95876875" x2="325.78801875" y2="40.98163125" width="0.025" layer="94"/>
<wire x1="325.78801875" y1="40.98163125" x2="325.63943125" y2="41.01591875" width="0.025" layer="94"/>
<wire x1="325.63943125" y1="41.01591875" x2="325.536559375" y2="41.03878125" width="0.025" layer="94"/>
<wire x1="325.536559375" y1="41.03878125" x2="325.3994" y2="41.07306875" width="0.025" layer="94"/>
<wire x1="325.3994" y1="41.07306875" x2="325.319390625" y2="41.09593125" width="0.025" layer="94"/>
<wire x1="325.319390625" y1="41.09593125" x2="325.21651875" y2="41.13021875" width="0.025" layer="94"/>
<wire x1="325.21651875" y1="41.13021875" x2="325.06793125" y2="41.175940625" width="0.025" layer="94"/>
<wire x1="325.06793125" y1="41.175940625" x2="324.97076875" y2="41.21251875" width="0.025" layer="94"/>
<wire x1="324.97076875" y1="41.21251875" x2="324.747890625" y2="41.30166875" width="0.025" layer="94"/>
<wire x1="324.747890625" y1="41.30166875" x2="324.565009375" y2="41.38168125" width="0.025" layer="94"/>
<wire x1="324.565009375" y1="41.38168125" x2="324.45185" y2="41.438259375" width="0.025" layer="94"/>
<wire x1="324.45185" y1="41.438259375" x2="324.393559375" y2="41.47311875" width="0.025" layer="94"/>
<wire x1="324.393559375" y1="41.47311875" x2="324.347840625" y2="41.49598125" width="0.025" layer="94"/>
<wire x1="324.347840625" y1="41.49598125" x2="324.18781875" y2="41.58741875" width="0.025" layer="94"/>
<wire x1="324.18781875" y1="41.58741875" x2="324.08495" y2="41.656" width="0.025" layer="94"/>
<wire x1="324.08495" y1="41.656" x2="323.85635" y2="41.82745" width="0.025" layer="94"/>
<wire x1="323.85635" y1="41.82745" x2="323.776340625" y2="41.89603125" width="0.025" layer="94"/>
<wire x1="323.776340625" y1="41.89603125" x2="323.662040625" y2="42.01033125" width="0.025" layer="94"/>
<wire x1="323.662040625" y1="42.01033125" x2="323.593459375" y2="42.090340625" width="0.025" layer="94"/>
<wire x1="323.593459375" y1="42.090340625" x2="323.547740625" y2="42.147490625" width="0.025" layer="94"/>
<wire x1="323.547740625" y1="42.147490625" x2="323.52488125" y2="42.18178125" width="0.025" layer="94"/>
<wire x1="323.52488125" y1="42.18178125" x2="323.490590625" y2="42.2275" width="0.025" layer="94"/>
<wire x1="323.490590625" y1="42.2275" x2="323.46773125" y2="42.261790625" width="0.025" layer="94"/>
<wire x1="323.46773125" y1="42.261790625" x2="323.433440625" y2="42.318940625" width="0.025" layer="94"/>
<wire x1="323.433440625" y1="42.318940625" x2="323.41058125" y2="42.35323125" width="0.025" layer="94"/>
<wire x1="323.41058125" y1="42.35323125" x2="323.354" y2="42.466390625" width="0.025" layer="94"/>
<wire x1="323.354" y1="42.466390625" x2="323.33056875" y2="42.52468125" width="0.025" layer="94"/>
<wire x1="323.33056875" y1="42.52468125" x2="323.29628125" y2="42.62755" width="0.025" layer="94"/>
<wire x1="323.29628125" y1="42.62755" x2="323.28485" y2="42.67326875" width="0.025" layer="94"/>
<wire x1="323.28485" y1="42.67326875" x2="323.27341875" y2="42.73041875" width="0.025" layer="94"/>
<wire x1="323.27341875" y1="42.73041875" x2="323.261990625" y2="42.81043125" width="0.025" layer="94"/>
<wire x1="323.261990625" y1="42.81043125" x2="323.250559375" y2="42.936159375" width="0.025" layer="94"/>
<wire x1="323.250559375" y1="42.936159375" x2="323.261990625" y2="43.061890625" width="0.025" layer="94"/>
<wire x1="323.261990625" y1="43.061890625" x2="323.27341875" y2="43.13046875" width="0.025" layer="94"/>
<wire x1="323.27341875" y1="43.13046875" x2="323.28485" y2="43.18761875" width="0.025" layer="94"/>
<wire x1="323.28485" y1="43.18761875" x2="323.29628125" y2="43.233340625" width="0.025" layer="94"/>
<wire x1="323.29628125" y1="43.233340625" x2="323.33056875" y2="43.336209375" width="0.025" layer="94"/>
<wire x1="323.33056875" y1="43.336209375" x2="323.35343125" y2="43.393359375" width="0.025" layer="94"/>
<wire x1="323.35343125" y1="43.393359375" x2="323.41058125" y2="43.507659375" width="0.025" layer="94"/>
<wire x1="323.41058125" y1="43.507659375" x2="323.44486875" y2="43.564809375" width="0.025" layer="94"/>
<wire x1="323.44486875" y1="43.564809375" x2="323.479159375" y2="43.61053125" width="0.025" layer="94"/>
<wire x1="323.479159375" y1="43.61053125" x2="323.536309375" y2="43.690540625" width="0.025" layer="94"/>
<wire x1="323.536309375" y1="43.690540625" x2="323.62775" y2="43.793409375" width="0.025" layer="94"/>
<wire x1="323.62775" y1="43.793409375" x2="323.78776875" y2="43.95343125" width="0.025" layer="94"/>
<wire x1="323.78776875" y1="43.95343125" x2="323.87806875" y2="44.0323" width="0.025" layer="94"/>
<wire x1="323.87806875" y1="44.0323" x2="323.9375" y2="44.08001875" width="0.025" layer="94"/>
<wire x1="323.9375" y1="44.08001875" x2="324.0278" y2="44.147740625" width="0.025" layer="94"/>
<wire x1="324.0278" y1="44.147740625" x2="324.164959375" y2="44.23918125" width="0.025" layer="94"/>
<wire x1="324.164959375" y1="44.23918125" x2="324.290690625" y2="44.319190625" width="0.025" layer="94"/>
<wire x1="324.290690625" y1="44.319190625" x2="324.393559375" y2="44.376340625" width="0.025" layer="94"/>
<wire x1="324.393559375" y1="44.376340625" x2="324.62101875" y2="44.49006875" width="0.025" layer="94"/>
<wire x1="324.62101875" y1="44.49006875" x2="324.72503125" y2="44.536359375" width="0.025" layer="94"/>
<wire x1="324.72503125" y1="44.536359375" x2="325.009640625" y2="44.6502" width="0.025" layer="94"/>
<wire x1="325.009640625" y1="44.6502" x2="325.10908125" y2="44.68343125" width="0.025" layer="94"/>
<wire x1="325.10908125" y1="44.68343125" x2="325.287390625" y2="44.742859375" width="0.025" layer="94"/>
<wire x1="325.287390625" y1="44.742859375" x2="325.35368125" y2="44.764959375" width="0.025" layer="94"/>
<wire x1="325.35368125" y1="44.764959375" x2="325.43483125" y2="44.7881" width="0.025" layer="94"/>
<wire x1="325.43483125" y1="44.7881" x2="325.5137" y2="44.81068125" width="0.025" layer="94"/>
<wire x1="325.5137" y1="44.81068125" x2="325.741159375" y2="44.867540625" width="0.025" layer="94"/>
<wire x1="325.741159375" y1="44.867540625" x2="325.84516875" y2="44.890690625" width="0.025" layer="94"/>
<wire x1="325.84516875" y1="44.890690625" x2="326.07376875" y2="44.936409375" width="0.025" layer="94"/>
<wire x1="326.07376875" y1="44.936409375" x2="326.14235" y2="44.947840625" width="0.025" layer="94"/>
<wire x1="326.14235" y1="44.947840625" x2="326.222359375" y2="44.95926875" width="0.025" layer="94"/>
<wire x1="326.222359375" y1="44.95926875" x2="326.35951875" y2="44.98213125" width="0.025" layer="94"/>
<wire x1="326.35951875" y1="44.98213125" x2="326.43953125" y2="44.993559375" width="0.025" layer="94"/>
<wire x1="326.43953125" y1="44.993559375" x2="326.622409375" y2="45.01641875" width="0.025" layer="94"/>
<wire x1="326.622409375" y1="45.01641875" x2="326.72528125" y2="45.02785" width="0.025" layer="94"/>
<wire x1="326.72528125" y1="45.02785" x2="326.862440625" y2="45.03928125" width="0.025" layer="94"/>
<wire x1="326.862440625" y1="45.03928125" x2="326.862440625" y2="44.764959375" width="0.025" layer="94"/>
<wire x1="326.862440625" y1="44.764959375" x2="326.75956875" y2="44.75353125" width="0.025" layer="94"/>
<wire x1="326.75956875" y1="44.75353125" x2="326.519540625" y2="44.719240625" width="0.025" layer="94"/>
<wire x1="326.519540625" y1="44.719240625" x2="326.450959375" y2="44.707809375" width="0.025" layer="94"/>
<wire x1="326.450959375" y1="44.707809375" x2="326.32523125" y2="44.68495" width="0.025" layer="94"/>
<wire x1="326.32523125" y1="44.68495" x2="326.09663125" y2="44.63923125" width="0.025" layer="94"/>
<wire x1="326.09663125" y1="44.63923125" x2="325.993759375" y2="44.61636875" width="0.025" layer="94"/>
<wire x1="325.993759375" y1="44.61636875" x2="325.84516875" y2="44.58208125" width="0.025" layer="94"/>
<wire x1="325.84516875" y1="44.58208125" x2="325.67371875" y2="44.536359375" width="0.025" layer="94"/>
<wire x1="325.67371875" y1="44.536359375" x2="325.520559375" y2="44.4926" width="0.025" layer="94"/>
<wire x1="325.520559375" y1="44.4926" x2="325.4154" y2="44.45786875" width="0.025" layer="94"/>
<wire x1="325.4154" y1="44.45786875" x2="325.23938125" y2="44.3992" width="0.025" layer="94"/>
<wire x1="325.23938125" y1="44.3992" x2="325.06793125" y2="44.33061875" width="0.025" layer="94"/>
<wire x1="325.06793125" y1="44.33061875" x2="324.93076875" y2="44.27346875" width="0.025" layer="94"/>
<wire x1="324.93076875" y1="44.27346875" x2="324.679309375" y2="44.147740625" width="0.025" layer="94"/>
<wire x1="324.679309375" y1="44.147740625" x2="324.531859375" y2="44.05695" width="0.025" layer="94"/>
<wire x1="324.531859375" y1="44.05695" x2="324.462140625" y2="44.01058125" width="0.025" layer="94"/>
<wire x1="324.462140625" y1="44.01058125" x2="324.279259375" y2="43.87341875" width="0.025" layer="94"/>
<wire x1="324.279259375" y1="43.87341875" x2="324.164959375" y2="43.77055" width="0.025" layer="94"/>
<wire x1="324.164959375" y1="43.77055" x2="324.03923125" y2="43.64481875" width="0.025" layer="94"/>
<wire x1="324.03923125" y1="43.64481875" x2="323.97065" y2="43.564809375" width="0.025" layer="94"/>
<wire x1="323.97065" y1="43.564809375" x2="323.90206875" y2="43.47336875" width="0.025" layer="94"/>
<wire x1="323.90206875" y1="43.47336875" x2="323.86778125" y2="43.41621875" width="0.025" layer="94"/>
<wire x1="323.86778125" y1="43.41621875" x2="323.822059375" y2="43.336209375" width="0.025" layer="94"/>
<wire x1="323.822059375" y1="43.336209375" x2="323.78776875" y2="43.26763125" width="0.025" layer="94"/>
<wire x1="323.78776875" y1="43.26763125" x2="323.75348125" y2="43.176190625" width="0.025" layer="94"/>
<wire x1="323.75348125" y1="43.176190625" x2="323.73061875" y2="43.107609375" width="0.025" layer="94"/>
<wire x1="323.73061875" y1="43.107609375" x2="323.707759375" y2="43.004740625" width="0.025" layer="94"/>
<wire x1="323.707759375" y1="43.004740625" x2="323.69633125" y2="42.90186875" width="0.025" layer="94"/>
<wire x1="323.69633125" y1="42.90186875" x2="323.6849" y2="42.821859375" width="0.025" layer="94"/>
<wire x1="323.6849" y1="42.821859375" x2="323.6849" y2="42.776140625" width="0.025" layer="94"/>
<wire x1="323.6849" y1="42.776140625" x2="323.69633125" y2="42.650409375" width="0.025" layer="94"/>
<wire x1="323.69633125" y1="42.650409375" x2="323.73061875" y2="42.51325" width="0.025" layer="94"/>
<wire x1="323.73061875" y1="42.51325" x2="323.75348125" y2="42.433240625" width="0.025" layer="94"/>
<wire x1="323.75348125" y1="42.433240625" x2="323.7992" y2="42.33036875" width="0.025" layer="94"/>
<wire x1="323.7992" y1="42.33036875" x2="323.822059375" y2="42.28465" width="0.025" layer="94"/>
<wire x1="323.822059375" y1="42.28465" x2="323.890640625" y2="42.18178125" width="0.025" layer="94"/>
<wire x1="323.890640625" y1="42.18178125" x2="323.95921875" y2="42.090340625" width="0.025" layer="94"/>
<wire x1="323.95921875" y1="42.090340625" x2="324.004940625" y2="42.033190625" width="0.025" layer="94"/>
<wire x1="324.004940625" y1="42.033190625" x2="324.08495" y2="41.95318125" width="0.025" layer="94"/>
<wire x1="324.08495" y1="41.95318125" x2="324.176390625" y2="41.87316875" width="0.025" layer="94"/>
<wire x1="324.176390625" y1="41.87316875" x2="324.24496875" y2="41.81601875" width="0.025" layer="94"/>
<wire x1="324.24496875" y1="41.81601875" x2="324.30211875" y2="41.7703" width="0.025" layer="94"/>
<wire x1="324.30211875" y1="41.7703" x2="324.347840625" y2="41.736009375" width="0.025" layer="94"/>
<wire x1="324.347840625" y1="41.736009375" x2="324.41641875" y2="41.690290625" width="0.025" layer="94"/>
<wire x1="324.41641875" y1="41.690290625" x2="324.53071875" y2="41.61028125" width="0.025" layer="94"/>
<wire x1="324.53071875" y1="41.61028125" x2="324.64501875" y2="41.5417" width="0.025" layer="94"/>
<wire x1="324.64501875" y1="41.5417" x2="324.78218125" y2="41.461690625" width="0.025" layer="94"/>
<wire x1="324.78218125" y1="41.461690625" x2="324.919340625" y2="41.393109375" width="0.025" layer="94"/>
<wire x1="324.919340625" y1="41.393109375" x2="325.022209375" y2="41.347390625" width="0.025" layer="94"/>
<wire x1="325.022209375" y1="41.347390625" x2="325.079359375" y2="41.32453125" width="0.025" layer="94"/>
<wire x1="325.079359375" y1="41.32453125" x2="325.15936875" y2="41.290240625" width="0.025" layer="94"/>
<wire x1="325.15936875" y1="41.290240625" x2="325.250809375" y2="41.25595" width="0.025" layer="94"/>
<wire x1="325.250809375" y1="41.25595" x2="325.38796875" y2="41.21023125" width="0.025" layer="94"/>
<wire x1="325.38796875" y1="41.21023125" x2="325.46798125" y2="41.18736875" width="0.025" layer="94"/>
<wire x1="325.46798125" y1="41.18736875" x2="325.58228125" y2="41.15308125" width="0.025" layer="94"/>
<wire x1="325.58228125" y1="41.15308125" x2="325.708009375" y2="41.118790625" width="0.025" layer="94"/>
<wire x1="325.708009375" y1="41.118790625" x2="325.82116875" y2="41.087640625" width="0.025" layer="94"/>
<wire x1="325.82116875" y1="41.087640625" x2="326.01661875" y2="41.03878125" width="0.025" layer="94"/>
<wire x1="326.01661875" y1="41.03878125" x2="326.062340625" y2="41.18736875" width="0.025" layer="94"/>
<wire x1="326.062340625" y1="41.18736875" x2="326.02805" y2="41.1988" width="0.025" layer="94"/>
<wire x1="326.02805" y1="41.1988" x2="325.78801875" y2="41.26738125" width="0.025" layer="94"/>
<wire x1="325.78801875" y1="41.26738125" x2="325.5137" y2="41.35881875" width="0.025" layer="94"/>
<wire x1="325.5137" y1="41.35881875" x2="325.433690625" y2="41.393109375" width="0.025" layer="94"/>
<wire x1="325.433690625" y1="41.393109375" x2="325.319390625" y2="41.43883125" width="0.025" layer="94"/>
<wire x1="325.319390625" y1="41.43883125" x2="325.233659375" y2="41.481690625" width="0.025" layer="94"/>
<wire x1="325.233659375" y1="41.481690625" x2="325.10221875" y2="41.55313125" width="0.025" layer="94"/>
<wire x1="325.10221875" y1="41.55313125" x2="325.01078125" y2="41.614090625" width="0.025" layer="94"/>
<wire x1="325.01078125" y1="41.614090625" x2="324.93076875" y2="41.66743125" width="0.025" layer="94"/>
<wire x1="324.93076875" y1="41.66743125" x2="324.88505" y2="41.70171875" width="0.025" layer="94"/>
<wire x1="324.88505" y1="41.70171875" x2="324.8279" y2="41.747440625" width="0.025" layer="94"/>
<wire x1="324.8279" y1="41.747440625" x2="324.78218125" y2="41.78173125" width="0.025" layer="94"/>
<wire x1="324.78218125" y1="41.78173125" x2="324.66788125" y2="41.87316875" width="0.025" layer="94"/>
<wire x1="324.66788125" y1="41.87316875" x2="324.507859375" y2="42.033190625" width="0.025" layer="94"/>
<wire x1="324.507859375" y1="42.033190625" x2="324.47356875" y2="42.078909375" width="0.025" layer="94"/>
<wire x1="324.47356875" y1="42.078909375" x2="324.42785" y2="42.147490625" width="0.025" layer="94"/>
<wire x1="324.42785" y1="42.147490625" x2="324.393559375" y2="42.204640625" width="0.025" layer="94"/>
<wire x1="324.393559375" y1="42.204640625" x2="324.3707" y2="42.250359375" width="0.025" layer="94"/>
<wire x1="324.3707" y1="42.250359375" x2="324.336409375" y2="42.33036875" width="0.025" layer="94"/>
<wire x1="324.336409375" y1="42.33036875" x2="324.31355" y2="42.41038125" width="0.025" layer="94"/>
<wire x1="324.31355" y1="42.41038125" x2="324.30211875" y2="42.44466875" width="0.025" layer="94"/>
<wire x1="324.30211875" y1="42.44466875" x2="324.290690625" y2="42.50181875" width="0.025" layer="94"/>
<wire x1="324.290690625" y1="42.50181875" x2="324.279259375" y2="42.604690625" width="0.025" layer="94"/>
<wire x1="324.279259375" y1="42.604690625" x2="324.279259375" y2="42.764709375" width="0.025" layer="94"/>
<wire x1="324.279259375" y1="42.764709375" x2="324.290690625" y2="42.84471875" width="0.025" layer="94"/>
<wire x1="324.290690625" y1="42.84471875" x2="324.30211875" y2="42.90186875" width="0.025" layer="94"/>
<wire x1="324.30211875" y1="42.90186875" x2="324.347840625" y2="43.03903125" width="0.025" layer="94"/>
<wire x1="324.347840625" y1="43.03903125" x2="324.38213125" y2="43.107609375" width="0.025" layer="94"/>
<wire x1="324.38213125" y1="43.107609375" x2="324.41641875" y2="43.164759375" width="0.025" layer="94"/>
<wire x1="324.41641875" y1="43.164759375" x2="324.519290625" y2="43.30191875" width="0.025" layer="94"/>
<wire x1="324.519290625" y1="43.30191875" x2="324.70216875" y2="43.4848" width="0.025" layer="94"/>
<wire x1="324.70216875" y1="43.4848" x2="324.77075" y2="43.54195" width="0.025" layer="94"/>
<wire x1="324.77075" y1="43.54195" x2="324.81646875" y2="43.576240625" width="0.025" layer="94"/>
<wire x1="324.81646875" y1="43.576240625" x2="324.98678125" y2="43.68978125" width="0.025" layer="94"/>
<wire x1="324.98678125" y1="43.68978125" x2="325.10108125" y2="43.75843125" width="0.025" layer="94"/>
<wire x1="325.10108125" y1="43.75843125" x2="325.177659375" y2="43.796840625" width="0.025" layer="94"/>
<wire x1="325.177659375" y1="43.796840625" x2="325.376540625" y2="43.89628125" width="0.025" layer="94"/>
<wire x1="325.376540625" y1="43.89628125" x2="325.45655" y2="43.93056875" width="0.025" layer="94"/>
<wire x1="325.45655" y1="43.93056875" x2="325.53426875" y2="43.96143125" width="0.025" layer="94"/>
<wire x1="325.53426875" y1="43.96143125" x2="325.68515" y2="44.022009375" width="0.025" layer="94"/>
<wire x1="325.68515" y1="44.022009375" x2="325.82116875" y2="44.06735" width="0.025" layer="94"/>
<wire x1="325.82116875" y1="44.06735" x2="325.94346875" y2="44.10235" width="0.025" layer="94"/>
<wire x1="325.94346875" y1="44.10235" x2="326.06348125" y2="44.136590625" width="0.025" layer="94"/>
<wire x1="326.06348125" y1="44.136590625" x2="326.15378125" y2="44.15916875" width="0.025" layer="94"/>
<wire x1="326.15378125" y1="44.15916875" x2="326.230359375" y2="44.17631875" width="0.025" layer="94"/>
<wire x1="326.230359375" y1="44.17631875" x2="326.35951875" y2="44.204890625" width="0.025" layer="94"/>
<wire x1="326.35951875" y1="44.204890625" x2="326.47381875" y2="44.22775" width="0.025" layer="94"/>
<wire x1="326.47381875" y1="44.22775" x2="326.622409375" y2="44.250609375" width="0.025" layer="94"/>
<wire x1="326.622409375" y1="44.250609375" x2="326.78243125" y2="44.27346875" width="0.025" layer="94"/>
<wire x1="326.78243125" y1="44.27346875" x2="326.89673125" y2="44.2849" width="0.025" layer="94"/>
<wire x1="326.89673125" y1="44.2849" x2="327.091040625" y2="44.29633125" width="0.025" layer="94"/>
<wire x1="327.091040625" y1="44.29633125" x2="327.091040625" y2="44.033440625" width="0.025" layer="94"/>
<wire x1="327.091040625" y1="44.033440625" x2="326.919590625" y2="44.033440625" width="0.025" layer="94"/>
<wire x1="326.919590625" y1="44.033440625" x2="326.793859375" y2="44.022009375" width="0.025" layer="94"/>
<wire x1="326.793859375" y1="44.022009375" x2="326.71385" y2="44.01058125" width="0.025" layer="94"/>
<wire x1="326.71385" y1="44.01058125" x2="326.576690625" y2="43.98771875" width="0.025" layer="94"/>
<wire x1="326.576690625" y1="43.98771875" x2="326.462390625" y2="43.964859375" width="0.025" layer="94"/>
<wire x1="326.462390625" y1="43.964859375" x2="326.28065" y2="43.91941875" width="0.025" layer="94"/>
<wire x1="326.28065" y1="43.91941875" x2="326.12063125" y2="43.87375" width="0.025" layer="94"/>
<wire x1="326.12063125" y1="43.87375" x2="326.02805" y2="43.850559375" width="0.025" layer="94"/>
<wire x1="326.02805" y1="43.850559375" x2="325.8566" y2="43.793409375" width="0.025" layer="94"/>
<wire x1="325.8566" y1="43.793409375" x2="325.708009375" y2="43.73396875" width="0.025" layer="94"/>
<wire x1="325.708009375" y1="43.73396875" x2="325.604" y2="43.68996875" width="0.025" layer="94"/>
<wire x1="325.604" y1="43.68996875" x2="325.536559375" y2="43.65625" width="0.025" layer="94"/>
<wire x1="325.536559375" y1="43.65625" x2="325.45655" y2="43.61053125" width="0.025" layer="94"/>
<wire x1="325.45655" y1="43.61053125" x2="325.341109375" y2="43.541190625" width="0.025" layer="94"/>
<wire x1="325.341109375" y1="43.541190625" x2="325.205090625" y2="43.450509375" width="0.025" layer="94"/>
<wire x1="325.205090625" y1="43.450509375" x2="325.11365" y2="43.38193125" width="0.025" layer="94"/>
<wire x1="325.11365" y1="43.38193125" x2="325.033640625" y2="43.32478125" width="0.025" layer="94"/>
<wire x1="325.033640625" y1="43.32478125" x2="324.86133125" y2="43.152190625" width="0.025" layer="94"/>
<wire x1="324.86133125" y1="43.152190625" x2="324.828759375" y2="43.10875" width="0.025" layer="94"/>
<wire x1="324.828759375" y1="43.10875" x2="324.75931875" y2="43.004740625" width="0.025" layer="94"/>
<wire x1="324.75931875" y1="43.004740625" x2="324.70216875" y2="42.890440625" width="0.025" layer="94"/>
<wire x1="324.70216875" y1="42.890440625" x2="324.66788125" y2="42.78756875" width="0.025" layer="94"/>
<wire x1="324.66788125" y1="42.78756875" x2="324.65645" y2="42.74185" width="0.025" layer="94"/>
<wire x1="324.65645" y1="42.74185" x2="324.633590625" y2="42.626409375" width="0.025" layer="94"/>
<wire x1="324.633590625" y1="42.626409375" x2="324.633590625" y2="42.50181875" width="0.025" layer="94"/>
<wire x1="324.633590625" y1="42.50181875" x2="324.64501875" y2="42.421809375" width="0.025" layer="94"/>
<wire x1="324.64501875" y1="42.421809375" x2="324.66788125" y2="42.33036875" width="0.025" layer="94"/>
<wire x1="324.66788125" y1="42.33036875" x2="324.690740625" y2="42.261790625" width="0.025" layer="94"/>
<wire x1="324.690740625" y1="42.261790625" x2="324.7136" y2="42.204640625" width="0.025" layer="94"/>
<wire x1="324.7136" y1="42.204640625" x2="324.747890625" y2="42.147490625" width="0.025" layer="94"/>
<wire x1="324.747890625" y1="42.147490625" x2="324.793609375" y2="42.06748125" width="0.025" layer="94"/>
<wire x1="324.793609375" y1="42.06748125" x2="324.874759375" y2="41.96346875" width="0.025" layer="94"/>
<wire x1="324.874759375" y1="41.96346875" x2="324.976490625" y2="41.861740625" width="0.025" layer="94"/>
<wire x1="324.976490625" y1="41.861740625" x2="325.090790625" y2="41.75886875" width="0.025" layer="94"/>
<wire x1="325.090790625" y1="41.75886875" x2="325.193659375" y2="41.678859375" width="0.025" layer="94"/>
<wire x1="325.193659375" y1="41.678859375" x2="325.27366875" y2="41.621709375" width="0.025" layer="94"/>
<wire x1="325.27366875" y1="41.621709375" x2="325.433690625" y2="41.53026875" width="0.025" layer="94"/>
<wire x1="325.433690625" y1="41.53026875" x2="325.58228125" y2="41.461690625" width="0.025" layer="94"/>
<wire x1="325.58228125" y1="41.461690625" x2="325.68515" y2="41.41596875" width="0.025" layer="94"/>
<wire x1="325.68515" y1="41.41596875" x2="325.84516875" y2="41.35881875" width="0.025" layer="94"/>
<wire x1="325.84516875" y1="41.35881875" x2="325.91718125" y2="41.33481875" width="0.025" layer="94"/>
<wire x1="325.91718125" y1="41.33481875" x2="325.969759375" y2="41.317290625" width="0.025" layer="94"/>
<wire x1="325.969759375" y1="41.317290625" x2="326.084059375" y2="41.279190625" width="0.025" layer="94"/>
<wire x1="326.084059375" y1="41.279190625" x2="326.11925" y2="41.437690625" width="0.025" layer="94"/>
<wire x1="326.11925" y1="41.437690625" x2="325.99261875" y2="41.49646875" width="0.025" layer="94"/>
<wire x1="325.99261875" y1="41.49646875" x2="325.91375" y2="41.53026875" width="0.025" layer="94"/>
<wire x1="325.91375" y1="41.53026875" x2="325.81088125" y2="41.575990625" width="0.025" layer="94"/>
<wire x1="325.81088125" y1="41.575990625" x2="325.652" y2="41.66678125" width="0.025" layer="94"/>
<wire x1="325.652" y1="41.66678125" x2="325.55941875" y2="41.72458125" width="0.025" layer="94"/>
<wire x1="325.55941875" y1="41.72458125" x2="325.422259375" y2="41.83888125" width="0.025" layer="94"/>
<wire x1="325.422259375" y1="41.83888125" x2="325.365109375" y2="41.89603125" width="0.025" layer="94"/>
<wire x1="325.365109375" y1="41.89603125" x2="325.307959375" y2="41.964609375" width="0.025" layer="94"/>
<wire x1="325.307959375" y1="41.964609375" x2="325.21651875" y2="42.10176875" width="0.025" layer="94"/>
<wire x1="325.21651875" y1="42.10176875" x2="325.18223125" y2="42.18178125" width="0.025" layer="94"/>
<wire x1="325.18223125" y1="42.18178125" x2="325.15936875" y2="42.250359375" width="0.025" layer="94"/>
<wire x1="325.15936875" y1="42.250359375" x2="325.147940625" y2="42.318940625" width="0.025" layer="94"/>
<wire x1="325.147940625" y1="42.318940625" x2="325.147940625" y2="42.478959375" width="0.025" layer="94"/>
<wire x1="325.147940625" y1="42.478959375" x2="325.15936875" y2="42.547540625" width="0.025" layer="94"/>
<wire x1="325.15936875" y1="42.547540625" x2="325.1708" y2="42.593259375" width="0.025" layer="94"/>
<wire x1="325.1708" y1="42.593259375" x2="325.193659375" y2="42.650409375" width="0.025" layer="94"/>
<wire x1="325.193659375" y1="42.650409375" x2="325.22795" y2="42.718990625" width="0.025" layer="94"/>
<wire x1="325.22795" y1="42.718990625" x2="325.250809375" y2="42.75328125" width="0.025" layer="94"/>
<wire x1="325.250809375" y1="42.75328125" x2="325.2851" y2="42.799" width="0.025" layer="94"/>
<wire x1="325.2851" y1="42.799" x2="325.44511875" y2="42.95901875" width="0.025" layer="94"/>
<wire x1="325.44511875" y1="42.95901875" x2="325.5377" y2="43.028359375" width="0.025" layer="94"/>
<wire x1="325.5377" y1="43.028359375" x2="325.605140625" y2="43.07331875" width="0.025" layer="94"/>
<wire x1="325.605140625" y1="43.07331875" x2="325.68858125" y2="43.123609375" width="0.025" layer="94"/>
<wire x1="325.68858125" y1="43.123609375" x2="325.776590625" y2="43.176190625" width="0.025" layer="94"/>
<wire x1="325.776590625" y1="43.176190625" x2="325.84516875" y2="43.21048125" width="0.025" layer="94"/>
<wire x1="325.84516875" y1="43.21048125" x2="326.005190625" y2="43.279059375" width="0.025" layer="94"/>
<wire x1="326.005190625" y1="43.279059375" x2="326.09663125" y2="43.31335" width="0.025" layer="94"/>
<wire x1="326.09663125" y1="43.31335" x2="326.37095" y2="43.404790625" width="0.025" layer="94"/>
<wire x1="326.37095" y1="43.404790625" x2="326.49668125" y2="43.43908125" width="0.025" layer="94"/>
<wire x1="326.49668125" y1="43.43908125" x2="326.56411875" y2="43.454640625" width="0.025" layer="94"/>
<wire x1="326.56411875" y1="43.454640625" x2="326.64526875" y2="43.47336875" width="0.025" layer="94"/>
<wire x1="326.64526875" y1="43.47336875" x2="326.75956875" y2="43.49623125" width="0.025" layer="94"/>
<wire x1="326.75956875" y1="43.49623125" x2="326.93101875" y2="43.519090625" width="0.025" layer="94"/>
<wire x1="326.93101875" y1="43.519090625" x2="327.06818125" y2="43.53051875" width="0.025" layer="94"/>
<wire x1="327.06818125" y1="43.53051875" x2="327.205340625" y2="43.53051875" width="0.025" layer="94"/>
<wire x1="327.205340625" y1="43.53051875" x2="327.205340625" y2="43.35906875" width="0.025" layer="94"/>
<wire x1="327.205340625" y1="43.35906875" x2="327.17105" y2="43.35906875" width="0.025" layer="94"/>
<wire x1="327.17105" y1="43.35906875" x2="327.04531875" y2="43.347640625" width="0.025" layer="94"/>
<wire x1="327.04531875" y1="43.347640625" x2="326.95388125" y2="43.336209375" width="0.025" layer="94"/>
<wire x1="326.95388125" y1="43.336209375" x2="326.81671875" y2="43.31335" width="0.025" layer="94"/>
<wire x1="326.81671875" y1="43.31335" x2="326.71385" y2="43.290490625" width="0.025" layer="94"/>
<wire x1="326.71385" y1="43.290490625" x2="326.565259375" y2="43.2562" width="0.025" layer="94"/>
<wire x1="326.565259375" y1="43.2562" x2="326.48525" y2="43.233340625" width="0.025" layer="94"/>
<wire x1="326.48525" y1="43.233340625" x2="326.41666875" y2="43.21048125" width="0.025" layer="94"/>
<wire x1="326.41666875" y1="43.21048125" x2="326.32523125" y2="43.176190625" width="0.025" layer="94"/>
<wire x1="326.32523125" y1="43.176190625" x2="326.21206875" y2="43.13093125" width="0.025" layer="94"/>
<wire x1="326.21206875" y1="43.13093125" x2="326.005190625" y2="43.0276" width="0.025" layer="94"/>
<wire x1="326.005190625" y1="43.0276" x2="325.948040625" y2="42.993309375" width="0.025" layer="94"/>
<wire x1="325.948040625" y1="42.993309375" x2="325.84516875" y2="42.92473125" width="0.025" layer="94"/>
<wire x1="325.84516875" y1="42.92473125" x2="325.79945" y2="42.890440625" width="0.025" layer="94"/>
<wire x1="325.79945" y1="42.890440625" x2="325.7423" y2="42.84471875" width="0.025" layer="94"/>
<wire x1="325.7423" y1="42.84471875" x2="325.708009375" y2="42.81043125" width="0.025" layer="94"/>
<wire x1="325.708009375" y1="42.81043125" x2="325.63943125" y2="42.73041875" width="0.025" layer="94"/>
<wire x1="325.63943125" y1="42.73041875" x2="325.593709375" y2="42.661840625" width="0.025" layer="94"/>
<wire x1="325.593709375" y1="42.661840625" x2="325.55941875" y2="42.593259375" width="0.025" layer="94"/>
<wire x1="325.55941875" y1="42.593259375" x2="325.536559375" y2="42.536109375" width="0.025" layer="94"/>
<wire x1="325.536559375" y1="42.536109375" x2="325.5137" y2="42.4561" width="0.025" layer="94"/>
<wire x1="325.5137" y1="42.4561" x2="325.50226875" y2="42.39895" width="0.025" layer="94"/>
<wire x1="325.50226875" y1="42.39895" x2="325.50226875" y2="42.261790625" width="0.025" layer="94"/>
<wire x1="325.50226875" y1="42.261790625" x2="325.5137" y2="42.204640625" width="0.025" layer="94"/>
<wire x1="325.5137" y1="42.204640625" x2="325.52513125" y2="42.15891875" width="0.025" layer="94"/>
<wire x1="325.52513125" y1="42.15891875" x2="325.547609375" y2="42.09148125" width="0.025" layer="94"/>
<wire x1="325.547609375" y1="42.09148125" x2="325.57085" y2="42.04461875" width="0.025" layer="94"/>
<wire x1="325.57085" y1="42.04461875" x2="325.605140625" y2="41.98746875" width="0.025" layer="94"/>
<wire x1="325.605140625" y1="41.98746875" x2="325.67371875" y2="41.89603125" width="0.025" layer="94"/>
<wire x1="325.67371875" y1="41.89603125" x2="325.75373125" y2="41.81601875" width="0.025" layer="94"/>
<wire x1="325.75373125" y1="41.81601875" x2="325.84516875" y2="41.747440625" width="0.025" layer="94"/>
<wire x1="325.84516875" y1="41.747440625" x2="325.95833125" y2="41.67955" width="0.025" layer="94"/>
<wire x1="325.95833125" y1="41.67955" x2="326.04061875" y2="41.63256875" width="0.025" layer="94"/>
<wire x1="326.04061875" y1="41.63256875" x2="326.108059375" y2="41.59885" width="0.025" layer="94"/>
<wire x1="326.108059375" y1="41.59885" x2="326.165509375" y2="41.57713125" width="0.025" layer="94"/>
<wire x1="326.165509375" y1="41.57713125" x2="326.35951875" y2="42.307509375" width="0.025" layer="94"/>
<wire x1="326.35951875" y1="42.307509375" x2="328.8284" y2="42.307509375" width="0.025" layer="94"/>
<wire x1="328.8284" y1="42.307509375" x2="329.022709375" y2="41.575990625" width="0.025" layer="94"/>
<wire x1="329.022709375" y1="41.575990625" x2="329.087859375" y2="41.597140625" width="0.025" layer="94"/>
<wire x1="329.087859375" y1="41.597140625" x2="329.14158125" y2="41.624" width="0.025" layer="94"/>
<wire x1="329.14158125" y1="41.624" x2="329.25245" y2="41.67961875" width="0.025" layer="94"/>
<wire x1="329.25245" y1="41.67961875" x2="329.42161875" y2="41.7924" width="0.025" layer="94"/>
<wire x1="329.42161875" y1="41.7924" x2="329.548490625" y2="41.918890625" width="0.025" layer="94"/>
<wire x1="329.548490625" y1="41.918890625" x2="329.6285" y2="42.033190625" width="0.025" layer="94"/>
<wire x1="329.6285" y1="42.033190625" x2="329.662790625" y2="42.1132" width="0.025" layer="94"/>
<wire x1="329.662790625" y1="42.1132" x2="329.68593125" y2="42.18291875" width="0.025" layer="94"/>
<wire x1="329.68593125" y1="42.18291875" x2="329.69708125" y2="42.2275" width="0.025" layer="94"/>
<wire x1="329.69708125" y1="42.2275" x2="329.708509375" y2="42.307509375" width="0.025" layer="94"/>
<wire x1="329.708509375" y1="42.307509375" x2="329.708509375" y2="42.35323125" width="0.025" layer="94"/>
<wire x1="329.708509375" y1="42.35323125" x2="329.69708125" y2="42.433240625" width="0.025" layer="94"/>
<wire x1="329.69708125" y1="42.433240625" x2="329.67421875" y2="42.52468125" width="0.025" layer="94"/>
<wire x1="329.67421875" y1="42.52468125" x2="329.651359375" y2="42.58183125" width="0.025" layer="94"/>
<wire x1="329.651359375" y1="42.58183125" x2="329.61706875" y2="42.650409375" width="0.025" layer="94"/>
<wire x1="329.61706875" y1="42.650409375" x2="329.570490625" y2="42.72013125" width="0.025" layer="94"/>
<wire x1="329.570490625" y1="42.72013125" x2="329.537059375" y2="42.764709375" width="0.025" layer="94"/>
<wire x1="329.537059375" y1="42.764709375" x2="329.43533125" y2="42.866440625" width="0.025" layer="94"/>
<wire x1="329.43533125" y1="42.866440625" x2="329.377040625" y2="42.9133" width="0.025" layer="94"/>
<wire x1="329.377040625" y1="42.9133" x2="329.33131875" y2="42.947590625" width="0.025" layer="94"/>
<wire x1="329.33131875" y1="42.947590625" x2="329.21701875" y2="43.01616875" width="0.025" layer="94"/>
<wire x1="329.21701875" y1="43.01616875" x2="329.137009375" y2="43.061890625" width="0.025" layer="94"/>
<wire x1="329.137009375" y1="43.061890625" x2="329.06843125" y2="43.09618125" width="0.025" layer="94"/>
<wire x1="329.06843125" y1="43.09618125" x2="328.965559375" y2="43.1419" width="0.025" layer="94"/>
<wire x1="328.965559375" y1="43.1419" x2="328.908409375" y2="43.164759375" width="0.025" layer="94"/>
<wire x1="328.908409375" y1="43.164759375" x2="328.81696875" y2="43.19905" width="0.025" layer="94"/>
<wire x1="328.81696875" y1="43.19905" x2="328.7141" y2="43.233340625" width="0.025" layer="94"/>
<wire x1="328.7141" y1="43.233340625" x2="328.634090625" y2="43.2562" width="0.025" layer="94"/>
<wire x1="328.634090625" y1="43.2562" x2="328.54265" y2="43.279059375" width="0.025" layer="94"/>
<wire x1="328.54265" y1="43.279059375" x2="328.43978125" y2="43.30191875" width="0.025" layer="94"/>
<wire x1="328.43978125" y1="43.30191875" x2="328.32661875" y2="43.32478125" width="0.025" layer="94"/>
<wire x1="328.32661875" y1="43.32478125" x2="328.255759375" y2="43.336209375" width="0.025" layer="94"/>
<wire x1="328.255759375" y1="43.336209375" x2="328.15516875" y2="43.347640625" width="0.025" layer="94"/>
<wire x1="328.15516875" y1="43.347640625" x2="328.075159375" y2="43.353709375" width="0.025" layer="94"/>
<wire x1="328.075159375" y1="43.353709375" x2="328.00658125" y2="43.35906875" width="0.025" layer="94"/>
<wire x1="328.00658125" y1="43.35906875" x2="328.005440625" y2="43.53051875" width="0.025" layer="94"/>
<wire x1="328.005440625" y1="43.53051875" x2="328.176890625" y2="43.53051875" width="0.025" layer="94"/>
<wire x1="328.176890625" y1="43.53051875" x2="328.291190625" y2="43.519090625" width="0.025" layer="94"/>
<wire x1="328.291190625" y1="43.519090625" x2="328.3712" y2="43.507659375" width="0.025" layer="94"/>
<wire x1="328.3712" y1="43.507659375" x2="328.43978125" y2="43.49623125" width="0.025" layer="94"/>
<wire x1="328.43978125" y1="43.49623125" x2="328.61123125" y2="43.461940625" width="0.025" layer="94"/>
<wire x1="328.61123125" y1="43.461940625" x2="328.70266875" y2="43.43908125" width="0.025" layer="94"/>
<wire x1="328.70266875" y1="43.43908125" x2="328.78268125" y2="43.41621875" width="0.025" layer="94"/>
<wire x1="328.78268125" y1="43.41621875" x2="328.892409375" y2="43.38306875" width="0.025" layer="94"/>
<wire x1="328.892409375" y1="43.38306875" x2="329.010140625" y2="43.34798125" width="0.025" layer="94"/>
<wire x1="329.010140625" y1="43.34798125" x2="329.079859375" y2="43.32191875" width="0.025" layer="94"/>
<wire x1="329.079859375" y1="43.32191875" x2="329.194159375" y2="43.279059375" width="0.025" layer="94"/>
<wire x1="329.194159375" y1="43.279059375" x2="329.3096" y2="43.23276875" width="0.025" layer="94"/>
<wire x1="329.3096" y1="43.23276875" x2="329.44561875" y2="43.164759375" width="0.025" layer="94"/>
<wire x1="329.44561875" y1="43.164759375" x2="329.50276875" y2="43.13046875" width="0.025" layer="94"/>
<wire x1="329.50276875" y1="43.13046875" x2="329.548490625" y2="43.107609375" width="0.025" layer="94"/>
<wire x1="329.548490625" y1="43.107609375" x2="329.61706875" y2="43.061890625" width="0.025" layer="94"/>
<wire x1="329.61706875" y1="43.061890625" x2="329.75423125" y2="42.95901875" width="0.025" layer="94"/>
<wire x1="329.75423125" y1="42.95901875" x2="329.81138125" y2="42.9133" width="0.025" layer="94"/>
<wire x1="329.891390625" y1="42.833290625" x2="329.89253125" y2="42.83215" width="0.025" layer="94"/>
<wire x1="329.89253125" y1="42.83215" x2="329.937109375" y2="42.776140625" width="0.025" layer="94"/>
<wire x1="329.937109375" y1="42.776140625" x2="329.95996875" y2="42.74185" width="0.025" layer="94"/>
<wire x1="329.95996875" y1="42.74185" x2="329.9954" y2="42.682409375" width="0.025" layer="94"/>
<wire x1="329.9954" y1="42.682409375" x2="330.03998125" y2="42.5704" width="0.025" layer="94"/>
<wire x1="330.03998125" y1="42.5704" x2="330.051409375" y2="42.512109375" width="0.025" layer="94"/>
<wire x1="330.051409375" y1="42.512109375" x2="330.062840625" y2="42.421809375" width="0.025" layer="94"/>
<wire x1="330.062840625" y1="42.421809375" x2="330.062840625" y2="42.35323125" width="0.025" layer="94"/>
<wire x1="330.062840625" y1="42.35323125" x2="330.051409375" y2="42.261790625" width="0.025" layer="94"/>
<wire x1="330.051409375" y1="42.261790625" x2="330.03998125" y2="42.21606875" width="0.025" layer="94"/>
<wire x1="330.03998125" y1="42.21606875" x2="330.01711875" y2="42.15891875" width="0.025" layer="94"/>
<wire x1="330.01711875" y1="42.15891875" x2="329.98283125" y2="42.090340625" width="0.025" layer="94"/>
<wire x1="329.98283125" y1="42.090340625" x2="329.95996875" y2="42.05605" width="0.025" layer="94"/>
<wire x1="329.95996875" y1="42.05605" x2="329.90281875" y2="41.976040625" width="0.025" layer="94"/>
<wire x1="329.90281875" y1="41.976040625" x2="329.834240625" y2="41.89603125" width="0.025" layer="94"/>
<wire x1="329.834240625" y1="41.89603125" x2="329.7428" y2="41.804590625" width="0.025" layer="94"/>
<wire x1="329.7428" y1="41.804590625" x2="329.6285" y2="41.71315" width="0.025" layer="94"/>
<wire x1="329.6285" y1="41.71315" x2="329.55878125" y2="41.666790625" width="0.025" layer="94"/>
<wire x1="329.55878125" y1="41.666790625" x2="329.45705" y2="41.61028125" width="0.025" layer="94"/>
<wire x1="329.45705" y1="41.61028125" x2="329.319890625" y2="41.5417" width="0.025" layer="94"/>
<wire x1="329.319890625" y1="41.5417" x2="329.1633" y2="41.474590625" width="0.025" layer="94"/>
<wire x1="329.1633" y1="41.474590625" x2="329.06843125" y2="41.43883125" width="0.025" layer="94"/>
<wire x1="329.06843125" y1="41.43883125" x2="329.10271875" y2="41.278809375" width="0.025" layer="94"/>
<wire x1="329.10271875" y1="41.278809375" x2="329.137009375" y2="41.290240625" width="0.025" layer="94"/>
<wire x1="329.137009375" y1="41.290240625" x2="329.21701875" y2="41.3131" width="0.025" layer="94"/>
<wire x1="329.21701875" y1="41.3131" x2="329.3999" y2="41.37025" width="0.025" layer="94"/>
<wire x1="329.3999" y1="41.37025" x2="329.57135" y2="41.43883125" width="0.025" layer="94"/>
<wire x1="329.57135" y1="41.43883125" x2="329.834240625" y2="41.564559375" width="0.025" layer="94"/>
<wire x1="329.834240625" y1="41.564559375" x2="329.891390625" y2="41.59885" width="0.025" layer="94"/>
<wire x1="329.891390625" y1="41.59885" x2="329.994259375" y2="41.66743125" width="0.025" layer="94"/>
<wire x1="329.994259375" y1="41.66743125" x2="330.051409375" y2="41.71315" width="0.025" layer="94"/>
<wire x1="330.051409375" y1="41.71315" x2="330.14285" y2="41.78173125" width="0.025" layer="94"/>
<wire x1="330.14285" y1="41.78173125" x2="330.177140625" y2="41.81601875" width="0.025" layer="94"/>
<wire x1="330.177140625" y1="41.81601875" x2="330.25715" y2="41.8846" width="0.025" layer="94"/>
<wire x1="330.25715" y1="41.8846" x2="330.30286875" y2="41.93031875" width="0.025" layer="94"/>
<wire x1="330.30286875" y1="41.93031875" x2="330.37145" y2="42.01033125" width="0.025" layer="94"/>
<wire x1="330.37145" y1="42.01033125" x2="330.405740625" y2="42.05605" width="0.025" layer="94"/>
<wire x1="330.405740625" y1="42.05605" x2="330.47478125" y2="42.171490625" width="0.025" layer="94"/>
<wire x1="330.47478125" y1="42.171490625" x2="330.520040625" y2="42.28465" width="0.025" layer="94"/>
<wire x1="330.520040625" y1="42.28465" x2="330.54251875" y2="42.352090625" width="0.025" layer="94"/>
<wire x1="330.54251875" y1="42.352090625" x2="330.565759375" y2="42.478959375" width="0.025" layer="94"/>
<wire x1="330.565759375" y1="42.478959375" x2="330.565759375" y2="42.650409375" width="0.025" layer="94"/>
<wire x1="330.565759375" y1="42.650409375" x2="330.55433125" y2="42.718990625" width="0.025" layer="94"/>
<wire x1="330.55433125" y1="42.718990625" x2="330.5429" y2="42.776140625" width="0.025" layer="94"/>
<wire x1="330.5429" y1="42.776140625" x2="330.520040625" y2="42.84471875" width="0.025" layer="94"/>
<wire x1="330.520040625" y1="42.84471875" x2="330.49718125" y2="42.90186875" width="0.025" layer="94"/>
<wire x1="330.49718125" y1="42.90186875" x2="330.451459375" y2="42.993309375" width="0.025" layer="94"/>
<wire x1="330.451459375" y1="42.993309375" x2="330.41716875" y2="43.050459375" width="0.025" layer="94"/>
<wire x1="330.41716875" y1="43.050459375" x2="330.394309375" y2="43.08475" width="0.025" layer="94"/>
<wire x1="330.394309375" y1="43.08475" x2="330.36001875" y2="43.13046875" width="0.025" layer="94"/>
<wire x1="330.36001875" y1="43.13046875" x2="330.291440625" y2="43.21048125" width="0.025" layer="94"/>
<wire x1="330.291440625" y1="43.21048125" x2="330.234290625" y2="43.26763125" width="0.025" layer="94"/>
<wire x1="330.234290625" y1="43.26763125" x2="330.15428125" y2="43.336209375" width="0.025" layer="94"/>
<wire x1="330.15428125" y1="43.336209375" x2="330.01711875" y2="43.43908125" width="0.025" layer="94"/>
<wire x1="330.01711875" y1="43.43908125" x2="329.879959375" y2="43.53051875" width="0.025" layer="94"/>
<wire x1="329.879959375" y1="43.53051875" x2="329.765659375" y2="43.5991" width="0.025" layer="94"/>
<wire x1="329.765659375" y1="43.5991" x2="329.68565" y2="43.64481875" width="0.025" layer="94"/>
<wire x1="329.68565" y1="43.64481875" x2="329.52563125" y2="43.72483125" width="0.025" layer="94"/>
<wire x1="329.52563125" y1="43.72483125" x2="329.44561875" y2="43.75911875" width="0.025" layer="94"/>
<wire x1="329.44561875" y1="43.75911875" x2="329.1713" y2="43.850559375" width="0.025" layer="94"/>
<wire x1="329.1713" y1="43.850559375" x2="329.04556875" y2="43.88485" width="0.025" layer="94"/>
<wire x1="329.04556875" y1="43.88485" x2="328.965559375" y2="43.907709375" width="0.025" layer="94"/>
<wire x1="328.965559375" y1="43.907709375" x2="328.691240625" y2="43.976290625" width="0.025" layer="94"/>
<wire x1="328.691240625" y1="43.976290625" x2="328.634090625" y2="43.98771875" width="0.025" layer="94"/>
<wire x1="328.634090625" y1="43.98771875" x2="328.565509375" y2="43.99915" width="0.025" layer="94"/>
<wire x1="328.565509375" y1="43.99915" x2="328.394059375" y2="44.022009375" width="0.025" layer="94"/>
<wire x1="328.394059375" y1="44.022009375" x2="328.2569" y2="44.033440625" width="0.025" layer="94"/>
<wire x1="328.2569" y1="44.033440625" x2="328.119740625" y2="44.033440625" width="0.025" layer="94"/>
<wire x1="328.119740625" y1="44.033440625" x2="328.119740625" y2="44.29633125" width="0.025" layer="94"/>
<wire x1="328.119740625" y1="44.29633125" x2="328.165459375" y2="44.29633125" width="0.025" layer="94"/>
<wire x1="328.165459375" y1="44.29633125" x2="328.32548125" y2="44.2849" width="0.025" layer="94"/>
<wire x1="328.32548125" y1="44.2849" x2="328.42835" y2="44.27346875" width="0.025" layer="94"/>
<wire x1="328.42835" y1="44.27346875" x2="328.519790625" y2="44.262040625" width="0.025" layer="94"/>
<wire x1="328.519790625" y1="44.262040625" x2="328.5998" y2="44.250609375" width="0.025" layer="94"/>
<wire x1="328.5998" y1="44.250609375" x2="328.72553125" y2="44.22775" width="0.025" layer="94"/>
<wire x1="328.72553125" y1="44.22775" x2="328.952990625" y2="44.18203125" width="0.025" layer="94"/>
<wire x1="328.952990625" y1="44.18203125" x2="329.22845" y2="44.11345" width="0.025" layer="94"/>
<wire x1="329.22845" y1="44.11345" x2="329.38846875" y2="44.06773125" width="0.025" layer="94"/>
<wire x1="329.38846875" y1="44.06773125" x2="329.52563125" y2="44.022009375" width="0.025" layer="94"/>
<wire x1="329.52563125" y1="44.022009375" x2="329.69708125" y2="43.95343125" width="0.025" layer="94"/>
<wire x1="329.69708125" y1="43.95343125" x2="329.90281875" y2="43.861990625" width="0.025" layer="94"/>
<wire x1="329.90281875" y1="43.861990625" x2="330.04111875" y2="43.79271875" width="0.025" layer="94"/>
<wire x1="330.04111875" y1="43.79271875" x2="330.14285" y2="43.736259375" width="0.025" layer="94"/>
<wire x1="330.14285" y1="43.736259375" x2="330.2" y2="43.70196875" width="0.025" layer="94"/>
<wire x1="330.2" y1="43.70196875" x2="330.337159375" y2="43.61053125" width="0.025" layer="94"/>
<wire x1="330.337159375" y1="43.61053125" x2="330.38288125" y2="43.576240625" width="0.025" layer="94"/>
<wire x1="330.38288125" y1="43.576240625" x2="330.55433125" y2="43.43908125" width="0.025" layer="94"/>
<wire x1="330.55433125" y1="43.43908125" x2="330.601190625" y2="43.39221875" width="0.025" layer="94"/>
<wire x1="330.601190625" y1="43.39221875" x2="330.644140625" y2="43.34193125" width="0.025" layer="94"/>
<wire x1="330.644140625" y1="43.34193125" x2="330.70291875" y2="43.279059375" width="0.025" layer="94"/>
<wire x1="330.70291875" y1="43.279059375" x2="330.7715" y2="43.18761875" width="0.025" layer="94"/>
<wire x1="330.7715" y1="43.18761875" x2="330.81721875" y2="43.119040625" width="0.025" layer="94"/>
<wire x1="330.81721875" y1="43.119040625" x2="330.862940625" y2="43.0276" width="0.025" layer="94"/>
<wire x1="330.862940625" y1="43.0276" x2="330.8858" y2="42.95901875" width="0.025" layer="94"/>
<wire x1="330.8858" y1="42.95901875" x2="330.908659375" y2="42.879009375" width="0.025" layer="94"/>
<wire x1="330.908659375" y1="42.879009375" x2="330.920090625" y2="42.81043125" width="0.025" layer="94"/>
<wire x1="330.920090625" y1="42.81043125" x2="330.93151875" y2="42.69613125" width="0.025" layer="94"/>
<wire x1="330.93151875" y1="42.69613125" x2="330.93151875" y2="42.63898125" width="0.025" layer="94"/>
<wire x1="330.93151875" y1="42.63898125" x2="330.920090625" y2="42.536109375" width="0.025" layer="94"/>
<wire x1="330.920090625" y1="42.536109375" x2="330.908659375" y2="42.478959375" width="0.025" layer="94"/>
<wire x1="330.908659375" y1="42.478959375" x2="330.8858" y2="42.38751875" width="0.025" layer="94"/>
<wire x1="330.8858" y1="42.38751875" x2="330.87436875" y2="42.35323125" width="0.025" layer="94"/>
<wire x1="330.87436875" y1="42.35323125" x2="330.82865" y2="42.23893125" width="0.025" layer="94"/>
<wire x1="330.82865" y1="42.23893125" x2="330.8051" y2="42.19206875" width="0.025" layer="94"/>
<wire x1="330.8051" y1="42.19206875" x2="330.770640625" y2="42.13491875" width="0.025" layer="94"/>
<wire x1="330.770640625" y1="42.13491875" x2="330.70291875" y2="42.04461875" width="0.025" layer="94"/>
<wire x1="330.70291875" y1="42.04461875" x2="330.6572" y2="41.98746875" width="0.025" layer="94"/>
<wire x1="330.6572" y1="41.98746875" x2="330.577190625" y2="41.907459375" width="0.025" layer="94"/>
<wire x1="330.577190625" y1="41.907459375" x2="330.49718125" y2="41.83888125" width="0.025" layer="94"/>
<wire x1="330.49718125" y1="41.83888125" x2="330.44003125" y2="41.793159375" width="0.025" layer="94"/>
<wire x1="330.44003125" y1="41.793159375" x2="330.394309375" y2="41.75886875" width="0.025" layer="94"/>
<wire x1="330.394309375" y1="41.75886875" x2="330.337159375" y2="41.71315" width="0.025" layer="94"/>
<wire x1="330.337159375" y1="41.71315" x2="330.291440625" y2="41.678859375" width="0.025" layer="94"/>
<wire x1="330.291440625" y1="41.678859375" x2="330.18856875" y2="41.61028125" width="0.025" layer="94"/>
<wire x1="330.18856875" y1="41.61028125" x2="330.14285" y2="41.575990625" width="0.025" layer="94"/>
<wire x1="330.14285" y1="41.575990625" x2="330.0857" y2="41.5417" width="0.025" layer="94"/>
<wire x1="330.0857" y1="41.5417" x2="329.834240625" y2="41.41596875" width="0.025" layer="94"/>
<wire x1="329.834240625" y1="41.41596875" x2="329.719940625" y2="41.37025" width="0.025" layer="94"/>
<wire x1="329.719940625" y1="41.37025" x2="329.594209375" y2="41.32453125" width="0.025" layer="94"/>
<wire x1="329.594209375" y1="41.32453125" x2="329.44333125" y2="41.278109375" width="0.025" layer="94"/>
<wire x1="329.44333125" y1="41.278109375" x2="329.29703125" y2="41.233090625" width="0.025" layer="94"/>
<wire x1="329.29703125" y1="41.233090625" x2="329.137009375" y2="41.18736875" width="0.025" layer="94"/>
<wire x1="329.137009375" y1="41.18736875" x2="329.1713" y2="41.03878125" width="0.025" layer="94"/>
<wire x1="329.1713" y1="41.03878125" x2="329.26616875" y2="41.059640625" width="0.025" layer="94"/>
<wire x1="329.26616875" y1="41.059640625" x2="329.50163125" y2="41.1185" width="0.025" layer="94"/>
<wire x1="329.50163125" y1="41.1185" x2="329.6045" y2="41.147859375" width="0.025" layer="94"/>
<wire x1="329.6045" y1="41.147859375" x2="329.743940625" y2="41.18775" width="0.025" layer="94"/>
<wire x1="329.743940625" y1="41.18775" x2="329.948540625" y2="41.25595" width="0.025" layer="94"/>
<wire x1="329.948540625" y1="41.25595" x2="330.03998125" y2="41.290240625" width="0.025" layer="94"/>
<wire x1="330.03998125" y1="41.290240625" x2="330.21143125" y2="41.35881875" width="0.025" layer="94"/>
<wire x1="330.21143125" y1="41.35881875" x2="330.48575" y2="41.49598125" width="0.025" layer="94"/>
<wire x1="330.48575" y1="41.49598125" x2="330.60005" y2="41.564559375" width="0.025" layer="94"/>
<wire x1="330.60005" y1="41.564559375" x2="330.69035" y2="41.62101875" width="0.025" layer="94"/>
<wire x1="330.69035" y1="41.62101875" x2="330.737209375" y2="41.656" width="0.025" layer="94"/>
<wire x1="330.737209375" y1="41.656" x2="330.805790625" y2="41.70171875" width="0.025" layer="94"/>
<wire x1="330.805790625" y1="41.70171875" x2="330.851509375" y2="41.736009375" width="0.025" layer="94"/>
<wire x1="330.851509375" y1="41.736009375" x2="331.080109375" y2="41.918890625" width="0.025" layer="94"/>
<wire x1="331.080109375" y1="41.918890625" x2="331.2287" y2="42.06748125" width="0.025" layer="94"/>
<wire x1="331.2287" y1="42.06748125" x2="331.308709375" y2="42.17035" width="0.025" layer="94"/>
<wire x1="331.308709375" y1="42.17035" x2="331.33156875" y2="42.204640625" width="0.025" layer="94"/>
<wire x1="331.33156875" y1="42.204640625" x2="331.365859375" y2="42.261790625" width="0.025" layer="94"/>
<wire x1="331.365859375" y1="42.261790625" x2="331.434440625" y2="42.39895" width="0.025" layer="94"/>
<wire x1="331.434440625" y1="42.39895" x2="331.4573" y2="42.4561" width="0.025" layer="94"/>
<wire x1="331.4573" y1="42.4561" x2="331.46873125" y2="42.490390625" width="0.025" layer="94"/>
<wire x1="331.46873125" y1="42.490390625" x2="331.491590625" y2="42.58183125" width="0.025" layer="94"/>
<wire x1="331.491590625" y1="42.58183125" x2="331.50301875" y2="42.650409375" width="0.025" layer="94"/>
<wire x1="331.50301875" y1="42.650409375" x2="331.51445" y2="42.764709375" width="0.025" layer="94"/>
<wire x1="331.51445" y1="42.764709375" x2="331.51445" y2="42.84471875" width="0.025" layer="94"/>
<wire x1="331.51445" y1="42.84471875" x2="331.50988125" y2="42.89615" width="0.025" layer="94"/>
<wire x1="331.50988125" y1="42.89615" x2="331.50301875" y2="42.97045" width="0.025" layer="94"/>
<wire x1="331.50301875" y1="42.97045" x2="331.480159375" y2="43.08475" width="0.025" layer="94"/>
<wire x1="331.480159375" y1="43.08475" x2="331.434440625" y2="43.221909375" width="0.025" layer="94"/>
<wire x1="331.434440625" y1="43.221909375" x2="331.41158125" y2="43.279059375" width="0.025" layer="94"/>
<wire x1="331.41158125" y1="43.279059375" x2="331.36643125" y2="43.369359375" width="0.025" layer="94"/>
<wire x1="331.36643125" y1="43.369359375" x2="331.27518125" y2="43.50651875" width="0.025" layer="94"/>
<wire x1="331.27518125" y1="43.50651875" x2="331.24013125" y2="43.55338125" width="0.025" layer="94"/>
<wire x1="331.24013125" y1="43.55338125" x2="331.194409375" y2="43.61053125" width="0.025" layer="94"/>
<wire x1="331.194409375" y1="43.61053125" x2="330.98866875" y2="43.81626875" width="0.025" layer="94"/>
<wire x1="330.98866875" y1="43.81626875" x2="330.920090625" y2="43.87341875" width="0.025" layer="94"/>
<wire x1="330.920090625" y1="43.87341875" x2="330.862940625" y2="43.919140625" width="0.025" layer="94"/>
<wire x1="330.862940625" y1="43.919140625" x2="330.7715" y2="43.98771875" width="0.025" layer="94"/>
<wire x1="330.7715" y1="43.98771875" x2="330.66863125" y2="44.0563" width="0.025" layer="94"/>
<wire x1="330.66863125" y1="44.0563" x2="330.610340625" y2="44.09135" width="0.025" layer="94"/>
<wire x1="330.610340625" y1="44.09135" x2="330.577190625" y2="44.11345" width="0.025" layer="94"/>
<wire x1="330.577190625" y1="44.11345" x2="330.520040625" y2="44.147740625" width="0.025" layer="94"/>
<wire x1="330.520040625" y1="44.147740625" x2="330.24571875" y2="44.2849" width="0.025" layer="94"/>
<wire x1="330.24571875" y1="44.2849" x2="329.95768125" y2="44.399959375" width="0.025" layer="94"/>
<wire x1="329.95768125" y1="44.399959375" x2="329.651359375" y2="44.50206875" width="0.025" layer="94"/>
<wire x1="329.651359375" y1="44.50206875" x2="329.4902" y2="44.54806875" width="0.025" layer="94"/>
<wire x1="329.4902" y1="44.54806875" x2="329.21701875" y2="44.61636875" width="0.025" layer="94"/>
<wire x1="329.21701875" y1="44.61636875" x2="328.87411875" y2="44.68495" width="0.025" layer="94"/>
<wire x1="328.87411875" y1="44.68495" x2="328.736959375" y2="44.707809375" width="0.025" layer="94"/>
<wire x1="328.736959375" y1="44.707809375" x2="328.65695" y2="44.719240625" width="0.025" layer="94"/>
<wire x1="328.65695" y1="44.719240625" x2="328.47406875" y2="44.7421" width="0.025" layer="94"/>
<wire x1="328.47406875" y1="44.7421" x2="328.370059375" y2="44.75353125" width="0.025" layer="94"/>
<wire x1="328.370059375" y1="44.75353125" x2="328.222609375" y2="44.764959375" width="0.025" layer="94"/>
<wire x1="328.222609375" y1="44.764959375" x2="328.222609375" y2="45.03928125" width="0.025" layer="94"/>
<wire x1="328.222609375" y1="45.03928125" x2="328.279759375" y2="45.03928125" width="0.025" layer="94"/>
<wire x1="328.279759375" y1="45.03928125" x2="328.42835" y2="45.02785" width="0.025" layer="94"/>
<wire x1="328.42835" y1="45.02785" x2="328.55408125" y2="45.01641875" width="0.025" layer="94"/>
<wire x1="328.55408125" y1="45.01641875" x2="328.65695" y2="45.004990625" width="0.025" layer="94"/>
<wire x1="328.65695" y1="45.004990625" x2="328.736959375" y2="44.994990625" width="0.025" layer="94"/>
<wire x1="328.736959375" y1="44.994990625" x2="328.79983125" y2="44.98713125" width="0.025" layer="94"/>
<wire x1="328.79983125" y1="44.98713125" x2="328.93126875" y2="44.9707" width="0.025" layer="94"/>
<wire x1="328.93126875" y1="44.9707" x2="329.137009375" y2="44.936409375" width="0.025" layer="94"/>
<wire x1="329.137009375" y1="44.936409375" x2="329.251309375" y2="44.91355" width="0.025" layer="94"/>
<wire x1="329.251309375" y1="44.91355" x2="329.29703125" y2="44.90211875" width="0.025" layer="94"/>
<wire x1="329.29703125" y1="44.90211875" x2="329.3999" y2="44.879259375" width="0.025" layer="94"/>
<wire x1="329.3999" y1="44.879259375" x2="329.45705" y2="44.86783125" width="0.025" layer="94"/>
<wire x1="329.45705" y1="44.86783125" x2="329.63993125" y2="44.822109375" width="0.025" layer="94"/>
<wire x1="329.63993125" y1="44.822109375" x2="329.719940625" y2="44.79925" width="0.025" layer="94"/>
<wire x1="329.719940625" y1="44.79925" x2="329.81138125" y2="44.776390625" width="0.025" layer="94"/>
<wire x1="329.81138125" y1="44.776390625" x2="329.891390625" y2="44.75353125" width="0.025" layer="94"/>
<wire x1="329.891390625" y1="44.75353125" x2="330.165709375" y2="44.662090625" width="0.025" layer="94"/>
<wire x1="330.165709375" y1="44.662090625" x2="330.25715" y2="44.6278" width="0.025" layer="94"/>
<wire x1="330.25715" y1="44.6278" x2="330.3143" y2="44.604940625" width="0.025" layer="94"/>
<wire x1="330.3143" y1="44.604940625" x2="330.394309375" y2="44.57065" width="0.025" layer="94"/>
<wire x1="330.394309375" y1="44.57065" x2="330.508609375" y2="44.52493125" width="0.025" layer="94"/>
<wire x1="330.508609375" y1="44.52493125" x2="330.87436875" y2="44.34205" width="0.025" layer="94"/>
<wire x1="330.87436875" y1="44.34205" x2="331.04581875" y2="44.23918125" width="0.025" layer="94"/>
<wire x1="331.04581875" y1="44.23918125" x2="331.080109375" y2="44.21631875" width="0.025" layer="94"/>
<wire x1="331.080109375" y1="44.21631875" x2="331.137259375" y2="44.1706" width="0.025" layer="94"/>
<wire x1="331.137259375" y1="44.1706" x2="331.36471875" y2="44.000009375" width="0.025" layer="94"/>
<wire x1="331.36471875" y1="44.000009375" x2="331.451590625" y2="43.91341875" width="0.025" layer="94"/>
<wire x1="331.451590625" y1="43.91341875" x2="331.62875" y2="43.736259375" width="0.025" layer="94"/>
<wire x1="331.62875" y1="43.736259375" x2="331.67446875" y2="43.679109375" width="0.025" layer="94"/>
<wire x1="331.67446875" y1="43.679109375" x2="331.74305" y2="43.58766875" width="0.025" layer="94"/>
<wire x1="331.74305" y1="43.58766875" x2="331.777340625" y2="43.53051875" width="0.025" layer="94"/>
<wire x1="331.777340625" y1="43.53051875" x2="331.823059375" y2="43.450509375" width="0.025" layer="94"/>
<wire x1="331.823059375" y1="43.450509375" x2="331.85735" y2="43.38193125" width="0.025" layer="94"/>
<wire x1="331.85735" y1="43.38193125" x2="331.880209375" y2="43.32478125" width="0.025" layer="94"/>
<wire x1="331.880209375" y1="43.32478125" x2="331.90306875" y2="43.2562" width="0.025" layer="94"/>
<wire x1="331.90306875" y1="43.2562" x2="331.92593125" y2="43.164759375" width="0.025" layer="94"/>
<wire x1="331.92593125" y1="43.164759375" x2="331.937359375" y2="43.095040625" width="0.025" layer="94"/>
<wire x1="331.937359375" y1="43.095040625" x2="331.948790625" y2="42.993309375" width="0.025" layer="94"/>
<wire x1="331.948790625" y1="42.993309375" x2="331.948790625" y2="42.84471875" width="0.025" layer="94"/>
<wire x1="331.948790625" y1="42.84471875" x2="331.937359375" y2="42.75328125" width="0.025" layer="94"/>
<wire x1="331.937359375" y1="42.75328125" x2="331.92593125" y2="42.69613125" width="0.025" layer="94"/>
<wire x1="331.92593125" y1="42.69613125" x2="331.90306875" y2="42.604690625" width="0.025" layer="94"/>
<wire x1="331.90306875" y1="42.604690625" x2="331.886609375" y2="42.55783125" width="0.025" layer="94"/>
<wire x1="331.886609375" y1="42.55783125" x2="331.84591875" y2="42.4561" width="0.025" layer="94"/>
<wire x1="331.84591875" y1="42.4561" x2="331.777340625" y2="42.318940625" width="0.025" layer="94"/>
<wire x1="331.777340625" y1="42.318940625" x2="331.708759375" y2="42.21606875" width="0.025" layer="94"/>
<wire x1="331.708759375" y1="42.21606875" x2="331.67446875" y2="42.17035" width="0.025" layer="94"/>
<wire x1="331.67446875" y1="42.17035" x2="331.56016875" y2="42.033190625" width="0.025" layer="94"/>
<wire x1="331.56016875" y1="42.033190625" x2="331.40015" y2="41.87316875" width="0.025" layer="94"/>
<wire x1="331.40015" y1="41.87316875" x2="331.262990625" y2="41.75886875" width="0.025" layer="94"/>
<wire x1="331.262990625" y1="41.75886875" x2="331.17155" y2="41.690290625" width="0.025" layer="94"/>
<wire x1="331.17155" y1="41.690290625" x2="331.034390625" y2="41.59885" width="0.025" layer="94"/>
<wire x1="331.034390625" y1="41.59885" x2="330.977240625" y2="41.564559375" width="0.025" layer="94"/>
<wire x1="330.977240625" y1="41.564559375" x2="330.89723125" y2="41.518840625" width="0.025" layer="94"/>
<wire x1="330.89723125" y1="41.518840625" x2="330.84008125" y2="41.48455" width="0.025" layer="94"/>
<wire x1="330.84008125" y1="41.48455" x2="330.5429" y2="41.335959375" width="0.025" layer="94"/>
<wire x1="330.5429" y1="41.335959375" x2="330.462890625" y2="41.30166875" width="0.025" layer="94"/>
<wire x1="330.462890625" y1="41.30166875" x2="330.348590625" y2="41.25595" width="0.025" layer="94"/>
<wire x1="330.348590625" y1="41.25595" x2="330.25715" y2="41.221659375" width="0.025" layer="94"/>
<wire x1="330.25715" y1="41.221659375" x2="330.13141875" y2="41.175940625" width="0.025" layer="94"/>
<wire x1="330.13141875" y1="41.175940625" x2="329.994259375" y2="41.13021875" width="0.025" layer="94"/>
<wire x1="329.994259375" y1="41.13021875" x2="329.879959375" y2="41.09593125" width="0.025" layer="94"/>
<wire x1="329.879959375" y1="41.09593125" x2="329.75423125" y2="41.061640625" width="0.025" layer="94"/>
<wire x1="329.75423125" y1="41.061640625" x2="329.67421875" y2="41.03878125" width="0.025" layer="94"/>
<wire x1="329.67421875" y1="41.03878125" x2="329.52563125" y2="41.004490625" width="0.025" layer="94"/>
<wire x1="329.52563125" y1="41.004490625" x2="329.319890625" y2="40.95876875" width="0.025" layer="94"/>
<wire x1="329.319890625" y1="40.95876875" x2="329.194159375" y2="40.935909375" width="0.025" layer="94"/>
<wire x1="328.35108125" y1="37.03828125" x2="327.58253125" y2="40.78731875" width="0.025" layer="94"/>
<wire x1="327.58253125" y1="40.78731875" x2="326.86038125" y2="37.084" width="0.025" layer="94"/>
<wire x1="326.86038125" y1="37.084" x2="326.805290625" y2="37.01541875" width="0.025" layer="94"/>
<wire x1="271.78" y1="236.22" x2="271.78" y2="231.14" width="0.025" layer="94"/>
<wire x1="271.78" y1="231.14" x2="271.78" y2="226.06" width="0.025" layer="94"/>
<wire x1="271.78" y1="226.06" x2="279.4" y2="226.06" width="0.025" layer="94"/>
<wire x1="279.4" y1="226.06" x2="289.56" y2="226.06" width="0.025" layer="94"/>
<wire x1="289.56" y1="226.06" x2="299.72" y2="226.06" width="0.025" layer="94"/>
<wire x1="299.72" y1="226.06" x2="309.88" y2="226.06" width="0.025" layer="94"/>
<wire x1="309.88" y1="226.06" x2="375.92" y2="226.06" width="0.025" layer="94"/>
<wire x1="271.78" y1="231.14" x2="375.92" y2="231.14" width="0.025" layer="94"/>
<wire x1="279.4" y1="236.22" x2="279.4" y2="226.06" width="0.025" layer="94"/>
<wire x1="289.56" y1="236.22" x2="289.56" y2="226.06" width="0.025" layer="94"/>
<wire x1="299.72" y1="236.22" x2="299.72" y2="226.06" width="0.025" layer="94"/>
<wire x1="309.88" y1="236.22" x2="309.88" y2="226.06" width="0.025" layer="94"/>
<text x="272.796" y="35.306" size="1.778" layer="94" font="vector" ratio="12">DRAWN
BY</text>
<text x="272.542" y="28.702" size="1.778" layer="94" font="vector" ratio="12">CHECKED
BY</text>
<text x="272.542" y="22.352" size="1.778" layer="94" font="vector" ratio="12">DESIGN
APPROVAL</text>
<text x="287.02" y="42.164" size="1.778" layer="94" font="vector" ratio="12">INIT</text>
<text x="294.386" y="42.164" size="1.778" layer="94" font="vector" ratio="12">DATE</text>
<text x="333.502" y="33.528" size="1.6764" layer="94" font="vector" ratio="12">JOHN MEZZALINGUA ASSOCIATES
 7645 HENRY CLAY BOULEVARD
  LIVERPOOL, NEW YORK 13088
       TEL. 315-431-7100</text>
<text x="307.34" y="22.86" size="1.27" layer="94" font="vector" ratio="12">THIS DRAWING AND SPECIFICATIONS ARE THE PROPERTY OF
JOHN MEZZALINGUA ASSOCIATES AND SHALL NOT BE 
REPRODICED, COPIED, OR USED AS A BASIS FOR MANUFACTURING OR
SALE OF EQUIPMENT OR DEVICES WITHOUT WRITTEN PERMISSION.</text>
<text x="305.562" y="19.812" size="1.27" layer="94" font="vector" ratio="12">TITLE: </text>
<text x="305.562" y="13.462" size="1.27" layer="94" font="vector" ratio="12">DWG. NO.</text>
<text x="370.332" y="13.462" size="1.27" layer="94" font="vector" ratio="12">REV.</text>
<text x="360.172" y="7.112" size="1.27" layer="94" font="vector" ratio="12">SIZE:   B</text>
<text x="323.342" y="7.112" size="1.27" layer="94" font="vector" ratio="12">SHEET</text>
<text x="305.562" y="7.112" size="1.27" layer="94" font="vector" ratio="12">SCALE: NONE</text>
<text x="317.5" y="10.16" size="1.778" layer="94" font="vector">&gt;DRAWING_NAME</text>
<text x="332.486" y="5.842" size="1.27" layer="94" font="vector">&gt;SHEET</text>
<text x="273.304" y="233.68" size="1.778" layer="94" font="vector" ratio="12">REV</text>
<text x="281.178" y="233.68" size="1.778" layer="94" font="vector" ratio="12">ZONE</text>
<text x="291.592" y="233.68" size="1.778" layer="94" font="vector" ratio="12">DATE</text>
<text x="302.26" y="233.68" size="1.778" layer="94" font="vector" ratio="12">ECO</text>
<text x="333.502" y="233.426" size="1.778" layer="94" font="vector" ratio="12">DESCRIPTION</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="A3_LOC_JMA_RELEASE">
<gates>
<gate name="G$1" symbol="A3_LOC_JMA_RELEASE" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="JMA_LBR_v2" urn="urn:adsk.eagle:library:8413580">
<packages>
<package name="RESC1608X55" urn="urn:adsk.eagle:footprint:3811167/1" library_version="38">
<description>CHIP, 1.6 X 0.85 X 0.55 mm body
&lt;p&gt;CHIP package with body size 1.6 X 0.85 X 0.55 mm&lt;/p&gt;</description>
<wire x1="0.875" y1="0.8036" x2="-0.875" y2="0.8036" width="0.12" layer="21"/>
<wire x1="0.875" y1="-0.8036" x2="-0.875" y2="-0.8036" width="0.12" layer="21"/>
<wire x1="0.875" y1="-0.475" x2="-0.875" y2="-0.475" width="0.12" layer="51"/>
<wire x1="-0.875" y1="-0.475" x2="-0.875" y2="0.475" width="0.12" layer="51"/>
<wire x1="-0.875" y1="0.475" x2="0.875" y2="0.475" width="0.12" layer="51"/>
<wire x1="0.875" y1="0.475" x2="0.875" y2="-0.475" width="0.12" layer="51"/>
<smd name="1" x="-0.7996" y="0" dx="0.8709" dy="0.9791" layer="1"/>
<smd name="2" x="0.7996" y="0" dx="0.8709" dy="0.9791" layer="1"/>
<text x="0" y="1.4386" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.4386" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="RESC2012X70" urn="urn:adsk.eagle:footprint:3811170/1" library_version="38">
<description>CHIP, 2 X 1.25 X 0.7 mm body
&lt;p&gt;CHIP package with body size 2 X 1.25 X 0.7 mm&lt;/p&gt;</description>
<wire x1="1.1" y1="1.0036" x2="-1.1" y2="1.0036" width="0.12" layer="21"/>
<wire x1="1.1" y1="-1.0036" x2="-1.1" y2="-1.0036" width="0.12" layer="21"/>
<wire x1="1.1" y1="-0.675" x2="-1.1" y2="-0.675" width="0.12" layer="51"/>
<wire x1="-1.1" y1="-0.675" x2="-1.1" y2="0.675" width="0.12" layer="51"/>
<wire x1="-1.1" y1="0.675" x2="1.1" y2="0.675" width="0.12" layer="51"/>
<wire x1="1.1" y1="0.675" x2="1.1" y2="-0.675" width="0.12" layer="51"/>
<smd name="1" x="-0.94" y="0" dx="1.0354" dy="1.3791" layer="1"/>
<smd name="2" x="0.94" y="0" dx="1.0354" dy="1.3791" layer="1"/>
<text x="0" y="1.6386" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.6386" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC2012X127" urn="urn:adsk.eagle:footprint:3793140/1" library_version="38">
<description>CHIP, 2.01 X 1.25 X 1.27 mm body
&lt;p&gt;CHIP package with body size 2.01 X 1.25 X 1.27 mm&lt;/p&gt;</description>
<wire x1="1.105" y1="1.0467" x2="-1.105" y2="1.0467" width="0.12" layer="21"/>
<wire x1="1.105" y1="-1.0467" x2="-1.105" y2="-1.0467" width="0.12" layer="21"/>
<wire x1="1.105" y1="-0.725" x2="-1.105" y2="-0.725" width="0.12" layer="51"/>
<wire x1="-1.105" y1="-0.725" x2="-1.105" y2="0.725" width="0.12" layer="51"/>
<wire x1="-1.105" y1="0.725" x2="1.105" y2="0.725" width="0.12" layer="51"/>
<wire x1="1.105" y1="0.725" x2="1.105" y2="-0.725" width="0.12" layer="51"/>
<smd name="1" x="-0.8804" y="0" dx="1.1646" dy="1.4653" layer="1"/>
<smd name="2" x="0.8804" y="0" dx="1.1646" dy="1.4653" layer="1"/>
<text x="0" y="1.6817" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.6817" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC1608X55" urn="urn:adsk.eagle:footprint:3793129/2" library_version="27">
<description>CHIP, 1.6 X 0.8 X 0.55 mm body
&lt;p&gt;CHIP package with body size 1.6 X 0.8 X 0.55 mm&lt;/p&gt;</description>
<wire x1="0.85" y1="0.6516" x2="-0.85" y2="0.6516" width="0.12" layer="21"/>
<wire x1="0.85" y1="-0.6516" x2="-0.85" y2="-0.6516" width="0.12" layer="21"/>
<wire x1="0.85" y1="-0.45" x2="-0.85" y2="-0.45" width="0.12" layer="51"/>
<wire x1="-0.85" y1="-0.45" x2="-0.85" y2="0.45" width="0.12" layer="51"/>
<wire x1="-0.85" y1="0.45" x2="0.85" y2="0.45" width="0.12" layer="51"/>
<wire x1="0.85" y1="0.45" x2="0.85" y2="-0.45" width="0.12" layer="51"/>
<smd name="1" x="-0.7704" y="0" dx="0.8884" dy="0.9291" layer="1"/>
<smd name="2" x="0.7704" y="0" dx="0.8884" dy="0.9291" layer="1"/>
<text x="0" y="1.4136" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.4136" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="GDTMELF5875" urn="urn:adsk.eagle:footprint:3890113/3" library_version="27">
<description>MELF, 5.865 mm length, 7.5 mm diameter
&lt;p&gt;MELF Resistor package with 5.865 mm length and 7.5 mm diameter&lt;/p&gt;</description>
<wire x1="1.7525" y1="4.1693" x2="-1.9303" y2="4.1693" width="0.12" layer="21"/>
<wire x1="-1.9303" y1="-4.1947" x2="1.7525" y2="-4.1947" width="0.12" layer="21"/>
<wire x1="3.015" y1="-3.85" x2="-3.015" y2="-3.85" width="0.12" layer="51"/>
<wire x1="-3.015" y1="-3.85" x2="-3.015" y2="3.85" width="0.12" layer="51"/>
<wire x1="-3.015" y1="3.85" x2="3.015" y2="3.85" width="0.12" layer="51"/>
<wire x1="3.015" y1="3.85" x2="3.015" y2="-3.85" width="0.12" layer="51"/>
<text x="0" y="4.8567" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-4.8567" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
<smd name="1" x="-3.016" y="0" dx="8.25" dy="1.78" layer="1" rot="R90" thermals="no"/>
<smd name="2" x="2.824" y="0" dx="8.25" dy="1.78" layer="1" rot="R90" thermals="no"/>
</package>
<package name="TRANSITION_CLIP_.141" urn="urn:adsk.eagle:footprint:8414034/1" library_version="27">
<wire x1="1.53" y1="3.165" x2="0.006" y2="4.689" width="0" layer="20" curve="-90"/>
<wire x1="1.53" y1="3.165" x2="5.34" y2="3.165" width="0" layer="20"/>
<wire x1="5.34" y1="3.165" x2="5.975" y2="3.8" width="0" layer="20" curve="-90"/>
<wire x1="5.975" y1="3.8" x2="6.604" y2="3.165" width="0" layer="20" curve="-90"/>
<wire x1="6.604" y1="3.165" x2="6.604" y2="-3.185" width="0" layer="20"/>
<wire x1="1.534" y1="-3.185" x2="0.01" y2="-4.709" width="0" layer="20" curve="90"/>
<wire x1="1.534" y1="-3.185" x2="5.344" y2="-3.185" width="0" layer="20"/>
<wire x1="5.344" y1="-3.185" x2="5.979" y2="-3.82" width="0" layer="20" curve="90"/>
<wire x1="5.979" y1="-3.82" x2="6.604" y2="-3.185" width="0" layer="20" curve="90"/>
<wire x1="0" y1="4.689" x2="0" y2="-4.689" width="0" layer="20"/>
<smd name="1" x="10.29" y="0" dx="5.334" dy="1.7526" layer="1" cream="no"/>
<smd name="2" x="4.7625" y="0" dx="8.509" dy="12.192" layer="16" thermals="no" cream="no"/>
<rectangle x1="0.508" y1="-3.175" x2="7.112" y2="3.175" layer="41"/>
<rectangle x1="0.508" y1="-3.175" x2="7.112" y2="3.175" layer="42"/>
<polygon width="0.1524" layer="30">
<vertex x="0" y="5.08"/>
<vertex x="0" y="-5.08"/>
<vertex x="0.508" y="-5.08"/>
<vertex x="0.508" y="-4.572" curve="-90"/>
<vertex x="1.524" y="-3.556"/>
<vertex x="5.08" y="-3.556" curve="90"/>
<vertex x="5.588" y="-4.064"/>
<vertex x="6.604" y="-4.064" curve="90"/>
<vertex x="7.112" y="-3.556"/>
<vertex x="7.112" y="3.556" curve="90"/>
<vertex x="6.604" y="4.064"/>
<vertex x="5.588" y="4.064" curve="90"/>
<vertex x="5.08" y="3.556"/>
<vertex x="1.524" y="3.556" curve="-90"/>
<vertex x="0.508" y="4.572"/>
<vertex x="0.508" y="5.08"/>
</polygon>
<polygon width="0.1524" layer="29">
<vertex x="0" y="5.08"/>
<vertex x="0" y="-5.08"/>
<vertex x="0.508" y="-5.08"/>
<vertex x="0.508" y="-4.572" curve="-90"/>
<vertex x="1.524" y="-3.556"/>
<vertex x="5.08" y="-3.556" curve="90"/>
<vertex x="5.588" y="-4.064"/>
<vertex x="6.604" y="-4.064" curve="90"/>
<vertex x="7.112" y="-3.556"/>
<vertex x="7.112" y="3.556" curve="90"/>
<vertex x="6.604" y="4.064"/>
<vertex x="5.588" y="4.064" curve="90"/>
<vertex x="5.08" y="3.556"/>
<vertex x="1.524" y="3.556" curve="-90"/>
<vertex x="0.508" y="4.572"/>
<vertex x="0.508" y="5.08"/>
</polygon>
</package>
<package name="RESC1005X40" urn="urn:adsk.eagle:footprint:3811164/1" library_version="38">
<description>CHIP, 1 X 0.5 X 0.4 mm body
&lt;p&gt;CHIP package with body size 1 X 0.5 X 0.4 mm&lt;/p&gt;</description>
<wire x1="0.525" y1="0.614" x2="-0.525" y2="0.614" width="0.12" layer="21"/>
<wire x1="0.525" y1="-0.614" x2="-0.525" y2="-0.614" width="0.12" layer="21"/>
<wire x1="0.525" y1="-0.275" x2="-0.525" y2="-0.275" width="0.12" layer="51"/>
<wire x1="-0.525" y1="-0.275" x2="-0.525" y2="0.275" width="0.12" layer="51"/>
<wire x1="-0.525" y1="0.275" x2="0.525" y2="0.275" width="0.12" layer="51"/>
<wire x1="0.525" y1="0.275" x2="0.525" y2="-0.275" width="0.12" layer="51"/>
<smd name="1" x="-0.475" y="0" dx="0.55" dy="0.6" layer="1"/>
<smd name="2" x="0.475" y="0" dx="0.55" dy="0.6" layer="1"/>
<text x="0" y="1.249" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.249" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC1005X60" urn="urn:adsk.eagle:footprint:3793110/1" library_version="38">
<description>CHIP, 1.025 X 0.525 X 0.6 mm body
&lt;p&gt;CHIP package with body size 1.025 X 0.525 X 0.6 mm&lt;/p&gt;</description>
<wire x1="0.55" y1="0.6325" x2="-0.55" y2="0.6325" width="0.12" layer="21"/>
<wire x1="0.55" y1="-0.6325" x2="-0.55" y2="-0.6325" width="0.12" layer="21"/>
<wire x1="0.55" y1="-0.3" x2="-0.55" y2="-0.3" width="0.12" layer="51"/>
<wire x1="-0.55" y1="-0.3" x2="-0.55" y2="0.3" width="0.12" layer="51"/>
<wire x1="-0.55" y1="0.3" x2="0.55" y2="0.3" width="0.12" layer="51"/>
<wire x1="0.55" y1="0.3" x2="0.55" y2="-0.3" width="0.12" layer="51"/>
<smd name="1" x="-0.4875" y="0" dx="0.5621" dy="0.6371" layer="1"/>
<smd name="2" x="0.4875" y="0" dx="0.5621" dy="0.6371" layer="1"/>
<text x="0" y="1.2675" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.2675" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="DO214AB_SMC" urn="urn:adsk.eagle:footprint:15618117/2" library_version="34">
<description>&lt;b&gt;DIODE&lt;/b&gt;</description>
<smd name="1" x="-3.5685" y="0" dx="2.794" dy="3.81" layer="1"/>
<smd name="2" x="3.5685" y="0" dx="2.794" dy="3.81" layer="1"/>
<wire x1="-4" y1="2.5" x2="-4" y2="3" width="0.127" layer="21"/>
<wire x1="-4" y1="3" x2="4" y2="3" width="0.127" layer="21"/>
<wire x1="4" y1="3" x2="4" y2="2.5" width="0.127" layer="21"/>
<wire x1="-4" y1="-2.5" x2="-4" y2="-3" width="0.127" layer="21"/>
<wire x1="-4" y1="-3" x2="4" y2="-3" width="0.127" layer="21"/>
<wire x1="4" y1="-3" x2="4" y2="-2.5" width="0.127" layer="21"/>
<wire x1="-4" y1="3" x2="4" y2="3" width="0.127" layer="51"/>
<wire x1="-4" y1="-3" x2="4" y2="-3" width="0.127" layer="51"/>
<wire x1="-4" y1="3" x2="-4" y2="2" width="0.127" layer="51"/>
<wire x1="-4" y1="2" x2="-5" y2="2" width="0.127" layer="51"/>
<wire x1="-5" y1="2" x2="-5" y2="-2" width="0.127" layer="51"/>
<wire x1="-5" y1="-2" x2="-4" y2="-2" width="0.127" layer="51"/>
<wire x1="-4" y1="-2" x2="-4" y2="-3" width="0.127" layer="51"/>
<wire x1="4" y1="3" x2="4" y2="2" width="0.127" layer="51"/>
<wire x1="4" y1="2" x2="5" y2="2" width="0.127" layer="51"/>
<wire x1="5" y1="2" x2="5" y2="-2" width="0.127" layer="51"/>
<wire x1="5" y1="-2" x2="4" y2="-2" width="0.127" layer="51"/>
<wire x1="4" y1="-2" x2="4" y2="-3" width="0.127" layer="51"/>
<wire x1="-5.5" y1="3" x2="-5.5" y2="-3" width="0.127" layer="51"/>
<wire x1="-5.5" y1="3" x2="-5.5" y2="-3" width="0.127" layer="21"/>
<wire x1="-5.5" y1="3" x2="-5.5" y2="-3" width="0.127" layer="51"/>
<text x="-5.08" y="3.81" size="0.8128" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="0" y="3.81" size="0.8128" layer="27" font="vector" ratio="15">&gt;VALUE</text>
</package>
<package name="DO214AC_SMA" urn="urn:adsk.eagle:footprint:15671032/1" library_version="30">
<wire x1="3.43" y1="-2.02" x2="-3.43" y2="-2.02" width="0.127" layer="21"/>
<wire x1="-3.43" y1="-2.02" x2="-3.43" y2="2.02" width="0.127" layer="21"/>
<wire x1="-3.43" y1="2.02" x2="3.43" y2="2.02" width="0.127" layer="21"/>
<wire x1="3.43" y1="2.02" x2="3.43" y2="-2.02" width="0.127" layer="21" style="shortdash"/>
<wire x1="3.43" y1="-2.02" x2="-3.43" y2="-2.02" width="0.127" layer="21"/>
<wire x1="-3.43" y1="-2.02" x2="-3.43" y2="2.02" width="0.127" layer="21"/>
<wire x1="-3.43" y1="2.02" x2="3.43" y2="2.02" width="0.127" layer="21"/>
<wire x1="3.43" y1="2.02" x2="3.43" y2="-2.02" width="0.127" layer="21" style="shortdash"/>
<wire x1="3.65" y1="-2" x2="3.65" y2="2" width="0.127" layer="21"/>
<wire x1="3.65" y1="-2" x2="3.65" y2="2" width="0.127" layer="21"/>
<smd name="1" x="-2.25" y="0" dx="1.7" dy="2.5" layer="1" roundness="25"/>
<smd name="2" x="2.25" y="0" dx="1.7" dy="2.5" layer="1" roundness="25"/>
<text x="2" y="3" size="0.635" layer="21" ratio="11" rot="R180">&gt;NAME</text>
<text x="2" y="-2.25" size="0.635" layer="27" font="vector" ratio="10" rot="R180">&gt;VALUE</text>
</package>
<package name="TO457P1000X238-3" urn="urn:adsk.eagle:footprint:3920003/2" library_version="38">
<description>3-TO, DPAK, 4.57 mm pitch, 10 mm span, 6.6 X 6.1 X 2.38 mm body
&lt;p&gt;3-pin TO, DPAK package with 4.57 mm pitch, 10 mm span with body size 6.6 X 6.1 X 2.38 mm&lt;/p&gt;</description>
<circle x="-4.75" y="3.3836" radius="0.25" width="0" layer="21"/>
<wire x1="3.935" y1="3.0207" x2="3.935" y2="3.365" width="0.12" layer="21"/>
<wire x1="3.935" y1="3.365" x2="-2.285" y2="3.365" width="0.12" layer="21"/>
<wire x1="-2.285" y1="3.365" x2="-2.285" y2="-3.365" width="0.12" layer="21"/>
<wire x1="-2.285" y1="-3.365" x2="3.935" y2="-3.365" width="0.12" layer="21"/>
<wire x1="3.935" y1="-3.365" x2="3.935" y2="-3.0207" width="0.12" layer="21"/>
<wire x1="3.935" y1="-3.365" x2="-2.285" y2="-3.365" width="0.12" layer="51"/>
<wire x1="-2.285" y1="-3.365" x2="-2.285" y2="3.365" width="0.12" layer="51"/>
<wire x1="-2.285" y1="3.365" x2="3.935" y2="3.365" width="0.12" layer="51"/>
<wire x1="3.935" y1="3.365" x2="3.935" y2="-3.365" width="0.12" layer="51"/>
<smd name="1" x="-4.75" y="2.285" dx="1.6078" dy="1.1891" layer="1"/>
<smd name="2" x="-4.75" y="-2.285" dx="1.6078" dy="1.1891" layer="1"/>
<smd name="3" x="2.545" y="0" dx="6.0178" dy="5.5334" layer="1"/>
<text x="0" y="4.2686" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-4" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="QFN50P300X300X100-17" urn="urn:adsk.eagle:footprint:3986173/2" library_version="38">
<description>16-QFN, 0.5 mm pitch, 3 X 3 X 1 mm body
&lt;p&gt;16-pin QFN package with 0.5 mm pitch with body size 3 X 3 X 1 mm&lt;/p&gt;</description>
<circle x="-2.054" y="1.386" radius="0.25" width="0" layer="21"/>
<wire x1="-1.55" y1="1.136" x2="-1.55" y2="1.55" width="0.12" layer="21"/>
<wire x1="-1.55" y1="1.55" x2="-1.136" y2="1.55" width="0.12" layer="21"/>
<wire x1="1.55" y1="1.136" x2="1.55" y2="1.55" width="0.12" layer="21"/>
<wire x1="1.55" y1="1.55" x2="1.136" y2="1.55" width="0.12" layer="21"/>
<wire x1="1.55" y1="-1.136" x2="1.55" y2="-1.55" width="0.12" layer="21"/>
<wire x1="1.55" y1="-1.55" x2="1.136" y2="-1.55" width="0.12" layer="21"/>
<wire x1="-1.55" y1="-1.136" x2="-1.55" y2="-1.55" width="0.12" layer="21"/>
<wire x1="-1.55" y1="-1.55" x2="-1.136" y2="-1.55" width="0.12" layer="21"/>
<wire x1="1.55" y1="-1.55" x2="-1.55" y2="-1.55" width="0.12" layer="51"/>
<wire x1="-1.55" y1="-1.55" x2="-1.55" y2="1.55" width="0.12" layer="51"/>
<wire x1="-1.55" y1="1.55" x2="1.55" y2="1.55" width="0.12" layer="51"/>
<wire x1="1.55" y1="1.55" x2="1.55" y2="-1.55" width="0.12" layer="51"/>
<smd name="1" x="-1.4346" y="0.75" dx="0.86" dy="0.264" layer="1" roundness="100"/>
<smd name="2" x="-1.4346" y="0.25" dx="0.86" dy="0.264" layer="1" roundness="100"/>
<smd name="3" x="-1.4346" y="-0.25" dx="0.86" dy="0.264" layer="1" roundness="100"/>
<smd name="4" x="-1.4346" y="-0.75" dx="0.86" dy="0.264" layer="1" roundness="100"/>
<smd name="5" x="-0.75" y="-1.4346" dx="0.86" dy="0.264" layer="1" roundness="100" rot="R90"/>
<smd name="6" x="-0.25" y="-1.4346" dx="0.86" dy="0.264" layer="1" roundness="100" rot="R90"/>
<smd name="7" x="0.25" y="-1.4346" dx="0.86" dy="0.264" layer="1" roundness="100" rot="R90"/>
<smd name="8" x="0.75" y="-1.4346" dx="0.86" dy="0.264" layer="1" roundness="100" rot="R90"/>
<smd name="9" x="1.4346" y="-0.75" dx="0.86" dy="0.264" layer="1" roundness="100"/>
<smd name="10" x="1.4346" y="-0.25" dx="0.86" dy="0.264" layer="1" roundness="100"/>
<smd name="11" x="1.4346" y="0.25" dx="0.86" dy="0.264" layer="1" roundness="100"/>
<smd name="12" x="1.4346" y="0.75" dx="0.86" dy="0.264" layer="1" roundness="100"/>
<smd name="13" x="0.75" y="1.4346" dx="0.86" dy="0.264" layer="1" roundness="100" rot="R90"/>
<smd name="14" x="0.25" y="1.4346" dx="0.86" dy="0.264" layer="1" roundness="100" rot="R90"/>
<smd name="15" x="-0.25" y="1.4346" dx="0.86" dy="0.264" layer="1" roundness="100" rot="R90"/>
<smd name="16" x="-0.75" y="1.4346" dx="0.86" dy="0.264" layer="1" roundness="100" rot="R90"/>
<smd name="17" x="0" y="0" dx="1.45" dy="1.45" layer="1"/>
<text x="0" y="2.4996" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.4996" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="XTAL1140X470X330" urn="urn:adsk.eagle:footprint:3837787/1" library_version="38">
<description>CRYSTAL, 11.4 X 4.7 X 3.3 mm body
&lt;p&gt;CRYSTAL package with body size 11.4 X 4.7 X 3.3 mm&lt;/p&gt;</description>
<wire x1="-5.8" y1="1.0617" x2="-5.8" y2="2.425" width="0.12" layer="21"/>
<wire x1="-5.8" y1="2.425" x2="5.8" y2="2.425" width="0.12" layer="21"/>
<wire x1="5.8" y1="2.425" x2="5.8" y2="1.0617" width="0.12" layer="21"/>
<wire x1="-5.8" y1="-1.0617" x2="-5.8" y2="-2.425" width="0.12" layer="21"/>
<wire x1="-5.8" y1="-2.425" x2="5.8" y2="-2.425" width="0.12" layer="21"/>
<wire x1="5.8" y1="-2.425" x2="5.8" y2="-1.0617" width="0.12" layer="21"/>
<wire x1="5.8" y1="-2.425" x2="-5.8" y2="-2.425" width="0.12" layer="51"/>
<wire x1="-5.8" y1="-2.425" x2="-5.8" y2="2.425" width="0.12" layer="51"/>
<wire x1="-5.8" y1="2.425" x2="5.8" y2="2.425" width="0.12" layer="51"/>
<wire x1="5.8" y1="2.425" x2="5.8" y2="-2.425" width="0.12" layer="51"/>
<smd name="1" x="-4.8216" y="0" dx="4.662" dy="1.6153" layer="1"/>
<smd name="2" x="4.8216" y="0" dx="4.662" dy="1.6153" layer="1"/>
<text x="0" y="3.06" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-3.06" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="POWERPAK_SO-8" urn="urn:adsk.eagle:footprint:8413717/1" library_version="30">
<smd name="1" x="-2.67" y="1.905" dx="1.27" dy="0.61" layer="1"/>
<smd name="2" x="-2.67" y="0.635" dx="1.27" dy="0.61" layer="1"/>
<smd name="3" x="-2.67" y="-0.635" dx="1.27" dy="0.61" layer="1"/>
<smd name="4" x="-2.67" y="-1.905" dx="1.27" dy="0.61" layer="1"/>
<smd name="5" x="2.795" y="-1.905" dx="1.02" dy="0.61" layer="1"/>
<smd name="6" x="2.795" y="-0.635" dx="1.02" dy="0.61" layer="1"/>
<smd name="7" x="2.795" y="0.635" dx="1.02" dy="0.61" layer="1"/>
<smd name="8" x="2.795" y="1.905" dx="1.02" dy="0.61" layer="1"/>
<smd name="9" x="0.69" y="0" dx="3.81" dy="3.91" layer="1"/>
<wire x1="-3.075" y1="2.575" x2="3.075" y2="2.575" width="0.127" layer="21"/>
<wire x1="-3.075" y1="-2.575" x2="3.075" y2="-2.575" width="0.127" layer="21"/>
<wire x1="-3.075" y1="2.575" x2="3.075" y2="2.575" width="0.127" layer="51"/>
<wire x1="-3.075" y1="-2.575" x2="3.075" y2="-2.575" width="0.127" layer="51"/>
<text x="-3.81" y="3.175" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.81" y="-4.445" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="SOIC127P540X170-9T271X340N" urn="urn:adsk.eagle:footprint:15672504/2" library_version="34">
<description>8-SOIC, 1.27 mm pitch, 5.40 mm span, 4.90 X 2.95 X 1.70 mm body, 3.40 X 2.71 mm thermal pad
&lt;p&gt;8-pin SOIC package with 1.27 mm pitch, 5.40 mm span with body size 4.90 X 2.95 X 1.70 mm and thermal pad size 3.40 X 2.71 mm&lt;/p&gt;</description>
<circle x="-2.2905" y="2.7086" radius="0.25" width="0" layer="21"/>
<wire x1="-1.475" y1="2.5186" x2="1.475" y2="2.5186" width="0.12" layer="21"/>
<wire x1="-1.475" y1="-2.5186" x2="1.475" y2="-2.5186" width="0.12" layer="21"/>
<wire x1="1.475" y1="-2.45" x2="-1.475" y2="-2.45" width="0.12" layer="51"/>
<wire x1="-1.475" y1="-2.45" x2="-1.475" y2="2.45" width="0.12" layer="51"/>
<wire x1="-1.475" y1="2.45" x2="1.475" y2="2.45" width="0.12" layer="51"/>
<wire x1="1.475" y1="2.45" x2="1.475" y2="-2.45" width="0.12" layer="51"/>
<smd name="1" x="-2.7" y="1.905" dx="1.55" dy="0.6" layer="1"/>
<smd name="2" x="-2.7" y="0.635" dx="1.55" dy="0.6" layer="1"/>
<smd name="3" x="-2.7" y="-0.635" dx="1.55" dy="0.6" layer="1"/>
<smd name="4" x="-2.7" y="-1.905" dx="1.55" dy="0.6" layer="1"/>
<smd name="5" x="2.7" y="-1.905" dx="1.55" dy="0.6" layer="1"/>
<smd name="6" x="2.7" y="-0.635" dx="1.55" dy="0.6" layer="1"/>
<smd name="7" x="2.7" y="0.635" dx="1.55" dy="0.6" layer="1"/>
<smd name="8" x="2.7" y="1.905" dx="1.55" dy="0.6" layer="1"/>
<smd name="9" x="0" y="0" dx="2.71" dy="3.4" layer="1" thermals="no"/>
<text x="0" y="3.5936" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-3.1536" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="SOIC127P600X175-8" urn="urn:adsk.eagle:footprint:3833981/2" library_version="38">
<description>8-SOIC, 1.27 mm pitch, 6 mm span, 4.9 X 3.9 X 1.75 mm body
&lt;p&gt;8-pin SOIC package with 1.27 mm pitch, 6 mm span with body size 4.9 X 3.9 X 1.75 mm&lt;/p&gt;</description>
<circle x="-2.7288" y="2.7086" radius="0.25" width="0" layer="21"/>
<wire x1="-2" y1="2.5186" x2="2" y2="2.5186" width="0.12" layer="21"/>
<wire x1="-2" y1="-2.5186" x2="2" y2="-2.5186" width="0.12" layer="21"/>
<wire x1="2" y1="-2.5" x2="-2" y2="-2.5" width="0.12" layer="51"/>
<wire x1="-2" y1="-2.5" x2="-2" y2="2.5" width="0.12" layer="51"/>
<wire x1="-2" y1="2.5" x2="2" y2="2.5" width="0.12" layer="51"/>
<wire x1="2" y1="2.5" x2="2" y2="-2.5" width="0.12" layer="51"/>
<smd name="1" x="-2.4734" y="1.905" dx="1.9685" dy="0.5991" layer="1"/>
<smd name="2" x="-2.4734" y="0.635" dx="1.9685" dy="0.5991" layer="1"/>
<smd name="3" x="-2.4734" y="-0.635" dx="1.9685" dy="0.5991" layer="1"/>
<smd name="4" x="-2.4734" y="-1.905" dx="1.9685" dy="0.5991" layer="1"/>
<smd name="5" x="2.4734" y="-1.905" dx="1.9685" dy="0.5991" layer="1"/>
<smd name="6" x="2.4734" y="-0.635" dx="1.9685" dy="0.5991" layer="1"/>
<smd name="7" x="2.4734" y="0.635" dx="1.9685" dy="0.5991" layer="1"/>
<smd name="8" x="2.4734" y="1.905" dx="1.9685" dy="0.5991" layer="1"/>
<text x="0" y="3.5936" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-3.1536" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CIR_PAD_1MM" urn="urn:adsk.eagle:footprint:8413703/1" library_version="30" library_locally_modified="yes">
<text x="-1.905" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<smd name="P$1" x="0" y="0" dx="1" dy="1" layer="1" roundness="100"/>
</package>
<package name="TP_1.5MM" urn="urn:adsk.eagle:footprint:8413705/1" library_version="30" library_locally_modified="yes">
<pad name="1" x="0" y="0" drill="1" diameter="1.5" rot="R180"/>
<text x="-1.905" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<rectangle x1="-0.35" y1="-0.35" x2="0.35" y2="0.35" layer="51"/>
</package>
<package name="TP_1.5MM_PAD" urn="urn:adsk.eagle:footprint:8413706/1" library_version="30" library_locally_modified="yes">
<pad name="1" x="0" y="0" drill="1" diameter="1.5" rot="R180"/>
<text x="-1.27" y="4.445" size="1.27" layer="25">&gt;NAME</text>
<rectangle x1="-0.35" y1="-0.35" x2="0.35" y2="0.35" layer="51"/>
<rectangle x1="-2" y1="-2.5" x2="8" y2="2.5" layer="1"/>
</package>
<package name="TP_1.7MM" urn="urn:adsk.eagle:footprint:8413707/1" library_version="30" library_locally_modified="yes">
<pad name="1" x="0" y="0" drill="1" diameter="1.6764" rot="R180"/>
<text x="-1.905" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<rectangle x1="-0.35" y1="-0.35" x2="0.35" y2="0.35" layer="51"/>
</package>
<package name="C120H40" urn="urn:adsk.eagle:footprint:8413699/1" library_version="30" library_locally_modified="yes">
<pad name="1" x="0" y="0" drill="0.6" diameter="1.2" thermals="no"/>
<text x="0.9" y="-0.1" size="0.254" layer="25">&gt;NAME</text>
</package>
<package name="C131H51" urn="urn:adsk.eagle:footprint:8413700/1" library_version="30" library_locally_modified="yes">
<pad name="1" x="0" y="0" drill="0.71" diameter="1.31" thermals="no"/>
<text x="1.2" y="0.1" size="0.254" layer="25">&gt;NAME</text>
<text x="1.2" y="-0.2" size="0.254" layer="21">&gt;LABEL</text>
</package>
<package name="C406H326" urn="urn:adsk.eagle:footprint:8413701/1" library_version="30" library_locally_modified="yes">
<pad name="1" x="0" y="0" drill="3.26" diameter="4.06" thermals="no"/>
<text x="2.2" y="1.1" size="0.254" layer="25">&gt;NAME</text>
<text x="2.2" y="0.8" size="0.254" layer="21">&gt;LABEL</text>
</package>
<package name="C491H411" urn="urn:adsk.eagle:footprint:8413702/1" library_version="30" library_locally_modified="yes">
<pad name="1" x="0" y="0" drill="4.31" diameter="4.91" thermals="no"/>
<text x="3.2" y="1.1" size="0.254" layer="25">&gt;NAME</text>
<text x="3.2" y="0.8" size="0.254" layer="21">&gt;LABEL</text>
</package>
<package name="CIR_PAD_2MM" urn="urn:adsk.eagle:footprint:8413704/1" library_version="30" library_locally_modified="yes">
<text x="-1.905" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<smd name="P$1" x="0" y="0" dx="2" dy="2" layer="1" roundness="100"/>
</package>
<package name="BEADC1608X55" urn="urn:adsk.eagle:footprint:3887240/1" library_version="38">
<description>CHIP, 1.6 X 0.85 X 0.55 mm body
&lt;p&gt;CHIP package with body size 1.6 X 0.85 X 0.55 mm&lt;/p&gt;</description>
<wire x1="0.875" y1="0.8036" x2="-0.875" y2="0.8036" width="0.12" layer="21"/>
<wire x1="0.875" y1="-0.8036" x2="-0.875" y2="-0.8036" width="0.12" layer="21"/>
<wire x1="0.875" y1="-0.475" x2="-0.875" y2="-0.475" width="0.12" layer="51"/>
<wire x1="-0.875" y1="-0.475" x2="-0.875" y2="0.475" width="0.12" layer="51"/>
<wire x1="-0.875" y1="0.475" x2="0.875" y2="0.475" width="0.12" layer="51"/>
<wire x1="0.875" y1="0.475" x2="0.875" y2="-0.475" width="0.12" layer="51"/>
<smd name="1" x="-0.7996" y="0" dx="0.8709" dy="0.9791" layer="1"/>
<smd name="2" x="0.7996" y="0" dx="0.8709" dy="0.9791" layer="1"/>
<text x="0" y="1.4386" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.4386" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="INDM5050X430" urn="urn:adsk.eagle:footprint:3890170/1" library_version="38">
<description>MOLDED BODY, 5 X 5 X 4.3 mm body
&lt;p&gt;MOLDED BODY package with body size 5 X 5 X 4.3 mm&lt;/p&gt;</description>
<wire x1="-2.51" y1="2.5202" x2="2.51" y2="2.5202" width="0.12" layer="21"/>
<wire x1="-2.51" y1="-2.5202" x2="2.51" y2="-2.5202" width="0.12" layer="21"/>
<wire x1="2.51" y1="-2.51" x2="-2.51" y2="-2.51" width="0.12" layer="51"/>
<wire x1="-2.51" y1="-2.51" x2="-2.51" y2="2.51" width="0.12" layer="51"/>
<wire x1="-2.51" y1="2.51" x2="2.51" y2="2.51" width="0.12" layer="51"/>
<wire x1="2.51" y1="2.51" x2="2.51" y2="-2.51" width="0.12" layer="51"/>
<smd name="1" x="-1.8761" y="0" dx="2.3466" dy="4.4123" layer="1"/>
<smd name="2" x="1.8761" y="0" dx="2.3466" dy="4.4123" layer="1"/>
<text x="0" y="3.1552" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-3.1552" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="RADIAL_THT" urn="urn:adsk.eagle:footprint:8413685/1" library_version="30" library_locally_modified="yes">
<pad name="1" x="1.27" y="0" drill="0.6" diameter="1" rot="R180"/>
<pad name="2" x="-1.27" y="0" drill="0.6" diameter="1" rot="R180"/>
<text x="-3.175" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="4.445" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<circle x="0" y="0" radius="2.60108125" width="0.127" layer="21"/>
</package>
<package name="DIOM7959X262N" urn="urn:adsk.eagle:footprint:15726459/1" library_version="38">
<description>Molded Body, 7.94 X 5.91 X 2.62 mm body
&lt;p&gt;Molded Body package with body size 7.94 X 5.91 X 2.62 mm&lt;/p&gt;</description>
<wire x1="4.065" y1="3.11" x2="-4.8871" y2="3.11" width="0.12" layer="21"/>
<wire x1="-4.8871" y1="3.11" x2="-4.8871" y2="-3.11" width="0.12" layer="21"/>
<wire x1="-4.8871" y1="-3.11" x2="4.065" y2="-3.11" width="0.12" layer="21"/>
<wire x1="4.065" y1="-3.11" x2="-4.065" y2="-3.11" width="0.12" layer="51"/>
<wire x1="-4.065" y1="-3.11" x2="-4.065" y2="3.11" width="0.12" layer="51"/>
<wire x1="-4.065" y1="3.11" x2="4.065" y2="3.11" width="0.12" layer="51"/>
<wire x1="4.065" y1="3.11" x2="4.065" y2="-3.11" width="0.12" layer="51"/>
<smd name="1" x="-3.4827" y="0" dx="2.1808" dy="3.1202" layer="1"/>
<smd name="2" x="3.4827" y="0" dx="2.1808" dy="3.1202" layer="1"/>
<text x="0" y="3.745" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-3.745" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="DIOM7957X265N" urn="urn:adsk.eagle:footprint:15725897/1" library_version="38">
<description>Molded Body, 7.95 X 5.75 X 2.65 mm body
&lt;p&gt;Molded Body package with body size 7.95 X 5.75 X 2.65 mm&lt;/p&gt;</description>
<wire x1="4.075" y1="3.125" x2="-4.8967" y2="3.125" width="0.12" layer="21"/>
<wire x1="-4.8967" y1="3.125" x2="-4.8967" y2="-3.125" width="0.12" layer="21"/>
<wire x1="-4.8967" y1="-3.125" x2="4.075" y2="-3.125" width="0.12" layer="21"/>
<wire x1="4.075" y1="-3.125" x2="-4.075" y2="-3.125" width="0.12" layer="51"/>
<wire x1="-4.075" y1="-3.125" x2="-4.075" y2="3.125" width="0.12" layer="51"/>
<wire x1="-4.075" y1="3.125" x2="4.075" y2="3.125" width="0.12" layer="51"/>
<wire x1="4.075" y1="3.125" x2="4.075" y2="-3.125" width="0.12" layer="51"/>
<smd name="1" x="-3.4983" y="0" dx="2.1688" dy="3.1202" layer="1"/>
<smd name="2" x="3.4983" y="0" dx="2.1688" dy="3.1202" layer="1"/>
<text x="0" y="3.76" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-3.76" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="SOP50P490X110-10" urn="urn:adsk.eagle:footprint:3921299/1" library_version="38">
<description>10-SOP, 0.5 mm pitch, 4.9 mm span, 3 X 3 X 1.1 mm body
&lt;p&gt;10-pin SOP package with 0.5 mm pitch, 4.9 mm span with body size 3 X 3 X 1.1 mm&lt;/p&gt;</description>
<circle x="-2.2175" y="1.659" radius="0.25" width="0" layer="21"/>
<wire x1="-1.55" y1="1.409" x2="-1.55" y2="1.55" width="0.12" layer="21"/>
<wire x1="-1.55" y1="1.55" x2="1.55" y2="1.55" width="0.12" layer="21"/>
<wire x1="1.55" y1="1.55" x2="1.55" y2="1.409" width="0.12" layer="21"/>
<wire x1="-1.55" y1="-1.409" x2="-1.55" y2="-1.55" width="0.12" layer="21"/>
<wire x1="-1.55" y1="-1.55" x2="1.55" y2="-1.55" width="0.12" layer="21"/>
<wire x1="1.55" y1="-1.55" x2="1.55" y2="-1.409" width="0.12" layer="21"/>
<wire x1="1.55" y1="-1.55" x2="-1.55" y2="-1.55" width="0.12" layer="51"/>
<wire x1="-1.55" y1="-1.55" x2="-1.55" y2="1.55" width="0.12" layer="51"/>
<wire x1="-1.55" y1="1.55" x2="1.55" y2="1.55" width="0.12" layer="51"/>
<wire x1="1.55" y1="1.55" x2="1.55" y2="-1.55" width="0.12" layer="51"/>
<smd name="1" x="-2.1496" y="1" dx="1.4709" dy="0.31" layer="1"/>
<smd name="2" x="-2.1496" y="0.5" dx="1.4709" dy="0.31" layer="1"/>
<smd name="3" x="-2.1496" y="0" dx="1.4709" dy="0.31" layer="1"/>
<smd name="4" x="-2.1496" y="-0.5" dx="1.4709" dy="0.31" layer="1"/>
<smd name="5" x="-2.1496" y="-1" dx="1.4709" dy="0.31" layer="1"/>
<smd name="6" x="2.1496" y="-1" dx="1.4709" dy="0.31" layer="1"/>
<smd name="7" x="2.1496" y="-0.5" dx="1.4709" dy="0.31" layer="1"/>
<smd name="8" x="2.1496" y="0" dx="1.4709" dy="0.31" layer="1"/>
<smd name="9" x="2.1496" y="0.5" dx="1.4709" dy="0.31" layer="1"/>
<smd name="10" x="2.1496" y="1" dx="1.4709" dy="0.31" layer="1"/>
<text x="0" y="2.544" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.185" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="RESC0603X30" urn="urn:adsk.eagle:footprint:3811152/1" library_version="38">
<description>CHIP, 0.6 X 0.3 X 0.3 mm body
&lt;p&gt;CHIP package with body size 0.6 X 0.3 X 0.3 mm&lt;/p&gt;</description>
<wire x1="0.315" y1="0.5124" x2="-0.315" y2="0.5124" width="0.12" layer="21"/>
<wire x1="0.315" y1="-0.5124" x2="-0.315" y2="-0.5124" width="0.12" layer="21"/>
<wire x1="0.315" y1="-0.165" x2="-0.315" y2="-0.165" width="0.12" layer="51"/>
<wire x1="-0.315" y1="-0.165" x2="-0.315" y2="0.165" width="0.12" layer="51"/>
<wire x1="-0.315" y1="0.165" x2="0.315" y2="0.165" width="0.12" layer="51"/>
<wire x1="0.315" y1="0.165" x2="0.315" y2="-0.165" width="0.12" layer="51"/>
<smd name="1" x="-0.325" y="0" dx="0.4469" dy="0.3969" layer="1"/>
<smd name="2" x="0.325" y="0" dx="0.4469" dy="0.3969" layer="1"/>
<text x="0" y="1.1474" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.1474" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="RESC3115X70" urn="urn:adsk.eagle:footprint:3811174/1" library_version="38">
<description>CHIP, 3.125 X 1.55 X 0.7 mm body
&lt;p&gt;CHIP package with body size 3.125 X 1.55 X 0.7 mm&lt;/p&gt;</description>
<wire x1="1.625" y1="1.1536" x2="-1.625" y2="1.1536" width="0.12" layer="21"/>
<wire x1="1.625" y1="-1.1536" x2="-1.625" y2="-1.1536" width="0.12" layer="21"/>
<wire x1="1.625" y1="-0.825" x2="-1.625" y2="-0.825" width="0.12" layer="51"/>
<wire x1="-1.625" y1="-0.825" x2="-1.625" y2="0.825" width="0.12" layer="51"/>
<wire x1="-1.625" y1="0.825" x2="1.625" y2="0.825" width="0.12" layer="51"/>
<wire x1="1.625" y1="0.825" x2="1.625" y2="-0.825" width="0.12" layer="51"/>
<smd name="1" x="-1.4449" y="0" dx="1.0841" dy="1.6791" layer="1"/>
<smd name="2" x="1.4449" y="0" dx="1.0841" dy="1.6791" layer="1"/>
<text x="0" y="1.7886" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.7886" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="RESC3225X70" urn="urn:adsk.eagle:footprint:3811185/1" library_version="38">
<description>CHIP, 3.2 X 2.5 X 0.7 mm body
&lt;p&gt;CHIP package with body size 3.2 X 2.5 X 0.7 mm&lt;/p&gt;</description>
<wire x1="1.7" y1="1.6717" x2="-1.7" y2="1.6717" width="0.12" layer="21"/>
<wire x1="1.7" y1="-1.6717" x2="-1.7" y2="-1.6717" width="0.12" layer="21"/>
<wire x1="1.7" y1="-1.35" x2="-1.7" y2="-1.35" width="0.12" layer="51"/>
<wire x1="-1.7" y1="-1.35" x2="-1.7" y2="1.35" width="0.12" layer="51"/>
<wire x1="-1.7" y1="1.35" x2="1.7" y2="1.35" width="0.12" layer="51"/>
<wire x1="1.7" y1="1.35" x2="1.7" y2="-1.35" width="0.12" layer="51"/>
<smd name="1" x="-1.49" y="0" dx="1.1354" dy="2.7153" layer="1"/>
<smd name="2" x="1.49" y="0" dx="1.1354" dy="2.7153" layer="1"/>
<text x="0" y="2.3067" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.3067" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="RESC4532X70" urn="urn:adsk.eagle:footprint:3811192/1" library_version="38">
<description>CHIP, 4.5 X 3.2 X 0.7 mm body
&lt;p&gt;CHIP package with body size 4.5 X 3.2 X 0.7 mm&lt;/p&gt;</description>
<wire x1="2.35" y1="2.0217" x2="-2.35" y2="2.0217" width="0.12" layer="21"/>
<wire x1="2.35" y1="-2.0217" x2="-2.35" y2="-2.0217" width="0.12" layer="21"/>
<wire x1="2.35" y1="-1.7" x2="-2.35" y2="-1.7" width="0.12" layer="51"/>
<wire x1="-2.35" y1="-1.7" x2="-2.35" y2="1.7" width="0.12" layer="51"/>
<wire x1="-2.35" y1="1.7" x2="2.35" y2="1.7" width="0.12" layer="51"/>
<wire x1="2.35" y1="1.7" x2="2.35" y2="-1.7" width="0.12" layer="51"/>
<smd name="1" x="-2.14" y="0" dx="1.1354" dy="3.4153" layer="1"/>
<smd name="2" x="2.14" y="0" dx="1.1354" dy="3.4153" layer="1"/>
<text x="0" y="2.6567" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.6567" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="RESC5025X70" urn="urn:adsk.eagle:footprint:3811199/1" library_version="38">
<description>CHIP, 5 X 2.5 X 0.7 mm body
&lt;p&gt;CHIP package with body size 5 X 2.5 X 0.7 mm&lt;/p&gt;</description>
<wire x1="2.6" y1="1.6717" x2="-2.6" y2="1.6717" width="0.12" layer="21"/>
<wire x1="2.6" y1="-1.6717" x2="-2.6" y2="-1.6717" width="0.12" layer="21"/>
<wire x1="2.6" y1="-1.35" x2="-2.6" y2="-1.35" width="0.12" layer="51"/>
<wire x1="-2.6" y1="-1.35" x2="-2.6" y2="1.35" width="0.12" layer="51"/>
<wire x1="-2.6" y1="1.35" x2="2.6" y2="1.35" width="0.12" layer="51"/>
<wire x1="2.6" y1="1.35" x2="2.6" y2="-1.35" width="0.12" layer="51"/>
<smd name="1" x="-2.34" y="0" dx="1.2354" dy="2.7153" layer="1"/>
<smd name="2" x="2.34" y="0" dx="1.2354" dy="2.7153" layer="1"/>
<text x="0" y="2.3067" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.3067" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="RESC5750X229" urn="urn:adsk.eagle:footprint:3811213/1" library_version="38">
<description>CHIP, 5.7 X 5 X 2.29 mm body
&lt;p&gt;CHIP package with body size 5.7 X 5 X 2.29 mm&lt;/p&gt;</description>
<wire x1="3.05" y1="3.0179" x2="-3.05" y2="3.0179" width="0.12" layer="21"/>
<wire x1="3.05" y1="-3.0179" x2="-3.05" y2="-3.0179" width="0.12" layer="21"/>
<wire x1="3.05" y1="-2.7" x2="-3.05" y2="-2.7" width="0.12" layer="51"/>
<wire x1="-3.05" y1="-2.7" x2="-3.05" y2="2.7" width="0.12" layer="51"/>
<wire x1="-3.05" y1="2.7" x2="3.05" y2="2.7" width="0.12" layer="51"/>
<wire x1="3.05" y1="2.7" x2="3.05" y2="-2.7" width="0.12" layer="51"/>
<smd name="1" x="-2.6355" y="0" dx="1.5368" dy="5.4078" layer="1"/>
<smd name="2" x="2.6355" y="0" dx="1.5368" dy="5.4078" layer="1"/>
<text x="0" y="3.6529" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-3.6529" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="RESC6432X70" urn="urn:adsk.eagle:footprint:3811285/1" library_version="38">
<description>CHIP, 6.4 X 3.2 X 0.7 mm body
&lt;p&gt;CHIP package with body size 6.4 X 3.2 X 0.7 mm&lt;/p&gt;</description>
<wire x1="3.3" y1="2.0217" x2="-3.3" y2="2.0217" width="0.12" layer="21"/>
<wire x1="3.3" y1="-2.0217" x2="-3.3" y2="-2.0217" width="0.12" layer="21"/>
<wire x1="3.3" y1="-1.7" x2="-3.3" y2="-1.7" width="0.12" layer="51"/>
<wire x1="-3.3" y1="-1.7" x2="-3.3" y2="1.7" width="0.12" layer="51"/>
<wire x1="-3.3" y1="1.7" x2="3.3" y2="1.7" width="0.12" layer="51"/>
<wire x1="3.3" y1="1.7" x2="3.3" y2="-1.7" width="0.12" layer="51"/>
<smd name="1" x="-3.04" y="0" dx="1.2354" dy="3.4153" layer="1"/>
<smd name="2" x="3.04" y="0" dx="1.2354" dy="3.4153" layer="1"/>
<text x="0" y="2.6567" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.6567" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="RESC2037X52" urn="urn:adsk.eagle:footprint:3950970/1" library_version="38">
<description>CHIP, 2 X 3.75 X 0.52 mm body
&lt;p&gt;CHIP package with body size 2 X 3.75 X 0.52 mm&lt;/p&gt;</description>
<wire x1="1.1" y1="2.3442" x2="-1.1" y2="2.3442" width="0.12" layer="21"/>
<wire x1="1.1" y1="-2.3442" x2="-1.1" y2="-2.3442" width="0.12" layer="21"/>
<wire x1="1.1" y1="-2.025" x2="-1.1" y2="-2.025" width="0.12" layer="51"/>
<wire x1="-1.1" y1="-2.025" x2="-1.1" y2="2.025" width="0.12" layer="51"/>
<wire x1="-1.1" y1="2.025" x2="1.1" y2="2.025" width="0.12" layer="51"/>
<wire x1="1.1" y1="2.025" x2="1.1" y2="-2.025" width="0.12" layer="51"/>
<smd name="1" x="-0.94" y="0" dx="1.0354" dy="4.0603" layer="1"/>
<smd name="2" x="0.94" y="0" dx="1.0354" dy="4.0603" layer="1"/>
<text x="0" y="2.9792" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.9792" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC1220X107" urn="urn:adsk.eagle:footprint:3793123/1" library_version="38">
<description>CHIP, 1.25 X 2 X 1.07 mm body
&lt;p&gt;CHIP package with body size 1.25 X 2 X 1.07 mm&lt;/p&gt;</description>
<wire x1="0.725" y1="1.4217" x2="-0.725" y2="1.4217" width="0.12" layer="21"/>
<wire x1="0.725" y1="-1.4217" x2="-0.725" y2="-1.4217" width="0.12" layer="21"/>
<wire x1="0.725" y1="-1.1" x2="-0.725" y2="-1.1" width="0.12" layer="51"/>
<wire x1="-0.725" y1="-1.1" x2="-0.725" y2="1.1" width="0.12" layer="51"/>
<wire x1="-0.725" y1="1.1" x2="0.725" y2="1.1" width="0.12" layer="51"/>
<wire x1="0.725" y1="1.1" x2="0.725" y2="-1.1" width="0.12" layer="51"/>
<smd name="1" x="-0.552" y="0" dx="0.7614" dy="2.2153" layer="1"/>
<smd name="2" x="0.552" y="0" dx="0.7614" dy="2.2153" layer="1"/>
<text x="0" y="2.0567" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.0567" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC1632X168" urn="urn:adsk.eagle:footprint:3793133/1" library_version="38">
<description>CHIP, 1.6 X 3.2 X 1.68 mm body
&lt;p&gt;CHIP package with body size 1.6 X 3.2 X 1.68 mm&lt;/p&gt;</description>
<wire x1="0.9" y1="2.0217" x2="-0.9" y2="2.0217" width="0.12" layer="21"/>
<wire x1="0.9" y1="-2.0217" x2="-0.9" y2="-2.0217" width="0.12" layer="21"/>
<wire x1="0.9" y1="-1.7" x2="-0.9" y2="-1.7" width="0.12" layer="51"/>
<wire x1="-0.9" y1="-1.7" x2="-0.9" y2="1.7" width="0.12" layer="51"/>
<wire x1="-0.9" y1="1.7" x2="0.9" y2="1.7" width="0.12" layer="51"/>
<wire x1="0.9" y1="1.7" x2="0.9" y2="-1.7" width="0.12" layer="51"/>
<smd name="1" x="-0.786" y="0" dx="0.9434" dy="3.4153" layer="1"/>
<smd name="2" x="0.786" y="0" dx="0.9434" dy="3.4153" layer="1"/>
<text x="0" y="2.6567" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.6567" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC3215X168" urn="urn:adsk.eagle:footprint:3793146/1" library_version="38">
<description>CHIP, 3.2 X 1.5 X 1.68 mm body
&lt;p&gt;CHIP package with body size 3.2 X 1.5 X 1.68 mm&lt;/p&gt;</description>
<wire x1="1.7" y1="1.1286" x2="-1.7" y2="1.1286" width="0.12" layer="21"/>
<wire x1="1.7" y1="-1.1286" x2="-1.7" y2="-1.1286" width="0.12" layer="21"/>
<wire x1="1.7" y1="-0.8" x2="-1.7" y2="-0.8" width="0.12" layer="51"/>
<wire x1="-1.7" y1="-0.8" x2="-1.7" y2="0.8" width="0.12" layer="51"/>
<wire x1="-1.7" y1="0.8" x2="1.7" y2="0.8" width="0.12" layer="51"/>
<wire x1="1.7" y1="0.8" x2="1.7" y2="-0.8" width="0.12" layer="51"/>
<smd name="1" x="-1.4913" y="0" dx="1.1327" dy="1.6291" layer="1"/>
<smd name="2" x="1.4913" y="0" dx="1.1327" dy="1.6291" layer="1"/>
<text x="0" y="1.7636" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.7636" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC3225X168" urn="urn:adsk.eagle:footprint:3793150/1" library_version="38">
<description>CHIP, 3.2 X 2.5 X 1.68 mm body
&lt;p&gt;CHIP package with body size 3.2 X 2.5 X 1.68 mm&lt;/p&gt;</description>
<wire x1="1.7" y1="1.6717" x2="-1.7" y2="1.6717" width="0.12" layer="21"/>
<wire x1="1.7" y1="-1.6717" x2="-1.7" y2="-1.6717" width="0.12" layer="21"/>
<wire x1="1.7" y1="-1.35" x2="-1.7" y2="-1.35" width="0.12" layer="51"/>
<wire x1="-1.7" y1="-1.35" x2="-1.7" y2="1.35" width="0.12" layer="51"/>
<wire x1="-1.7" y1="1.35" x2="1.7" y2="1.35" width="0.12" layer="51"/>
<wire x1="1.7" y1="1.35" x2="1.7" y2="-1.35" width="0.12" layer="51"/>
<smd name="1" x="-1.4913" y="0" dx="1.1327" dy="2.7153" layer="1"/>
<smd name="2" x="1.4913" y="0" dx="1.1327" dy="2.7153" layer="1"/>
<text x="0" y="2.3067" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.3067" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC4520X168" urn="urn:adsk.eagle:footprint:3793156/1" library_version="38">
<description>CHIP, 4.5 X 2.03 X 1.68 mm body
&lt;p&gt;CHIP package with body size 4.5 X 2.03 X 1.68 mm&lt;/p&gt;</description>
<wire x1="2.375" y1="1.4602" x2="-2.375" y2="1.4602" width="0.12" layer="21"/>
<wire x1="2.375" y1="-1.4602" x2="-2.375" y2="-1.4602" width="0.12" layer="21"/>
<wire x1="2.375" y1="-1.14" x2="-2.375" y2="-1.14" width="0.12" layer="51"/>
<wire x1="-2.375" y1="-1.14" x2="-2.375" y2="1.14" width="0.12" layer="51"/>
<wire x1="-2.375" y1="1.14" x2="2.375" y2="1.14" width="0.12" layer="51"/>
<wire x1="2.375" y1="1.14" x2="2.375" y2="-1.14" width="0.12" layer="51"/>
<smd name="1" x="-2.1266" y="0" dx="1.2091" dy="2.2923" layer="1"/>
<smd name="2" x="2.1266" y="0" dx="1.2091" dy="2.2923" layer="1"/>
<text x="0" y="2.0952" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.0952" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC4532X218" urn="urn:adsk.eagle:footprint:3793162/1" library_version="38">
<description>CHIP, 4.5 X 3.2 X 2.18 mm body
&lt;p&gt;CHIP package with body size 4.5 X 3.2 X 2.18 mm&lt;/p&gt;</description>
<wire x1="2.375" y1="2.0217" x2="-2.375" y2="2.0217" width="0.12" layer="21"/>
<wire x1="2.375" y1="-2.0217" x2="-2.375" y2="-2.0217" width="0.12" layer="21"/>
<wire x1="2.375" y1="-1.7" x2="-2.375" y2="-1.7" width="0.12" layer="51"/>
<wire x1="-2.375" y1="-1.7" x2="-2.375" y2="1.7" width="0.12" layer="51"/>
<wire x1="-2.375" y1="1.7" x2="2.375" y2="1.7" width="0.12" layer="51"/>
<wire x1="2.375" y1="1.7" x2="2.375" y2="-1.7" width="0.12" layer="51"/>
<smd name="1" x="-2.1266" y="0" dx="1.2091" dy="3.4153" layer="1"/>
<smd name="2" x="2.1266" y="0" dx="1.2091" dy="3.4153" layer="1"/>
<text x="0" y="2.6567" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.6567" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC4564X218" urn="urn:adsk.eagle:footprint:3793169/1" library_version="38">
<description>CHIP, 4.5 X 6.4 X 2.18 mm body
&lt;p&gt;CHIP package with body size 4.5 X 6.4 X 2.18 mm&lt;/p&gt;</description>
<wire x1="2.375" y1="3.6452" x2="-2.375" y2="3.6452" width="0.12" layer="21"/>
<wire x1="2.375" y1="-3.6452" x2="-2.375" y2="-3.6452" width="0.12" layer="21"/>
<wire x1="2.375" y1="-3.325" x2="-2.375" y2="-3.325" width="0.12" layer="51"/>
<wire x1="-2.375" y1="-3.325" x2="-2.375" y2="3.325" width="0.12" layer="51"/>
<wire x1="-2.375" y1="3.325" x2="2.375" y2="3.325" width="0.12" layer="51"/>
<wire x1="2.375" y1="3.325" x2="2.375" y2="-3.325" width="0.12" layer="51"/>
<smd name="1" x="-2.1266" y="0" dx="1.2091" dy="6.6623" layer="1"/>
<smd name="2" x="2.1266" y="0" dx="1.2091" dy="6.6623" layer="1"/>
<text x="0" y="4.2802" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-4.2802" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC5650X218" urn="urn:adsk.eagle:footprint:3793172/1" library_version="38">
<description>CHIP, 5.64 X 5.08 X 2.18 mm body
&lt;p&gt;CHIP package with body size 5.64 X 5.08 X 2.18 mm&lt;/p&gt;</description>
<wire x1="2.895" y1="2.9852" x2="-2.895" y2="2.9852" width="0.12" layer="21"/>
<wire x1="2.895" y1="-2.9852" x2="-2.895" y2="-2.9852" width="0.12" layer="21"/>
<wire x1="2.895" y1="-2.665" x2="-2.895" y2="-2.665" width="0.12" layer="51"/>
<wire x1="-2.895" y1="-2.665" x2="-2.895" y2="2.665" width="0.12" layer="51"/>
<wire x1="-2.895" y1="2.665" x2="2.895" y2="2.665" width="0.12" layer="51"/>
<wire x1="2.895" y1="2.665" x2="2.895" y2="-2.665" width="0.12" layer="51"/>
<smd name="1" x="-2.6854" y="0" dx="1.1393" dy="5.3423" layer="1"/>
<smd name="2" x="2.6854" y="0" dx="1.1393" dy="5.3423" layer="1"/>
<text x="0" y="3.6202" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-3.6202" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC5563X218" urn="urn:adsk.eagle:footprint:3793175/1" library_version="38">
<description>CHIP, 5.59 X 6.35 X 2.18 mm body
&lt;p&gt;CHIP package with body size 5.59 X 6.35 X 2.18 mm&lt;/p&gt;</description>
<wire x1="2.92" y1="3.6202" x2="-2.92" y2="3.6202" width="0.12" layer="21"/>
<wire x1="2.92" y1="-3.6202" x2="-2.92" y2="-3.6202" width="0.12" layer="21"/>
<wire x1="2.92" y1="-3.3" x2="-2.92" y2="-3.3" width="0.12" layer="51"/>
<wire x1="-2.92" y1="-3.3" x2="-2.92" y2="3.3" width="0.12" layer="51"/>
<wire x1="-2.92" y1="3.3" x2="2.92" y2="3.3" width="0.12" layer="51"/>
<wire x1="2.92" y1="3.3" x2="2.92" y2="-3.3" width="0.12" layer="51"/>
<smd name="1" x="-2.6716" y="0" dx="1.2091" dy="6.6123" layer="1"/>
<smd name="2" x="2.6716" y="0" dx="1.2091" dy="6.6123" layer="1"/>
<text x="0" y="4.2552" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-4.2552" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC9198X218" urn="urn:adsk.eagle:footprint:3793179/1" library_version="38">
<description>CHIP, 9.14 X 9.82 X 2.18 mm body
&lt;p&gt;CHIP package with body size 9.14 X 9.82 X 2.18 mm&lt;/p&gt;</description>
<wire x1="4.76" y1="5.2799" x2="-4.76" y2="5.2799" width="0.12" layer="21"/>
<wire x1="4.76" y1="-5.2799" x2="-4.76" y2="-5.2799" width="0.12" layer="21"/>
<wire x1="4.76" y1="-4.91" x2="-4.76" y2="-4.91" width="0.12" layer="51"/>
<wire x1="-4.76" y1="-4.91" x2="-4.76" y2="4.91" width="0.12" layer="51"/>
<wire x1="-4.76" y1="4.91" x2="4.76" y2="4.91" width="0.12" layer="51"/>
<wire x1="4.76" y1="4.91" x2="4.76" y2="-4.91" width="0.12" layer="51"/>
<smd name="1" x="-4.4571" y="0" dx="1.314" dy="9.9318" layer="1"/>
<smd name="2" x="4.4571" y="0" dx="1.314" dy="9.9318" layer="1"/>
<text x="0" y="5.9149" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-5.9149" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="GDTMELF4450" urn="urn:adsk.eagle:footprint:3890132/2" library_version="38">
<description>MELF, 4.4 mm length, 5 mm diameter
&lt;p&gt;MELF Resistor package with 4.4 mm length and 5 mm diameter&lt;/p&gt;</description>
<wire x1="1.4868" y1="2.9952" x2="-1.4868" y2="2.9952" width="0.12" layer="21"/>
<wire x1="-1.4868" y1="-2.9952" x2="1.4868" y2="-2.9952" width="0.12" layer="21"/>
<wire x1="2.3" y1="-2.625" x2="-2.3" y2="-2.625" width="0.12" layer="51"/>
<wire x1="-2.3" y1="-2.625" x2="-2.3" y2="2.625" width="0.12" layer="51"/>
<wire x1="-2.3" y1="2.625" x2="2.3" y2="2.625" width="0.12" layer="51"/>
<wire x1="2.3" y1="2.625" x2="2.3" y2="-2.625" width="0.12" layer="51"/>
<text x="0" y="3.6302" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-3.6302" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
<smd name="1" x="-2" y="0" dx="5.6" dy="1.3" layer="1" rot="R90"/>
<smd name="2" x="2" y="0" dx="5.6" dy="1.3" layer="1" rot="R90"/>
</package>
<package name="SOT230P700X180-4" urn="urn:adsk.eagle:footprint:3834047/1" library_version="38">
<description>4-SOT223, 2.3 mm pitch, 7 mm span, 6.5 X 3.5 X 1.8 mm body
&lt;p&gt;4-pin SOT223 package with 2.3 mm pitch, 7 mm span with body size 6.5 X 3.5 X 1.8 mm&lt;/p&gt;</description>
<circle x="-2.9276" y="3.2847" radius="0.25" width="0" layer="21"/>
<wire x1="-1.85" y1="3.0347" x2="-1.85" y2="3.35" width="0.12" layer="21"/>
<wire x1="-1.85" y1="3.35" x2="1.85" y2="3.35" width="0.12" layer="21"/>
<wire x1="1.85" y1="3.35" x2="1.85" y2="1.8847" width="0.12" layer="21"/>
<wire x1="-1.85" y1="-3.0347" x2="-1.85" y2="-3.35" width="0.12" layer="21"/>
<wire x1="-1.85" y1="-3.35" x2="1.85" y2="-3.35" width="0.12" layer="21"/>
<wire x1="1.85" y1="-3.35" x2="1.85" y2="-1.8847" width="0.12" layer="21"/>
<wire x1="1.85" y1="-3.35" x2="-1.85" y2="-3.35" width="0.12" layer="51"/>
<wire x1="-1.85" y1="-3.35" x2="-1.85" y2="3.35" width="0.12" layer="51"/>
<wire x1="-1.85" y1="3.35" x2="1.85" y2="3.35" width="0.12" layer="51"/>
<wire x1="1.85" y1="3.35" x2="1.85" y2="-3.35" width="0.12" layer="51"/>
<smd name="1" x="-2.9226" y="2.3" dx="2.1651" dy="0.9615" layer="1"/>
<smd name="2" x="-2.9226" y="0" dx="2.1651" dy="0.9615" layer="1"/>
<smd name="3" x="-2.9226" y="-2.3" dx="2.1651" dy="0.9615" layer="1"/>
<smd name="4" x="2.9226" y="0" dx="2.1651" dy="3.2615" layer="1"/>
<text x="0" y="4.1697" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-3.985" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="SOT95P280X135-3" urn="urn:adsk.eagle:footprint:3809968/2" library_version="38">
<description>3-SOT23, 0.95 mm pitch, 2.8 mm span, 2.9 X 1.6 X 1.35 mm body
&lt;p&gt;3-pin SOT23 package with 0.95 mm pitch, 2.8 mm span with body size 2.9 X 1.6 X 1.35 mm&lt;/p&gt;</description>
<circle x="-1.3538" y="1.7786" radius="0.25" width="0" layer="21"/>
<wire x1="-0.85" y1="1.5886" x2="0.85" y2="1.5886" width="0.12" layer="21"/>
<wire x1="0.85" y1="1.5886" x2="0.85" y2="0.5786" width="0.12" layer="21"/>
<wire x1="-0.85" y1="-1.5886" x2="0.85" y2="-1.5886" width="0.12" layer="21"/>
<wire x1="0.85" y1="-1.5886" x2="0.85" y2="-0.5786" width="0.12" layer="21"/>
<wire x1="0.85" y1="-1.5" x2="-0.85" y2="-1.5" width="0.12" layer="51"/>
<wire x1="-0.85" y1="-1.5" x2="-0.85" y2="1.5" width="0.12" layer="51"/>
<wire x1="-0.85" y1="1.5" x2="0.85" y2="1.5" width="0.12" layer="51"/>
<wire x1="0.85" y1="1.5" x2="0.85" y2="-1.5" width="0.12" layer="51"/>
<smd name="A" x="-1.2644" y="0.95" dx="1.1864" dy="0.6492" layer="1"/>
<smd name="NC" x="-1.2644" y="-0.95" dx="1.1864" dy="0.6492" layer="1"/>
<smd name="K" x="1.2644" y="0" dx="1.1864" dy="0.6492" layer="1"/>
<text x="0" y="2.6636" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.2236" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="DIOM5226X292" urn="urn:adsk.eagle:footprint:3793195/2" library_version="38">
<description>MOLDED BODY, 5.265 X 2.6 X 2.92 mm body
&lt;p&gt;MOLDED BODY package with body size 5.265 X 2.6 X 2.92 mm&lt;/p&gt;</description>
<wire x1="2.8" y1="1.45" x2="-2.8" y2="1.45" width="0.12" layer="21"/>
<wire x1="-3.6186" y1="1.4614" x2="-3.6186" y2="-1.4614" width="0.12" layer="21"/>
<wire x1="-2.8" y1="-1.45" x2="2.8" y2="-1.45" width="0.12" layer="21"/>
<wire x1="2.8" y1="-1.45" x2="-2.8" y2="-1.45" width="0.12" layer="51"/>
<wire x1="-2.8" y1="-1.45" x2="-2.8" y2="1.45" width="0.12" layer="51"/>
<wire x1="-2.8" y1="1.45" x2="2.8" y2="1.45" width="0.12" layer="51"/>
<wire x1="2.8" y1="1.45" x2="2.8" y2="-1.45" width="0.12" layer="51"/>
<smd name="K" x="-2.164" y="0" dx="2.2812" dy="1.5153" layer="1"/>
<smd name="A" x="2.164" y="0" dx="2.2812" dy="1.5153" layer="1"/>
<text x="0" y="2.085" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.085" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="DIOM5436X265" urn="urn:adsk.eagle:footprint:3793203/2" library_version="38">
<description>MOLDED BODY, 5.4 X 3.6 X 2.65 mm body
&lt;p&gt;MOLDED BODY package with body size 5.4 X 3.6 X 2.65 mm&lt;/p&gt;</description>
<wire x1="-2.795" y1="1.95" x2="2.795" y2="1.95" width="0.12" layer="21"/>
<wire x1="-3.6171" y1="1.95" x2="-3.6171" y2="-1.95" width="0.12" layer="21"/>
<wire x1="2.795" y1="-1.95" x2="-2.795" y2="-1.95" width="0.12" layer="21"/>
<wire x1="2.795" y1="-1.95" x2="-2.795" y2="-1.95" width="0.12" layer="51"/>
<wire x1="-2.795" y1="-1.95" x2="-2.795" y2="1.95" width="0.12" layer="51"/>
<wire x1="-2.795" y1="1.95" x2="2.795" y2="1.95" width="0.12" layer="51"/>
<wire x1="2.795" y1="1.95" x2="2.795" y2="-1.95" width="0.12" layer="51"/>
<smd name="K" x="-2.2127" y="0" dx="2.1808" dy="2.0291" layer="1"/>
<smd name="A" x="2.2127" y="0" dx="2.1808" dy="2.0291" layer="1"/>
<text x="0" y="2.585" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.585" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="DIOM7959X265" urn="urn:adsk.eagle:footprint:3793209/2" library_version="38">
<description>MOLDED BODY, 7.94 X 5.95 X 2.65 mm body
&lt;p&gt;MOLDED BODY package with body size 7.94 X 5.95 X 2.65 mm&lt;/p&gt;</description>
<wire x1="4.065" y1="3.15" x2="-4.07" y2="3.15" width="0.12" layer="21"/>
<wire x1="-4.8871" y1="3.15" x2="-4.8871" y2="-3.15" width="0.12" layer="21"/>
<wire x1="-4.07" y1="-3.15" x2="4.065" y2="-3.15" width="0.12" layer="21"/>
<wire x1="4.065" y1="-3.15" x2="-4.065" y2="-3.15" width="0.12" layer="51"/>
<wire x1="-4.065" y1="-3.15" x2="-4.065" y2="3.15" width="0.12" layer="51"/>
<wire x1="-4.065" y1="3.15" x2="4.065" y2="3.15" width="0.12" layer="51"/>
<wire x1="4.065" y1="3.15" x2="4.065" y2="-3.15" width="0.12" layer="51"/>
<smd name="K" x="-3.4827" y="0" dx="2.1808" dy="3.1202" layer="1"/>
<smd name="A" x="3.4827" y="0" dx="2.1808" dy="3.1202" layer="1"/>
<text x="0" y="3.785" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-3.785" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="SOD2512X100" urn="urn:adsk.eagle:footprint:3793220/2" library_version="38">
<description>SOD, 2.5 mm span, 1.7 X 1.25 X 1 mm body
&lt;p&gt;SOD package with 2.5 mm span with body size 1.7 X 1.25 X 1 mm&lt;/p&gt;</description>
<wire x1="-0.9" y1="0.675" x2="0.9" y2="0.675" width="0.12" layer="21"/>
<wire x1="-2.0217" y1="0.675" x2="-2.0217" y2="-0.675" width="0.12" layer="21"/>
<wire x1="0.9" y1="-0.675" x2="-0.9" y2="-0.675" width="0.12" layer="21"/>
<wire x1="0.9" y1="-0.675" x2="-0.9" y2="-0.675" width="0.12" layer="51"/>
<wire x1="-0.9" y1="-0.675" x2="-0.9" y2="0.675" width="0.12" layer="51"/>
<wire x1="-0.9" y1="0.675" x2="0.9" y2="0.675" width="0.12" layer="51"/>
<wire x1="0.9" y1="0.675" x2="0.9" y2="-0.675" width="0.12" layer="51"/>
<smd name="K" x="-1.1968" y="0" dx="1.0218" dy="0.4971" layer="1"/>
<smd name="A" x="1.1968" y="0" dx="1.0218" dy="0.4971" layer="1"/>
<text x="0" y="1.31" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.31" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="SOD3716X135" urn="urn:adsk.eagle:footprint:3793234/2" library_version="38">
<description>SOD, 3.7 mm span, 2.7 X 1.6 X 1.35 mm body
&lt;p&gt;SOD package with 3.7 mm span with body size 2.7 X 1.6 X 1.35 mm&lt;/p&gt;</description>
<wire x1="-1.425" y1="0.9" x2="1.425" y2="0.9" width="0.12" layer="21"/>
<wire x1="-2.5991" y1="0.9" x2="-2.5991" y2="-0.9" width="0.12" layer="21"/>
<wire x1="1.425" y1="-0.9" x2="-1.425" y2="-0.9" width="0.12" layer="21"/>
<wire x1="1.425" y1="-0.9" x2="-1.425" y2="-0.9" width="0.12" layer="51"/>
<wire x1="-1.425" y1="-0.9" x2="-1.425" y2="0.9" width="0.12" layer="51"/>
<wire x1="-1.425" y1="0.9" x2="1.425" y2="0.9" width="0.12" layer="51"/>
<wire x1="1.425" y1="0.9" x2="1.425" y2="-0.9" width="0.12" layer="51"/>
<smd name="K" x="-1.7116" y="0" dx="1.147" dy="0.7891" layer="1"/>
<smd name="A" x="1.7116" y="0" dx="1.147" dy="0.7891" layer="1"/>
<text x="0" y="1.535" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.535" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="SOT65P210X110-3" urn="urn:adsk.eagle:footprint:3809952/2" library_version="38">
<description>3-SOT23, 0.65 mm pitch, 2.1 mm span, 2 X 1.25 X 1.1 mm body
&lt;p&gt;3-pin SOT23 package with 0.65 mm pitch, 2.1 mm span with body size 2 X 1.25 X 1.1 mm&lt;/p&gt;</description>
<circle x="-1.0698" y="1.409" radius="0.25" width="0" layer="21"/>
<wire x1="-0.675" y1="1.219" x2="0.675" y2="1.219" width="0.12" layer="21"/>
<wire x1="0.675" y1="1.219" x2="0.675" y2="0.509" width="0.12" layer="21"/>
<wire x1="-0.675" y1="-1.219" x2="0.675" y2="-1.219" width="0.12" layer="21"/>
<wire x1="0.675" y1="-1.219" x2="0.675" y2="-0.509" width="0.12" layer="21"/>
<wire x1="0.675" y1="-1.05" x2="-0.675" y2="-1.05" width="0.12" layer="51"/>
<wire x1="-0.675" y1="-1.05" x2="-0.675" y2="1.05" width="0.12" layer="51"/>
<wire x1="-0.675" y1="1.05" x2="0.675" y2="1.05" width="0.12" layer="51"/>
<wire x1="0.675" y1="1.05" x2="0.675" y2="-1.05" width="0.12" layer="51"/>
<smd name="A" x="-0.9704" y="0.65" dx="0.9884" dy="0.51" layer="1"/>
<smd name="NC" x="-0.9704" y="-0.65" dx="0.9884" dy="0.51" layer="1"/>
<smd name="K" x="0.9704" y="0" dx="0.9884" dy="0.51" layer="1"/>
<text x="0" y="2.294" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.854" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="SODFL4725X110" urn="urn:adsk.eagle:footprint:3921258/2" library_version="38">
<description>SODFL, 4.7 mm span, 3.8 X 2.5 X 1.1 mm body
&lt;p&gt;SODFL package with 4.7 mm span with body size 3.8 X 2.5 X 1.1 mm&lt;/p&gt;</description>
<wire x1="-3.0192" y1="1.35" x2="-3.0192" y2="-1.35" width="0.12" layer="21"/>
<wire x1="2" y1="-1.35" x2="-2" y2="-1.35" width="0.12" layer="51"/>
<wire x1="-2" y1="-1.35" x2="-2" y2="1.35" width="0.12" layer="51"/>
<wire x1="-2" y1="1.35" x2="2" y2="1.35" width="0.12" layer="51"/>
<wire x1="2" y1="1.35" x2="2" y2="-1.35" width="0.12" layer="51"/>
<smd name="K" x="-1.9923" y="0" dx="1.4257" dy="1.9202" layer="1"/>
<smd name="A" x="1.9923" y="0" dx="1.4257" dy="1.9202" layer="1"/>
<text x="0" y="1.985" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.985" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="DIOC0502X50" urn="urn:adsk.eagle:footprint:3948091/2" library_version="38">
<description>CHIP, 0.5 X 0.25 X 0.5 mm body
&lt;p&gt;CHIP package with body size 0.5 X 0.25 X 0.5 mm&lt;/p&gt;</description>
<wire x1="-0.754" y1="0.235" x2="-0.754" y2="-0.235" width="0.12" layer="21"/>
<wire x1="0.275" y1="-0.15" x2="-0.275" y2="-0.15" width="0.12" layer="51"/>
<wire x1="-0.275" y1="-0.15" x2="-0.275" y2="0.15" width="0.12" layer="51"/>
<wire x1="-0.275" y1="0.15" x2="0.275" y2="0.15" width="0.12" layer="51"/>
<wire x1="0.275" y1="0.15" x2="0.275" y2="-0.15" width="0.12" layer="51"/>
<smd name="K" x="-0.275" y="0" dx="0.45" dy="0.35" layer="1"/>
<smd name="A" x="0.275" y="0" dx="0.45" dy="0.35" layer="1"/>
<text x="0" y="1.124" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.124" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="DIOC1005X84" urn="urn:adsk.eagle:footprint:3948102/2" library_version="38">
<description>CHIP, 1.07 X 0.56 X 0.84 mm body
&lt;p&gt;CHIP package with body size 1.07 X 0.56 X 0.84 mm&lt;/p&gt;</description>
<wire x1="-1.0741" y1="0.4165" x2="-1.0741" y2="-0.4165" width="0.12" layer="21"/>
<wire x1="0.61" y1="-0.345" x2="-0.61" y2="-0.345" width="0.12" layer="51"/>
<wire x1="-0.61" y1="-0.345" x2="-0.61" y2="0.345" width="0.12" layer="51"/>
<wire x1="-0.61" y1="0.345" x2="0.61" y2="0.345" width="0.12" layer="51"/>
<wire x1="0.61" y1="0.345" x2="0.61" y2="-0.345" width="0.12" layer="51"/>
<smd name="K" x="-0.51" y="0" dx="0.6202" dy="0.713" layer="1"/>
<smd name="A" x="0.51" y="0" dx="0.6202" dy="0.713" layer="1"/>
<text x="0" y="1.3055" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.3055" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="DIOC1206X63" urn="urn:adsk.eagle:footprint:3948142/2" library_version="38">
<description>CHIP, 1.27 X 0.6 X 0.63 mm body
&lt;p&gt;CHIP package with body size 1.27 X 0.6 X 0.63 mm&lt;/p&gt;</description>
<wire x1="-1.1741" y1="0.4365" x2="-1.1741" y2="-0.4365" width="0.12" layer="21"/>
<wire x1="0.71" y1="-0.365" x2="-0.71" y2="-0.365" width="0.12" layer="51"/>
<wire x1="-0.71" y1="-0.365" x2="-0.71" y2="0.365" width="0.12" layer="51"/>
<wire x1="-0.71" y1="0.365" x2="0.71" y2="0.365" width="0.12" layer="51"/>
<wire x1="0.71" y1="0.365" x2="0.71" y2="-0.365" width="0.12" layer="51"/>
<smd name="K" x="-0.545" y="0" dx="0.7502" dy="0.753" layer="1"/>
<smd name="A" x="0.545" y="0" dx="0.7502" dy="0.753" layer="1"/>
<text x="0" y="1.3255" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.3255" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="DIOC1608X84" urn="urn:adsk.eagle:footprint:3948148/2" library_version="38">
<description>CHIP, 1.63 X 0.81 X 0.84 mm body
&lt;p&gt;CHIP package with body size 1.63 X 0.81 X 0.84 mm&lt;/p&gt;</description>
<wire x1="-1.5041" y1="0.5415" x2="-1.5041" y2="-0.5415" width="0.12" layer="21"/>
<wire x1="0.89" y1="-0.47" x2="-0.89" y2="-0.47" width="0.12" layer="51"/>
<wire x1="-0.89" y1="-0.47" x2="-0.89" y2="0.47" width="0.12" layer="51"/>
<wire x1="-0.89" y1="0.47" x2="0.89" y2="0.47" width="0.12" layer="51"/>
<wire x1="0.89" y1="0.47" x2="0.89" y2="-0.47" width="0.12" layer="51"/>
<smd name="K" x="-0.845" y="0" dx="0.8102" dy="0.963" layer="1"/>
<smd name="A" x="0.845" y="0" dx="0.8102" dy="0.963" layer="1"/>
<text x="0" y="1.4305" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.4305" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="DIOC2012X70" urn="urn:adsk.eagle:footprint:3948151/2" library_version="38">
<description>CHIP, 2 X 1.25 X 0.7 mm body
&lt;p&gt;CHIP package with body size 2 X 1.25 X 0.7 mm&lt;/p&gt;</description>
<wire x1="-1.7117" y1="0.7496" x2="-1.7117" y2="-0.7496" width="0.12" layer="21"/>
<wire x1="1.1" y1="-0.675" x2="-1.1" y2="-0.675" width="0.12" layer="51"/>
<wire x1="-1.1" y1="-0.675" x2="-1.1" y2="0.675" width="0.12" layer="51"/>
<wire x1="-1.1" y1="0.675" x2="1.1" y2="0.675" width="0.12" layer="51"/>
<wire x1="1.1" y1="0.675" x2="1.1" y2="-0.675" width="0.12" layer="51"/>
<smd name="K" x="-0.94" y="0" dx="1.0354" dy="1.3791" layer="1"/>
<smd name="A" x="0.94" y="0" dx="1.0354" dy="1.3791" layer="1"/>
<text x="0" y="1.6386" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.6386" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="DIOC3216X84" urn="urn:adsk.eagle:footprint:3948169/2" library_version="38">
<description>CHIP, 3.2 X 1.6 X 0.84 mm body
&lt;p&gt;CHIP package with body size 3.2 X 1.6 X 0.84 mm&lt;/p&gt;</description>
<wire x1="-2.2891" y1="0.9365" x2="-2.2891" y2="-0.9365" width="0.12" layer="21"/>
<wire x1="1.675" y1="-0.865" x2="-1.675" y2="-0.865" width="0.12" layer="51"/>
<wire x1="-1.675" y1="-0.865" x2="-1.675" y2="0.865" width="0.12" layer="51"/>
<wire x1="-1.675" y1="0.865" x2="1.675" y2="0.865" width="0.12" layer="51"/>
<wire x1="1.675" y1="0.865" x2="1.675" y2="-0.865" width="0.12" layer="51"/>
<smd name="K" x="-1.5131" y="0" dx="1.044" dy="1.753" layer="1"/>
<smd name="A" x="1.5131" y="0" dx="1.044" dy="1.753" layer="1"/>
<text x="0" y="1.8255" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.8255" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="DIOC5324X84" urn="urn:adsk.eagle:footprint:3948172/2" library_version="38">
<description>CHIP, 5.31 X 2.49 X 0.84 mm body
&lt;p&gt;CHIP package with body size 5.31 X 2.49 X 0.84 mm&lt;/p&gt;</description>
<wire x1="-3.3441" y1="1.3815" x2="-3.3441" y2="-1.3815" width="0.12" layer="21"/>
<wire x1="2.73" y1="-1.31" x2="-2.73" y2="-1.31" width="0.12" layer="51"/>
<wire x1="-2.73" y1="-1.31" x2="-2.73" y2="1.31" width="0.12" layer="51"/>
<wire x1="-2.73" y1="1.31" x2="2.73" y2="1.31" width="0.12" layer="51"/>
<wire x1="2.73" y1="1.31" x2="2.73" y2="-1.31" width="0.12" layer="51"/>
<smd name="K" x="-2.555" y="0" dx="1.0702" dy="2.643" layer="1"/>
<smd name="A" x="2.555" y="0" dx="1.0702" dy="2.643" layer="1"/>
<text x="0" y="2.2705" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.2705" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="SOIC127P600X170-9T271X340N" urn="urn:adsk.eagle:footprint:15661906/1" library_version="38">
<description>8-SOIC, 1.27 mm pitch, 6.00 mm span, 4.90 X 3.90 X 1.70 mm body, 3.40 X 2.71 mm thermal pad
&lt;p&gt;8-pin SOIC package with 1.27 mm pitch, 6.00 mm span with body size 4.90 X 3.90 X 1.70 mm and thermal pad size 3.40 X 2.71 mm&lt;/p&gt;</description>
<circle x="-2.7288" y="2.7086" radius="0.25" width="0" layer="21"/>
<wire x1="-2" y1="2.4586" x2="-2" y2="2.5" width="0.12" layer="21"/>
<wire x1="-2" y1="2.5" x2="2" y2="2.5" width="0.12" layer="21"/>
<wire x1="2" y1="2.5" x2="2" y2="2.4586" width="0.12" layer="21"/>
<wire x1="-2" y1="-2.4586" x2="-2" y2="-2.5" width="0.12" layer="21"/>
<wire x1="-2" y1="-2.5" x2="2" y2="-2.5" width="0.12" layer="21"/>
<wire x1="2" y1="-2.5" x2="2" y2="-2.4586" width="0.12" layer="21"/>
<wire x1="2" y1="-2.5" x2="-2" y2="-2.5" width="0.12" layer="51"/>
<wire x1="-2" y1="-2.5" x2="-2" y2="2.5" width="0.12" layer="51"/>
<wire x1="-2" y1="2.5" x2="2" y2="2.5" width="0.12" layer="51"/>
<wire x1="2" y1="2.5" x2="2" y2="-2.5" width="0.12" layer="51"/>
<smd name="1" x="-2.4734" y="1.905" dx="1.9685" dy="0.5991" layer="1"/>
<smd name="2" x="-2.4734" y="0.635" dx="1.9685" dy="0.5991" layer="1"/>
<smd name="3" x="-2.4734" y="-0.635" dx="1.9685" dy="0.5991" layer="1"/>
<smd name="4" x="-2.4734" y="-1.905" dx="1.9685" dy="0.5991" layer="1"/>
<smd name="5" x="2.4734" y="-1.905" dx="1.9685" dy="0.5991" layer="1"/>
<smd name="6" x="2.4734" y="-0.635" dx="1.9685" dy="0.5991" layer="1"/>
<smd name="7" x="2.4734" y="0.635" dx="1.9685" dy="0.5991" layer="1"/>
<smd name="8" x="2.4734" y="1.905" dx="1.9685" dy="0.5991" layer="1"/>
<smd name="9" x="0" y="0" dx="2.71" dy="3.4" layer="1" thermals="no" cream="no"/>
<rectangle x1="-1.86" y1="-3.655" x2="1.86" y2="-0.325" layer="31"/>
<rectangle x1="-1.86" y1="0.325" x2="1.86" y2="3.655" layer="31"/>
<text x="0" y="3.5936" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-3.135" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="BEADC0603X30" urn="urn:adsk.eagle:footprint:3853344/1" library_version="38">
<description>CHIP, 0.6 X 0.3 X 0.3 mm body
&lt;p&gt;CHIP package with body size 0.6 X 0.3 X 0.3 mm&lt;/p&gt;</description>
<wire x1="0.315" y1="0.5124" x2="-0.315" y2="0.5124" width="0.12" layer="21"/>
<wire x1="0.315" y1="-0.5124" x2="-0.315" y2="-0.5124" width="0.12" layer="21"/>
<wire x1="0.315" y1="-0.165" x2="-0.315" y2="-0.165" width="0.12" layer="51"/>
<wire x1="-0.315" y1="-0.165" x2="-0.315" y2="0.165" width="0.12" layer="51"/>
<wire x1="-0.315" y1="0.165" x2="0.315" y2="0.165" width="0.12" layer="51"/>
<wire x1="0.315" y1="0.165" x2="0.315" y2="-0.165" width="0.12" layer="51"/>
<smd name="1" x="-0.325" y="0" dx="0.4469" dy="0.3969" layer="1"/>
<smd name="2" x="0.325" y="0" dx="0.4469" dy="0.3969" layer="1"/>
<text x="0" y="1.1474" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.1474" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="BEADC1005X40" urn="urn:adsk.eagle:footprint:3853347/1" library_version="38">
<description>CHIP, 1 X 0.5 X 0.4 mm body
&lt;p&gt;CHIP package with body size 1 X 0.5 X 0.4 mm&lt;/p&gt;</description>
<wire x1="0.525" y1="0.614" x2="-0.525" y2="0.614" width="0.12" layer="21"/>
<wire x1="0.525" y1="-0.614" x2="-0.525" y2="-0.614" width="0.12" layer="21"/>
<wire x1="0.525" y1="-0.275" x2="-0.525" y2="-0.275" width="0.12" layer="51"/>
<wire x1="-0.525" y1="-0.275" x2="-0.525" y2="0.275" width="0.12" layer="51"/>
<wire x1="-0.525" y1="0.275" x2="0.525" y2="0.275" width="0.12" layer="51"/>
<wire x1="0.525" y1="0.275" x2="0.525" y2="-0.275" width="0.12" layer="51"/>
<smd name="1" x="-0.475" y="0" dx="0.55" dy="0.6" layer="1"/>
<smd name="2" x="0.475" y="0" dx="0.55" dy="0.6" layer="1"/>
<text x="0" y="1.249" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.249" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="BEADC2012X70" urn="urn:adsk.eagle:footprint:3887255/1" library_version="38">
<description>CHIP, 2 X 1.25 X 0.7 mm body
&lt;p&gt;CHIP package with body size 2 X 1.25 X 0.7 mm&lt;/p&gt;</description>
<wire x1="1.1" y1="1.0036" x2="-1.1" y2="1.0036" width="0.12" layer="21"/>
<wire x1="1.1" y1="-1.0036" x2="-1.1" y2="-1.0036" width="0.12" layer="21"/>
<wire x1="1.1" y1="-0.675" x2="-1.1" y2="-0.675" width="0.12" layer="51"/>
<wire x1="-1.1" y1="-0.675" x2="-1.1" y2="0.675" width="0.12" layer="51"/>
<wire x1="-1.1" y1="0.675" x2="1.1" y2="0.675" width="0.12" layer="51"/>
<wire x1="1.1" y1="0.675" x2="1.1" y2="-0.675" width="0.12" layer="51"/>
<smd name="1" x="-0.94" y="0" dx="1.0354" dy="1.3791" layer="1"/>
<smd name="2" x="0.94" y="0" dx="1.0354" dy="1.3791" layer="1"/>
<text x="0" y="1.6386" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.6386" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="BEADC3115X70" urn="urn:adsk.eagle:footprint:3887316/1" library_version="38">
<description>CHIP, 3.125 X 1.55 X 0.7 mm body
&lt;p&gt;CHIP package with body size 3.125 X 1.55 X 0.7 mm&lt;/p&gt;</description>
<wire x1="1.625" y1="1.1536" x2="-1.625" y2="1.1536" width="0.12" layer="21"/>
<wire x1="1.625" y1="-1.1536" x2="-1.625" y2="-1.1536" width="0.12" layer="21"/>
<wire x1="1.625" y1="-0.825" x2="-1.625" y2="-0.825" width="0.12" layer="51"/>
<wire x1="-1.625" y1="-0.825" x2="-1.625" y2="0.825" width="0.12" layer="51"/>
<wire x1="-1.625" y1="0.825" x2="1.625" y2="0.825" width="0.12" layer="51"/>
<wire x1="1.625" y1="0.825" x2="1.625" y2="-0.825" width="0.12" layer="51"/>
<smd name="1" x="-1.4449" y="0" dx="1.0841" dy="1.6791" layer="1"/>
<smd name="2" x="1.4449" y="0" dx="1.0841" dy="1.6791" layer="1"/>
<text x="0" y="1.7886" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.7886" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="BEADC3225X70" urn="urn:adsk.eagle:footprint:3887429/1" library_version="38">
<description>CHIP, 3.2 X 2.5 X 0.7 mm body
&lt;p&gt;CHIP package with body size 3.2 X 2.5 X 0.7 mm&lt;/p&gt;</description>
<wire x1="1.7" y1="1.6717" x2="-1.7" y2="1.6717" width="0.12" layer="21"/>
<wire x1="1.7" y1="-1.6717" x2="-1.7" y2="-1.6717" width="0.12" layer="21"/>
<wire x1="1.7" y1="-1.35" x2="-1.7" y2="-1.35" width="0.12" layer="51"/>
<wire x1="-1.7" y1="-1.35" x2="-1.7" y2="1.35" width="0.12" layer="51"/>
<wire x1="-1.7" y1="1.35" x2="1.7" y2="1.35" width="0.12" layer="51"/>
<wire x1="1.7" y1="1.35" x2="1.7" y2="-1.35" width="0.12" layer="51"/>
<smd name="1" x="-1.49" y="0" dx="1.1354" dy="2.7153" layer="1"/>
<smd name="2" x="1.49" y="0" dx="1.1354" dy="2.7153" layer="1"/>
<text x="0" y="2.3067" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.3067" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="BEADC4532X70" urn="urn:adsk.eagle:footprint:3887434/1" library_version="38">
<description>CHIP, 4.5 X 3.2 X 0.7 mm body
&lt;p&gt;CHIP package with body size 4.5 X 3.2 X 0.7 mm&lt;/p&gt;</description>
<wire x1="2.35" y1="2.0217" x2="-2.35" y2="2.0217" width="0.12" layer="21"/>
<wire x1="2.35" y1="-2.0217" x2="-2.35" y2="-2.0217" width="0.12" layer="21"/>
<wire x1="2.35" y1="-1.7" x2="-2.35" y2="-1.7" width="0.12" layer="51"/>
<wire x1="-2.35" y1="-1.7" x2="-2.35" y2="1.7" width="0.12" layer="51"/>
<wire x1="-2.35" y1="1.7" x2="2.35" y2="1.7" width="0.12" layer="51"/>
<wire x1="2.35" y1="1.7" x2="2.35" y2="-1.7" width="0.12" layer="51"/>
<smd name="1" x="-2.14" y="0" dx="1.1354" dy="3.4153" layer="1"/>
<smd name="2" x="2.14" y="0" dx="1.1354" dy="3.4153" layer="1"/>
<text x="0" y="2.6567" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.6567" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="BEADC5025X70" urn="urn:adsk.eagle:footprint:3887438/1" library_version="38">
<description>CHIP, 5 X 2.5 X 0.7 mm body
&lt;p&gt;CHIP package with body size 5 X 2.5 X 0.7 mm&lt;/p&gt;</description>
<wire x1="2.6" y1="1.6717" x2="-2.6" y2="1.6717" width="0.12" layer="21"/>
<wire x1="2.6" y1="-1.6717" x2="-2.6" y2="-1.6717" width="0.12" layer="21"/>
<wire x1="2.6" y1="-1.35" x2="-2.6" y2="-1.35" width="0.12" layer="51"/>
<wire x1="-2.6" y1="-1.35" x2="-2.6" y2="1.35" width="0.12" layer="51"/>
<wire x1="-2.6" y1="1.35" x2="2.6" y2="1.35" width="0.12" layer="51"/>
<wire x1="2.6" y1="1.35" x2="2.6" y2="-1.35" width="0.12" layer="51"/>
<smd name="1" x="-2.34" y="0" dx="1.2354" dy="2.7153" layer="1"/>
<smd name="2" x="2.34" y="0" dx="1.2354" dy="2.7153" layer="1"/>
<text x="0" y="2.3067" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.3067" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="BEADC5750X229" urn="urn:adsk.eagle:footprint:3889734/1" library_version="38">
<description>CHIP, 5.7 X 5 X 2.29 mm body
&lt;p&gt;CHIP package with body size 5.7 X 5 X 2.29 mm&lt;/p&gt;</description>
<wire x1="3.05" y1="3.0179" x2="-3.05" y2="3.0179" width="0.12" layer="21"/>
<wire x1="3.05" y1="-3.0179" x2="-3.05" y2="-3.0179" width="0.12" layer="21"/>
<wire x1="3.05" y1="-2.7" x2="-3.05" y2="-2.7" width="0.12" layer="51"/>
<wire x1="-3.05" y1="-2.7" x2="-3.05" y2="2.7" width="0.12" layer="51"/>
<wire x1="-3.05" y1="2.7" x2="3.05" y2="2.7" width="0.12" layer="51"/>
<wire x1="3.05" y1="2.7" x2="3.05" y2="-2.7" width="0.12" layer="51"/>
<smd name="1" x="-2.6355" y="0" dx="1.5368" dy="5.4078" layer="1"/>
<smd name="2" x="2.6355" y="0" dx="1.5368" dy="5.4078" layer="1"/>
<text x="0" y="3.6529" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-3.6529" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="BEADC6432X70" urn="urn:adsk.eagle:footprint:3889737/1" library_version="38">
<description>CHIP, 6.4 X 3.2 X 0.7 mm body
&lt;p&gt;CHIP package with body size 6.4 X 3.2 X 0.7 mm&lt;/p&gt;</description>
<wire x1="3.3" y1="2.0217" x2="-3.3" y2="2.0217" width="0.12" layer="21"/>
<wire x1="3.3" y1="-2.0217" x2="-3.3" y2="-2.0217" width="0.12" layer="21"/>
<wire x1="3.3" y1="-1.7" x2="-3.3" y2="-1.7" width="0.12" layer="51"/>
<wire x1="-3.3" y1="-1.7" x2="-3.3" y2="1.7" width="0.12" layer="51"/>
<wire x1="-3.3" y1="1.7" x2="3.3" y2="1.7" width="0.12" layer="51"/>
<wire x1="3.3" y1="1.7" x2="3.3" y2="-1.7" width="0.12" layer="51"/>
<smd name="1" x="-3.04" y="0" dx="1.2354" dy="3.4153" layer="1"/>
<smd name="2" x="3.04" y="0" dx="1.2354" dy="3.4153" layer="1"/>
<text x="0" y="2.6567" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.6567" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="INDC1005X60" urn="urn:adsk.eagle:footprint:3810111/1" library_version="38">
<description>CHIP, 1 X 0.5 X 0.6 mm body
&lt;p&gt;CHIP package with body size 1 X 0.5 X 0.6 mm&lt;/p&gt;</description>
<wire x1="0.55" y1="0.6717" x2="-0.55" y2="0.6717" width="0.12" layer="21"/>
<wire x1="0.55" y1="-0.6717" x2="-0.55" y2="-0.6717" width="0.12" layer="21"/>
<wire x1="0.55" y1="-0.35" x2="-0.55" y2="-0.35" width="0.12" layer="51"/>
<wire x1="-0.55" y1="-0.35" x2="-0.55" y2="0.35" width="0.12" layer="51"/>
<wire x1="-0.55" y1="0.35" x2="0.55" y2="0.35" width="0.12" layer="51"/>
<wire x1="0.55" y1="0.35" x2="0.55" y2="-0.35" width="0.12" layer="51"/>
<smd name="1" x="-0.475" y="0" dx="0.5791" dy="0.7153" layer="1"/>
<smd name="2" x="0.475" y="0" dx="0.5791" dy="0.7153" layer="1"/>
<text x="0" y="1.3067" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.3067" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="INDC1608X95" urn="urn:adsk.eagle:footprint:3810120/1" library_version="38">
<description>CHIP, 1.6 X 0.8 X 0.95 mm body
&lt;p&gt;CHIP package with body size 1.6 X 0.8 X 0.95 mm&lt;/p&gt;</description>
<wire x1="0.875" y1="0.7991" x2="-0.875" y2="0.7991" width="0.12" layer="21"/>
<wire x1="0.875" y1="-0.7991" x2="-0.875" y2="-0.7991" width="0.12" layer="21"/>
<wire x1="0.875" y1="-0.475" x2="-0.875" y2="-0.475" width="0.12" layer="51"/>
<wire x1="-0.875" y1="-0.475" x2="-0.875" y2="0.475" width="0.12" layer="51"/>
<wire x1="-0.875" y1="0.475" x2="0.875" y2="0.475" width="0.12" layer="51"/>
<wire x1="0.875" y1="0.475" x2="0.875" y2="-0.475" width="0.12" layer="51"/>
<smd name="1" x="-0.7746" y="0" dx="0.9209" dy="0.9702" layer="1"/>
<smd name="2" x="0.7746" y="0" dx="0.9209" dy="0.9702" layer="1"/>
<text x="0" y="1.4341" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.4341" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="INDC2520X220" urn="urn:adsk.eagle:footprint:3810124/1" library_version="38">
<description>CHIP, 2.5 X 2 X 2.2 mm body
&lt;p&gt;CHIP package with body size 2.5 X 2 X 2.2 mm&lt;/p&gt;</description>
<wire x1="1.35" y1="1.4692" x2="-1.35" y2="1.4692" width="0.12" layer="21"/>
<wire x1="1.35" y1="-1.4692" x2="-1.35" y2="-1.4692" width="0.12" layer="21"/>
<wire x1="1.35" y1="-1.15" x2="-1.35" y2="-1.15" width="0.12" layer="51"/>
<wire x1="-1.35" y1="-1.15" x2="-1.35" y2="1.15" width="0.12" layer="51"/>
<wire x1="-1.35" y1="1.15" x2="1.35" y2="1.15" width="0.12" layer="51"/>
<wire x1="1.35" y1="1.15" x2="1.35" y2="-1.15" width="0.12" layer="51"/>
<smd name="1" x="-1.2283" y="0" dx="0.9588" dy="2.3103" layer="1"/>
<smd name="2" x="1.2283" y="0" dx="0.9588" dy="2.3103" layer="1"/>
<text x="0" y="2.1042" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.1042" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="INDC3225X135" urn="urn:adsk.eagle:footprint:3810699/1" library_version="38">
<description>CHIP, 3.2 X 2.5 X 1.35 mm body
&lt;p&gt;CHIP package with body size 3.2 X 2.5 X 1.35 mm&lt;/p&gt;</description>
<wire x1="1.7" y1="1.6717" x2="-1.7" y2="1.6717" width="0.12" layer="21"/>
<wire x1="1.7" y1="-1.6717" x2="-1.7" y2="-1.6717" width="0.12" layer="21"/>
<wire x1="1.7" y1="-1.35" x2="-1.7" y2="-1.35" width="0.12" layer="51"/>
<wire x1="-1.7" y1="-1.35" x2="-1.7" y2="1.35" width="0.12" layer="51"/>
<wire x1="-1.7" y1="1.35" x2="1.7" y2="1.35" width="0.12" layer="51"/>
<wire x1="1.7" y1="1.35" x2="1.7" y2="-1.35" width="0.12" layer="51"/>
<smd name="1" x="-1.4754" y="0" dx="1.1646" dy="2.7153" layer="1"/>
<smd name="2" x="1.4754" y="0" dx="1.1646" dy="2.7153" layer="1"/>
<text x="0" y="2.3067" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.3067" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="INDC4509X190" urn="urn:adsk.eagle:footprint:3810713/1" library_version="38">
<description>CHIP, 4.5 X 0.9 X 1.9 mm body
&lt;p&gt;CHIP package with body size 4.5 X 0.9 X 1.9 mm&lt;/p&gt;</description>
<wire x1="2.4" y1="0.9192" x2="-2.4" y2="0.9192" width="0.12" layer="21"/>
<wire x1="2.4" y1="-0.9192" x2="-2.4" y2="-0.9192" width="0.12" layer="21"/>
<wire x1="2.4" y1="-0.6" x2="-2.4" y2="-0.6" width="0.12" layer="51"/>
<wire x1="-2.4" y1="-0.6" x2="-2.4" y2="0.6" width="0.12" layer="51"/>
<wire x1="-2.4" y1="0.6" x2="2.4" y2="0.6" width="0.12" layer="51"/>
<wire x1="2.4" y1="0.6" x2="2.4" y2="-0.6" width="0.12" layer="51"/>
<smd name="1" x="-2.11" y="0" dx="1.2904" dy="1.2103" layer="1"/>
<smd name="2" x="2.11" y="0" dx="1.2904" dy="1.2103" layer="1"/>
<text x="0" y="1.5542" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.5542" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="INDC4532X175" urn="urn:adsk.eagle:footprint:3810720/1" library_version="38">
<description>CHIP, 4.5 X 3.2 X 1.75 mm body
&lt;p&gt;CHIP package with body size 4.5 X 3.2 X 1.75 mm&lt;/p&gt;</description>
<wire x1="2.4" y1="2.0217" x2="-2.4" y2="2.0217" width="0.12" layer="21"/>
<wire x1="2.4" y1="-2.0217" x2="-2.4" y2="-2.0217" width="0.12" layer="21"/>
<wire x1="2.4" y1="-1.7" x2="-2.4" y2="-1.7" width="0.12" layer="51"/>
<wire x1="-2.4" y1="-1.7" x2="-2.4" y2="1.7" width="0.12" layer="51"/>
<wire x1="-2.4" y1="1.7" x2="2.4" y2="1.7" width="0.12" layer="51"/>
<wire x1="2.4" y1="1.7" x2="2.4" y2="-1.7" width="0.12" layer="51"/>
<smd name="1" x="-2.0565" y="0" dx="1.3973" dy="3.4153" layer="1"/>
<smd name="2" x="2.0565" y="0" dx="1.3973" dy="3.4153" layer="1"/>
<text x="0" y="2.6567" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.6567" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="INDC5750X180" urn="urn:adsk.eagle:footprint:3810723/1" library_version="38">
<description>CHIP, 5.7 X 5 X 1.8 mm body
&lt;p&gt;CHIP package with body size 5.7 X 5 X 1.8 mm&lt;/p&gt;</description>
<wire x1="2.95" y1="2.9692" x2="-2.95" y2="2.9692" width="0.12" layer="21"/>
<wire x1="2.95" y1="-2.9692" x2="-2.95" y2="-2.9692" width="0.12" layer="21"/>
<wire x1="2.95" y1="-2.65" x2="-2.95" y2="-2.65" width="0.12" layer="51"/>
<wire x1="-2.95" y1="-2.65" x2="-2.95" y2="2.65" width="0.12" layer="51"/>
<wire x1="-2.95" y1="2.65" x2="2.95" y2="2.65" width="0.12" layer="51"/>
<wire x1="2.95" y1="2.65" x2="2.95" y2="-2.65" width="0.12" layer="51"/>
<smd name="1" x="-2.7004" y="0" dx="1.2146" dy="5.3103" layer="1"/>
<smd name="2" x="2.7004" y="0" dx="1.2146" dy="5.3103" layer="1"/>
<text x="0" y="3.6042" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-3.6042" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="INDC6350X200" urn="urn:adsk.eagle:footprint:3811097/1" library_version="38">
<description>CHIP, 6.3 X 5 X 2 mm body
&lt;p&gt;CHIP package with body size 6.3 X 5 X 2 mm&lt;/p&gt;</description>
<wire x1="3.25" y1="2.9692" x2="-3.25" y2="2.9692" width="0.12" layer="21"/>
<wire x1="3.25" y1="-2.9692" x2="-3.25" y2="-2.9692" width="0.12" layer="21"/>
<wire x1="3.25" y1="-2.65" x2="-3.25" y2="-2.65" width="0.12" layer="51"/>
<wire x1="-3.25" y1="-2.65" x2="-3.25" y2="2.65" width="0.12" layer="51"/>
<wire x1="-3.25" y1="2.65" x2="3.25" y2="2.65" width="0.12" layer="51"/>
<wire x1="3.25" y1="2.65" x2="3.25" y2="-2.65" width="0.12" layer="51"/>
<smd name="1" x="-3.0004" y="0" dx="1.2146" dy="5.3103" layer="1"/>
<smd name="2" x="3.0004" y="0" dx="1.2146" dy="5.3103" layer="1"/>
<text x="0" y="3.6042" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-3.6042" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="INDM4848X300" urn="urn:adsk.eagle:footprint:3837794/1" library_version="38">
<description>MOLDED BODY, 4.8 X 4.8 X 3 mm body
&lt;p&gt;MOLDED BODY package with body size 4.8 X 4.8 X 3 mm&lt;/p&gt;</description>
<wire x1="-2.5" y1="2.5" x2="2.5" y2="2.5" width="0.12" layer="21"/>
<wire x1="-2.5" y1="-2.5" x2="2.5" y2="-2.5" width="0.12" layer="21"/>
<wire x1="2.5" y1="-2.5" x2="-2.5" y2="-2.5" width="0.12" layer="51"/>
<wire x1="-2.5" y1="-2.5" x2="-2.5" y2="2.5" width="0.12" layer="51"/>
<wire x1="-2.5" y1="2.5" x2="2.5" y2="2.5" width="0.12" layer="51"/>
<wire x1="2.5" y1="2.5" x2="2.5" y2="-2.5" width="0.12" layer="51"/>
<smd name="1" x="-1.74" y="0" dx="2.5354" dy="4.2118" layer="1"/>
<smd name="2" x="1.74" y="0" dx="2.5354" dy="4.2118" layer="1"/>
<text x="0" y="3.135" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-3.135" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="INDM4040X300" urn="urn:adsk.eagle:footprint:3890468/1" library_version="38">
<description>MOLDED BODY, 4 X 4 X 3 mm body
&lt;p&gt;MOLDED BODY package with body size 4 X 4 X 3 mm&lt;/p&gt;</description>
<wire x1="-2" y1="2.1699" x2="2" y2="2.1699" width="0.12" layer="21"/>
<wire x1="-2" y1="-2.1699" x2="2" y2="-2.1699" width="0.12" layer="21"/>
<wire x1="2" y1="-2" x2="-2" y2="-2" width="0.12" layer="51"/>
<wire x1="-2" y1="-2" x2="-2" y2="2" width="0.12" layer="51"/>
<wire x1="-2" y1="2" x2="2" y2="2" width="0.12" layer="51"/>
<wire x1="2" y1="2" x2="2" y2="-2" width="0.12" layer="51"/>
<smd name="1" x="-1.5795" y="0" dx="1.9528" dy="3.7118" layer="1"/>
<smd name="2" x="1.5795" y="0" dx="1.9528" dy="3.7118" layer="1"/>
<text x="0" y="2.8049" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.8049" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="INDM4440X300" urn="urn:adsk.eagle:footprint:3890490/1" library_version="38">
<description>MOLDED BODY, 4.45 X 4.06 X 3 mm body
&lt;p&gt;MOLDED BODY package with body size 4.45 X 4.06 X 3 mm&lt;/p&gt;</description>
<wire x1="-2.35" y1="2.3349" x2="2.35" y2="2.3349" width="0.12" layer="21"/>
<wire x1="-2.35" y1="-2.3349" x2="2.35" y2="-2.3349" width="0.12" layer="21"/>
<wire x1="2.35" y1="-2.155" x2="-2.35" y2="-2.155" width="0.12" layer="51"/>
<wire x1="-2.35" y1="-2.155" x2="-2.35" y2="2.155" width="0.12" layer="51"/>
<wire x1="-2.35" y1="2.155" x2="2.35" y2="2.155" width="0.12" layer="51"/>
<wire x1="2.35" y1="2.155" x2="2.35" y2="-2.155" width="0.12" layer="51"/>
<smd name="1" x="-2.0191" y="0" dx="1.674" dy="4.0418" layer="1"/>
<smd name="2" x="2.0191" y="0" dx="1.674" dy="4.0418" layer="1"/>
<text x="0" y="2.9699" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.9699" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="INDM7070X450" urn="urn:adsk.eagle:footprint:3890495/1" library_version="38">
<description>MOLDED BODY, 7 X 7 X 4.5 mm body
&lt;p&gt;MOLDED BODY package with body size 7 X 7 X 4.5 mm&lt;/p&gt;</description>
<wire x1="-3.5" y1="3.6699" x2="3.5" y2="3.6699" width="0.12" layer="21"/>
<wire x1="-3.5" y1="-3.6699" x2="3.5" y2="-3.6699" width="0.12" layer="21"/>
<wire x1="3.5" y1="-3.5" x2="-3.5" y2="-3.5" width="0.12" layer="51"/>
<wire x1="-3.5" y1="-3.5" x2="-3.5" y2="3.5" width="0.12" layer="51"/>
<wire x1="-3.5" y1="3.5" x2="3.5" y2="3.5" width="0.12" layer="51"/>
<wire x1="3.5" y1="3.5" x2="3.5" y2="-3.5" width="0.12" layer="51"/>
<smd name="1" x="-3.175" y="0" dx="1.7618" dy="6.7118" layer="1"/>
<smd name="2" x="3.175" y="0" dx="1.7618" dy="6.7118" layer="1"/>
<text x="0" y="4.3049" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-4.3049" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="INDM4949X400" urn="urn:adsk.eagle:footprint:3904917/1" library_version="38">
<description>MOLDED BODY, 4.9 X 4.9 X 4 mm body
&lt;p&gt;MOLDED BODY package with body size 4.9 X 4.9 X 4 mm&lt;/p&gt;</description>
<wire x1="-2.45" y1="2.6199" x2="2.45" y2="2.6199" width="0.12" layer="21"/>
<wire x1="-2.45" y1="-2.6199" x2="2.45" y2="-2.6199" width="0.12" layer="21"/>
<wire x1="2.45" y1="-2.45" x2="-2.45" y2="-2.45" width="0.12" layer="51"/>
<wire x1="-2.45" y1="-2.45" x2="-2.45" y2="2.45" width="0.12" layer="51"/>
<wire x1="-2.45" y1="2.45" x2="2.45" y2="2.45" width="0.12" layer="51"/>
<wire x1="2.45" y1="2.45" x2="2.45" y2="-2.45" width="0.12" layer="51"/>
<smd name="1" x="-1.9795" y="0" dx="2.0528" dy="4.6118" layer="1"/>
<smd name="2" x="1.9795" y="0" dx="2.0528" dy="4.6118" layer="1"/>
<text x="0" y="3.2549" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-3.2549" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="INDM6363X500" urn="urn:adsk.eagle:footprint:3920874/1" library_version="38">
<description>MOLDED BODY, 6.3 X 6.3 X 5 mm body
&lt;p&gt;MOLDED BODY package with body size 6.3 X 6.3 X 5 mm&lt;/p&gt;</description>
<wire x1="-3.15" y1="3.3199" x2="3.15" y2="3.3199" width="0.12" layer="21"/>
<wire x1="-3.15" y1="-3.3199" x2="3.15" y2="-3.3199" width="0.12" layer="21"/>
<wire x1="3.15" y1="-3.15" x2="-3.15" y2="-3.15" width="0.12" layer="51"/>
<wire x1="-3.15" y1="-3.15" x2="-3.15" y2="3.15" width="0.12" layer="51"/>
<wire x1="-3.15" y1="3.15" x2="3.15" y2="3.15" width="0.12" layer="51"/>
<wire x1="3.15" y1="3.15" x2="3.15" y2="-3.15" width="0.12" layer="51"/>
<smd name="1" x="-2.475" y="0" dx="2.4618" dy="6.0118" layer="1"/>
<smd name="2" x="2.475" y="0" dx="2.4618" dy="6.0118" layer="1"/>
<text x="0" y="3.9549" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-3.9549" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="INDM238238X1016" urn="urn:adsk.eagle:footprint:3949007/1" library_version="38">
<description>MOLDED BODY, 23.88 X 23.88 X 10.16 mm body
&lt;p&gt;MOLDED BODY package with body size 23.88 X 23.88 X 10.16 mm&lt;/p&gt;</description>
<wire x1="-11.94" y1="12.1099" x2="11.94" y2="12.1099" width="0.12" layer="21"/>
<wire x1="-11.94" y1="-12.1099" x2="11.94" y2="-12.1099" width="0.12" layer="21"/>
<wire x1="11.94" y1="-11.94" x2="-11.94" y2="-11.94" width="0.12" layer="51"/>
<wire x1="-11.94" y1="-11.94" x2="-11.94" y2="11.94" width="0.12" layer="51"/>
<wire x1="-11.94" y1="11.94" x2="11.94" y2="11.94" width="0.12" layer="51"/>
<wire x1="11.94" y1="11.94" x2="11.94" y2="-11.94" width="0.12" layer="51"/>
<smd name="1" x="-11.035" y="0" dx="2.9218" dy="23.5918" layer="1"/>
<smd name="2" x="11.035" y="0" dx="2.9218" dy="23.5918" layer="1"/>
<text x="0" y="12.7449" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-12.7449" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="INDM8080X400" urn="urn:adsk.eagle:footprint:3950983/1" library_version="38">
<description>MOLDED BODY, 8 X 8 X 4 mm body
&lt;p&gt;MOLDED BODY package with body size 8 X 8 X 4 mm&lt;/p&gt;</description>
<wire x1="-4" y1="4.1699" x2="4" y2="4.1699" width="0.12" layer="21"/>
<wire x1="-4" y1="-4.1699" x2="4" y2="-4.1699" width="0.12" layer="21"/>
<wire x1="4" y1="-4" x2="-4" y2="-4" width="0.12" layer="51"/>
<wire x1="-4" y1="-4" x2="-4" y2="4" width="0.12" layer="51"/>
<wire x1="-4" y1="4" x2="4" y2="4" width="0.12" layer="51"/>
<wire x1="4" y1="4" x2="4" y2="-4" width="0.12" layer="51"/>
<smd name="1" x="-3.175" y="0" dx="2.7618" dy="7.7118" layer="1"/>
<smd name="2" x="3.175" y="0" dx="2.7618" dy="7.7118" layer="1"/>
<text x="0" y="4.8049" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-4.8049" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="INDM125125X650" urn="urn:adsk.eagle:footprint:3950989/1" library_version="38">
<description>MOLDED BODY, 12.5 X 12.5 X 6.5 mm body
&lt;p&gt;MOLDED BODY package with body size 12.5 X 12.5 X 6.5 mm&lt;/p&gt;</description>
<wire x1="-6.25" y1="6.4199" x2="6.25" y2="6.4199" width="0.12" layer="21"/>
<wire x1="-6.25" y1="-6.4199" x2="6.25" y2="-6.4199" width="0.12" layer="21"/>
<wire x1="6.25" y1="-6.25" x2="-6.25" y2="-6.25" width="0.12" layer="51"/>
<wire x1="-6.25" y1="-6.25" x2="-6.25" y2="6.25" width="0.12" layer="51"/>
<wire x1="-6.25" y1="6.25" x2="6.25" y2="6.25" width="0.12" layer="51"/>
<wire x1="6.25" y1="6.25" x2="6.25" y2="-6.25" width="0.12" layer="51"/>
<smd name="1" x="-5.125" y="0" dx="3.3618" dy="12.2118" layer="1"/>
<smd name="2" x="5.125" y="0" dx="3.3618" dy="12.2118" layer="1"/>
<text x="0" y="7.0549" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-7.0549" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="MOLEX_5031590400_SD" urn="urn:adsk.eagle:footprint:8413689/1" library_version="40">
<pad name="P$1" x="0" y="0" drill="0.7"/>
<pad name="P$2" x="1.5" y="2" drill="0.7"/>
<pad name="P$3" x="3" y="0" drill="0.7"/>
<pad name="P$4" x="4.5" y="2" drill="0.7"/>
<wire x1="-2.75" y1="-1.4" x2="-2.75" y2="5.6" width="0.127" layer="21"/>
<wire x1="-2.75" y1="5.6" x2="7.25" y2="5.6" width="0.127" layer="21"/>
<wire x1="7.25" y1="5.6" x2="7.25" y2="-1.4" width="0.127" layer="21"/>
<wire x1="7.25" y1="-1.4" x2="-2.75" y2="-1.4" width="0.127" layer="21"/>
<wire x1="-2.75" y1="-1.4" x2="7.25" y2="-1.4" width="0.127" layer="51"/>
<wire x1="7.25" y1="-1.4" x2="7.25" y2="5.6" width="0.127" layer="51"/>
<wire x1="7.25" y1="5.6" x2="-2.75" y2="5.6" width="0.127" layer="51"/>
<wire x1="-2.75" y1="5.6" x2="-2.75" y2="-1.4" width="0.127" layer="51"/>
<text x="9.8" y="0.7" size="0.6096" layer="25" font="vector">&gt;NAME</text>
<text x="9.8" y="0" size="0.6096" layer="27" font="vector">&gt;VALUE</text>
<hole x="-1.65" y="3.8" drill="0.8"/>
</package>
<package name="SAMTEC_TSW-104-XX-X-S" urn="urn:adsk.eagle:footprint:8413711/1" library_version="40">
<wire x1="-5.209" y1="1.155" x2="5.209" y2="1.155" width="0.2032" layer="21"/>
<wire x1="5.209" y1="1.155" x2="5.209" y2="-1.155" width="0.2032" layer="21"/>
<wire x1="5.209" y1="-1.155" x2="-5.209" y2="-1.155" width="0.2032" layer="21"/>
<wire x1="-5.209" y1="-1.155" x2="-5.209" y2="1.155" width="0.2032" layer="21"/>
<wire x1="5.715" y1="0.9525" x2="5.715" y2="-0.9525" width="0.127" layer="21"/>
<wire x1="3.81" y1="1.5875" x2="5.715" y2="1.5875" width="0.127" layer="51"/>
<wire x1="5.715" y1="1.5875" x2="5.715" y2="0" width="0.127" layer="51"/>
<wire x1="-5.209" y1="1.155" x2="5.209" y2="1.155" width="0.2032" layer="51"/>
<wire x1="5.209" y1="1.155" x2="5.209" y2="-1.155" width="0.2032" layer="51"/>
<wire x1="5.209" y1="-1.155" x2="-5.209" y2="-1.155" width="0.2032" layer="51"/>
<wire x1="-5.209" y1="-1.155" x2="-5.209" y2="1.155" width="0.2032" layer="51"/>
<pad name="1" x="3.81" y="0" drill="1" diameter="1.5" shape="square" rot="R180"/>
<pad name="2" x="1.27" y="0" drill="1" diameter="1.5" rot="R180"/>
<pad name="3" x="-1.27" y="0" drill="1" diameter="1.5" rot="R180"/>
<pad name="4" x="-3.81" y="0" drill="1" diameter="1.5" rot="R180"/>
<text x="-5.715" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="8.255" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-4.16" y1="-0.35" x2="-3.46" y2="0.35" layer="51"/>
<rectangle x1="-1.62" y1="-0.35" x2="-0.92" y2="0.35" layer="51"/>
<rectangle x1="0.92" y1="-0.35" x2="1.62" y2="0.35" layer="51"/>
<rectangle x1="3.46" y1="-0.35" x2="4.16" y2="0.35" layer="51"/>
</package>
<package name="SAMTEC_TSW-104-XX-X-S_NO_OUTLINE" urn="urn:adsk.eagle:footprint:8413712/1" library_version="40">
<pad name="1" x="3.81" y="0" drill="1" diameter="1.5" rot="R180"/>
<pad name="2" x="1.27" y="0" drill="1" diameter="1.5" rot="R180"/>
<pad name="3" x="-1.27" y="0" drill="1" diameter="1.5" rot="R180"/>
<pad name="4" x="-3.81" y="0" drill="1" diameter="1.5" rot="R180"/>
<text x="-5.715" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="8.255" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-4.16" y1="-0.35" x2="-3.46" y2="0.35" layer="51"/>
<rectangle x1="-1.62" y1="-0.35" x2="-0.92" y2="0.35" layer="51"/>
<rectangle x1="0.92" y1="-0.35" x2="1.62" y2="0.35" layer="51"/>
<rectangle x1="3.46" y1="-0.35" x2="4.16" y2="0.35" layer="51"/>
<wire x1="5.08" y1="-0.3175" x2="5.08" y2="0.3175" width="0.254" layer="21"/>
<wire x1="5.08" y1="-0.3175" x2="5.08" y2="0.3175" width="0.254" layer="51"/>
</package>
<package name="SAMTEC_TSW-104-XX-X-S_FLEXSMD_PAD" urn="urn:adsk.eagle:footprint:8413716/1" library_version="40">
<pad name="1" x="3.81" y="0" drill="1" diameter="1.5" rot="R180"/>
<pad name="2" x="1.27" y="0" drill="1" diameter="1.5" rot="R180"/>
<pad name="3" x="-1.27" y="0" drill="1" diameter="1.5" rot="R180"/>
<pad name="4" x="-3.81" y="0" drill="1" diameter="1.5" rot="R180"/>
<text x="-5.715" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="8.255" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-4.16" y1="-0.35" x2="-3.46" y2="0.35" layer="51"/>
<rectangle x1="-1.62" y1="-0.35" x2="-0.92" y2="0.35" layer="51"/>
<rectangle x1="0.92" y1="-0.35" x2="1.62" y2="0.35" layer="51"/>
<rectangle x1="3.46" y1="-0.35" x2="4.16" y2="0.35" layer="51"/>
<smd name="P$1" x="3.81" y="1.27" dx="1.27" dy="0.635" layer="1" rot="R90"/>
<smd name="P$2" x="1.27" y="1.27" dx="1.27" dy="0.635" layer="1" rot="R90"/>
<smd name="P$3" x="-1.27" y="1.27" dx="1.27" dy="0.635" layer="1" rot="R90"/>
<smd name="P$4" x="-3.81" y="1.27" dx="1.27" dy="0.635" layer="1" rot="R90"/>
<wire x1="-3.81" y1="0" x2="-3.81" y2="1.27" width="0.6096" layer="1"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="1.27" width="0.6096" layer="1"/>
<wire x1="1.27" y1="0" x2="1.27" y2="1.27" width="0.6096" layer="1"/>
<wire x1="3.81" y1="0" x2="3.81" y2="1.27" width="0.6096" layer="1"/>
</package>
<package name="JST_B4P-SHF-1AA" urn="urn:adsk.eagle:footprint:8413723/1" library_version="40">
<pad name="1" x="3.75" y="-0.7" drill="0.9"/>
<pad name="2" x="1.25" y="-0.7" drill="0.9"/>
<pad name="3" x="-1.25" y="-0.7" drill="0.9"/>
<pad name="4" x="-3.75" y="-0.7" drill="0.9"/>
<wire x1="-5.25" y1="2.8" x2="5.25" y2="2.8" width="0.127" layer="21"/>
<wire x1="-5.25" y1="-2.8" x2="5.25" y2="-2.8" width="0.127" layer="21"/>
<wire x1="-5.25" y1="-2.8" x2="-5.25" y2="-2.1" width="0.127" layer="21"/>
<wire x1="-5.25" y1="-2.1" x2="-5.25" y2="2.8" width="0.127" layer="21"/>
<wire x1="5.25" y1="-2.8" x2="5.25" y2="-2.1" width="0.127" layer="21"/>
<wire x1="5.25" y1="-2.1" x2="5.25" y2="2.8" width="0.127" layer="21"/>
<wire x1="-5.25" y1="-2.1" x2="5.25" y2="-2.1" width="0.127" layer="21"/>
<wire x1="-5.25" y1="2.8" x2="5.25" y2="2.8" width="0.127" layer="51"/>
<wire x1="-5.25" y1="-2.8" x2="5.25" y2="-2.8" width="0.127" layer="51"/>
<wire x1="-5.25" y1="-2.8" x2="-5.25" y2="-2.1" width="0.127" layer="51"/>
<wire x1="-5.25" y1="-2.1" x2="-5.25" y2="2.8" width="0.127" layer="51"/>
<wire x1="5.25" y1="-2.8" x2="5.25" y2="-2.1" width="0.127" layer="51"/>
<wire x1="5.25" y1="-2.1" x2="5.25" y2="2.8" width="0.127" layer="51"/>
<wire x1="-5.25" y1="-2.1" x2="5.25" y2="-2.1" width="0.127" layer="51"/>
<text x="-0.55" y="-4.3" size="1.27" layer="25">&gt;NAME</text>
<text x="-7.75" y="-4.3" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="JST_BS4P-SHF-1AA" urn="urn:adsk.eagle:footprint:8413724/1" library_version="40">
<pad name="1" x="3.75" y="0" drill="0.9"/>
<pad name="2" x="1.25" y="0" drill="0.9"/>
<pad name="3" x="-1.25" y="0" drill="0.9"/>
<pad name="4" x="-3.75" y="0" drill="0.9"/>
<text x="-0.55" y="-4.5" size="1.27" layer="25">&gt;NAME</text>
<text x="-7.75" y="-4.5" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-5.21" y1="3" x2="5.25" y2="3" width="0.127" layer="21"/>
<wire x1="-5.21" y1="-3" x2="5.25" y2="-3" width="0.127" layer="21"/>
<wire x1="-5.21" y1="3" x2="-5.21" y2="-3" width="0.127" layer="21"/>
<wire x1="5.25" y1="3" x2="5.25" y2="-3" width="0.127" layer="21"/>
<wire x1="-5.21" y1="3" x2="5.25" y2="3" width="0.127" layer="51"/>
<wire x1="-5.21" y1="-3" x2="5.25" y2="-3" width="0.127" layer="51"/>
<wire x1="-5.21" y1="3" x2="-5.21" y2="-3" width="0.127" layer="51"/>
<wire x1="5.25" y1="3" x2="5.25" y2="-3" width="0.127" layer="51"/>
</package>
<package name="JST_F4P-HVQ" urn="urn:adsk.eagle:footprint:8413725/1" library_version="40">
<pad name="1" x="3.75" y="-0.7" drill="1"/>
<pad name="2" x="1.25" y="-0.7" drill="1"/>
<pad name="3" x="-1.25" y="-0.7" drill="1"/>
<pad name="4" x="-3.75" y="-0.7" drill="1"/>
<wire x1="-7.25" y1="-5.3" x2="-7.25" y2="2.8" width="0.127" layer="51"/>
<wire x1="7.25" y1="-5.3" x2="7.25" y2="2.8" width="0.127" layer="51"/>
<wire x1="-7.25" y1="2.8" x2="7.25" y2="2.8" width="0.127" layer="51"/>
<wire x1="-5.75" y1="-1.9" x2="5.75" y2="-1.9" width="0.127" layer="51"/>
<wire x1="-7.25" y1="-5.3" x2="-5.75" y2="-5.3" width="0.127" layer="51"/>
<wire x1="5.75" y1="-5.3" x2="7.25" y2="-5.3" width="0.127" layer="51"/>
<wire x1="-5.75" y1="-1.9" x2="-5.75" y2="-5.3" width="0.127" layer="51"/>
<wire x1="5.75" y1="-1.9" x2="5.75" y2="-5.3" width="0.127" layer="51"/>
<wire x1="-7.25" y1="-1.9" x2="-7.25" y2="2.8" width="0.127" layer="21"/>
<wire x1="7.25" y1="-1.9" x2="7.25" y2="2.8" width="0.127" layer="21"/>
<wire x1="-7.25" y1="2.8" x2="7.25" y2="2.8" width="0.127" layer="21"/>
<wire x1="-7.25" y1="-1.9" x2="7.25" y2="-1.9" width="0.127" layer="21"/>
<text x="-0.55" y="-6.95" size="1.27" layer="25">&gt;NAME</text>
<text x="-7.75" y="-6.95" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="JST_F4P-SHVQ" urn="urn:adsk.eagle:footprint:8413726/1" library_version="40">
<pad name="1" x="3.82" y="-3.35" drill="1"/>
<pad name="2" x="1.32" y="-3.35" drill="1"/>
<pad name="3" x="-1.18" y="-3.35" drill="1"/>
<pad name="4" x="-3.68" y="-3.35" drill="1"/>
<text x="1.3" y="-5.95" size="1.27" layer="25">&gt;NAME</text>
<text x="-6.2" y="-5.95" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-7.2" y1="3.05" x2="7.3" y2="3.05" width="0.127" layer="21"/>
<wire x1="-7.2" y1="-4.45" x2="-7.2" y2="3.05" width="0.127" layer="21"/>
<wire x1="7.3" y1="-4.45" x2="7.3" y2="3.05" width="0.127" layer="21"/>
<wire x1="7.3" y1="-4.45" x2="-7.2" y2="-4.45" width="0.127" layer="21"/>
<wire x1="-5.7" y1="3.05" x2="5.8" y2="3.05" width="0.127" layer="51"/>
<wire x1="-7.2" y1="-4.45" x2="-7.2" y2="4.85" width="0.127" layer="51"/>
<wire x1="7.3" y1="-4.45" x2="7.3" y2="4.85" width="0.127" layer="51"/>
<wire x1="7.3" y1="-4.45" x2="-7.2" y2="-4.45" width="0.127" layer="51"/>
<wire x1="-7.2" y1="4.85" x2="-5.7" y2="4.85" width="0.127" layer="51"/>
<wire x1="5.8" y1="4.85" x2="7.3" y2="4.85" width="0.127" layer="51"/>
<wire x1="-5.7" y1="4.85" x2="-5.7" y2="3.05" width="0.127" layer="51"/>
<wire x1="5.8" y1="4.85" x2="5.8" y2="3.05" width="0.127" layer="51"/>
</package>
<package name="JST_04JQ-BT" urn="urn:adsk.eagle:footprint:8413851/1" library_version="40">
<pad name="1" x="3.75" y="0" drill="0.9"/>
<pad name="2" x="1.25" y="0" drill="0.9"/>
<pad name="3" x="-1.25" y="0" drill="0.9"/>
<pad name="4" x="-3.75" y="0" drill="0.9"/>
<wire x1="-6.25" y1="2.7" x2="6.25" y2="2.7" width="0.127" layer="21"/>
<wire x1="6.25" y1="-3.3" x2="-6.25" y2="-3.3" width="0.127" layer="21"/>
<wire x1="-6.25" y1="2.7" x2="-6.25" y2="-3.3" width="0.127" layer="21"/>
<wire x1="6.25" y1="2.7" x2="6.25" y2="-3.3" width="0.127" layer="21"/>
<wire x1="-6.25" y1="2.7" x2="6.25" y2="2.7" width="0.127" layer="51"/>
<wire x1="6.25" y1="-3.3" x2="-6.25" y2="-3.3" width="0.127" layer="51"/>
<wire x1="-6.25" y1="2.7" x2="-6.25" y2="-3.3" width="0.127" layer="51"/>
<wire x1="6.25" y1="2.7" x2="6.25" y2="-3.3" width="0.127" layer="51"/>
<text x="0.5" y="2.93" size="1.27" layer="25">&gt;NAME</text>
<text x="-7.75" y="2.93" size="1.27" layer="27">&gt;VALUE</text>
<circle x="3.75" y="-3.85" radius="0.25" width="0.127" layer="21"/>
<circle x="3.75" y="-3.85" radius="0.13" width="0.127" layer="21"/>
<circle x="3.75" y="-3.85" radius="0.014140625" width="0.127" layer="21"/>
<circle x="3.75" y="-3.85" radius="0.25" width="0.127" layer="51"/>
<circle x="3.75" y="-3.85" radius="0.13" width="0.127" layer="51"/>
<circle x="3.75" y="-3.85" radius="0.014140625" width="0.127" layer="51"/>
</package>
<package name="JST_04JQ-ST" urn="urn:adsk.eagle:footprint:8413852/1" library_version="40">
<pad name="1" x="3.75" y="0" drill="0.9"/>
<pad name="2" x="1.25" y="0" drill="0.9"/>
<pad name="3" x="-1.25" y="0" drill="0.9"/>
<pad name="4" x="-3.75" y="0" drill="0.9"/>
<wire x1="-6.25" y1="-2.2" x2="6.25" y2="-2.2" width="0.127" layer="51"/>
<wire x1="-6.25" y1="2.6" x2="-5.35" y2="2.6" width="0.127" layer="51"/>
<wire x1="-5.35" y1="2.6" x2="5.35" y2="2.6" width="0.127" layer="51"/>
<wire x1="5.35" y1="2.6" x2="6.25" y2="2.6" width="0.127" layer="51"/>
<wire x1="6.25" y1="-2.2" x2="6.25" y2="2.6" width="0.127" layer="51"/>
<wire x1="-6.25" y1="-2.2" x2="-6.25" y2="2.6" width="0.127" layer="51"/>
<wire x1="-6.25" y1="-2.2" x2="6.25" y2="-2.2" width="0.127" layer="21"/>
<wire x1="-6.25" y1="2.6" x2="6.25" y2="2.6" width="0.127" layer="21"/>
<wire x1="6.25" y1="-2.2" x2="6.25" y2="2.6" width="0.127" layer="21"/>
<wire x1="-6.25" y1="-2.2" x2="-6.25" y2="2.6" width="0.127" layer="21"/>
<wire x1="-5.35" y1="2.6" x2="-5.35" y2="7.3" width="0.127" layer="51"/>
<wire x1="5.35" y1="2.6" x2="5.35" y2="7.3" width="0.127" layer="51"/>
<wire x1="-5.35" y1="7.3" x2="5.35" y2="7.3" width="0.127" layer="51"/>
<text x="-4.15" y="-3.82" size="1.27" layer="25">&gt;NAME</text>
<text x="-12.4" y="-3.82" size="1.27" layer="27">&gt;VALUE</text>
<circle x="3.75" y="-2.75" radius="0.25" width="0.127" layer="21"/>
<circle x="3.75" y="-2.75" radius="0.13" width="0.127" layer="21"/>
<circle x="3.75" y="-2.75" radius="0.014140625" width="0.127" layer="21"/>
<circle x="3.75" y="-2.75" radius="0.25" width="0.127" layer="51"/>
<circle x="3.75" y="-2.75" radius="0.13" width="0.127" layer="51"/>
<circle x="3.75" y="-2.75" radius="0.014140625" width="0.127" layer="51"/>
</package>
<package name="JST_B4B-XH-A" urn="urn:adsk.eagle:footprint:8413853/1" library_version="40">
<pad name="1" x="3.73" y="0" drill="0.9"/>
<pad name="2" x="1.23" y="0" drill="0.9"/>
<pad name="3" x="-1.27" y="0" drill="0.9"/>
<pad name="4" x="-3.77" y="0" drill="0.9"/>
<wire x1="-6.22" y1="2.35" x2="6.18" y2="2.35" width="0.127" layer="51"/>
<wire x1="-6.22" y1="-3.4" x2="6.18" y2="-3.4" width="0.127" layer="51"/>
<wire x1="-6.22" y1="2.35" x2="-6.22" y2="-3.4" width="0.127" layer="51"/>
<wire x1="6.18" y1="2.35" x2="6.18" y2="-3.4" width="0.127" layer="51"/>
<wire x1="-6.22" y1="2.35" x2="6.18" y2="2.35" width="0.127" layer="21"/>
<wire x1="-6.22" y1="-3.4" x2="6.18" y2="-3.4" width="0.127" layer="21"/>
<wire x1="-6.22" y1="2.35" x2="-6.22" y2="-3.4" width="0.127" layer="21"/>
<wire x1="6.18" y1="2.35" x2="6.18" y2="-3.4" width="0.127" layer="21"/>
<text x="-2.75" y="-4.94" size="1.27" layer="25">&gt;NAME</text>
<text x="-10" y="-4.94" size="1.27" layer="27">&gt;VALUE</text>
<circle x="3.73" y="-4" radius="0.25" width="0.127" layer="21"/>
<circle x="3.73" y="-4" radius="0.13" width="0.127" layer="21"/>
<circle x="3.73" y="-4" radius="0.014140625" width="0.127" layer="21"/>
<circle x="3.73" y="-4" radius="0.25" width="0.127" layer="51"/>
<circle x="3.73" y="-4" radius="0.13" width="0.127" layer="51"/>
<circle x="3.73" y="-4" radius="0.014140625" width="0.127" layer="51"/>
</package>
<package name="JST_S4B-XH-A-1" urn="urn:adsk.eagle:footprint:8413854/1" library_version="40">
<pad name="1" x="3.75" y="0" drill="0.9"/>
<pad name="2" x="1.25" y="0" drill="0.9"/>
<pad name="3" x="-1.25" y="0" drill="0.9"/>
<pad name="4" x="-3.75" y="0" drill="0.9"/>
<wire x1="-6.2" y1="7.6" x2="6.2" y2="7.6" width="0.127" layer="21"/>
<wire x1="-6.2" y1="-3.9" x2="6.2" y2="-3.9" width="0.127" layer="21"/>
<wire x1="-6.2" y1="-3.9" x2="-6.2" y2="7.6" width="0.127" layer="21"/>
<wire x1="6.2" y1="-3.9" x2="6.2" y2="7.6" width="0.127" layer="21"/>
<wire x1="-6.2" y1="7.6" x2="6.2" y2="7.6" width="0.127" layer="51"/>
<wire x1="-6.2" y1="-3.9" x2="6.2" y2="-3.9" width="0.127" layer="51"/>
<wire x1="-6.2" y1="-3.9" x2="-6.2" y2="7.6" width="0.127" layer="51"/>
<wire x1="6.2" y1="-3.9" x2="6.2" y2="7.6" width="0.127" layer="51"/>
<text x="0.2" y="-5.395" size="1.27" layer="25">&gt;NAME</text>
<text x="-7.3" y="-5.395" size="1.27" layer="27">&gt;VALUE</text>
<circle x="3.75" y="-2.125" radius="0.25" width="0.127" layer="21"/>
<circle x="3.75" y="-2.125" radius="0.13" width="0.127" layer="21"/>
<circle x="3.75" y="-2.125" radius="0.014140625" width="0.127" layer="21"/>
<circle x="3.75" y="-2.125" radius="0.25" width="0.127" layer="51"/>
<circle x="3.75" y="-2.125" radius="0.13" width="0.127" layer="51"/>
<circle x="3.75" y="-2.125" radius="0.014140625" width="0.127" layer="51"/>
</package>
<package name="JST_B04B-PASK" urn="urn:adsk.eagle:footprint:8413901/1" library_version="40">
<pad name="P$1" x="3" y="0" drill="0.7" rot="R90"/>
<pad name="P$2" x="1" y="0" drill="0.7" rot="R90"/>
<pad name="P$3" x="-1" y="0" drill="0.7" rot="R90"/>
<pad name="P$4" x="-3" y="0" drill="0.7" rot="R90"/>
<hole x="4.5" y="1.7" drill="1.1"/>
<wire x1="-5" y1="-3.05" x2="5" y2="-3.05" width="0.127" layer="21"/>
<wire x1="-5" y1="2.25" x2="4.03" y2="2.25" width="0.127" layer="21"/>
<wire x1="-5" y1="2.25" x2="-5" y2="-3.05" width="0.127" layer="21"/>
<wire x1="5" y1="-3.05" x2="5" y2="1.17" width="0.127" layer="21"/>
<wire x1="-5" y1="-3.05" x2="5" y2="-3.05" width="0.127" layer="51"/>
<wire x1="-5" y1="2.25" x2="4.03" y2="2.25" width="0.127" layer="51"/>
<wire x1="-5" y1="2.25" x2="-5" y2="-3.05" width="0.127" layer="51"/>
<wire x1="5" y1="-3.05" x2="5" y2="1.17" width="0.127" layer="51"/>
<text x="-2.5" y="2.48" size="1.27" layer="25">&gt;NAME</text>
<text x="-10.75" y="2.48" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="JST_S04B-PASK-2" urn="urn:adsk.eagle:footprint:8413902/1" library_version="40">
<pad name="1" x="3" y="-2.35" drill="0.7"/>
<pad name="2" x="1" y="-2.35" drill="0.7"/>
<pad name="3" x="-1" y="-2.35" drill="0.7"/>
<pad name="4" x="-3" y="-2.35" drill="0.7"/>
<hole x="4.5" y="-0.25" drill="1.1"/>
<hole x="-4.5" y="-0.25" drill="1.1"/>
<wire x1="-5" y1="5.85" x2="5" y2="5.85" width="0.127" layer="21"/>
<wire x1="-5" y1="-5.85" x2="5" y2="-5.85" width="0.127" layer="21"/>
<wire x1="-5" y1="5.85" x2="-5" y2="0.27" width="0.127" layer="21"/>
<wire x1="5" y1="5.85" x2="5" y2="0.27" width="0.127" layer="21"/>
<wire x1="-5" y1="-5.85" x2="-5" y2="-0.77" width="0.127" layer="21"/>
<wire x1="5" y1="-5.85" x2="5" y2="-0.77" width="0.127" layer="21"/>
<wire x1="-5" y1="5.85" x2="5" y2="5.85" width="0.127" layer="51"/>
<wire x1="-5" y1="-5.85" x2="5" y2="-5.85" width="0.127" layer="51"/>
<wire x1="-5" y1="5.85" x2="-5" y2="0.27" width="0.127" layer="51"/>
<wire x1="5" y1="5.85" x2="5" y2="0.27" width="0.127" layer="51"/>
<wire x1="-5" y1="-5.85" x2="-5" y2="-0.77" width="0.127" layer="51"/>
<wire x1="5" y1="-5.85" x2="5" y2="-0.77" width="0.127" layer="51"/>
<text x="-0.95" y="-7.35" size="1.27" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="-9.2" y="-7.35" size="1.27" layer="27" font="vector" ratio="15">&gt;VALUE</text>
<circle x="3" y="-4" radius="0.25" width="0.127" layer="21"/>
<circle x="3" y="-4" radius="0.13" width="0.127" layer="21"/>
<circle x="3" y="-4" radius="0.014140625" width="0.127" layer="21"/>
<circle x="3" y="-4" radius="0.25" width="0.127" layer="51"/>
<circle x="3" y="-4" radius="0.13" width="0.127" layer="51"/>
<circle x="3" y="-4" radius="0.014140625" width="0.127" layer="51"/>
</package>
<package name="JST_B4B-PH-K-S" urn="urn:adsk.eagle:footprint:8413934/1" library_version="40">
<pad name="1" x="3" y="-0.55" drill="0.7"/>
<pad name="2" x="1" y="-0.55" drill="0.7"/>
<pad name="3" x="-1" y="-0.55" drill="0.7"/>
<pad name="4" x="-3" y="-0.55" drill="0.7"/>
<wire x1="-4.95" y1="-2.25" x2="4.95" y2="-2.25" width="0.127" layer="21"/>
<wire x1="-4.95" y1="2.25" x2="4.95" y2="2.25" width="0.127" layer="21"/>
<wire x1="-4.95" y1="2.25" x2="-4.95" y2="-2.25" width="0.127" layer="21"/>
<wire x1="4.95" y1="2.25" x2="4.95" y2="-2.25" width="0.127" layer="21"/>
<wire x1="-4.95" y1="-2.25" x2="4.95" y2="-2.25" width="0.127" layer="51"/>
<wire x1="-4.95" y1="2.25" x2="4.95" y2="2.25" width="0.127" layer="51"/>
<wire x1="-4.95" y1="2.25" x2="-4.95" y2="-2.25" width="0.127" layer="51"/>
<wire x1="4.95" y1="2.25" x2="4.95" y2="-2.25" width="0.127" layer="51"/>
<text x="-4.55" y="-4.07" size="1.27" layer="25">&gt;NAME</text>
<text x="-12.8" y="-4.07" size="1.27" layer="27">&gt;VALUE</text>
<circle x="3" y="-2.8" radius="0.25" width="0.127" layer="21"/>
<circle x="3" y="-2.8" radius="0.13" width="0.127" layer="21"/>
<circle x="3" y="-2.8" radius="0.014140625" width="0.127" layer="21"/>
<circle x="3" y="-2.8" radius="0.25" width="0.127" layer="51"/>
<circle x="3" y="-2.8" radius="0.13" width="0.127" layer="51"/>
<circle x="3" y="-2.8" radius="0.014140625" width="0.127" layer="51"/>
</package>
<package name="JST_S4B-PH-K-S" urn="urn:adsk.eagle:footprint:8413935/1" library_version="40">
<pad name="1" x="3" y="-2.875" drill="0.7"/>
<pad name="2" x="1" y="-2.875" drill="0.7"/>
<pad name="3" x="-1" y="-2.875" drill="0.7"/>
<pad name="4" x="-3" y="-2.875" drill="0.7"/>
<text x="-3.3" y="-5.395" size="1.27" layer="25">&gt;NAME</text>
<text x="-10.55" y="-5.395" size="1.27" layer="27">&gt;VALUE</text>
<circle x="3" y="-4.375" radius="0.25" width="0.127" layer="21"/>
<circle x="3" y="-4.375" radius="0.13" width="0.127" layer="21"/>
<circle x="3" y="-4.375" radius="0.014140625" width="0.127" layer="21"/>
<circle x="3" y="-4.375" radius="0.25" width="0.127" layer="51"/>
<circle x="3" y="-4.375" radius="0.13" width="0.127" layer="51"/>
<circle x="3" y="-4.375" radius="0.014140625" width="0.127" layer="51"/>
<wire x1="-4.95" y1="-3.925" x2="4.95" y2="-3.925" width="0.127" layer="51"/>
<wire x1="-4.95" y1="3.925" x2="4.95" y2="3.925" width="0.127" layer="51"/>
<wire x1="-4.95" y1="3.925" x2="-4.95" y2="-3.925" width="0.127" layer="51"/>
<wire x1="4.95" y1="3.925" x2="4.95" y2="-3.925" width="0.127" layer="51"/>
<wire x1="-4.95" y1="-3.925" x2="4.95" y2="-3.925" width="0.127" layer="21"/>
<wire x1="-4.95" y1="3.925" x2="4.95" y2="3.925" width="0.127" layer="21"/>
<wire x1="-4.95" y1="3.925" x2="-4.95" y2="-3.925" width="0.127" layer="21"/>
<wire x1="4.95" y1="3.925" x2="4.95" y2="-3.925" width="0.127" layer="21"/>
</package>
<package name="JST_B04B-PSILE-A1_B04B-PSIY-B1_B04B-PSIR-C1" urn="urn:adsk.eagle:footprint:8413966/1" library_version="40">
<pad name="P$1" x="6" y="1" drill="1.65"/>
<pad name="P$2" x="2" y="1" drill="1.65"/>
<pad name="P$3" x="-2" y="1" drill="1.65"/>
<pad name="P$4" x="-6" y="1" drill="1.65"/>
<hole x="7.65" y="-3.5" drill="1.3"/>
<wire x1="-8.35" y1="4.55" x2="8.35" y2="4.55" width="0.127" layer="51"/>
<wire x1="-8.35" y1="-4.55" x2="8.35" y2="-4.55" width="0.127" layer="51"/>
<wire x1="-8.35" y1="4.55" x2="-8.35" y2="-4.55" width="0.127" layer="51"/>
<wire x1="8.35" y1="-3.07" x2="8.35" y2="4.55" width="0.127" layer="51"/>
<wire x1="8.35" y1="-4.55" x2="8.35" y2="-3.93" width="0.127" layer="51"/>
<wire x1="-8.35" y1="4.55" x2="8.35" y2="4.55" width="0.127" layer="21"/>
<wire x1="-8.35" y1="-4.55" x2="8.35" y2="-4.55" width="0.127" layer="21"/>
<wire x1="-8.35" y1="4.55" x2="-8.35" y2="-4.55" width="0.127" layer="21"/>
<wire x1="8.35" y1="-3.07" x2="8.35" y2="4.55" width="0.127" layer="21"/>
<wire x1="8.35" y1="-4.55" x2="8.35" y2="-3.93" width="0.127" layer="21"/>
<text x="2.45" y="-6.07" size="1.27" layer="25">&gt;NAME</text>
<text x="-4.8" y="-6.07" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="JST_B4PS-VH" urn="urn:adsk.eagle:footprint:8413982/1" library_version="40">
<pad name="P$1" x="-5.94" y="0" drill="1.65"/>
<pad name="P$2" x="-1.98" y="0" drill="1.65"/>
<pad name="P$3" x="1.98" y="0" drill="1.65"/>
<pad name="P$4" x="5.94" y="0" drill="1.65"/>
<text x="-7.96" y="3" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.96" y="3" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-7.89" y1="-7.2" x2="-7.89" y2="1.72" width="0.127" layer="51"/>
<wire x1="-7.36" y1="2.25" x2="7.92" y2="2.25" width="0.127" layer="51"/>
<wire x1="-7.89" y1="-7.2" x2="7.92" y2="-7.2" width="0.127" layer="51"/>
<wire x1="-7.89" y1="1.72" x2="-7.36" y2="2.25" width="0.127" layer="51"/>
<wire x1="7.92" y1="2.25" x2="7.92" y2="-7.2" width="0.127" layer="51"/>
<wire x1="-7.89" y1="-7.2" x2="-7.89" y2="1.72" width="0.127" layer="21"/>
<wire x1="-7.36" y1="2.25" x2="7.92" y2="2.25" width="0.127" layer="21"/>
<wire x1="-7.89" y1="-7.2" x2="7.92" y2="-7.2" width="0.127" layer="21"/>
<wire x1="-7.89" y1="1.72" x2="-7.36" y2="2.25" width="0.127" layer="21"/>
<wire x1="7.92" y1="2.25" x2="7.92" y2="-7.2" width="0.127" layer="21"/>
</package>
<package name="JST_B4P-VH" urn="urn:adsk.eagle:footprint:8413983/1" library_version="40">
<pad name="P$1" x="-5.94" y="0" drill="1.65"/>
<pad name="P$2" x="-1.98" y="0" drill="1.65"/>
<pad name="P$3" x="1.98" y="0" drill="1.65"/>
<pad name="P$4" x="5.94" y="0" drill="1.65"/>
<text x="-7.96" y="-7" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.96" y="-7" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-7.91" y1="-5.25" x2="-7.91" y2="2.5" width="0.127" layer="51"/>
<wire x1="-7.91" y1="2.5" x2="-7.91" y2="3.25" width="0.127" layer="51"/>
<wire x1="7.89" y1="3.25" x2="7.89" y2="2.5" width="0.127" layer="51"/>
<wire x1="7.89" y1="2.5" x2="7.9" y2="-5.25" width="0.127" layer="51"/>
<wire x1="-7.91" y1="3.25" x2="7.89" y2="3.25" width="0.127" layer="51"/>
<wire x1="-7.91" y1="-5.25" x2="7.9" y2="-5.25" width="0.127" layer="51"/>
<wire x1="-7.91" y1="2.5" x2="7.89" y2="2.5" width="0.127" layer="51"/>
<wire x1="-7.91" y1="-5.25" x2="-7.91" y2="2.5" width="0.127" layer="21"/>
<wire x1="-7.91" y1="2.5" x2="-7.91" y2="3.25" width="0.127" layer="21"/>
<wire x1="7.89" y1="3.25" x2="7.89" y2="2.5" width="0.127" layer="21"/>
<wire x1="7.89" y1="2.5" x2="7.9" y2="-5.25" width="0.127" layer="21"/>
<wire x1="-7.91" y1="3.25" x2="7.89" y2="3.25" width="0.127" layer="21"/>
<wire x1="-7.91" y1="-5.25" x2="7.9" y2="-5.25" width="0.127" layer="21"/>
<wire x1="-7.91" y1="2.5" x2="7.89" y2="2.5" width="0.127" layer="21"/>
</package>
<package name="JST_B4P-VH-FB-B" urn="urn:adsk.eagle:footprint:8413984/1" library_version="40">
<pad name="P$1" x="-5.94" y="0" drill="1.65"/>
<pad name="P$2" x="-1.98" y="0" drill="1.65"/>
<pad name="P$3" x="1.98" y="0" drill="1.65"/>
<pad name="P$4" x="5.94" y="0" drill="1.65"/>
<hole x="-7.44" y="-3.4" drill="1.4"/>
<wire x1="-8.86" y1="4.85" x2="8.86" y2="4.85" width="0.127" layer="21"/>
<wire x1="-8.86" y1="-4.85" x2="8.86" y2="-4.85" width="0.127" layer="21"/>
<wire x1="-8.86" y1="4.85" x2="-8.86" y2="-4.85" width="0.127" layer="21"/>
<wire x1="8.86" y1="4.85" x2="8.86" y2="-4.85" width="0.127" layer="21"/>
<wire x1="-8.86" y1="4.85" x2="8.86" y2="4.85" width="0.127" layer="51"/>
<wire x1="-8.86" y1="-4.85" x2="8.86" y2="-4.85" width="0.127" layer="51"/>
<wire x1="-8.86" y1="4.85" x2="-8.86" y2="-4.85" width="0.127" layer="51"/>
<wire x1="8.86" y1="4.85" x2="8.86" y2="-4.85" width="0.127" layer="51"/>
<text x="-8.96" y="5" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.96" y="5" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="JST_B04B-ZESK-1D" urn="urn:adsk.eagle:footprint:8414007/1" library_version="40">
<pad name="P$1" x="2.25" y="1.98" drill="0.7" rot="R180"/>
<pad name="P$2" x="0.75" y="-0.02" drill="0.7" rot="R180"/>
<pad name="P$3" x="-0.75" y="1.98" drill="0.7" rot="R180"/>
<pad name="P$4" x="-2.25" y="-0.02" drill="0.7" rot="R180"/>
<hole x="3.9" y="-1.82" drill="0.8"/>
<wire x1="-4.505" y1="3.115" x2="-4.505" y2="-3.105" width="0.127" layer="51"/>
<wire x1="4.505" y1="3.115" x2="4.505" y2="-3.105" width="0.127" layer="51"/>
<wire x1="-4.505" y1="3.115" x2="4.505" y2="3.115" width="0.127" layer="51"/>
<wire x1="-4.505" y1="-3.105" x2="4.505" y2="-3.105" width="0.127" layer="51"/>
<wire x1="-4.505" y1="3.115" x2="-4.505" y2="-3.105" width="0.127" layer="21"/>
<wire x1="4.505" y1="3.115" x2="4.505" y2="-3.105" width="0.127" layer="21"/>
<wire x1="-4.505" y1="3.115" x2="4.505" y2="3.115" width="0.127" layer="21"/>
<wire x1="-4.505" y1="-3.105" x2="4.505" y2="-3.105" width="0.127" layer="21"/>
<text x="-1.5" y="3.48" size="1.27" layer="25">&gt;NAME</text>
<text x="-9.75" y="3.48" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="JST_S04B-ZESK-2D" urn="urn:adsk.eagle:footprint:8414008/1" library_version="40">
<pad name="P$1" x="2.25" y="-7.5" drill="0.7"/>
<pad name="P$2" x="0.75" y="-3.8" drill="0.7"/>
<pad name="P$3" x="-0.75" y="-7.5" drill="0.7"/>
<pad name="P$4" x="-2.25" y="-3.8" drill="0.7"/>
<hole x="3.8" y="-5.65" drill="1.1"/>
<hole x="-3.8" y="-5.65" drill="1.1"/>
<wire x1="4.5" y1="7.22" x2="4.5" y2="6.47" width="0.127" layer="21"/>
<wire x1="4.5" y1="6.47" x2="4.5" y2="-8.38" width="0.127" layer="21"/>
<wire x1="-4.5" y1="7.22" x2="-4.5" y2="6.47" width="0.127" layer="21"/>
<wire x1="-4.5" y1="6.47" x2="-4.5" y2="-8.38" width="0.127" layer="21"/>
<wire x1="-4.5" y1="-8.38" x2="4.5" y2="-8.38" width="0.127" layer="21"/>
<wire x1="-4.5" y1="7.22" x2="4.5" y2="7.22" width="0.127" layer="21"/>
<wire x1="4.5" y1="7.22" x2="4.5" y2="6.47" width="0.127" layer="51"/>
<wire x1="4.5" y1="6.47" x2="4.5" y2="-8.38" width="0.127" layer="51"/>
<wire x1="-4.5" y1="7.22" x2="-4.5" y2="6.47" width="0.127" layer="51"/>
<wire x1="-4.5" y1="6.47" x2="-4.5" y2="-8.38" width="0.127" layer="51"/>
<wire x1="-4.5" y1="-8.38" x2="4.5" y2="-8.38" width="0.127" layer="51"/>
<wire x1="-4.5" y1="7.22" x2="4.5" y2="7.22" width="0.127" layer="51"/>
<wire x1="-4.5" y1="6.47" x2="4.5" y2="6.47" width="0.127" layer="21"/>
<wire x1="-4.5" y1="6.47" x2="4.5" y2="6.47" width="0.127" layer="51"/>
<text x="-1.2" y="-10" size="1.27" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="-9.45" y="-10" size="1.27" layer="27" font="vector" ratio="15">&gt;VALUE</text>
</package>
<package name="HDRRA4W64P254_1X4_1016X254X254B" urn="urn:adsk.eagle:footprint:15597976/1" library_version="40">
<description>Single-row, 4-pin Pin Header (Male) Right Angle, 2.54 mm (0.10 in) col pitch, 5.84 mm mating length, 10.16 X 2.54 X 2.54 mm body
&lt;p&gt;Single-row (1X4), 4-pin Pin Header (Male) Right Angle package with 2.54 mm (0.10 in) col pitch, 0.64 mm lead width, 2.29 mm tail length and 5.84 mm mating length with body size 10.16 X 2.54 X 2.54 mm, pin pattern - clockwise from top left&lt;/p&gt;</description>
<circle x="-1.3565" y="0" radius="0.25" width="0" layer="21"/>
<wire x1="8.89" y1="-4.06" x2="-1.27" y2="-4.06" width="0.12" layer="21"/>
<wire x1="-1.27" y1="-4.06" x2="-1.27" y2="-1.52" width="0.12" layer="21"/>
<wire x1="-1.27" y1="-1.52" x2="8.89" y2="-1.52" width="0.12" layer="21"/>
<wire x1="8.89" y1="-1.52" x2="8.89" y2="-4.06" width="0.12" layer="21"/>
<wire x1="0" y1="-4.12" x2="0" y2="-9.96" width="0.12" layer="21"/>
<wire x1="2.54" y1="-4.12" x2="2.54" y2="-9.96" width="0.12" layer="21"/>
<wire x1="5.08" y1="-4.12" x2="5.08" y2="-9.96" width="0.12" layer="21"/>
<wire x1="7.62" y1="-4.12" x2="7.62" y2="-9.96" width="0.12" layer="21"/>
<wire x1="8.89" y1="-4.06" x2="-1.27" y2="-4.06" width="0.12" layer="51"/>
<wire x1="-1.27" y1="-4.06" x2="-1.27" y2="-1.52" width="0.12" layer="51"/>
<wire x1="-1.27" y1="-1.52" x2="8.89" y2="-1.52" width="0.12" layer="51"/>
<wire x1="8.89" y1="-1.52" x2="8.89" y2="-4.06" width="0.12" layer="51"/>
<pad name="1" x="0" y="0" drill="1.1051" diameter="1.7051"/>
<pad name="2" x="2.54" y="0" drill="1.1051" diameter="1.7051"/>
<pad name="3" x="5.08" y="0" drill="1.1051" diameter="1.7051"/>
<pad name="4" x="7.62" y="0" drill="1.1051" diameter="1.7051"/>
<text x="0" y="1.4875" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-10.595" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="HDRV4W64P254_1X4_1016X508X1829B" urn="urn:adsk.eagle:footprint:15737104/1" library_version="40">
<description>Single-row, 4-pin Pin Header (Male) Straight, 2.54 mm (0.10 in) col pitch, 15.75 mm mating length, 10.16 X 5.08 X 18.29 mm body
&lt;p&gt;Single-row (1X4), 4-pin Pin Header (Male) Straight package with 2.54 mm (0.10 in) col pitch, 0.64 mm lead width, 2.79 mm tail length and 15.75 mm mating length with overall size 10.16 X 5.08 X 18.29 mm, pin pattern - clockwise from top left&lt;/p&gt;</description>
<circle x="0" y="3.044" radius="0.25" width="0" layer="21"/>
<wire x1="8.89" y1="-2.54" x2="-1.27" y2="-2.54" width="0.12" layer="21"/>
<wire x1="-1.27" y1="-2.54" x2="-1.27" y2="2.54" width="0.12" layer="21"/>
<wire x1="-1.27" y1="2.54" x2="8.89" y2="2.54" width="0.12" layer="21"/>
<wire x1="8.89" y1="2.54" x2="8.89" y2="-2.54" width="0.12" layer="21"/>
<wire x1="8.89" y1="-2.54" x2="-1.27" y2="-2.54" width="0.12" layer="51"/>
<wire x1="-1.27" y1="-2.54" x2="-1.27" y2="2.54" width="0.12" layer="51"/>
<wire x1="-1.27" y1="2.54" x2="8.89" y2="2.54" width="0.12" layer="51"/>
<wire x1="8.89" y1="2.54" x2="8.89" y2="-2.54" width="0.12" layer="51"/>
<pad name="1" x="0" y="0" drill="1.1051" diameter="1.7051"/>
<pad name="2" x="2.54" y="0" drill="1.1051" diameter="1.7051"/>
<pad name="3" x="5.08" y="0" drill="1.1051" diameter="1.7051"/>
<pad name="4" x="7.62" y="0" drill="1.1051" diameter="1.7051"/>
<text x="0" y="3.929" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-3.175" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="RESC1608X55" urn="urn:adsk.eagle:package:3811166/1" type="model" library_version="38">
<description>CHIP, 1.6 X 0.85 X 0.55 mm body
&lt;p&gt;CHIP package with body size 1.6 X 0.85 X 0.55 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESC1608X55"/>
</packageinstances>
</package3d>
<package3d name="RESC2012X70" urn="urn:adsk.eagle:package:3811169/1" type="model" library_version="38">
<description>CHIP, 2 X 1.25 X 0.7 mm body
&lt;p&gt;CHIP package with body size 2 X 1.25 X 0.7 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESC2012X70"/>
</packageinstances>
</package3d>
<package3d name="CAPC2012X127" urn="urn:adsk.eagle:package:3793139/1" type="model" library_version="38">
<description>CHIP, 2.01 X 1.25 X 1.27 mm body
&lt;p&gt;CHIP package with body size 2.01 X 1.25 X 1.27 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC2012X127"/>
</packageinstances>
</package3d>
<package3d name="CAPC1608X55" urn="urn:adsk.eagle:package:3793126/2" type="model" library_version="27">
<description>CHIP, 1.6 X 0.8 X 0.55 mm body
&lt;p&gt;CHIP package with body size 1.6 X 0.8 X 0.55 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC1608X55"/>
</packageinstances>
</package3d>
<package3d name="RESMELF5875" urn="urn:adsk.eagle:package:3890106/3" type="model" library_version="27">
<description>MELF, 5.865 mm length, 7.5 mm diameter
&lt;p&gt;MELF Resistor package with 5.865 mm length and 7.5 mm diameter&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="GDTMELF5875"/>
</packageinstances>
</package3d>
<package3d name="TRANSITION_CLIP_.141" urn="urn:adsk.eagle:package:8414403/4" type="model" library_version="27" library_locally_modified="yes">
<packageinstances>
<packageinstance name="TRANSITION_CLIP_.141"/>
</packageinstances>
</package3d>
<package3d name="RESC1005X40" urn="urn:adsk.eagle:package:3811154/1" type="model" library_version="38">
<description>CHIP, 1 X 0.5 X 0.4 mm body
&lt;p&gt;CHIP package with body size 1 X 0.5 X 0.4 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESC1005X40"/>
</packageinstances>
</package3d>
<package3d name="CAPC1005X60" urn="urn:adsk.eagle:package:3793108/1" type="model" library_version="38">
<description>CHIP, 1.025 X 0.525 X 0.6 mm body
&lt;p&gt;CHIP package with body size 1.025 X 0.525 X 0.6 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC1005X60"/>
</packageinstances>
</package3d>
<package3d name="DO214AB" urn="urn:adsk.eagle:package:15618120/3" type="model" library_version="34">
<description>&lt;b&gt;DIODE&lt;/b&gt;</description>
<packageinstances>
<packageinstance name="DO214AB_SMC"/>
</packageinstances>
</package3d>
<package3d name="DO214AC_SMA" urn="urn:adsk.eagle:package:15671034/2" type="model" library_version="34">
<packageinstances>
<packageinstance name="DO214AC_SMA"/>
</packageinstances>
</package3d>
<package3d name="TO457P1000X238-3" urn="urn:adsk.eagle:package:3920001/2" type="model" library_version="38">
<description>3-TO, DPAK, 4.57 mm pitch, 10 mm span, 6.6 X 6.1 X 2.38 mm body
&lt;p&gt;3-pin TO, DPAK package with 4.57 mm pitch, 10 mm span with body size 6.6 X 6.1 X 2.38 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="TO457P1000X238-3"/>
</packageinstances>
</package3d>
<package3d name="QFN50P300X300X100-16" urn="urn:adsk.eagle:package:3986130/2" type="model" library_version="38">
<description>16-QFN, 0.5 mm pitch, 3 X 3 X 1 mm body
&lt;p&gt;16-pin QFN package with 0.5 mm pitch with body size 3 X 3 X 1 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="QFN50P300X300X100-17"/>
</packageinstances>
</package3d>
<package3d name="XTAL1140X470X330" urn="urn:adsk.eagle:package:3837785/1" type="model" library_version="38">
<description>CRYSTAL, 11.4 X 4.7 X 3.3 mm body
&lt;p&gt;CRYSTAL package with body size 11.4 X 4.7 X 3.3 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="XTAL1140X470X330"/>
</packageinstances>
</package3d>
<package3d name="POWERPAK_SO-8" urn="urn:adsk.eagle:package:8414082/1" type="box" library_version="30">
<packageinstances>
<packageinstance name="POWERPAK_SO-8"/>
</packageinstances>
</package3d>
<package3d name="SOIC127P540X170-9T271X340N" urn="urn:adsk.eagle:package:15672442/2" type="model" library_version="34">
<description>8-SOIC, 1.27 mm pitch, 5.40 mm span, 4.90 X 2.95 X 1.70 mm body, 3.40 X 2.71 mm thermal pad
&lt;p&gt;8-pin SOIC package with 1.27 mm pitch, 5.40 mm span with body size 4.90 X 2.95 X 1.70 mm and thermal pad size 3.40 X 2.71 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="SOIC127P540X170-9T271X340N"/>
</packageinstances>
</package3d>
<package3d name="SOIC127P600X175-8" urn="urn:adsk.eagle:package:3833977/2" type="model" library_version="38">
<description>8-SOIC, 1.27 mm pitch, 6 mm span, 4.9 X 3.9 X 1.75 mm body
&lt;p&gt;8-pin SOIC package with 1.27 mm pitch, 6 mm span with body size 4.9 X 3.9 X 1.75 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="SOIC127P600X175-8"/>
</packageinstances>
</package3d>
<package3d name="CIR_PAD_1MM" urn="urn:adsk.eagle:package:8414067/2" type="empty" library_version="30" library_locally_modified="yes">
<packageinstances>
<packageinstance name="CIR_PAD_1MM"/>
</packageinstances>
</package3d>
<package3d name="TP_1.5MM" urn="urn:adsk.eagle:package:8414069/2" type="empty" library_version="30" library_locally_modified="yes">
<packageinstances>
<packageinstance name="TP_1.5MM"/>
</packageinstances>
</package3d>
<package3d name="TP_1.5MM_PAD" urn="urn:adsk.eagle:package:8414070/2" type="empty" library_version="30" library_locally_modified="yes">
<packageinstances>
<packageinstance name="TP_1.5MM_PAD"/>
</packageinstances>
</package3d>
<package3d name="TP_1.7MM" urn="urn:adsk.eagle:package:8414071/2" type="empty" library_version="30" library_locally_modified="yes">
<packageinstances>
<packageinstance name="TP_1.7MM"/>
</packageinstances>
</package3d>
<package3d name="C120H40" urn="urn:adsk.eagle:package:8414063/2" type="empty" library_version="30" library_locally_modified="yes">
<packageinstances>
<packageinstance name="C120H40"/>
</packageinstances>
</package3d>
<package3d name="C131H51" urn="urn:adsk.eagle:package:8414064/2" type="empty" library_version="30" library_locally_modified="yes">
<packageinstances>
<packageinstance name="C131H51"/>
</packageinstances>
</package3d>
<package3d name="C406H326" urn="urn:adsk.eagle:package:8414065/2" type="empty" library_version="30" library_locally_modified="yes">
<packageinstances>
<packageinstance name="C406H326"/>
</packageinstances>
</package3d>
<package3d name="C491H411" urn="urn:adsk.eagle:package:8414066/2" type="empty" library_version="30" library_locally_modified="yes">
<packageinstances>
<packageinstance name="C491H411"/>
</packageinstances>
</package3d>
<package3d name="CIR_PAD_2MM" urn="urn:adsk.eagle:package:8414068/2" type="empty" library_version="30" library_locally_modified="yes">
<packageinstances>
<packageinstance name="CIR_PAD_2MM"/>
</packageinstances>
</package3d>
<package3d name="BEADC1608X55" urn="urn:adsk.eagle:package:3887236/1" type="model" library_version="38">
<description>CHIP, 1.6 X 0.85 X 0.55 mm body
&lt;p&gt;CHIP package with body size 1.6 X 0.85 X 0.55 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="BEADC1608X55"/>
</packageinstances>
</package3d>
<package3d name="INDM5050X430" urn="urn:adsk.eagle:package:3890169/1" type="model" library_version="38">
<description>MOLDED BODY, 5 X 5 X 4.3 mm body
&lt;p&gt;MOLDED BODY package with body size 5 X 5 X 4.3 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="INDM5050X430"/>
</packageinstances>
</package3d>
<package3d name="RADIAL_THT" urn="urn:adsk.eagle:package:8414048/1" type="box" library_version="30" library_locally_modified="yes">
<packageinstances>
<packageinstance name="RADIAL_THT"/>
</packageinstances>
</package3d>
<package3d name="DIOM7959X262N" urn="urn:adsk.eagle:package:15726434/1" type="model" library_version="38">
<description>Molded Body, 7.94 X 5.91 X 2.62 mm body
&lt;p&gt;Molded Body package with body size 7.94 X 5.91 X 2.62 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="DIOM7959X262N"/>
</packageinstances>
</package3d>
<package3d name="DIOM7957X265N" urn="urn:adsk.eagle:package:15725844/1" type="model" library_version="38">
<description>Molded Body, 7.95 X 5.75 X 2.65 mm body
&lt;p&gt;Molded Body package with body size 7.95 X 5.75 X 2.65 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="DIOM7957X265N"/>
</packageinstances>
</package3d>
<package3d name="SOP50P490X110-10" urn="urn:adsk.eagle:package:3921298/1" type="model" library_version="38">
<description>10-SOP, 0.5 mm pitch, 4.9 mm span, 3 X 3 X 1.1 mm body
&lt;p&gt;10-pin SOP package with 0.5 mm pitch, 4.9 mm span with body size 3 X 3 X 1.1 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="SOP50P490X110-10"/>
</packageinstances>
</package3d>
<package3d name="RESC0603X30" urn="urn:adsk.eagle:package:3811151/1" type="model" library_version="38">
<description>CHIP, 0.6 X 0.3 X 0.3 mm body
&lt;p&gt;CHIP package with body size 0.6 X 0.3 X 0.3 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESC0603X30"/>
</packageinstances>
</package3d>
<package3d name="RESC3115X70" urn="urn:adsk.eagle:package:3811173/1" type="model" library_version="38">
<description>CHIP, 3.125 X 1.55 X 0.7 mm body
&lt;p&gt;CHIP package with body size 3.125 X 1.55 X 0.7 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESC3115X70"/>
</packageinstances>
</package3d>
<package3d name="RESC3225X70" urn="urn:adsk.eagle:package:3811180/1" type="model" library_version="38">
<description>CHIP, 3.2 X 2.5 X 0.7 mm body
&lt;p&gt;CHIP package with body size 3.2 X 2.5 X 0.7 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESC3225X70"/>
</packageinstances>
</package3d>
<package3d name="RESC4532X70" urn="urn:adsk.eagle:package:3811188/1" type="model" library_version="38">
<description>CHIP, 4.5 X 3.2 X 0.7 mm body
&lt;p&gt;CHIP package with body size 4.5 X 3.2 X 0.7 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESC4532X70"/>
</packageinstances>
</package3d>
<package3d name="RESC5025X70" urn="urn:adsk.eagle:package:3811197/1" type="model" library_version="38">
<description>CHIP, 5 X 2.5 X 0.7 mm body
&lt;p&gt;CHIP package with body size 5 X 2.5 X 0.7 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESC5025X70"/>
</packageinstances>
</package3d>
<package3d name="RESC5750X229" urn="urn:adsk.eagle:package:3811212/1" type="model" library_version="38">
<description>CHIP, 5.7 X 5 X 2.29 mm body
&lt;p&gt;CHIP package with body size 5.7 X 5 X 2.29 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESC5750X229"/>
</packageinstances>
</package3d>
<package3d name="RESC6432X70" urn="urn:adsk.eagle:package:3811284/1" type="model" library_version="38">
<description>CHIP, 6.4 X 3.2 X 0.7 mm body
&lt;p&gt;CHIP package with body size 6.4 X 3.2 X 0.7 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESC6432X70"/>
</packageinstances>
</package3d>
<package3d name="RESC2037X52" urn="urn:adsk.eagle:package:3950966/1" type="model" library_version="38">
<description>CHIP, 2 X 3.75 X 0.52 mm body
&lt;p&gt;CHIP package with body size 2 X 3.75 X 0.52 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESC2037X52"/>
</packageinstances>
</package3d>
<package3d name="CAPC1220X107" urn="urn:adsk.eagle:package:3793115/1" type="model" library_version="38">
<description>CHIP, 1.25 X 2 X 1.07 mm body
&lt;p&gt;CHIP package with body size 1.25 X 2 X 1.07 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC1220X107"/>
</packageinstances>
</package3d>
<package3d name="CAPC1632X168" urn="urn:adsk.eagle:package:3793131/1" type="model" library_version="38">
<description>CHIP, 1.6 X 3.2 X 1.68 mm body
&lt;p&gt;CHIP package with body size 1.6 X 3.2 X 1.68 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC1632X168"/>
</packageinstances>
</package3d>
<package3d name="CAPC3215X168" urn="urn:adsk.eagle:package:3793145/1" type="model" library_version="38">
<description>CHIP, 3.2 X 1.5 X 1.68 mm body
&lt;p&gt;CHIP package with body size 3.2 X 1.5 X 1.68 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC3215X168"/>
</packageinstances>
</package3d>
<package3d name="CAPC3225X168" urn="urn:adsk.eagle:package:3793149/1" type="model" library_version="38">
<description>CHIP, 3.2 X 2.5 X 1.68 mm body
&lt;p&gt;CHIP package with body size 3.2 X 2.5 X 1.68 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC3225X168"/>
</packageinstances>
</package3d>
<package3d name="CAPC4520X168" urn="urn:adsk.eagle:package:3793154/1" type="model" library_version="38">
<description>CHIP, 4.5 X 2.03 X 1.68 mm body
&lt;p&gt;CHIP package with body size 4.5 X 2.03 X 1.68 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC4520X168"/>
</packageinstances>
</package3d>
<package3d name="CAPC4532X218" urn="urn:adsk.eagle:package:3793159/1" type="model" library_version="38">
<description>CHIP, 4.5 X 3.2 X 2.18 mm body
&lt;p&gt;CHIP package with body size 4.5 X 3.2 X 2.18 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC4532X218"/>
</packageinstances>
</package3d>
<package3d name="CAPC4564X218" urn="urn:adsk.eagle:package:3793166/1" type="model" library_version="38">
<description>CHIP, 4.5 X 6.4 X 2.18 mm body
&lt;p&gt;CHIP package with body size 4.5 X 6.4 X 2.18 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC4564X218"/>
</packageinstances>
</package3d>
<package3d name="CAPC5650X218" urn="urn:adsk.eagle:package:3793171/1" type="model" library_version="38">
<description>CHIP, 5.64 X 5.08 X 2.18 mm body
&lt;p&gt;CHIP package with body size 5.64 X 5.08 X 2.18 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC5650X218"/>
</packageinstances>
</package3d>
<package3d name="CAPC5563X218" urn="urn:adsk.eagle:package:3793174/1" type="model" library_version="38">
<description>CHIP, 5.59 X 6.35 X 2.18 mm body
&lt;p&gt;CHIP package with body size 5.59 X 6.35 X 2.18 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC5563X218"/>
</packageinstances>
</package3d>
<package3d name="CAPC9198X218" urn="urn:adsk.eagle:package:3793178/1" type="model" library_version="38">
<description>CHIP, 9.14 X 9.82 X 2.18 mm body
&lt;p&gt;CHIP package with body size 9.14 X 9.82 X 2.18 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC9198X218"/>
</packageinstances>
</package3d>
<package3d name="RESMELF4450" urn="urn:adsk.eagle:package:3890127/2" type="model" library_version="38">
<description>MELF, 4.4 mm length, 5 mm diameter
&lt;p&gt;MELF Resistor package with 4.4 mm length and 5 mm diameter&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="GDTMELF4450"/>
</packageinstances>
</package3d>
<package3d name="SOT230P700X180-4" urn="urn:adsk.eagle:package:3834044/1" type="model" library_version="38">
<description>4-SOT223, 2.3 mm pitch, 7 mm span, 6.5 X 3.5 X 1.8 mm body
&lt;p&gt;4-pin SOT223 package with 2.3 mm pitch, 7 mm span with body size 6.5 X 3.5 X 1.8 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="SOT230P700X180-4"/>
</packageinstances>
</package3d>
<package3d name="SOT95P280X135-3" urn="urn:adsk.eagle:package:3809961/2" type="model" library_version="38">
<description>3-SOT23, 0.95 mm pitch, 2.8 mm span, 2.9 X 1.6 X 1.35 mm body
&lt;p&gt;3-pin SOT23 package with 0.95 mm pitch, 2.8 mm span with body size 2.9 X 1.6 X 1.35 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="SOT95P280X135-3"/>
</packageinstances>
</package3d>
<package3d name="DIOM5226X292" urn="urn:adsk.eagle:package:3793189/2" type="model" library_version="38">
<description>MOLDED BODY, 5.265 X 2.6 X 2.92 mm body
&lt;p&gt;MOLDED BODY package with body size 5.265 X 2.6 X 2.92 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="DIOM5226X292"/>
</packageinstances>
</package3d>
<package3d name="DIOM5436X265" urn="urn:adsk.eagle:package:3793201/2" type="model" library_version="38">
<description>MOLDED BODY, 5.4 X 3.6 X 2.65 mm body
&lt;p&gt;MOLDED BODY package with body size 5.4 X 3.6 X 2.65 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="DIOM5436X265"/>
</packageinstances>
</package3d>
<package3d name="DIOM7959X265" urn="urn:adsk.eagle:package:3793207/2" type="model" library_version="38">
<description>MOLDED BODY, 7.94 X 5.95 X 2.65 mm body
&lt;p&gt;MOLDED BODY package with body size 7.94 X 5.95 X 2.65 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="DIOM7959X265"/>
</packageinstances>
</package3d>
<package3d name="SOD2512X100" urn="urn:adsk.eagle:package:3793216/2" type="model" library_version="38">
<description>SOD, 2.5 mm span, 1.7 X 1.25 X 1 mm body
&lt;p&gt;SOD package with 2.5 mm span with body size 1.7 X 1.25 X 1 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="SOD2512X100"/>
</packageinstances>
</package3d>
<package3d name="SOD3716X135" urn="urn:adsk.eagle:package:3793231/2" type="model" library_version="38">
<description>SOD, 3.7 mm span, 2.7 X 1.6 X 1.35 mm body
&lt;p&gt;SOD package with 3.7 mm span with body size 2.7 X 1.6 X 1.35 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="SOD3716X135"/>
</packageinstances>
</package3d>
<package3d name="SOT65P210X110-3" urn="urn:adsk.eagle:package:3809951/2" type="model" library_version="38">
<description>3-SOT23, 0.65 mm pitch, 2.1 mm span, 2 X 1.25 X 1.1 mm body
&lt;p&gt;3-pin SOT23 package with 0.65 mm pitch, 2.1 mm span with body size 2 X 1.25 X 1.1 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="SOT65P210X110-3"/>
</packageinstances>
</package3d>
<package3d name="SODFL4725X110" urn="urn:adsk.eagle:package:3921253/2" type="model" library_version="38">
<description>SODFL, 4.7 mm span, 3.8 X 2.5 X 1.1 mm body
&lt;p&gt;SODFL package with 4.7 mm span with body size 3.8 X 2.5 X 1.1 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="SODFL4725X110"/>
</packageinstances>
</package3d>
<package3d name="DIOC0502X50" urn="urn:adsk.eagle:package:3948086/2" type="model" library_version="38">
<description>CHIP, 0.5 X 0.25 X 0.5 mm body
&lt;p&gt;CHIP package with body size 0.5 X 0.25 X 0.5 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="DIOC0502X50"/>
</packageinstances>
</package3d>
<package3d name="DIOC1005X84" urn="urn:adsk.eagle:package:3948100/2" type="model" library_version="38">
<description>CHIP, 1.07 X 0.56 X 0.84 mm body
&lt;p&gt;CHIP package with body size 1.07 X 0.56 X 0.84 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="DIOC1005X84"/>
</packageinstances>
</package3d>
<package3d name="DIOC1206X63" urn="urn:adsk.eagle:package:3948141/2" type="model" library_version="38">
<description>CHIP, 1.27 X 0.6 X 0.63 mm body
&lt;p&gt;CHIP package with body size 1.27 X 0.6 X 0.63 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="DIOC1206X63"/>
</packageinstances>
</package3d>
<package3d name="DIOC1608X84" urn="urn:adsk.eagle:package:3948147/2" type="model" library_version="38">
<description>CHIP, 1.63 X 0.81 X 0.84 mm body
&lt;p&gt;CHIP package with body size 1.63 X 0.81 X 0.84 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="DIOC1608X84"/>
</packageinstances>
</package3d>
<package3d name="DIOC2012X70" urn="urn:adsk.eagle:package:3948150/2" type="model" library_version="38">
<description>CHIP, 2 X 1.25 X 0.7 mm body
&lt;p&gt;CHIP package with body size 2 X 1.25 X 0.7 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="DIOC2012X70"/>
</packageinstances>
</package3d>
<package3d name="DIOC3216X84" urn="urn:adsk.eagle:package:3948168/2" type="model" library_version="38">
<description>CHIP, 3.2 X 1.6 X 0.84 mm body
&lt;p&gt;CHIP package with body size 3.2 X 1.6 X 0.84 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="DIOC3216X84"/>
</packageinstances>
</package3d>
<package3d name="DIOC5324X84" urn="urn:adsk.eagle:package:3948171/2" type="model" library_version="38">
<description>CHIP, 5.31 X 2.49 X 0.84 mm body
&lt;p&gt;CHIP package with body size 5.31 X 2.49 X 0.84 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="DIOC5324X84"/>
</packageinstances>
</package3d>
<package3d name="SOIC127P600X170-9T271X340N" urn="urn:adsk.eagle:package:15661897/1" type="model" library_version="38">
<description>8-SOIC, 1.27 mm pitch, 6.00 mm span, 4.90 X 3.90 X 1.70 mm body, 3.40 X 2.71 mm thermal pad
&lt;p&gt;8-pin SOIC package with 1.27 mm pitch, 6.00 mm span with body size 4.90 X 3.90 X 1.70 mm and thermal pad size 3.40 X 2.71 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="SOIC127P600X170-9T271X340N"/>
</packageinstances>
</package3d>
<package3d name="BEADC0603X30" urn="urn:adsk.eagle:package:3853343/1" type="model" library_version="38">
<description>CHIP, 0.6 X 0.3 X 0.3 mm body
&lt;p&gt;CHIP package with body size 0.6 X 0.3 X 0.3 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="BEADC0603X30"/>
</packageinstances>
</package3d>
<package3d name="BEADC1005X40" urn="urn:adsk.eagle:package:3853346/1" type="model" library_version="38">
<description>CHIP, 1 X 0.5 X 0.4 mm body
&lt;p&gt;CHIP package with body size 1 X 0.5 X 0.4 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="BEADC1005X40"/>
</packageinstances>
</package3d>
<package3d name="BEADC2012X70" urn="urn:adsk.eagle:package:3887243/1" type="model" library_version="38">
<description>CHIP, 2 X 1.25 X 0.7 mm body
&lt;p&gt;CHIP package with body size 2 X 1.25 X 0.7 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="BEADC2012X70"/>
</packageinstances>
</package3d>
<package3d name="BEADC3115X70" urn="urn:adsk.eagle:package:3887314/1" type="model" library_version="38">
<description>CHIP, 3.125 X 1.55 X 0.7 mm body
&lt;p&gt;CHIP package with body size 3.125 X 1.55 X 0.7 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="BEADC3115X70"/>
</packageinstances>
</package3d>
<package3d name="BEADC3225X70" urn="urn:adsk.eagle:package:3887427/1" type="model" library_version="38">
<description>CHIP, 3.2 X 2.5 X 0.7 mm body
&lt;p&gt;CHIP package with body size 3.2 X 2.5 X 0.7 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="BEADC3225X70"/>
</packageinstances>
</package3d>
<package3d name="BEADC4532X70" urn="urn:adsk.eagle:package:3887433/1" type="model" library_version="38">
<description>CHIP, 4.5 X 3.2 X 0.7 mm body
&lt;p&gt;CHIP package with body size 4.5 X 3.2 X 0.7 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="BEADC4532X70"/>
</packageinstances>
</package3d>
<package3d name="BEADC5025X70" urn="urn:adsk.eagle:package:3887437/1" type="model" library_version="38">
<description>CHIP, 5 X 2.5 X 0.7 mm body
&lt;p&gt;CHIP package with body size 5 X 2.5 X 0.7 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="BEADC5025X70"/>
</packageinstances>
</package3d>
<package3d name="BEADC5750X229" urn="urn:adsk.eagle:package:3889733/1" type="model" library_version="38">
<description>CHIP, 5.7 X 5 X 2.29 mm body
&lt;p&gt;CHIP package with body size 5.7 X 5 X 2.29 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="BEADC5750X229"/>
</packageinstances>
</package3d>
<package3d name="BEADC6432X70" urn="urn:adsk.eagle:package:3889736/1" type="model" library_version="38">
<description>CHIP, 6.4 X 3.2 X 0.7 mm body
&lt;p&gt;CHIP package with body size 6.4 X 3.2 X 0.7 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="BEADC6432X70"/>
</packageinstances>
</package3d>
<package3d name="INDC1005X60" urn="urn:adsk.eagle:package:3810108/1" type="model" library_version="38">
<description>CHIP, 1 X 0.5 X 0.6 mm body
&lt;p&gt;CHIP package with body size 1 X 0.5 X 0.6 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="INDC1005X60"/>
</packageinstances>
</package3d>
<package3d name="INDC1608X95" urn="urn:adsk.eagle:package:3810118/1" type="model" library_version="38">
<description>CHIP, 1.6 X 0.8 X 0.95 mm body
&lt;p&gt;CHIP package with body size 1.6 X 0.8 X 0.95 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="INDC1608X95"/>
</packageinstances>
</package3d>
<package3d name="INDC2520X220" urn="urn:adsk.eagle:package:3810123/1" type="model" library_version="38">
<description>CHIP, 2.5 X 2 X 2.2 mm body
&lt;p&gt;CHIP package with body size 2.5 X 2 X 2.2 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="INDC2520X220"/>
</packageinstances>
</package3d>
<package3d name="INDC3225X135" urn="urn:adsk.eagle:package:3810128/1" type="model" library_version="38">
<description>CHIP, 3.2 X 2.5 X 1.35 mm body
&lt;p&gt;CHIP package with body size 3.2 X 2.5 X 1.35 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="INDC3225X135"/>
</packageinstances>
</package3d>
<package3d name="INDC4509X190" urn="urn:adsk.eagle:package:3810712/1" type="model" library_version="38">
<description>CHIP, 4.5 X 0.9 X 1.9 mm body
&lt;p&gt;CHIP package with body size 4.5 X 0.9 X 1.9 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="INDC4509X190"/>
</packageinstances>
</package3d>
<package3d name="INDC4532X175" urn="urn:adsk.eagle:package:3810715/1" type="model" library_version="38">
<description>CHIP, 4.5 X 3.2 X 1.75 mm body
&lt;p&gt;CHIP package with body size 4.5 X 3.2 X 1.75 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="INDC4532X175"/>
</packageinstances>
</package3d>
<package3d name="INDC5750X180" urn="urn:adsk.eagle:package:3810722/1" type="model" library_version="38">
<description>CHIP, 5.7 X 5 X 1.8 mm body
&lt;p&gt;CHIP package with body size 5.7 X 5 X 1.8 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="INDC5750X180"/>
</packageinstances>
</package3d>
<package3d name="INDC6350X200" urn="urn:adsk.eagle:package:3811095/1" type="model" library_version="38">
<description>CHIP, 6.3 X 5 X 2 mm body
&lt;p&gt;CHIP package with body size 6.3 X 5 X 2 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="INDC6350X200"/>
</packageinstances>
</package3d>
<package3d name="INDM4848X300" urn="urn:adsk.eagle:package:3837793/1" type="model" library_version="38">
<description>MOLDED BODY, 4.8 X 4.8 X 3 mm body
&lt;p&gt;MOLDED BODY package with body size 4.8 X 4.8 X 3 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="INDM4848X300"/>
</packageinstances>
</package3d>
<package3d name="INDM4040X300" urn="urn:adsk.eagle:package:3890461/1" type="model" library_version="38">
<description>MOLDED BODY, 4 X 4 X 3 mm body
&lt;p&gt;MOLDED BODY package with body size 4 X 4 X 3 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="INDM4040X300"/>
</packageinstances>
</package3d>
<package3d name="INDM4440X300" urn="urn:adsk.eagle:package:3890484/1" type="model" library_version="38">
<description>MOLDED BODY, 4.45 X 4.06 X 3 mm body
&lt;p&gt;MOLDED BODY package with body size 4.45 X 4.06 X 3 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="INDM4440X300"/>
</packageinstances>
</package3d>
<package3d name="INDM7070X450" urn="urn:adsk.eagle:package:3890494/1" type="model" library_version="38">
<description>MOLDED BODY, 7 X 7 X 4.5 mm body
&lt;p&gt;MOLDED BODY package with body size 7 X 7 X 4.5 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="INDM7070X450"/>
</packageinstances>
</package3d>
<package3d name="INDM4949X400" urn="urn:adsk.eagle:package:3904911/1" type="model" library_version="38">
<description>MOLDED BODY, 4.9 X 4.9 X 4 mm body
&lt;p&gt;MOLDED BODY package with body size 4.9 X 4.9 X 4 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="INDM4949X400"/>
</packageinstances>
</package3d>
<package3d name="INDM6363X500" urn="urn:adsk.eagle:package:3920872/1" type="model" library_version="38">
<description>MOLDED BODY, 6.3 X 6.3 X 5 mm body
&lt;p&gt;MOLDED BODY package with body size 6.3 X 6.3 X 5 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="INDM6363X500"/>
</packageinstances>
</package3d>
<package3d name="INDM238238X1016" urn="urn:adsk.eagle:package:3949005/1" type="model" library_version="38">
<description>MOLDED BODY, 23.88 X 23.88 X 10.16 mm body
&lt;p&gt;MOLDED BODY package with body size 23.88 X 23.88 X 10.16 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="INDM238238X1016"/>
</packageinstances>
</package3d>
<package3d name="INDM8080X400" urn="urn:adsk.eagle:package:3950981/1" type="model" library_version="38">
<description>MOLDED BODY, 8 X 8 X 4 mm body
&lt;p&gt;MOLDED BODY package with body size 8 X 8 X 4 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="INDM8080X400"/>
</packageinstances>
</package3d>
<package3d name="INDM125125X650" urn="urn:adsk.eagle:package:3950988/1" type="model" library_version="38">
<description>MOLDED BODY, 12.5 X 12.5 X 6.5 mm body
&lt;p&gt;MOLDED BODY package with body size 12.5 X 12.5 X 6.5 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="INDM125125X650"/>
</packageinstances>
</package3d>
<package3d name="MOLEX_5031590400_SD" urn="urn:adsk.eagle:package:8414052/1" type="box" library_version="40">
<packageinstances>
<packageinstance name="MOLEX_5031590400_SD"/>
</packageinstances>
</package3d>
<package3d name="SAMTEC_TSW-104-XX-X-S" urn="urn:adsk.eagle:package:8414075/1" type="box" library_version="40">
<packageinstances>
<packageinstance name="SAMTEC_TSW-104-XX-X-S"/>
</packageinstances>
</package3d>
<package3d name="SAMTEC_TSW-104-XX-X-S_NO_OUTLINE" urn="urn:adsk.eagle:package:8414076/1" type="box" library_version="40">
<packageinstances>
<packageinstance name="SAMTEC_TSW-104-XX-X-S_NO_OUTLINE"/>
</packageinstances>
</package3d>
<package3d name="SAMTEC_TSW-104-XX-X-S_FLEXSMD_PAD" urn="urn:adsk.eagle:package:8414081/1" type="box" library_version="40">
<packageinstances>
<packageinstance name="SAMTEC_TSW-104-XX-X-S_FLEXSMD_PAD"/>
</packageinstances>
</package3d>
<package3d name="JST_B4P-SHF-1AA" urn="urn:adsk.eagle:package:8414089/2" type="model" library_version="40">
<packageinstances>
<packageinstance name="JST_B4P-SHF-1AA"/>
</packageinstances>
</package3d>
<package3d name="JST_BS4P-SHF-1AA" urn="urn:adsk.eagle:package:8414090/1" type="box" library_version="40">
<packageinstances>
<packageinstance name="JST_BS4P-SHF-1AA"/>
</packageinstances>
</package3d>
<package3d name="JST_F4P-HVQ" urn="urn:adsk.eagle:package:8414091/1" type="box" library_version="40">
<packageinstances>
<packageinstance name="JST_F4P-HVQ"/>
</packageinstances>
</package3d>
<package3d name="JST_F4P-SHVQ" urn="urn:adsk.eagle:package:8414092/1" type="box" library_version="40">
<packageinstances>
<packageinstance name="JST_F4P-SHVQ"/>
</packageinstances>
</package3d>
<package3d name="JST_04JQ-BT" urn="urn:adsk.eagle:package:8414218/2" type="model" library_version="40">
<packageinstances>
<packageinstance name="JST_04JQ-BT"/>
</packageinstances>
</package3d>
<package3d name="JST_04JQ-ST" urn="urn:adsk.eagle:package:8414219/2" type="model" library_version="40">
<packageinstances>
<packageinstance name="JST_04JQ-ST"/>
</packageinstances>
</package3d>
<package3d name="JST_B4B-XH-A" urn="urn:adsk.eagle:package:8414220/2" type="model" library_version="40">
<packageinstances>
<packageinstance name="JST_B4B-XH-A"/>
</packageinstances>
</package3d>
<package3d name="JST_S4B-XH-A-1" urn="urn:adsk.eagle:package:8414221/1" type="box" library_version="40">
<packageinstances>
<packageinstance name="JST_S4B-XH-A-1"/>
</packageinstances>
</package3d>
<package3d name="JST_B04B-PASK" urn="urn:adsk.eagle:package:8414269/2" type="model" library_version="40">
<packageinstances>
<packageinstance name="JST_B04B-PASK"/>
</packageinstances>
</package3d>
<package3d name="JST_S04B-PASK-2" urn="urn:adsk.eagle:package:8414270/1" type="box" library_version="40">
<packageinstances>
<packageinstance name="JST_S04B-PASK-2"/>
</packageinstances>
</package3d>
<package3d name="JST_B4B-PH-K-S" urn="urn:adsk.eagle:package:8414302/2" type="model" library_version="40">
<packageinstances>
<packageinstance name="JST_B4B-PH-K-S"/>
</packageinstances>
</package3d>
<package3d name="JST_S4B-PH-K-S" urn="urn:adsk.eagle:package:8414303/1" type="box" library_version="40">
<packageinstances>
<packageinstance name="JST_S4B-PH-K-S"/>
</packageinstances>
</package3d>
<package3d name="JST_B04B-PSILE-A1_B04B-PSIY-B1_B04B-PSIR-C1" urn="urn:adsk.eagle:package:8414334/2" type="model" library_version="40">
<packageinstances>
<packageinstance name="JST_B04B-PSILE-A1_B04B-PSIY-B1_B04B-PSIR-C1"/>
</packageinstances>
</package3d>
<package3d name="JST_B4PS-VH" urn="urn:adsk.eagle:package:8414350/2" type="model" library_version="40">
<packageinstances>
<packageinstance name="JST_B4PS-VH"/>
</packageinstances>
</package3d>
<package3d name="JST_B4P-VH" urn="urn:adsk.eagle:package:8414351/2" type="model" library_version="40">
<packageinstances>
<packageinstance name="JST_B4P-VH"/>
</packageinstances>
</package3d>
<package3d name="JST_B4P-VH-FB-B" urn="urn:adsk.eagle:package:8414352/2" type="model" library_version="40">
<packageinstances>
<packageinstance name="JST_B4P-VH-FB-B"/>
</packageinstances>
</package3d>
<package3d name="JST_B04B-ZESK-1D" urn="urn:adsk.eagle:package:8414376/2" type="model" library_version="40">
<packageinstances>
<packageinstance name="JST_B04B-ZESK-1D"/>
</packageinstances>
</package3d>
<package3d name="JST_S04B-ZESK-2D" urn="urn:adsk.eagle:package:8414377/1" type="box" library_version="40">
<packageinstances>
<packageinstance name="JST_S04B-ZESK-2D"/>
</packageinstances>
</package3d>
<package3d name="HDRRA4W64P254_1X4_1016X254X254B" urn="urn:adsk.eagle:package:15597948/1" type="model" library_version="40">
<description>Single-row, 4-pin Pin Header (Male) Right Angle, 2.54 mm (0.10 in) col pitch, 5.84 mm mating length, 10.16 X 2.54 X 2.54 mm body
&lt;p&gt;Single-row (1X4), 4-pin Pin Header (Male) Right Angle package with 2.54 mm (0.10 in) col pitch, 0.64 mm lead width, 2.29 mm tail length and 5.84 mm mating length with body size 10.16 X 2.54 X 2.54 mm, pin pattern - clockwise from top left&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="HDRRA4W64P254_1X4_1016X254X254B"/>
</packageinstances>
</package3d>
<package3d name="HDRV4W64P254_1X4_1016X508X1829B" urn="urn:adsk.eagle:package:15735943/2" type="model" library_version="40">
<description>Single-row, 4-pin Pin Header (Male) Straight, 2.54 mm (0.10 in) col pitch, 15.75 mm mating length, 10.16 X 5.08 X 18.29 mm body
&lt;p&gt;Single-row (1X4), 4-pin Pin Header (Male) Straight package with 2.54 mm (0.10 in) col pitch, 0.64 mm lead width, 2.79 mm tail length and 15.75 mm mating length with overall size 10.16 X 5.08 X 18.29 mm, pin pattern - clockwise from top left&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="HDRV4W64P254_1X4_1016X508X1829B"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="RESISTOR" urn="urn:adsk.eagle:symbol:8413611/1" library_version="27">
<text x="-2.54" y="1.524" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.2032" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.2032" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.2032" layer="94"/>
</symbol>
<symbol name="CAPACITOR" urn="urn:adsk.eagle:symbol:8413617/1" library_version="27">
<wire x1="-2.54" y1="0" x2="-0.762" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="0.762" y2="0" width="0.1524" layer="94"/>
<text x="-2.54" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-4.318" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-1.524" y1="-0.254" x2="2.54" y2="0.254" layer="94" rot="R90"/>
<rectangle x1="-2.54" y1="-0.254" x2="1.524" y2="0.254" layer="94" rot="R90"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="GDT_2POS" urn="urn:adsk.eagle:symbol:8413625/1" library_version="27">
<polygon width="0.254" layer="94">
<vertex x="-0.635" y="1.27"/>
<vertex x="0.635" y="1.27"/>
<vertex x="0.635" y="0.635"/>
<vertex x="-0.635" y="0.635"/>
</polygon>
<wire x1="-0.635" y1="-0.635" x2="-0.635" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-0.635" y1="-1.27" x2="0.635" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0.635" y1="-1.27" x2="0.635" y2="-0.635" width="0.254" layer="94"/>
<wire x1="0.635" y1="-0.635" x2="-0.635" y2="-0.635" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="2.54" y2="0" width="0.254" layer="94" curve="90"/>
<wire x1="2.54" y1="0" x2="0" y2="2.54" width="0.254" layer="94" curve="90"/>
<wire x1="0" y1="2.54" x2="-2.54" y2="0" width="0.254" layer="94" curve="90"/>
<wire x1="-2.54" y1="0" x2="0" y2="-2.54" width="0.254" layer="94" curve="90"/>
<pin name="1" x="0" y="5.08" visible="pad" length="short" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="pad" length="short" rot="R90"/>
<text x="5.08" y="-2.54" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
</symbol>
<symbol name="CONNECTOR_SMA" urn="urn:adsk.eagle:symbol:8413639/1" library_version="27">
<wire x1="2.54" y1="-2.54" x2="1.778" y2="-1.778" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="2.032" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="2.54" x2="2.54" y2="0.508" width="0.3048" layer="94" curve="-79.611142" cap="flat"/>
<wire x1="0" y1="-2.54" x2="2.54" y2="-0.508" width="0.3048" layer="94" curve="79.611142" cap="flat"/>
<text x="0" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<text x="0" y="3.302" size="1.778" layer="95">&gt;NAME</text>
<rectangle x1="0" y1="-0.254" x2="2.286" y2="0.254" layer="94"/>
<pin name="1" x="5.08" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<pin name="2" x="5.08" y="-2.54" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="DIODE_ZENER" urn="urn:adsk.eagle:symbol:8413614/1" library_version="27" library_locally_modified="yes">
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="0.635" y1="1.778" x2="1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="-1.27" x2="1.905" y2="-1.778" width="0.254" layer="94"/>
<text x="-2.54" y="1.905" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.778" layer="96">&gt;VALUE</text>
<pin name="A" x="-3.81" y="0" visible="off" length="short" direction="pas"/>
<pin name="K" x="3.81" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="DIODE_SCHOTTKY" urn="urn:adsk.eagle:symbol:8413616/1" library_version="28" library_locally_modified="yes">
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.905" y1="1.27" x2="1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.905" y1="1.27" x2="1.905" y2="1.016" width="0.254" layer="94"/>
<wire x1="1.27" y1="-1.27" x2="0.635" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0.635" y1="-1.016" x2="0.635" y2="-1.27" width="0.254" layer="94"/>
<text x="-2.54" y="1.905" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.778" layer="96">&gt;VALUE</text>
<pin name="A" x="-3.81" y="0" visible="off" length="short" direction="pas"/>
<pin name="K" x="3.81" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="TI_SN65HVD62RGTR" urn="urn:adsk.eagle:symbol:8413653/1" library_version="30">
<pin name="SYNCOUT" x="-5.08" y="-2.54" visible="pad" length="middle" direction="in"/>
<pin name="TXIN" x="-5.08" y="-5.08" visible="pad" length="middle" direction="in"/>
<pin name="VL" x="-5.08" y="-7.62" visible="pad" length="middle" direction="in"/>
<pin name="RXOUT" x="-5.08" y="-10.16" visible="pad" length="middle" direction="in"/>
<pin name="DIR" x="-5.08" y="-12.7" visible="pad" length="middle" direction="in"/>
<pin name="DIRSET2" x="-5.08" y="-15.24" visible="pad" length="middle" direction="in"/>
<pin name="DIRSET1" x="-5.08" y="-17.78" visible="pad" length="middle" direction="in"/>
<pin name="GND_2" x="-5.08" y="-20.32" visible="pad" length="middle" direction="in"/>
<pin name="RES" x="30.48" y="-22.86" visible="pad" length="middle" direction="in" rot="R180"/>
<pin name="BIAS" x="30.48" y="-20.32" visible="pad" length="middle" direction="in" rot="R180"/>
<pin name="RXIN" x="30.48" y="-17.78" visible="pad" length="middle" direction="in" rot="R180"/>
<pin name="TXOUT" x="30.48" y="-15.24" visible="pad" length="middle" direction="in" rot="R180"/>
<pin name="VCC" x="30.48" y="-12.7" visible="pad" length="middle" direction="in" rot="R180"/>
<pin name="XTAL1" x="30.48" y="-10.16" visible="pad" length="middle" direction="in" rot="R180"/>
<pin name="XTAL2" x="30.48" y="-7.62" visible="pad" length="middle" direction="in" rot="R180"/>
<pin name="GND" x="30.48" y="-5.08" visible="pad" length="middle" direction="in" rot="R180"/>
<pin name="EPAD" x="30.48" y="-2.54" visible="pad" length="middle" direction="in" rot="R180"/>
<wire x1="0" y1="0" x2="0" y2="-25.4" width="0.1524" layer="94"/>
<wire x1="0" y1="-25.4" x2="25.4" y2="-25.4" width="0.1524" layer="94"/>
<wire x1="25.4" y1="-25.4" x2="25.4" y2="0" width="0.1524" layer="94"/>
<wire x1="25.4" y1="0" x2="0" y2="0" width="0.1524" layer="94"/>
<text x="7.62" y="5.08" size="2.0828" layer="95" ratio="6" rot="SR0">&gt;Name</text>
<text x="7.62" y="2.54" size="2.0828" layer="96" ratio="6" rot="SR0">&gt;Value</text>
<text x="1.016" y="-3.556" size="1.778" layer="94" ratio="15">SYNCOUT</text>
<text x="1.016" y="-6.096" size="1.778" layer="94" ratio="15">TXIN</text>
<text x="1.016" y="-8.636" size="1.778" layer="94" ratio="15">VL</text>
<text x="1.016" y="-11.176" size="1.778" layer="94" ratio="15">RXOUT</text>
<text x="1.016" y="-13.716" size="1.778" layer="94" ratio="15">DIR</text>
<text x="1.016" y="-16.256" size="1.778" layer="94" ratio="15">DIRSET2</text>
<text x="1.016" y="-18.796" size="1.778" layer="94" ratio="15">DIRSET1</text>
<text x="1.016" y="-21.336" size="1.778" layer="94" ratio="15">GND_2</text>
<text x="24.13" y="-1.524" size="1.778" layer="94" ratio="15" rot="R180">EPAD</text>
<text x="24.13" y="-4.064" size="1.778" layer="94" ratio="15" rot="R180">GND</text>
<text x="24.13" y="-6.604" size="1.778" layer="94" ratio="15" rot="R180">XTAL2</text>
<text x="24.13" y="-9.144" size="1.778" layer="94" ratio="15" rot="R180">XTAL1</text>
<text x="24.13" y="-11.938" size="1.778" layer="94" ratio="15" rot="R180">VCC</text>
<text x="24.13" y="-14.478" size="1.778" layer="94" ratio="15" rot="R180">TXOUT</text>
<text x="24.13" y="-17.018" size="1.778" layer="94" ratio="15" rot="R180">RXIN</text>
<text x="24.13" y="-19.558" size="1.778" layer="94" ratio="15" rot="R180">BIAS</text>
<text x="24.13" y="-22.098" size="1.778" layer="94" ratio="15" rot="R180">RES</text>
</symbol>
<symbol name="CRYSTAL_2-POS" urn="urn:adsk.eagle:symbol:8413600/1" library_version="30">
<wire x1="1.016" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.016" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.381" y1="1.524" x2="-0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="-0.381" y1="-1.524" x2="0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="-1.524" x2="0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="1.524" x2="-0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="1.016" y1="1.778" x2="1.016" y2="-1.778" width="0.254" layer="94"/>
<wire x1="-1.016" y1="1.778" x2="-1.016" y2="-1.778" width="0.254" layer="94"/>
<text x="2.54" y="1.016" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<text x="-2.159" y="-1.143" size="0.8636" layer="93">1</text>
<text x="1.524" y="-1.143" size="0.8636" layer="93">2</text>
<pin name="2" x="2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="MOSFET-NCH" urn="urn:adsk.eagle:symbol:8413608/1" library_version="30">
<wire x1="-2.54" y1="-2.54" x2="-1.2192" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="0.762" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-0.762" width="0.254" layer="94"/>
<wire x1="0" y1="3.683" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="1.397" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.397" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-3.683" width="0.254" layer="94"/>
<wire x1="-1.143" y1="2.54" x2="-1.143" y2="-2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="0.508" width="0.1524" layer="94"/>
<wire x1="5.08" y1="0.508" x2="5.08" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="5.08" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="2.54" x2="5.08" y2="2.54" width="0.1524" layer="94"/>
<wire x1="5.588" y1="0.508" x2="5.08" y2="0.508" width="0.1524" layer="94"/>
<wire x1="5.08" y1="0.508" x2="4.572" y2="0.508" width="0.1524" layer="94"/>
<circle x="2.54" y="-2.54" radius="0.3592" width="0" layer="94"/>
<circle x="2.54" y="2.54" radius="0.3592" width="0" layer="94"/>
<text x="-11.43" y="0" size="1.778" layer="96">&gt;VALUE</text>
<text x="-11.43" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<pin name="S" x="2.54" y="-7.62" visible="off" length="middle" direction="pas" rot="R90"/>
<pin name="G" x="-5.08" y="-2.54" visible="off" length="short" direction="pas"/>
<pin name="D" x="2.54" y="7.62" visible="off" length="middle" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="5.08" y="0.508"/>
<vertex x="4.572" y="-0.254"/>
<vertex x="5.588" y="-0.254"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="0.254" y="0"/>
<vertex x="1.27" y="0.762"/>
<vertex x="1.27" y="-0.762"/>
</polygon>
</symbol>
<symbol name="TI_LM22672MRX-ADJ" urn="urn:adsk.eagle:symbol:8413651/1" library_version="30">
<pin name="BOOT" x="25.4" y="-5.08" visible="pad" length="middle" rot="R180"/>
<pin name="SS" x="-5.08" y="-17.78" visible="pad" length="middle"/>
<pin name="RT/SYNC" x="-5.08" y="-12.7" visible="pad" length="middle"/>
<pin name="FB" x="25.4" y="-10.16" visible="pad" length="middle" rot="R180"/>
<pin name="EN" x="-5.08" y="-7.62" visible="pad" length="middle"/>
<pin name="GND" x="25.4" y="-15.24" visible="pad" length="middle" rot="R180"/>
<pin name="VIN" x="-5.08" y="-2.54" visible="pad" length="middle"/>
<pin name="SW" x="25.4" y="-2.54" visible="pad" length="middle" rot="R180"/>
<pin name="PAD" x="25.4" y="-17.78" visible="pad" length="middle" rot="R180"/>
<wire x1="0" y1="0" x2="0" y2="-20.32" width="0.1524" layer="94"/>
<wire x1="0" y1="-20.32" x2="20.32" y2="-20.32" width="0.1524" layer="94"/>
<wire x1="20.32" y1="-20.32" x2="20.32" y2="0" width="0.1524" layer="94"/>
<wire x1="20.32" y1="0" x2="0" y2="0" width="0.1524" layer="94"/>
<text x="5.08" y="2.54" size="2.0828" layer="95" ratio="6" rot="SR0">&gt;Name</text>
<text x="5.08" y="0.254" size="2.0828" layer="96" ratio="6" rot="SR0">&gt;Value</text>
<text x="2.54" y="-2.54" size="1.778" layer="94">VIN</text>
<text x="2.54" y="-7.62" size="1.778" layer="94">EN</text>
<text x="2.54" y="-12.7" size="1.778" layer="94">RT/SYNC</text>
<text x="2.54" y="-17.78" size="1.778" layer="94">SS</text>
<text x="17.78" y="-0.762" size="1.778" layer="94" rot="R180">SW</text>
<text x="17.78" y="-3.302" size="1.778" layer="94" rot="R180">BOOT</text>
<text x="17.78" y="-8.382" size="1.778" layer="94" rot="R180">FB</text>
<text x="17.78" y="-13.716" size="1.778" layer="94" rot="R180">GND</text>
<text x="17.78" y="-16.002" size="1.778" layer="94" rot="R180">PAD</text>
</symbol>
<symbol name="TP" urn="urn:adsk.eagle:symbol:8413657/1" library_version="30" library_locally_modified="yes">
<wire x1="0.762" y1="1.778" x2="0" y2="0.254" width="0.254" layer="94"/>
<wire x1="0" y1="0.254" x2="-0.762" y2="1.778" width="0.254" layer="94"/>
<wire x1="-0.762" y1="1.778" x2="0.762" y2="1.778" width="0.254" layer="94" curve="-180"/>
<text x="-1.27" y="3.81" size="1.778" layer="95">&gt;NAME</text>
<pin name="PP" x="0" y="0" visible="off" length="short" direction="in" rot="R90"/>
</symbol>
<symbol name="TRANSCEIVER_RS485" urn="urn:adsk.eagle:symbol:8413649/1" library_version="30" library_locally_modified="yes">
<text x="1.27" y="-7.62" size="1.778" layer="94" ratio="15">R0</text>
<text x="1.27" y="-12.7" size="1.778" layer="94" ratio="15">!RE</text>
<text x="1.27" y="-17.78" size="1.778" layer="94" ratio="15">DE</text>
<text x="1.27" y="-22.86" size="1.778" layer="94" ratio="15">DI</text>
<text x="19.05" y="-22.86" size="1.778" layer="94" ratio="15" align="bottom-right">GND</text>
<text x="19.05" y="-17.78" size="1.778" layer="94" ratio="15" align="bottom-right">A(D0/RI)</text>
<text x="19.05" y="-12.7" size="1.778" layer="94" ratio="15" align="bottom-right">B(!D0!/!RI!)</text>
<text x="19.05" y="-7.62" size="1.778" layer="94" ratio="15" align="bottom-right">VCC</text>
<text x="6.35" y="-2.54" size="1.778" layer="94" ratio="15">RS485</text>
<text x="0" y="2.54" size="1.778" layer="95" ratio="15">&gt;NAME</text>
<text x="0" y="-27.94" size="1.778" layer="96" ratio="15">&gt;VALUE</text>
<wire x1="0" y1="-2.54" x2="0" y2="-25.4" width="0.254" layer="94"/>
<wire x1="0" y1="-25.4" x2="20.32" y2="-25.4" width="0.254" layer="94"/>
<wire x1="20.32" y1="-25.4" x2="20.32" y2="0" width="0.254" layer="94"/>
<wire x1="20.32" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="0" y2="-2.54" width="0.254" layer="94"/>
<pin name="1" x="-2.54" y="-7.62" visible="pad" length="short"/>
<pin name="2" x="-2.54" y="-12.7" visible="pad" length="short"/>
<pin name="3" x="-2.54" y="-17.78" visible="pad" length="short"/>
<pin name="4" x="-2.54" y="-22.86" visible="pad" length="short"/>
<pin name="5" x="22.86" y="-22.86" visible="pad" length="short" rot="R180"/>
<pin name="6" x="22.86" y="-17.78" visible="pad" length="short" rot="R180"/>
<pin name="7" x="22.86" y="-12.7" visible="pad" length="short" rot="R180"/>
<pin name="8" x="22.86" y="-7.62" visible="pad" length="short" rot="R180"/>
</symbol>
<symbol name="FERRITE_BEAD" urn="urn:adsk.eagle:symbol:8413593/1" library_version="30" library_locally_modified="yes">
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="-1.27" y1="0" x2="0" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="0" y1="0" x2="1.27" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="-2.54" y1="1.27" x2="2.54" y2="1.27" width="0.254" layer="94"/>
<text x="-2.54" y="3.81" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="INDUCTOR" urn="urn:adsk.eagle:symbol:8413612/1" library_version="30" library_locally_modified="yes">
<text x="-2.54" y="1.524" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="-1.27" y1="0" x2="0" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="0" y1="0" x2="1.27" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.254" layer="94" curve="-180"/>
</symbol>
<symbol name="DIODE_ZENER_BI-DIRECTIONAL" urn="urn:adsk.eagle:symbol:8413645/1" library_version="35" library_locally_modified="yes">
<wire x1="-2.54" y1="-1.27" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-2.54" y2="1.27" width="0.254" layer="94"/>
<wire x1="-0.635" y1="1.778" x2="0" y2="1.27" width="0.254" layer="94"/>
<wire x1="0" y1="1.27" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="-2.54" y1="1.27" x2="-2.54" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="0.635" y2="-1.778" width="0.254" layer="94"/>
<wire x1="2.54" y1="1.27" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="2.54" y2="-1.27" width="0.254" layer="94"/>
<wire x1="2.54" y1="-1.27" x2="2.54" y2="1.27" width="0.254" layer="94"/>
<text x="-3.81" y="1.905" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.81" size="1.778" layer="96">&gt;VALUE</text>
<pin name="A" x="-5.08" y="0" visible="off" length="short" direction="pas"/>
<pin name="K" x="5.08" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="TI_TPS2491DGSR" urn="urn:adsk.eagle:symbol:8413655/1" library_version="35" library_locally_modified="yes">
<wire x1="20.32" y1="0" x2="20.32" y2="-10.16" width="0.254" layer="94"/>
<wire x1="20.32" y1="-10.16" x2="0" y2="-10.16" width="0.254" layer="94"/>
<wire x1="0" y1="-10.16" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="20.32" y2="0" width="0.254" layer="94"/>
<pin name="10_VCC" x="-2.54" y="-2.54" visible="pad" length="short"/>
<pin name="1_EN" x="-2.54" y="-5.08" visible="pad" length="short"/>
<pin name="2_VREF" x="-2.54" y="-7.62" visible="pad" length="short"/>
<pin name="3_PROG" x="5.08" y="-12.7" visible="pad" length="short" rot="R90"/>
<pin name="4_TIMER" x="10.16" y="-12.7" visible="pad" length="short" rot="R90"/>
<pin name="5_GND" x="15.24" y="-12.7" visible="pad" length="short" rot="R90"/>
<pin name="6_PG" x="22.86" y="-2.54" visible="pad" length="short" rot="R180"/>
<pin name="9_SENSE" x="5.08" y="2.54" visible="pad" length="short" rot="R270"/>
<pin name="8_GATE" x="10.16" y="2.54" visible="pad" length="short" rot="R270"/>
<pin name="7_OUT" x="15.24" y="2.54" visible="pad" length="short" rot="R270"/>
<text x="7.112" y="-5.08" size="1.27" layer="94">TPS2491</text>
<text x="3.556" y="-1.27" size="0.8128" layer="94">SENSE</text>
<text x="8.89" y="-1.27" size="0.8128" layer="94">GATE</text>
<text x="14.224" y="-1.27" size="0.8128" layer="94">OUT</text>
<text x="18.542" y="-2.794" size="0.8128" layer="94">PG</text>
<text x="14.224" y="-9.652" size="0.8128" layer="94">GND</text>
<text x="8.89" y="-9.652" size="0.8128" layer="94">TIMER</text>
<text x="3.81" y="-9.652" size="0.8128" layer="94">PROG</text>
<text x="0.508" y="-8.128" size="0.8128" layer="94">VREF</text>
<text x="0.508" y="-5.588" size="0.8128" layer="94">EN</text>
<text x="0.508" y="-3.048" size="0.8128" layer="94">VCC</text>
<text x="-2.54" y="2.54" size="1.27" layer="95" ratio="15">&gt;NAME</text>
<text x="-2.54" y="5.08" size="1.27" layer="95" ratio="15">&gt;VALUE</text>
</symbol>
<symbol name="PIN" urn="urn:adsk.eagle:symbol:8413622/1" library_version="40">
<pin name="1" x="-5.08" y="0" visible="off"/>
<wire x1="-3.302" y1="0" x2="-1.27" y2="0" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="0" x2="2.54" y2="0" width="0.8128" layer="94"/>
<text x="-3.302" y="0.508" size="1.27" layer="95" ratio="15">&gt;NAME</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="RESISTOR" urn="urn:adsk.eagle:component:8414463/1" prefix="R" uservalue="yes" library_version="38">
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="0201" package="RESC0603X30">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3811151/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402" package="RESC1005X40">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3811154/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="RESC1608X55">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3811166/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="RESC2012X70">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3811169/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="RESC3115X70">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3811173/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1210" package="RESC3225X70">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3811180/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1812" package="RESC4532X70">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3811188/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2010" package="RESC5025X70">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3811197/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2220" package="RESC5750X229">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3811212/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2512" package="RESC6432X70">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3811284/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1508_WIDE" package="RESC2037X52">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3950966/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAPACITOR" urn="urn:adsk.eagle:component:8414470/2" prefix="C" uservalue="yes" library_version="38">
<gates>
<gate name="G$1" symbol="CAPACITOR" x="0" y="0"/>
</gates>
<devices>
<device name="0402" package="CAPC1005X60">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793108/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0508" package="CAPC1220X107">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793115/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="CAPC1608X55">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793126/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0612" package="CAPC1632X168">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793131/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="CAPC2012X127">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793139/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="CAPC3215X168">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793145/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1210" package="CAPC3225X168">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793149/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1808" package="CAPC4520X168">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793154/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1812" package="CAPC4532X218">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793159/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1825" package="CAPC4564X218">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793166/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2220" package="CAPC5650X218">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793171/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2225" package="CAPC5563X218">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793174/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3640" package="CAPC9198X218">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793178/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GDT_2POS" urn="urn:adsk.eagle:component:8414479/2" prefix="F" uservalue="yes" library_version="38">
<gates>
<gate name="G$1" symbol="GDT_2POS" x="0" y="0"/>
</gates>
<devices>
<device name="BOURNS_2029-XX-SMLF" package="GDTMELF5875">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3890106/3"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="BOURNS_2035-XX-SM" package="GDTMELF4450">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3890127/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TRANSITION_CLIP" urn="urn:adsk.eagle:component:8414559/2" prefix="J" uservalue="yes" library_version="30">
<gates>
<gate name="G$1" symbol="CONNECTOR_SMA" x="0" y="0"/>
</gates>
<devices>
<device name="TRANSITION_CLIP_.141" package="TRANSITION_CLIP_.141">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414403/4"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DIODE_ZENER" urn="urn:adsk.eagle:component:8414467/6" prefix="D" uservalue="yes" library_version="38">
<gates>
<gate name="G$1" symbol="DIODE_ZENER" x="0" y="0"/>
</gates>
<devices>
<device name="SMA" package="DIOM5226X292">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793189/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMB" package="DIOM5436X265">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793201/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMC" package="DIOM7959X265">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793207/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOD-323" package="SOD2512X100">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793216/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOD-123" package="SOD3716X135">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793231/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOT-323" package="SOT65P210X110-3">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3809951/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOT-23" package="SOT95P280X135-3">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3809961/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOD-128" package="SODFL4725X110">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3921253/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0201" package="DIOC0502X50">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3948086/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402" package="DIOC1005X84">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3948100/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0502" package="DIOC1206X63">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3948141/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="DIOC1608X84">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3948147/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="DIOC2012X70">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3948150/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="DIOC3216X84">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3948168/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2010" package="DIOC5324X84">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3948171/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TO-252-3" package="TO457P1000X238-3">
<connects>
<connect gate="G$1" pin="A" pad="1 2"/>
<connect gate="G$1" pin="K" pad="3"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3920001/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="DO214AB" package="DO214AB_SMC">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="K" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15618120/3"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DIODE_SCHOTTKY" urn="urn:adsk.eagle:component:8414469/5" prefix="D" uservalue="yes" library_version="38">
<gates>
<gate name="G$1" symbol="DIODE_SCHOTTKY" x="0" y="0"/>
</gates>
<devices>
<device name="SMA" package="DIOM5226X292">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793189/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMB" package="DIOM5436X265">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793201/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMC" package="DIOM7959X265">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793207/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOD-323" package="SOD2512X100">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793216/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOD-123" package="SOD3716X135">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793231/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOT-323" package="SOT65P210X110-3">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3809951/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOT-23" package="SOT95P280X135-3">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3809961/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOD-128" package="SODFL4725X110">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3921253/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0201" package="DIOC0502X50">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3948086/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402" package="DIOC1005X84">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3948100/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0502" package="DIOC1206X63">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3948141/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="DIOC1608X84">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3948147/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="DIOC2012X70">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3948150/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="DIOC3216X84">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3948168/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2010" package="DIOC5324X84">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3948171/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="DO214AC_SMA" package="DO214AC_SMA">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="K" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15671034/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TI_SN65HVD62RGTR" urn="urn:adsk.eagle:component:8414514/1" prefix="U" uservalue="yes" library_version="38">
<gates>
<gate name="G$1" symbol="TI_SN65HVD62RGTR" x="0" y="0"/>
</gates>
<devices>
<device name="16-VFQFN_EXPOSED_PAD" package="QFN50P300X300X100-17">
<connects>
<connect gate="G$1" pin="BIAS" pad="10"/>
<connect gate="G$1" pin="DIR" pad="5"/>
<connect gate="G$1" pin="DIRSET1" pad="7"/>
<connect gate="G$1" pin="DIRSET2" pad="6"/>
<connect gate="G$1" pin="EPAD" pad="17"/>
<connect gate="G$1" pin="GND" pad="16"/>
<connect gate="G$1" pin="GND_2" pad="8"/>
<connect gate="G$1" pin="RES" pad="9"/>
<connect gate="G$1" pin="RXIN" pad="11"/>
<connect gate="G$1" pin="RXOUT" pad="4"/>
<connect gate="G$1" pin="SYNCOUT" pad="1"/>
<connect gate="G$1" pin="TXIN" pad="2"/>
<connect gate="G$1" pin="TXOUT" pad="12"/>
<connect gate="G$1" pin="VCC" pad="13"/>
<connect gate="G$1" pin="VL" pad="3"/>
<connect gate="G$1" pin="XTAL1" pad="14"/>
<connect gate="G$1" pin="XTAL2" pad="15"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3986130/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CRYSTAL_2-POS" urn="urn:adsk.eagle:component:8414444/1" prefix="Y" uservalue="yes" library_version="38">
<gates>
<gate name="G$1" symbol="CRYSTAL_2-POS" x="0" y="0"/>
</gates>
<devices>
<device name="HC-49/US" package="XTAL1140X470X330">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3837785/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MOSFET_NCH" urn="urn:adsk.eagle:component:8414460/1" prefix="Q" uservalue="yes" library_version="38">
<gates>
<gate name="G$1" symbol="MOSFET-NCH" x="0" y="0"/>
</gates>
<devices>
<device name="SOIC-8" package="SOIC127P600X175-8">
<connects>
<connect gate="G$1" pin="D" pad="5 6 7 8"/>
<connect gate="G$1" pin="G" pad="4"/>
<connect gate="G$1" pin="S" pad="1 2 3"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3833977/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOT-223" package="SOT230P700X180-4">
<connects>
<connect gate="G$1" pin="D" pad="2 4"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="3"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3834044/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOT-23" package="SOT95P280X135-3">
<connects>
<connect gate="G$1" pin="D" pad="K"/>
<connect gate="G$1" pin="G" pad="NC"/>
<connect gate="G$1" pin="S" pad="A"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3809961/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TO-252-3_DPAK" package="TO457P1000X238-3">
<connects>
<connect gate="G$1" pin="D" pad="3"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3920001/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POWERPAK_SO-8" package="POWERPAK_SO-8">
<connects>
<connect gate="G$1" pin="D" pad="5 6 7 8 9"/>
<connect gate="G$1" pin="G" pad="4"/>
<connect gate="G$1" pin="S" pad="1 2 3"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414082/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TI_LM22674MRE-ADJ/NOPBTR-ND" urn="urn:adsk.eagle:component:15661918/2" library_version="38">
<gates>
<gate name="G$1" symbol="TI_LM22672MRX-ADJ" x="-10.16" y="10.16"/>
</gates>
<devices>
<device name="" package="SOIC127P600X170-9T271X340N">
<connects>
<connect gate="G$1" pin="BOOT" pad="1"/>
<connect gate="G$1" pin="EN" pad="5"/>
<connect gate="G$1" pin="FB" pad="4"/>
<connect gate="G$1" pin="GND" pad="6"/>
<connect gate="G$1" pin="PAD" pad="9"/>
<connect gate="G$1" pin="RT/SYNC" pad="3"/>
<connect gate="G$1" pin="SS" pad="2"/>
<connect gate="G$1" pin="SW" pad="8"/>
<connect gate="G$1" pin="VIN" pad="7"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15661897/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="8-SOPOWERPAD" package="SOIC127P540X170-9T271X340N">
<connects>
<connect gate="G$1" pin="BOOT" pad="1"/>
<connect gate="G$1" pin="EN" pad="5"/>
<connect gate="G$1" pin="FB" pad="4"/>
<connect gate="G$1" pin="GND" pad="6"/>
<connect gate="G$1" pin="PAD" pad="9"/>
<connect gate="G$1" pin="RT/SYNC" pad="3"/>
<connect gate="G$1" pin="SS" pad="2"/>
<connect gate="G$1" pin="SW" pad="8"/>
<connect gate="G$1" pin="VIN" pad="7"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15672442/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TP_CLASS_B" urn="urn:adsk.eagle:component:8414518/4" prefix="TP" uservalue="yes" library_version="30" library_locally_modified="yes">
<gates>
<gate name="G$1" symbol="TP" x="0" y="0"/>
</gates>
<devices>
<device name="TP_1.5MM" package="TP_1.5MM">
<connects>
<connect gate="G$1" pin="PP" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414069/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TP_1.5MM_PAD" package="TP_1.5MM_PAD">
<connects>
<connect gate="G$1" pin="PP" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414070/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TP_1.7MM" package="TP_1.7MM">
<connects>
<connect gate="G$1" pin="PP" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414071/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C120H40" package="C120H40">
<connects>
<connect gate="G$1" pin="PP" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414063/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C131H51" package="C131H51">
<connects>
<connect gate="G$1" pin="PP" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414064/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C406H326" package="C406H326">
<connects>
<connect gate="G$1" pin="PP" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414065/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C491H411" package="C491H411">
<connects>
<connect gate="G$1" pin="PP" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414066/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CIR_PAD_1MM" package="CIR_PAD_1MM">
<connects>
<connect gate="G$1" pin="PP" pad="P$1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414067/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CIR_PAD_2MM" package="CIR_PAD_2MM">
<connects>
<connect gate="G$1" pin="PP" pad="P$1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414068/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TRANSCEIVER_RS485" urn="urn:adsk.eagle:component:8414510/1" prefix="U" uservalue="yes" library_version="38">
<gates>
<gate name="G$1" symbol="TRANSCEIVER_RS485" x="0" y="0"/>
</gates>
<devices>
<device name="8-SOIC" package="SOIC127P600X175-8">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3833977/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="FERRITE_BEAD" urn="urn:adsk.eagle:component:8414429/1" prefix="FB" uservalue="yes" library_version="38">
<gates>
<gate name="G$1" symbol="FERRITE_BEAD" x="0" y="0"/>
</gates>
<devices>
<device name="0201" package="BEADC0603X30">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3853343/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402" package="BEADC1005X40">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3853346/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="BEADC1608X55">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3887236/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="BEADC2012X70">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3887243/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="BEADC3115X70">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3887314/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1210" package="BEADC3225X70">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3887427/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1812" package="BEADC4532X70">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3887433/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2010" package="BEADC5025X70">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3887437/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2220" package="BEADC5750X229">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3889733/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2512" package="BEADC6432X70">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3889736/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="INDUCTOR" urn="urn:adsk.eagle:component:8414465/1" prefix="L" uservalue="yes" library_version="38">
<gates>
<gate name="G$1" symbol="INDUCTOR" x="0" y="0"/>
</gates>
<devices>
<device name="0402" package="INDC1005X60">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3810108/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="INDC1608X95">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3810118/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1008" package="INDC2520X220">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3810123/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1210" package="INDC3225X135">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3810128/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1806" package="INDC4509X190">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3810712/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1812" package="INDC4532X175">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3810715/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2220" package="INDC5750X180">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3810722/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2520" package="INDC6350X200">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3811095/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="4.80MM_X_4.80MM_SMT" package="INDM4848X300">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3837793/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5.00MM_X_5.00MM_SMT" package="INDM5050X430">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3890169/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="4.00MM_X_4.00MM_SMT" package="INDM4040X300">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3890461/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="4.45MM_X_4.06MM_SMT" package="INDM4440X300">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3890484/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="7.00MM_X_7.00MM_SMT" package="INDM7070X450">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3890494/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="4.90MM_X_4.90MM_SMT" package="INDM4949X400">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3904911/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="RADIAL_THT" package="RADIAL_THT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414048/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="6.30MM_X_6.30MM_SMT" package="INDM6363X500">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3920872/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="23.88MM_X_23.88MM_SMT" package="INDM238238X1016">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3949005/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="8.00MM_X_8.00MM_SMT" package="INDM8080X400">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3950981/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="12.5MM_X_12.5MM_SMT" package="INDM125125X650">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3950988/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DIODE_TVS_UNI-DIRECTIONAL" urn="urn:adsk.eagle:component:15726467/2" prefix="D" library_version="38">
<gates>
<gate name="G$1" symbol="DIODE_ZENER" x="0" y="0"/>
</gates>
<devices>
<device name="DIO214AB_SMC_TVS" package="DIOM7959X262N">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="K" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15726434/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DIODE_TVS_BI-DIRECTIONAL" urn="urn:adsk.eagle:component:15725918/3" library_version="38">
<gates>
<gate name="G$1" symbol="DIODE_ZENER_BI-DIRECTIONAL" x="0" y="0"/>
</gates>
<devices>
<device name="DO214AB_SMC" package="DIOM7957X265N">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="K" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15725844/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TI_TPS2491DGSR" urn="urn:adsk.eagle:component:8414516/1" prefix="U" uservalue="yes" library_version="38">
<gates>
<gate name="G$1" symbol="TI_TPS2491DGSR" x="0" y="0"/>
</gates>
<devices>
<device name="10-TFSOP" package="SOP50P490X110-10">
<connects>
<connect gate="G$1" pin="10_VCC" pad="10"/>
<connect gate="G$1" pin="1_EN" pad="1"/>
<connect gate="G$1" pin="2_VREF" pad="2"/>
<connect gate="G$1" pin="3_PROG" pad="3"/>
<connect gate="G$1" pin="4_TIMER" pad="4"/>
<connect gate="G$1" pin="5_GND" pad="5"/>
<connect gate="G$1" pin="6_PG" pad="6"/>
<connect gate="G$1" pin="7_OUT" pad="7"/>
<connect gate="G$1" pin="8_GATE" pad="8"/>
<connect gate="G$1" pin="9_SENSE" pad="9"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3921298/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CONNECTOR_4POS" urn="urn:adsk.eagle:component:8414493/6" prefix="J" uservalue="yes" library_version="40">
<gates>
<gate name="-1" symbol="PIN" x="5.08" y="0"/>
<gate name="-2" symbol="PIN" x="5.08" y="-2.54"/>
<gate name="-3" symbol="PIN" x="5.08" y="-5.08"/>
<gate name="-4" symbol="PIN" x="5.08" y="-7.62"/>
</gates>
<devices>
<device name="MOLEX_5031590400" package="MOLEX_5031590400_SD">
<connects>
<connect gate="-1" pin="1" pad="P$1"/>
<connect gate="-2" pin="1" pad="P$2"/>
<connect gate="-3" pin="1" pad="P$3"/>
<connect gate="-4" pin="1" pad="P$4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414052/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SAMTEC_TSW-104-XX-X-S" package="SAMTEC_TSW-104-XX-X-S">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-2" pin="1" pad="2"/>
<connect gate="-3" pin="1" pad="3"/>
<connect gate="-4" pin="1" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414075/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SAMTEC_TSW-104-XX-X-S_NO_OUTLINE" package="SAMTEC_TSW-104-XX-X-S_NO_OUTLINE">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-2" pin="1" pad="2"/>
<connect gate="-3" pin="1" pad="3"/>
<connect gate="-4" pin="1" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414076/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SAMTEC_TSW-104-XX-X-S_FLEXSMD_PAD" package="SAMTEC_TSW-104-XX-X-S_FLEXSMD_PAD">
<connects>
<connect gate="-1" pin="1" pad="1 P$1"/>
<connect gate="-2" pin="1" pad="2 P$2"/>
<connect gate="-3" pin="1" pad="3 P$3"/>
<connect gate="-4" pin="1" pad="4 P$4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414081/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST_B4P-SHF-1AA" package="JST_B4P-SHF-1AA">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-2" pin="1" pad="2"/>
<connect gate="-3" pin="1" pad="3"/>
<connect gate="-4" pin="1" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414089/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST_BS4P-SHF-1AA" package="JST_BS4P-SHF-1AA">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-2" pin="1" pad="2"/>
<connect gate="-3" pin="1" pad="3"/>
<connect gate="-4" pin="1" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414090/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST_F4P-HVQ" package="JST_F4P-HVQ">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-2" pin="1" pad="2"/>
<connect gate="-3" pin="1" pad="3"/>
<connect gate="-4" pin="1" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414091/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST_F4P-SHVQ" package="JST_F4P-SHVQ">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-2" pin="1" pad="2"/>
<connect gate="-3" pin="1" pad="3"/>
<connect gate="-4" pin="1" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414092/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST_04JQ-BT" package="JST_04JQ-BT">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-2" pin="1" pad="2"/>
<connect gate="-3" pin="1" pad="3"/>
<connect gate="-4" pin="1" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414218/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST_04JQ-ST" package="JST_04JQ-ST">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-2" pin="1" pad="2"/>
<connect gate="-3" pin="1" pad="3"/>
<connect gate="-4" pin="1" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414219/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST_B4B-XH-A" package="JST_B4B-XH-A">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-2" pin="1" pad="2"/>
<connect gate="-3" pin="1" pad="3"/>
<connect gate="-4" pin="1" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414220/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST_S4B-XH-A-1" package="JST_S4B-XH-A-1">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-2" pin="1" pad="2"/>
<connect gate="-3" pin="1" pad="3"/>
<connect gate="-4" pin="1" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414221/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST_B04B-PASK" package="JST_B04B-PASK">
<connects>
<connect gate="-1" pin="1" pad="P$1"/>
<connect gate="-2" pin="1" pad="P$2"/>
<connect gate="-3" pin="1" pad="P$3"/>
<connect gate="-4" pin="1" pad="P$4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414269/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST_S04B-PASK-2" package="JST_S04B-PASK-2">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-2" pin="1" pad="2"/>
<connect gate="-3" pin="1" pad="3"/>
<connect gate="-4" pin="1" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414270/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST_B4B-PK-K-S" package="JST_B4B-PH-K-S">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-2" pin="1" pad="2"/>
<connect gate="-3" pin="1" pad="3"/>
<connect gate="-4" pin="1" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414302/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST_S4B-PH-K-S" package="JST_S4B-PH-K-S">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-2" pin="1" pad="2"/>
<connect gate="-3" pin="1" pad="3"/>
<connect gate="-4" pin="1" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414303/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST_B04B-PSILE-A1_B04B-PSIY-B1_B04B-PSIR-C1" package="JST_B04B-PSILE-A1_B04B-PSIY-B1_B04B-PSIR-C1">
<connects>
<connect gate="-1" pin="1" pad="P$1"/>
<connect gate="-2" pin="1" pad="P$2"/>
<connect gate="-3" pin="1" pad="P$3"/>
<connect gate="-4" pin="1" pad="P$4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414334/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST_B4PS-VH" package="JST_B4PS-VH">
<connects>
<connect gate="-1" pin="1" pad="P$1"/>
<connect gate="-2" pin="1" pad="P$2"/>
<connect gate="-3" pin="1" pad="P$3"/>
<connect gate="-4" pin="1" pad="P$4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414350/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST_B4P-VH" package="JST_B4P-VH">
<connects>
<connect gate="-1" pin="1" pad="P$1"/>
<connect gate="-2" pin="1" pad="P$2"/>
<connect gate="-3" pin="1" pad="P$3"/>
<connect gate="-4" pin="1" pad="P$4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414351/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST_B4P-VH-FB-B" package="JST_B4P-VH-FB-B">
<connects>
<connect gate="-1" pin="1" pad="P$1"/>
<connect gate="-2" pin="1" pad="P$2"/>
<connect gate="-3" pin="1" pad="P$3"/>
<connect gate="-4" pin="1" pad="P$4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414352/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST_B04B-ESK-1D" package="JST_B04B-ZESK-1D">
<connects>
<connect gate="-1" pin="1" pad="P$1"/>
<connect gate="-2" pin="1" pad="P$2"/>
<connect gate="-3" pin="1" pad="P$3"/>
<connect gate="-4" pin="1" pad="P$4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414376/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST_S04B-ZESK-2D" package="JST_S04B-ZESK-2D">
<connects>
<connect gate="-1" pin="1" pad="P$1"/>
<connect gate="-2" pin="1" pad="P$2"/>
<connect gate="-3" pin="1" pad="P$3"/>
<connect gate="-4" pin="1" pad="P$4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414377/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TSW-104-08-F-S-RA" package="HDRRA4W64P254_1X4_1016X254X254B">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-2" pin="1" pad="2"/>
<connect gate="-3" pin="1" pad="3"/>
<connect gate="-4" pin="1" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15597948/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="DW-04-09-F-S-512" package="HDRV4W64P254_1X4_1016X508X1829B">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-2" pin="1" pad="2"/>
<connect gate="-3" pin="1" pad="3"/>
<connect gate="-4" pin="1" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15735943/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
<class number="1" name="RF" width="0.237490625" drill="0.254">
<clearance class="0" value="0.508"/>
<clearance class="1" value="0.508"/>
</class>
</classes>
<parts>
<part name="U1" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TI_SN65HVD62RGTR" device="16-VFQFN_EXPOSED_PAD" package3d_urn="urn:adsk.eagle:package:3986130/2" value="SN65HVD63RGTT"/>
<part name="C11" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0603" package3d_urn="urn:adsk.eagle:package:3793126/2" value="39pF"/>
<part name="C10" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0603" package3d_urn="urn:adsk.eagle:package:3793126/2" value="39pF"/>
<part name="R9" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0603" package3d_urn="urn:adsk.eagle:package:3811166/1" value="49.9"/>
<part name="C2" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0603" package3d_urn="urn:adsk.eagle:package:3793126/2" value="470pF"/>
<part name="C7" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0805" package3d_urn="urn:adsk.eagle:package:3793139/1" value="0.22uF 5% , 100V"/>
<part name="R4" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0603" package3d_urn="urn:adsk.eagle:package:3811166/1" value="4.12K"/>
<part name="R1" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0603" package3d_urn="urn:adsk.eagle:package:3811166/1" value="10K"/>
<part name="SUPPLY2" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="R6" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0603" package3d_urn="urn:adsk.eagle:package:3811166/1" value="0"/>
<part name="R5" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0603" package3d_urn="urn:adsk.eagle:package:3811166/1" value="0"/>
<part name="R13" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0603" package3d_urn="urn:adsk.eagle:package:3811166/1" value="120"/>
<part name="D2" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="DIODE_TVS_BI-DIRECTIONAL" device="DO214AB_SMC" package3d_urn="urn:adsk.eagle:package:15725844/1" value="SM15T10CA"/>
<part name="D3" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="DIODE_TVS_BI-DIRECTIONAL" device="DO214AB_SMC" package3d_urn="urn:adsk.eagle:package:15725844/1" value="SM15T10CA"/>
<part name="P+1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="P+2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="P+3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="R7" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0603" package3d_urn="urn:adsk.eagle:package:3811166/1" value="0"/>
<part name="R8" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0603" package3d_urn="urn:adsk.eagle:package:3811166/1" value="0"/>
<part name="R11" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0603" package3d_urn="urn:adsk.eagle:package:3811166/1" value="0"/>
<part name="SUPPLY4" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="U2" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TRANSCEIVER_RS485" device="8-SOIC" package3d_urn="urn:adsk.eagle:package:3833977/2" value="SN65176BDR"/>
<part name="R15" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0603" package3d_urn="urn:adsk.eagle:package:3811166/1" value="DNP"/>
<part name="C5" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0603" package3d_urn="urn:adsk.eagle:package:3793126/2" value="0.1uF"/>
<part name="R14" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0603" package3d_urn="urn:adsk.eagle:package:3811166/1" value="DNP"/>
<part name="C3" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0603" package3d_urn="urn:adsk.eagle:package:3793126/2" value="0.1uF"/>
<part name="C8" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0603" package3d_urn="urn:adsk.eagle:package:3793126/2" value="0.1uF"/>
<part name="C6" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0603" package3d_urn="urn:adsk.eagle:package:3793126/2" value="1uF"/>
<part name="C4" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0603" package3d_urn="urn:adsk.eagle:package:3793126/2" value="1uF"/>
<part name="C9" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0603" package3d_urn="urn:adsk.eagle:package:3793126/2" value="1uF"/>
<part name="F102" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="GDT_2POS" device="BOURNS_2029-XX-SMLF" package3d_urn="urn:adsk.eagle:package:3890106/3" value="B88069X1630T602"/>
<part name="F101" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="GDT_2POS" device="BOURNS_2029-XX-SMLF" package3d_urn="urn:adsk.eagle:package:3890106/3" value="B88069X1630T602"/>
<part name="F5" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="GDT_2POS" device="BOURNS_2029-XX-SMLF" package3d_urn="urn:adsk.eagle:package:3890106/3" value="B88069X1630T602"/>
<part name="F2" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="GDT_2POS" device="BOURNS_2029-XX-SMLF" package3d_urn="urn:adsk.eagle:package:3890106/3" value="B88069X1630T602"/>
<part name="C101" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0805" package3d_urn="urn:adsk.eagle:package:3793139/1" value="47pF, 100V"/>
<part name="C1" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0603" package3d_urn="urn:adsk.eagle:package:3793126/2" value="1uF"/>
<part name="R2" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0603" package3d_urn="urn:adsk.eagle:package:3811166/1" value="DNP"/>
<part name="R3" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0603" package3d_urn="urn:adsk.eagle:package:3811166/1" value="DNP"/>
<part name="R12" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0603" package3d_urn="urn:adsk.eagle:package:3811166/1" value="DNP"/>
<part name="TP2" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TP_CLASS_B" device="CIR_PAD_1MM" package3d_urn="urn:adsk.eagle:package:8414067/2" value="TP_OOK"/>
<part name="TP4" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TP_CLASS_B" device="CIR_PAD_1MM" package3d_urn="urn:adsk.eagle:package:8414067/2" value="TP_OOK_OUT"/>
<part name="DP" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TP_CLASS_B" device="CIR_PAD_1MM" package3d_urn="urn:adsk.eagle:package:8414067/2" value="TP_DN"/>
<part name="DN" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TP_CLASS_B" device="CIR_PAD_1MM" package3d_urn="urn:adsk.eagle:package:8414067/2" value="TP_DP"/>
<part name="TP5" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TP_CLASS_B" device="CIR_PAD_1MM" package3d_urn="urn:adsk.eagle:package:8414067/2" value="TP_RX"/>
<part name="TP1" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TP_CLASS_B" device="CIR_PAD_1MM" package3d_urn="urn:adsk.eagle:package:8414067/2" value="TP_DIR"/>
<part name="TP3" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TP_CLASS_B" device="CIR_PAD_1MM" package3d_urn="urn:adsk.eagle:package:8414067/2" value="TP_TX"/>
<part name="F1" library="JMA_LBR" library_urn="urn:adsk.eagle:library:15620506" deviceset="0.5MM_FIDUCIAL" device="" package3d_urn="urn:adsk.eagle:package:15620691/1" override_package3d_urn="urn:adsk.eagle:package:15620691/10" override_package_urn="urn:adsk.eagle:footprint:15620619/1"/>
<part name="F4" library="JMA_LBR" library_urn="urn:adsk.eagle:library:15620506" deviceset="0.5MM_FIDUCIAL" device="" package3d_urn="urn:adsk.eagle:package:15620691/1" override_package3d_urn="urn:adsk.eagle:package:15620691/12" override_package_urn="urn:adsk.eagle:footprint:15620619/1"/>
<part name="F104" library="JMA_LBR" library_urn="urn:adsk.eagle:library:15620506" deviceset="0.5MM_FIDUCIAL" device="" package3d_urn="urn:adsk.eagle:package:15620691/1" override_package3d_urn="urn:adsk.eagle:package:15620691/15" override_package_urn="urn:adsk.eagle:footprint:15620619/1"/>
<part name="F100" library="JMA_LBR" library_urn="urn:adsk.eagle:library:15620506" deviceset="0.5MM_FIDUCIAL" device="" package3d_urn="urn:adsk.eagle:package:15620691/1" override_package3d_urn="urn:adsk.eagle:package:15620691/13" override_package_urn="urn:adsk.eagle:footprint:15620619/1"/>
<part name="F103" library="JMA_LBR" library_urn="urn:adsk.eagle:library:15620506" deviceset="0.5MM_FIDUCIAL" device="" package3d_urn="urn:adsk.eagle:package:15620691/1" override_package3d_urn="urn:adsk.eagle:package:15620691/14" override_package_urn="urn:adsk.eagle:footprint:15620619/1"/>
<part name="F3" library="JMA_LBR" library_urn="urn:adsk.eagle:library:15620506" deviceset="0.5MM_FIDUCIAL" device="" package3d_urn="urn:adsk.eagle:package:15620691/1" override_package3d_urn="urn:adsk.eagle:package:15620691/11" override_package_urn="urn:adsk.eagle:footprint:15620619/1"/>
<part name="C102" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0603" package3d_urn="urn:adsk.eagle:package:3793126/2" value="DNP"/>
<part name="SUPPLY1" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="U$2" library="frames" library_urn="urn:adsk.eagle:library:229" deviceset="A3_LOC_JMA_RELEASE" device=""/>
<part name="J100" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TRANSITION_CLIP" device="TRANSITION_CLIP_.141" package3d_urn="urn:adsk.eagle:package:8414403/4" value="415-0029-mm750"/>
<part name="C109" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0603" package3d_urn="urn:adsk.eagle:package:3793126/2" value="10uF"/>
<part name="C110" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0805" package3d_urn="urn:adsk.eagle:package:3793139/1" value="22uF"/>
<part name="C111" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0805" package3d_urn="urn:adsk.eagle:package:3793139/1" value="22uF"/>
<part name="R111" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0603" package3d_urn="urn:adsk.eagle:package:3811166/1" value="348"/>
<part name="R109" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0603" package3d_urn="urn:adsk.eagle:package:3811166/1" value="1K"/>
<part name="SUPPLY6" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY7" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="C112" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0603" package3d_urn="urn:adsk.eagle:package:3793126/2" value="1uF"/>
<part name="R115" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0603" package3d_urn="urn:adsk.eagle:package:3811166/1" value="DNP"/>
<part name="P+5" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="SUPPLY8" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="FB100" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="FERRITE_BEAD" device="0603" package3d_urn="urn:adsk.eagle:package:3887236/1" value="MPZ1608S300ATAH0"/>
<part name="C105" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0603" package3d_urn="urn:adsk.eagle:package:3793126/2" value="DNP"/>
<part name="C104" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0805" package3d_urn="urn:adsk.eagle:package:3793139/1" value="3.3uF, 50V"/>
<part name="L200" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="INDUCTOR" device="5.00MM_X_5.00MM_SMT" package3d_urn="urn:adsk.eagle:package:3890169/1" value="SRN5040-220M"/>
<part name="Y1" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CRYSTAL_2-POS" device="HC-49/US" package3d_urn="urn:adsk.eagle:package:3837785/1" value="E1SEA40-8.704M TR"/>
<part name="C106" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="10nF"/>
<part name="R114" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="3"/>
<part name="R112" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="5.1"/>
<part name="C108" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="330pF"/>
<part name="U100" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TI_TPS2491DGSR" device="10-TFSOP" package3d_urn="urn:adsk.eagle:package:3921298/1" value="TPS2491DGSR"/>
<part name="R104" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:3811169/1" value="0.2"/>
<part name="R102" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:3811169/1" value="0"/>
<part name="R103" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:3811169/1" value="DNP"/>
<part name="Q100" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="MOSFET_NCH" device="TO-252-3_DPAK" package3d_urn="urn:adsk.eagle:package:3920001/2" value="BUK72150-55A,118"/>
<part name="R110" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0603" package3d_urn="urn:adsk.eagle:package:3811166/1" value="10K"/>
<part name="R113" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0603" package3d_urn="urn:adsk.eagle:package:3811166/1" value="8.87K"/>
<part name="R106" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0603" package3d_urn="urn:adsk.eagle:package:3811166/1" value="56K"/>
<part name="R107" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0603" package3d_urn="urn:adsk.eagle:package:3811166/1" value="10K"/>
<part name="R101" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0603" package3d_urn="urn:adsk.eagle:package:3811166/1" value="10"/>
<part name="R105" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:3811169/1" value="470K"/>
<part name="R100" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0603" package3d_urn="urn:adsk.eagle:package:3811166/1" value="1K"/>
<part name="C100" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0805" package3d_urn="urn:adsk.eagle:package:3793139/1" value="22nF"/>
<part name="C103" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0805" package3d_urn="urn:adsk.eagle:package:3793139/1" value="DNP"/>
<part name="C107" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0603" package3d_urn="urn:adsk.eagle:package:3793126/2" value="6.8nF"/>
<part name="SUPPLY11" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="R108" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0603" package3d_urn="urn:adsk.eagle:package:3811166/1" value="DNP"/>
<part name="R10" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0603" package3d_urn="urn:adsk.eagle:package:3811166/1" value="20"/>
<part name="L100" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="INDUCTOR" device="5.00MM_X_5.00MM_SMT" package3d_urn="urn:adsk.eagle:package:3890169/1" value="SRN5040-100M"/>
<part name="D102" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="DIODE_ZENER" device="DO214AB" package3d_urn="urn:adsk.eagle:package:15618120/3" value="SMCJ33A-13-F"/>
<part name="D101" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="DIODE_SCHOTTKY" device="DO214AC_SMA" package3d_urn="urn:adsk.eagle:package:15671034/2" value="CDBA140-G"/>
<part name="U101" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TI_LM22674MRE-ADJ/NOPBTR-ND" device="8-SOPOWERPAD" package3d_urn="urn:adsk.eagle:package:15672442/2" value="TI_LM22674MRE-ADJ/NOPBTR-ND8-SOPOWERPAD"/>
<part name="D1" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="DIODE_TVS_UNI-DIRECTIONAL" device="DIO214AB_SMC_TVS" package3d_urn="urn:adsk.eagle:package:15726434/1" value="SM15T6V8A"/>
<part name="D100" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="DIODE_TVS_UNI-DIRECTIONAL" device="DIO214AB_SMC_TVS" package3d_urn="urn:adsk.eagle:package:15726434/1" value="1.5SMC480A"/>
<part name="J1" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CONNECTOR_4POS" device="DW-04-09-F-S-512" package3d_urn="urn:adsk.eagle:package:15735943/2" value="DW-04-09-F-S-512"/>
</parts>
<sheets>
<sheet>
<plain>
<text x="7.62" y="208.788" size="1.778" layer="95" ratio="15">Modulated RF Input</text>
<text x="312.42" y="17.78" size="1.778" layer="94">AISG 3.0 Modem</text>
<text x="370.84" y="10.16" size="1.778" layer="94">01</text>
<text x="281.94" y="228.6" size="1.778" layer="94">01</text>
<text x="289.56" y="228.6" size="1.778" layer="94">12/28/17</text>
<text x="317.5" y="228.6" size="1.778" layer="94">AISG 3.0 Modem Initial Release</text>
<text x="353.06" y="124.46" size="1.778" layer="95">10V - 30V</text>
<text x="353.06" y="99.06" size="1.778" layer="95">DATA_P</text>
<text x="353.06" y="66.04" size="1.778" layer="95">DATA_N</text>
<text x="353.06" y="50.8" size="1.778" layer="95">GND</text>
</plain>
<instances>
<instance part="U1" gate="G$1" x="139.7" y="93.98" smashed="yes" rot="MR0">
<attribute name="NAME" x="139.7" y="99.06" size="1.778" layer="95" ratio="15" rot="SMR0"/>
<attribute name="VALUE" x="139.7" y="96.774" size="1.778" layer="96" ratio="15" rot="SMR0"/>
</instance>
<instance part="C11" gate="G$1" x="53.34" y="91.44" smashed="yes" rot="MR180">
<attribute name="NAME" x="52.07" y="91.948" size="1.778" layer="95" ratio="15" rot="MR0"/>
<attribute name="VALUE" x="54.61" y="90.932" size="1.778" layer="96" ratio="15" rot="MR180"/>
</instance>
<instance part="C10" gate="G$1" x="53.34" y="81.28" smashed="yes" rot="MR180">
<attribute name="NAME" x="52.07" y="81.788" size="1.778" layer="95" ratio="15" rot="MR0"/>
<attribute name="VALUE" x="54.61" y="80.772" size="1.778" layer="96" ratio="15" rot="MR180"/>
</instance>
<instance part="R9" gate="G$1" x="86.36" y="78.74" smashed="yes">
<attribute name="NAME" x="81.28" y="78.994" size="1.778" layer="95" ratio="15"/>
<attribute name="VALUE" x="88.9" y="78.994" size="1.778" layer="96" ratio="15"/>
</instance>
<instance part="C2" gate="G$1" x="50.8" y="66.04" smashed="yes" rot="MR90">
<attribute name="NAME" x="50.546" y="69.342" size="1.778" layer="95" ratio="15" rot="MR0"/>
<attribute name="VALUE" x="50.546" y="61.468" size="1.778" layer="96" ratio="15" rot="MR0"/>
</instance>
<instance part="C7" gate="G$1" x="35.56" y="104.14" smashed="yes" rot="R90">
<attribute name="NAME" x="35.306" y="109.22" size="1.778" layer="95" ratio="15" rot="R180"/>
<attribute name="VALUE" x="35.306" y="100.838" size="1.778" layer="96" ratio="15" rot="R180"/>
</instance>
<instance part="R4" gate="G$1" x="86.36" y="73.66" smashed="yes">
<attribute name="NAME" x="80.264" y="73.914" size="1.778" layer="95" ratio="15"/>
<attribute name="VALUE" x="88.9" y="73.914" size="1.778" layer="96" ratio="15"/>
</instance>
<instance part="R1" gate="G$1" x="78.74" y="63.5" smashed="yes" rot="R90">
<attribute name="NAME" x="78.486" y="68.58" size="1.778" layer="95" ratio="15" rot="R180"/>
<attribute name="VALUE" x="78.486" y="60.198" size="1.778" layer="96" ratio="15" rot="R180"/>
</instance>
<instance part="SUPPLY2" gate="GND" x="190.5" y="48.26" smashed="yes">
<attribute name="VALUE" x="190.246" y="50.292" size="1.778" layer="96" ratio="15" rot="R180"/>
</instance>
<instance part="R6" gate="G$1" x="182.88" y="78.74" smashed="yes">
<attribute name="NAME" x="176.022" y="78.994" size="1.778" layer="95" ratio="15"/>
<attribute name="VALUE" x="187.198" y="78.994" size="1.778" layer="96" ratio="15"/>
</instance>
<instance part="R5" gate="G$1" x="182.88" y="76.2" smashed="yes">
<attribute name="NAME" x="176.022" y="76.454" size="1.778" layer="95" ratio="15"/>
<attribute name="VALUE" x="187.198" y="76.454" size="1.778" layer="96" ratio="15"/>
</instance>
<instance part="R13" gate="G$1" x="261.62" y="83.82" smashed="yes" rot="R90">
<attribute name="NAME" x="261.366" y="88.9" size="1.778" layer="95" ratio="15" rot="R180"/>
<attribute name="VALUE" x="261.366" y="80.518" size="1.778" layer="96" ratio="15" rot="R180"/>
</instance>
<instance part="D2" gate="G$1" x="276.86" y="76.2" smashed="yes" rot="R90">
<attribute name="NAME" x="276.606" y="81.28" size="1.778" layer="95" ratio="15" rot="R180"/>
<attribute name="VALUE" x="276.606" y="72.898" size="1.778" layer="96" ratio="15" rot="R180"/>
</instance>
<instance part="D3" gate="G$1" x="276.86" y="91.44" smashed="yes" rot="R270">
<attribute name="NAME" x="276.606" y="96.52" size="1.778" layer="95" ratio="15" rot="R180"/>
<attribute name="VALUE" x="276.606" y="88.138" size="1.778" layer="96" ratio="15" rot="R180"/>
</instance>
<instance part="P+1" gate="1" x="96.52" y="104.14" smashed="yes">
<attribute name="VALUE" x="99.568" y="105.918" size="1.778" layer="96" ratio="15" rot="R180"/>
</instance>
<instance part="P+2" gate="1" x="160.02" y="109.22" smashed="yes">
<attribute name="VALUE" x="160.02" y="109.22" size="1.778" layer="96" ratio="15"/>
</instance>
<instance part="P+3" gate="1" x="248.92" y="119.38" smashed="yes">
<attribute name="VALUE" x="248.92" y="119.38" size="1.778" layer="96" ratio="15"/>
</instance>
<instance part="R7" gate="G$1" x="182.88" y="81.28" smashed="yes">
<attribute name="NAME" x="176.022" y="81.534" size="1.778" layer="95" ratio="15"/>
<attribute name="VALUE" x="187.198" y="81.534" size="1.778" layer="96" ratio="15"/>
</instance>
<instance part="R8" gate="G$1" x="182.88" y="83.82" smashed="yes">
<attribute name="NAME" x="176.022" y="84.074" size="1.778" layer="95" ratio="15"/>
<attribute name="VALUE" x="187.198" y="84.074" size="1.778" layer="96" ratio="15"/>
</instance>
<instance part="R11" gate="G$1" x="182.88" y="88.9" smashed="yes">
<attribute name="NAME" x="176.276" y="89.154" size="1.778" layer="95" ratio="15"/>
<attribute name="VALUE" x="187.198" y="89.154" size="1.778" layer="96" ratio="15"/>
</instance>
<instance part="SUPPLY4" gate="GND" x="233.68" y="48.26" smashed="yes">
<attribute name="VALUE" x="233.426" y="50.292" size="1.778" layer="96" ratio="15" rot="R180"/>
</instance>
<instance part="U2" gate="G$1" x="208.28" y="96.52" smashed="yes">
<attribute name="NAME" x="210.82" y="99.06" size="1.778" layer="95" ratio="15"/>
<attribute name="VALUE" x="210.82" y="97.028" size="1.778" layer="96" ratio="15"/>
</instance>
<instance part="R15" gate="G$1" x="256.54" y="106.68" smashed="yes" rot="R270">
<attribute name="NAME" x="256.286" y="111.76" size="1.778" layer="95" ratio="15" rot="R180"/>
<attribute name="VALUE" x="256.286" y="103.378" size="1.778" layer="96" ratio="15" rot="R180"/>
</instance>
<instance part="C5" gate="G$1" x="96.52" y="60.96" smashed="yes" rot="MR90">
<attribute name="NAME" x="96.266" y="64.262" size="1.778" layer="95" ratio="15" rot="MR0"/>
<attribute name="VALUE" x="96.266" y="55.88" size="1.778" layer="96" ratio="15" rot="MR0"/>
</instance>
<instance part="R14" gate="G$1" x="256.54" y="58.42" smashed="yes" rot="R270">
<attribute name="NAME" x="256.54" y="63.5" size="1.778" layer="95" ratio="15" rot="R180"/>
<attribute name="VALUE" x="256.286" y="55.118" size="1.778" layer="96" ratio="15" rot="R180"/>
</instance>
<instance part="C3" gate="G$1" x="160.02" y="60.96" smashed="yes" rot="MR90">
<attribute name="NAME" x="159.766" y="64.262" size="1.778" layer="95" ratio="15" rot="MR0"/>
<attribute name="VALUE" x="159.766" y="56.134" size="1.778" layer="96" ratio="15" rot="MR0"/>
</instance>
<instance part="C8" gate="G$1" x="248.92" y="60.96" smashed="yes" rot="MR90">
<attribute name="NAME" x="248.666" y="64.262" size="1.778" layer="95" ratio="15" rot="MR0"/>
<attribute name="VALUE" x="248.666" y="56.134" size="1.778" layer="96" ratio="15" rot="MR0"/>
</instance>
<instance part="C6" gate="G$1" x="86.36" y="60.96" smashed="yes" rot="MR90">
<attribute name="NAME" x="86.106" y="64.262" size="1.778" layer="95" ratio="15" rot="MR0"/>
<attribute name="VALUE" x="86.106" y="55.88" size="1.778" layer="96" ratio="15" rot="MR0"/>
</instance>
<instance part="C4" gate="G$1" x="152.4" y="60.96" smashed="yes" rot="MR90">
<attribute name="NAME" x="152.146" y="64.262" size="1.778" layer="95" ratio="15" rot="MR0"/>
<attribute name="VALUE" x="152.146" y="56.134" size="1.778" layer="96" ratio="15" rot="MR0"/>
</instance>
<instance part="C9" gate="G$1" x="238.76" y="60.96" smashed="yes" rot="MR90">
<attribute name="NAME" x="238.506" y="64.262" size="1.778" layer="95" ratio="15" rot="MR0"/>
<attribute name="VALUE" x="238.506" y="56.134" size="1.778" layer="96" ratio="15" rot="MR0"/>
</instance>
<instance part="F102" gate="G$1" x="38.1" y="190.5" smashed="yes">
<attribute name="NAME" x="37.846" y="196.596" size="1.778" layer="95" ratio="15" rot="R180"/>
<attribute name="VALUE" x="37.846" y="186.182" size="1.778" layer="96" ratio="15" rot="R180"/>
</instance>
<instance part="F101" gate="G$1" x="304.8" y="83.82" smashed="yes">
<attribute name="NAME" x="304.546" y="89.662" size="1.778" layer="95" ratio="15" rot="R180"/>
<attribute name="VALUE" x="304.546" y="79.502" size="1.778" layer="96" ratio="15" rot="R180"/>
</instance>
<instance part="F5" gate="G$1" x="330.2" y="88.9" smashed="yes">
<attribute name="NAME" x="329.946" y="94.996" size="1.778" layer="95" ratio="15" rot="R180"/>
<attribute name="VALUE" x="329.946" y="84.582" size="1.778" layer="96" ratio="15" rot="R180"/>
</instance>
<instance part="F2" gate="G$1" x="330.2" y="73.66" smashed="yes" rot="R180">
<attribute name="NAME" x="329.946" y="79.756" size="1.778" layer="95" ratio="15" rot="R180"/>
<attribute name="VALUE" x="329.946" y="69.596" size="1.778" layer="96" ratio="15" rot="R180"/>
</instance>
<instance part="C101" gate="G$1" x="53.34" y="190.5" smashed="yes" rot="R90">
<attribute name="NAME" x="53.086" y="193.802" size="1.778" layer="95" ratio="15" align="bottom-right"/>
<attribute name="VALUE" x="53.086" y="187.198" size="1.778" layer="96" ratio="15" rot="R180"/>
</instance>
<instance part="C1" gate="G$1" x="104.14" y="60.96" smashed="yes" rot="MR90">
<attribute name="NAME" x="103.886" y="64.262" size="1.778" layer="95" ratio="15" rot="MR0"/>
<attribute name="VALUE" x="103.886" y="55.88" size="1.778" layer="96" ratio="15" rot="MR0"/>
</instance>
<instance part="R2" gate="G$1" x="165.1" y="96.52" smashed="yes" rot="R90">
<attribute name="NAME" x="164.846" y="101.6" size="1.778" layer="95" ratio="15" rot="R180"/>
<attribute name="VALUE" x="164.846" y="93.218" size="1.778" layer="96" ratio="15" rot="R180"/>
</instance>
<instance part="R3" gate="G$1" x="172.72" y="96.52" smashed="yes" rot="R90">
<attribute name="NAME" x="172.466" y="101.6" size="1.778" layer="95" ratio="15" rot="R180"/>
<attribute name="VALUE" x="172.466" y="93.218" size="1.778" layer="96" ratio="15" rot="R180"/>
</instance>
<instance part="R12" gate="G$1" x="147.32" y="60.96" smashed="yes" rot="R90">
<attribute name="NAME" x="147.066" y="66.04" size="1.778" layer="95" ratio="15" rot="R180"/>
<attribute name="VALUE" x="147.066" y="57.658" size="1.778" layer="96" ratio="15" rot="R180"/>
</instance>
<instance part="TP2" gate="G$1" x="33.02" y="83.82" smashed="yes" rot="R90">
<attribute name="NAME" x="33.02" y="82.55" size="1.778" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="TP4" gate="G$1" x="101.6" y="93.98" smashed="yes">
<attribute name="NAME" x="100.076" y="97.028" size="1.778" layer="95" ratio="15"/>
</instance>
<instance part="DP" gate="G$1" x="236.22" y="84.836" smashed="yes">
<attribute name="NAME" x="235.458" y="86.614" size="1.778" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="DN" gate="G$1" x="236.22" y="79.756" smashed="yes">
<attribute name="NAME" x="235.458" y="81.534" size="1.778" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="TP5" gate="G$1" x="203.2" y="89.916" smashed="yes">
<attribute name="NAME" x="202.438" y="91.694" size="1.778" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="TP1" gate="G$1" x="203.2" y="84.836" smashed="yes">
<attribute name="NAME" x="202.438" y="86.614" size="1.778" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="TP3" gate="G$1" x="203.2" y="74.422" smashed="yes">
<attribute name="NAME" x="202.438" y="76.2" size="1.778" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="F1" gate="G$1" x="15.24" y="20.32" smashed="yes">
<attribute name="NAME" x="15.24" y="27.178" size="1.778" layer="95"/>
</instance>
<instance part="F4" gate="G$1" x="30.48" y="20.32" smashed="yes">
<attribute name="NAME" x="30.48" y="27.178" size="1.778" layer="95"/>
</instance>
<instance part="F104" gate="G$1" x="45.72" y="20.32" smashed="yes">
<attribute name="NAME" x="45.72" y="27.178" size="1.778" layer="95"/>
</instance>
<instance part="F100" gate="G$1" x="60.96" y="20.32" smashed="yes">
<attribute name="NAME" x="60.96" y="27.178" size="1.778" layer="95"/>
</instance>
<instance part="F103" gate="G$1" x="76.2" y="20.32" smashed="yes">
<attribute name="NAME" x="76.2" y="27.178" size="1.778" layer="95"/>
</instance>
<instance part="F3" gate="G$1" x="91.44" y="20.32" smashed="yes">
<attribute name="NAME" x="91.44" y="27.178" size="1.778" layer="95"/>
</instance>
<instance part="C102" gate="G$1" x="78.74" y="190.5" smashed="yes" rot="MR90">
<attribute name="NAME" x="78.74" y="193.802" size="1.778" layer="95" ratio="15" rot="MR0"/>
<attribute name="VALUE" x="78.486" y="185.674" size="1.778" layer="96" ratio="15" rot="MR0"/>
</instance>
<instance part="SUPPLY1" gate="GND" x="205.74" y="165.1" smashed="yes">
<attribute name="VALUE" x="205.994" y="165.608" size="1.778" layer="96" ratio="15"/>
</instance>
<instance part="U$2" gate="G$1" x="0" y="0" smashed="yes">
<attribute name="DRAWING_NAME" x="317.5" y="10.16" size="1.778" layer="94"/>
<attribute name="SHEET" x="332.486" y="5.842" size="1.778" layer="94"/>
</instance>
<instance part="J100" gate="G$1" x="7.62" y="205.74" smashed="yes">
<attribute name="VALUE" x="7.62" y="211.074" size="1.778" layer="96" ratio="15"/>
<attribute name="NAME" x="7.62" y="213.106" size="1.778" layer="95" ratio="15"/>
</instance>
<instance part="C109" gate="G$1" x="332.74" y="198.12" smashed="yes" rot="R90">
<attribute name="NAME" x="332.486" y="203.2" size="1.778" layer="95" ratio="10" rot="R180"/>
<attribute name="VALUE" x="332.486" y="194.818" size="1.778" layer="96" ratio="10" rot="R180"/>
</instance>
<instance part="C110" gate="G$1" x="340.36" y="198.12" smashed="yes" rot="R90">
<attribute name="NAME" x="340.106" y="203.2" size="1.778" layer="95" ratio="10" rot="R180"/>
<attribute name="VALUE" x="340.106" y="194.818" size="1.778" layer="96" ratio="10" rot="R180"/>
</instance>
<instance part="C111" gate="G$1" x="347.98" y="198.12" smashed="yes" rot="R90">
<attribute name="NAME" x="347.726" y="203.2" size="1.778" layer="95" ratio="10" rot="R180"/>
<attribute name="VALUE" x="347.726" y="194.818" size="1.778" layer="96" ratio="10" rot="R180"/>
</instance>
<instance part="R111" gate="G$1" x="294.64" y="213.36" smashed="yes" rot="R180">
<attribute name="NAME" x="292.1" y="214.884" size="1.778" layer="95" ratio="15"/>
<attribute name="VALUE" x="293.116" y="210.058" size="1.778" layer="96" ratio="15"/>
</instance>
<instance part="R109" gate="G$1" x="312.42" y="213.36" smashed="yes">
<attribute name="NAME" x="309.88" y="214.884" size="1.778" layer="95" ratio="15"/>
<attribute name="VALUE" x="314.452" y="212.09" size="1.778" layer="96" ratio="15" rot="R180"/>
</instance>
<instance part="SUPPLY6" gate="GND" x="287.02" y="208.28" smashed="yes">
<attribute name="VALUE" x="286.766" y="210.312" size="1.778" layer="96" ratio="15" rot="R180"/>
</instance>
<instance part="SUPPLY7" gate="GND" x="322.58" y="165.1" smashed="yes">
<attribute name="VALUE" x="322.58" y="166.878" size="1.778" layer="96" ratio="15" rot="R180"/>
</instance>
<instance part="C112" gate="G$1" x="231.14" y="182.88" smashed="yes" rot="R90">
<attribute name="NAME" x="230.886" y="187.96" size="1.778" layer="95" ratio="10" rot="R180"/>
<attribute name="VALUE" x="230.886" y="179.578" size="1.778" layer="96" ratio="10" rot="R180"/>
</instance>
<instance part="R115" gate="G$1" x="223.52" y="185.42" smashed="yes" rot="R90">
<attribute name="NAME" x="223.266" y="190.5" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="223.266" y="182.118" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="P+5" gate="1" x="373.38" y="218.44" smashed="yes">
<attribute name="VALUE" x="368.3" y="218.44" size="1.778" layer="96" ratio="15"/>
</instance>
<instance part="SUPPLY8" gate="GND" x="48.26" y="165.1" smashed="yes">
<attribute name="VALUE" x="48.514" y="165.608" size="1.778" layer="96" ratio="15"/>
</instance>
<instance part="FB100" gate="G$1" x="358.14" y="205.74" smashed="yes">
<attribute name="NAME" x="355.6" y="209.55" size="1.778" layer="95" ratio="15"/>
<attribute name="VALUE" x="350.52" y="203.2" size="1.778" layer="96" ratio="15"/>
</instance>
<instance part="C105" gate="G$1" x="213.36" y="187.96" smashed="yes" rot="R90">
<attribute name="NAME" x="213.106" y="193.04" size="1.778" layer="95" ratio="10" rot="R180"/>
<attribute name="VALUE" x="213.106" y="184.658" size="1.778" layer="96" ratio="10" rot="R180"/>
</instance>
<instance part="C104" gate="G$1" x="187.96" y="187.96" smashed="yes" rot="R90">
<attribute name="NAME" x="187.706" y="192.786" size="1.778" layer="95" ratio="10" rot="R180"/>
<attribute name="VALUE" x="187.706" y="184.658" size="1.778" layer="96" ratio="10" rot="R180"/>
</instance>
<instance part="L200" gate="G$1" x="322.58" y="205.74" smashed="yes">
<attribute name="NAME" x="320.04" y="208.788" size="1.778" layer="95" ratio="15"/>
<attribute name="VALUE" x="315.214" y="206.756" size="1.778" layer="96" ratio="15"/>
</instance>
<instance part="Y1" gate="G$1" x="81.28" y="86.36" smashed="yes" rot="R90">
<attribute name="NAME" x="81.28" y="89.662" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="81.28" y="84.836" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="C106" gate="G$1" x="287.02" y="203.2" smashed="yes">
<attribute name="NAME" x="280.162" y="203.454" size="1.778" layer="95" ratio="10"/>
<attribute name="VALUE" x="287.782" y="203.454" size="1.778" layer="96" ratio="10"/>
</instance>
<instance part="R114" gate="G$1" x="274.32" y="203.2" smashed="yes">
<attribute name="NAME" x="265.938" y="203.4286" size="1.778" layer="95"/>
<attribute name="VALUE" x="277.114" y="203.454" size="1.778" layer="96"/>
</instance>
<instance part="R112" gate="G$1" x="294.64" y="190.5" smashed="yes" rot="R90">
<attribute name="NAME" x="294.386" y="195.3514" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="294.386" y="186.69" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="C108" gate="G$1" x="294.64" y="177.8" smashed="yes" rot="R90">
<attribute name="NAME" x="294.386" y="182.88" size="1.778" layer="95" ratio="10" rot="R180"/>
<attribute name="VALUE" x="294.386" y="174.752" size="1.778" layer="96" ratio="10" rot="R180"/>
</instance>
<instance part="U100" gate="G$1" x="124.46" y="177.8" smashed="yes">
<attribute name="NAME" x="119.38" y="175.26" size="1.778" layer="95" ratio="15" rot="R180"/>
<attribute name="VALUE" x="119.38" y="172.72" size="1.778" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="R104" gate="G$1" x="114.3" y="205.74" smashed="yes">
<attribute name="NAME" x="116.84" y="209.296" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="116.84" y="203.962" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R102" gate="G$1" x="124.46" y="193.04" smashed="yes" rot="R90">
<attribute name="NAME" x="123.19" y="196.6214" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="123.19" y="191.262" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R103" gate="G$1" x="114.3" y="185.42" smashed="yes" rot="R180">
<attribute name="NAME" x="116.078" y="189.0014" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="116.332" y="183.642" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="Q100" gate="G$1" x="132.08" y="203.2" smashed="yes" rot="R90">
<attribute name="NAME" x="129.286" y="210.82" size="1.778" layer="95" ratio="15"/>
<attribute name="VALUE" x="129.032" y="208.28" size="1.778" layer="96" ratio="15"/>
</instance>
<instance part="R110" gate="G$1" x="119.38" y="162.56" smashed="yes" rot="R90">
<attribute name="NAME" x="118.11" y="166.1414" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="118.11" y="160.782" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R113" gate="G$1" x="119.38" y="147.32" smashed="yes" rot="R90">
<attribute name="NAME" x="118.11" y="150.9014" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="118.11" y="145.542" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R106" gate="G$1" x="99.06" y="193.04" smashed="yes" rot="R90">
<attribute name="NAME" x="97.79" y="196.6214" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="97.79" y="191.262" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R107" gate="G$1" x="99.06" y="162.56" smashed="yes" rot="R90">
<attribute name="NAME" x="97.79" y="166.1414" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="97.79" y="160.782" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R101" gate="G$1" x="134.62" y="193.04" smashed="yes" rot="R90">
<attribute name="NAME" x="133.35" y="196.6214" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="133.35" y="191.262" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R105" gate="G$1" x="149.86" y="193.04" smashed="yes" rot="R90">
<attribute name="NAME" x="148.59" y="196.6214" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="148.59" y="191.262" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R100" gate="G$1" x="152.4" y="162.56" smashed="yes" rot="R90">
<attribute name="NAME" x="151.13" y="166.1414" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="151.13" y="160.782" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="C100" gate="G$1" x="152.4" y="147.32" smashed="yes" rot="R90">
<attribute name="NAME" x="151.13" y="151.13" size="1.778" layer="95" ratio="10" rot="R180"/>
<attribute name="VALUE" x="151.13" y="144.78" size="1.778" layer="96" ratio="10" rot="R180"/>
</instance>
<instance part="C103" gate="G$1" x="200.66" y="187.96" smashed="yes" rot="R90">
<attribute name="NAME" x="200.406" y="192.786" size="1.778" layer="95" ratio="10" rot="R180"/>
<attribute name="VALUE" x="200.406" y="184.658" size="1.778" layer="96" ratio="10" rot="R180"/>
</instance>
<instance part="C107" gate="G$1" x="134.62" y="157.48" smashed="yes" rot="R90">
<attribute name="NAME" x="134.62" y="162.56" size="1.778" layer="95" ratio="10" rot="R180"/>
<attribute name="VALUE" x="134.62" y="152.4" size="1.778" layer="96" ratio="10" rot="R180"/>
</instance>
<instance part="SUPPLY11" gate="GND" x="129.54" y="134.62" smashed="yes">
<attribute name="VALUE" x="129.794" y="135.128" size="1.778" layer="96" ratio="15"/>
</instance>
<instance part="R108" gate="G$1" x="167.64" y="175.26" smashed="yes" rot="R180">
<attribute name="NAME" x="166.37" y="176.7586" size="1.778" layer="95"/>
<attribute name="VALUE" x="166.37" y="171.958" size="1.778" layer="96"/>
</instance>
<instance part="R10" gate="G$1" x="86.36" y="76.2" smashed="yes">
<attribute name="NAME" x="78.74" y="76.454" size="1.778" layer="95" ratio="15"/>
<attribute name="VALUE" x="88.9" y="76.454" size="1.778" layer="96" ratio="15"/>
</instance>
<instance part="L100" gate="G$1" x="170.18" y="205.74" smashed="yes">
<attribute name="NAME" x="167.64" y="208.788" size="1.778" layer="95" ratio="15"/>
<attribute name="VALUE" x="162.814" y="206.756" size="1.778" layer="96" ratio="15"/>
</instance>
<instance part="D102" gate="G$1" x="71.12" y="190.5" smashed="yes" rot="R90">
<attribute name="NAME" x="69.215" y="187.96" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="67.31" y="187.96" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="D101" gate="G$1" x="309.88" y="190.5" smashed="yes" rot="R90">
<attribute name="NAME" x="307.975" y="187.96" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="305.562" y="187.96" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="U101" gate="G$1" x="238.76" y="208.28" smashed="yes">
<attribute name="NAME" x="238.76" y="210.82" size="2.0828" layer="95" ratio="6" rot="SR0"/>
<attribute name="VALUE" x="238.76" y="218.694" size="2.0828" layer="96" ratio="6" rot="SR0"/>
</instance>
<instance part="D1" gate="G$1" x="35.56" y="66.04" smashed="yes" rot="R90">
<attribute name="NAME" x="33.655" y="63.5" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="31.75" y="63.5" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="D100" gate="G$1" x="274.32" y="124.46" smashed="yes">
<attribute name="NAME" x="271.78" y="126.365" size="1.778" layer="95"/>
<attribute name="VALUE" x="271.78" y="129.54" size="1.778" layer="96"/>
</instance>
<instance part="J1" gate="-1" x="347.98" y="66.04" smashed="yes">
<attribute name="NAME" x="344.678" y="66.548" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J1" gate="-2" x="347.98" y="50.8" smashed="yes">
<attribute name="NAME" x="344.678" y="51.308" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J1" gate="-3" x="347.98" y="99.06" smashed="yes">
<attribute name="NAME" x="344.678" y="99.568" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J1" gate="-4" x="347.98" y="124.46" smashed="yes">
<attribute name="NAME" x="344.678" y="124.968" size="1.27" layer="95" ratio="15"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="N$1" class="0">
<segment>
<wire x1="58.42" y1="81.28" x2="81.28" y2="81.28" width="0.1524" layer="91"/>
<wire x1="81.28" y1="81.28" x2="88.9" y2="81.28" width="0.1524" layer="91"/>
<wire x1="88.9" y1="81.28" x2="88.9" y2="83.82" width="0.1524" layer="91"/>
<pinref part="C10" gate="G$1" pin="2"/>
<pinref part="U1" gate="G$1" pin="XTAL1"/>
<wire x1="88.9" y1="83.82" x2="109.22" y2="83.82" width="0.1524" layer="91"/>
<wire x1="81.28" y1="83.82" x2="81.28" y2="81.28" width="0.1524" layer="91"/>
<junction x="81.28" y="81.28"/>
<pinref part="Y1" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="C11" gate="G$1" pin="2"/>
<wire x1="88.9" y1="86.36" x2="88.9" y2="91.44" width="0.1524" layer="91"/>
<wire x1="88.9" y1="91.44" x2="81.28" y2="91.44" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="XTAL2"/>
<wire x1="81.28" y1="91.44" x2="58.42" y2="91.44" width="0.1524" layer="91"/>
<wire x1="88.9" y1="86.36" x2="109.22" y2="86.36" width="0.1524" layer="91"/>
<wire x1="81.28" y1="88.9" x2="81.28" y2="91.44" width="0.1524" layer="91"/>
<junction x="81.28" y="91.44"/>
<pinref part="Y1" gate="G$1" pin="2"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<wire x1="335.28" y1="81.28" x2="335.28" y2="50.8" width="0.1524" layer="91"/>
<wire x1="233.68" y1="50.8" x2="238.76" y2="50.8" width="0.1524" layer="91"/>
<junction x="233.68" y="50.8"/>
<pinref part="SUPPLY4" gate="GND" pin="GND"/>
<wire x1="238.76" y1="50.8" x2="248.92" y2="50.8" width="0.1524" layer="91"/>
<wire x1="248.92" y1="50.8" x2="256.54" y2="50.8" width="0.1524" layer="91"/>
<wire x1="256.54" y1="50.8" x2="281.94" y2="50.8" width="0.1524" layer="91"/>
<wire x1="281.94" y1="50.8" x2="304.8" y2="50.8" width="0.1524" layer="91"/>
<wire x1="304.8" y1="50.8" x2="335.28" y2="50.8" width="0.1524" layer="91"/>
<wire x1="231.14" y1="73.66" x2="233.68" y2="73.66" width="0.1524" layer="91"/>
<wire x1="233.68" y1="73.66" x2="233.68" y2="50.8" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="5"/>
<wire x1="281.94" y1="50.8" x2="281.94" y2="83.82" width="0.1524" layer="91"/>
<junction x="281.94" y="50.8"/>
<pinref part="D3" gate="G$1" pin="K"/>
<pinref part="D2" gate="G$1" pin="K"/>
<wire x1="276.86" y1="86.36" x2="276.86" y2="83.82" width="0.1524" layer="91"/>
<wire x1="276.86" y1="83.82" x2="276.86" y2="81.28" width="0.1524" layer="91"/>
<wire x1="281.94" y1="83.82" x2="276.86" y2="83.82" width="0.1524" layer="91"/>
<junction x="276.86" y="83.82"/>
<wire x1="304.8" y1="78.74" x2="304.8" y2="50.8" width="0.1524" layer="91"/>
<junction x="304.8" y="50.8"/>
<pinref part="R14" gate="G$1" pin="2"/>
<wire x1="256.54" y1="53.34" x2="256.54" y2="50.8" width="0.1524" layer="91"/>
<junction x="256.54" y="50.8"/>
<pinref part="C8" gate="G$1" pin="1"/>
<junction x="248.92" y="50.8"/>
<wire x1="248.92" y1="55.88" x2="248.92" y2="50.8" width="0.1524" layer="91"/>
<pinref part="F101" gate="G$1" pin="2"/>
<pinref part="F5" gate="G$1" pin="2"/>
<pinref part="F2" gate="G$1" pin="2"/>
<wire x1="330.2" y1="83.82" x2="330.2" y2="81.28" width="0.1524" layer="91"/>
<wire x1="330.2" y1="81.28" x2="330.2" y2="78.74" width="0.1524" layer="91"/>
<wire x1="335.28" y1="81.28" x2="330.2" y2="81.28" width="0.1524" layer="91"/>
<junction x="330.2" y="81.28"/>
<pinref part="C9" gate="G$1" pin="1"/>
<wire x1="238.76" y1="55.88" x2="238.76" y2="50.8" width="0.1524" layer="91"/>
<junction x="238.76" y="50.8"/>
<junction x="335.28" y="50.8"/>
<wire x1="341.63" y1="50.8" x2="340.36" y2="50.8" width="0.1524" layer="91"/>
<pinref part="J1" gate="-2" pin="1"/>
<wire x1="340.36" y1="50.8" x2="335.28" y2="50.8" width="0.1524" layer="91"/>
<wire x1="342.9" y1="50.8" x2="341.63" y2="50.8" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R6" gate="G$1" pin="2"/>
<wire x1="187.96" y1="78.74" x2="190.5" y2="78.74" width="0.1524" layer="91"/>
<wire x1="190.5" y1="78.74" x2="190.5" y2="76.2" width="0.1524" layer="91"/>
<pinref part="R5" gate="G$1" pin="2"/>
<wire x1="190.5" y1="76.2" x2="187.96" y2="76.2" width="0.1524" layer="91"/>
<wire x1="190.5" y1="76.2" x2="190.5" y2="73.66" width="0.1524" layer="91"/>
<junction x="190.5" y="76.2"/>
<pinref part="SUPPLY2" gate="GND" pin="GND"/>
<pinref part="U1" gate="G$1" pin="GND_2"/>
<wire x1="190.5" y1="73.66" x2="190.5" y2="50.8" width="0.1524" layer="91"/>
<wire x1="190.5" y1="73.66" x2="144.78" y2="73.66" width="0.1524" layer="91"/>
<junction x="190.5" y="73.66"/>
<junction x="190.5" y="50.8"/>
<wire x1="99.06" y1="50.8" x2="104.14" y2="50.8" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="EPAD"/>
<wire x1="104.14" y1="50.8" x2="147.32" y2="50.8" width="0.1524" layer="91"/>
<wire x1="147.32" y1="50.8" x2="152.4" y2="50.8" width="0.1524" layer="91"/>
<wire x1="152.4" y1="50.8" x2="160.02" y2="50.8" width="0.1524" layer="91"/>
<wire x1="160.02" y1="50.8" x2="190.5" y2="50.8" width="0.1524" layer="91"/>
<wire x1="109.22" y1="91.44" x2="99.06" y2="91.44" width="0.1524" layer="91"/>
<wire x1="99.06" y1="91.44" x2="99.06" y2="88.9" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="GND"/>
<wire x1="99.06" y1="88.9" x2="99.06" y2="50.8" width="0.1524" layer="91"/>
<wire x1="109.22" y1="88.9" x2="99.06" y2="88.9" width="0.1524" layer="91"/>
<junction x="99.06" y="88.9"/>
<pinref part="C2" gate="G$1" pin="1"/>
<wire x1="50.8" y1="60.96" x2="50.8" y2="50.8" width="0.1524" layer="91"/>
<pinref part="C11" gate="G$1" pin="1"/>
<wire x1="48.26" y1="91.44" x2="43.18" y2="91.44" width="0.1524" layer="91"/>
<wire x1="43.18" y1="91.44" x2="43.18" y2="81.28" width="0.1524" layer="91"/>
<pinref part="C10" gate="G$1" pin="1"/>
<wire x1="48.26" y1="81.28" x2="43.18" y2="81.28" width="0.1524" layer="91"/>
<junction x="43.18" y="81.28"/>
<wire x1="43.18" y1="50.8" x2="43.18" y2="81.28" width="0.1524" layer="91"/>
<wire x1="43.18" y1="50.8" x2="50.8" y2="50.8" width="0.1524" layer="91"/>
<wire x1="99.06" y1="50.8" x2="96.52" y2="50.8" width="0.1524" layer="91"/>
<pinref part="R1" gate="G$1" pin="1"/>
<wire x1="96.52" y1="50.8" x2="86.36" y2="50.8" width="0.1524" layer="91"/>
<wire x1="86.36" y1="50.8" x2="78.74" y2="50.8" width="0.1524" layer="91"/>
<wire x1="50.8" y1="50.8" x2="78.74" y2="50.8" width="0.1524" layer="91"/>
<wire x1="78.74" y1="50.8" x2="78.74" y2="58.42" width="0.1524" layer="91"/>
<junction x="50.8" y="50.8"/>
<junction x="78.74" y="50.8"/>
<junction x="99.06" y="50.8"/>
<pinref part="C5" gate="G$1" pin="1"/>
<junction x="96.52" y="50.8"/>
<pinref part="C3" gate="G$1" pin="1"/>
<wire x1="96.52" y1="55.88" x2="96.52" y2="50.8" width="0.1524" layer="91"/>
<junction x="160.02" y="50.8"/>
<wire x1="160.02" y1="55.88" x2="160.02" y2="50.8" width="0.1524" layer="91"/>
<wire x1="35.56" y1="50.8" x2="43.18" y2="50.8" width="0.1524" layer="91"/>
<junction x="43.18" y="50.8"/>
<pinref part="C1" gate="G$1" pin="1"/>
<wire x1="104.14" y1="55.88" x2="104.14" y2="50.8" width="0.1524" layer="91"/>
<junction x="104.14" y="50.8"/>
<pinref part="C6" gate="G$1" pin="1"/>
<wire x1="86.36" y1="55.88" x2="86.36" y2="50.8" width="0.1524" layer="91"/>
<junction x="86.36" y="50.8"/>
<pinref part="C4" gate="G$1" pin="1"/>
<wire x1="152.4" y1="55.88" x2="152.4" y2="50.8" width="0.1524" layer="91"/>
<junction x="152.4" y="50.8"/>
<pinref part="R12" gate="G$1" pin="1"/>
<wire x1="147.32" y1="55.88" x2="147.32" y2="50.8" width="0.1524" layer="91"/>
<junction x="147.32" y="50.8"/>
<pinref part="D1" gate="G$1" pin="A"/>
<wire x1="35.56" y1="62.23" x2="35.56" y2="50.8" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R111" gate="G$1" pin="2"/>
<pinref part="SUPPLY6" gate="GND" pin="GND"/>
<wire x1="289.56" y1="213.36" x2="287.02" y2="213.36" width="0.1524" layer="91"/>
<wire x1="287.02" y1="213.36" x2="287.02" y2="210.82" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="264.16" y1="193.04" x2="269.24" y2="193.04" width="0.1524" layer="91"/>
<wire x1="269.24" y1="193.04" x2="269.24" y2="190.5" width="0.1524" layer="91"/>
<wire x1="269.24" y1="190.5" x2="264.16" y2="190.5" width="0.1524" layer="91"/>
<wire x1="309.88" y1="186.69" x2="309.88" y2="170.18" width="0.1524" layer="91"/>
<wire x1="309.88" y1="170.18" x2="294.64" y2="170.18" width="0.1524" layer="91"/>
<wire x1="294.64" y1="170.18" x2="269.24" y2="170.18" width="0.1524" layer="91"/>
<wire x1="269.24" y1="170.18" x2="269.24" y2="190.5" width="0.1524" layer="91"/>
<junction x="269.24" y="190.5"/>
<junction x="309.88" y="170.18"/>
<pinref part="C109" gate="G$1" pin="1"/>
<wire x1="332.74" y1="193.04" x2="332.74" y2="170.18" width="0.1524" layer="91"/>
<wire x1="332.74" y1="170.18" x2="322.58" y2="170.18" width="0.1524" layer="91"/>
<pinref part="C110" gate="G$1" pin="1"/>
<wire x1="322.58" y1="170.18" x2="309.88" y2="170.18" width="0.1524" layer="91"/>
<wire x1="340.36" y1="193.04" x2="340.36" y2="170.18" width="0.1524" layer="91"/>
<wire x1="340.36" y1="170.18" x2="332.74" y2="170.18" width="0.1524" layer="91"/>
<junction x="332.74" y="170.18"/>
<pinref part="C111" gate="G$1" pin="1"/>
<wire x1="347.98" y1="193.04" x2="347.98" y2="170.18" width="0.1524" layer="91"/>
<wire x1="347.98" y1="170.18" x2="340.36" y2="170.18" width="0.1524" layer="91"/>
<junction x="340.36" y="170.18"/>
<pinref part="SUPPLY7" gate="GND" pin="GND"/>
<wire x1="322.58" y1="167.64" x2="322.58" y2="170.18" width="0.1524" layer="91"/>
<junction x="322.58" y="170.18"/>
<pinref part="C108" gate="G$1" pin="1"/>
<wire x1="294.64" y1="172.72" x2="294.64" y2="170.18" width="0.1524" layer="91"/>
<junction x="294.64" y="170.18"/>
<pinref part="D101" gate="G$1" pin="A"/>
<pinref part="U101" gate="G$1" pin="GND"/>
<pinref part="U101" gate="G$1" pin="PAD"/>
</segment>
<segment>
<pinref part="C112" gate="G$1" pin="1"/>
<wire x1="231.14" y1="177.8" x2="231.14" y2="170.18" width="0.1524" layer="91"/>
<pinref part="R115" gate="G$1" pin="1"/>
<wire x1="223.52" y1="180.34" x2="223.52" y2="170.18" width="0.1524" layer="91"/>
<wire x1="223.52" y1="170.18" x2="231.14" y2="170.18" width="0.1524" layer="91"/>
<wire x1="223.52" y1="170.18" x2="213.36" y2="170.18" width="0.1524" layer="91"/>
<junction x="223.52" y="170.18"/>
<pinref part="SUPPLY1" gate="GND" pin="GND"/>
<wire x1="213.36" y1="170.18" x2="205.74" y2="170.18" width="0.1524" layer="91"/>
<wire x1="205.74" y1="170.18" x2="205.74" y2="167.64" width="0.1524" layer="91"/>
<junction x="205.74" y="170.18"/>
<wire x1="187.96" y1="170.18" x2="200.66" y2="170.18" width="0.1524" layer="91"/>
<pinref part="C104" gate="G$1" pin="1"/>
<wire x1="200.66" y1="170.18" x2="205.74" y2="170.18" width="0.1524" layer="91"/>
<wire x1="187.96" y1="182.88" x2="187.96" y2="170.18" width="0.1524" layer="91"/>
<pinref part="C103" gate="G$1" pin="1"/>
<wire x1="200.66" y1="182.88" x2="200.66" y2="170.18" width="0.1524" layer="91"/>
<junction x="200.66" y="170.18"/>
<pinref part="C105" gate="G$1" pin="1"/>
<wire x1="213.36" y1="182.88" x2="213.36" y2="170.18" width="0.1524" layer="91"/>
<junction x="213.36" y="170.18"/>
</segment>
<segment>
<wire x1="53.34" y1="170.18" x2="71.12" y2="170.18" width="0.1524" layer="91"/>
<pinref part="C102" gate="G$1" pin="1"/>
<wire x1="71.12" y1="170.18" x2="78.74" y2="170.18" width="0.1524" layer="91"/>
<wire x1="78.74" y1="185.42" x2="78.74" y2="170.18" width="0.1524" layer="91"/>
<pinref part="C101" gate="G$1" pin="1"/>
<wire x1="53.34" y1="185.42" x2="53.34" y2="170.18" width="0.1524" layer="91"/>
<junction x="53.34" y="170.18"/>
<pinref part="F102" gate="G$1" pin="2"/>
<wire x1="53.34" y1="170.18" x2="48.26" y2="170.18" width="0.1524" layer="91"/>
<wire x1="48.26" y1="170.18" x2="38.1" y2="170.18" width="0.1524" layer="91"/>
<wire x1="38.1" y1="170.18" x2="38.1" y2="185.42" width="0.1524" layer="91"/>
<wire x1="15.24" y1="203.2" x2="15.24" y2="170.18" width="0.1524" layer="91"/>
<wire x1="15.24" y1="170.18" x2="38.1" y2="170.18" width="0.1524" layer="91"/>
<junction x="38.1" y="170.18"/>
<pinref part="J100" gate="G$1" pin="2"/>
<wire x1="15.24" y1="203.2" x2="12.7" y2="203.2" width="0.1524" layer="91"/>
<wire x1="71.12" y1="186.69" x2="71.12" y2="170.18" width="0.1524" layer="91"/>
<junction x="71.12" y="170.18"/>
<pinref part="SUPPLY8" gate="GND" pin="GND"/>
<wire x1="48.26" y1="167.64" x2="48.26" y2="170.18" width="0.1524" layer="91"/>
<junction x="48.26" y="170.18"/>
<pinref part="D102" gate="G$1" pin="A"/>
</segment>
<segment>
<pinref part="U100" gate="G$1" pin="5_GND"/>
<wire x1="139.7" y1="165.1" x2="139.7" y2="144.78" width="0.1524" layer="91"/>
<pinref part="C107" gate="G$1" pin="1"/>
<wire x1="134.62" y1="152.4" x2="134.62" y2="144.78" width="0.1524" layer="91"/>
<wire x1="134.62" y1="144.78" x2="139.7" y2="144.78" width="0.1524" layer="91"/>
<pinref part="R107" gate="G$1" pin="1"/>
<wire x1="99.06" y1="157.48" x2="99.06" y2="139.7" width="0.1524" layer="91"/>
<pinref part="C100" gate="G$1" pin="1"/>
<wire x1="99.06" y1="139.7" x2="119.38" y2="139.7" width="0.1524" layer="91"/>
<wire x1="119.38" y1="139.7" x2="129.54" y2="139.7" width="0.1524" layer="91"/>
<wire x1="129.54" y1="139.7" x2="139.7" y2="139.7" width="0.1524" layer="91"/>
<wire x1="139.7" y1="139.7" x2="152.4" y2="139.7" width="0.1524" layer="91"/>
<wire x1="152.4" y1="139.7" x2="152.4" y2="142.24" width="0.1524" layer="91"/>
<wire x1="139.7" y1="144.78" x2="139.7" y2="139.7" width="0.1524" layer="91"/>
<junction x="139.7" y="144.78"/>
<junction x="139.7" y="139.7"/>
<pinref part="R113" gate="G$1" pin="1"/>
<wire x1="119.38" y1="142.24" x2="119.38" y2="139.7" width="0.1524" layer="91"/>
<junction x="119.38" y="139.7"/>
<pinref part="SUPPLY11" gate="GND" pin="GND"/>
<wire x1="129.54" y1="137.16" x2="129.54" y2="139.7" width="0.1524" layer="91"/>
<junction x="129.54" y="139.7"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="TXOUT"/>
<pinref part="R9" gate="G$1" pin="2"/>
<wire x1="109.22" y1="78.74" x2="101.6" y2="78.74" width="0.1524" layer="91"/>
<wire x1="101.6" y1="78.74" x2="91.44" y2="78.74" width="0.1524" layer="91"/>
<wire x1="101.6" y1="93.98" x2="101.6" y2="78.74" width="0.1524" layer="91"/>
<junction x="101.6" y="78.74"/>
<pinref part="TP4" gate="G$1" pin="PP"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="BIAS"/>
<pinref part="R4" gate="G$1" pin="2"/>
<wire x1="109.22" y1="73.66" x2="104.14" y2="73.66" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="2"/>
<wire x1="104.14" y1="73.66" x2="91.44" y2="73.66" width="0.1524" layer="91"/>
<wire x1="104.14" y1="66.04" x2="104.14" y2="73.66" width="0.1524" layer="91"/>
<junction x="104.14" y="73.66"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="DIRSET2"/>
<wire x1="144.78" y1="78.74" x2="172.72" y2="78.74" width="0.1524" layer="91"/>
<pinref part="R6" gate="G$1" pin="1"/>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="172.72" y1="78.74" x2="177.8" y2="78.74" width="0.1524" layer="91"/>
<wire x1="172.72" y1="91.44" x2="172.72" y2="78.74" width="0.1524" layer="91"/>
<junction x="172.72" y="78.74"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="DIRSET1"/>
<wire x1="144.78" y1="76.2" x2="165.1" y2="76.2" width="0.1524" layer="91"/>
<pinref part="R5" gate="G$1" pin="1"/>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="165.1" y1="76.2" x2="177.8" y2="76.2" width="0.1524" layer="91"/>
<wire x1="165.1" y1="91.44" x2="165.1" y2="76.2" width="0.1524" layer="91"/>
<junction x="165.1" y="76.2"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<wire x1="193.04" y1="83.82" x2="193.04" y2="73.66" width="0.1524" layer="91"/>
<wire x1="193.04" y1="73.66" x2="203.2" y2="73.66" width="0.1524" layer="91"/>
<wire x1="203.2" y1="73.66" x2="205.74" y2="73.66" width="0.1524" layer="91"/>
<pinref part="R8" gate="G$1" pin="2"/>
<wire x1="187.96" y1="83.82" x2="193.04" y2="83.82" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="4"/>
<wire x1="203.2" y1="74.422" x2="203.2" y2="73.66" width="0.1524" layer="91"/>
<junction x="203.2" y="73.66"/>
<pinref part="TP3" gate="G$1" pin="PP"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<wire x1="195.58" y1="81.28" x2="195.58" y2="83.82" width="0.1524" layer="91"/>
<pinref part="R7" gate="G$1" pin="2"/>
<wire x1="195.58" y1="83.82" x2="200.66" y2="83.82" width="0.1524" layer="91"/>
<wire x1="187.96" y1="81.28" x2="195.58" y2="81.28" width="0.1524" layer="91"/>
<wire x1="200.66" y1="83.82" x2="200.66" y2="78.74" width="0.1524" layer="91"/>
<wire x1="200.66" y1="78.74" x2="205.74" y2="78.74" width="0.1524" layer="91"/>
<junction x="200.66" y="83.82"/>
<pinref part="U2" gate="G$1" pin="2"/>
<pinref part="U2" gate="G$1" pin="3"/>
<wire x1="200.66" y1="83.82" x2="203.2" y2="83.82" width="0.1524" layer="91"/>
<wire x1="203.2" y1="83.82" x2="205.74" y2="83.82" width="0.1524" layer="91"/>
<wire x1="203.2" y1="84.836" x2="203.2" y2="83.82" width="0.1524" layer="91"/>
<junction x="203.2" y="83.82"/>
<pinref part="TP1" gate="G$1" pin="PP"/>
</segment>
</net>
<net name="DATA_P" class="0">
<segment>
<wire x1="231.14" y1="83.82" x2="236.22" y2="83.82" width="0.1524" layer="91"/>
<wire x1="236.22" y1="83.82" x2="251.46" y2="83.82" width="0.1524" layer="91"/>
<wire x1="261.62" y1="99.06" x2="256.54" y2="99.06" width="0.1524" layer="91"/>
<wire x1="256.54" y1="99.06" x2="251.46" y2="99.06" width="0.1524" layer="91"/>
<wire x1="251.46" y1="99.06" x2="251.46" y2="83.82" width="0.1524" layer="91"/>
<pinref part="R13" gate="G$1" pin="2"/>
<wire x1="261.62" y1="88.9" x2="261.62" y2="99.06" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="7"/>
<wire x1="261.62" y1="99.06" x2="276.86" y2="99.06" width="0.1524" layer="91"/>
<junction x="261.62" y="99.06"/>
<pinref part="D3" gate="G$1" pin="A"/>
<wire x1="276.86" y1="99.06" x2="330.2" y2="99.06" width="0.1524" layer="91"/>
<wire x1="276.86" y1="96.52" x2="276.86" y2="99.06" width="0.1524" layer="91"/>
<junction x="276.86" y="99.06"/>
<wire x1="236.22" y1="84.836" x2="236.22" y2="83.82" width="0.1524" layer="91"/>
<junction x="236.22" y="83.82"/>
<pinref part="F5" gate="G$1" pin="1"/>
<wire x1="330.2" y1="93.98" x2="330.2" y2="99.06" width="0.1524" layer="91"/>
<pinref part="DP" gate="G$1" pin="PP"/>
<pinref part="R15" gate="G$1" pin="2"/>
<wire x1="256.54" y1="101.6" x2="256.54" y2="99.06" width="0.1524" layer="91"/>
<junction x="256.54" y="99.06"/>
<pinref part="J1" gate="-3" pin="1"/>
<wire x1="342.9" y1="99.06" x2="330.2" y2="99.06" width="0.1524" layer="91"/>
<junction x="330.2" y="99.06"/>
</segment>
</net>
<net name="DATA_N" class="0">
<segment>
<wire x1="261.62" y1="66.04" x2="256.54" y2="66.04" width="0.1524" layer="91"/>
<wire x1="256.54" y1="66.04" x2="251.46" y2="66.04" width="0.1524" layer="91"/>
<pinref part="R13" gate="G$1" pin="1"/>
<wire x1="231.14" y1="78.74" x2="236.22" y2="78.74" width="0.1524" layer="91"/>
<wire x1="236.22" y1="78.74" x2="251.46" y2="78.74" width="0.1524" layer="91"/>
<wire x1="261.62" y1="78.74" x2="261.62" y2="66.04" width="0.1524" layer="91"/>
<wire x1="251.46" y1="66.04" x2="251.46" y2="78.74" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="6"/>
<pinref part="R14" gate="G$1" pin="1"/>
<wire x1="256.54" y1="63.5" x2="256.54" y2="66.04" width="0.1524" layer="91"/>
<junction x="256.54" y="66.04"/>
<wire x1="261.62" y1="66.04" x2="276.86" y2="66.04" width="0.1524" layer="91"/>
<junction x="261.62" y="66.04"/>
<pinref part="D2" gate="G$1" pin="A"/>
<wire x1="276.86" y1="66.04" x2="330.2" y2="66.04" width="0.1524" layer="91"/>
<wire x1="276.86" y1="71.12" x2="276.86" y2="66.04" width="0.1524" layer="91"/>
<junction x="276.86" y="66.04"/>
<wire x1="236.22" y1="79.756" x2="236.22" y2="78.74" width="0.1524" layer="91"/>
<junction x="236.22" y="78.74"/>
<pinref part="F2" gate="G$1" pin="1"/>
<wire x1="330.2" y1="68.58" x2="330.2" y2="66.04" width="0.1524" layer="91"/>
<wire x1="330.2" y1="66.04" x2="340.36" y2="66.04" width="0.1524" layer="91"/>
<junction x="330.2" y="66.04"/>
<pinref part="DN" gate="G$1" pin="PP"/>
<pinref part="J1" gate="-1" pin="1"/>
<wire x1="340.36" y1="66.04" x2="341.63" y2="66.04" width="0.1524" layer="91"/>
<wire x1="342.9" y1="66.04" x2="341.63" y2="66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="VL"/>
<wire x1="144.78" y1="86.36" x2="160.02" y2="86.36" width="0.1524" layer="91"/>
<wire x1="160.02" y1="86.36" x2="160.02" y2="104.14" width="0.1524" layer="91"/>
<pinref part="P+2" gate="1" pin="+5V"/>
<pinref part="C3" gate="G$1" pin="2"/>
<wire x1="160.02" y1="104.14" x2="160.02" y2="106.68" width="0.1524" layer="91"/>
<wire x1="160.02" y1="86.36" x2="160.02" y2="68.58" width="0.1524" layer="91"/>
<junction x="160.02" y="86.36"/>
<pinref part="C4" gate="G$1" pin="2"/>
<wire x1="160.02" y1="68.58" x2="160.02" y2="66.04" width="0.1524" layer="91"/>
<wire x1="152.4" y1="66.04" x2="152.4" y2="68.58" width="0.1524" layer="91"/>
<wire x1="152.4" y1="68.58" x2="160.02" y2="68.58" width="0.1524" layer="91"/>
<junction x="160.02" y="68.58"/>
<pinref part="R3" gate="G$1" pin="2"/>
<wire x1="172.72" y1="101.6" x2="172.72" y2="104.14" width="0.1524" layer="91"/>
<wire x1="172.72" y1="104.14" x2="165.1" y2="104.14" width="0.1524" layer="91"/>
<pinref part="R2" gate="G$1" pin="2"/>
<wire x1="165.1" y1="104.14" x2="165.1" y2="101.6" width="0.1524" layer="91"/>
<wire x1="160.02" y1="104.14" x2="165.1" y2="104.14" width="0.1524" layer="91"/>
<junction x="160.02" y="104.14"/>
<junction x="165.1" y="104.14"/>
</segment>
<segment>
<wire x1="231.14" y1="88.9" x2="248.92" y2="88.9" width="0.1524" layer="91"/>
<wire x1="248.92" y1="88.9" x2="248.92" y2="114.3" width="0.1524" layer="91"/>
<pinref part="P+3" gate="1" pin="+5V"/>
<pinref part="U2" gate="G$1" pin="8"/>
<pinref part="R15" gate="G$1" pin="1"/>
<wire x1="248.92" y1="114.3" x2="248.92" y2="116.84" width="0.1524" layer="91"/>
<wire x1="256.54" y1="111.76" x2="256.54" y2="114.3" width="0.1524" layer="91"/>
<wire x1="256.54" y1="114.3" x2="248.92" y2="114.3" width="0.1524" layer="91"/>
<junction x="248.92" y="114.3"/>
<pinref part="C8" gate="G$1" pin="2"/>
<wire x1="248.92" y1="88.9" x2="248.92" y2="68.58" width="0.1524" layer="91"/>
<junction x="248.92" y="88.9"/>
<pinref part="C9" gate="G$1" pin="2"/>
<wire x1="248.92" y1="68.58" x2="248.92" y2="66.04" width="0.1524" layer="91"/>
<wire x1="238.76" y1="66.04" x2="238.76" y2="68.58" width="0.1524" layer="91"/>
<wire x1="238.76" y1="68.58" x2="248.92" y2="68.58" width="0.1524" layer="91"/>
<junction x="248.92" y="68.58"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="VCC"/>
<wire x1="109.22" y1="81.28" x2="96.52" y2="81.28" width="0.1524" layer="91"/>
<wire x1="96.52" y1="81.28" x2="96.52" y2="101.6" width="0.1524" layer="91"/>
<pinref part="P+1" gate="1" pin="+5V"/>
<pinref part="C5" gate="G$1" pin="2"/>
<wire x1="96.52" y1="81.28" x2="96.52" y2="68.58" width="0.1524" layer="91"/>
<junction x="96.52" y="81.28"/>
<pinref part="C6" gate="G$1" pin="2"/>
<wire x1="96.52" y1="68.58" x2="96.52" y2="66.04" width="0.1524" layer="91"/>
<wire x1="86.36" y1="66.04" x2="86.36" y2="68.58" width="0.1524" layer="91"/>
<wire x1="86.36" y1="68.58" x2="96.52" y2="68.58" width="0.1524" layer="91"/>
<junction x="96.52" y="68.58"/>
</segment>
<segment>
<wire x1="363.22" y1="205.74" x2="365.76" y2="205.74" width="0.1524" layer="91"/>
<pinref part="P+5" gate="1" pin="+5V"/>
<wire x1="365.76" y1="205.74" x2="373.38" y2="205.74" width="0.1524" layer="91"/>
<wire x1="373.38" y1="205.74" x2="373.38" y2="215.9" width="0.1524" layer="91"/>
<pinref part="FB100" gate="G$1" pin="2"/>
<pinref part="R109" gate="G$1" pin="2"/>
<wire x1="317.5" y1="213.36" x2="365.76" y2="213.36" width="0.1524" layer="91"/>
<wire x1="365.76" y1="213.36" x2="365.76" y2="205.74" width="0.1524" layer="91"/>
<junction x="365.76" y="205.74"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="DIR"/>
<pinref part="R7" gate="G$1" pin="1"/>
<wire x1="144.78" y1="81.28" x2="177.8" y2="81.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="RXOUT"/>
<pinref part="R8" gate="G$1" pin="1"/>
<wire x1="144.78" y1="83.82" x2="177.8" y2="83.82" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="TXIN"/>
<pinref part="R11" gate="G$1" pin="1"/>
<wire x1="144.78" y1="88.9" x2="177.8" y2="88.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<pinref part="R11" gate="G$1" pin="2"/>
<wire x1="187.96" y1="88.9" x2="203.2" y2="88.9" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="1"/>
<wire x1="203.2" y1="88.9" x2="205.74" y2="88.9" width="0.1524" layer="91"/>
<wire x1="203.2" y1="89.916" x2="203.2" y2="88.9" width="0.1524" layer="91"/>
<junction x="203.2" y="88.9"/>
<pinref part="TP5" gate="G$1" pin="PP"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<wire x1="78.74" y1="71.12" x2="109.22" y2="71.12" width="0.1524" layer="91"/>
<pinref part="R1" gate="G$1" pin="2"/>
<wire x1="78.74" y1="68.58" x2="78.74" y2="71.12" width="0.1524" layer="91"/>
<pinref part="R4" gate="G$1" pin="1"/>
<wire x1="81.28" y1="73.66" x2="78.74" y2="73.66" width="0.1524" layer="91"/>
<wire x1="78.74" y1="73.66" x2="78.74" y2="71.12" width="0.1524" layer="91"/>
<junction x="78.74" y="71.12"/>
<pinref part="U1" gate="G$1" pin="RES"/>
</segment>
</net>
<net name="N$25" class="1">
<segment>
<pinref part="R9" gate="G$1" pin="1"/>
<wire x1="81.28" y1="78.74" x2="78.74" y2="78.74" width="0.1524" layer="91"/>
<pinref part="C2" gate="G$1" pin="2"/>
<wire x1="78.74" y1="78.74" x2="50.8" y2="78.74" width="0.1524" layer="91"/>
<wire x1="50.8" y1="71.12" x2="50.8" y2="78.74" width="0.1524" layer="91"/>
<junction x="50.8" y="78.74"/>
<wire x1="50.8" y1="78.74" x2="35.56" y2="78.74" width="0.1524" layer="91"/>
<wire x1="78.74" y1="78.74" x2="78.74" y2="76.2" width="0.1524" layer="91"/>
<junction x="78.74" y="78.74"/>
<wire x1="35.56" y1="83.82" x2="35.56" y2="78.74" width="0.1524" layer="91"/>
<wire x1="33.02" y1="83.82" x2="35.56" y2="83.82" width="0.1524" layer="91"/>
<pinref part="TP2" gate="G$1" pin="PP"/>
<wire x1="35.56" y1="69.85" x2="35.56" y2="78.74" width="0.1524" layer="91"/>
<junction x="35.56" y="78.74"/>
<pinref part="R10" gate="G$1" pin="1"/>
<wire x1="81.28" y1="76.2" x2="78.74" y2="76.2" width="0.1524" layer="91"/>
<pinref part="C7" gate="G$1" pin="1"/>
<wire x1="35.56" y1="99.06" x2="35.56" y2="83.82" width="0.1524" layer="91"/>
<junction x="35.56" y="83.82"/>
<pinref part="D1" gate="G$1" pin="K"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<wire x1="304.8" y1="88.9" x2="304.8" y2="124.46" width="0.1524" layer="91"/>
<pinref part="F101" gate="G$1" pin="1"/>
<wire x1="278.13" y1="124.46" x2="304.8" y2="124.46" width="0.1524" layer="91"/>
<wire x1="304.8" y1="124.46" x2="342.9" y2="124.46" width="0.1524" layer="91"/>
<junction x="304.8" y="124.46"/>
<pinref part="D100" gate="G$1" pin="K"/>
<pinref part="J1" gate="-4" pin="1"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="R114" gate="G$1" pin="1"/>
<wire x1="264.16" y1="203.2" x2="269.24" y2="203.2" width="0.1524" layer="91"/>
<pinref part="U101" gate="G$1" pin="BOOT"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<wire x1="264.16" y1="198.12" x2="302.26" y2="198.12" width="0.1524" layer="91"/>
<wire x1="302.26" y1="198.12" x2="302.26" y2="213.36" width="0.1524" layer="91"/>
<pinref part="R111" gate="G$1" pin="1"/>
<pinref part="R109" gate="G$1" pin="1"/>
<wire x1="299.72" y1="213.36" x2="302.26" y2="213.36" width="0.1524" layer="91"/>
<wire x1="302.26" y1="213.36" x2="307.34" y2="213.36" width="0.1524" layer="91"/>
<junction x="302.26" y="213.36"/>
<pinref part="U101" gate="G$1" pin="FB"/>
</segment>
</net>
<net name="N$43" class="0">
<segment>
<pinref part="C112" gate="G$1" pin="2"/>
<wire x1="233.68" y1="190.5" x2="231.14" y2="190.5" width="0.1524" layer="91"/>
<wire x1="231.14" y1="190.5" x2="231.14" y2="187.96" width="0.1524" layer="91"/>
<pinref part="U101" gate="G$1" pin="SS"/>
</segment>
</net>
<net name="N$46" class="0">
<segment>
<pinref part="R115" gate="G$1" pin="2"/>
<wire x1="233.68" y1="195.58" x2="223.52" y2="195.58" width="0.1524" layer="91"/>
<wire x1="223.52" y1="195.58" x2="223.52" y2="190.5" width="0.1524" layer="91"/>
<pinref part="U101" gate="G$1" pin="RT/SYNC"/>
</segment>
</net>
<net name="N$28" class="0">
<segment>
<wire x1="187.96" y1="205.74" x2="200.66" y2="205.74" width="0.1524" layer="91"/>
<wire x1="200.66" y1="205.74" x2="213.36" y2="205.74" width="0.1524" layer="91"/>
<wire x1="213.36" y1="205.74" x2="233.68" y2="205.74" width="0.1524" layer="91"/>
<pinref part="C105" gate="G$1" pin="2"/>
<wire x1="213.36" y1="193.04" x2="213.36" y2="205.74" width="0.1524" layer="91"/>
<junction x="213.36" y="205.74"/>
<pinref part="C103" gate="G$1" pin="2"/>
<wire x1="200.66" y1="193.04" x2="200.66" y2="205.74" width="0.1524" layer="91"/>
<junction x="200.66" y="205.74"/>
<pinref part="C104" gate="G$1" pin="2"/>
<wire x1="187.96" y1="193.04" x2="187.96" y2="205.74" width="0.1524" layer="91"/>
<pinref part="L100" gate="G$1" pin="2"/>
<wire x1="175.26" y1="205.74" x2="187.96" y2="205.74" width="0.1524" layer="91"/>
<junction x="187.96" y="205.74"/>
<pinref part="U101" gate="G$1" pin="VIN"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="FB100" gate="G$1" pin="1"/>
<pinref part="C111" gate="G$1" pin="2"/>
<wire x1="347.98" y1="203.2" x2="347.98" y2="205.74" width="0.1524" layer="91"/>
<junction x="347.98" y="205.74"/>
<wire x1="347.98" y1="205.74" x2="353.06" y2="205.74" width="0.1524" layer="91"/>
<wire x1="340.36" y1="205.74" x2="347.98" y2="205.74" width="0.1524" layer="91"/>
<pinref part="C110" gate="G$1" pin="2"/>
<wire x1="340.36" y1="203.2" x2="340.36" y2="205.74" width="0.1524" layer="91"/>
<junction x="340.36" y="205.74"/>
<wire x1="332.74" y1="205.74" x2="340.36" y2="205.74" width="0.1524" layer="91"/>
<pinref part="C109" gate="G$1" pin="2"/>
<wire x1="332.74" y1="203.2" x2="332.74" y2="205.74" width="0.1524" layer="91"/>
<pinref part="L200" gate="G$1" pin="2"/>
<wire x1="327.66" y1="205.74" x2="332.74" y2="205.74" width="0.1524" layer="91"/>
<junction x="332.74" y="205.74"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<pinref part="R114" gate="G$1" pin="2"/>
<pinref part="C106" gate="G$1" pin="1"/>
<wire x1="279.4" y1="203.2" x2="281.94" y2="203.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<pinref part="C106" gate="G$1" pin="2"/>
<pinref part="R112" gate="G$1" pin="2"/>
<wire x1="292.1" y1="203.2" x2="294.64" y2="203.2" width="0.1524" layer="91"/>
<wire x1="294.64" y1="203.2" x2="294.64" y2="195.58" width="0.1524" layer="91"/>
<wire x1="264.16" y1="205.74" x2="294.64" y2="205.74" width="0.1524" layer="91"/>
<wire x1="294.64" y1="205.74" x2="309.88" y2="205.74" width="0.1524" layer="91"/>
<wire x1="309.88" y1="205.74" x2="317.5" y2="205.74" width="0.1524" layer="91"/>
<junction x="309.88" y="205.74"/>
<pinref part="L200" gate="G$1" pin="1"/>
<wire x1="309.88" y1="194.31" x2="309.88" y2="205.74" width="0.1524" layer="91"/>
<wire x1="294.64" y1="203.2" x2="294.64" y2="205.74" width="0.1524" layer="91"/>
<junction x="294.64" y="203.2"/>
<junction x="294.64" y="205.74"/>
<pinref part="D101" gate="G$1" pin="K"/>
<pinref part="U101" gate="G$1" pin="SW"/>
</segment>
</net>
<net name="N$26" class="0">
<segment>
<pinref part="R112" gate="G$1" pin="1"/>
<pinref part="C108" gate="G$1" pin="2"/>
<wire x1="294.64" y1="185.42" x2="294.64" y2="182.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="R102" gate="G$1" pin="2"/>
<wire x1="124.46" y1="198.12" x2="124.46" y2="205.74" width="0.1524" layer="91"/>
<pinref part="R104" gate="G$1" pin="2"/>
<wire x1="124.46" y1="205.74" x2="119.38" y2="205.74" width="0.1524" layer="91"/>
<pinref part="Q100" gate="G$1" pin="D"/>
<junction x="124.46" y="205.74"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<pinref part="R102" gate="G$1" pin="1"/>
<wire x1="124.46" y1="187.96" x2="124.46" y2="185.42" width="0.1524" layer="91"/>
<pinref part="R103" gate="G$1" pin="1"/>
<wire x1="124.46" y1="185.42" x2="119.38" y2="185.42" width="0.1524" layer="91"/>
<pinref part="U100" gate="G$1" pin="9_SENSE"/>
<wire x1="129.54" y1="180.34" x2="129.54" y2="182.88" width="0.1524" layer="91"/>
<junction x="124.46" y="185.42"/>
<wire x1="129.54" y1="182.88" x2="124.46" y2="182.88" width="0.1524" layer="91"/>
<wire x1="124.46" y1="182.88" x2="124.46" y2="185.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$33" class="0">
<segment>
<pinref part="R103" gate="G$1" pin="2"/>
<wire x1="109.22" y1="185.42" x2="104.14" y2="185.42" width="0.1524" layer="91"/>
<wire x1="104.14" y1="185.42" x2="104.14" y2="205.74" width="0.1524" layer="91"/>
<pinref part="R104" gate="G$1" pin="1"/>
<wire x1="104.14" y1="205.74" x2="109.22" y2="205.74" width="0.1524" layer="91"/>
<pinref part="U100" gate="G$1" pin="10_VCC"/>
<wire x1="121.92" y1="175.26" x2="104.14" y2="175.26" width="0.1524" layer="91"/>
<wire x1="104.14" y1="175.26" x2="104.14" y2="185.42" width="0.1524" layer="91"/>
<junction x="104.14" y="185.42"/>
<wire x1="270.51" y1="124.46" x2="88.9" y2="124.46" width="0.1524" layer="91"/>
<pinref part="C102" gate="G$1" pin="2"/>
<wire x1="78.74" y1="195.58" x2="78.74" y2="205.74" width="0.1524" layer="91"/>
<wire x1="71.12" y1="194.31" x2="71.12" y2="205.74" width="0.1524" layer="91"/>
<pinref part="C101" gate="G$1" pin="2"/>
<wire x1="53.34" y1="195.58" x2="53.34" y2="205.74" width="0.1524" layer="91"/>
<junction x="53.34" y="205.74"/>
<pinref part="J100" gate="G$1" pin="1"/>
<pinref part="F102" gate="G$1" pin="1"/>
<wire x1="38.1" y1="195.58" x2="38.1" y2="205.74" width="0.1524" layer="91"/>
<junction x="38.1" y="205.74"/>
<wire x1="38.1" y1="205.74" x2="12.7" y2="205.74" width="0.1524" layer="91"/>
<wire x1="38.1" y1="205.74" x2="53.34" y2="205.74" width="0.1524" layer="91"/>
<wire x1="53.34" y1="205.74" x2="71.12" y2="205.74" width="0.1524" layer="91"/>
<junction x="71.12" y="205.74"/>
<wire x1="71.12" y1="205.74" x2="78.74" y2="205.74" width="0.1524" layer="91"/>
<junction x="78.74" y="205.74"/>
<wire x1="78.74" y1="205.74" x2="88.9" y2="205.74" width="0.1524" layer="91"/>
<wire x1="88.9" y1="124.46" x2="88.9" y2="205.74" width="0.1524" layer="91"/>
<wire x1="104.14" y1="205.74" x2="99.06" y2="205.74" width="0.1524" layer="91"/>
<junction x="104.14" y="205.74"/>
<junction x="88.9" y="205.74"/>
<pinref part="R106" gate="G$1" pin="2"/>
<wire x1="99.06" y1="205.74" x2="88.9" y2="205.74" width="0.1524" layer="91"/>
<wire x1="99.06" y1="198.12" x2="99.06" y2="205.74" width="0.1524" layer="91"/>
<junction x="99.06" y="205.74"/>
<pinref part="D102" gate="G$1" pin="K"/>
<pinref part="D100" gate="G$1" pin="A"/>
</segment>
</net>
<net name="N$37" class="0">
<segment>
<pinref part="U100" gate="G$1" pin="2_VREF"/>
<pinref part="R110" gate="G$1" pin="2"/>
<wire x1="121.92" y1="170.18" x2="119.38" y2="170.18" width="0.1524" layer="91"/>
<wire x1="119.38" y1="170.18" x2="119.38" y2="167.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$38" class="0">
<segment>
<pinref part="R110" gate="G$1" pin="1"/>
<pinref part="R113" gate="G$1" pin="2"/>
<wire x1="119.38" y1="157.48" x2="119.38" y2="154.94" width="0.1524" layer="91"/>
<pinref part="U100" gate="G$1" pin="3_PROG"/>
<wire x1="119.38" y1="154.94" x2="119.38" y2="152.4" width="0.1524" layer="91"/>
<wire x1="129.54" y1="165.1" x2="129.54" y2="154.94" width="0.1524" layer="91"/>
<wire x1="129.54" y1="154.94" x2="119.38" y2="154.94" width="0.1524" layer="91"/>
<junction x="119.38" y="154.94"/>
</segment>
</net>
<net name="N$40" class="0">
<segment>
<pinref part="U100" gate="G$1" pin="1_EN"/>
<wire x1="121.92" y1="172.72" x2="99.06" y2="172.72" width="0.1524" layer="91"/>
<pinref part="R107" gate="G$1" pin="2"/>
<wire x1="99.06" y1="167.64" x2="99.06" y2="172.72" width="0.1524" layer="91"/>
<pinref part="R106" gate="G$1" pin="1"/>
<wire x1="99.06" y1="187.96" x2="99.06" y2="172.72" width="0.1524" layer="91"/>
<junction x="99.06" y="172.72"/>
</segment>
</net>
<net name="N$39" class="0">
<segment>
<pinref part="R101" gate="G$1" pin="2"/>
<pinref part="Q100" gate="G$1" pin="G"/>
</segment>
</net>
<net name="N$42" class="0">
<segment>
<pinref part="U100" gate="G$1" pin="6_PG"/>
<pinref part="R105" gate="G$1" pin="1"/>
<wire x1="147.32" y1="175.26" x2="149.86" y2="175.26" width="0.1524" layer="91"/>
<wire x1="149.86" y1="175.26" x2="149.86" y2="187.96" width="0.1524" layer="91"/>
<wire x1="149.86" y1="175.26" x2="162.56" y2="175.26" width="0.1524" layer="91"/>
<junction x="149.86" y="175.26"/>
<pinref part="R108" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$47" class="0">
<segment>
<pinref part="R100" gate="G$1" pin="1"/>
<pinref part="C100" gate="G$1" pin="2"/>
<wire x1="152.4" y1="157.48" x2="152.4" y2="152.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$44" class="0">
<segment>
<pinref part="U100" gate="G$1" pin="4_TIMER"/>
<pinref part="C107" gate="G$1" pin="2"/>
<wire x1="134.62" y1="165.1" x2="134.62" y2="162.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$45" class="0">
<segment>
<pinref part="R108" gate="G$1" pin="1"/>
<wire x1="172.72" y1="175.26" x2="175.26" y2="175.26" width="0.1524" layer="91"/>
<wire x1="175.26" y1="175.26" x2="175.26" y2="200.66" width="0.1524" layer="91"/>
<wire x1="175.26" y1="200.66" x2="233.68" y2="200.66" width="0.1524" layer="91"/>
<pinref part="U101" gate="G$1" pin="EN"/>
</segment>
</net>
<net name="N$31" class="0">
<segment>
<pinref part="R100" gate="G$1" pin="2"/>
<wire x1="152.4" y1="167.64" x2="152.4" y2="182.88" width="0.1524" layer="91"/>
<pinref part="R101" gate="G$1" pin="1"/>
<pinref part="U100" gate="G$1" pin="8_GATE"/>
<wire x1="134.62" y1="187.96" x2="134.62" y2="182.88" width="0.1524" layer="91"/>
<wire x1="134.62" y1="182.88" x2="134.62" y2="180.34" width="0.1524" layer="91"/>
<wire x1="152.4" y1="182.88" x2="134.62" y2="182.88" width="0.1524" layer="91"/>
<junction x="134.62" y="182.88"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="R10" gate="G$1" pin="2"/>
<pinref part="U1" gate="G$1" pin="RXIN"/>
<wire x1="91.44" y1="76.2" x2="109.22" y2="76.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$29" class="1">
<segment>
<pinref part="C7" gate="G$1" pin="2"/>
<wire x1="35.56" y1="109.22" x2="35.56" y2="119.38" width="0.1524" layer="91"/>
<pinref part="R105" gate="G$1" pin="2"/>
<wire x1="149.86" y1="198.12" x2="149.86" y2="205.74" width="0.1524" layer="91"/>
<pinref part="Q100" gate="G$1" pin="S"/>
<wire x1="139.7" y1="205.74" x2="149.86" y2="205.74" width="0.1524" layer="91"/>
<junction x="139.7" y="205.74"/>
<pinref part="U100" gate="G$1" pin="7_OUT"/>
<wire x1="139.7" y1="180.34" x2="139.7" y2="205.74" width="0.1524" layer="91"/>
<pinref part="L100" gate="G$1" pin="1"/>
<wire x1="165.1" y1="205.74" x2="157.48" y2="205.74" width="0.1524" layer="91"/>
<junction x="149.86" y="205.74"/>
<wire x1="157.48" y1="205.74" x2="149.86" y2="205.74" width="0.1524" layer="91"/>
<wire x1="35.56" y1="119.38" x2="157.48" y2="119.38" width="0.1524" layer="91"/>
<wire x1="157.48" y1="119.38" x2="157.48" y2="205.74" width="0.1524" layer="91"/>
<junction x="157.48" y="205.74"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="SYNCOUT"/>
<pinref part="R12" gate="G$1" pin="2"/>
<wire x1="144.78" y1="91.44" x2="147.32" y2="91.44" width="0.1524" layer="91"/>
<wire x1="147.32" y1="91.44" x2="147.32" y2="66.04" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
<note version="9.4" severity="warning">
Since Version 9.4, EAGLE supports the overriding of 3D packages
in schematics and board files. Those overridden 3d packages
will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
