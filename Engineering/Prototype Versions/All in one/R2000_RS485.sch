<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.5.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="yes" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="5" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="no" active="no"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="no" active="no"/>
<layer number="103" name="tMap" color="7" fill="1" visible="no" active="no"/>
<layer number="104" name="Name" color="7" fill="1" visible="no" active="no"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="no" active="no"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="no" active="no"/>
<layer number="107" name="Crop" color="7" fill="1" visible="no" active="no"/>
<layer number="108" name="fp8" color="7" fill="1" visible="yes" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="yes" active="yes"/>
<layer number="110" name="110" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="111" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="7" fill="1" visible="yes" active="yes"/>
<layer number="114" name="FRNTMAAT1" color="7" fill="1" visible="no" active="no"/>
<layer number="115" name="FRNTMAAT2" color="7" fill="1" visible="no" active="no"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="no" active="no"/>
<layer number="117" name="BACKMAAT1" color="7" fill="1" visible="no" active="no"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="yes" active="yes"/>
<layer number="119" name="KAP_TEKEN" color="7" fill="1" visible="no" active="no"/>
<layer number="120" name="KAP_MAAT1" color="7" fill="1" visible="no" active="no"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="no" active="no"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="no" active="no"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="top_silk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="130" name="SMDSTROOK" color="7" fill="1" visible="no" active="no"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="133" name="bottom_silk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="DrillLegend" color="7" fill="1" visible="no" active="no"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="14" fill="1" visible="no" active="no"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="no"/>
<layer number="201" name="201bmp" color="2" fill="1" visible="no" active="no"/>
<layer number="202" name="202bmp" color="3" fill="1" visible="no" active="no"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="no" active="no"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="no" active="no"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="no" active="no"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="no" active="no"/>
<layer number="207" name="207bmp" color="15" fill="10" visible="no" active="no"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="no" active="no"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="Eagle3D_PG1" color="7" fill="1" visible="no" active="no"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="no"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="OrgLBR" color="13" fill="1" visible="no" active="no"/>
<layer number="255" name="Accent" color="7" fill="1" visible="no" active="no"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="supply2" urn="urn:adsk.eagle:library:372">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
Please keep in mind, that these devices are necessary for the
automatic wiring of the supply signals.&lt;p&gt;
The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND" urn="urn:adsk.eagle:symbol:26990/1" library_version="2">
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="-1.27" y2="0" width="0.254" layer="94"/>
<text x="-1.905" y="-3.175" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" urn="urn:adsk.eagle:component:27037/1" prefix="SUPPLY" library_version="2">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="GND" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="frames">
<description>&lt;b&gt;Frames for Sheet and Layout&lt;/b&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="A3_LOC_JMA_RELEASE">
<wire x1="0" y1="241.3" x2="63.5" y2="241.3" width="0.025" layer="94"/>
<wire x1="63.5" y1="241.3" x2="127" y2="241.3" width="0.025" layer="94"/>
<wire x1="127" y1="241.3" x2="190.5" y2="241.3" width="0.025" layer="94"/>
<wire x1="190.5" y1="241.3" x2="254" y2="241.3" width="0.025" layer="94"/>
<wire x1="254" y1="241.3" x2="317.5" y2="241.3" width="0.025" layer="94"/>
<wire x1="317.5" y1="241.3" x2="380.97883125" y2="241.3" width="0.025" layer="94"/>
<wire x1="380.97883125" y1="241.3" x2="380.97883125" y2="0.02116875" width="0.025" layer="94"/>
<wire x1="380.97883125" y1="0.02116875" x2="0" y2="0.02116875" width="0.025" layer="94"/>
<wire x1="0" y1="0.02116875" x2="0" y2="60.325" width="0.025" layer="94"/>
<wire x1="0" y1="60.325" x2="0" y2="120.65" width="0.025" layer="94"/>
<wire x1="0" y1="120.65" x2="0" y2="180.975" width="0.025" layer="94"/>
<wire x1="0" y1="180.975" x2="0" y2="241.3" width="0.025" layer="94"/>
<wire x1="5.079890625" y1="236.22041875" x2="375.898909375" y2="236.22041875" width="0.025" layer="94"/>
<wire x1="375.898909375" y1="236.22041875" x2="375.898909375" y2="5.10075" width="0.025" layer="94"/>
<wire x1="375.898909375" y1="5.10075" x2="5.079890625" y2="5.10075" width="0.025" layer="94"/>
<wire x1="5.079890625" y1="5.10075" x2="5.079890625" y2="236.22041875" width="0.025" layer="94"/>
<wire x1="317.5" y1="5.08" x2="317.5" y2="0" width="0.025" layer="94"/>
<wire x1="317.5" y1="236.22" x2="317.5" y2="241.3" width="0.025" layer="94"/>
<wire x1="254" y1="5.08" x2="254" y2="0" width="0.025" layer="94"/>
<wire x1="254" y1="236.22" x2="254" y2="241.3" width="0.025" layer="94"/>
<wire x1="190.5" y1="5.08" x2="190.5" y2="0" width="0.025" layer="94"/>
<wire x1="190.5" y1="236.22" x2="190.5" y2="241.3" width="0.025" layer="94"/>
<wire x1="127" y1="5.08" x2="127" y2="0" width="0.025" layer="94"/>
<wire x1="127" y1="236.22" x2="127" y2="241.3" width="0.025" layer="94"/>
<wire x1="63.5" y1="5.08" x2="63.5" y2="0" width="0.025" layer="94"/>
<wire x1="63.5" y1="236.22" x2="63.5" y2="241.3" width="0.025" layer="94"/>
<wire x1="5.08" y1="180.975" x2="0" y2="180.975" width="0.025" layer="94"/>
<wire x1="375.92" y1="180.975" x2="381" y2="180.975" width="0.025" layer="94"/>
<wire x1="5.08" y1="120.65" x2="0" y2="120.65" width="0.025" layer="94"/>
<wire x1="375.92" y1="120.65" x2="381" y2="120.65" width="0.025" layer="94"/>
<wire x1="5.08" y1="60.325" x2="0" y2="60.325" width="0.025" layer="94"/>
<wire x1="375.92" y1="60.325" x2="381" y2="60.325" width="0.025" layer="94"/>
<wire x1="271.78" y1="5.08" x2="271.78" y2="21.59" width="0.025" layer="94"/>
<wire x1="271.78" y1="21.59" x2="271.78" y2="27.94" width="0.025" layer="94"/>
<wire x1="271.78" y1="27.94" x2="271.78" y2="34.29" width="0.025" layer="94"/>
<wire x1="271.78" y1="34.29" x2="271.78" y2="40.64" width="0.025" layer="94"/>
<wire x1="271.78" y1="40.64" x2="271.78" y2="45.72" width="0.025" layer="94"/>
<wire x1="271.78" y1="45.72" x2="285.75" y2="45.72" width="0.025" layer="94"/>
<wire x1="285.75" y1="45.72" x2="293.37" y2="45.72" width="0.025" layer="94"/>
<wire x1="293.37" y1="45.72" x2="304.8" y2="45.72" width="0.025" layer="94"/>
<wire x1="304.8" y1="45.72" x2="375.92" y2="45.72" width="0.025" layer="94"/>
<wire x1="304.8" y1="5.08" x2="304.8" y2="8.89" width="0.025" layer="94"/>
<wire x1="304.8" y1="8.89" x2="304.8" y2="15.24" width="0.025" layer="94"/>
<wire x1="304.8" y1="15.24" x2="304.8" y2="21.59" width="0.025" layer="94"/>
<wire x1="304.8" y1="21.59" x2="304.8" y2="27.94" width="0.025" layer="94"/>
<wire x1="304.8" y1="27.94" x2="304.8" y2="34.29" width="0.025" layer="94"/>
<wire x1="304.8" y1="34.29" x2="304.8" y2="40.64" width="0.025" layer="94"/>
<wire x1="304.8" y1="40.64" x2="304.8" y2="45.72" width="0.025" layer="94"/>
<wire x1="271.78" y1="40.64" x2="304.8" y2="40.64" width="0.025" layer="94"/>
<wire x1="271.78" y1="34.29" x2="304.8" y2="34.29" width="0.025" layer="94"/>
<wire x1="271.78" y1="27.94" x2="304.8" y2="27.94" width="0.025" layer="94"/>
<wire x1="271.78" y1="21.59" x2="285.75" y2="21.59" width="0.025" layer="94"/>
<wire x1="285.75" y1="21.59" x2="293.37" y2="21.59" width="0.025" layer="94"/>
<wire x1="293.37" y1="21.59" x2="304.8" y2="21.59" width="0.025" layer="94"/>
<wire x1="304.8" y1="8.89" x2="322.58" y2="8.89" width="0.025" layer="94"/>
<wire x1="322.58" y1="8.89" x2="359.41" y2="8.89" width="0.025" layer="94"/>
<wire x1="359.41" y1="8.89" x2="369.57" y2="8.89" width="0.025" layer="94"/>
<wire x1="369.57" y1="8.89" x2="375.92" y2="8.89" width="0.025" layer="94"/>
<wire x1="322.58" y1="8.89" x2="322.58" y2="5.08" width="0.025" layer="94"/>
<wire x1="359.41" y1="8.89" x2="359.41" y2="5.08" width="0.025" layer="94"/>
<wire x1="304.8" y1="15.24" x2="369.57" y2="15.24" width="0.025" layer="94"/>
<wire x1="369.57" y1="15.24" x2="375.92" y2="15.24" width="0.025" layer="94"/>
<wire x1="369.57" y1="15.24" x2="369.57" y2="8.89" width="0.025" layer="94"/>
<wire x1="304.8" y1="21.59" x2="375.92" y2="21.59" width="0.025" layer="94"/>
<wire x1="285.75" y1="45.72" x2="285.75" y2="21.59" width="0.025" layer="94"/>
<wire x1="293.37" y1="45.72" x2="293.37" y2="21.59" width="0.025" layer="94"/>
<wire x1="307.34" y1="31.75" x2="373.38" y2="31.75" width="0.025" layer="94"/>
<wire x1="329.29703125" y1="44.90211875" x2="329.35418125" y2="44.890690625" width="0.025" layer="94"/>
<wire x1="329.35418125" y1="44.890690625" x2="329.3999" y2="44.879259375" width="0.025" layer="94"/>
<wire x1="329.81138125" y1="44.776390625" x2="329.84566875" y2="44.764959375" width="0.025" layer="94"/>
<wire x1="329.84566875" y1="44.764959375" x2="329.891390625" y2="44.75353125" width="0.025" layer="94"/>
<wire x1="331.137259375" y1="44.1706" x2="331.365859375" y2="43.99915" width="0.025" layer="94"/>
<wire x1="331.365859375" y1="43.99915" x2="331.451590625" y2="43.91341875" width="0.025" layer="94"/>
<wire x1="331.92593125" y1="43.164759375" x2="331.937359375" y2="43.09618125" width="0.025" layer="94"/>
<wire x1="331.937359375" y1="43.09618125" x2="331.948790625" y2="42.993309375" width="0.025" layer="94"/>
<wire x1="331.891640625" y1="42.5704" x2="331.90306875" y2="42.604690625" width="0.025" layer="94"/>
<wire x1="331.84591875" y1="42.4561" x2="331.891640625" y2="42.5704" width="0.025" layer="94"/>
<wire x1="329.262740625" y1="40.947340625" x2="329.319890625" y2="40.95876875" width="0.025" layer="94"/>
<wire x1="329.194159375" y1="40.935909375" x2="329.262740625" y2="40.947340625" width="0.025" layer="94"/>
<wire x1="329.194159375" y1="40.935909375" x2="331.44586875" y2="32.512" width="0.025" layer="94"/>
<wire x1="323.764909375" y1="32.512" x2="324.9625" y2="37.03828125" width="0.025" layer="94"/>
<wire x1="324.9625" y1="37.03828125" x2="325.993759375" y2="40.935909375" width="0.025" layer="94"/>
<wire x1="325.719440625" y1="40.993059375" x2="325.78801875" y2="40.98163125" width="0.025" layer="94"/>
<wire x1="325.63943125" y1="41.01591875" x2="325.719440625" y2="40.993059375" width="0.025" layer="94"/>
<wire x1="325.45655" y1="41.061640625" x2="325.536559375" y2="41.03878125" width="0.025" layer="94"/>
<wire x1="325.3994" y1="41.07306875" x2="325.45655" y2="41.061640625" width="0.025" layer="94"/>
<wire x1="325.136509375" y1="41.15308125" x2="325.21651875" y2="41.13021875" width="0.025" layer="94"/>
<wire x1="325.06793125" y1="41.175940625" x2="325.136509375" y2="41.15308125" width="0.025" layer="94"/>
<wire x1="324.976490625" y1="41.21023125" x2="325.06793125" y2="41.175940625" width="0.025" layer="94"/>
<wire x1="324.97076875" y1="41.21251875" x2="324.976490625" y2="41.21023125" width="0.025" layer="94"/>
<wire x1="324.64501875" y1="41.347390625" x2="324.747890625" y2="41.30166875" width="0.025" layer="94"/>
<wire x1="324.565009375" y1="41.38168125" x2="324.64501875" y2="41.347390625" width="0.025" layer="94"/>
<wire x1="324.450709375" y1="41.43883125" x2="324.45185" y2="41.438259375" width="0.025" layer="94"/>
<wire x1="324.393559375" y1="41.47311875" x2="324.450709375" y2="41.43883125" width="0.025" layer="94"/>
<wire x1="324.26783125" y1="41.5417" x2="324.347840625" y2="41.49598125" width="0.025" layer="94"/>
<wire x1="324.233540625" y1="41.564559375" x2="324.26783125" y2="41.5417" width="0.025" layer="94"/>
<wire x1="324.18781875" y1="41.58741875" x2="324.233540625" y2="41.564559375" width="0.025" layer="94"/>
<wire x1="324.1421" y1="41.621709375" x2="324.18781875" y2="41.58741875" width="0.025" layer="94"/>
<wire x1="324.08495" y1="41.656" x2="324.1421" y2="41.621709375" width="0.025" layer="94"/>
<wire x1="323.822059375" y1="41.861740625" x2="323.85635" y2="41.82745" width="0.025" layer="94"/>
<wire x1="323.776340625" y1="41.89603125" x2="323.822059375" y2="41.861740625" width="0.025" layer="94"/>
<wire x1="323.62775" y1="42.05605" x2="323.662040625" y2="42.01033125" width="0.025" layer="94"/>
<wire x1="323.593459375" y1="42.090340625" x2="323.62775" y2="42.05605" width="0.025" layer="94"/>
<wire x1="323.35343125" y1="42.46753125" x2="323.354" y2="42.466390625" width="0.025" layer="94"/>
<wire x1="323.33056875" y1="42.52468125" x2="323.35343125" y2="42.46753125" width="0.025" layer="94"/>
<wire x1="323.536309375" y1="43.690540625" x2="323.58203125" y2="43.736259375" width="0.025" layer="94"/>
<wire x1="323.58203125" y1="43.736259375" x2="323.62775" y2="43.793409375" width="0.025" layer="94"/>
<wire x1="323.78776875" y1="43.95343125" x2="323.84491875" y2="43.99915" width="0.025" layer="94"/>
<wire x1="323.84491875" y1="43.99915" x2="323.87806875" y2="44.0323" width="0.025" layer="94"/>
<wire x1="323.87806875" y1="44.0323" x2="323.879209375" y2="44.033440625" width="0.025" layer="94"/>
<wire x1="323.879209375" y1="44.033440625" x2="323.936359375" y2="44.079159375" width="0.025" layer="94"/>
<wire x1="323.936359375" y1="44.079159375" x2="324.0278" y2="44.147740625" width="0.025" layer="94"/>
<wire x1="324.164959375" y1="44.23918125" x2="324.222109375" y2="44.27346875" width="0.025" layer="94"/>
<wire x1="324.222109375" y1="44.27346875" x2="324.290690625" y2="44.319190625" width="0.025" layer="94"/>
<wire x1="324.290690625" y1="44.319190625" x2="324.336409375" y2="44.34205" width="0.025" layer="94"/>
<wire x1="324.336409375" y1="44.34205" x2="324.393559375" y2="44.376340625" width="0.025" layer="94"/>
<wire x1="324.62101875" y1="44.49006875" x2="324.622159375" y2="44.490640625" width="0.025" layer="94"/>
<wire x1="324.622159375" y1="44.490640625" x2="324.679309375" y2="44.5135" width="0.025" layer="94"/>
<wire x1="324.679309375" y1="44.5135" x2="324.72503125" y2="44.536359375" width="0.025" layer="94"/>
<wire x1="324.72503125" y1="44.536359375" x2="325.01078125" y2="44.650659375" width="0.025" layer="94"/>
<wire x1="325.01078125" y1="44.650659375" x2="325.35368125" y2="44.764959375" width="0.025" layer="94"/>
<wire x1="325.35368125" y1="44.764959375" x2="325.433690625" y2="44.78781875" width="0.025" layer="94"/>
<wire x1="325.433690625" y1="44.78781875" x2="325.479409375" y2="44.79925" width="0.025" layer="94"/>
<wire x1="325.479409375" y1="44.79925" x2="325.5137" y2="44.81068125" width="0.025" layer="94"/>
<wire x1="325.5137" y1="44.81068125" x2="325.7423" y2="44.86783125" width="0.025" layer="94"/>
<wire x1="325.7423" y1="44.86783125" x2="325.79945" y2="44.879259375" width="0.025" layer="94"/>
<wire x1="325.79945" y1="44.879259375" x2="325.84516875" y2="44.890690625" width="0.025" layer="94"/>
<wire x1="326.393809375" y1="44.69638125" x2="326.450959375" y2="44.707809375" width="0.025" layer="94"/>
<wire x1="326.32523125" y1="44.68495" x2="326.393809375" y2="44.69638125" width="0.025" layer="94"/>
<wire x1="325.936609375" y1="44.604940625" x2="325.993759375" y2="44.61636875" width="0.025" layer="94"/>
<wire x1="325.84516875" y1="44.58208125" x2="325.936609375" y2="44.604940625" width="0.025" layer="94"/>
<wire x1="325.765159375" y1="44.55921875" x2="325.84516875" y2="44.58208125" width="0.025" layer="94"/>
<wire x1="325.67371875" y1="44.536359375" x2="325.765159375" y2="44.55921875" width="0.025" layer="94"/>
<wire x1="325.5137" y1="44.490640625" x2="325.520559375" y2="44.4926" width="0.025" layer="94"/>
<wire x1="325.23938125" y1="44.3992" x2="325.5137" y2="44.490640625" width="0.025" layer="94"/>
<wire x1="324.98791875" y1="44.29633125" x2="325.06793125" y2="44.33061875" width="0.025" layer="94"/>
<wire x1="324.93076875" y1="44.27346875" x2="324.98791875" y2="44.29633125" width="0.025" layer="94"/>
<wire x1="324.61073125" y1="44.10201875" x2="324.679309375" y2="44.147740625" width="0.025" layer="94"/>
<wire x1="324.53071875" y1="44.0563" x2="324.531859375" y2="44.05695" width="0.025" layer="94"/>
<wire x1="324.531859375" y1="44.05695" x2="324.61073125" y2="44.10201875" width="0.025" layer="94"/>
<wire x1="324.462140625" y1="44.01058125" x2="324.53071875" y2="44.0563" width="0.025" layer="94"/>
<wire x1="324.233540625" y1="43.8277" x2="324.279259375" y2="43.87341875" width="0.025" layer="94"/>
<wire x1="324.164959375" y1="43.77055" x2="324.233540625" y2="43.8277" width="0.025" layer="94"/>
<wire x1="323.73061875" y1="42.51325" x2="323.74205" y2="42.478959375" width="0.025" layer="94"/>
<wire x1="323.74205" y1="42.478959375" x2="323.75348125" y2="42.433240625" width="0.025" layer="94"/>
<wire x1="323.75348125" y1="42.433240625" x2="323.776340625" y2="42.38751875" width="0.025" layer="94"/>
<wire x1="323.776340625" y1="42.38751875" x2="323.7992" y2="42.33036875" width="0.025" layer="94"/>
<wire x1="324.08495" y1="41.95318125" x2="324.1421" y2="41.907459375" width="0.025" layer="94"/>
<wire x1="324.1421" y1="41.907459375" x2="324.176390625" y2="41.87316875" width="0.025" layer="94"/>
<wire x1="324.41641875" y1="41.690290625" x2="324.462140625" y2="41.656" width="0.025" layer="94"/>
<wire x1="324.462140625" y1="41.656" x2="324.53071875" y2="41.61028125" width="0.025" layer="94"/>
<wire x1="324.64501875" y1="41.5417" x2="324.690740625" y2="41.518840625" width="0.025" layer="94"/>
<wire x1="324.690740625" y1="41.518840625" x2="324.72503125" y2="41.49598125" width="0.025" layer="94"/>
<wire x1="324.72503125" y1="41.49598125" x2="324.78218125" y2="41.461690625" width="0.025" layer="94"/>
<wire x1="324.919340625" y1="41.393109375" x2="324.976490625" y2="41.37025" width="0.025" layer="94"/>
<wire x1="324.976490625" y1="41.37025" x2="325.022209375" y2="41.347390625" width="0.025" layer="94"/>
<wire x1="325.46798125" y1="41.18736875" x2="325.536559375" y2="41.164509375" width="0.025" layer="94"/>
<wire x1="325.536559375" y1="41.164509375" x2="325.58228125" y2="41.15308125" width="0.025" layer="94"/>
<wire x1="325.58228125" y1="41.15308125" x2="325.61656875" y2="41.14165" width="0.025" layer="94"/>
<wire x1="325.61656875" y1="41.14165" x2="325.708009375" y2="41.118790625" width="0.025" layer="94"/>
<wire x1="325.708009375" y1="41.118790625" x2="325.7423" y2="41.107359375" width="0.025" layer="94"/>
<wire x1="325.7423" y1="41.107359375" x2="326.01661875" y2="41.03878125" width="0.025" layer="94"/>
<wire x1="325.45655" y1="41.38168125" x2="325.5137" y2="41.35881875" width="0.025" layer="94"/>
<wire x1="325.433690625" y1="41.393109375" x2="325.45655" y2="41.38168125" width="0.025" layer="94"/>
<wire x1="325.15936875" y1="41.518840625" x2="325.319390625" y2="41.43883125" width="0.025" layer="94"/>
<wire x1="325.10221875" y1="41.55313125" x2="325.15936875" y2="41.518840625" width="0.025" layer="94"/>
<wire x1="324.70216875" y1="43.4848" x2="324.747890625" y2="43.519090625" width="0.025" layer="94"/>
<wire x1="324.747890625" y1="43.519090625" x2="324.77075" y2="43.54195" width="0.025" layer="94"/>
<wire x1="324.98678125" y1="43.68978125" x2="324.98791875" y2="43.690540625" width="0.025" layer="94"/>
<wire x1="324.98791875" y1="43.690540625" x2="325.10221875" y2="43.75911875" width="0.025" layer="94"/>
<wire x1="325.10221875" y1="43.75911875" x2="325.177659375" y2="43.796840625" width="0.025" layer="94"/>
<wire x1="325.45655" y1="43.93056875" x2="325.68515" y2="44.022009375" width="0.025" layer="94"/>
<wire x1="325.82116875" y1="44.06735" x2="325.822309375" y2="44.06773125" width="0.025" layer="94"/>
<wire x1="325.822309375" y1="44.06773125" x2="326.062340625" y2="44.136309375" width="0.025" layer="94"/>
<wire x1="326.062340625" y1="44.136309375" x2="326.15378125" y2="44.15916875" width="0.025" layer="94"/>
<wire x1="326.15378125" y1="44.15916875" x2="326.35951875" y2="44.204890625" width="0.025" layer="94"/>
<wire x1="326.279509375" y1="43.919140625" x2="326.462390625" y2="43.964859375" width="0.025" layer="94"/>
<wire x1="326.119490625" y1="43.87341875" x2="326.279509375" y2="43.919140625" width="0.025" layer="94"/>
<wire x1="326.02805" y1="43.850559375" x2="326.119490625" y2="43.87341875" width="0.025" layer="94"/>
<wire x1="325.68515" y1="43.72483125" x2="325.8566" y2="43.793409375" width="0.025" layer="94"/>
<wire x1="325.605140625" y1="43.690540625" x2="325.68515" y2="43.72483125" width="0.025" layer="94"/>
<wire x1="325.604" y1="43.68996875" x2="325.605140625" y2="43.690540625" width="0.025" layer="94"/>
<wire x1="325.34225" y1="43.54195" x2="325.45655" y2="43.61053125" width="0.025" layer="94"/>
<wire x1="325.341109375" y1="43.541190625" x2="325.34225" y2="43.54195" width="0.025" layer="94"/>
<wire x1="324.862190625" y1="43.15333125" x2="325.033640625" y2="43.32478125" width="0.025" layer="94"/>
<wire x1="324.8279" y1="43.107609375" x2="324.862190625" y2="43.15333125" width="0.025" layer="94"/>
<wire x1="324.75931875" y1="43.004740625" x2="324.8279" y2="43.107609375" width="0.025" layer="94"/>
<wire x1="324.633590625" y1="42.62755" x2="324.65645" y2="42.74185" width="0.025" layer="94"/>
<wire x1="324.633590625" y1="42.626409375" x2="324.633590625" y2="42.62755" width="0.025" layer="94"/>
<wire x1="324.690740625" y1="42.261790625" x2="324.70216875" y2="42.23893125" width="0.025" layer="94"/>
<wire x1="324.70216875" y1="42.23893125" x2="324.7136" y2="42.204640625" width="0.025" layer="94"/>
<wire x1="324.7136" y1="42.204640625" x2="324.72503125" y2="42.18178125" width="0.025" layer="94"/>
<wire x1="324.72503125" y1="42.18178125" x2="324.747890625" y2="42.147490625" width="0.025" layer="94"/>
<wire x1="324.793609375" y1="42.06748125" x2="324.87361875" y2="41.964609375" width="0.025" layer="94"/>
<wire x1="324.87361875" y1="41.964609375" x2="324.874759375" y2="41.96346875" width="0.025" layer="94"/>
<wire x1="325.84516875" y1="41.35881875" x2="325.969759375" y2="41.317290625" width="0.025" layer="94"/>
<wire x1="326.084059375" y1="41.279190625" x2="326.0852" y2="41.278809375" width="0.025" layer="94"/>
<wire x1="326.0852" y1="41.278809375" x2="326.119490625" y2="41.43883125" width="0.025" layer="94"/>
<wire x1="326.0852" y1="41.461690625" x2="326.119490625" y2="41.43883125" width="0.025" layer="94"/>
<wire x1="325.993759375" y1="41.49598125" x2="326.0852" y2="41.461690625" width="0.025" layer="94"/>
<wire x1="325.99261875" y1="41.49646875" x2="325.993759375" y2="41.49598125" width="0.025" layer="94"/>
<wire x1="325.650859375" y1="41.66743125" x2="325.652" y2="41.66678125" width="0.025" layer="94"/>
<wire x1="325.55941875" y1="41.72458125" x2="325.650859375" y2="41.66743125" width="0.025" layer="94"/>
<wire x1="325.44511875" y1="42.95901875" x2="325.536559375" y2="43.0276" width="0.025" layer="94"/>
<wire x1="325.536559375" y1="43.0276" x2="325.5377" y2="43.028359375" width="0.025" layer="94"/>
<wire x1="325.605140625" y1="43.07331875" x2="325.776590625" y2="43.176190625" width="0.025" layer="94"/>
<wire x1="326.49668125" y1="43.43908125" x2="326.64526875" y2="43.47336875" width="0.025" layer="94"/>
<wire x1="326.75956875" y1="43.30191875" x2="326.81671875" y2="43.31335" width="0.025" layer="94"/>
<wire x1="326.71385" y1="43.290490625" x2="326.75956875" y2="43.30191875" width="0.025" layer="94"/>
<wire x1="326.6567" y1="43.279059375" x2="326.71385" y2="43.290490625" width="0.025" layer="94"/>
<wire x1="326.565259375" y1="43.2562" x2="326.6567" y2="43.279059375" width="0.025" layer="94"/>
<wire x1="326.35951875" y1="43.18761875" x2="326.41666875" y2="43.21048125" width="0.025" layer="94"/>
<wire x1="326.32523125" y1="43.176190625" x2="326.35951875" y2="43.18761875" width="0.025" layer="94"/>
<wire x1="326.21093125" y1="43.13046875" x2="326.32523125" y2="43.176190625" width="0.025" layer="94"/>
<wire x1="326.005190625" y1="43.0276" x2="326.21093125" y2="43.13046875" width="0.025" layer="94"/>
<wire x1="325.547609375" y1="42.09148125" x2="325.547990625" y2="42.090340625" width="0.025" layer="94"/>
<wire x1="325.547990625" y1="42.090340625" x2="325.57085" y2="42.04461875" width="0.025" layer="94"/>
<wire x1="325.84516875" y1="41.747440625" x2="325.95946875" y2="41.678859375" width="0.025" layer="94"/>
<wire x1="325.95946875" y1="41.678859375" x2="326.03948125" y2="41.633140625" width="0.025" layer="94"/>
<wire x1="326.03948125" y1="41.633140625" x2="326.108059375" y2="41.59885" width="0.025" layer="94"/>
<wire x1="326.108059375" y1="41.59885" x2="326.165209375" y2="41.575990625" width="0.025" layer="94"/>
<wire x1="326.165209375" y1="41.575990625" x2="326.35951875" y2="42.307509375" width="0.025" layer="94"/>
<wire x1="329.022709375" y1="41.575990625" x2="329.04556875" y2="41.575990625" width="0.025" layer="94"/>
<wire x1="329.04556875" y1="41.575990625" x2="329.251309375" y2="41.678859375" width="0.025" layer="94"/>
<wire x1="329.251309375" y1="41.678859375" x2="329.25245" y2="41.67961875" width="0.025" layer="94"/>
<wire x1="329.42161875" y1="41.7924" x2="329.422759375" y2="41.793159375" width="0.025" layer="94"/>
<wire x1="329.422759375" y1="41.793159375" x2="329.548490625" y2="41.918890625" width="0.025" layer="94"/>
<wire x1="329.548490625" y1="41.918890625" x2="329.58278125" y2="41.964609375" width="0.025" layer="94"/>
<wire x1="329.58278125" y1="41.964609375" x2="329.6285" y2="42.033190625" width="0.025" layer="94"/>
<wire x1="329.662790625" y1="42.1132" x2="329.68565" y2="42.18178125" width="0.025" layer="94"/>
<wire x1="329.68565" y1="42.18178125" x2="329.69708125" y2="42.2275" width="0.025" layer="94"/>
<wire x1="329.57135" y1="42.718990625" x2="329.61706875" y2="42.650409375" width="0.025" layer="94"/>
<wire x1="329.537059375" y1="42.764709375" x2="329.57135" y2="42.718990625" width="0.025" layer="94"/>
<wire x1="329.434190625" y1="42.86758125" x2="329.43533125" y2="42.866440625" width="0.025" layer="94"/>
<wire x1="329.377040625" y1="42.9133" x2="329.434190625" y2="42.86758125" width="0.025" layer="94"/>
<wire x1="329.01128125" y1="43.119040625" x2="329.06843125" y2="43.09618125" width="0.025" layer="94"/>
<wire x1="328.965559375" y1="43.1419" x2="329.01128125" y2="43.119040625" width="0.025" layer="94"/>
<wire x1="328.32548125" y1="43.32478125" x2="328.43978125" y2="43.30191875" width="0.025" layer="94"/>
<wire x1="328.2569" y1="43.336209375" x2="328.32548125" y2="43.32478125" width="0.025" layer="94"/>
<wire x1="328.15403125" y1="43.347640625" x2="328.2569" y2="43.336209375" width="0.025" layer="94"/>
<wire x1="328.005440625" y1="43.35906875" x2="328.15403125" y2="43.347640625" width="0.025" layer="94"/>
<wire x1="328.005440625" y1="43.35906875" x2="328.005440625" y2="43.53051875" width="0.025" layer="94"/>
<wire x1="328.78268125" y1="43.41621875" x2="329.010140625" y2="43.34798125" width="0.025" layer="94"/>
<wire x1="329.010140625" y1="43.34798125" x2="329.01128125" y2="43.347640625" width="0.025" layer="94"/>
<wire x1="329.01128125" y1="43.347640625" x2="329.194159375" y2="43.279059375" width="0.025" layer="94"/>
<wire x1="329.194159375" y1="43.279059375" x2="329.308459375" y2="43.233340625" width="0.025" layer="94"/>
<wire x1="329.308459375" y1="43.233340625" x2="329.3096" y2="43.23276875" width="0.025" layer="94"/>
<wire x1="329.81138125" y1="42.9133" x2="329.891390625" y2="42.833290625" width="0.025" layer="94"/>
<wire x1="329.891390625" y1="42.833290625" x2="329.937109375" y2="42.776140625" width="0.025" layer="94"/>
<wire x1="329.95996875" y1="42.74185" x2="329.994259375" y2="42.6847" width="0.025" layer="94"/>
<wire x1="329.994259375" y1="42.6847" x2="330.03998125" y2="42.5704" width="0.025" layer="94"/>
<wire x1="330.03998125" y1="42.5704" x2="330.051409375" y2="42.51325" width="0.025" layer="94"/>
<wire x1="330.051409375" y1="42.51325" x2="330.062840625" y2="42.421809375" width="0.025" layer="94"/>
<wire x1="329.55991875" y1="41.66743125" x2="329.6285" y2="41.71315" width="0.025" layer="94"/>
<wire x1="329.45705" y1="41.61028125" x2="329.55991875" y2="41.66743125" width="0.025" layer="94"/>
<wire x1="329.15986875" y1="41.47311875" x2="329.1633" y2="41.474590625" width="0.025" layer="94"/>
<wire x1="329.06843125" y1="41.43883125" x2="329.15986875" y2="41.47311875" width="0.025" layer="94"/>
<wire x1="329.137009375" y1="41.290240625" x2="329.18273125" y2="41.30166875" width="0.025" layer="94"/>
<wire x1="329.18273125" y1="41.30166875" x2="329.21701875" y2="41.3131" width="0.025" layer="94"/>
<wire x1="329.21701875" y1="41.3131" x2="329.262740625" y2="41.32453125" width="0.025" layer="94"/>
<wire x1="329.262740625" y1="41.32453125" x2="329.3999" y2="41.37025" width="0.025" layer="94"/>
<wire x1="329.57135" y1="41.43883125" x2="329.605640625" y2="41.450259375" width="0.025" layer="94"/>
<wire x1="329.605640625" y1="41.450259375" x2="329.834240625" y2="41.564559375" width="0.025" layer="94"/>
<wire x1="329.834240625" y1="41.564559375" x2="329.86853125" y2="41.58741875" width="0.025" layer="94"/>
<wire x1="329.86853125" y1="41.58741875" x2="329.891390625" y2="41.59885" width="0.025" layer="94"/>
<wire x1="330.30286875" y1="41.93031875" x2="330.348590625" y2="41.98746875" width="0.025" layer="94"/>
<wire x1="330.348590625" y1="41.98746875" x2="330.37145" y2="42.01033125" width="0.025" layer="94"/>
<wire x1="330.405740625" y1="42.05605" x2="330.47431875" y2="42.17035" width="0.025" layer="94"/>
<wire x1="330.47431875" y1="42.17035" x2="330.520040625" y2="42.28465" width="0.025" layer="94"/>
<wire x1="330.54251875" y1="42.352090625" x2="330.5429" y2="42.35323125" width="0.025" layer="94"/>
<wire x1="330.5429" y1="42.35323125" x2="330.55433125" y2="42.41038125" width="0.025" layer="94"/>
<wire x1="330.55433125" y1="42.41038125" x2="330.565759375" y2="42.478959375" width="0.025" layer="94"/>
<wire x1="328.72553125" y1="44.22775" x2="328.95413125" y2="44.18203125" width="0.025" layer="94"/>
<wire x1="328.95413125" y1="44.18203125" x2="329.22845" y2="44.11345" width="0.025" layer="94"/>
<wire x1="329.69708125" y1="43.95343125" x2="329.7428" y2="43.93056875" width="0.025" layer="94"/>
<wire x1="329.7428" y1="43.93056875" x2="329.79995" y2="43.907709375" width="0.025" layer="94"/>
<wire x1="329.79995" y1="43.907709375" x2="329.84566875" y2="43.88485" width="0.025" layer="94"/>
<wire x1="329.84566875" y1="43.88485" x2="329.90281875" y2="43.861990625" width="0.025" layer="94"/>
<wire x1="329.90281875" y1="43.861990625" x2="330.03998125" y2="43.793409375" width="0.025" layer="94"/>
<wire x1="330.03998125" y1="43.793409375" x2="330.09713125" y2="43.75911875" width="0.025" layer="94"/>
<wire x1="330.09713125" y1="43.75911875" x2="330.14285" y2="43.736259375" width="0.025" layer="94"/>
<wire x1="330.55433125" y1="43.43908125" x2="330.60005" y2="43.393359375" width="0.025" layer="94"/>
<wire x1="330.60005" y1="43.393359375" x2="330.66863125" y2="43.31335" width="0.025" layer="94"/>
<wire x1="330.66863125" y1="43.31335" x2="330.70291875" y2="43.279059375" width="0.025" layer="94"/>
<wire x1="330.805790625" y1="42.193209375" x2="330.82865" y2="42.23893125" width="0.025" layer="94"/>
<wire x1="330.7715" y1="42.136059375" x2="330.805790625" y2="42.193209375" width="0.025" layer="94"/>
<wire x1="330.70291875" y1="42.04461875" x2="330.7715" y2="42.136059375" width="0.025" layer="94"/>
<wire x1="329.29703125" y1="41.233090625" x2="329.594209375" y2="41.32453125" width="0.025" layer="94"/>
<wire x1="329.1713" y1="41.03878125" x2="329.18273125" y2="41.03878125" width="0.025" layer="94"/>
<wire x1="329.18273125" y1="41.03878125" x2="329.26616875" y2="41.059640625" width="0.025" layer="94"/>
<wire x1="329.26616875" y1="41.059640625" x2="329.50276875" y2="41.118790625" width="0.025" layer="94"/>
<wire x1="329.50276875" y1="41.118790625" x2="329.7428" y2="41.18736875" width="0.025" layer="94"/>
<wire x1="329.7428" y1="41.18736875" x2="329.743940625" y2="41.18775" width="0.025" layer="94"/>
<wire x1="329.948540625" y1="41.25595" x2="330.005690625" y2="41.278809375" width="0.025" layer="94"/>
<wire x1="330.005690625" y1="41.278809375" x2="330.03998125" y2="41.290240625" width="0.025" layer="94"/>
<wire x1="330.60005" y1="41.564559375" x2="330.634340625" y2="41.58741875" width="0.025" layer="94"/>
<wire x1="330.634340625" y1="41.58741875" x2="330.691490625" y2="41.621709375" width="0.025" layer="94"/>
<wire x1="330.691490625" y1="41.621709375" x2="330.737209375" y2="41.656" width="0.025" layer="94"/>
<wire x1="331.2287" y1="42.06748125" x2="331.27441875" y2="42.12463125" width="0.025" layer="94"/>
<wire x1="331.27441875" y1="42.12463125" x2="331.308709375" y2="42.17035" width="0.025" layer="94"/>
<wire x1="331.33156875" y1="42.204640625" x2="331.343" y2="42.2275" width="0.025" layer="94"/>
<wire x1="331.343" y1="42.2275" x2="331.365859375" y2="42.261790625" width="0.025" layer="94"/>
<wire x1="331.50301875" y1="42.97045" x2="331.51445" y2="42.84471875" width="0.025" layer="94"/>
<wire x1="331.365859375" y1="43.3705" x2="331.36643125" y2="43.369359375" width="0.025" layer="94"/>
<wire x1="331.27441875" y1="43.507659375" x2="331.365859375" y2="43.3705" width="0.025" layer="94"/>
<wire x1="331.24013125" y1="43.55338125" x2="331.27441875" y2="43.507659375" width="0.025" layer="94"/>
<wire x1="330.61148125" y1="44.090590625" x2="330.66863125" y2="44.0563" width="0.025" layer="94"/>
<wire x1="330.610340625" y1="44.09135" x2="330.61148125" y2="44.090590625" width="0.025" layer="94"/>
<wire x1="329.95996875" y1="44.3992" x2="330.24571875" y2="44.2849" width="0.025" layer="94"/>
<wire x1="329.651359375" y1="44.50206875" x2="329.95996875" y2="44.3992" width="0.025" layer="94"/>
<wire x1="329.491340625" y1="44.547790625" x2="329.651359375" y2="44.50206875" width="0.025" layer="94"/>
<wire x1="329.21701875" y1="44.61636875" x2="329.491340625" y2="44.547790625" width="0.025" layer="94"/>
<wire x1="328.3712" y1="44.75353125" x2="328.47406875" y2="44.7421" width="0.025" layer="94"/>
<wire x1="328.2569" y1="44.764959375" x2="328.3712" y2="44.75353125" width="0.025" layer="94"/>
<wire x1="328.222609375" y1="44.764959375" x2="328.2569" y2="44.764959375" width="0.025" layer="94"/>
<wire x1="327.58253125" y1="40.78731875" x2="328.519790625" y2="36.21531875" width="0.025" layer="94"/>
<wire x1="326.690990625" y1="36.21531875" x2="327.58253125" y2="40.78731875" width="0.025" layer="94"/>
<wire x1="326.690990625" y1="36.21531875" x2="326.86038125" y2="37.084" width="0.025" layer="94"/>
<wire x1="324.9746" y1="37.084" x2="323.764909375" y2="32.512" width="0.025" layer="94"/>
<wire x1="323.764909375" y1="32.512" x2="325.8566" y2="32.512" width="0.025" layer="94"/>
<wire x1="325.8566" y1="32.512" x2="326.336659375" y2="34.5694" width="0.025" layer="94"/>
<wire x1="326.336659375" y1="34.5694" x2="328.862690625" y2="34.5694" width="0.025" layer="94"/>
<wire x1="328.862690625" y1="34.5694" x2="329.319890625" y2="32.512" width="0.025" layer="94"/>
<wire x1="329.319890625" y1="32.512" x2="331.44586875" y2="32.512" width="0.025" layer="94"/>
<wire x1="331.44586875" y1="32.512" x2="330.22376875" y2="37.084" width="0.025" layer="94"/>
<wire x1="328.341709375" y1="37.084" x2="328.35108125" y2="37.03828125" width="0.025" layer="94"/>
<wire x1="328.35108125" y1="37.03828125" x2="328.519790625" y2="36.21531875" width="0.025" layer="94"/>
<wire x1="328.519790625" y1="36.21531875" x2="326.690990625" y2="36.21531875" width="0.025" layer="94"/>
<wire x1="314.41516875" y1="42.29608125" x2="314.41516875" y2="32.512" width="0.025" layer="94"/>
<wire x1="314.41516875" y1="32.512" x2="316.26683125" y2="32.512" width="0.025" layer="94"/>
<wire x1="316.26683125" y1="32.512" x2="316.26683125" y2="40.21581875" width="0.025" layer="94"/>
<wire x1="316.26683125" y1="40.21581875" x2="317.98133125" y2="32.512" width="0.025" layer="94"/>
<wire x1="317.98133125" y1="32.512" x2="319.55866875" y2="32.512" width="0.025" layer="94"/>
<wire x1="319.55866875" y1="32.512" x2="321.250309375" y2="40.21581875" width="0.025" layer="94"/>
<wire x1="321.250309375" y1="40.21581875" x2="321.250309375" y2="32.512" width="0.025" layer="94"/>
<wire x1="321.250309375" y1="32.512" x2="323.12483125" y2="32.512" width="0.025" layer="94"/>
<wire x1="323.12483125" y1="32.512" x2="323.12483125" y2="42.29608125" width="0.025" layer="94"/>
<wire x1="323.12483125" y1="42.29608125" x2="320.15303125" y2="42.29608125" width="0.025" layer="94"/>
<wire x1="320.15303125" y1="42.29608125" x2="318.767990625" y2="35.575240625" width="0.025" layer="94"/>
<wire x1="318.767990625" y1="35.575240625" x2="317.364109375" y2="42.29608125" width="0.025" layer="94"/>
<wire x1="317.364109375" y1="42.29608125" x2="314.41516875" y2="42.29608125" width="0.025" layer="94"/>
<wire x1="312.83783125" y1="35.392359375" x2="312.83783125" y2="42.3418" width="0.025" layer="94"/>
<wire x1="312.83783125" y1="42.3418" x2="310.78043125" y2="42.3418" width="0.025" layer="94"/>
<wire x1="310.78043125" y1="42.3418" x2="310.78043125" y2="35.1409" width="0.025" layer="94"/>
<wire x1="310.78043125" y1="35.1409" x2="310.75756875" y2="35.0266" width="0.025" layer="94"/>
<wire x1="310.75756875" y1="35.0266" x2="310.71185" y2="34.889440625" width="0.025" layer="94"/>
<wire x1="310.71185" y1="34.889440625" x2="310.63346875" y2="34.75228125" width="0.025" layer="94"/>
<wire x1="310.63346875" y1="34.75228125" x2="310.620409375" y2="34.72941875" width="0.025" layer="94"/>
<wire x1="310.620409375" y1="34.72941875" x2="310.55183125" y2="34.660840625" width="0.025" layer="94"/>
<wire x1="310.55183125" y1="34.660840625" x2="310.506109375" y2="34.619690625" width="0.025" layer="94"/>
<wire x1="310.506109375" y1="34.619690625" x2="310.41466875" y2="34.546540625" width="0.025" layer="94"/>
<wire x1="310.41466875" y1="34.546540625" x2="310.30036875" y2="34.477959375" width="0.025" layer="94"/>
<wire x1="310.30036875" y1="34.477959375" x2="310.18606875" y2="34.432240625" width="0.025" layer="94"/>
<wire x1="310.18606875" y1="34.432240625" x2="310.02605" y2="34.38651875" width="0.025" layer="94"/>
<wire x1="310.02605" y1="34.38651875" x2="309.84316875" y2="34.363659375" width="0.025" layer="94"/>
<wire x1="309.84316875" y1="34.363659375" x2="309.660290625" y2="34.363659375" width="0.025" layer="94"/>
<wire x1="309.660290625" y1="34.363659375" x2="309.56885" y2="34.3789" width="0.025" layer="94"/>
<wire x1="309.56885" y1="34.3789" x2="309.52313125" y2="34.38651875" width="0.025" layer="94"/>
<wire x1="309.52313125" y1="34.38651875" x2="309.477409375" y2="34.39958125" width="0.025" layer="94"/>
<wire x1="309.477409375" y1="34.39958125" x2="309.431690625" y2="34.41265" width="0.025" layer="94"/>
<wire x1="309.431690625" y1="34.41265" x2="309.363109375" y2="34.432240625" width="0.025" layer="94"/>
<wire x1="309.363109375" y1="34.432240625" x2="309.22595" y2="34.50081875" width="0.025" layer="94"/>
<wire x1="309.22595" y1="34.50081875" x2="309.088790625" y2="34.592259375" width="0.025" layer="94"/>
<wire x1="309.088790625" y1="34.592259375" x2="308.95163125" y2="34.72941875" width="0.025" layer="94"/>
<wire x1="308.95163125" y1="34.72941875" x2="308.70733125" y2="35.05515" width="0.025" layer="94"/>
<wire x1="308.70733125" y1="35.05515" x2="307.145690625" y2="33.81501875" width="0.025" layer="94"/>
<wire x1="307.145690625" y1="33.81501875" x2="307.259990625" y2="33.58641875" width="0.025" layer="94"/>
<wire x1="307.259990625" y1="33.58641875" x2="307.305709375" y2="33.525459375" width="0.025" layer="94"/>
<wire x1="307.305709375" y1="33.525459375" x2="307.34571875" y2="33.47211875" width="0.025" layer="94"/>
<wire x1="307.34571875" y1="33.47211875" x2="307.39715" y2="33.403540625" width="0.025" layer="94"/>
<wire x1="307.39715" y1="33.403540625" x2="307.43935" y2="33.35781875" width="0.025" layer="94"/>
<wire x1="307.43935" y1="33.35781875" x2="307.488590625" y2="33.30448125" width="0.025" layer="94"/>
<wire x1="307.488590625" y1="33.30448125" x2="307.523759375" y2="33.26638125" width="0.025" layer="94"/>
<wire x1="307.523759375" y1="33.26638125" x2="307.55716875" y2="33.23018125" width="0.025" layer="94"/>
<wire x1="307.55716875" y1="33.23018125" x2="307.587059375" y2="33.1978" width="0.025" layer="94"/>
<wire x1="307.587059375" y1="33.1978" x2="307.62575" y2="33.155890625" width="0.025" layer="94"/>
<wire x1="307.62575" y1="33.155890625" x2="307.65036875" y2="33.12921875" width="0.025" layer="94"/>
<wire x1="307.65036875" y1="33.12921875" x2="307.67146875" y2="33.106359375" width="0.025" layer="94"/>
<wire x1="307.67146875" y1="33.106359375" x2="307.762909375" y2="33.02323125" width="0.025" layer="94"/>
<wire x1="307.762909375" y1="33.02323125" x2="307.7972" y2="32.992059375" width="0.025" layer="94"/>
<wire x1="307.7972" y1="32.992059375" x2="307.85435" y2="32.940109375" width="0.025" layer="94"/>
<wire x1="307.85435" y1="32.940109375" x2="307.89778125" y2="32.90061875" width="0.025" layer="94"/>
<wire x1="307.89778125" y1="32.90061875" x2="307.92293125" y2="32.877759375" width="0.025" layer="94"/>
<wire x1="307.92293125" y1="32.877759375" x2="308.12866875" y2="32.7406" width="0.025" layer="94"/>
<wire x1="308.12866875" y1="32.7406" x2="308.174390625" y2="32.71011875" width="0.025" layer="94"/>
<wire x1="308.174390625" y1="32.71011875" x2="308.24296875" y2="32.6705" width="0.025" layer="94"/>
<wire x1="308.24296875" y1="32.6705" x2="308.288690625" y2="32.646109375" width="0.025" layer="94"/>
<wire x1="308.288690625" y1="32.646109375" x2="308.3687" y2="32.603440625" width="0.025" layer="94"/>
<wire x1="308.3687" y1="32.603440625" x2="308.42585" y2="32.572959375" width="0.025" layer="94"/>
<wire x1="308.42585" y1="32.572959375" x2="308.54015" y2="32.512" width="0.025" layer="94"/>
<wire x1="308.54015" y1="32.512" x2="308.66206875" y2="32.46628125" width="0.025" layer="94"/>
<wire x1="308.66206875" y1="32.46628125" x2="308.72303125" y2="32.44341875" width="0.025" layer="94"/>
<wire x1="308.72303125" y1="32.44341875" x2="308.905909375" y2="32.374840625" width="0.025" layer="94"/>
<wire x1="308.905909375" y1="32.374840625" x2="308.99735" y2="32.353740625" width="0.025" layer="94"/>
<wire x1="308.99735" y1="32.353740625" x2="309.203090625" y2="32.306259375" width="0.025" layer="94"/>
<wire x1="309.203090625" y1="32.306259375" x2="309.27166875" y2="32.295709375" width="0.025" layer="94"/>
<wire x1="309.27166875" y1="32.295709375" x2="309.35168125" y2="32.2834" width="0.025" layer="94"/>
<wire x1="309.35168125" y1="32.2834" x2="309.40883125" y2="32.274609375" width="0.025" layer="94"/>
<wire x1="309.40883125" y1="32.274609375" x2="309.477409375" y2="32.264059375" width="0.025" layer="94"/>
<wire x1="309.477409375" y1="32.264059375" x2="309.50026875" y2="32.260540625" width="0.025" layer="94"/>
<wire x1="309.50026875" y1="32.260540625" x2="309.68315" y2="32.2453" width="0.025" layer="94"/>
<wire x1="309.68315" y1="32.2453" x2="309.774590625" y2="32.23768125" width="0.025" layer="94"/>
<wire x1="309.774590625" y1="32.23768125" x2="310.048909375" y2="32.260540625" width="0.025" layer="94"/>
<wire x1="310.048909375" y1="32.260540625" x2="310.231790625" y2="32.2834" width="0.025" layer="94"/>
<wire x1="310.231790625" y1="32.2834" x2="310.460390625" y2="32.32911875" width="0.025" layer="94"/>
<wire x1="310.460390625" y1="32.32911875" x2="310.620409375" y2="32.374840625" width="0.025" layer="94"/>
<wire x1="310.620409375" y1="32.374840625" x2="310.82615" y2="32.44341875" width="0.025" layer="94"/>
<wire x1="310.82615" y1="32.44341875" x2="311.05475" y2="32.534859375" width="0.025" layer="94"/>
<wire x1="311.05475" y1="32.534859375" x2="311.374790625" y2="32.69488125" width="0.025" layer="94"/>
<wire x1="311.374790625" y1="32.69488125" x2="311.58053125" y2="32.832040625" width="0.025" layer="94"/>
<wire x1="311.58053125" y1="32.832040625" x2="311.649109375" y2="32.889190625" width="0.025" layer="94"/>
<wire x1="311.649109375" y1="32.889190625" x2="311.69483125" y2="32.927290625" width="0.025" layer="94"/>
<wire x1="311.69483125" y1="32.927290625" x2="311.763409375" y2="32.984440625" width="0.025" layer="94"/>
<wire x1="311.763409375" y1="32.984440625" x2="311.85485" y2="33.060640625" width="0.025" layer="94"/>
<wire x1="311.85485" y1="33.060640625" x2="312.106309375" y2="33.3121" width="0.025" layer="94"/>
<wire x1="312.106309375" y1="33.3121" x2="312.15203125" y2="33.373059375" width="0.025" layer="94"/>
<wire x1="312.15203125" y1="33.373059375" x2="312.174890625" y2="33.403540625" width="0.025" layer="94"/>
<wire x1="312.174890625" y1="33.403540625" x2="312.220609375" y2="33.4645" width="0.025" layer="94"/>
<wire x1="312.220609375" y1="33.4645" x2="312.258709375" y2="33.517840625" width="0.025" layer="94"/>
<wire x1="312.258709375" y1="33.517840625" x2="312.30443125" y2="33.58641875" width="0.025" layer="94"/>
<wire x1="312.30443125" y1="33.58641875" x2="312.334909375" y2="33.632140625" width="0.025" layer="94"/>
<wire x1="312.334909375" y1="33.632140625" x2="312.39586875" y2="33.72358125" width="0.025" layer="94"/>
<wire x1="312.39586875" y1="33.72358125" x2="312.42635" y2="33.7693" width="0.025" layer="94"/>
<wire x1="312.42635" y1="33.7693" x2="312.60923125" y2="34.135059375" width="0.025" layer="94"/>
<wire x1="312.60923125" y1="34.135059375" x2="312.65495" y2="34.249359375" width="0.025" layer="94"/>
<wire x1="312.65495" y1="34.249359375" x2="312.68108125" y2="34.3408" width="0.025" layer="94"/>
<wire x1="312.68108125" y1="34.3408" x2="312.7121" y2="34.4551" width="0.025" layer="94"/>
<wire x1="312.7121" y1="34.4551" x2="312.746390625" y2="34.61511875" width="0.025" layer="94"/>
<wire x1="312.746390625" y1="34.61511875" x2="312.76925" y2="34.72941875" width="0.025" layer="94"/>
<wire x1="312.76925" y1="34.72941875" x2="312.792109375" y2="34.889440625" width="0.025" layer="94"/>
<wire x1="312.792109375" y1="34.889440625" x2="312.809890625" y2="35.049459375" width="0.025" layer="94"/>
<wire x1="312.809890625" y1="35.049459375" x2="312.81673125" y2="35.118040625" width="0.025" layer="94"/>
<wire x1="312.81673125" y1="35.118040625" x2="312.823759375" y2="35.20948125" width="0.025" layer="94"/>
<wire x1="312.823759375" y1="35.20948125" x2="312.829040625" y2="35.278059375" width="0.025" layer="94"/>
<wire x1="312.829040625" y1="35.278059375" x2="312.83783125" y2="35.392359375" width="0.025" layer="94"/>
<wire x1="324.9625" y1="37.03828125" x2="325.251490625" y2="38.13625" width="0.025" layer="94"/>
<wire x1="325.251490625" y1="38.13625" x2="325.993759375" y2="40.935909375" width="0.025" layer="94"/>
<wire x1="325.993759375" y1="40.935909375" x2="325.90231875" y2="40.95876875" width="0.025" layer="94"/>
<wire x1="325.90231875" y1="40.95876875" x2="325.78801875" y2="40.98163125" width="0.025" layer="94"/>
<wire x1="325.78801875" y1="40.98163125" x2="325.63943125" y2="41.01591875" width="0.025" layer="94"/>
<wire x1="325.63943125" y1="41.01591875" x2="325.536559375" y2="41.03878125" width="0.025" layer="94"/>
<wire x1="325.536559375" y1="41.03878125" x2="325.3994" y2="41.07306875" width="0.025" layer="94"/>
<wire x1="325.3994" y1="41.07306875" x2="325.319390625" y2="41.09593125" width="0.025" layer="94"/>
<wire x1="325.319390625" y1="41.09593125" x2="325.21651875" y2="41.13021875" width="0.025" layer="94"/>
<wire x1="325.21651875" y1="41.13021875" x2="325.06793125" y2="41.175940625" width="0.025" layer="94"/>
<wire x1="325.06793125" y1="41.175940625" x2="324.97076875" y2="41.21251875" width="0.025" layer="94"/>
<wire x1="324.97076875" y1="41.21251875" x2="324.747890625" y2="41.30166875" width="0.025" layer="94"/>
<wire x1="324.747890625" y1="41.30166875" x2="324.565009375" y2="41.38168125" width="0.025" layer="94"/>
<wire x1="324.565009375" y1="41.38168125" x2="324.45185" y2="41.438259375" width="0.025" layer="94"/>
<wire x1="324.45185" y1="41.438259375" x2="324.393559375" y2="41.47311875" width="0.025" layer="94"/>
<wire x1="324.393559375" y1="41.47311875" x2="324.347840625" y2="41.49598125" width="0.025" layer="94"/>
<wire x1="324.347840625" y1="41.49598125" x2="324.18781875" y2="41.58741875" width="0.025" layer="94"/>
<wire x1="324.18781875" y1="41.58741875" x2="324.08495" y2="41.656" width="0.025" layer="94"/>
<wire x1="324.08495" y1="41.656" x2="323.85635" y2="41.82745" width="0.025" layer="94"/>
<wire x1="323.85635" y1="41.82745" x2="323.776340625" y2="41.89603125" width="0.025" layer="94"/>
<wire x1="323.776340625" y1="41.89603125" x2="323.662040625" y2="42.01033125" width="0.025" layer="94"/>
<wire x1="323.662040625" y1="42.01033125" x2="323.593459375" y2="42.090340625" width="0.025" layer="94"/>
<wire x1="323.593459375" y1="42.090340625" x2="323.547740625" y2="42.147490625" width="0.025" layer="94"/>
<wire x1="323.547740625" y1="42.147490625" x2="323.52488125" y2="42.18178125" width="0.025" layer="94"/>
<wire x1="323.52488125" y1="42.18178125" x2="323.490590625" y2="42.2275" width="0.025" layer="94"/>
<wire x1="323.490590625" y1="42.2275" x2="323.46773125" y2="42.261790625" width="0.025" layer="94"/>
<wire x1="323.46773125" y1="42.261790625" x2="323.433440625" y2="42.318940625" width="0.025" layer="94"/>
<wire x1="323.433440625" y1="42.318940625" x2="323.41058125" y2="42.35323125" width="0.025" layer="94"/>
<wire x1="323.41058125" y1="42.35323125" x2="323.354" y2="42.466390625" width="0.025" layer="94"/>
<wire x1="323.354" y1="42.466390625" x2="323.33056875" y2="42.52468125" width="0.025" layer="94"/>
<wire x1="323.33056875" y1="42.52468125" x2="323.29628125" y2="42.62755" width="0.025" layer="94"/>
<wire x1="323.29628125" y1="42.62755" x2="323.28485" y2="42.67326875" width="0.025" layer="94"/>
<wire x1="323.28485" y1="42.67326875" x2="323.27341875" y2="42.73041875" width="0.025" layer="94"/>
<wire x1="323.27341875" y1="42.73041875" x2="323.261990625" y2="42.81043125" width="0.025" layer="94"/>
<wire x1="323.261990625" y1="42.81043125" x2="323.250559375" y2="42.936159375" width="0.025" layer="94"/>
<wire x1="323.250559375" y1="42.936159375" x2="323.261990625" y2="43.061890625" width="0.025" layer="94"/>
<wire x1="323.261990625" y1="43.061890625" x2="323.27341875" y2="43.13046875" width="0.025" layer="94"/>
<wire x1="323.27341875" y1="43.13046875" x2="323.28485" y2="43.18761875" width="0.025" layer="94"/>
<wire x1="323.28485" y1="43.18761875" x2="323.29628125" y2="43.233340625" width="0.025" layer="94"/>
<wire x1="323.29628125" y1="43.233340625" x2="323.33056875" y2="43.336209375" width="0.025" layer="94"/>
<wire x1="323.33056875" y1="43.336209375" x2="323.35343125" y2="43.393359375" width="0.025" layer="94"/>
<wire x1="323.35343125" y1="43.393359375" x2="323.41058125" y2="43.507659375" width="0.025" layer="94"/>
<wire x1="323.41058125" y1="43.507659375" x2="323.44486875" y2="43.564809375" width="0.025" layer="94"/>
<wire x1="323.44486875" y1="43.564809375" x2="323.479159375" y2="43.61053125" width="0.025" layer="94"/>
<wire x1="323.479159375" y1="43.61053125" x2="323.536309375" y2="43.690540625" width="0.025" layer="94"/>
<wire x1="323.536309375" y1="43.690540625" x2="323.62775" y2="43.793409375" width="0.025" layer="94"/>
<wire x1="323.62775" y1="43.793409375" x2="323.78776875" y2="43.95343125" width="0.025" layer="94"/>
<wire x1="323.78776875" y1="43.95343125" x2="323.87806875" y2="44.0323" width="0.025" layer="94"/>
<wire x1="323.87806875" y1="44.0323" x2="323.9375" y2="44.08001875" width="0.025" layer="94"/>
<wire x1="323.9375" y1="44.08001875" x2="324.0278" y2="44.147740625" width="0.025" layer="94"/>
<wire x1="324.0278" y1="44.147740625" x2="324.164959375" y2="44.23918125" width="0.025" layer="94"/>
<wire x1="324.164959375" y1="44.23918125" x2="324.290690625" y2="44.319190625" width="0.025" layer="94"/>
<wire x1="324.290690625" y1="44.319190625" x2="324.393559375" y2="44.376340625" width="0.025" layer="94"/>
<wire x1="324.393559375" y1="44.376340625" x2="324.62101875" y2="44.49006875" width="0.025" layer="94"/>
<wire x1="324.62101875" y1="44.49006875" x2="324.72503125" y2="44.536359375" width="0.025" layer="94"/>
<wire x1="324.72503125" y1="44.536359375" x2="325.009640625" y2="44.6502" width="0.025" layer="94"/>
<wire x1="325.009640625" y1="44.6502" x2="325.10908125" y2="44.68343125" width="0.025" layer="94"/>
<wire x1="325.10908125" y1="44.68343125" x2="325.287390625" y2="44.742859375" width="0.025" layer="94"/>
<wire x1="325.287390625" y1="44.742859375" x2="325.35368125" y2="44.764959375" width="0.025" layer="94"/>
<wire x1="325.35368125" y1="44.764959375" x2="325.43483125" y2="44.7881" width="0.025" layer="94"/>
<wire x1="325.43483125" y1="44.7881" x2="325.5137" y2="44.81068125" width="0.025" layer="94"/>
<wire x1="325.5137" y1="44.81068125" x2="325.741159375" y2="44.867540625" width="0.025" layer="94"/>
<wire x1="325.741159375" y1="44.867540625" x2="325.84516875" y2="44.890690625" width="0.025" layer="94"/>
<wire x1="325.84516875" y1="44.890690625" x2="326.07376875" y2="44.936409375" width="0.025" layer="94"/>
<wire x1="326.07376875" y1="44.936409375" x2="326.14235" y2="44.947840625" width="0.025" layer="94"/>
<wire x1="326.14235" y1="44.947840625" x2="326.222359375" y2="44.95926875" width="0.025" layer="94"/>
<wire x1="326.222359375" y1="44.95926875" x2="326.35951875" y2="44.98213125" width="0.025" layer="94"/>
<wire x1="326.35951875" y1="44.98213125" x2="326.43953125" y2="44.993559375" width="0.025" layer="94"/>
<wire x1="326.43953125" y1="44.993559375" x2="326.622409375" y2="45.01641875" width="0.025" layer="94"/>
<wire x1="326.622409375" y1="45.01641875" x2="326.72528125" y2="45.02785" width="0.025" layer="94"/>
<wire x1="326.72528125" y1="45.02785" x2="326.862440625" y2="45.03928125" width="0.025" layer="94"/>
<wire x1="326.862440625" y1="45.03928125" x2="326.862440625" y2="44.764959375" width="0.025" layer="94"/>
<wire x1="326.862440625" y1="44.764959375" x2="326.75956875" y2="44.75353125" width="0.025" layer="94"/>
<wire x1="326.75956875" y1="44.75353125" x2="326.519540625" y2="44.719240625" width="0.025" layer="94"/>
<wire x1="326.519540625" y1="44.719240625" x2="326.450959375" y2="44.707809375" width="0.025" layer="94"/>
<wire x1="326.450959375" y1="44.707809375" x2="326.32523125" y2="44.68495" width="0.025" layer="94"/>
<wire x1="326.32523125" y1="44.68495" x2="326.09663125" y2="44.63923125" width="0.025" layer="94"/>
<wire x1="326.09663125" y1="44.63923125" x2="325.993759375" y2="44.61636875" width="0.025" layer="94"/>
<wire x1="325.993759375" y1="44.61636875" x2="325.84516875" y2="44.58208125" width="0.025" layer="94"/>
<wire x1="325.84516875" y1="44.58208125" x2="325.67371875" y2="44.536359375" width="0.025" layer="94"/>
<wire x1="325.67371875" y1="44.536359375" x2="325.520559375" y2="44.4926" width="0.025" layer="94"/>
<wire x1="325.520559375" y1="44.4926" x2="325.4154" y2="44.45786875" width="0.025" layer="94"/>
<wire x1="325.4154" y1="44.45786875" x2="325.23938125" y2="44.3992" width="0.025" layer="94"/>
<wire x1="325.23938125" y1="44.3992" x2="325.06793125" y2="44.33061875" width="0.025" layer="94"/>
<wire x1="325.06793125" y1="44.33061875" x2="324.93076875" y2="44.27346875" width="0.025" layer="94"/>
<wire x1="324.93076875" y1="44.27346875" x2="324.679309375" y2="44.147740625" width="0.025" layer="94"/>
<wire x1="324.679309375" y1="44.147740625" x2="324.531859375" y2="44.05695" width="0.025" layer="94"/>
<wire x1="324.531859375" y1="44.05695" x2="324.462140625" y2="44.01058125" width="0.025" layer="94"/>
<wire x1="324.462140625" y1="44.01058125" x2="324.279259375" y2="43.87341875" width="0.025" layer="94"/>
<wire x1="324.279259375" y1="43.87341875" x2="324.164959375" y2="43.77055" width="0.025" layer="94"/>
<wire x1="324.164959375" y1="43.77055" x2="324.03923125" y2="43.64481875" width="0.025" layer="94"/>
<wire x1="324.03923125" y1="43.64481875" x2="323.97065" y2="43.564809375" width="0.025" layer="94"/>
<wire x1="323.97065" y1="43.564809375" x2="323.90206875" y2="43.47336875" width="0.025" layer="94"/>
<wire x1="323.90206875" y1="43.47336875" x2="323.86778125" y2="43.41621875" width="0.025" layer="94"/>
<wire x1="323.86778125" y1="43.41621875" x2="323.822059375" y2="43.336209375" width="0.025" layer="94"/>
<wire x1="323.822059375" y1="43.336209375" x2="323.78776875" y2="43.26763125" width="0.025" layer="94"/>
<wire x1="323.78776875" y1="43.26763125" x2="323.75348125" y2="43.176190625" width="0.025" layer="94"/>
<wire x1="323.75348125" y1="43.176190625" x2="323.73061875" y2="43.107609375" width="0.025" layer="94"/>
<wire x1="323.73061875" y1="43.107609375" x2="323.707759375" y2="43.004740625" width="0.025" layer="94"/>
<wire x1="323.707759375" y1="43.004740625" x2="323.69633125" y2="42.90186875" width="0.025" layer="94"/>
<wire x1="323.69633125" y1="42.90186875" x2="323.6849" y2="42.821859375" width="0.025" layer="94"/>
<wire x1="323.6849" y1="42.821859375" x2="323.6849" y2="42.776140625" width="0.025" layer="94"/>
<wire x1="323.6849" y1="42.776140625" x2="323.69633125" y2="42.650409375" width="0.025" layer="94"/>
<wire x1="323.69633125" y1="42.650409375" x2="323.73061875" y2="42.51325" width="0.025" layer="94"/>
<wire x1="323.73061875" y1="42.51325" x2="323.75348125" y2="42.433240625" width="0.025" layer="94"/>
<wire x1="323.75348125" y1="42.433240625" x2="323.7992" y2="42.33036875" width="0.025" layer="94"/>
<wire x1="323.7992" y1="42.33036875" x2="323.822059375" y2="42.28465" width="0.025" layer="94"/>
<wire x1="323.822059375" y1="42.28465" x2="323.890640625" y2="42.18178125" width="0.025" layer="94"/>
<wire x1="323.890640625" y1="42.18178125" x2="323.95921875" y2="42.090340625" width="0.025" layer="94"/>
<wire x1="323.95921875" y1="42.090340625" x2="324.004940625" y2="42.033190625" width="0.025" layer="94"/>
<wire x1="324.004940625" y1="42.033190625" x2="324.08495" y2="41.95318125" width="0.025" layer="94"/>
<wire x1="324.08495" y1="41.95318125" x2="324.176390625" y2="41.87316875" width="0.025" layer="94"/>
<wire x1="324.176390625" y1="41.87316875" x2="324.24496875" y2="41.81601875" width="0.025" layer="94"/>
<wire x1="324.24496875" y1="41.81601875" x2="324.30211875" y2="41.7703" width="0.025" layer="94"/>
<wire x1="324.30211875" y1="41.7703" x2="324.347840625" y2="41.736009375" width="0.025" layer="94"/>
<wire x1="324.347840625" y1="41.736009375" x2="324.41641875" y2="41.690290625" width="0.025" layer="94"/>
<wire x1="324.41641875" y1="41.690290625" x2="324.53071875" y2="41.61028125" width="0.025" layer="94"/>
<wire x1="324.53071875" y1="41.61028125" x2="324.64501875" y2="41.5417" width="0.025" layer="94"/>
<wire x1="324.64501875" y1="41.5417" x2="324.78218125" y2="41.461690625" width="0.025" layer="94"/>
<wire x1="324.78218125" y1="41.461690625" x2="324.919340625" y2="41.393109375" width="0.025" layer="94"/>
<wire x1="324.919340625" y1="41.393109375" x2="325.022209375" y2="41.347390625" width="0.025" layer="94"/>
<wire x1="325.022209375" y1="41.347390625" x2="325.079359375" y2="41.32453125" width="0.025" layer="94"/>
<wire x1="325.079359375" y1="41.32453125" x2="325.15936875" y2="41.290240625" width="0.025" layer="94"/>
<wire x1="325.15936875" y1="41.290240625" x2="325.250809375" y2="41.25595" width="0.025" layer="94"/>
<wire x1="325.250809375" y1="41.25595" x2="325.38796875" y2="41.21023125" width="0.025" layer="94"/>
<wire x1="325.38796875" y1="41.21023125" x2="325.46798125" y2="41.18736875" width="0.025" layer="94"/>
<wire x1="325.46798125" y1="41.18736875" x2="325.58228125" y2="41.15308125" width="0.025" layer="94"/>
<wire x1="325.58228125" y1="41.15308125" x2="325.708009375" y2="41.118790625" width="0.025" layer="94"/>
<wire x1="325.708009375" y1="41.118790625" x2="325.82116875" y2="41.087640625" width="0.025" layer="94"/>
<wire x1="325.82116875" y1="41.087640625" x2="326.01661875" y2="41.03878125" width="0.025" layer="94"/>
<wire x1="326.01661875" y1="41.03878125" x2="326.062340625" y2="41.18736875" width="0.025" layer="94"/>
<wire x1="326.062340625" y1="41.18736875" x2="326.02805" y2="41.1988" width="0.025" layer="94"/>
<wire x1="326.02805" y1="41.1988" x2="325.78801875" y2="41.26738125" width="0.025" layer="94"/>
<wire x1="325.78801875" y1="41.26738125" x2="325.5137" y2="41.35881875" width="0.025" layer="94"/>
<wire x1="325.5137" y1="41.35881875" x2="325.433690625" y2="41.393109375" width="0.025" layer="94"/>
<wire x1="325.433690625" y1="41.393109375" x2="325.319390625" y2="41.43883125" width="0.025" layer="94"/>
<wire x1="325.319390625" y1="41.43883125" x2="325.233659375" y2="41.481690625" width="0.025" layer="94"/>
<wire x1="325.233659375" y1="41.481690625" x2="325.10221875" y2="41.55313125" width="0.025" layer="94"/>
<wire x1="325.10221875" y1="41.55313125" x2="325.01078125" y2="41.614090625" width="0.025" layer="94"/>
<wire x1="325.01078125" y1="41.614090625" x2="324.93076875" y2="41.66743125" width="0.025" layer="94"/>
<wire x1="324.93076875" y1="41.66743125" x2="324.88505" y2="41.70171875" width="0.025" layer="94"/>
<wire x1="324.88505" y1="41.70171875" x2="324.8279" y2="41.747440625" width="0.025" layer="94"/>
<wire x1="324.8279" y1="41.747440625" x2="324.78218125" y2="41.78173125" width="0.025" layer="94"/>
<wire x1="324.78218125" y1="41.78173125" x2="324.66788125" y2="41.87316875" width="0.025" layer="94"/>
<wire x1="324.66788125" y1="41.87316875" x2="324.507859375" y2="42.033190625" width="0.025" layer="94"/>
<wire x1="324.507859375" y1="42.033190625" x2="324.47356875" y2="42.078909375" width="0.025" layer="94"/>
<wire x1="324.47356875" y1="42.078909375" x2="324.42785" y2="42.147490625" width="0.025" layer="94"/>
<wire x1="324.42785" y1="42.147490625" x2="324.393559375" y2="42.204640625" width="0.025" layer="94"/>
<wire x1="324.393559375" y1="42.204640625" x2="324.3707" y2="42.250359375" width="0.025" layer="94"/>
<wire x1="324.3707" y1="42.250359375" x2="324.336409375" y2="42.33036875" width="0.025" layer="94"/>
<wire x1="324.336409375" y1="42.33036875" x2="324.31355" y2="42.41038125" width="0.025" layer="94"/>
<wire x1="324.31355" y1="42.41038125" x2="324.30211875" y2="42.44466875" width="0.025" layer="94"/>
<wire x1="324.30211875" y1="42.44466875" x2="324.290690625" y2="42.50181875" width="0.025" layer="94"/>
<wire x1="324.290690625" y1="42.50181875" x2="324.279259375" y2="42.604690625" width="0.025" layer="94"/>
<wire x1="324.279259375" y1="42.604690625" x2="324.279259375" y2="42.764709375" width="0.025" layer="94"/>
<wire x1="324.279259375" y1="42.764709375" x2="324.290690625" y2="42.84471875" width="0.025" layer="94"/>
<wire x1="324.290690625" y1="42.84471875" x2="324.30211875" y2="42.90186875" width="0.025" layer="94"/>
<wire x1="324.30211875" y1="42.90186875" x2="324.347840625" y2="43.03903125" width="0.025" layer="94"/>
<wire x1="324.347840625" y1="43.03903125" x2="324.38213125" y2="43.107609375" width="0.025" layer="94"/>
<wire x1="324.38213125" y1="43.107609375" x2="324.41641875" y2="43.164759375" width="0.025" layer="94"/>
<wire x1="324.41641875" y1="43.164759375" x2="324.519290625" y2="43.30191875" width="0.025" layer="94"/>
<wire x1="324.519290625" y1="43.30191875" x2="324.70216875" y2="43.4848" width="0.025" layer="94"/>
<wire x1="324.70216875" y1="43.4848" x2="324.77075" y2="43.54195" width="0.025" layer="94"/>
<wire x1="324.77075" y1="43.54195" x2="324.81646875" y2="43.576240625" width="0.025" layer="94"/>
<wire x1="324.81646875" y1="43.576240625" x2="324.98678125" y2="43.68978125" width="0.025" layer="94"/>
<wire x1="324.98678125" y1="43.68978125" x2="325.10108125" y2="43.75843125" width="0.025" layer="94"/>
<wire x1="325.10108125" y1="43.75843125" x2="325.177659375" y2="43.796840625" width="0.025" layer="94"/>
<wire x1="325.177659375" y1="43.796840625" x2="325.376540625" y2="43.89628125" width="0.025" layer="94"/>
<wire x1="325.376540625" y1="43.89628125" x2="325.45655" y2="43.93056875" width="0.025" layer="94"/>
<wire x1="325.45655" y1="43.93056875" x2="325.53426875" y2="43.96143125" width="0.025" layer="94"/>
<wire x1="325.53426875" y1="43.96143125" x2="325.68515" y2="44.022009375" width="0.025" layer="94"/>
<wire x1="325.68515" y1="44.022009375" x2="325.82116875" y2="44.06735" width="0.025" layer="94"/>
<wire x1="325.82116875" y1="44.06735" x2="325.94346875" y2="44.10235" width="0.025" layer="94"/>
<wire x1="325.94346875" y1="44.10235" x2="326.06348125" y2="44.136590625" width="0.025" layer="94"/>
<wire x1="326.06348125" y1="44.136590625" x2="326.15378125" y2="44.15916875" width="0.025" layer="94"/>
<wire x1="326.15378125" y1="44.15916875" x2="326.230359375" y2="44.17631875" width="0.025" layer="94"/>
<wire x1="326.230359375" y1="44.17631875" x2="326.35951875" y2="44.204890625" width="0.025" layer="94"/>
<wire x1="326.35951875" y1="44.204890625" x2="326.47381875" y2="44.22775" width="0.025" layer="94"/>
<wire x1="326.47381875" y1="44.22775" x2="326.622409375" y2="44.250609375" width="0.025" layer="94"/>
<wire x1="326.622409375" y1="44.250609375" x2="326.78243125" y2="44.27346875" width="0.025" layer="94"/>
<wire x1="326.78243125" y1="44.27346875" x2="326.89673125" y2="44.2849" width="0.025" layer="94"/>
<wire x1="326.89673125" y1="44.2849" x2="327.091040625" y2="44.29633125" width="0.025" layer="94"/>
<wire x1="327.091040625" y1="44.29633125" x2="327.091040625" y2="44.033440625" width="0.025" layer="94"/>
<wire x1="327.091040625" y1="44.033440625" x2="326.919590625" y2="44.033440625" width="0.025" layer="94"/>
<wire x1="326.919590625" y1="44.033440625" x2="326.793859375" y2="44.022009375" width="0.025" layer="94"/>
<wire x1="326.793859375" y1="44.022009375" x2="326.71385" y2="44.01058125" width="0.025" layer="94"/>
<wire x1="326.71385" y1="44.01058125" x2="326.576690625" y2="43.98771875" width="0.025" layer="94"/>
<wire x1="326.576690625" y1="43.98771875" x2="326.462390625" y2="43.964859375" width="0.025" layer="94"/>
<wire x1="326.462390625" y1="43.964859375" x2="326.28065" y2="43.91941875" width="0.025" layer="94"/>
<wire x1="326.28065" y1="43.91941875" x2="326.12063125" y2="43.87375" width="0.025" layer="94"/>
<wire x1="326.12063125" y1="43.87375" x2="326.02805" y2="43.850559375" width="0.025" layer="94"/>
<wire x1="326.02805" y1="43.850559375" x2="325.8566" y2="43.793409375" width="0.025" layer="94"/>
<wire x1="325.8566" y1="43.793409375" x2="325.708009375" y2="43.73396875" width="0.025" layer="94"/>
<wire x1="325.708009375" y1="43.73396875" x2="325.604" y2="43.68996875" width="0.025" layer="94"/>
<wire x1="325.604" y1="43.68996875" x2="325.536559375" y2="43.65625" width="0.025" layer="94"/>
<wire x1="325.536559375" y1="43.65625" x2="325.45655" y2="43.61053125" width="0.025" layer="94"/>
<wire x1="325.45655" y1="43.61053125" x2="325.341109375" y2="43.541190625" width="0.025" layer="94"/>
<wire x1="325.341109375" y1="43.541190625" x2="325.205090625" y2="43.450509375" width="0.025" layer="94"/>
<wire x1="325.205090625" y1="43.450509375" x2="325.11365" y2="43.38193125" width="0.025" layer="94"/>
<wire x1="325.11365" y1="43.38193125" x2="325.033640625" y2="43.32478125" width="0.025" layer="94"/>
<wire x1="325.033640625" y1="43.32478125" x2="324.86133125" y2="43.152190625" width="0.025" layer="94"/>
<wire x1="324.86133125" y1="43.152190625" x2="324.828759375" y2="43.10875" width="0.025" layer="94"/>
<wire x1="324.828759375" y1="43.10875" x2="324.75931875" y2="43.004740625" width="0.025" layer="94"/>
<wire x1="324.75931875" y1="43.004740625" x2="324.70216875" y2="42.890440625" width="0.025" layer="94"/>
<wire x1="324.70216875" y1="42.890440625" x2="324.66788125" y2="42.78756875" width="0.025" layer="94"/>
<wire x1="324.66788125" y1="42.78756875" x2="324.65645" y2="42.74185" width="0.025" layer="94"/>
<wire x1="324.65645" y1="42.74185" x2="324.633590625" y2="42.626409375" width="0.025" layer="94"/>
<wire x1="324.633590625" y1="42.626409375" x2="324.633590625" y2="42.50181875" width="0.025" layer="94"/>
<wire x1="324.633590625" y1="42.50181875" x2="324.64501875" y2="42.421809375" width="0.025" layer="94"/>
<wire x1="324.64501875" y1="42.421809375" x2="324.66788125" y2="42.33036875" width="0.025" layer="94"/>
<wire x1="324.66788125" y1="42.33036875" x2="324.690740625" y2="42.261790625" width="0.025" layer="94"/>
<wire x1="324.690740625" y1="42.261790625" x2="324.7136" y2="42.204640625" width="0.025" layer="94"/>
<wire x1="324.7136" y1="42.204640625" x2="324.747890625" y2="42.147490625" width="0.025" layer="94"/>
<wire x1="324.747890625" y1="42.147490625" x2="324.793609375" y2="42.06748125" width="0.025" layer="94"/>
<wire x1="324.793609375" y1="42.06748125" x2="324.874759375" y2="41.96346875" width="0.025" layer="94"/>
<wire x1="324.874759375" y1="41.96346875" x2="324.976490625" y2="41.861740625" width="0.025" layer="94"/>
<wire x1="324.976490625" y1="41.861740625" x2="325.090790625" y2="41.75886875" width="0.025" layer="94"/>
<wire x1="325.090790625" y1="41.75886875" x2="325.193659375" y2="41.678859375" width="0.025" layer="94"/>
<wire x1="325.193659375" y1="41.678859375" x2="325.27366875" y2="41.621709375" width="0.025" layer="94"/>
<wire x1="325.27366875" y1="41.621709375" x2="325.433690625" y2="41.53026875" width="0.025" layer="94"/>
<wire x1="325.433690625" y1="41.53026875" x2="325.58228125" y2="41.461690625" width="0.025" layer="94"/>
<wire x1="325.58228125" y1="41.461690625" x2="325.68515" y2="41.41596875" width="0.025" layer="94"/>
<wire x1="325.68515" y1="41.41596875" x2="325.84516875" y2="41.35881875" width="0.025" layer="94"/>
<wire x1="325.84516875" y1="41.35881875" x2="325.91718125" y2="41.33481875" width="0.025" layer="94"/>
<wire x1="325.91718125" y1="41.33481875" x2="325.969759375" y2="41.317290625" width="0.025" layer="94"/>
<wire x1="325.969759375" y1="41.317290625" x2="326.084059375" y2="41.279190625" width="0.025" layer="94"/>
<wire x1="326.084059375" y1="41.279190625" x2="326.11925" y2="41.437690625" width="0.025" layer="94"/>
<wire x1="326.11925" y1="41.437690625" x2="325.99261875" y2="41.49646875" width="0.025" layer="94"/>
<wire x1="325.99261875" y1="41.49646875" x2="325.91375" y2="41.53026875" width="0.025" layer="94"/>
<wire x1="325.91375" y1="41.53026875" x2="325.81088125" y2="41.575990625" width="0.025" layer="94"/>
<wire x1="325.81088125" y1="41.575990625" x2="325.652" y2="41.66678125" width="0.025" layer="94"/>
<wire x1="325.652" y1="41.66678125" x2="325.55941875" y2="41.72458125" width="0.025" layer="94"/>
<wire x1="325.55941875" y1="41.72458125" x2="325.422259375" y2="41.83888125" width="0.025" layer="94"/>
<wire x1="325.422259375" y1="41.83888125" x2="325.365109375" y2="41.89603125" width="0.025" layer="94"/>
<wire x1="325.365109375" y1="41.89603125" x2="325.307959375" y2="41.964609375" width="0.025" layer="94"/>
<wire x1="325.307959375" y1="41.964609375" x2="325.21651875" y2="42.10176875" width="0.025" layer="94"/>
<wire x1="325.21651875" y1="42.10176875" x2="325.18223125" y2="42.18178125" width="0.025" layer="94"/>
<wire x1="325.18223125" y1="42.18178125" x2="325.15936875" y2="42.250359375" width="0.025" layer="94"/>
<wire x1="325.15936875" y1="42.250359375" x2="325.147940625" y2="42.318940625" width="0.025" layer="94"/>
<wire x1="325.147940625" y1="42.318940625" x2="325.147940625" y2="42.478959375" width="0.025" layer="94"/>
<wire x1="325.147940625" y1="42.478959375" x2="325.15936875" y2="42.547540625" width="0.025" layer="94"/>
<wire x1="325.15936875" y1="42.547540625" x2="325.1708" y2="42.593259375" width="0.025" layer="94"/>
<wire x1="325.1708" y1="42.593259375" x2="325.193659375" y2="42.650409375" width="0.025" layer="94"/>
<wire x1="325.193659375" y1="42.650409375" x2="325.22795" y2="42.718990625" width="0.025" layer="94"/>
<wire x1="325.22795" y1="42.718990625" x2="325.250809375" y2="42.75328125" width="0.025" layer="94"/>
<wire x1="325.250809375" y1="42.75328125" x2="325.2851" y2="42.799" width="0.025" layer="94"/>
<wire x1="325.2851" y1="42.799" x2="325.44511875" y2="42.95901875" width="0.025" layer="94"/>
<wire x1="325.44511875" y1="42.95901875" x2="325.5377" y2="43.028359375" width="0.025" layer="94"/>
<wire x1="325.5377" y1="43.028359375" x2="325.605140625" y2="43.07331875" width="0.025" layer="94"/>
<wire x1="325.605140625" y1="43.07331875" x2="325.68858125" y2="43.123609375" width="0.025" layer="94"/>
<wire x1="325.68858125" y1="43.123609375" x2="325.776590625" y2="43.176190625" width="0.025" layer="94"/>
<wire x1="325.776590625" y1="43.176190625" x2="325.84516875" y2="43.21048125" width="0.025" layer="94"/>
<wire x1="325.84516875" y1="43.21048125" x2="326.005190625" y2="43.279059375" width="0.025" layer="94"/>
<wire x1="326.005190625" y1="43.279059375" x2="326.09663125" y2="43.31335" width="0.025" layer="94"/>
<wire x1="326.09663125" y1="43.31335" x2="326.37095" y2="43.404790625" width="0.025" layer="94"/>
<wire x1="326.37095" y1="43.404790625" x2="326.49668125" y2="43.43908125" width="0.025" layer="94"/>
<wire x1="326.49668125" y1="43.43908125" x2="326.56411875" y2="43.454640625" width="0.025" layer="94"/>
<wire x1="326.56411875" y1="43.454640625" x2="326.64526875" y2="43.47336875" width="0.025" layer="94"/>
<wire x1="326.64526875" y1="43.47336875" x2="326.75956875" y2="43.49623125" width="0.025" layer="94"/>
<wire x1="326.75956875" y1="43.49623125" x2="326.93101875" y2="43.519090625" width="0.025" layer="94"/>
<wire x1="326.93101875" y1="43.519090625" x2="327.06818125" y2="43.53051875" width="0.025" layer="94"/>
<wire x1="327.06818125" y1="43.53051875" x2="327.205340625" y2="43.53051875" width="0.025" layer="94"/>
<wire x1="327.205340625" y1="43.53051875" x2="327.205340625" y2="43.35906875" width="0.025" layer="94"/>
<wire x1="327.205340625" y1="43.35906875" x2="327.17105" y2="43.35906875" width="0.025" layer="94"/>
<wire x1="327.17105" y1="43.35906875" x2="327.04531875" y2="43.347640625" width="0.025" layer="94"/>
<wire x1="327.04531875" y1="43.347640625" x2="326.95388125" y2="43.336209375" width="0.025" layer="94"/>
<wire x1="326.95388125" y1="43.336209375" x2="326.81671875" y2="43.31335" width="0.025" layer="94"/>
<wire x1="326.81671875" y1="43.31335" x2="326.71385" y2="43.290490625" width="0.025" layer="94"/>
<wire x1="326.71385" y1="43.290490625" x2="326.565259375" y2="43.2562" width="0.025" layer="94"/>
<wire x1="326.565259375" y1="43.2562" x2="326.48525" y2="43.233340625" width="0.025" layer="94"/>
<wire x1="326.48525" y1="43.233340625" x2="326.41666875" y2="43.21048125" width="0.025" layer="94"/>
<wire x1="326.41666875" y1="43.21048125" x2="326.32523125" y2="43.176190625" width="0.025" layer="94"/>
<wire x1="326.32523125" y1="43.176190625" x2="326.21206875" y2="43.13093125" width="0.025" layer="94"/>
<wire x1="326.21206875" y1="43.13093125" x2="326.005190625" y2="43.0276" width="0.025" layer="94"/>
<wire x1="326.005190625" y1="43.0276" x2="325.948040625" y2="42.993309375" width="0.025" layer="94"/>
<wire x1="325.948040625" y1="42.993309375" x2="325.84516875" y2="42.92473125" width="0.025" layer="94"/>
<wire x1="325.84516875" y1="42.92473125" x2="325.79945" y2="42.890440625" width="0.025" layer="94"/>
<wire x1="325.79945" y1="42.890440625" x2="325.7423" y2="42.84471875" width="0.025" layer="94"/>
<wire x1="325.7423" y1="42.84471875" x2="325.708009375" y2="42.81043125" width="0.025" layer="94"/>
<wire x1="325.708009375" y1="42.81043125" x2="325.63943125" y2="42.73041875" width="0.025" layer="94"/>
<wire x1="325.63943125" y1="42.73041875" x2="325.593709375" y2="42.661840625" width="0.025" layer="94"/>
<wire x1="325.593709375" y1="42.661840625" x2="325.55941875" y2="42.593259375" width="0.025" layer="94"/>
<wire x1="325.55941875" y1="42.593259375" x2="325.536559375" y2="42.536109375" width="0.025" layer="94"/>
<wire x1="325.536559375" y1="42.536109375" x2="325.5137" y2="42.4561" width="0.025" layer="94"/>
<wire x1="325.5137" y1="42.4561" x2="325.50226875" y2="42.39895" width="0.025" layer="94"/>
<wire x1="325.50226875" y1="42.39895" x2="325.50226875" y2="42.261790625" width="0.025" layer="94"/>
<wire x1="325.50226875" y1="42.261790625" x2="325.5137" y2="42.204640625" width="0.025" layer="94"/>
<wire x1="325.5137" y1="42.204640625" x2="325.52513125" y2="42.15891875" width="0.025" layer="94"/>
<wire x1="325.52513125" y1="42.15891875" x2="325.547609375" y2="42.09148125" width="0.025" layer="94"/>
<wire x1="325.547609375" y1="42.09148125" x2="325.57085" y2="42.04461875" width="0.025" layer="94"/>
<wire x1="325.57085" y1="42.04461875" x2="325.605140625" y2="41.98746875" width="0.025" layer="94"/>
<wire x1="325.605140625" y1="41.98746875" x2="325.67371875" y2="41.89603125" width="0.025" layer="94"/>
<wire x1="325.67371875" y1="41.89603125" x2="325.75373125" y2="41.81601875" width="0.025" layer="94"/>
<wire x1="325.75373125" y1="41.81601875" x2="325.84516875" y2="41.747440625" width="0.025" layer="94"/>
<wire x1="325.84516875" y1="41.747440625" x2="325.95833125" y2="41.67955" width="0.025" layer="94"/>
<wire x1="325.95833125" y1="41.67955" x2="326.04061875" y2="41.63256875" width="0.025" layer="94"/>
<wire x1="326.04061875" y1="41.63256875" x2="326.108059375" y2="41.59885" width="0.025" layer="94"/>
<wire x1="326.108059375" y1="41.59885" x2="326.165509375" y2="41.57713125" width="0.025" layer="94"/>
<wire x1="326.165509375" y1="41.57713125" x2="326.35951875" y2="42.307509375" width="0.025" layer="94"/>
<wire x1="326.35951875" y1="42.307509375" x2="328.8284" y2="42.307509375" width="0.025" layer="94"/>
<wire x1="328.8284" y1="42.307509375" x2="329.022709375" y2="41.575990625" width="0.025" layer="94"/>
<wire x1="329.022709375" y1="41.575990625" x2="329.087859375" y2="41.597140625" width="0.025" layer="94"/>
<wire x1="329.087859375" y1="41.597140625" x2="329.14158125" y2="41.624" width="0.025" layer="94"/>
<wire x1="329.14158125" y1="41.624" x2="329.25245" y2="41.67961875" width="0.025" layer="94"/>
<wire x1="329.25245" y1="41.67961875" x2="329.42161875" y2="41.7924" width="0.025" layer="94"/>
<wire x1="329.42161875" y1="41.7924" x2="329.548490625" y2="41.918890625" width="0.025" layer="94"/>
<wire x1="329.548490625" y1="41.918890625" x2="329.6285" y2="42.033190625" width="0.025" layer="94"/>
<wire x1="329.6285" y1="42.033190625" x2="329.662790625" y2="42.1132" width="0.025" layer="94"/>
<wire x1="329.662790625" y1="42.1132" x2="329.68593125" y2="42.18291875" width="0.025" layer="94"/>
<wire x1="329.68593125" y1="42.18291875" x2="329.69708125" y2="42.2275" width="0.025" layer="94"/>
<wire x1="329.69708125" y1="42.2275" x2="329.708509375" y2="42.307509375" width="0.025" layer="94"/>
<wire x1="329.708509375" y1="42.307509375" x2="329.708509375" y2="42.35323125" width="0.025" layer="94"/>
<wire x1="329.708509375" y1="42.35323125" x2="329.69708125" y2="42.433240625" width="0.025" layer="94"/>
<wire x1="329.69708125" y1="42.433240625" x2="329.67421875" y2="42.52468125" width="0.025" layer="94"/>
<wire x1="329.67421875" y1="42.52468125" x2="329.651359375" y2="42.58183125" width="0.025" layer="94"/>
<wire x1="329.651359375" y1="42.58183125" x2="329.61706875" y2="42.650409375" width="0.025" layer="94"/>
<wire x1="329.61706875" y1="42.650409375" x2="329.570490625" y2="42.72013125" width="0.025" layer="94"/>
<wire x1="329.570490625" y1="42.72013125" x2="329.537059375" y2="42.764709375" width="0.025" layer="94"/>
<wire x1="329.537059375" y1="42.764709375" x2="329.43533125" y2="42.866440625" width="0.025" layer="94"/>
<wire x1="329.43533125" y1="42.866440625" x2="329.377040625" y2="42.9133" width="0.025" layer="94"/>
<wire x1="329.377040625" y1="42.9133" x2="329.33131875" y2="42.947590625" width="0.025" layer="94"/>
<wire x1="329.33131875" y1="42.947590625" x2="329.21701875" y2="43.01616875" width="0.025" layer="94"/>
<wire x1="329.21701875" y1="43.01616875" x2="329.137009375" y2="43.061890625" width="0.025" layer="94"/>
<wire x1="329.137009375" y1="43.061890625" x2="329.06843125" y2="43.09618125" width="0.025" layer="94"/>
<wire x1="329.06843125" y1="43.09618125" x2="328.965559375" y2="43.1419" width="0.025" layer="94"/>
<wire x1="328.965559375" y1="43.1419" x2="328.908409375" y2="43.164759375" width="0.025" layer="94"/>
<wire x1="328.908409375" y1="43.164759375" x2="328.81696875" y2="43.19905" width="0.025" layer="94"/>
<wire x1="328.81696875" y1="43.19905" x2="328.7141" y2="43.233340625" width="0.025" layer="94"/>
<wire x1="328.7141" y1="43.233340625" x2="328.634090625" y2="43.2562" width="0.025" layer="94"/>
<wire x1="328.634090625" y1="43.2562" x2="328.54265" y2="43.279059375" width="0.025" layer="94"/>
<wire x1="328.54265" y1="43.279059375" x2="328.43978125" y2="43.30191875" width="0.025" layer="94"/>
<wire x1="328.43978125" y1="43.30191875" x2="328.32661875" y2="43.32478125" width="0.025" layer="94"/>
<wire x1="328.32661875" y1="43.32478125" x2="328.255759375" y2="43.336209375" width="0.025" layer="94"/>
<wire x1="328.255759375" y1="43.336209375" x2="328.15516875" y2="43.347640625" width="0.025" layer="94"/>
<wire x1="328.15516875" y1="43.347640625" x2="328.075159375" y2="43.353709375" width="0.025" layer="94"/>
<wire x1="328.075159375" y1="43.353709375" x2="328.00658125" y2="43.35906875" width="0.025" layer="94"/>
<wire x1="328.00658125" y1="43.35906875" x2="328.005440625" y2="43.53051875" width="0.025" layer="94"/>
<wire x1="328.005440625" y1="43.53051875" x2="328.176890625" y2="43.53051875" width="0.025" layer="94"/>
<wire x1="328.176890625" y1="43.53051875" x2="328.291190625" y2="43.519090625" width="0.025" layer="94"/>
<wire x1="328.291190625" y1="43.519090625" x2="328.3712" y2="43.507659375" width="0.025" layer="94"/>
<wire x1="328.3712" y1="43.507659375" x2="328.43978125" y2="43.49623125" width="0.025" layer="94"/>
<wire x1="328.43978125" y1="43.49623125" x2="328.61123125" y2="43.461940625" width="0.025" layer="94"/>
<wire x1="328.61123125" y1="43.461940625" x2="328.70266875" y2="43.43908125" width="0.025" layer="94"/>
<wire x1="328.70266875" y1="43.43908125" x2="328.78268125" y2="43.41621875" width="0.025" layer="94"/>
<wire x1="328.78268125" y1="43.41621875" x2="328.892409375" y2="43.38306875" width="0.025" layer="94"/>
<wire x1="328.892409375" y1="43.38306875" x2="329.010140625" y2="43.34798125" width="0.025" layer="94"/>
<wire x1="329.010140625" y1="43.34798125" x2="329.079859375" y2="43.32191875" width="0.025" layer="94"/>
<wire x1="329.079859375" y1="43.32191875" x2="329.194159375" y2="43.279059375" width="0.025" layer="94"/>
<wire x1="329.194159375" y1="43.279059375" x2="329.3096" y2="43.23276875" width="0.025" layer="94"/>
<wire x1="329.3096" y1="43.23276875" x2="329.44561875" y2="43.164759375" width="0.025" layer="94"/>
<wire x1="329.44561875" y1="43.164759375" x2="329.50276875" y2="43.13046875" width="0.025" layer="94"/>
<wire x1="329.50276875" y1="43.13046875" x2="329.548490625" y2="43.107609375" width="0.025" layer="94"/>
<wire x1="329.548490625" y1="43.107609375" x2="329.61706875" y2="43.061890625" width="0.025" layer="94"/>
<wire x1="329.61706875" y1="43.061890625" x2="329.75423125" y2="42.95901875" width="0.025" layer="94"/>
<wire x1="329.75423125" y1="42.95901875" x2="329.81138125" y2="42.9133" width="0.025" layer="94"/>
<wire x1="329.891390625" y1="42.833290625" x2="329.89253125" y2="42.83215" width="0.025" layer="94"/>
<wire x1="329.89253125" y1="42.83215" x2="329.937109375" y2="42.776140625" width="0.025" layer="94"/>
<wire x1="329.937109375" y1="42.776140625" x2="329.95996875" y2="42.74185" width="0.025" layer="94"/>
<wire x1="329.95996875" y1="42.74185" x2="329.9954" y2="42.682409375" width="0.025" layer="94"/>
<wire x1="329.9954" y1="42.682409375" x2="330.03998125" y2="42.5704" width="0.025" layer="94"/>
<wire x1="330.03998125" y1="42.5704" x2="330.051409375" y2="42.512109375" width="0.025" layer="94"/>
<wire x1="330.051409375" y1="42.512109375" x2="330.062840625" y2="42.421809375" width="0.025" layer="94"/>
<wire x1="330.062840625" y1="42.421809375" x2="330.062840625" y2="42.35323125" width="0.025" layer="94"/>
<wire x1="330.062840625" y1="42.35323125" x2="330.051409375" y2="42.261790625" width="0.025" layer="94"/>
<wire x1="330.051409375" y1="42.261790625" x2="330.03998125" y2="42.21606875" width="0.025" layer="94"/>
<wire x1="330.03998125" y1="42.21606875" x2="330.01711875" y2="42.15891875" width="0.025" layer="94"/>
<wire x1="330.01711875" y1="42.15891875" x2="329.98283125" y2="42.090340625" width="0.025" layer="94"/>
<wire x1="329.98283125" y1="42.090340625" x2="329.95996875" y2="42.05605" width="0.025" layer="94"/>
<wire x1="329.95996875" y1="42.05605" x2="329.90281875" y2="41.976040625" width="0.025" layer="94"/>
<wire x1="329.90281875" y1="41.976040625" x2="329.834240625" y2="41.89603125" width="0.025" layer="94"/>
<wire x1="329.834240625" y1="41.89603125" x2="329.7428" y2="41.804590625" width="0.025" layer="94"/>
<wire x1="329.7428" y1="41.804590625" x2="329.6285" y2="41.71315" width="0.025" layer="94"/>
<wire x1="329.6285" y1="41.71315" x2="329.55878125" y2="41.666790625" width="0.025" layer="94"/>
<wire x1="329.55878125" y1="41.666790625" x2="329.45705" y2="41.61028125" width="0.025" layer="94"/>
<wire x1="329.45705" y1="41.61028125" x2="329.319890625" y2="41.5417" width="0.025" layer="94"/>
<wire x1="329.319890625" y1="41.5417" x2="329.1633" y2="41.474590625" width="0.025" layer="94"/>
<wire x1="329.1633" y1="41.474590625" x2="329.06843125" y2="41.43883125" width="0.025" layer="94"/>
<wire x1="329.06843125" y1="41.43883125" x2="329.10271875" y2="41.278809375" width="0.025" layer="94"/>
<wire x1="329.10271875" y1="41.278809375" x2="329.137009375" y2="41.290240625" width="0.025" layer="94"/>
<wire x1="329.137009375" y1="41.290240625" x2="329.21701875" y2="41.3131" width="0.025" layer="94"/>
<wire x1="329.21701875" y1="41.3131" x2="329.3999" y2="41.37025" width="0.025" layer="94"/>
<wire x1="329.3999" y1="41.37025" x2="329.57135" y2="41.43883125" width="0.025" layer="94"/>
<wire x1="329.57135" y1="41.43883125" x2="329.834240625" y2="41.564559375" width="0.025" layer="94"/>
<wire x1="329.834240625" y1="41.564559375" x2="329.891390625" y2="41.59885" width="0.025" layer="94"/>
<wire x1="329.891390625" y1="41.59885" x2="329.994259375" y2="41.66743125" width="0.025" layer="94"/>
<wire x1="329.994259375" y1="41.66743125" x2="330.051409375" y2="41.71315" width="0.025" layer="94"/>
<wire x1="330.051409375" y1="41.71315" x2="330.14285" y2="41.78173125" width="0.025" layer="94"/>
<wire x1="330.14285" y1="41.78173125" x2="330.177140625" y2="41.81601875" width="0.025" layer="94"/>
<wire x1="330.177140625" y1="41.81601875" x2="330.25715" y2="41.8846" width="0.025" layer="94"/>
<wire x1="330.25715" y1="41.8846" x2="330.30286875" y2="41.93031875" width="0.025" layer="94"/>
<wire x1="330.30286875" y1="41.93031875" x2="330.37145" y2="42.01033125" width="0.025" layer="94"/>
<wire x1="330.37145" y1="42.01033125" x2="330.405740625" y2="42.05605" width="0.025" layer="94"/>
<wire x1="330.405740625" y1="42.05605" x2="330.47478125" y2="42.171490625" width="0.025" layer="94"/>
<wire x1="330.47478125" y1="42.171490625" x2="330.520040625" y2="42.28465" width="0.025" layer="94"/>
<wire x1="330.520040625" y1="42.28465" x2="330.54251875" y2="42.352090625" width="0.025" layer="94"/>
<wire x1="330.54251875" y1="42.352090625" x2="330.565759375" y2="42.478959375" width="0.025" layer="94"/>
<wire x1="330.565759375" y1="42.478959375" x2="330.565759375" y2="42.650409375" width="0.025" layer="94"/>
<wire x1="330.565759375" y1="42.650409375" x2="330.55433125" y2="42.718990625" width="0.025" layer="94"/>
<wire x1="330.55433125" y1="42.718990625" x2="330.5429" y2="42.776140625" width="0.025" layer="94"/>
<wire x1="330.5429" y1="42.776140625" x2="330.520040625" y2="42.84471875" width="0.025" layer="94"/>
<wire x1="330.520040625" y1="42.84471875" x2="330.49718125" y2="42.90186875" width="0.025" layer="94"/>
<wire x1="330.49718125" y1="42.90186875" x2="330.451459375" y2="42.993309375" width="0.025" layer="94"/>
<wire x1="330.451459375" y1="42.993309375" x2="330.41716875" y2="43.050459375" width="0.025" layer="94"/>
<wire x1="330.41716875" y1="43.050459375" x2="330.394309375" y2="43.08475" width="0.025" layer="94"/>
<wire x1="330.394309375" y1="43.08475" x2="330.36001875" y2="43.13046875" width="0.025" layer="94"/>
<wire x1="330.36001875" y1="43.13046875" x2="330.291440625" y2="43.21048125" width="0.025" layer="94"/>
<wire x1="330.291440625" y1="43.21048125" x2="330.234290625" y2="43.26763125" width="0.025" layer="94"/>
<wire x1="330.234290625" y1="43.26763125" x2="330.15428125" y2="43.336209375" width="0.025" layer="94"/>
<wire x1="330.15428125" y1="43.336209375" x2="330.01711875" y2="43.43908125" width="0.025" layer="94"/>
<wire x1="330.01711875" y1="43.43908125" x2="329.879959375" y2="43.53051875" width="0.025" layer="94"/>
<wire x1="329.879959375" y1="43.53051875" x2="329.765659375" y2="43.5991" width="0.025" layer="94"/>
<wire x1="329.765659375" y1="43.5991" x2="329.68565" y2="43.64481875" width="0.025" layer="94"/>
<wire x1="329.68565" y1="43.64481875" x2="329.52563125" y2="43.72483125" width="0.025" layer="94"/>
<wire x1="329.52563125" y1="43.72483125" x2="329.44561875" y2="43.75911875" width="0.025" layer="94"/>
<wire x1="329.44561875" y1="43.75911875" x2="329.1713" y2="43.850559375" width="0.025" layer="94"/>
<wire x1="329.1713" y1="43.850559375" x2="329.04556875" y2="43.88485" width="0.025" layer="94"/>
<wire x1="329.04556875" y1="43.88485" x2="328.965559375" y2="43.907709375" width="0.025" layer="94"/>
<wire x1="328.965559375" y1="43.907709375" x2="328.691240625" y2="43.976290625" width="0.025" layer="94"/>
<wire x1="328.691240625" y1="43.976290625" x2="328.634090625" y2="43.98771875" width="0.025" layer="94"/>
<wire x1="328.634090625" y1="43.98771875" x2="328.565509375" y2="43.99915" width="0.025" layer="94"/>
<wire x1="328.565509375" y1="43.99915" x2="328.394059375" y2="44.022009375" width="0.025" layer="94"/>
<wire x1="328.394059375" y1="44.022009375" x2="328.2569" y2="44.033440625" width="0.025" layer="94"/>
<wire x1="328.2569" y1="44.033440625" x2="328.119740625" y2="44.033440625" width="0.025" layer="94"/>
<wire x1="328.119740625" y1="44.033440625" x2="328.119740625" y2="44.29633125" width="0.025" layer="94"/>
<wire x1="328.119740625" y1="44.29633125" x2="328.165459375" y2="44.29633125" width="0.025" layer="94"/>
<wire x1="328.165459375" y1="44.29633125" x2="328.32548125" y2="44.2849" width="0.025" layer="94"/>
<wire x1="328.32548125" y1="44.2849" x2="328.42835" y2="44.27346875" width="0.025" layer="94"/>
<wire x1="328.42835" y1="44.27346875" x2="328.519790625" y2="44.262040625" width="0.025" layer="94"/>
<wire x1="328.519790625" y1="44.262040625" x2="328.5998" y2="44.250609375" width="0.025" layer="94"/>
<wire x1="328.5998" y1="44.250609375" x2="328.72553125" y2="44.22775" width="0.025" layer="94"/>
<wire x1="328.72553125" y1="44.22775" x2="328.952990625" y2="44.18203125" width="0.025" layer="94"/>
<wire x1="328.952990625" y1="44.18203125" x2="329.22845" y2="44.11345" width="0.025" layer="94"/>
<wire x1="329.22845" y1="44.11345" x2="329.38846875" y2="44.06773125" width="0.025" layer="94"/>
<wire x1="329.38846875" y1="44.06773125" x2="329.52563125" y2="44.022009375" width="0.025" layer="94"/>
<wire x1="329.52563125" y1="44.022009375" x2="329.69708125" y2="43.95343125" width="0.025" layer="94"/>
<wire x1="329.69708125" y1="43.95343125" x2="329.90281875" y2="43.861990625" width="0.025" layer="94"/>
<wire x1="329.90281875" y1="43.861990625" x2="330.04111875" y2="43.79271875" width="0.025" layer="94"/>
<wire x1="330.04111875" y1="43.79271875" x2="330.14285" y2="43.736259375" width="0.025" layer="94"/>
<wire x1="330.14285" y1="43.736259375" x2="330.2" y2="43.70196875" width="0.025" layer="94"/>
<wire x1="330.2" y1="43.70196875" x2="330.337159375" y2="43.61053125" width="0.025" layer="94"/>
<wire x1="330.337159375" y1="43.61053125" x2="330.38288125" y2="43.576240625" width="0.025" layer="94"/>
<wire x1="330.38288125" y1="43.576240625" x2="330.55433125" y2="43.43908125" width="0.025" layer="94"/>
<wire x1="330.55433125" y1="43.43908125" x2="330.601190625" y2="43.39221875" width="0.025" layer="94"/>
<wire x1="330.601190625" y1="43.39221875" x2="330.644140625" y2="43.34193125" width="0.025" layer="94"/>
<wire x1="330.644140625" y1="43.34193125" x2="330.70291875" y2="43.279059375" width="0.025" layer="94"/>
<wire x1="330.70291875" y1="43.279059375" x2="330.7715" y2="43.18761875" width="0.025" layer="94"/>
<wire x1="330.7715" y1="43.18761875" x2="330.81721875" y2="43.119040625" width="0.025" layer="94"/>
<wire x1="330.81721875" y1="43.119040625" x2="330.862940625" y2="43.0276" width="0.025" layer="94"/>
<wire x1="330.862940625" y1="43.0276" x2="330.8858" y2="42.95901875" width="0.025" layer="94"/>
<wire x1="330.8858" y1="42.95901875" x2="330.908659375" y2="42.879009375" width="0.025" layer="94"/>
<wire x1="330.908659375" y1="42.879009375" x2="330.920090625" y2="42.81043125" width="0.025" layer="94"/>
<wire x1="330.920090625" y1="42.81043125" x2="330.93151875" y2="42.69613125" width="0.025" layer="94"/>
<wire x1="330.93151875" y1="42.69613125" x2="330.93151875" y2="42.63898125" width="0.025" layer="94"/>
<wire x1="330.93151875" y1="42.63898125" x2="330.920090625" y2="42.536109375" width="0.025" layer="94"/>
<wire x1="330.920090625" y1="42.536109375" x2="330.908659375" y2="42.478959375" width="0.025" layer="94"/>
<wire x1="330.908659375" y1="42.478959375" x2="330.8858" y2="42.38751875" width="0.025" layer="94"/>
<wire x1="330.8858" y1="42.38751875" x2="330.87436875" y2="42.35323125" width="0.025" layer="94"/>
<wire x1="330.87436875" y1="42.35323125" x2="330.82865" y2="42.23893125" width="0.025" layer="94"/>
<wire x1="330.82865" y1="42.23893125" x2="330.8051" y2="42.19206875" width="0.025" layer="94"/>
<wire x1="330.8051" y1="42.19206875" x2="330.770640625" y2="42.13491875" width="0.025" layer="94"/>
<wire x1="330.770640625" y1="42.13491875" x2="330.70291875" y2="42.04461875" width="0.025" layer="94"/>
<wire x1="330.70291875" y1="42.04461875" x2="330.6572" y2="41.98746875" width="0.025" layer="94"/>
<wire x1="330.6572" y1="41.98746875" x2="330.577190625" y2="41.907459375" width="0.025" layer="94"/>
<wire x1="330.577190625" y1="41.907459375" x2="330.49718125" y2="41.83888125" width="0.025" layer="94"/>
<wire x1="330.49718125" y1="41.83888125" x2="330.44003125" y2="41.793159375" width="0.025" layer="94"/>
<wire x1="330.44003125" y1="41.793159375" x2="330.394309375" y2="41.75886875" width="0.025" layer="94"/>
<wire x1="330.394309375" y1="41.75886875" x2="330.337159375" y2="41.71315" width="0.025" layer="94"/>
<wire x1="330.337159375" y1="41.71315" x2="330.291440625" y2="41.678859375" width="0.025" layer="94"/>
<wire x1="330.291440625" y1="41.678859375" x2="330.18856875" y2="41.61028125" width="0.025" layer="94"/>
<wire x1="330.18856875" y1="41.61028125" x2="330.14285" y2="41.575990625" width="0.025" layer="94"/>
<wire x1="330.14285" y1="41.575990625" x2="330.0857" y2="41.5417" width="0.025" layer="94"/>
<wire x1="330.0857" y1="41.5417" x2="329.834240625" y2="41.41596875" width="0.025" layer="94"/>
<wire x1="329.834240625" y1="41.41596875" x2="329.719940625" y2="41.37025" width="0.025" layer="94"/>
<wire x1="329.719940625" y1="41.37025" x2="329.594209375" y2="41.32453125" width="0.025" layer="94"/>
<wire x1="329.594209375" y1="41.32453125" x2="329.44333125" y2="41.278109375" width="0.025" layer="94"/>
<wire x1="329.44333125" y1="41.278109375" x2="329.29703125" y2="41.233090625" width="0.025" layer="94"/>
<wire x1="329.29703125" y1="41.233090625" x2="329.137009375" y2="41.18736875" width="0.025" layer="94"/>
<wire x1="329.137009375" y1="41.18736875" x2="329.1713" y2="41.03878125" width="0.025" layer="94"/>
<wire x1="329.1713" y1="41.03878125" x2="329.26616875" y2="41.059640625" width="0.025" layer="94"/>
<wire x1="329.26616875" y1="41.059640625" x2="329.50163125" y2="41.1185" width="0.025" layer="94"/>
<wire x1="329.50163125" y1="41.1185" x2="329.6045" y2="41.147859375" width="0.025" layer="94"/>
<wire x1="329.6045" y1="41.147859375" x2="329.743940625" y2="41.18775" width="0.025" layer="94"/>
<wire x1="329.743940625" y1="41.18775" x2="329.948540625" y2="41.25595" width="0.025" layer="94"/>
<wire x1="329.948540625" y1="41.25595" x2="330.03998125" y2="41.290240625" width="0.025" layer="94"/>
<wire x1="330.03998125" y1="41.290240625" x2="330.21143125" y2="41.35881875" width="0.025" layer="94"/>
<wire x1="330.21143125" y1="41.35881875" x2="330.48575" y2="41.49598125" width="0.025" layer="94"/>
<wire x1="330.48575" y1="41.49598125" x2="330.60005" y2="41.564559375" width="0.025" layer="94"/>
<wire x1="330.60005" y1="41.564559375" x2="330.69035" y2="41.62101875" width="0.025" layer="94"/>
<wire x1="330.69035" y1="41.62101875" x2="330.737209375" y2="41.656" width="0.025" layer="94"/>
<wire x1="330.737209375" y1="41.656" x2="330.805790625" y2="41.70171875" width="0.025" layer="94"/>
<wire x1="330.805790625" y1="41.70171875" x2="330.851509375" y2="41.736009375" width="0.025" layer="94"/>
<wire x1="330.851509375" y1="41.736009375" x2="331.080109375" y2="41.918890625" width="0.025" layer="94"/>
<wire x1="331.080109375" y1="41.918890625" x2="331.2287" y2="42.06748125" width="0.025" layer="94"/>
<wire x1="331.2287" y1="42.06748125" x2="331.308709375" y2="42.17035" width="0.025" layer="94"/>
<wire x1="331.308709375" y1="42.17035" x2="331.33156875" y2="42.204640625" width="0.025" layer="94"/>
<wire x1="331.33156875" y1="42.204640625" x2="331.365859375" y2="42.261790625" width="0.025" layer="94"/>
<wire x1="331.365859375" y1="42.261790625" x2="331.434440625" y2="42.39895" width="0.025" layer="94"/>
<wire x1="331.434440625" y1="42.39895" x2="331.4573" y2="42.4561" width="0.025" layer="94"/>
<wire x1="331.4573" y1="42.4561" x2="331.46873125" y2="42.490390625" width="0.025" layer="94"/>
<wire x1="331.46873125" y1="42.490390625" x2="331.491590625" y2="42.58183125" width="0.025" layer="94"/>
<wire x1="331.491590625" y1="42.58183125" x2="331.50301875" y2="42.650409375" width="0.025" layer="94"/>
<wire x1="331.50301875" y1="42.650409375" x2="331.51445" y2="42.764709375" width="0.025" layer="94"/>
<wire x1="331.51445" y1="42.764709375" x2="331.51445" y2="42.84471875" width="0.025" layer="94"/>
<wire x1="331.51445" y1="42.84471875" x2="331.50988125" y2="42.89615" width="0.025" layer="94"/>
<wire x1="331.50988125" y1="42.89615" x2="331.50301875" y2="42.97045" width="0.025" layer="94"/>
<wire x1="331.50301875" y1="42.97045" x2="331.480159375" y2="43.08475" width="0.025" layer="94"/>
<wire x1="331.480159375" y1="43.08475" x2="331.434440625" y2="43.221909375" width="0.025" layer="94"/>
<wire x1="331.434440625" y1="43.221909375" x2="331.41158125" y2="43.279059375" width="0.025" layer="94"/>
<wire x1="331.41158125" y1="43.279059375" x2="331.36643125" y2="43.369359375" width="0.025" layer="94"/>
<wire x1="331.36643125" y1="43.369359375" x2="331.27518125" y2="43.50651875" width="0.025" layer="94"/>
<wire x1="331.27518125" y1="43.50651875" x2="331.24013125" y2="43.55338125" width="0.025" layer="94"/>
<wire x1="331.24013125" y1="43.55338125" x2="331.194409375" y2="43.61053125" width="0.025" layer="94"/>
<wire x1="331.194409375" y1="43.61053125" x2="330.98866875" y2="43.81626875" width="0.025" layer="94"/>
<wire x1="330.98866875" y1="43.81626875" x2="330.920090625" y2="43.87341875" width="0.025" layer="94"/>
<wire x1="330.920090625" y1="43.87341875" x2="330.862940625" y2="43.919140625" width="0.025" layer="94"/>
<wire x1="330.862940625" y1="43.919140625" x2="330.7715" y2="43.98771875" width="0.025" layer="94"/>
<wire x1="330.7715" y1="43.98771875" x2="330.66863125" y2="44.0563" width="0.025" layer="94"/>
<wire x1="330.66863125" y1="44.0563" x2="330.610340625" y2="44.09135" width="0.025" layer="94"/>
<wire x1="330.610340625" y1="44.09135" x2="330.577190625" y2="44.11345" width="0.025" layer="94"/>
<wire x1="330.577190625" y1="44.11345" x2="330.520040625" y2="44.147740625" width="0.025" layer="94"/>
<wire x1="330.520040625" y1="44.147740625" x2="330.24571875" y2="44.2849" width="0.025" layer="94"/>
<wire x1="330.24571875" y1="44.2849" x2="329.95768125" y2="44.399959375" width="0.025" layer="94"/>
<wire x1="329.95768125" y1="44.399959375" x2="329.651359375" y2="44.50206875" width="0.025" layer="94"/>
<wire x1="329.651359375" y1="44.50206875" x2="329.4902" y2="44.54806875" width="0.025" layer="94"/>
<wire x1="329.4902" y1="44.54806875" x2="329.21701875" y2="44.61636875" width="0.025" layer="94"/>
<wire x1="329.21701875" y1="44.61636875" x2="328.87411875" y2="44.68495" width="0.025" layer="94"/>
<wire x1="328.87411875" y1="44.68495" x2="328.736959375" y2="44.707809375" width="0.025" layer="94"/>
<wire x1="328.736959375" y1="44.707809375" x2="328.65695" y2="44.719240625" width="0.025" layer="94"/>
<wire x1="328.65695" y1="44.719240625" x2="328.47406875" y2="44.7421" width="0.025" layer="94"/>
<wire x1="328.47406875" y1="44.7421" x2="328.370059375" y2="44.75353125" width="0.025" layer="94"/>
<wire x1="328.370059375" y1="44.75353125" x2="328.222609375" y2="44.764959375" width="0.025" layer="94"/>
<wire x1="328.222609375" y1="44.764959375" x2="328.222609375" y2="45.03928125" width="0.025" layer="94"/>
<wire x1="328.222609375" y1="45.03928125" x2="328.279759375" y2="45.03928125" width="0.025" layer="94"/>
<wire x1="328.279759375" y1="45.03928125" x2="328.42835" y2="45.02785" width="0.025" layer="94"/>
<wire x1="328.42835" y1="45.02785" x2="328.55408125" y2="45.01641875" width="0.025" layer="94"/>
<wire x1="328.55408125" y1="45.01641875" x2="328.65695" y2="45.004990625" width="0.025" layer="94"/>
<wire x1="328.65695" y1="45.004990625" x2="328.736959375" y2="44.994990625" width="0.025" layer="94"/>
<wire x1="328.736959375" y1="44.994990625" x2="328.79983125" y2="44.98713125" width="0.025" layer="94"/>
<wire x1="328.79983125" y1="44.98713125" x2="328.93126875" y2="44.9707" width="0.025" layer="94"/>
<wire x1="328.93126875" y1="44.9707" x2="329.137009375" y2="44.936409375" width="0.025" layer="94"/>
<wire x1="329.137009375" y1="44.936409375" x2="329.251309375" y2="44.91355" width="0.025" layer="94"/>
<wire x1="329.251309375" y1="44.91355" x2="329.29703125" y2="44.90211875" width="0.025" layer="94"/>
<wire x1="329.29703125" y1="44.90211875" x2="329.3999" y2="44.879259375" width="0.025" layer="94"/>
<wire x1="329.3999" y1="44.879259375" x2="329.45705" y2="44.86783125" width="0.025" layer="94"/>
<wire x1="329.45705" y1="44.86783125" x2="329.63993125" y2="44.822109375" width="0.025" layer="94"/>
<wire x1="329.63993125" y1="44.822109375" x2="329.719940625" y2="44.79925" width="0.025" layer="94"/>
<wire x1="329.719940625" y1="44.79925" x2="329.81138125" y2="44.776390625" width="0.025" layer="94"/>
<wire x1="329.81138125" y1="44.776390625" x2="329.891390625" y2="44.75353125" width="0.025" layer="94"/>
<wire x1="329.891390625" y1="44.75353125" x2="330.165709375" y2="44.662090625" width="0.025" layer="94"/>
<wire x1="330.165709375" y1="44.662090625" x2="330.25715" y2="44.6278" width="0.025" layer="94"/>
<wire x1="330.25715" y1="44.6278" x2="330.3143" y2="44.604940625" width="0.025" layer="94"/>
<wire x1="330.3143" y1="44.604940625" x2="330.394309375" y2="44.57065" width="0.025" layer="94"/>
<wire x1="330.394309375" y1="44.57065" x2="330.508609375" y2="44.52493125" width="0.025" layer="94"/>
<wire x1="330.508609375" y1="44.52493125" x2="330.87436875" y2="44.34205" width="0.025" layer="94"/>
<wire x1="330.87436875" y1="44.34205" x2="331.04581875" y2="44.23918125" width="0.025" layer="94"/>
<wire x1="331.04581875" y1="44.23918125" x2="331.080109375" y2="44.21631875" width="0.025" layer="94"/>
<wire x1="331.080109375" y1="44.21631875" x2="331.137259375" y2="44.1706" width="0.025" layer="94"/>
<wire x1="331.137259375" y1="44.1706" x2="331.36471875" y2="44.000009375" width="0.025" layer="94"/>
<wire x1="331.36471875" y1="44.000009375" x2="331.451590625" y2="43.91341875" width="0.025" layer="94"/>
<wire x1="331.451590625" y1="43.91341875" x2="331.62875" y2="43.736259375" width="0.025" layer="94"/>
<wire x1="331.62875" y1="43.736259375" x2="331.67446875" y2="43.679109375" width="0.025" layer="94"/>
<wire x1="331.67446875" y1="43.679109375" x2="331.74305" y2="43.58766875" width="0.025" layer="94"/>
<wire x1="331.74305" y1="43.58766875" x2="331.777340625" y2="43.53051875" width="0.025" layer="94"/>
<wire x1="331.777340625" y1="43.53051875" x2="331.823059375" y2="43.450509375" width="0.025" layer="94"/>
<wire x1="331.823059375" y1="43.450509375" x2="331.85735" y2="43.38193125" width="0.025" layer="94"/>
<wire x1="331.85735" y1="43.38193125" x2="331.880209375" y2="43.32478125" width="0.025" layer="94"/>
<wire x1="331.880209375" y1="43.32478125" x2="331.90306875" y2="43.2562" width="0.025" layer="94"/>
<wire x1="331.90306875" y1="43.2562" x2="331.92593125" y2="43.164759375" width="0.025" layer="94"/>
<wire x1="331.92593125" y1="43.164759375" x2="331.937359375" y2="43.095040625" width="0.025" layer="94"/>
<wire x1="331.937359375" y1="43.095040625" x2="331.948790625" y2="42.993309375" width="0.025" layer="94"/>
<wire x1="331.948790625" y1="42.993309375" x2="331.948790625" y2="42.84471875" width="0.025" layer="94"/>
<wire x1="331.948790625" y1="42.84471875" x2="331.937359375" y2="42.75328125" width="0.025" layer="94"/>
<wire x1="331.937359375" y1="42.75328125" x2="331.92593125" y2="42.69613125" width="0.025" layer="94"/>
<wire x1="331.92593125" y1="42.69613125" x2="331.90306875" y2="42.604690625" width="0.025" layer="94"/>
<wire x1="331.90306875" y1="42.604690625" x2="331.886609375" y2="42.55783125" width="0.025" layer="94"/>
<wire x1="331.886609375" y1="42.55783125" x2="331.84591875" y2="42.4561" width="0.025" layer="94"/>
<wire x1="331.84591875" y1="42.4561" x2="331.777340625" y2="42.318940625" width="0.025" layer="94"/>
<wire x1="331.777340625" y1="42.318940625" x2="331.708759375" y2="42.21606875" width="0.025" layer="94"/>
<wire x1="331.708759375" y1="42.21606875" x2="331.67446875" y2="42.17035" width="0.025" layer="94"/>
<wire x1="331.67446875" y1="42.17035" x2="331.56016875" y2="42.033190625" width="0.025" layer="94"/>
<wire x1="331.56016875" y1="42.033190625" x2="331.40015" y2="41.87316875" width="0.025" layer="94"/>
<wire x1="331.40015" y1="41.87316875" x2="331.262990625" y2="41.75886875" width="0.025" layer="94"/>
<wire x1="331.262990625" y1="41.75886875" x2="331.17155" y2="41.690290625" width="0.025" layer="94"/>
<wire x1="331.17155" y1="41.690290625" x2="331.034390625" y2="41.59885" width="0.025" layer="94"/>
<wire x1="331.034390625" y1="41.59885" x2="330.977240625" y2="41.564559375" width="0.025" layer="94"/>
<wire x1="330.977240625" y1="41.564559375" x2="330.89723125" y2="41.518840625" width="0.025" layer="94"/>
<wire x1="330.89723125" y1="41.518840625" x2="330.84008125" y2="41.48455" width="0.025" layer="94"/>
<wire x1="330.84008125" y1="41.48455" x2="330.5429" y2="41.335959375" width="0.025" layer="94"/>
<wire x1="330.5429" y1="41.335959375" x2="330.462890625" y2="41.30166875" width="0.025" layer="94"/>
<wire x1="330.462890625" y1="41.30166875" x2="330.348590625" y2="41.25595" width="0.025" layer="94"/>
<wire x1="330.348590625" y1="41.25595" x2="330.25715" y2="41.221659375" width="0.025" layer="94"/>
<wire x1="330.25715" y1="41.221659375" x2="330.13141875" y2="41.175940625" width="0.025" layer="94"/>
<wire x1="330.13141875" y1="41.175940625" x2="329.994259375" y2="41.13021875" width="0.025" layer="94"/>
<wire x1="329.994259375" y1="41.13021875" x2="329.879959375" y2="41.09593125" width="0.025" layer="94"/>
<wire x1="329.879959375" y1="41.09593125" x2="329.75423125" y2="41.061640625" width="0.025" layer="94"/>
<wire x1="329.75423125" y1="41.061640625" x2="329.67421875" y2="41.03878125" width="0.025" layer="94"/>
<wire x1="329.67421875" y1="41.03878125" x2="329.52563125" y2="41.004490625" width="0.025" layer="94"/>
<wire x1="329.52563125" y1="41.004490625" x2="329.319890625" y2="40.95876875" width="0.025" layer="94"/>
<wire x1="329.319890625" y1="40.95876875" x2="329.194159375" y2="40.935909375" width="0.025" layer="94"/>
<wire x1="328.35108125" y1="37.03828125" x2="327.58253125" y2="40.78731875" width="0.025" layer="94"/>
<wire x1="327.58253125" y1="40.78731875" x2="326.86038125" y2="37.084" width="0.025" layer="94"/>
<wire x1="326.86038125" y1="37.084" x2="326.805290625" y2="37.01541875" width="0.025" layer="94"/>
<wire x1="271.78" y1="236.22" x2="271.78" y2="231.14" width="0.025" layer="94"/>
<wire x1="271.78" y1="231.14" x2="271.78" y2="226.06" width="0.025" layer="94"/>
<wire x1="271.78" y1="226.06" x2="279.4" y2="226.06" width="0.025" layer="94"/>
<wire x1="279.4" y1="226.06" x2="289.56" y2="226.06" width="0.025" layer="94"/>
<wire x1="289.56" y1="226.06" x2="299.72" y2="226.06" width="0.025" layer="94"/>
<wire x1="299.72" y1="226.06" x2="309.88" y2="226.06" width="0.025" layer="94"/>
<wire x1="309.88" y1="226.06" x2="375.92" y2="226.06" width="0.025" layer="94"/>
<wire x1="271.78" y1="231.14" x2="375.92" y2="231.14" width="0.025" layer="94"/>
<wire x1="279.4" y1="236.22" x2="279.4" y2="226.06" width="0.025" layer="94"/>
<wire x1="289.56" y1="236.22" x2="289.56" y2="226.06" width="0.025" layer="94"/>
<wire x1="299.72" y1="236.22" x2="299.72" y2="226.06" width="0.025" layer="94"/>
<wire x1="309.88" y1="236.22" x2="309.88" y2="226.06" width="0.025" layer="94"/>
<text x="272.796" y="35.306" size="1.778" layer="94" font="vector" ratio="12">DRAWN
BY</text>
<text x="272.542" y="28.702" size="1.778" layer="94" font="vector" ratio="12">CHECKED
BY</text>
<text x="272.542" y="22.352" size="1.778" layer="94" font="vector" ratio="12">DESIGN
APPROVAL</text>
<text x="287.02" y="42.164" size="1.778" layer="94" font="vector" ratio="12">INIT</text>
<text x="294.386" y="42.164" size="1.778" layer="94" font="vector" ratio="12">DATE</text>
<text x="333.502" y="33.528" size="1.6764" layer="94" font="vector" ratio="12">JOHN MEZZALINGUA ASSOCIATES
 7645 HENRY CLAY BOULEVARD
  LIVERPOOL, NEW YORK 13088
       TEL. 315-431-7100</text>
<text x="307.34" y="22.86" size="1.27" layer="94" font="vector" ratio="12">THIS DRAWING AND SPECIFICATIONS ARE THE PROPERTY OF
JOHN MEZZALINGUA ASSOCIATES AND SHALL NOT BE 
REPRODICED, COPIED, OR USED AS A BASIS FOR MANUFACTURING OR
SALE OF EQUIPMENT OR DEVICES WITHOUT WRITTEN PERMISSION.</text>
<text x="305.562" y="19.812" size="1.27" layer="94" font="vector" ratio="12">TITLE: </text>
<text x="305.562" y="13.462" size="1.27" layer="94" font="vector" ratio="12">DWG. NO.</text>
<text x="370.332" y="13.462" size="1.27" layer="94" font="vector" ratio="12">REV.</text>
<text x="360.172" y="7.112" size="1.27" layer="94" font="vector" ratio="12">SIZE:   B</text>
<text x="323.342" y="7.112" size="1.27" layer="94" font="vector" ratio="12">SHEET</text>
<text x="305.562" y="7.112" size="1.27" layer="94" font="vector" ratio="12">SCALE: NONE</text>
<text x="317.5" y="10.16" size="1.778" layer="94" font="vector">&gt;DRAWING_NAME</text>
<text x="332.486" y="5.842" size="1.27" layer="94" font="vector">&gt;SHEET</text>
<text x="273.304" y="233.68" size="1.778" layer="94" font="vector" ratio="12">REV</text>
<text x="281.178" y="233.68" size="1.778" layer="94" font="vector" ratio="12">ZONE</text>
<text x="291.592" y="233.68" size="1.778" layer="94" font="vector" ratio="12">DATE</text>
<text x="302.26" y="233.68" size="1.778" layer="94" font="vector" ratio="12">ECO</text>
<text x="333.502" y="233.426" size="1.778" layer="94" font="vector" ratio="12">DESCRIPTION</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="A3_LOC_JMA_RELEASE">
<gates>
<gate name="G$1" symbol="A3_LOC_JMA_RELEASE" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="JMA_LBR_v2" urn="urn:adsk.eagle:library:8413580">
<packages>
<package name="SOIC127P600X175-8" urn="urn:adsk.eagle:footprint:3833981/2" library_version="44">
<description>8-SOIC, 1.27 mm pitch, 6 mm span, 4.9 X 3.9 X 1.75 mm body
&lt;p&gt;8-pin SOIC package with 1.27 mm pitch, 6 mm span with body size 4.9 X 3.9 X 1.75 mm&lt;/p&gt;</description>
<circle x="-2.7288" y="2.7086" radius="0.25" width="0" layer="21"/>
<wire x1="-2" y1="2.5186" x2="2" y2="2.5186" width="0.12" layer="21"/>
<wire x1="-2" y1="-2.5186" x2="2" y2="-2.5186" width="0.12" layer="21"/>
<wire x1="2" y1="-2.5" x2="-2" y2="-2.5" width="0.12" layer="51"/>
<wire x1="-2" y1="-2.5" x2="-2" y2="2.5" width="0.12" layer="51"/>
<wire x1="-2" y1="2.5" x2="2" y2="2.5" width="0.12" layer="51"/>
<wire x1="2" y1="2.5" x2="2" y2="-2.5" width="0.12" layer="51"/>
<smd name="1" x="-2.4734" y="1.905" dx="1.9685" dy="0.5991" layer="1"/>
<smd name="2" x="-2.4734" y="0.635" dx="1.9685" dy="0.5991" layer="1"/>
<smd name="3" x="-2.4734" y="-0.635" dx="1.9685" dy="0.5991" layer="1"/>
<smd name="4" x="-2.4734" y="-1.905" dx="1.9685" dy="0.5991" layer="1"/>
<smd name="5" x="2.4734" y="-1.905" dx="1.9685" dy="0.5991" layer="1"/>
<smd name="6" x="2.4734" y="-0.635" dx="1.9685" dy="0.5991" layer="1"/>
<smd name="7" x="2.4734" y="0.635" dx="1.9685" dy="0.5991" layer="1"/>
<smd name="8" x="2.4734" y="1.905" dx="1.9685" dy="0.5991" layer="1"/>
<text x="0" y="3.5936" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-3.1536" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC1005X60" urn="urn:adsk.eagle:footprint:3793110/1" library_version="44">
<description>CHIP, 1.025 X 0.525 X 0.6 mm body
&lt;p&gt;CHIP package with body size 1.025 X 0.525 X 0.6 mm&lt;/p&gt;</description>
<wire x1="0.55" y1="0.6325" x2="-0.55" y2="0.6325" width="0.12" layer="21"/>
<wire x1="0.55" y1="-0.6325" x2="-0.55" y2="-0.6325" width="0.12" layer="21"/>
<wire x1="0.55" y1="-0.3" x2="-0.55" y2="-0.3" width="0.12" layer="51"/>
<wire x1="-0.55" y1="-0.3" x2="-0.55" y2="0.3" width="0.12" layer="51"/>
<wire x1="-0.55" y1="0.3" x2="0.55" y2="0.3" width="0.12" layer="51"/>
<wire x1="0.55" y1="0.3" x2="0.55" y2="-0.3" width="0.12" layer="51"/>
<smd name="1" x="-0.4875" y="0" dx="0.5621" dy="0.6371" layer="1"/>
<smd name="2" x="0.4875" y="0" dx="0.5621" dy="0.6371" layer="1"/>
<text x="0" y="1.2675" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.2675" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC1220X107" urn="urn:adsk.eagle:footprint:3793123/1" library_version="44">
<description>CHIP, 1.25 X 2 X 1.07 mm body
&lt;p&gt;CHIP package with body size 1.25 X 2 X 1.07 mm&lt;/p&gt;</description>
<wire x1="0.725" y1="1.4217" x2="-0.725" y2="1.4217" width="0.12" layer="21"/>
<wire x1="0.725" y1="-1.4217" x2="-0.725" y2="-1.4217" width="0.12" layer="21"/>
<wire x1="0.725" y1="-1.1" x2="-0.725" y2="-1.1" width="0.12" layer="51"/>
<wire x1="-0.725" y1="-1.1" x2="-0.725" y2="1.1" width="0.12" layer="51"/>
<wire x1="-0.725" y1="1.1" x2="0.725" y2="1.1" width="0.12" layer="51"/>
<wire x1="0.725" y1="1.1" x2="0.725" y2="-1.1" width="0.12" layer="51"/>
<smd name="1" x="-0.552" y="0" dx="0.7614" dy="2.2153" layer="1"/>
<smd name="2" x="0.552" y="0" dx="0.7614" dy="2.2153" layer="1"/>
<text x="0" y="2.0567" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.0567" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC1608X55" urn="urn:adsk.eagle:footprint:3793129/2" library_version="44">
<description>CHIP, 1.6 X 0.8 X 0.55 mm body
&lt;p&gt;CHIP package with body size 1.6 X 0.8 X 0.55 mm&lt;/p&gt;</description>
<wire x1="0.85" y1="0.6516" x2="-0.85" y2="0.6516" width="0.12" layer="21"/>
<wire x1="0.85" y1="-0.6516" x2="-0.85" y2="-0.6516" width="0.12" layer="21"/>
<wire x1="0.85" y1="-0.45" x2="-0.85" y2="-0.45" width="0.12" layer="51"/>
<wire x1="-0.85" y1="-0.45" x2="-0.85" y2="0.45" width="0.12" layer="51"/>
<wire x1="-0.85" y1="0.45" x2="0.85" y2="0.45" width="0.12" layer="51"/>
<wire x1="0.85" y1="0.45" x2="0.85" y2="-0.45" width="0.12" layer="51"/>
<smd name="1" x="-0.7704" y="0" dx="0.8884" dy="0.9291" layer="1"/>
<smd name="2" x="0.7704" y="0" dx="0.8884" dy="0.9291" layer="1"/>
<text x="0" y="1.4136" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.4136" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC1632X168" urn="urn:adsk.eagle:footprint:3793133/1" library_version="44">
<description>CHIP, 1.6 X 3.2 X 1.68 mm body
&lt;p&gt;CHIP package with body size 1.6 X 3.2 X 1.68 mm&lt;/p&gt;</description>
<wire x1="0.9" y1="2.0217" x2="-0.9" y2="2.0217" width="0.12" layer="21"/>
<wire x1="0.9" y1="-2.0217" x2="-0.9" y2="-2.0217" width="0.12" layer="21"/>
<wire x1="0.9" y1="-1.7" x2="-0.9" y2="-1.7" width="0.12" layer="51"/>
<wire x1="-0.9" y1="-1.7" x2="-0.9" y2="1.7" width="0.12" layer="51"/>
<wire x1="-0.9" y1="1.7" x2="0.9" y2="1.7" width="0.12" layer="51"/>
<wire x1="0.9" y1="1.7" x2="0.9" y2="-1.7" width="0.12" layer="51"/>
<smd name="1" x="-0.786" y="0" dx="0.9434" dy="3.4153" layer="1"/>
<smd name="2" x="0.786" y="0" dx="0.9434" dy="3.4153" layer="1"/>
<text x="0" y="2.6567" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.6567" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC2012X127" urn="urn:adsk.eagle:footprint:3793140/1" library_version="44">
<description>CHIP, 2.01 X 1.25 X 1.27 mm body
&lt;p&gt;CHIP package with body size 2.01 X 1.25 X 1.27 mm&lt;/p&gt;</description>
<wire x1="1.105" y1="1.0467" x2="-1.105" y2="1.0467" width="0.12" layer="21"/>
<wire x1="1.105" y1="-1.0467" x2="-1.105" y2="-1.0467" width="0.12" layer="21"/>
<wire x1="1.105" y1="-0.725" x2="-1.105" y2="-0.725" width="0.12" layer="51"/>
<wire x1="-1.105" y1="-0.725" x2="-1.105" y2="0.725" width="0.12" layer="51"/>
<wire x1="-1.105" y1="0.725" x2="1.105" y2="0.725" width="0.12" layer="51"/>
<wire x1="1.105" y1="0.725" x2="1.105" y2="-0.725" width="0.12" layer="51"/>
<smd name="1" x="-0.8804" y="0" dx="1.1646" dy="1.4653" layer="1"/>
<smd name="2" x="0.8804" y="0" dx="1.1646" dy="1.4653" layer="1"/>
<text x="0" y="1.6817" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.6817" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC3215X168" urn="urn:adsk.eagle:footprint:3793146/1" library_version="44">
<description>CHIP, 3.2 X 1.5 X 1.68 mm body
&lt;p&gt;CHIP package with body size 3.2 X 1.5 X 1.68 mm&lt;/p&gt;</description>
<wire x1="1.7" y1="1.1286" x2="-1.7" y2="1.1286" width="0.12" layer="21"/>
<wire x1="1.7" y1="-1.1286" x2="-1.7" y2="-1.1286" width="0.12" layer="21"/>
<wire x1="1.7" y1="-0.8" x2="-1.7" y2="-0.8" width="0.12" layer="51"/>
<wire x1="-1.7" y1="-0.8" x2="-1.7" y2="0.8" width="0.12" layer="51"/>
<wire x1="-1.7" y1="0.8" x2="1.7" y2="0.8" width="0.12" layer="51"/>
<wire x1="1.7" y1="0.8" x2="1.7" y2="-0.8" width="0.12" layer="51"/>
<smd name="1" x="-1.4913" y="0" dx="1.1327" dy="1.6291" layer="1"/>
<smd name="2" x="1.4913" y="0" dx="1.1327" dy="1.6291" layer="1"/>
<text x="0" y="1.7636" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.7636" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC3225X168" urn="urn:adsk.eagle:footprint:3793150/1" library_version="44">
<description>CHIP, 3.2 X 2.5 X 1.68 mm body
&lt;p&gt;CHIP package with body size 3.2 X 2.5 X 1.68 mm&lt;/p&gt;</description>
<wire x1="1.7" y1="1.6717" x2="-1.7" y2="1.6717" width="0.12" layer="21"/>
<wire x1="1.7" y1="-1.6717" x2="-1.7" y2="-1.6717" width="0.12" layer="21"/>
<wire x1="1.7" y1="-1.35" x2="-1.7" y2="-1.35" width="0.12" layer="51"/>
<wire x1="-1.7" y1="-1.35" x2="-1.7" y2="1.35" width="0.12" layer="51"/>
<wire x1="-1.7" y1="1.35" x2="1.7" y2="1.35" width="0.12" layer="51"/>
<wire x1="1.7" y1="1.35" x2="1.7" y2="-1.35" width="0.12" layer="51"/>
<smd name="1" x="-1.4913" y="0" dx="1.1327" dy="2.7153" layer="1"/>
<smd name="2" x="1.4913" y="0" dx="1.1327" dy="2.7153" layer="1"/>
<text x="0" y="2.3067" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.3067" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC4520X168" urn="urn:adsk.eagle:footprint:3793156/1" library_version="44">
<description>CHIP, 4.5 X 2.03 X 1.68 mm body
&lt;p&gt;CHIP package with body size 4.5 X 2.03 X 1.68 mm&lt;/p&gt;</description>
<wire x1="2.375" y1="1.4602" x2="-2.375" y2="1.4602" width="0.12" layer="21"/>
<wire x1="2.375" y1="-1.4602" x2="-2.375" y2="-1.4602" width="0.12" layer="21"/>
<wire x1="2.375" y1="-1.14" x2="-2.375" y2="-1.14" width="0.12" layer="51"/>
<wire x1="-2.375" y1="-1.14" x2="-2.375" y2="1.14" width="0.12" layer="51"/>
<wire x1="-2.375" y1="1.14" x2="2.375" y2="1.14" width="0.12" layer="51"/>
<wire x1="2.375" y1="1.14" x2="2.375" y2="-1.14" width="0.12" layer="51"/>
<smd name="1" x="-2.1266" y="0" dx="1.2091" dy="2.2923" layer="1"/>
<smd name="2" x="2.1266" y="0" dx="1.2091" dy="2.2923" layer="1"/>
<text x="0" y="2.0952" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.0952" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC4532X218" urn="urn:adsk.eagle:footprint:3793162/1" library_version="44">
<description>CHIP, 4.5 X 3.2 X 2.18 mm body
&lt;p&gt;CHIP package with body size 4.5 X 3.2 X 2.18 mm&lt;/p&gt;</description>
<wire x1="2.375" y1="2.0217" x2="-2.375" y2="2.0217" width="0.12" layer="21"/>
<wire x1="2.375" y1="-2.0217" x2="-2.375" y2="-2.0217" width="0.12" layer="21"/>
<wire x1="2.375" y1="-1.7" x2="-2.375" y2="-1.7" width="0.12" layer="51"/>
<wire x1="-2.375" y1="-1.7" x2="-2.375" y2="1.7" width="0.12" layer="51"/>
<wire x1="-2.375" y1="1.7" x2="2.375" y2="1.7" width="0.12" layer="51"/>
<wire x1="2.375" y1="1.7" x2="2.375" y2="-1.7" width="0.12" layer="51"/>
<smd name="1" x="-2.1266" y="0" dx="1.2091" dy="3.4153" layer="1"/>
<smd name="2" x="2.1266" y="0" dx="1.2091" dy="3.4153" layer="1"/>
<text x="0" y="2.6567" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.6567" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC4564X218" urn="urn:adsk.eagle:footprint:3793169/1" library_version="44">
<description>CHIP, 4.5 X 6.4 X 2.18 mm body
&lt;p&gt;CHIP package with body size 4.5 X 6.4 X 2.18 mm&lt;/p&gt;</description>
<wire x1="2.375" y1="3.6452" x2="-2.375" y2="3.6452" width="0.12" layer="21"/>
<wire x1="2.375" y1="-3.6452" x2="-2.375" y2="-3.6452" width="0.12" layer="21"/>
<wire x1="2.375" y1="-3.325" x2="-2.375" y2="-3.325" width="0.12" layer="51"/>
<wire x1="-2.375" y1="-3.325" x2="-2.375" y2="3.325" width="0.12" layer="51"/>
<wire x1="-2.375" y1="3.325" x2="2.375" y2="3.325" width="0.12" layer="51"/>
<wire x1="2.375" y1="3.325" x2="2.375" y2="-3.325" width="0.12" layer="51"/>
<smd name="1" x="-2.1266" y="0" dx="1.2091" dy="6.6623" layer="1"/>
<smd name="2" x="2.1266" y="0" dx="1.2091" dy="6.6623" layer="1"/>
<text x="0" y="4.2802" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-4.2802" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC5650X218" urn="urn:adsk.eagle:footprint:3793172/1" library_version="44">
<description>CHIP, 5.64 X 5.08 X 2.18 mm body
&lt;p&gt;CHIP package with body size 5.64 X 5.08 X 2.18 mm&lt;/p&gt;</description>
<wire x1="2.895" y1="2.9852" x2="-2.895" y2="2.9852" width="0.12" layer="21"/>
<wire x1="2.895" y1="-2.9852" x2="-2.895" y2="-2.9852" width="0.12" layer="21"/>
<wire x1="2.895" y1="-2.665" x2="-2.895" y2="-2.665" width="0.12" layer="51"/>
<wire x1="-2.895" y1="-2.665" x2="-2.895" y2="2.665" width="0.12" layer="51"/>
<wire x1="-2.895" y1="2.665" x2="2.895" y2="2.665" width="0.12" layer="51"/>
<wire x1="2.895" y1="2.665" x2="2.895" y2="-2.665" width="0.12" layer="51"/>
<smd name="1" x="-2.6854" y="0" dx="1.1393" dy="5.3423" layer="1"/>
<smd name="2" x="2.6854" y="0" dx="1.1393" dy="5.3423" layer="1"/>
<text x="0" y="3.6202" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-3.6202" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC5563X218" urn="urn:adsk.eagle:footprint:3793175/1" library_version="44">
<description>CHIP, 5.59 X 6.35 X 2.18 mm body
&lt;p&gt;CHIP package with body size 5.59 X 6.35 X 2.18 mm&lt;/p&gt;</description>
<wire x1="2.92" y1="3.6202" x2="-2.92" y2="3.6202" width="0.12" layer="21"/>
<wire x1="2.92" y1="-3.6202" x2="-2.92" y2="-3.6202" width="0.12" layer="21"/>
<wire x1="2.92" y1="-3.3" x2="-2.92" y2="-3.3" width="0.12" layer="51"/>
<wire x1="-2.92" y1="-3.3" x2="-2.92" y2="3.3" width="0.12" layer="51"/>
<wire x1="-2.92" y1="3.3" x2="2.92" y2="3.3" width="0.12" layer="51"/>
<wire x1="2.92" y1="3.3" x2="2.92" y2="-3.3" width="0.12" layer="51"/>
<smd name="1" x="-2.6716" y="0" dx="1.2091" dy="6.6123" layer="1"/>
<smd name="2" x="2.6716" y="0" dx="1.2091" dy="6.6123" layer="1"/>
<text x="0" y="4.2552" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-4.2552" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC9198X218" urn="urn:adsk.eagle:footprint:3793179/1" library_version="44">
<description>CHIP, 9.14 X 9.82 X 2.18 mm body
&lt;p&gt;CHIP package with body size 9.14 X 9.82 X 2.18 mm&lt;/p&gt;</description>
<wire x1="4.76" y1="5.2799" x2="-4.76" y2="5.2799" width="0.12" layer="21"/>
<wire x1="4.76" y1="-5.2799" x2="-4.76" y2="-5.2799" width="0.12" layer="21"/>
<wire x1="4.76" y1="-4.91" x2="-4.76" y2="-4.91" width="0.12" layer="51"/>
<wire x1="-4.76" y1="-4.91" x2="-4.76" y2="4.91" width="0.12" layer="51"/>
<wire x1="-4.76" y1="4.91" x2="4.76" y2="4.91" width="0.12" layer="51"/>
<wire x1="4.76" y1="4.91" x2="4.76" y2="-4.91" width="0.12" layer="51"/>
<smd name="1" x="-4.4571" y="0" dx="1.314" dy="9.9318" layer="1"/>
<smd name="2" x="4.4571" y="0" dx="1.314" dy="9.9318" layer="1"/>
<text x="0" y="5.9149" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-5.9149" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="RESC1005X40" urn="urn:adsk.eagle:footprint:3811164/1" library_version="44">
<description>CHIP, 1 X 0.5 X 0.4 mm body
&lt;p&gt;CHIP package with body size 1 X 0.5 X 0.4 mm&lt;/p&gt;</description>
<wire x1="0.525" y1="0.614" x2="-0.525" y2="0.614" width="0.12" layer="21"/>
<wire x1="0.525" y1="-0.614" x2="-0.525" y2="-0.614" width="0.12" layer="21"/>
<wire x1="0.525" y1="-0.275" x2="-0.525" y2="-0.275" width="0.12" layer="51"/>
<wire x1="-0.525" y1="-0.275" x2="-0.525" y2="0.275" width="0.12" layer="51"/>
<wire x1="-0.525" y1="0.275" x2="0.525" y2="0.275" width="0.12" layer="51"/>
<wire x1="0.525" y1="0.275" x2="0.525" y2="-0.275" width="0.12" layer="51"/>
<smd name="1" x="-0.475" y="0" dx="0.55" dy="0.6" layer="1"/>
<smd name="2" x="0.475" y="0" dx="0.55" dy="0.6" layer="1"/>
<text x="0" y="1.249" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.249" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="RESC0603X30" urn="urn:adsk.eagle:footprint:3811152/1" library_version="44">
<description>CHIP, 0.6 X 0.3 X 0.3 mm body
&lt;p&gt;CHIP package with body size 0.6 X 0.3 X 0.3 mm&lt;/p&gt;</description>
<wire x1="0.315" y1="0.5124" x2="-0.315" y2="0.5124" width="0.12" layer="21"/>
<wire x1="0.315" y1="-0.5124" x2="-0.315" y2="-0.5124" width="0.12" layer="21"/>
<wire x1="0.315" y1="-0.165" x2="-0.315" y2="-0.165" width="0.12" layer="51"/>
<wire x1="-0.315" y1="-0.165" x2="-0.315" y2="0.165" width="0.12" layer="51"/>
<wire x1="-0.315" y1="0.165" x2="0.315" y2="0.165" width="0.12" layer="51"/>
<wire x1="0.315" y1="0.165" x2="0.315" y2="-0.165" width="0.12" layer="51"/>
<smd name="1" x="-0.325" y="0" dx="0.4469" dy="0.3969" layer="1"/>
<smd name="2" x="0.325" y="0" dx="0.4469" dy="0.3969" layer="1"/>
<text x="0" y="1.1474" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.1474" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="RESC1608X55" urn="urn:adsk.eagle:footprint:3811167/1" library_version="44">
<description>CHIP, 1.6 X 0.85 X 0.55 mm body
&lt;p&gt;CHIP package with body size 1.6 X 0.85 X 0.55 mm&lt;/p&gt;</description>
<wire x1="0.875" y1="0.8036" x2="-0.875" y2="0.8036" width="0.12" layer="21"/>
<wire x1="0.875" y1="-0.8036" x2="-0.875" y2="-0.8036" width="0.12" layer="21"/>
<wire x1="0.875" y1="-0.475" x2="-0.875" y2="-0.475" width="0.12" layer="51"/>
<wire x1="-0.875" y1="-0.475" x2="-0.875" y2="0.475" width="0.12" layer="51"/>
<wire x1="-0.875" y1="0.475" x2="0.875" y2="0.475" width="0.12" layer="51"/>
<wire x1="0.875" y1="0.475" x2="0.875" y2="-0.475" width="0.12" layer="51"/>
<smd name="1" x="-0.7996" y="0" dx="0.8709" dy="0.9791" layer="1"/>
<smd name="2" x="0.7996" y="0" dx="0.8709" dy="0.9791" layer="1"/>
<text x="0" y="1.4386" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.4386" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="RESC2012X70" urn="urn:adsk.eagle:footprint:3811170/1" library_version="44">
<description>CHIP, 2 X 1.25 X 0.7 mm body
&lt;p&gt;CHIP package with body size 2 X 1.25 X 0.7 mm&lt;/p&gt;</description>
<wire x1="1.1" y1="1.0036" x2="-1.1" y2="1.0036" width="0.12" layer="21"/>
<wire x1="1.1" y1="-1.0036" x2="-1.1" y2="-1.0036" width="0.12" layer="21"/>
<wire x1="1.1" y1="-0.675" x2="-1.1" y2="-0.675" width="0.12" layer="51"/>
<wire x1="-1.1" y1="-0.675" x2="-1.1" y2="0.675" width="0.12" layer="51"/>
<wire x1="-1.1" y1="0.675" x2="1.1" y2="0.675" width="0.12" layer="51"/>
<wire x1="1.1" y1="0.675" x2="1.1" y2="-0.675" width="0.12" layer="51"/>
<smd name="1" x="-0.94" y="0" dx="1.0354" dy="1.3791" layer="1"/>
<smd name="2" x="0.94" y="0" dx="1.0354" dy="1.3791" layer="1"/>
<text x="0" y="1.6386" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.6386" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="RESC3115X70" urn="urn:adsk.eagle:footprint:3811174/1" library_version="44">
<description>CHIP, 3.125 X 1.55 X 0.7 mm body
&lt;p&gt;CHIP package with body size 3.125 X 1.55 X 0.7 mm&lt;/p&gt;</description>
<wire x1="1.625" y1="1.1536" x2="-1.625" y2="1.1536" width="0.12" layer="21"/>
<wire x1="1.625" y1="-1.1536" x2="-1.625" y2="-1.1536" width="0.12" layer="21"/>
<wire x1="1.625" y1="-0.825" x2="-1.625" y2="-0.825" width="0.12" layer="51"/>
<wire x1="-1.625" y1="-0.825" x2="-1.625" y2="0.825" width="0.12" layer="51"/>
<wire x1="-1.625" y1="0.825" x2="1.625" y2="0.825" width="0.12" layer="51"/>
<wire x1="1.625" y1="0.825" x2="1.625" y2="-0.825" width="0.12" layer="51"/>
<smd name="1" x="-1.4449" y="0" dx="1.0841" dy="1.6791" layer="1"/>
<smd name="2" x="1.4449" y="0" dx="1.0841" dy="1.6791" layer="1"/>
<text x="0" y="1.7886" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.7886" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="RESC3225X70" urn="urn:adsk.eagle:footprint:3811185/1" library_version="44">
<description>CHIP, 3.2 X 2.5 X 0.7 mm body
&lt;p&gt;CHIP package with body size 3.2 X 2.5 X 0.7 mm&lt;/p&gt;</description>
<wire x1="1.7" y1="1.6717" x2="-1.7" y2="1.6717" width="0.12" layer="21"/>
<wire x1="1.7" y1="-1.6717" x2="-1.7" y2="-1.6717" width="0.12" layer="21"/>
<wire x1="1.7" y1="-1.35" x2="-1.7" y2="-1.35" width="0.12" layer="51"/>
<wire x1="-1.7" y1="-1.35" x2="-1.7" y2="1.35" width="0.12" layer="51"/>
<wire x1="-1.7" y1="1.35" x2="1.7" y2="1.35" width="0.12" layer="51"/>
<wire x1="1.7" y1="1.35" x2="1.7" y2="-1.35" width="0.12" layer="51"/>
<smd name="1" x="-1.49" y="0" dx="1.1354" dy="2.7153" layer="1"/>
<smd name="2" x="1.49" y="0" dx="1.1354" dy="2.7153" layer="1"/>
<text x="0" y="2.3067" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.3067" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="RESC4532X70" urn="urn:adsk.eagle:footprint:3811192/1" library_version="44">
<description>CHIP, 4.5 X 3.2 X 0.7 mm body
&lt;p&gt;CHIP package with body size 4.5 X 3.2 X 0.7 mm&lt;/p&gt;</description>
<wire x1="2.35" y1="2.0217" x2="-2.35" y2="2.0217" width="0.12" layer="21"/>
<wire x1="2.35" y1="-2.0217" x2="-2.35" y2="-2.0217" width="0.12" layer="21"/>
<wire x1="2.35" y1="-1.7" x2="-2.35" y2="-1.7" width="0.12" layer="51"/>
<wire x1="-2.35" y1="-1.7" x2="-2.35" y2="1.7" width="0.12" layer="51"/>
<wire x1="-2.35" y1="1.7" x2="2.35" y2="1.7" width="0.12" layer="51"/>
<wire x1="2.35" y1="1.7" x2="2.35" y2="-1.7" width="0.12" layer="51"/>
<smd name="1" x="-2.14" y="0" dx="1.1354" dy="3.4153" layer="1"/>
<smd name="2" x="2.14" y="0" dx="1.1354" dy="3.4153" layer="1"/>
<text x="0" y="2.6567" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.6567" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="RESC5025X70" urn="urn:adsk.eagle:footprint:3811199/1" library_version="44">
<description>CHIP, 5 X 2.5 X 0.7 mm body
&lt;p&gt;CHIP package with body size 5 X 2.5 X 0.7 mm&lt;/p&gt;</description>
<wire x1="2.6" y1="1.6717" x2="-2.6" y2="1.6717" width="0.12" layer="21"/>
<wire x1="2.6" y1="-1.6717" x2="-2.6" y2="-1.6717" width="0.12" layer="21"/>
<wire x1="2.6" y1="-1.35" x2="-2.6" y2="-1.35" width="0.12" layer="51"/>
<wire x1="-2.6" y1="-1.35" x2="-2.6" y2="1.35" width="0.12" layer="51"/>
<wire x1="-2.6" y1="1.35" x2="2.6" y2="1.35" width="0.12" layer="51"/>
<wire x1="2.6" y1="1.35" x2="2.6" y2="-1.35" width="0.12" layer="51"/>
<smd name="1" x="-2.34" y="0" dx="1.2354" dy="2.7153" layer="1"/>
<smd name="2" x="2.34" y="0" dx="1.2354" dy="2.7153" layer="1"/>
<text x="0" y="2.3067" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.3067" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="RESC5750X229" urn="urn:adsk.eagle:footprint:3811213/1" library_version="44">
<description>CHIP, 5.7 X 5 X 2.29 mm body
&lt;p&gt;CHIP package with body size 5.7 X 5 X 2.29 mm&lt;/p&gt;</description>
<wire x1="3.05" y1="3.0179" x2="-3.05" y2="3.0179" width="0.12" layer="21"/>
<wire x1="3.05" y1="-3.0179" x2="-3.05" y2="-3.0179" width="0.12" layer="21"/>
<wire x1="3.05" y1="-2.7" x2="-3.05" y2="-2.7" width="0.12" layer="51"/>
<wire x1="-3.05" y1="-2.7" x2="-3.05" y2="2.7" width="0.12" layer="51"/>
<wire x1="-3.05" y1="2.7" x2="3.05" y2="2.7" width="0.12" layer="51"/>
<wire x1="3.05" y1="2.7" x2="3.05" y2="-2.7" width="0.12" layer="51"/>
<smd name="1" x="-2.6355" y="0" dx="1.5368" dy="5.4078" layer="1"/>
<smd name="2" x="2.6355" y="0" dx="1.5368" dy="5.4078" layer="1"/>
<text x="0" y="3.6529" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-3.6529" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="RESC6432X70" urn="urn:adsk.eagle:footprint:3811285/1" library_version="44">
<description>CHIP, 6.4 X 3.2 X 0.7 mm body
&lt;p&gt;CHIP package with body size 6.4 X 3.2 X 0.7 mm&lt;/p&gt;</description>
<wire x1="3.3" y1="2.0217" x2="-3.3" y2="2.0217" width="0.12" layer="21"/>
<wire x1="3.3" y1="-2.0217" x2="-3.3" y2="-2.0217" width="0.12" layer="21"/>
<wire x1="3.3" y1="-1.7" x2="-3.3" y2="-1.7" width="0.12" layer="51"/>
<wire x1="-3.3" y1="-1.7" x2="-3.3" y2="1.7" width="0.12" layer="51"/>
<wire x1="-3.3" y1="1.7" x2="3.3" y2="1.7" width="0.12" layer="51"/>
<wire x1="3.3" y1="1.7" x2="3.3" y2="-1.7" width="0.12" layer="51"/>
<smd name="1" x="-3.04" y="0" dx="1.2354" dy="3.4153" layer="1"/>
<smd name="2" x="3.04" y="0" dx="1.2354" dy="3.4153" layer="1"/>
<text x="0" y="2.6567" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.6567" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="RESC2037X52" urn="urn:adsk.eagle:footprint:3950970/1" library_version="44">
<description>CHIP, 2 X 3.75 X 0.52 mm body
&lt;p&gt;CHIP package with body size 2 X 3.75 X 0.52 mm&lt;/p&gt;</description>
<wire x1="1.1" y1="2.3442" x2="-1.1" y2="2.3442" width="0.12" layer="21"/>
<wire x1="1.1" y1="-2.3442" x2="-1.1" y2="-2.3442" width="0.12" layer="21"/>
<wire x1="1.1" y1="-2.025" x2="-1.1" y2="-2.025" width="0.12" layer="51"/>
<wire x1="-1.1" y1="-2.025" x2="-1.1" y2="2.025" width="0.12" layer="51"/>
<wire x1="-1.1" y1="2.025" x2="1.1" y2="2.025" width="0.12" layer="51"/>
<wire x1="1.1" y1="2.025" x2="1.1" y2="-2.025" width="0.12" layer="51"/>
<smd name="1" x="-0.94" y="0" dx="1.0354" dy="4.0603" layer="1"/>
<smd name="2" x="0.94" y="0" dx="1.0354" dy="4.0603" layer="1"/>
<text x="0" y="2.9792" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.9792" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="SOIC127P600X175-8" urn="urn:adsk.eagle:package:3833977/2" type="model" library_version="44">
<description>8-SOIC, 1.27 mm pitch, 6 mm span, 4.9 X 3.9 X 1.75 mm body
&lt;p&gt;8-pin SOIC package with 1.27 mm pitch, 6 mm span with body size 4.9 X 3.9 X 1.75 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="SOIC127P600X175-8"/>
</packageinstances>
</package3d>
<package3d name="CAPC1005X60" urn="urn:adsk.eagle:package:3793108/1" type="model" library_version="44">
<description>CHIP, 1.025 X 0.525 X 0.6 mm body
&lt;p&gt;CHIP package with body size 1.025 X 0.525 X 0.6 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC1005X60"/>
</packageinstances>
</package3d>
<package3d name="CAPC1220X107" urn="urn:adsk.eagle:package:3793115/1" type="model" library_version="44">
<description>CHIP, 1.25 X 2 X 1.07 mm body
&lt;p&gt;CHIP package with body size 1.25 X 2 X 1.07 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC1220X107"/>
</packageinstances>
</package3d>
<package3d name="CAPC1608X55" urn="urn:adsk.eagle:package:3793126/2" type="model" library_version="44">
<description>CHIP, 1.6 X 0.8 X 0.55 mm body
&lt;p&gt;CHIP package with body size 1.6 X 0.8 X 0.55 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC1608X55"/>
</packageinstances>
</package3d>
<package3d name="CAPC1632X168" urn="urn:adsk.eagle:package:3793131/1" type="model" library_version="44">
<description>CHIP, 1.6 X 3.2 X 1.68 mm body
&lt;p&gt;CHIP package with body size 1.6 X 3.2 X 1.68 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC1632X168"/>
</packageinstances>
</package3d>
<package3d name="CAPC2012X127" urn="urn:adsk.eagle:package:3793139/1" type="model" library_version="44">
<description>CHIP, 2.01 X 1.25 X 1.27 mm body
&lt;p&gt;CHIP package with body size 2.01 X 1.25 X 1.27 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC2012X127"/>
</packageinstances>
</package3d>
<package3d name="CAPC3215X168" urn="urn:adsk.eagle:package:3793145/1" type="model" library_version="44">
<description>CHIP, 3.2 X 1.5 X 1.68 mm body
&lt;p&gt;CHIP package with body size 3.2 X 1.5 X 1.68 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC3215X168"/>
</packageinstances>
</package3d>
<package3d name="CAPC3225X168" urn="urn:adsk.eagle:package:3793149/1" type="model" library_version="44">
<description>CHIP, 3.2 X 2.5 X 1.68 mm body
&lt;p&gt;CHIP package with body size 3.2 X 2.5 X 1.68 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC3225X168"/>
</packageinstances>
</package3d>
<package3d name="CAPC4520X168" urn="urn:adsk.eagle:package:3793154/1" type="model" library_version="44">
<description>CHIP, 4.5 X 2.03 X 1.68 mm body
&lt;p&gt;CHIP package with body size 4.5 X 2.03 X 1.68 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC4520X168"/>
</packageinstances>
</package3d>
<package3d name="CAPC4532X218" urn="urn:adsk.eagle:package:3793159/1" type="model" library_version="44">
<description>CHIP, 4.5 X 3.2 X 2.18 mm body
&lt;p&gt;CHIP package with body size 4.5 X 3.2 X 2.18 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC4532X218"/>
</packageinstances>
</package3d>
<package3d name="CAPC4564X218" urn="urn:adsk.eagle:package:3793166/1" type="model" library_version="44">
<description>CHIP, 4.5 X 6.4 X 2.18 mm body
&lt;p&gt;CHIP package with body size 4.5 X 6.4 X 2.18 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC4564X218"/>
</packageinstances>
</package3d>
<package3d name="CAPC5650X218" urn="urn:adsk.eagle:package:3793171/1" type="model" library_version="44">
<description>CHIP, 5.64 X 5.08 X 2.18 mm body
&lt;p&gt;CHIP package with body size 5.64 X 5.08 X 2.18 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC5650X218"/>
</packageinstances>
</package3d>
<package3d name="CAPC5563X218" urn="urn:adsk.eagle:package:3793174/1" type="model" library_version="44">
<description>CHIP, 5.59 X 6.35 X 2.18 mm body
&lt;p&gt;CHIP package with body size 5.59 X 6.35 X 2.18 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC5563X218"/>
</packageinstances>
</package3d>
<package3d name="CAPC9198X218" urn="urn:adsk.eagle:package:3793178/1" type="model" library_version="44">
<description>CHIP, 9.14 X 9.82 X 2.18 mm body
&lt;p&gt;CHIP package with body size 9.14 X 9.82 X 2.18 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC9198X218"/>
</packageinstances>
</package3d>
<package3d name="RESC1005X40" urn="urn:adsk.eagle:package:3811154/1" type="model" library_version="44">
<description>CHIP, 1 X 0.5 X 0.4 mm body
&lt;p&gt;CHIP package with body size 1 X 0.5 X 0.4 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESC1005X40"/>
</packageinstances>
</package3d>
<package3d name="RESC0603X30" urn="urn:adsk.eagle:package:3811151/1" type="model" library_version="44">
<description>CHIP, 0.6 X 0.3 X 0.3 mm body
&lt;p&gt;CHIP package with body size 0.6 X 0.3 X 0.3 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESC0603X30"/>
</packageinstances>
</package3d>
<package3d name="RESC1608X55" urn="urn:adsk.eagle:package:3811166/1" type="model" library_version="44">
<description>CHIP, 1.6 X 0.85 X 0.55 mm body
&lt;p&gt;CHIP package with body size 1.6 X 0.85 X 0.55 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESC1608X55"/>
</packageinstances>
</package3d>
<package3d name="RESC2012X70" urn="urn:adsk.eagle:package:3811169/1" type="model" library_version="44">
<description>CHIP, 2 X 1.25 X 0.7 mm body
&lt;p&gt;CHIP package with body size 2 X 1.25 X 0.7 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESC2012X70"/>
</packageinstances>
</package3d>
<package3d name="RESC3115X70" urn="urn:adsk.eagle:package:3811173/1" type="model" library_version="44">
<description>CHIP, 3.125 X 1.55 X 0.7 mm body
&lt;p&gt;CHIP package with body size 3.125 X 1.55 X 0.7 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESC3115X70"/>
</packageinstances>
</package3d>
<package3d name="RESC3225X70" urn="urn:adsk.eagle:package:3811180/1" type="model" library_version="44">
<description>CHIP, 3.2 X 2.5 X 0.7 mm body
&lt;p&gt;CHIP package with body size 3.2 X 2.5 X 0.7 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESC3225X70"/>
</packageinstances>
</package3d>
<package3d name="RESC4532X70" urn="urn:adsk.eagle:package:3811188/1" type="model" library_version="44">
<description>CHIP, 4.5 X 3.2 X 0.7 mm body
&lt;p&gt;CHIP package with body size 4.5 X 3.2 X 0.7 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESC4532X70"/>
</packageinstances>
</package3d>
<package3d name="RESC5025X70" urn="urn:adsk.eagle:package:3811197/1" type="model" library_version="44">
<description>CHIP, 5 X 2.5 X 0.7 mm body
&lt;p&gt;CHIP package with body size 5 X 2.5 X 0.7 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESC5025X70"/>
</packageinstances>
</package3d>
<package3d name="RESC5750X229" urn="urn:adsk.eagle:package:3811212/1" type="model" library_version="44">
<description>CHIP, 5.7 X 5 X 2.29 mm body
&lt;p&gt;CHIP package with body size 5.7 X 5 X 2.29 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESC5750X229"/>
</packageinstances>
</package3d>
<package3d name="RESC6432X70" urn="urn:adsk.eagle:package:3811284/1" type="model" library_version="44">
<description>CHIP, 6.4 X 3.2 X 0.7 mm body
&lt;p&gt;CHIP package with body size 6.4 X 3.2 X 0.7 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESC6432X70"/>
</packageinstances>
</package3d>
<package3d name="RESC2037X52" urn="urn:adsk.eagle:package:3950966/1" type="model" library_version="44">
<description>CHIP, 2 X 3.75 X 0.52 mm body
&lt;p&gt;CHIP package with body size 2 X 3.75 X 0.52 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESC2037X52"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="TRANSCEIVER_RS485" urn="urn:adsk.eagle:symbol:8413649/1" library_version="44">
<text x="1.27" y="-7.62" size="1.778" layer="94" ratio="15">R0</text>
<text x="1.27" y="-12.7" size="1.778" layer="94" ratio="15">!RE</text>
<text x="1.27" y="-17.78" size="1.778" layer="94" ratio="15">DE</text>
<text x="1.27" y="-22.86" size="1.778" layer="94" ratio="15">DI</text>
<text x="19.05" y="-22.86" size="1.778" layer="94" ratio="15" align="bottom-right">GND</text>
<text x="19.05" y="-17.78" size="1.778" layer="94" ratio="15" align="bottom-right">A(D0/RI)</text>
<text x="19.05" y="-12.7" size="1.778" layer="94" ratio="15" align="bottom-right">B(!D0!/!RI!)</text>
<text x="19.05" y="-7.62" size="1.778" layer="94" ratio="15" align="bottom-right">VCC</text>
<text x="6.35" y="-2.54" size="1.778" layer="94" ratio="15">RS485</text>
<text x="0" y="2.54" size="1.778" layer="95" ratio="15">&gt;NAME</text>
<text x="0" y="-27.94" size="1.778" layer="96" ratio="15">&gt;VALUE</text>
<wire x1="0" y1="-2.54" x2="0" y2="-25.4" width="0.254" layer="94"/>
<wire x1="0" y1="-25.4" x2="20.32" y2="-25.4" width="0.254" layer="94"/>
<wire x1="20.32" y1="-25.4" x2="20.32" y2="0" width="0.254" layer="94"/>
<wire x1="20.32" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="0" y2="-2.54" width="0.254" layer="94"/>
<pin name="1" x="-2.54" y="-7.62" visible="pad" length="short"/>
<pin name="2" x="-2.54" y="-12.7" visible="pad" length="short"/>
<pin name="3" x="-2.54" y="-17.78" visible="pad" length="short"/>
<pin name="4" x="-2.54" y="-22.86" visible="pad" length="short"/>
<pin name="5" x="22.86" y="-22.86" visible="pad" length="short" rot="R180"/>
<pin name="6" x="22.86" y="-17.78" visible="pad" length="short" rot="R180"/>
<pin name="7" x="22.86" y="-12.7" visible="pad" length="short" rot="R180"/>
<pin name="8" x="22.86" y="-7.62" visible="pad" length="short" rot="R180"/>
</symbol>
<symbol name="CAPACITOR" urn="urn:adsk.eagle:symbol:8413617/1" library_version="44">
<wire x1="-2.54" y1="0" x2="-0.762" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="0.762" y2="0" width="0.1524" layer="94"/>
<text x="-2.54" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-4.318" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-1.524" y1="-0.254" x2="2.54" y2="0.254" layer="94" rot="R90"/>
<rectangle x1="-2.54" y1="-0.254" x2="1.524" y2="0.254" layer="94" rot="R90"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="RESISTOR" urn="urn:adsk.eagle:symbol:8413611/1" library_version="44">
<text x="-2.54" y="1.524" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.2032" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.2032" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.2032" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="TRANSCEIVER_RS485" urn="urn:adsk.eagle:component:8414510/1" prefix="U" uservalue="yes" library_version="44">
<gates>
<gate name="G$1" symbol="TRANSCEIVER_RS485" x="0" y="0"/>
</gates>
<devices>
<device name="8-SOIC" package="SOIC127P600X175-8">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3833977/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAPACITOR" urn="urn:adsk.eagle:component:8414470/2" prefix="C" uservalue="yes" library_version="44">
<gates>
<gate name="G$1" symbol="CAPACITOR" x="0" y="0"/>
</gates>
<devices>
<device name="0402" package="CAPC1005X60">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793108/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0508" package="CAPC1220X107">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793115/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="CAPC1608X55">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793126/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0612" package="CAPC1632X168">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793131/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="CAPC2012X127">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793139/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="CAPC3215X168">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793145/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1210" package="CAPC3225X168">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793149/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1808" package="CAPC4520X168">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793154/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1812" package="CAPC4532X218">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793159/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1825" package="CAPC4564X218">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793166/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2220" package="CAPC5650X218">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793171/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2225" package="CAPC5563X218">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793174/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3640" package="CAPC9198X218">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793178/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RESISTOR" urn="urn:adsk.eagle:component:8414463/1" prefix="R" uservalue="yes" library_version="44">
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="0201" package="RESC0603X30">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3811151/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402" package="RESC1005X40">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3811154/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="RESC1608X55">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3811166/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="RESC2012X70">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3811169/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="RESC3115X70">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3811173/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1210" package="RESC3225X70">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3811180/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1812" package="RESC4532X70">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3811188/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2010" package="RESC5025X70">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3811197/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2220" package="RESC5750X229">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3811212/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2512" package="RESC6432X70">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3811284/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1508_WIDE" package="RESC2037X52">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3950966/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1" urn="urn:adsk.eagle:library:371">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="VCC" urn="urn:adsk.eagle:symbol:26928/1" library_version="1">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="VCC" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="VCC" urn="urn:adsk.eagle:component:26957/1" prefix="P+" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="VCC" symbol="VCC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="U$3" library="frames" deviceset="A3_LOC_JMA_RELEASE" device=""/>
<part name="U5" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TRANSCEIVER_RS485" device="8-SOIC" package3d_urn="urn:adsk.eagle:package:3833977/2" value="ADM3061EBRZ"/>
<part name="R38" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="0"/>
<part name="R47" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="0"/>
<part name="R36" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="0"/>
<part name="R49" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="0"/>
<part name="C21" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="15pF"/>
<part name="C22" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="15pF"/>
<part name="C20" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0603" package3d_urn="urn:adsk.eagle:package:3793126/2" value="1uF"/>
<part name="SUPPLY55" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY56" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY69" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="P+1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<text x="127" y="215.9" size="1.27" layer="97">DRIVE OUT -&gt; MP_RXD</text>
<text x="127" y="193.04" size="1.27" layer="97">DRIVE IN &lt;- MP_TXD</text>
</plain>
<instances>
<instance part="U$3" gate="G$1" x="0" y="0" smashed="yes">
<attribute name="DRAWING_NAME" x="317.5" y="10.16" size="1.27" layer="94" ratio="15"/>
<attribute name="SHEET" x="332.486" y="5.842" size="1.27" layer="94" ratio="15"/>
</instance>
<instance part="U5" gate="G$1" x="106.68" y="220.98" smashed="yes" rot="MR0">
<attribute name="NAME" x="106.68" y="223.52" size="1.27" layer="95" ratio="15" rot="MR0"/>
<attribute name="VALUE" x="106.68" y="221.488" size="1.27" layer="96" ratio="15" rot="MR0"/>
</instance>
<instance part="R38" gate="G$1" x="50.8" y="210.82" smashed="yes">
<attribute name="NAME" x="43.942" y="211.3026" size="1.27" layer="95"/>
<attribute name="VALUE" x="53.34" y="211.328" size="1.27" layer="96"/>
</instance>
<instance part="R47" gate="G$1" x="50.8" y="203.2" smashed="yes">
<attribute name="NAME" x="43.942" y="203.6826" size="1.27" layer="95"/>
<attribute name="VALUE" x="53.34" y="203.708" size="1.27" layer="96"/>
</instance>
<instance part="R36" gate="G$1" x="119.38" y="213.36" smashed="yes">
<attribute name="NAME" x="112.268" y="213.8426" size="1.27" layer="95"/>
<attribute name="VALUE" x="121.92" y="213.868" size="1.27" layer="96"/>
</instance>
<instance part="R49" gate="G$1" x="119.38" y="198.12" smashed="yes">
<attribute name="NAME" x="112.268" y="198.6026" size="1.27" layer="95"/>
<attribute name="VALUE" x="121.92" y="198.628" size="1.27" layer="96"/>
</instance>
<instance part="C21" gate="G$1" x="33.02" y="193.04" smashed="yes" rot="R90">
<attribute name="NAME" x="31.496" y="191.77" size="1.27" layer="95" ratio="10" rot="R270"/>
<attribute name="VALUE" x="32.512" y="194.31" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C22" gate="G$1" x="38.1" y="193.04" smashed="yes" rot="R90">
<attribute name="NAME" x="36.576" y="191.77" size="1.27" layer="95" ratio="10" rot="R270"/>
<attribute name="VALUE" x="37.592" y="194.31" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C20" gate="G$1" x="71.12" y="223.52" smashed="yes" rot="R180">
<attribute name="NAME" x="66.04" y="224.028" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="72.39" y="224.028" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="SUPPLY55" gate="GND" x="63.5" y="218.44" smashed="yes">
<attribute name="VALUE" x="61.595" y="215.265" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY56" gate="GND" x="30.48" y="182.88" smashed="yes">
<attribute name="VALUE" x="28.575" y="183.515" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="SUPPLY69" gate="GND" x="81.28" y="193.04" smashed="yes">
<attribute name="VALUE" x="84.455" y="192.405" size="1.27" layer="96"/>
</instance>
<instance part="P+1" gate="VCC" x="78.74" y="231.14" smashed="yes">
<attribute name="VALUE" x="76.2" y="228.6" size="1.778" layer="96" rot="R90"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="C20" gate="G$1" pin="2"/>
<wire x1="66.04" y1="223.52" x2="63.5" y2="223.52" width="0.1524" layer="91"/>
<pinref part="SUPPLY55" gate="GND" pin="GND"/>
<wire x1="63.5" y1="223.52" x2="63.5" y2="220.98" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C21" gate="G$1" pin="1"/>
<pinref part="SUPPLY56" gate="GND" pin="GND"/>
<wire x1="33.02" y1="187.96" x2="33.02" y2="185.42" width="0.1524" layer="91"/>
<pinref part="C22" gate="G$1" pin="1"/>
<wire x1="33.02" y1="185.42" x2="30.48" y2="185.42" width="0.1524" layer="91"/>
<wire x1="38.1" y1="187.96" x2="38.1" y2="185.42" width="0.1524" layer="91"/>
<wire x1="38.1" y1="185.42" x2="33.02" y2="185.42" width="0.1524" layer="91"/>
<junction x="33.02" y="185.42"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="5"/>
<pinref part="SUPPLY69" gate="GND" pin="GND"/>
<wire x1="83.82" y1="198.12" x2="81.28" y2="198.12" width="0.1524" layer="91"/>
<wire x1="81.28" y1="198.12" x2="81.28" y2="195.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$99" class="0">
<segment>
<pinref part="R38" gate="G$1" pin="2"/>
<wire x1="55.88" y1="210.82" x2="58.42" y2="210.82" width="0.1524" layer="91"/>
<wire x1="58.42" y1="210.82" x2="58.42" y2="208.28" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="7"/>
<wire x1="58.42" y1="208.28" x2="83.82" y2="208.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$120" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="6"/>
<wire x1="55.88" y1="203.2" x2="83.82" y2="203.2" width="0.1524" layer="91"/>
<pinref part="R47" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$124" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="1"/>
<pinref part="R36" gate="G$1" pin="1"/>
<wire x1="109.22" y1="213.36" x2="114.3" y2="213.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$126" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="4"/>
<pinref part="R49" gate="G$1" pin="1"/>
<wire x1="109.22" y1="198.12" x2="114.3" y2="198.12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="RS485_N" class="0">
<segment>
<pinref part="R38" gate="G$1" pin="1"/>
<wire x1="45.72" y1="210.82" x2="40.64" y2="210.82" width="0.1524" layer="91"/>
<wire x1="40.64" y1="210.82" x2="40.64" y2="205.74" width="0.1524" layer="91"/>
<wire x1="40.64" y1="205.74" x2="38.1" y2="205.74" width="0.1524" layer="91"/>
<pinref part="C22" gate="G$1" pin="2"/>
<wire x1="38.1" y1="205.74" x2="30.48" y2="205.74" width="0.1524" layer="91"/>
<wire x1="38.1" y1="198.12" x2="38.1" y2="205.74" width="0.1524" layer="91"/>
<junction x="38.1" y="205.74"/>
<label x="30.48" y="205.74" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="RS485_P" class="0">
<segment>
<pinref part="R47" gate="G$1" pin="1"/>
<wire x1="45.72" y1="203.2" x2="33.02" y2="203.2" width="0.1524" layer="91"/>
<pinref part="C21" gate="G$1" pin="2"/>
<wire x1="33.02" y1="203.2" x2="30.48" y2="203.2" width="0.1524" layer="91"/>
<wire x1="33.02" y1="198.12" x2="33.02" y2="203.2" width="0.1524" layer="91"/>
<junction x="33.02" y="203.2"/>
<label x="30.48" y="203.2" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="RXD_IN" class="0">
<segment>
<pinref part="R36" gate="G$1" pin="2"/>
<wire x1="124.46" y1="213.36" x2="132.08" y2="213.36" width="0.1524" layer="91"/>
<label x="132.08" y="213.36" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="EN" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="3"/>
<wire x1="109.22" y1="203.2" x2="114.3" y2="203.2" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="2"/>
<wire x1="114.3" y1="203.2" x2="132.08" y2="203.2" width="0.1524" layer="91"/>
<wire x1="109.22" y1="208.28" x2="114.3" y2="208.28" width="0.1524" layer="91"/>
<wire x1="114.3" y1="208.28" x2="114.3" y2="203.2" width="0.1524" layer="91"/>
<junction x="114.3" y="203.2"/>
<label x="132.08" y="203.2" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="TXD_OUT" class="0">
<segment>
<pinref part="R49" gate="G$1" pin="2"/>
<wire x1="124.46" y1="198.12" x2="132.08" y2="198.12" width="0.1524" layer="91"/>
<label x="132.08" y="198.12" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="VCC" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="8"/>
<wire x1="83.82" y1="213.36" x2="78.74" y2="213.36" width="0.1524" layer="91"/>
<wire x1="78.74" y1="213.36" x2="78.74" y2="223.52" width="0.1524" layer="91"/>
<pinref part="C20" gate="G$1" pin="1"/>
<wire x1="78.74" y1="223.52" x2="78.74" y2="228.6" width="0.1524" layer="91"/>
<wire x1="76.2" y1="223.52" x2="78.74" y2="223.52" width="0.1524" layer="91"/>
<junction x="78.74" y="223.52"/>
<pinref part="P+1" gate="VCC" pin="VCC"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
