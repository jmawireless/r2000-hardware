<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.2.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting keepoldvectorfont="yes"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="yes" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="5" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="no" active="no"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="no" active="no"/>
<layer number="103" name="tMap" color="7" fill="1" visible="no" active="no"/>
<layer number="104" name="Name" color="7" fill="1" visible="no" active="no"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="no" active="no"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="no" active="no"/>
<layer number="107" name="Crop" color="7" fill="1" visible="no" active="no"/>
<layer number="108" name="fp8" color="7" fill="1" visible="yes" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="yes" active="yes"/>
<layer number="110" name="110" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="111" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="7" fill="1" visible="yes" active="yes"/>
<layer number="114" name="FRNTMAAT1" color="7" fill="1" visible="no" active="no"/>
<layer number="115" name="FRNTMAAT2" color="7" fill="1" visible="no" active="no"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="no" active="no"/>
<layer number="117" name="BACKMAAT1" color="7" fill="1" visible="no" active="no"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="yes" active="yes"/>
<layer number="119" name="KAP_TEKEN" color="7" fill="1" visible="no" active="no"/>
<layer number="120" name="KAP_MAAT1" color="7" fill="1" visible="no" active="no"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="no" active="no"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="no" active="no"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="top_silk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="130" name="SMDSTROOK" color="7" fill="1" visible="no" active="no"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="133" name="bottom_silk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="DrillLegend" color="7" fill="1" visible="no" active="no"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="14" fill="1" visible="no" active="no"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="no"/>
<layer number="201" name="201bmp" color="2" fill="1" visible="no" active="no"/>
<layer number="202" name="202bmp" color="3" fill="1" visible="no" active="no"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="no" active="no"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="no" active="no"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="no" active="no"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="no" active="no"/>
<layer number="207" name="207bmp" color="15" fill="10" visible="no" active="no"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="no" active="no"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="Eagle3D_PG1" color="7" fill="1" visible="no" active="no"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="no"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="OrgLBR" color="13" fill="1" visible="no" active="no"/>
<layer number="255" name="Accent" color="7" fill="1" visible="no" active="no"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="frames">
<description>&lt;b&gt;Frames for Sheet and Layout&lt;/b&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="A3L-LOC_JMA">
<wire x1="288.29" y1="3.81" x2="342.265" y2="3.81" width="0.1016" layer="94"/>
<wire x1="342.265" y1="3.81" x2="373.38" y2="3.81" width="0.1016" layer="94"/>
<wire x1="373.38" y1="3.81" x2="383.54" y2="3.81" width="0.1016" layer="94"/>
<wire x1="383.54" y1="3.81" x2="383.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="383.54" y1="8.89" x2="383.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="383.54" y1="13.97" x2="383.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="383.54" y1="19.05" x2="383.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="288.29" y1="3.81" x2="288.29" y2="24.13" width="0.1016" layer="94"/>
<wire x1="288.29" y1="24.13" x2="342.265" y2="24.13" width="0.1016" layer="94"/>
<wire x1="342.265" y1="24.13" x2="383.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="373.38" y1="3.81" x2="373.38" y2="8.89" width="0.1016" layer="94"/>
<wire x1="373.38" y1="8.89" x2="383.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="373.38" y1="8.89" x2="342.265" y2="8.89" width="0.1016" layer="94"/>
<wire x1="342.265" y1="8.89" x2="342.265" y2="3.81" width="0.1016" layer="94"/>
<wire x1="342.265" y1="8.89" x2="342.265" y2="13.97" width="0.1016" layer="94"/>
<wire x1="342.265" y1="13.97" x2="383.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="342.265" y1="13.97" x2="342.265" y2="19.05" width="0.1016" layer="94"/>
<wire x1="342.265" y1="19.05" x2="383.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="342.265" y1="19.05" x2="342.265" y2="24.13" width="0.1016" layer="94"/>
<text x="344.17" y="15.24" size="2.54" layer="94">&gt;DRAWING_NAME</text>
<text x="344.17" y="10.16" size="2.286" layer="94">&gt;LAST_DATE_TIME</text>
<text x="357.505" y="5.08" size="2.54" layer="94">&gt;SHEET</text>
<text x="343.916" y="4.953" size="2.54" layer="94">Sheet:</text>
<frame x1="0" y1="0" x2="387.35" y2="260.35" columns="8" rows="5" layer="94"/>
<text x="292.1" y="15.24" size="2.54" layer="94" ratio="15">Proprietory Information
JMA WIRELESS </text>
<text x="344.424" y="20.32" size="2.54" layer="95" ratio="15">&gt;AUTHER</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="A3L-LOC_JMA">
<gates>
<gate name="G$1" symbol="A3L-LOC_JMA" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="JMA_LBR">
<packages>
<package name="JST_B8B-PHDSS">
<pad name="1" x="3" y="-1" drill="0.7"/>
<pad name="2" x="3" y="1" drill="0.7"/>
<pad name="3" x="1" y="-1" drill="0.7"/>
<pad name="4" x="1" y="1" drill="0.7"/>
<pad name="5" x="-1" y="-1" drill="0.7"/>
<pad name="6" x="-1" y="1" drill="0.7"/>
<pad name="7" x="-3" y="-1" drill="0.7"/>
<pad name="8" x="-3" y="1" drill="0.7"/>
<wire x1="-5.5" y1="3.5" x2="5.5" y2="3.5" width="0.127" layer="21"/>
<wire x1="5.5" y1="3.5" x2="5.5" y2="-3.5" width="0.127" layer="21"/>
<wire x1="5.5" y1="-3.5" x2="-5.5" y2="-3.5" width="0.127" layer="21"/>
<wire x1="-5.5" y1="-3.5" x2="-5.5" y2="3.5" width="0.127" layer="21"/>
<wire x1="-5.5" y1="3.5" x2="5.5" y2="3.5" width="0.127" layer="51"/>
<wire x1="5.5" y1="3.5" x2="5.5" y2="-3.5" width="0.127" layer="51"/>
<wire x1="5.5" y1="-3.5" x2="-5.5" y2="-3.5" width="0.127" layer="51"/>
<wire x1="-5.5" y1="-3.5" x2="-5.5" y2="3.5" width="0.127" layer="51"/>
<wire x1="4" y1="-4" x2="6" y2="-4" width="0.127" layer="51"/>
<wire x1="6" y1="-4" x2="6" y2="-2" width="0.127" layer="51"/>
<wire x1="4" y1="-4" x2="6" y2="-4" width="0.127" layer="21"/>
<wire x1="6" y1="-4" x2="6" y2="-2" width="0.127" layer="21"/>
<text x="6.35" y="2.54" size="0.8128" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="6.35" y="1.27" size="0.8128" layer="27" font="vector" ratio="15">&gt;VALUE</text>
</package>
<package name="JST_PHDR-08VS_FEMALE">
<text x="9" y="6" size="0.8128" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="9" y="5" size="0.8128" layer="27" font="vector" ratio="15">&gt;VALUE</text>
<circle x="0" y="-1.25" radius="10" width="0.1524" layer="21"/>
<wire x1="-3.95" y1="-10.47" x2="-3.95" y2="-13.97" width="0.1524" layer="21"/>
<wire x1="-3.95" y1="-13.97" x2="4" y2="-13.97" width="0.1524" layer="21"/>
<wire x1="4" y1="-13.97" x2="4" y2="-10.48" width="0.1524" layer="21"/>
<pad name="P$1" x="-3" y="-1" drill="0.7"/>
<pad name="P$2" x="-3" y="1" drill="0.7"/>
<pad name="P$3" x="-1" y="-1" drill="0.7"/>
<pad name="P$4" x="-1" y="1" drill="0.7"/>
<pad name="P$5" x="1" y="-1" drill="0.7"/>
<pad name="P$6" x="1" y="1" drill="0.7"/>
<pad name="P$7" x="3" y="-1" drill="0.7"/>
<pad name="P$8" x="3" y="1" drill="0.7"/>
</package>
</packages>
<symbols>
<symbol name="PIN">
<pin name="1" x="-5.08" y="0" visible="off"/>
<wire x1="-3.302" y1="0" x2="-1.27" y2="0" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="0" x2="2.54" y2="0" width="0.8128" layer="94"/>
<text x="-3.302" y="0.508" size="1.27" layer="95" ratio="15">&gt;NAME</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="JST_B8B-PHDSS" prefix="J" uservalue="yes">
<gates>
<gate name="-1" symbol="PIN" x="5.08" y="7.62"/>
<gate name="-2" symbol="PIN" x="5.08" y="5.08"/>
<gate name="-3" symbol="PIN" x="5.08" y="2.54"/>
<gate name="-4" symbol="PIN" x="5.08" y="0"/>
<gate name="-5" symbol="PIN" x="5.08" y="-2.54"/>
<gate name="-6" symbol="PIN" x="5.08" y="-5.08"/>
<gate name="-7" symbol="PIN" x="5.08" y="-7.62"/>
<gate name="-8" symbol="PIN" x="5.08" y="-10.16"/>
</gates>
<devices>
<device name="" package="JST_B8B-PHDSS">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-2" pin="1" pad="2"/>
<connect gate="-3" pin="1" pad="3"/>
<connect gate="-4" pin="1" pad="4"/>
<connect gate="-5" pin="1" pad="5"/>
<connect gate="-6" pin="1" pad="6"/>
<connect gate="-7" pin="1" pad="7"/>
<connect gate="-8" pin="1" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST_PHDR-08VS_FEMALE" package="JST_PHDR-08VS_FEMALE">
<connects>
<connect gate="-1" pin="1" pad="P$1"/>
<connect gate="-2" pin="1" pad="P$2"/>
<connect gate="-3" pin="1" pad="P$3"/>
<connect gate="-4" pin="1" pad="P$4"/>
<connect gate="-5" pin="1" pad="P$5"/>
<connect gate="-6" pin="1" pad="P$6"/>
<connect gate="-7" pin="1" pad="P$7"/>
<connect gate="-8" pin="1" pad="P$8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="U$2" library="frames" deviceset="A3L-LOC_JMA" device=""/>
<part name="J1" library="JMA_LBR" deviceset="JST_B8B-PHDSS" device="JST_PHDR-08VS_FEMALE"/>
<part name="J2" library="JMA_LBR" deviceset="JST_B8B-PHDSS" device="JST_PHDR-08VS_FEMALE"/>
<part name="J3" library="JMA_LBR" deviceset="JST_B8B-PHDSS" device="JST_PHDR-08VS_FEMALE"/>
<part name="J4" library="JMA_LBR" deviceset="JST_B8B-PHDSS" device="JST_PHDR-08VS_FEMALE"/>
<part name="J5" library="JMA_LBR" deviceset="JST_B8B-PHDSS" device="JST_PHDR-08VS_FEMALE"/>
<part name="J6" library="JMA_LBR" deviceset="JST_B8B-PHDSS" device="JST_PHDR-08VS_FEMALE"/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="U$2" gate="G$1" x="0" y="0" smashed="yes">
<attribute name="DRAWING_NAME" x="344.17" y="15.24" size="2.54" layer="94"/>
<attribute name="LAST_DATE_TIME" x="344.17" y="10.16" size="2.286" layer="94"/>
<attribute name="SHEET" x="357.505" y="5.08" size="2.54" layer="94"/>
</instance>
<instance part="J1" gate="-1" x="152.4" y="172.72" smashed="yes">
<attribute name="NAME" x="149.098" y="173.228" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J1" gate="-2" x="152.4" y="170.18" smashed="yes">
<attribute name="NAME" x="149.098" y="170.688" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J1" gate="-3" x="152.4" y="167.64" smashed="yes">
<attribute name="NAME" x="149.098" y="168.148" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J1" gate="-4" x="152.4" y="165.1" smashed="yes">
<attribute name="NAME" x="149.098" y="165.608" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J1" gate="-5" x="152.4" y="162.56" smashed="yes">
<attribute name="NAME" x="149.098" y="163.068" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J1" gate="-6" x="152.4" y="160.02" smashed="yes">
<attribute name="NAME" x="149.098" y="160.528" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J1" gate="-7" x="152.4" y="157.48" smashed="yes">
<attribute name="NAME" x="149.098" y="157.988" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J1" gate="-8" x="152.4" y="154.94" smashed="yes">
<attribute name="NAME" x="149.098" y="155.448" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J2" gate="-1" x="195.58" y="172.72" smashed="yes">
<attribute name="NAME" x="192.278" y="173.228" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J2" gate="-2" x="195.58" y="170.18" smashed="yes">
<attribute name="NAME" x="192.278" y="170.688" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J2" gate="-3" x="195.58" y="167.64" smashed="yes">
<attribute name="NAME" x="192.278" y="168.148" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J2" gate="-4" x="195.58" y="165.1" smashed="yes">
<attribute name="NAME" x="192.278" y="165.608" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J2" gate="-5" x="195.58" y="162.56" smashed="yes">
<attribute name="NAME" x="192.278" y="163.068" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J2" gate="-6" x="195.58" y="160.02" smashed="yes">
<attribute name="NAME" x="192.278" y="160.528" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J2" gate="-7" x="195.58" y="157.48" smashed="yes">
<attribute name="NAME" x="192.278" y="157.988" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J2" gate="-8" x="195.58" y="154.94" smashed="yes">
<attribute name="NAME" x="192.278" y="155.448" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J3" gate="-1" x="238.76" y="172.72" smashed="yes">
<attribute name="NAME" x="235.458" y="173.228" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J3" gate="-2" x="238.76" y="170.18" smashed="yes">
<attribute name="NAME" x="235.458" y="170.688" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J3" gate="-3" x="238.76" y="167.64" smashed="yes">
<attribute name="NAME" x="235.458" y="168.148" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J3" gate="-4" x="238.76" y="165.1" smashed="yes">
<attribute name="NAME" x="235.458" y="165.608" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J3" gate="-5" x="238.76" y="162.56" smashed="yes">
<attribute name="NAME" x="235.458" y="163.068" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J3" gate="-6" x="238.76" y="160.02" smashed="yes">
<attribute name="NAME" x="235.458" y="160.528" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J3" gate="-7" x="238.76" y="157.48" smashed="yes">
<attribute name="NAME" x="235.458" y="157.988" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J3" gate="-8" x="238.76" y="154.94" smashed="yes">
<attribute name="NAME" x="235.458" y="155.448" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J4" gate="-1" x="154.94" y="124.46" smashed="yes">
<attribute name="NAME" x="151.638" y="124.968" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J4" gate="-2" x="154.94" y="121.92" smashed="yes">
<attribute name="NAME" x="151.638" y="122.428" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J4" gate="-3" x="154.94" y="119.38" smashed="yes">
<attribute name="NAME" x="151.638" y="119.888" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J4" gate="-4" x="154.94" y="116.84" smashed="yes">
<attribute name="NAME" x="151.638" y="117.348" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J4" gate="-5" x="154.94" y="114.3" smashed="yes">
<attribute name="NAME" x="151.638" y="114.808" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J4" gate="-6" x="154.94" y="111.76" smashed="yes">
<attribute name="NAME" x="151.638" y="112.268" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J4" gate="-7" x="154.94" y="109.22" smashed="yes">
<attribute name="NAME" x="151.638" y="109.728" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J4" gate="-8" x="154.94" y="106.68" smashed="yes">
<attribute name="NAME" x="151.638" y="107.188" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J5" gate="-1" x="198.12" y="124.46" smashed="yes">
<attribute name="NAME" x="194.818" y="124.968" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J5" gate="-2" x="198.12" y="121.92" smashed="yes">
<attribute name="NAME" x="194.818" y="122.428" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J5" gate="-3" x="198.12" y="119.38" smashed="yes">
<attribute name="NAME" x="194.818" y="119.888" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J5" gate="-4" x="198.12" y="116.84" smashed="yes">
<attribute name="NAME" x="194.818" y="117.348" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J5" gate="-5" x="198.12" y="114.3" smashed="yes">
<attribute name="NAME" x="194.818" y="114.808" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J5" gate="-6" x="198.12" y="111.76" smashed="yes">
<attribute name="NAME" x="194.818" y="112.268" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J5" gate="-7" x="198.12" y="109.22" smashed="yes">
<attribute name="NAME" x="194.818" y="109.728" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J5" gate="-8" x="198.12" y="106.68" smashed="yes">
<attribute name="NAME" x="194.818" y="107.188" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J6" gate="-1" x="241.3" y="124.46" smashed="yes">
<attribute name="NAME" x="237.998" y="124.968" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J6" gate="-2" x="241.3" y="121.92" smashed="yes">
<attribute name="NAME" x="237.998" y="122.428" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J6" gate="-3" x="241.3" y="119.38" smashed="yes">
<attribute name="NAME" x="237.998" y="119.888" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J6" gate="-4" x="241.3" y="116.84" smashed="yes">
<attribute name="NAME" x="237.998" y="117.348" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J6" gate="-5" x="241.3" y="114.3" smashed="yes">
<attribute name="NAME" x="237.998" y="114.808" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J6" gate="-6" x="241.3" y="111.76" smashed="yes">
<attribute name="NAME" x="237.998" y="112.268" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J6" gate="-7" x="241.3" y="109.22" smashed="yes">
<attribute name="NAME" x="237.998" y="109.728" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J6" gate="-8" x="241.3" y="106.68" smashed="yes">
<attribute name="NAME" x="237.998" y="107.188" size="1.27" layer="95" ratio="15"/>
</instance>
</instances>
<busses>
</busses>
<nets>
</nets>
</sheet>
</sheets>
<errors>
<approved hash="101,1,355.6,83.82,J2-1,1,,,,"/>
<approved hash="101,1,355.6,43.18,J2-PAD,1,,,,"/>
<approved hash="101,1,355.6,68.58,J2-4,1,,,,"/>
<approved hash="101,1,355.6,78.74,J2-2,1,,,,"/>
<approved hash="101,1,355.6,48.26,J2-8,1,,,,"/>
<approved hash="209,1,330.2,83.82,N$1,,,,,"/>
<approved hash="209,1,116.84,152.4,N$5,,,,,"/>
<approved hash="209,1,116.84,142.24,N$6,,,,,"/>
<approved hash="209,1,116.84,121.92,N$7,,,,,"/>
<approved hash="209,1,330.2,78.74,N$16,,,,,"/>
<approved hash="209,1,330.2,68.58,N$20,,,,,"/>
<approved hash="209,1,330.2,48.26,N$21,,,,,"/>
<approved hash="209,1,116.84,157.48,N$24,,,,,"/>
<approved hash="209,1,116.84,116.84,N$29,,,,,"/>
<approved hash="209,1,330.2,43.18,N$38,,,,,"/>
</errors>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
</compatibility>
</eagle>
