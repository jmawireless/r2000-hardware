<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.5.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="yes" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="5" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="no" active="no"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="no" active="no"/>
<layer number="103" name="tMap" color="7" fill="1" visible="no" active="no"/>
<layer number="104" name="Name" color="7" fill="1" visible="no" active="no"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="no" active="no"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="no" active="no"/>
<layer number="107" name="Crop" color="7" fill="1" visible="no" active="no"/>
<layer number="108" name="fp8" color="7" fill="1" visible="yes" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="yes" active="yes"/>
<layer number="110" name="110" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="111" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="7" fill="1" visible="yes" active="yes"/>
<layer number="114" name="FRNTMAAT1" color="7" fill="1" visible="no" active="no"/>
<layer number="115" name="FRNTMAAT2" color="7" fill="1" visible="no" active="no"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="no" active="no"/>
<layer number="117" name="BACKMAAT1" color="7" fill="1" visible="no" active="no"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="yes" active="yes"/>
<layer number="119" name="KAP_TEKEN" color="7" fill="1" visible="no" active="no"/>
<layer number="120" name="KAP_MAAT1" color="7" fill="1" visible="no" active="no"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="no" active="no"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="no" active="no"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="top_silk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="130" name="SMDSTROOK" color="7" fill="1" visible="no" active="no"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="133" name="bottom_silk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="DrillLegend" color="7" fill="1" visible="no" active="no"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="14" fill="1" visible="no" active="no"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="no"/>
<layer number="201" name="201bmp" color="2" fill="1" visible="no" active="no"/>
<layer number="202" name="202bmp" color="3" fill="1" visible="no" active="no"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="no" active="no"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="no" active="no"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="no" active="no"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="no" active="no"/>
<layer number="207" name="207bmp" color="15" fill="10" visible="no" active="no"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="no" active="no"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="Eagle3D_PG1" color="7" fill="1" visible="no" active="no"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="no"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="OrgLBR" color="13" fill="1" visible="no" active="no"/>
<layer number="255" name="Accent" color="7" fill="1" visible="no" active="no"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="supply2" urn="urn:adsk.eagle:library:372">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
Please keep in mind, that these devices are necessary for the
automatic wiring of the supply signals.&lt;p&gt;
The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND" urn="urn:adsk.eagle:symbol:26990/1" library_version="2">
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="-1.27" y2="0" width="0.254" layer="94"/>
<text x="-1.905" y="-3.175" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" urn="urn:adsk.eagle:component:27037/1" prefix="SUPPLY" library_version="2">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="GND" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="frames" urn="urn:adsk.eagle:library:229">
<description>&lt;b&gt;Frames for Sheet and Layout&lt;/b&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="A3_LOC_JMA_RELEASE">
<wire x1="0" y1="241.3" x2="63.5" y2="241.3" width="0.025" layer="94"/>
<wire x1="63.5" y1="241.3" x2="127" y2="241.3" width="0.025" layer="94"/>
<wire x1="127" y1="241.3" x2="190.5" y2="241.3" width="0.025" layer="94"/>
<wire x1="190.5" y1="241.3" x2="254" y2="241.3" width="0.025" layer="94"/>
<wire x1="254" y1="241.3" x2="317.5" y2="241.3" width="0.025" layer="94"/>
<wire x1="317.5" y1="241.3" x2="380.97883125" y2="241.3" width="0.025" layer="94"/>
<wire x1="380.97883125" y1="241.3" x2="380.97883125" y2="0.02116875" width="0.025" layer="94"/>
<wire x1="380.97883125" y1="0.02116875" x2="0" y2="0.02116875" width="0.025" layer="94"/>
<wire x1="0" y1="0.02116875" x2="0" y2="60.325" width="0.025" layer="94"/>
<wire x1="0" y1="60.325" x2="0" y2="120.65" width="0.025" layer="94"/>
<wire x1="0" y1="120.65" x2="0" y2="180.975" width="0.025" layer="94"/>
<wire x1="0" y1="180.975" x2="0" y2="241.3" width="0.025" layer="94"/>
<wire x1="5.079890625" y1="236.22041875" x2="375.898909375" y2="236.22041875" width="0.025" layer="94"/>
<wire x1="375.898909375" y1="236.22041875" x2="375.898909375" y2="5.10075" width="0.025" layer="94"/>
<wire x1="375.898909375" y1="5.10075" x2="5.079890625" y2="5.10075" width="0.025" layer="94"/>
<wire x1="5.079890625" y1="5.10075" x2="5.079890625" y2="236.22041875" width="0.025" layer="94"/>
<wire x1="317.5" y1="5.08" x2="317.5" y2="0" width="0.025" layer="94"/>
<wire x1="317.5" y1="236.22" x2="317.5" y2="241.3" width="0.025" layer="94"/>
<wire x1="254" y1="5.08" x2="254" y2="0" width="0.025" layer="94"/>
<wire x1="254" y1="236.22" x2="254" y2="241.3" width="0.025" layer="94"/>
<wire x1="190.5" y1="5.08" x2="190.5" y2="0" width="0.025" layer="94"/>
<wire x1="190.5" y1="236.22" x2="190.5" y2="241.3" width="0.025" layer="94"/>
<wire x1="127" y1="5.08" x2="127" y2="0" width="0.025" layer="94"/>
<wire x1="127" y1="236.22" x2="127" y2="241.3" width="0.025" layer="94"/>
<wire x1="63.5" y1="5.08" x2="63.5" y2="0" width="0.025" layer="94"/>
<wire x1="63.5" y1="236.22" x2="63.5" y2="241.3" width="0.025" layer="94"/>
<wire x1="5.08" y1="180.975" x2="0" y2="180.975" width="0.025" layer="94"/>
<wire x1="375.92" y1="180.975" x2="381" y2="180.975" width="0.025" layer="94"/>
<wire x1="5.08" y1="120.65" x2="0" y2="120.65" width="0.025" layer="94"/>
<wire x1="375.92" y1="120.65" x2="381" y2="120.65" width="0.025" layer="94"/>
<wire x1="5.08" y1="60.325" x2="0" y2="60.325" width="0.025" layer="94"/>
<wire x1="375.92" y1="60.325" x2="381" y2="60.325" width="0.025" layer="94"/>
<wire x1="271.78" y1="5.08" x2="271.78" y2="21.59" width="0.025" layer="94"/>
<wire x1="271.78" y1="21.59" x2="271.78" y2="27.94" width="0.025" layer="94"/>
<wire x1="271.78" y1="27.94" x2="271.78" y2="34.29" width="0.025" layer="94"/>
<wire x1="271.78" y1="34.29" x2="271.78" y2="40.64" width="0.025" layer="94"/>
<wire x1="271.78" y1="40.64" x2="271.78" y2="45.72" width="0.025" layer="94"/>
<wire x1="271.78" y1="45.72" x2="285.75" y2="45.72" width="0.025" layer="94"/>
<wire x1="285.75" y1="45.72" x2="293.37" y2="45.72" width="0.025" layer="94"/>
<wire x1="293.37" y1="45.72" x2="304.8" y2="45.72" width="0.025" layer="94"/>
<wire x1="304.8" y1="45.72" x2="375.92" y2="45.72" width="0.025" layer="94"/>
<wire x1="304.8" y1="5.08" x2="304.8" y2="8.89" width="0.025" layer="94"/>
<wire x1="304.8" y1="8.89" x2="304.8" y2="15.24" width="0.025" layer="94"/>
<wire x1="304.8" y1="15.24" x2="304.8" y2="21.59" width="0.025" layer="94"/>
<wire x1="304.8" y1="21.59" x2="304.8" y2="27.94" width="0.025" layer="94"/>
<wire x1="304.8" y1="27.94" x2="304.8" y2="34.29" width="0.025" layer="94"/>
<wire x1="304.8" y1="34.29" x2="304.8" y2="40.64" width="0.025" layer="94"/>
<wire x1="304.8" y1="40.64" x2="304.8" y2="45.72" width="0.025" layer="94"/>
<wire x1="271.78" y1="40.64" x2="304.8" y2="40.64" width="0.025" layer="94"/>
<wire x1="271.78" y1="34.29" x2="304.8" y2="34.29" width="0.025" layer="94"/>
<wire x1="271.78" y1="27.94" x2="304.8" y2="27.94" width="0.025" layer="94"/>
<wire x1="271.78" y1="21.59" x2="285.75" y2="21.59" width="0.025" layer="94"/>
<wire x1="285.75" y1="21.59" x2="293.37" y2="21.59" width="0.025" layer="94"/>
<wire x1="293.37" y1="21.59" x2="304.8" y2="21.59" width="0.025" layer="94"/>
<wire x1="304.8" y1="8.89" x2="322.58" y2="8.89" width="0.025" layer="94"/>
<wire x1="322.58" y1="8.89" x2="359.41" y2="8.89" width="0.025" layer="94"/>
<wire x1="359.41" y1="8.89" x2="369.57" y2="8.89" width="0.025" layer="94"/>
<wire x1="369.57" y1="8.89" x2="375.92" y2="8.89" width="0.025" layer="94"/>
<wire x1="322.58" y1="8.89" x2="322.58" y2="5.08" width="0.025" layer="94"/>
<wire x1="359.41" y1="8.89" x2="359.41" y2="5.08" width="0.025" layer="94"/>
<wire x1="304.8" y1="15.24" x2="369.57" y2="15.24" width="0.025" layer="94"/>
<wire x1="369.57" y1="15.24" x2="375.92" y2="15.24" width="0.025" layer="94"/>
<wire x1="369.57" y1="15.24" x2="369.57" y2="8.89" width="0.025" layer="94"/>
<wire x1="304.8" y1="21.59" x2="375.92" y2="21.59" width="0.025" layer="94"/>
<wire x1="285.75" y1="45.72" x2="285.75" y2="21.59" width="0.025" layer="94"/>
<wire x1="293.37" y1="45.72" x2="293.37" y2="21.59" width="0.025" layer="94"/>
<wire x1="307.34" y1="31.75" x2="373.38" y2="31.75" width="0.025" layer="94"/>
<wire x1="329.29703125" y1="44.90211875" x2="329.35418125" y2="44.890690625" width="0.025" layer="94"/>
<wire x1="329.35418125" y1="44.890690625" x2="329.3999" y2="44.879259375" width="0.025" layer="94"/>
<wire x1="329.81138125" y1="44.776390625" x2="329.84566875" y2="44.764959375" width="0.025" layer="94"/>
<wire x1="329.84566875" y1="44.764959375" x2="329.891390625" y2="44.75353125" width="0.025" layer="94"/>
<wire x1="331.137259375" y1="44.1706" x2="331.365859375" y2="43.99915" width="0.025" layer="94"/>
<wire x1="331.365859375" y1="43.99915" x2="331.451590625" y2="43.91341875" width="0.025" layer="94"/>
<wire x1="331.92593125" y1="43.164759375" x2="331.937359375" y2="43.09618125" width="0.025" layer="94"/>
<wire x1="331.937359375" y1="43.09618125" x2="331.948790625" y2="42.993309375" width="0.025" layer="94"/>
<wire x1="331.891640625" y1="42.5704" x2="331.90306875" y2="42.604690625" width="0.025" layer="94"/>
<wire x1="331.84591875" y1="42.4561" x2="331.891640625" y2="42.5704" width="0.025" layer="94"/>
<wire x1="329.262740625" y1="40.947340625" x2="329.319890625" y2="40.95876875" width="0.025" layer="94"/>
<wire x1="329.194159375" y1="40.935909375" x2="329.262740625" y2="40.947340625" width="0.025" layer="94"/>
<wire x1="329.194159375" y1="40.935909375" x2="331.44586875" y2="32.512" width="0.025" layer="94"/>
<wire x1="323.764909375" y1="32.512" x2="324.9625" y2="37.03828125" width="0.025" layer="94"/>
<wire x1="324.9625" y1="37.03828125" x2="325.993759375" y2="40.935909375" width="0.025" layer="94"/>
<wire x1="325.719440625" y1="40.993059375" x2="325.78801875" y2="40.98163125" width="0.025" layer="94"/>
<wire x1="325.63943125" y1="41.01591875" x2="325.719440625" y2="40.993059375" width="0.025" layer="94"/>
<wire x1="325.45655" y1="41.061640625" x2="325.536559375" y2="41.03878125" width="0.025" layer="94"/>
<wire x1="325.3994" y1="41.07306875" x2="325.45655" y2="41.061640625" width="0.025" layer="94"/>
<wire x1="325.136509375" y1="41.15308125" x2="325.21651875" y2="41.13021875" width="0.025" layer="94"/>
<wire x1="325.06793125" y1="41.175940625" x2="325.136509375" y2="41.15308125" width="0.025" layer="94"/>
<wire x1="324.976490625" y1="41.21023125" x2="325.06793125" y2="41.175940625" width="0.025" layer="94"/>
<wire x1="324.97076875" y1="41.21251875" x2="324.976490625" y2="41.21023125" width="0.025" layer="94"/>
<wire x1="324.64501875" y1="41.347390625" x2="324.747890625" y2="41.30166875" width="0.025" layer="94"/>
<wire x1="324.565009375" y1="41.38168125" x2="324.64501875" y2="41.347390625" width="0.025" layer="94"/>
<wire x1="324.450709375" y1="41.43883125" x2="324.45185" y2="41.438259375" width="0.025" layer="94"/>
<wire x1="324.393559375" y1="41.47311875" x2="324.450709375" y2="41.43883125" width="0.025" layer="94"/>
<wire x1="324.26783125" y1="41.5417" x2="324.347840625" y2="41.49598125" width="0.025" layer="94"/>
<wire x1="324.233540625" y1="41.564559375" x2="324.26783125" y2="41.5417" width="0.025" layer="94"/>
<wire x1="324.18781875" y1="41.58741875" x2="324.233540625" y2="41.564559375" width="0.025" layer="94"/>
<wire x1="324.1421" y1="41.621709375" x2="324.18781875" y2="41.58741875" width="0.025" layer="94"/>
<wire x1="324.08495" y1="41.656" x2="324.1421" y2="41.621709375" width="0.025" layer="94"/>
<wire x1="323.822059375" y1="41.861740625" x2="323.85635" y2="41.82745" width="0.025" layer="94"/>
<wire x1="323.776340625" y1="41.89603125" x2="323.822059375" y2="41.861740625" width="0.025" layer="94"/>
<wire x1="323.62775" y1="42.05605" x2="323.662040625" y2="42.01033125" width="0.025" layer="94"/>
<wire x1="323.593459375" y1="42.090340625" x2="323.62775" y2="42.05605" width="0.025" layer="94"/>
<wire x1="323.35343125" y1="42.46753125" x2="323.354" y2="42.466390625" width="0.025" layer="94"/>
<wire x1="323.33056875" y1="42.52468125" x2="323.35343125" y2="42.46753125" width="0.025" layer="94"/>
<wire x1="323.536309375" y1="43.690540625" x2="323.58203125" y2="43.736259375" width="0.025" layer="94"/>
<wire x1="323.58203125" y1="43.736259375" x2="323.62775" y2="43.793409375" width="0.025" layer="94"/>
<wire x1="323.78776875" y1="43.95343125" x2="323.84491875" y2="43.99915" width="0.025" layer="94"/>
<wire x1="323.84491875" y1="43.99915" x2="323.87806875" y2="44.0323" width="0.025" layer="94"/>
<wire x1="323.87806875" y1="44.0323" x2="323.879209375" y2="44.033440625" width="0.025" layer="94"/>
<wire x1="323.879209375" y1="44.033440625" x2="323.936359375" y2="44.079159375" width="0.025" layer="94"/>
<wire x1="323.936359375" y1="44.079159375" x2="324.0278" y2="44.147740625" width="0.025" layer="94"/>
<wire x1="324.164959375" y1="44.23918125" x2="324.222109375" y2="44.27346875" width="0.025" layer="94"/>
<wire x1="324.222109375" y1="44.27346875" x2="324.290690625" y2="44.319190625" width="0.025" layer="94"/>
<wire x1="324.290690625" y1="44.319190625" x2="324.336409375" y2="44.34205" width="0.025" layer="94"/>
<wire x1="324.336409375" y1="44.34205" x2="324.393559375" y2="44.376340625" width="0.025" layer="94"/>
<wire x1="324.62101875" y1="44.49006875" x2="324.622159375" y2="44.490640625" width="0.025" layer="94"/>
<wire x1="324.622159375" y1="44.490640625" x2="324.679309375" y2="44.5135" width="0.025" layer="94"/>
<wire x1="324.679309375" y1="44.5135" x2="324.72503125" y2="44.536359375" width="0.025" layer="94"/>
<wire x1="324.72503125" y1="44.536359375" x2="325.01078125" y2="44.650659375" width="0.025" layer="94"/>
<wire x1="325.01078125" y1="44.650659375" x2="325.35368125" y2="44.764959375" width="0.025" layer="94"/>
<wire x1="325.35368125" y1="44.764959375" x2="325.433690625" y2="44.78781875" width="0.025" layer="94"/>
<wire x1="325.433690625" y1="44.78781875" x2="325.479409375" y2="44.79925" width="0.025" layer="94"/>
<wire x1="325.479409375" y1="44.79925" x2="325.5137" y2="44.81068125" width="0.025" layer="94"/>
<wire x1="325.5137" y1="44.81068125" x2="325.7423" y2="44.86783125" width="0.025" layer="94"/>
<wire x1="325.7423" y1="44.86783125" x2="325.79945" y2="44.879259375" width="0.025" layer="94"/>
<wire x1="325.79945" y1="44.879259375" x2="325.84516875" y2="44.890690625" width="0.025" layer="94"/>
<wire x1="326.393809375" y1="44.69638125" x2="326.450959375" y2="44.707809375" width="0.025" layer="94"/>
<wire x1="326.32523125" y1="44.68495" x2="326.393809375" y2="44.69638125" width="0.025" layer="94"/>
<wire x1="325.936609375" y1="44.604940625" x2="325.993759375" y2="44.61636875" width="0.025" layer="94"/>
<wire x1="325.84516875" y1="44.58208125" x2="325.936609375" y2="44.604940625" width="0.025" layer="94"/>
<wire x1="325.765159375" y1="44.55921875" x2="325.84516875" y2="44.58208125" width="0.025" layer="94"/>
<wire x1="325.67371875" y1="44.536359375" x2="325.765159375" y2="44.55921875" width="0.025" layer="94"/>
<wire x1="325.5137" y1="44.490640625" x2="325.520559375" y2="44.4926" width="0.025" layer="94"/>
<wire x1="325.23938125" y1="44.3992" x2="325.5137" y2="44.490640625" width="0.025" layer="94"/>
<wire x1="324.98791875" y1="44.29633125" x2="325.06793125" y2="44.33061875" width="0.025" layer="94"/>
<wire x1="324.93076875" y1="44.27346875" x2="324.98791875" y2="44.29633125" width="0.025" layer="94"/>
<wire x1="324.61073125" y1="44.10201875" x2="324.679309375" y2="44.147740625" width="0.025" layer="94"/>
<wire x1="324.53071875" y1="44.0563" x2="324.531859375" y2="44.05695" width="0.025" layer="94"/>
<wire x1="324.531859375" y1="44.05695" x2="324.61073125" y2="44.10201875" width="0.025" layer="94"/>
<wire x1="324.462140625" y1="44.01058125" x2="324.53071875" y2="44.0563" width="0.025" layer="94"/>
<wire x1="324.233540625" y1="43.8277" x2="324.279259375" y2="43.87341875" width="0.025" layer="94"/>
<wire x1="324.164959375" y1="43.77055" x2="324.233540625" y2="43.8277" width="0.025" layer="94"/>
<wire x1="323.73061875" y1="42.51325" x2="323.74205" y2="42.478959375" width="0.025" layer="94"/>
<wire x1="323.74205" y1="42.478959375" x2="323.75348125" y2="42.433240625" width="0.025" layer="94"/>
<wire x1="323.75348125" y1="42.433240625" x2="323.776340625" y2="42.38751875" width="0.025" layer="94"/>
<wire x1="323.776340625" y1="42.38751875" x2="323.7992" y2="42.33036875" width="0.025" layer="94"/>
<wire x1="324.08495" y1="41.95318125" x2="324.1421" y2="41.907459375" width="0.025" layer="94"/>
<wire x1="324.1421" y1="41.907459375" x2="324.176390625" y2="41.87316875" width="0.025" layer="94"/>
<wire x1="324.41641875" y1="41.690290625" x2="324.462140625" y2="41.656" width="0.025" layer="94"/>
<wire x1="324.462140625" y1="41.656" x2="324.53071875" y2="41.61028125" width="0.025" layer="94"/>
<wire x1="324.64501875" y1="41.5417" x2="324.690740625" y2="41.518840625" width="0.025" layer="94"/>
<wire x1="324.690740625" y1="41.518840625" x2="324.72503125" y2="41.49598125" width="0.025" layer="94"/>
<wire x1="324.72503125" y1="41.49598125" x2="324.78218125" y2="41.461690625" width="0.025" layer="94"/>
<wire x1="324.919340625" y1="41.393109375" x2="324.976490625" y2="41.37025" width="0.025" layer="94"/>
<wire x1="324.976490625" y1="41.37025" x2="325.022209375" y2="41.347390625" width="0.025" layer="94"/>
<wire x1="325.46798125" y1="41.18736875" x2="325.536559375" y2="41.164509375" width="0.025" layer="94"/>
<wire x1="325.536559375" y1="41.164509375" x2="325.58228125" y2="41.15308125" width="0.025" layer="94"/>
<wire x1="325.58228125" y1="41.15308125" x2="325.61656875" y2="41.14165" width="0.025" layer="94"/>
<wire x1="325.61656875" y1="41.14165" x2="325.708009375" y2="41.118790625" width="0.025" layer="94"/>
<wire x1="325.708009375" y1="41.118790625" x2="325.7423" y2="41.107359375" width="0.025" layer="94"/>
<wire x1="325.7423" y1="41.107359375" x2="326.01661875" y2="41.03878125" width="0.025" layer="94"/>
<wire x1="325.45655" y1="41.38168125" x2="325.5137" y2="41.35881875" width="0.025" layer="94"/>
<wire x1="325.433690625" y1="41.393109375" x2="325.45655" y2="41.38168125" width="0.025" layer="94"/>
<wire x1="325.15936875" y1="41.518840625" x2="325.319390625" y2="41.43883125" width="0.025" layer="94"/>
<wire x1="325.10221875" y1="41.55313125" x2="325.15936875" y2="41.518840625" width="0.025" layer="94"/>
<wire x1="324.70216875" y1="43.4848" x2="324.747890625" y2="43.519090625" width="0.025" layer="94"/>
<wire x1="324.747890625" y1="43.519090625" x2="324.77075" y2="43.54195" width="0.025" layer="94"/>
<wire x1="324.98678125" y1="43.68978125" x2="324.98791875" y2="43.690540625" width="0.025" layer="94"/>
<wire x1="324.98791875" y1="43.690540625" x2="325.10221875" y2="43.75911875" width="0.025" layer="94"/>
<wire x1="325.10221875" y1="43.75911875" x2="325.177659375" y2="43.796840625" width="0.025" layer="94"/>
<wire x1="325.45655" y1="43.93056875" x2="325.68515" y2="44.022009375" width="0.025" layer="94"/>
<wire x1="325.82116875" y1="44.06735" x2="325.822309375" y2="44.06773125" width="0.025" layer="94"/>
<wire x1="325.822309375" y1="44.06773125" x2="326.062340625" y2="44.136309375" width="0.025" layer="94"/>
<wire x1="326.062340625" y1="44.136309375" x2="326.15378125" y2="44.15916875" width="0.025" layer="94"/>
<wire x1="326.15378125" y1="44.15916875" x2="326.35951875" y2="44.204890625" width="0.025" layer="94"/>
<wire x1="326.279509375" y1="43.919140625" x2="326.462390625" y2="43.964859375" width="0.025" layer="94"/>
<wire x1="326.119490625" y1="43.87341875" x2="326.279509375" y2="43.919140625" width="0.025" layer="94"/>
<wire x1="326.02805" y1="43.850559375" x2="326.119490625" y2="43.87341875" width="0.025" layer="94"/>
<wire x1="325.68515" y1="43.72483125" x2="325.8566" y2="43.793409375" width="0.025" layer="94"/>
<wire x1="325.605140625" y1="43.690540625" x2="325.68515" y2="43.72483125" width="0.025" layer="94"/>
<wire x1="325.604" y1="43.68996875" x2="325.605140625" y2="43.690540625" width="0.025" layer="94"/>
<wire x1="325.34225" y1="43.54195" x2="325.45655" y2="43.61053125" width="0.025" layer="94"/>
<wire x1="325.341109375" y1="43.541190625" x2="325.34225" y2="43.54195" width="0.025" layer="94"/>
<wire x1="324.862190625" y1="43.15333125" x2="325.033640625" y2="43.32478125" width="0.025" layer="94"/>
<wire x1="324.8279" y1="43.107609375" x2="324.862190625" y2="43.15333125" width="0.025" layer="94"/>
<wire x1="324.75931875" y1="43.004740625" x2="324.8279" y2="43.107609375" width="0.025" layer="94"/>
<wire x1="324.633590625" y1="42.62755" x2="324.65645" y2="42.74185" width="0.025" layer="94"/>
<wire x1="324.633590625" y1="42.626409375" x2="324.633590625" y2="42.62755" width="0.025" layer="94"/>
<wire x1="324.690740625" y1="42.261790625" x2="324.70216875" y2="42.23893125" width="0.025" layer="94"/>
<wire x1="324.70216875" y1="42.23893125" x2="324.7136" y2="42.204640625" width="0.025" layer="94"/>
<wire x1="324.7136" y1="42.204640625" x2="324.72503125" y2="42.18178125" width="0.025" layer="94"/>
<wire x1="324.72503125" y1="42.18178125" x2="324.747890625" y2="42.147490625" width="0.025" layer="94"/>
<wire x1="324.793609375" y1="42.06748125" x2="324.87361875" y2="41.964609375" width="0.025" layer="94"/>
<wire x1="324.87361875" y1="41.964609375" x2="324.874759375" y2="41.96346875" width="0.025" layer="94"/>
<wire x1="325.84516875" y1="41.35881875" x2="325.969759375" y2="41.317290625" width="0.025" layer="94"/>
<wire x1="326.084059375" y1="41.279190625" x2="326.0852" y2="41.278809375" width="0.025" layer="94"/>
<wire x1="326.0852" y1="41.278809375" x2="326.119490625" y2="41.43883125" width="0.025" layer="94"/>
<wire x1="326.0852" y1="41.461690625" x2="326.119490625" y2="41.43883125" width="0.025" layer="94"/>
<wire x1="325.993759375" y1="41.49598125" x2="326.0852" y2="41.461690625" width="0.025" layer="94"/>
<wire x1="325.99261875" y1="41.49646875" x2="325.993759375" y2="41.49598125" width="0.025" layer="94"/>
<wire x1="325.650859375" y1="41.66743125" x2="325.652" y2="41.66678125" width="0.025" layer="94"/>
<wire x1="325.55941875" y1="41.72458125" x2="325.650859375" y2="41.66743125" width="0.025" layer="94"/>
<wire x1="325.44511875" y1="42.95901875" x2="325.536559375" y2="43.0276" width="0.025" layer="94"/>
<wire x1="325.536559375" y1="43.0276" x2="325.5377" y2="43.028359375" width="0.025" layer="94"/>
<wire x1="325.605140625" y1="43.07331875" x2="325.776590625" y2="43.176190625" width="0.025" layer="94"/>
<wire x1="326.49668125" y1="43.43908125" x2="326.64526875" y2="43.47336875" width="0.025" layer="94"/>
<wire x1="326.75956875" y1="43.30191875" x2="326.81671875" y2="43.31335" width="0.025" layer="94"/>
<wire x1="326.71385" y1="43.290490625" x2="326.75956875" y2="43.30191875" width="0.025" layer="94"/>
<wire x1="326.6567" y1="43.279059375" x2="326.71385" y2="43.290490625" width="0.025" layer="94"/>
<wire x1="326.565259375" y1="43.2562" x2="326.6567" y2="43.279059375" width="0.025" layer="94"/>
<wire x1="326.35951875" y1="43.18761875" x2="326.41666875" y2="43.21048125" width="0.025" layer="94"/>
<wire x1="326.32523125" y1="43.176190625" x2="326.35951875" y2="43.18761875" width="0.025" layer="94"/>
<wire x1="326.21093125" y1="43.13046875" x2="326.32523125" y2="43.176190625" width="0.025" layer="94"/>
<wire x1="326.005190625" y1="43.0276" x2="326.21093125" y2="43.13046875" width="0.025" layer="94"/>
<wire x1="325.547609375" y1="42.09148125" x2="325.547990625" y2="42.090340625" width="0.025" layer="94"/>
<wire x1="325.547990625" y1="42.090340625" x2="325.57085" y2="42.04461875" width="0.025" layer="94"/>
<wire x1="325.84516875" y1="41.747440625" x2="325.95946875" y2="41.678859375" width="0.025" layer="94"/>
<wire x1="325.95946875" y1="41.678859375" x2="326.03948125" y2="41.633140625" width="0.025" layer="94"/>
<wire x1="326.03948125" y1="41.633140625" x2="326.108059375" y2="41.59885" width="0.025" layer="94"/>
<wire x1="326.108059375" y1="41.59885" x2="326.165209375" y2="41.575990625" width="0.025" layer="94"/>
<wire x1="326.165209375" y1="41.575990625" x2="326.35951875" y2="42.307509375" width="0.025" layer="94"/>
<wire x1="329.022709375" y1="41.575990625" x2="329.04556875" y2="41.575990625" width="0.025" layer="94"/>
<wire x1="329.04556875" y1="41.575990625" x2="329.251309375" y2="41.678859375" width="0.025" layer="94"/>
<wire x1="329.251309375" y1="41.678859375" x2="329.25245" y2="41.67961875" width="0.025" layer="94"/>
<wire x1="329.42161875" y1="41.7924" x2="329.422759375" y2="41.793159375" width="0.025" layer="94"/>
<wire x1="329.422759375" y1="41.793159375" x2="329.548490625" y2="41.918890625" width="0.025" layer="94"/>
<wire x1="329.548490625" y1="41.918890625" x2="329.58278125" y2="41.964609375" width="0.025" layer="94"/>
<wire x1="329.58278125" y1="41.964609375" x2="329.6285" y2="42.033190625" width="0.025" layer="94"/>
<wire x1="329.662790625" y1="42.1132" x2="329.68565" y2="42.18178125" width="0.025" layer="94"/>
<wire x1="329.68565" y1="42.18178125" x2="329.69708125" y2="42.2275" width="0.025" layer="94"/>
<wire x1="329.57135" y1="42.718990625" x2="329.61706875" y2="42.650409375" width="0.025" layer="94"/>
<wire x1="329.537059375" y1="42.764709375" x2="329.57135" y2="42.718990625" width="0.025" layer="94"/>
<wire x1="329.434190625" y1="42.86758125" x2="329.43533125" y2="42.866440625" width="0.025" layer="94"/>
<wire x1="329.377040625" y1="42.9133" x2="329.434190625" y2="42.86758125" width="0.025" layer="94"/>
<wire x1="329.01128125" y1="43.119040625" x2="329.06843125" y2="43.09618125" width="0.025" layer="94"/>
<wire x1="328.965559375" y1="43.1419" x2="329.01128125" y2="43.119040625" width="0.025" layer="94"/>
<wire x1="328.32548125" y1="43.32478125" x2="328.43978125" y2="43.30191875" width="0.025" layer="94"/>
<wire x1="328.2569" y1="43.336209375" x2="328.32548125" y2="43.32478125" width="0.025" layer="94"/>
<wire x1="328.15403125" y1="43.347640625" x2="328.2569" y2="43.336209375" width="0.025" layer="94"/>
<wire x1="328.005440625" y1="43.35906875" x2="328.15403125" y2="43.347640625" width="0.025" layer="94"/>
<wire x1="328.005440625" y1="43.35906875" x2="328.005440625" y2="43.53051875" width="0.025" layer="94"/>
<wire x1="328.78268125" y1="43.41621875" x2="329.010140625" y2="43.34798125" width="0.025" layer="94"/>
<wire x1="329.010140625" y1="43.34798125" x2="329.01128125" y2="43.347640625" width="0.025" layer="94"/>
<wire x1="329.01128125" y1="43.347640625" x2="329.194159375" y2="43.279059375" width="0.025" layer="94"/>
<wire x1="329.194159375" y1="43.279059375" x2="329.308459375" y2="43.233340625" width="0.025" layer="94"/>
<wire x1="329.308459375" y1="43.233340625" x2="329.3096" y2="43.23276875" width="0.025" layer="94"/>
<wire x1="329.81138125" y1="42.9133" x2="329.891390625" y2="42.833290625" width="0.025" layer="94"/>
<wire x1="329.891390625" y1="42.833290625" x2="329.937109375" y2="42.776140625" width="0.025" layer="94"/>
<wire x1="329.95996875" y1="42.74185" x2="329.994259375" y2="42.6847" width="0.025" layer="94"/>
<wire x1="329.994259375" y1="42.6847" x2="330.03998125" y2="42.5704" width="0.025" layer="94"/>
<wire x1="330.03998125" y1="42.5704" x2="330.051409375" y2="42.51325" width="0.025" layer="94"/>
<wire x1="330.051409375" y1="42.51325" x2="330.062840625" y2="42.421809375" width="0.025" layer="94"/>
<wire x1="329.55991875" y1="41.66743125" x2="329.6285" y2="41.71315" width="0.025" layer="94"/>
<wire x1="329.45705" y1="41.61028125" x2="329.55991875" y2="41.66743125" width="0.025" layer="94"/>
<wire x1="329.15986875" y1="41.47311875" x2="329.1633" y2="41.474590625" width="0.025" layer="94"/>
<wire x1="329.06843125" y1="41.43883125" x2="329.15986875" y2="41.47311875" width="0.025" layer="94"/>
<wire x1="329.137009375" y1="41.290240625" x2="329.18273125" y2="41.30166875" width="0.025" layer="94"/>
<wire x1="329.18273125" y1="41.30166875" x2="329.21701875" y2="41.3131" width="0.025" layer="94"/>
<wire x1="329.21701875" y1="41.3131" x2="329.262740625" y2="41.32453125" width="0.025" layer="94"/>
<wire x1="329.262740625" y1="41.32453125" x2="329.3999" y2="41.37025" width="0.025" layer="94"/>
<wire x1="329.57135" y1="41.43883125" x2="329.605640625" y2="41.450259375" width="0.025" layer="94"/>
<wire x1="329.605640625" y1="41.450259375" x2="329.834240625" y2="41.564559375" width="0.025" layer="94"/>
<wire x1="329.834240625" y1="41.564559375" x2="329.86853125" y2="41.58741875" width="0.025" layer="94"/>
<wire x1="329.86853125" y1="41.58741875" x2="329.891390625" y2="41.59885" width="0.025" layer="94"/>
<wire x1="330.30286875" y1="41.93031875" x2="330.348590625" y2="41.98746875" width="0.025" layer="94"/>
<wire x1="330.348590625" y1="41.98746875" x2="330.37145" y2="42.01033125" width="0.025" layer="94"/>
<wire x1="330.405740625" y1="42.05605" x2="330.47431875" y2="42.17035" width="0.025" layer="94"/>
<wire x1="330.47431875" y1="42.17035" x2="330.520040625" y2="42.28465" width="0.025" layer="94"/>
<wire x1="330.54251875" y1="42.352090625" x2="330.5429" y2="42.35323125" width="0.025" layer="94"/>
<wire x1="330.5429" y1="42.35323125" x2="330.55433125" y2="42.41038125" width="0.025" layer="94"/>
<wire x1="330.55433125" y1="42.41038125" x2="330.565759375" y2="42.478959375" width="0.025" layer="94"/>
<wire x1="328.72553125" y1="44.22775" x2="328.95413125" y2="44.18203125" width="0.025" layer="94"/>
<wire x1="328.95413125" y1="44.18203125" x2="329.22845" y2="44.11345" width="0.025" layer="94"/>
<wire x1="329.69708125" y1="43.95343125" x2="329.7428" y2="43.93056875" width="0.025" layer="94"/>
<wire x1="329.7428" y1="43.93056875" x2="329.79995" y2="43.907709375" width="0.025" layer="94"/>
<wire x1="329.79995" y1="43.907709375" x2="329.84566875" y2="43.88485" width="0.025" layer="94"/>
<wire x1="329.84566875" y1="43.88485" x2="329.90281875" y2="43.861990625" width="0.025" layer="94"/>
<wire x1="329.90281875" y1="43.861990625" x2="330.03998125" y2="43.793409375" width="0.025" layer="94"/>
<wire x1="330.03998125" y1="43.793409375" x2="330.09713125" y2="43.75911875" width="0.025" layer="94"/>
<wire x1="330.09713125" y1="43.75911875" x2="330.14285" y2="43.736259375" width="0.025" layer="94"/>
<wire x1="330.55433125" y1="43.43908125" x2="330.60005" y2="43.393359375" width="0.025" layer="94"/>
<wire x1="330.60005" y1="43.393359375" x2="330.66863125" y2="43.31335" width="0.025" layer="94"/>
<wire x1="330.66863125" y1="43.31335" x2="330.70291875" y2="43.279059375" width="0.025" layer="94"/>
<wire x1="330.805790625" y1="42.193209375" x2="330.82865" y2="42.23893125" width="0.025" layer="94"/>
<wire x1="330.7715" y1="42.136059375" x2="330.805790625" y2="42.193209375" width="0.025" layer="94"/>
<wire x1="330.70291875" y1="42.04461875" x2="330.7715" y2="42.136059375" width="0.025" layer="94"/>
<wire x1="329.29703125" y1="41.233090625" x2="329.594209375" y2="41.32453125" width="0.025" layer="94"/>
<wire x1="329.1713" y1="41.03878125" x2="329.18273125" y2="41.03878125" width="0.025" layer="94"/>
<wire x1="329.18273125" y1="41.03878125" x2="329.26616875" y2="41.059640625" width="0.025" layer="94"/>
<wire x1="329.26616875" y1="41.059640625" x2="329.50276875" y2="41.118790625" width="0.025" layer="94"/>
<wire x1="329.50276875" y1="41.118790625" x2="329.7428" y2="41.18736875" width="0.025" layer="94"/>
<wire x1="329.7428" y1="41.18736875" x2="329.743940625" y2="41.18775" width="0.025" layer="94"/>
<wire x1="329.948540625" y1="41.25595" x2="330.005690625" y2="41.278809375" width="0.025" layer="94"/>
<wire x1="330.005690625" y1="41.278809375" x2="330.03998125" y2="41.290240625" width="0.025" layer="94"/>
<wire x1="330.60005" y1="41.564559375" x2="330.634340625" y2="41.58741875" width="0.025" layer="94"/>
<wire x1="330.634340625" y1="41.58741875" x2="330.691490625" y2="41.621709375" width="0.025" layer="94"/>
<wire x1="330.691490625" y1="41.621709375" x2="330.737209375" y2="41.656" width="0.025" layer="94"/>
<wire x1="331.2287" y1="42.06748125" x2="331.27441875" y2="42.12463125" width="0.025" layer="94"/>
<wire x1="331.27441875" y1="42.12463125" x2="331.308709375" y2="42.17035" width="0.025" layer="94"/>
<wire x1="331.33156875" y1="42.204640625" x2="331.343" y2="42.2275" width="0.025" layer="94"/>
<wire x1="331.343" y1="42.2275" x2="331.365859375" y2="42.261790625" width="0.025" layer="94"/>
<wire x1="331.50301875" y1="42.97045" x2="331.51445" y2="42.84471875" width="0.025" layer="94"/>
<wire x1="331.365859375" y1="43.3705" x2="331.36643125" y2="43.369359375" width="0.025" layer="94"/>
<wire x1="331.27441875" y1="43.507659375" x2="331.365859375" y2="43.3705" width="0.025" layer="94"/>
<wire x1="331.24013125" y1="43.55338125" x2="331.27441875" y2="43.507659375" width="0.025" layer="94"/>
<wire x1="330.61148125" y1="44.090590625" x2="330.66863125" y2="44.0563" width="0.025" layer="94"/>
<wire x1="330.610340625" y1="44.09135" x2="330.61148125" y2="44.090590625" width="0.025" layer="94"/>
<wire x1="329.95996875" y1="44.3992" x2="330.24571875" y2="44.2849" width="0.025" layer="94"/>
<wire x1="329.651359375" y1="44.50206875" x2="329.95996875" y2="44.3992" width="0.025" layer="94"/>
<wire x1="329.491340625" y1="44.547790625" x2="329.651359375" y2="44.50206875" width="0.025" layer="94"/>
<wire x1="329.21701875" y1="44.61636875" x2="329.491340625" y2="44.547790625" width="0.025" layer="94"/>
<wire x1="328.3712" y1="44.75353125" x2="328.47406875" y2="44.7421" width="0.025" layer="94"/>
<wire x1="328.2569" y1="44.764959375" x2="328.3712" y2="44.75353125" width="0.025" layer="94"/>
<wire x1="328.222609375" y1="44.764959375" x2="328.2569" y2="44.764959375" width="0.025" layer="94"/>
<wire x1="327.58253125" y1="40.78731875" x2="328.519790625" y2="36.21531875" width="0.025" layer="94"/>
<wire x1="326.690990625" y1="36.21531875" x2="327.58253125" y2="40.78731875" width="0.025" layer="94"/>
<wire x1="326.690990625" y1="36.21531875" x2="326.86038125" y2="37.084" width="0.025" layer="94"/>
<wire x1="324.9746" y1="37.084" x2="323.764909375" y2="32.512" width="0.025" layer="94"/>
<wire x1="323.764909375" y1="32.512" x2="325.8566" y2="32.512" width="0.025" layer="94"/>
<wire x1="325.8566" y1="32.512" x2="326.336659375" y2="34.5694" width="0.025" layer="94"/>
<wire x1="326.336659375" y1="34.5694" x2="328.862690625" y2="34.5694" width="0.025" layer="94"/>
<wire x1="328.862690625" y1="34.5694" x2="329.319890625" y2="32.512" width="0.025" layer="94"/>
<wire x1="329.319890625" y1="32.512" x2="331.44586875" y2="32.512" width="0.025" layer="94"/>
<wire x1="331.44586875" y1="32.512" x2="330.22376875" y2="37.084" width="0.025" layer="94"/>
<wire x1="328.341709375" y1="37.084" x2="328.35108125" y2="37.03828125" width="0.025" layer="94"/>
<wire x1="328.35108125" y1="37.03828125" x2="328.519790625" y2="36.21531875" width="0.025" layer="94"/>
<wire x1="328.519790625" y1="36.21531875" x2="326.690990625" y2="36.21531875" width="0.025" layer="94"/>
<wire x1="314.41516875" y1="42.29608125" x2="314.41516875" y2="32.512" width="0.025" layer="94"/>
<wire x1="314.41516875" y1="32.512" x2="316.26683125" y2="32.512" width="0.025" layer="94"/>
<wire x1="316.26683125" y1="32.512" x2="316.26683125" y2="40.21581875" width="0.025" layer="94"/>
<wire x1="316.26683125" y1="40.21581875" x2="317.98133125" y2="32.512" width="0.025" layer="94"/>
<wire x1="317.98133125" y1="32.512" x2="319.55866875" y2="32.512" width="0.025" layer="94"/>
<wire x1="319.55866875" y1="32.512" x2="321.250309375" y2="40.21581875" width="0.025" layer="94"/>
<wire x1="321.250309375" y1="40.21581875" x2="321.250309375" y2="32.512" width="0.025" layer="94"/>
<wire x1="321.250309375" y1="32.512" x2="323.12483125" y2="32.512" width="0.025" layer="94"/>
<wire x1="323.12483125" y1="32.512" x2="323.12483125" y2="42.29608125" width="0.025" layer="94"/>
<wire x1="323.12483125" y1="42.29608125" x2="320.15303125" y2="42.29608125" width="0.025" layer="94"/>
<wire x1="320.15303125" y1="42.29608125" x2="318.767990625" y2="35.575240625" width="0.025" layer="94"/>
<wire x1="318.767990625" y1="35.575240625" x2="317.364109375" y2="42.29608125" width="0.025" layer="94"/>
<wire x1="317.364109375" y1="42.29608125" x2="314.41516875" y2="42.29608125" width="0.025" layer="94"/>
<wire x1="312.83783125" y1="35.392359375" x2="312.83783125" y2="42.3418" width="0.025" layer="94"/>
<wire x1="312.83783125" y1="42.3418" x2="310.78043125" y2="42.3418" width="0.025" layer="94"/>
<wire x1="310.78043125" y1="42.3418" x2="310.78043125" y2="35.1409" width="0.025" layer="94"/>
<wire x1="310.78043125" y1="35.1409" x2="310.75756875" y2="35.0266" width="0.025" layer="94"/>
<wire x1="310.75756875" y1="35.0266" x2="310.71185" y2="34.889440625" width="0.025" layer="94"/>
<wire x1="310.71185" y1="34.889440625" x2="310.63346875" y2="34.75228125" width="0.025" layer="94"/>
<wire x1="310.63346875" y1="34.75228125" x2="310.620409375" y2="34.72941875" width="0.025" layer="94"/>
<wire x1="310.620409375" y1="34.72941875" x2="310.55183125" y2="34.660840625" width="0.025" layer="94"/>
<wire x1="310.55183125" y1="34.660840625" x2="310.506109375" y2="34.619690625" width="0.025" layer="94"/>
<wire x1="310.506109375" y1="34.619690625" x2="310.41466875" y2="34.546540625" width="0.025" layer="94"/>
<wire x1="310.41466875" y1="34.546540625" x2="310.30036875" y2="34.477959375" width="0.025" layer="94"/>
<wire x1="310.30036875" y1="34.477959375" x2="310.18606875" y2="34.432240625" width="0.025" layer="94"/>
<wire x1="310.18606875" y1="34.432240625" x2="310.02605" y2="34.38651875" width="0.025" layer="94"/>
<wire x1="310.02605" y1="34.38651875" x2="309.84316875" y2="34.363659375" width="0.025" layer="94"/>
<wire x1="309.84316875" y1="34.363659375" x2="309.660290625" y2="34.363659375" width="0.025" layer="94"/>
<wire x1="309.660290625" y1="34.363659375" x2="309.56885" y2="34.3789" width="0.025" layer="94"/>
<wire x1="309.56885" y1="34.3789" x2="309.52313125" y2="34.38651875" width="0.025" layer="94"/>
<wire x1="309.52313125" y1="34.38651875" x2="309.477409375" y2="34.39958125" width="0.025" layer="94"/>
<wire x1="309.477409375" y1="34.39958125" x2="309.431690625" y2="34.41265" width="0.025" layer="94"/>
<wire x1="309.431690625" y1="34.41265" x2="309.363109375" y2="34.432240625" width="0.025" layer="94"/>
<wire x1="309.363109375" y1="34.432240625" x2="309.22595" y2="34.50081875" width="0.025" layer="94"/>
<wire x1="309.22595" y1="34.50081875" x2="309.088790625" y2="34.592259375" width="0.025" layer="94"/>
<wire x1="309.088790625" y1="34.592259375" x2="308.95163125" y2="34.72941875" width="0.025" layer="94"/>
<wire x1="308.95163125" y1="34.72941875" x2="308.70733125" y2="35.05515" width="0.025" layer="94"/>
<wire x1="308.70733125" y1="35.05515" x2="307.145690625" y2="33.81501875" width="0.025" layer="94"/>
<wire x1="307.145690625" y1="33.81501875" x2="307.259990625" y2="33.58641875" width="0.025" layer="94"/>
<wire x1="307.259990625" y1="33.58641875" x2="307.305709375" y2="33.525459375" width="0.025" layer="94"/>
<wire x1="307.305709375" y1="33.525459375" x2="307.34571875" y2="33.47211875" width="0.025" layer="94"/>
<wire x1="307.34571875" y1="33.47211875" x2="307.39715" y2="33.403540625" width="0.025" layer="94"/>
<wire x1="307.39715" y1="33.403540625" x2="307.43935" y2="33.35781875" width="0.025" layer="94"/>
<wire x1="307.43935" y1="33.35781875" x2="307.488590625" y2="33.30448125" width="0.025" layer="94"/>
<wire x1="307.488590625" y1="33.30448125" x2="307.523759375" y2="33.26638125" width="0.025" layer="94"/>
<wire x1="307.523759375" y1="33.26638125" x2="307.55716875" y2="33.23018125" width="0.025" layer="94"/>
<wire x1="307.55716875" y1="33.23018125" x2="307.587059375" y2="33.1978" width="0.025" layer="94"/>
<wire x1="307.587059375" y1="33.1978" x2="307.62575" y2="33.155890625" width="0.025" layer="94"/>
<wire x1="307.62575" y1="33.155890625" x2="307.65036875" y2="33.12921875" width="0.025" layer="94"/>
<wire x1="307.65036875" y1="33.12921875" x2="307.67146875" y2="33.106359375" width="0.025" layer="94"/>
<wire x1="307.67146875" y1="33.106359375" x2="307.762909375" y2="33.02323125" width="0.025" layer="94"/>
<wire x1="307.762909375" y1="33.02323125" x2="307.7972" y2="32.992059375" width="0.025" layer="94"/>
<wire x1="307.7972" y1="32.992059375" x2="307.85435" y2="32.940109375" width="0.025" layer="94"/>
<wire x1="307.85435" y1="32.940109375" x2="307.89778125" y2="32.90061875" width="0.025" layer="94"/>
<wire x1="307.89778125" y1="32.90061875" x2="307.92293125" y2="32.877759375" width="0.025" layer="94"/>
<wire x1="307.92293125" y1="32.877759375" x2="308.12866875" y2="32.7406" width="0.025" layer="94"/>
<wire x1="308.12866875" y1="32.7406" x2="308.174390625" y2="32.71011875" width="0.025" layer="94"/>
<wire x1="308.174390625" y1="32.71011875" x2="308.24296875" y2="32.6705" width="0.025" layer="94"/>
<wire x1="308.24296875" y1="32.6705" x2="308.288690625" y2="32.646109375" width="0.025" layer="94"/>
<wire x1="308.288690625" y1="32.646109375" x2="308.3687" y2="32.603440625" width="0.025" layer="94"/>
<wire x1="308.3687" y1="32.603440625" x2="308.42585" y2="32.572959375" width="0.025" layer="94"/>
<wire x1="308.42585" y1="32.572959375" x2="308.54015" y2="32.512" width="0.025" layer="94"/>
<wire x1="308.54015" y1="32.512" x2="308.66206875" y2="32.46628125" width="0.025" layer="94"/>
<wire x1="308.66206875" y1="32.46628125" x2="308.72303125" y2="32.44341875" width="0.025" layer="94"/>
<wire x1="308.72303125" y1="32.44341875" x2="308.905909375" y2="32.374840625" width="0.025" layer="94"/>
<wire x1="308.905909375" y1="32.374840625" x2="308.99735" y2="32.353740625" width="0.025" layer="94"/>
<wire x1="308.99735" y1="32.353740625" x2="309.203090625" y2="32.306259375" width="0.025" layer="94"/>
<wire x1="309.203090625" y1="32.306259375" x2="309.27166875" y2="32.295709375" width="0.025" layer="94"/>
<wire x1="309.27166875" y1="32.295709375" x2="309.35168125" y2="32.2834" width="0.025" layer="94"/>
<wire x1="309.35168125" y1="32.2834" x2="309.40883125" y2="32.274609375" width="0.025" layer="94"/>
<wire x1="309.40883125" y1="32.274609375" x2="309.477409375" y2="32.264059375" width="0.025" layer="94"/>
<wire x1="309.477409375" y1="32.264059375" x2="309.50026875" y2="32.260540625" width="0.025" layer="94"/>
<wire x1="309.50026875" y1="32.260540625" x2="309.68315" y2="32.2453" width="0.025" layer="94"/>
<wire x1="309.68315" y1="32.2453" x2="309.774590625" y2="32.23768125" width="0.025" layer="94"/>
<wire x1="309.774590625" y1="32.23768125" x2="310.048909375" y2="32.260540625" width="0.025" layer="94"/>
<wire x1="310.048909375" y1="32.260540625" x2="310.231790625" y2="32.2834" width="0.025" layer="94"/>
<wire x1="310.231790625" y1="32.2834" x2="310.460390625" y2="32.32911875" width="0.025" layer="94"/>
<wire x1="310.460390625" y1="32.32911875" x2="310.620409375" y2="32.374840625" width="0.025" layer="94"/>
<wire x1="310.620409375" y1="32.374840625" x2="310.82615" y2="32.44341875" width="0.025" layer="94"/>
<wire x1="310.82615" y1="32.44341875" x2="311.05475" y2="32.534859375" width="0.025" layer="94"/>
<wire x1="311.05475" y1="32.534859375" x2="311.374790625" y2="32.69488125" width="0.025" layer="94"/>
<wire x1="311.374790625" y1="32.69488125" x2="311.58053125" y2="32.832040625" width="0.025" layer="94"/>
<wire x1="311.58053125" y1="32.832040625" x2="311.649109375" y2="32.889190625" width="0.025" layer="94"/>
<wire x1="311.649109375" y1="32.889190625" x2="311.69483125" y2="32.927290625" width="0.025" layer="94"/>
<wire x1="311.69483125" y1="32.927290625" x2="311.763409375" y2="32.984440625" width="0.025" layer="94"/>
<wire x1="311.763409375" y1="32.984440625" x2="311.85485" y2="33.060640625" width="0.025" layer="94"/>
<wire x1="311.85485" y1="33.060640625" x2="312.106309375" y2="33.3121" width="0.025" layer="94"/>
<wire x1="312.106309375" y1="33.3121" x2="312.15203125" y2="33.373059375" width="0.025" layer="94"/>
<wire x1="312.15203125" y1="33.373059375" x2="312.174890625" y2="33.403540625" width="0.025" layer="94"/>
<wire x1="312.174890625" y1="33.403540625" x2="312.220609375" y2="33.4645" width="0.025" layer="94"/>
<wire x1="312.220609375" y1="33.4645" x2="312.258709375" y2="33.517840625" width="0.025" layer="94"/>
<wire x1="312.258709375" y1="33.517840625" x2="312.30443125" y2="33.58641875" width="0.025" layer="94"/>
<wire x1="312.30443125" y1="33.58641875" x2="312.334909375" y2="33.632140625" width="0.025" layer="94"/>
<wire x1="312.334909375" y1="33.632140625" x2="312.39586875" y2="33.72358125" width="0.025" layer="94"/>
<wire x1="312.39586875" y1="33.72358125" x2="312.42635" y2="33.7693" width="0.025" layer="94"/>
<wire x1="312.42635" y1="33.7693" x2="312.60923125" y2="34.135059375" width="0.025" layer="94"/>
<wire x1="312.60923125" y1="34.135059375" x2="312.65495" y2="34.249359375" width="0.025" layer="94"/>
<wire x1="312.65495" y1="34.249359375" x2="312.68108125" y2="34.3408" width="0.025" layer="94"/>
<wire x1="312.68108125" y1="34.3408" x2="312.7121" y2="34.4551" width="0.025" layer="94"/>
<wire x1="312.7121" y1="34.4551" x2="312.746390625" y2="34.61511875" width="0.025" layer="94"/>
<wire x1="312.746390625" y1="34.61511875" x2="312.76925" y2="34.72941875" width="0.025" layer="94"/>
<wire x1="312.76925" y1="34.72941875" x2="312.792109375" y2="34.889440625" width="0.025" layer="94"/>
<wire x1="312.792109375" y1="34.889440625" x2="312.809890625" y2="35.049459375" width="0.025" layer="94"/>
<wire x1="312.809890625" y1="35.049459375" x2="312.81673125" y2="35.118040625" width="0.025" layer="94"/>
<wire x1="312.81673125" y1="35.118040625" x2="312.823759375" y2="35.20948125" width="0.025" layer="94"/>
<wire x1="312.823759375" y1="35.20948125" x2="312.829040625" y2="35.278059375" width="0.025" layer="94"/>
<wire x1="312.829040625" y1="35.278059375" x2="312.83783125" y2="35.392359375" width="0.025" layer="94"/>
<wire x1="324.9625" y1="37.03828125" x2="325.251490625" y2="38.13625" width="0.025" layer="94"/>
<wire x1="325.251490625" y1="38.13625" x2="325.993759375" y2="40.935909375" width="0.025" layer="94"/>
<wire x1="325.993759375" y1="40.935909375" x2="325.90231875" y2="40.95876875" width="0.025" layer="94"/>
<wire x1="325.90231875" y1="40.95876875" x2="325.78801875" y2="40.98163125" width="0.025" layer="94"/>
<wire x1="325.78801875" y1="40.98163125" x2="325.63943125" y2="41.01591875" width="0.025" layer="94"/>
<wire x1="325.63943125" y1="41.01591875" x2="325.536559375" y2="41.03878125" width="0.025" layer="94"/>
<wire x1="325.536559375" y1="41.03878125" x2="325.3994" y2="41.07306875" width="0.025" layer="94"/>
<wire x1="325.3994" y1="41.07306875" x2="325.319390625" y2="41.09593125" width="0.025" layer="94"/>
<wire x1="325.319390625" y1="41.09593125" x2="325.21651875" y2="41.13021875" width="0.025" layer="94"/>
<wire x1="325.21651875" y1="41.13021875" x2="325.06793125" y2="41.175940625" width="0.025" layer="94"/>
<wire x1="325.06793125" y1="41.175940625" x2="324.97076875" y2="41.21251875" width="0.025" layer="94"/>
<wire x1="324.97076875" y1="41.21251875" x2="324.747890625" y2="41.30166875" width="0.025" layer="94"/>
<wire x1="324.747890625" y1="41.30166875" x2="324.565009375" y2="41.38168125" width="0.025" layer="94"/>
<wire x1="324.565009375" y1="41.38168125" x2="324.45185" y2="41.438259375" width="0.025" layer="94"/>
<wire x1="324.45185" y1="41.438259375" x2="324.393559375" y2="41.47311875" width="0.025" layer="94"/>
<wire x1="324.393559375" y1="41.47311875" x2="324.347840625" y2="41.49598125" width="0.025" layer="94"/>
<wire x1="324.347840625" y1="41.49598125" x2="324.18781875" y2="41.58741875" width="0.025" layer="94"/>
<wire x1="324.18781875" y1="41.58741875" x2="324.08495" y2="41.656" width="0.025" layer="94"/>
<wire x1="324.08495" y1="41.656" x2="323.85635" y2="41.82745" width="0.025" layer="94"/>
<wire x1="323.85635" y1="41.82745" x2="323.776340625" y2="41.89603125" width="0.025" layer="94"/>
<wire x1="323.776340625" y1="41.89603125" x2="323.662040625" y2="42.01033125" width="0.025" layer="94"/>
<wire x1="323.662040625" y1="42.01033125" x2="323.593459375" y2="42.090340625" width="0.025" layer="94"/>
<wire x1="323.593459375" y1="42.090340625" x2="323.547740625" y2="42.147490625" width="0.025" layer="94"/>
<wire x1="323.547740625" y1="42.147490625" x2="323.52488125" y2="42.18178125" width="0.025" layer="94"/>
<wire x1="323.52488125" y1="42.18178125" x2="323.490590625" y2="42.2275" width="0.025" layer="94"/>
<wire x1="323.490590625" y1="42.2275" x2="323.46773125" y2="42.261790625" width="0.025" layer="94"/>
<wire x1="323.46773125" y1="42.261790625" x2="323.433440625" y2="42.318940625" width="0.025" layer="94"/>
<wire x1="323.433440625" y1="42.318940625" x2="323.41058125" y2="42.35323125" width="0.025" layer="94"/>
<wire x1="323.41058125" y1="42.35323125" x2="323.354" y2="42.466390625" width="0.025" layer="94"/>
<wire x1="323.354" y1="42.466390625" x2="323.33056875" y2="42.52468125" width="0.025" layer="94"/>
<wire x1="323.33056875" y1="42.52468125" x2="323.29628125" y2="42.62755" width="0.025" layer="94"/>
<wire x1="323.29628125" y1="42.62755" x2="323.28485" y2="42.67326875" width="0.025" layer="94"/>
<wire x1="323.28485" y1="42.67326875" x2="323.27341875" y2="42.73041875" width="0.025" layer="94"/>
<wire x1="323.27341875" y1="42.73041875" x2="323.261990625" y2="42.81043125" width="0.025" layer="94"/>
<wire x1="323.261990625" y1="42.81043125" x2="323.250559375" y2="42.936159375" width="0.025" layer="94"/>
<wire x1="323.250559375" y1="42.936159375" x2="323.261990625" y2="43.061890625" width="0.025" layer="94"/>
<wire x1="323.261990625" y1="43.061890625" x2="323.27341875" y2="43.13046875" width="0.025" layer="94"/>
<wire x1="323.27341875" y1="43.13046875" x2="323.28485" y2="43.18761875" width="0.025" layer="94"/>
<wire x1="323.28485" y1="43.18761875" x2="323.29628125" y2="43.233340625" width="0.025" layer="94"/>
<wire x1="323.29628125" y1="43.233340625" x2="323.33056875" y2="43.336209375" width="0.025" layer="94"/>
<wire x1="323.33056875" y1="43.336209375" x2="323.35343125" y2="43.393359375" width="0.025" layer="94"/>
<wire x1="323.35343125" y1="43.393359375" x2="323.41058125" y2="43.507659375" width="0.025" layer="94"/>
<wire x1="323.41058125" y1="43.507659375" x2="323.44486875" y2="43.564809375" width="0.025" layer="94"/>
<wire x1="323.44486875" y1="43.564809375" x2="323.479159375" y2="43.61053125" width="0.025" layer="94"/>
<wire x1="323.479159375" y1="43.61053125" x2="323.536309375" y2="43.690540625" width="0.025" layer="94"/>
<wire x1="323.536309375" y1="43.690540625" x2="323.62775" y2="43.793409375" width="0.025" layer="94"/>
<wire x1="323.62775" y1="43.793409375" x2="323.78776875" y2="43.95343125" width="0.025" layer="94"/>
<wire x1="323.78776875" y1="43.95343125" x2="323.87806875" y2="44.0323" width="0.025" layer="94"/>
<wire x1="323.87806875" y1="44.0323" x2="323.9375" y2="44.08001875" width="0.025" layer="94"/>
<wire x1="323.9375" y1="44.08001875" x2="324.0278" y2="44.147740625" width="0.025" layer="94"/>
<wire x1="324.0278" y1="44.147740625" x2="324.164959375" y2="44.23918125" width="0.025" layer="94"/>
<wire x1="324.164959375" y1="44.23918125" x2="324.290690625" y2="44.319190625" width="0.025" layer="94"/>
<wire x1="324.290690625" y1="44.319190625" x2="324.393559375" y2="44.376340625" width="0.025" layer="94"/>
<wire x1="324.393559375" y1="44.376340625" x2="324.62101875" y2="44.49006875" width="0.025" layer="94"/>
<wire x1="324.62101875" y1="44.49006875" x2="324.72503125" y2="44.536359375" width="0.025" layer="94"/>
<wire x1="324.72503125" y1="44.536359375" x2="325.009640625" y2="44.6502" width="0.025" layer="94"/>
<wire x1="325.009640625" y1="44.6502" x2="325.10908125" y2="44.68343125" width="0.025" layer="94"/>
<wire x1="325.10908125" y1="44.68343125" x2="325.287390625" y2="44.742859375" width="0.025" layer="94"/>
<wire x1="325.287390625" y1="44.742859375" x2="325.35368125" y2="44.764959375" width="0.025" layer="94"/>
<wire x1="325.35368125" y1="44.764959375" x2="325.43483125" y2="44.7881" width="0.025" layer="94"/>
<wire x1="325.43483125" y1="44.7881" x2="325.5137" y2="44.81068125" width="0.025" layer="94"/>
<wire x1="325.5137" y1="44.81068125" x2="325.741159375" y2="44.867540625" width="0.025" layer="94"/>
<wire x1="325.741159375" y1="44.867540625" x2="325.84516875" y2="44.890690625" width="0.025" layer="94"/>
<wire x1="325.84516875" y1="44.890690625" x2="326.07376875" y2="44.936409375" width="0.025" layer="94"/>
<wire x1="326.07376875" y1="44.936409375" x2="326.14235" y2="44.947840625" width="0.025" layer="94"/>
<wire x1="326.14235" y1="44.947840625" x2="326.222359375" y2="44.95926875" width="0.025" layer="94"/>
<wire x1="326.222359375" y1="44.95926875" x2="326.35951875" y2="44.98213125" width="0.025" layer="94"/>
<wire x1="326.35951875" y1="44.98213125" x2="326.43953125" y2="44.993559375" width="0.025" layer="94"/>
<wire x1="326.43953125" y1="44.993559375" x2="326.622409375" y2="45.01641875" width="0.025" layer="94"/>
<wire x1="326.622409375" y1="45.01641875" x2="326.72528125" y2="45.02785" width="0.025" layer="94"/>
<wire x1="326.72528125" y1="45.02785" x2="326.862440625" y2="45.03928125" width="0.025" layer="94"/>
<wire x1="326.862440625" y1="45.03928125" x2="326.862440625" y2="44.764959375" width="0.025" layer="94"/>
<wire x1="326.862440625" y1="44.764959375" x2="326.75956875" y2="44.75353125" width="0.025" layer="94"/>
<wire x1="326.75956875" y1="44.75353125" x2="326.519540625" y2="44.719240625" width="0.025" layer="94"/>
<wire x1="326.519540625" y1="44.719240625" x2="326.450959375" y2="44.707809375" width="0.025" layer="94"/>
<wire x1="326.450959375" y1="44.707809375" x2="326.32523125" y2="44.68495" width="0.025" layer="94"/>
<wire x1="326.32523125" y1="44.68495" x2="326.09663125" y2="44.63923125" width="0.025" layer="94"/>
<wire x1="326.09663125" y1="44.63923125" x2="325.993759375" y2="44.61636875" width="0.025" layer="94"/>
<wire x1="325.993759375" y1="44.61636875" x2="325.84516875" y2="44.58208125" width="0.025" layer="94"/>
<wire x1="325.84516875" y1="44.58208125" x2="325.67371875" y2="44.536359375" width="0.025" layer="94"/>
<wire x1="325.67371875" y1="44.536359375" x2="325.520559375" y2="44.4926" width="0.025" layer="94"/>
<wire x1="325.520559375" y1="44.4926" x2="325.4154" y2="44.45786875" width="0.025" layer="94"/>
<wire x1="325.4154" y1="44.45786875" x2="325.23938125" y2="44.3992" width="0.025" layer="94"/>
<wire x1="325.23938125" y1="44.3992" x2="325.06793125" y2="44.33061875" width="0.025" layer="94"/>
<wire x1="325.06793125" y1="44.33061875" x2="324.93076875" y2="44.27346875" width="0.025" layer="94"/>
<wire x1="324.93076875" y1="44.27346875" x2="324.679309375" y2="44.147740625" width="0.025" layer="94"/>
<wire x1="324.679309375" y1="44.147740625" x2="324.531859375" y2="44.05695" width="0.025" layer="94"/>
<wire x1="324.531859375" y1="44.05695" x2="324.462140625" y2="44.01058125" width="0.025" layer="94"/>
<wire x1="324.462140625" y1="44.01058125" x2="324.279259375" y2="43.87341875" width="0.025" layer="94"/>
<wire x1="324.279259375" y1="43.87341875" x2="324.164959375" y2="43.77055" width="0.025" layer="94"/>
<wire x1="324.164959375" y1="43.77055" x2="324.03923125" y2="43.64481875" width="0.025" layer="94"/>
<wire x1="324.03923125" y1="43.64481875" x2="323.97065" y2="43.564809375" width="0.025" layer="94"/>
<wire x1="323.97065" y1="43.564809375" x2="323.90206875" y2="43.47336875" width="0.025" layer="94"/>
<wire x1="323.90206875" y1="43.47336875" x2="323.86778125" y2="43.41621875" width="0.025" layer="94"/>
<wire x1="323.86778125" y1="43.41621875" x2="323.822059375" y2="43.336209375" width="0.025" layer="94"/>
<wire x1="323.822059375" y1="43.336209375" x2="323.78776875" y2="43.26763125" width="0.025" layer="94"/>
<wire x1="323.78776875" y1="43.26763125" x2="323.75348125" y2="43.176190625" width="0.025" layer="94"/>
<wire x1="323.75348125" y1="43.176190625" x2="323.73061875" y2="43.107609375" width="0.025" layer="94"/>
<wire x1="323.73061875" y1="43.107609375" x2="323.707759375" y2="43.004740625" width="0.025" layer="94"/>
<wire x1="323.707759375" y1="43.004740625" x2="323.69633125" y2="42.90186875" width="0.025" layer="94"/>
<wire x1="323.69633125" y1="42.90186875" x2="323.6849" y2="42.821859375" width="0.025" layer="94"/>
<wire x1="323.6849" y1="42.821859375" x2="323.6849" y2="42.776140625" width="0.025" layer="94"/>
<wire x1="323.6849" y1="42.776140625" x2="323.69633125" y2="42.650409375" width="0.025" layer="94"/>
<wire x1="323.69633125" y1="42.650409375" x2="323.73061875" y2="42.51325" width="0.025" layer="94"/>
<wire x1="323.73061875" y1="42.51325" x2="323.75348125" y2="42.433240625" width="0.025" layer="94"/>
<wire x1="323.75348125" y1="42.433240625" x2="323.7992" y2="42.33036875" width="0.025" layer="94"/>
<wire x1="323.7992" y1="42.33036875" x2="323.822059375" y2="42.28465" width="0.025" layer="94"/>
<wire x1="323.822059375" y1="42.28465" x2="323.890640625" y2="42.18178125" width="0.025" layer="94"/>
<wire x1="323.890640625" y1="42.18178125" x2="323.95921875" y2="42.090340625" width="0.025" layer="94"/>
<wire x1="323.95921875" y1="42.090340625" x2="324.004940625" y2="42.033190625" width="0.025" layer="94"/>
<wire x1="324.004940625" y1="42.033190625" x2="324.08495" y2="41.95318125" width="0.025" layer="94"/>
<wire x1="324.08495" y1="41.95318125" x2="324.176390625" y2="41.87316875" width="0.025" layer="94"/>
<wire x1="324.176390625" y1="41.87316875" x2="324.24496875" y2="41.81601875" width="0.025" layer="94"/>
<wire x1="324.24496875" y1="41.81601875" x2="324.30211875" y2="41.7703" width="0.025" layer="94"/>
<wire x1="324.30211875" y1="41.7703" x2="324.347840625" y2="41.736009375" width="0.025" layer="94"/>
<wire x1="324.347840625" y1="41.736009375" x2="324.41641875" y2="41.690290625" width="0.025" layer="94"/>
<wire x1="324.41641875" y1="41.690290625" x2="324.53071875" y2="41.61028125" width="0.025" layer="94"/>
<wire x1="324.53071875" y1="41.61028125" x2="324.64501875" y2="41.5417" width="0.025" layer="94"/>
<wire x1="324.64501875" y1="41.5417" x2="324.78218125" y2="41.461690625" width="0.025" layer="94"/>
<wire x1="324.78218125" y1="41.461690625" x2="324.919340625" y2="41.393109375" width="0.025" layer="94"/>
<wire x1="324.919340625" y1="41.393109375" x2="325.022209375" y2="41.347390625" width="0.025" layer="94"/>
<wire x1="325.022209375" y1="41.347390625" x2="325.079359375" y2="41.32453125" width="0.025" layer="94"/>
<wire x1="325.079359375" y1="41.32453125" x2="325.15936875" y2="41.290240625" width="0.025" layer="94"/>
<wire x1="325.15936875" y1="41.290240625" x2="325.250809375" y2="41.25595" width="0.025" layer="94"/>
<wire x1="325.250809375" y1="41.25595" x2="325.38796875" y2="41.21023125" width="0.025" layer="94"/>
<wire x1="325.38796875" y1="41.21023125" x2="325.46798125" y2="41.18736875" width="0.025" layer="94"/>
<wire x1="325.46798125" y1="41.18736875" x2="325.58228125" y2="41.15308125" width="0.025" layer="94"/>
<wire x1="325.58228125" y1="41.15308125" x2="325.708009375" y2="41.118790625" width="0.025" layer="94"/>
<wire x1="325.708009375" y1="41.118790625" x2="325.82116875" y2="41.087640625" width="0.025" layer="94"/>
<wire x1="325.82116875" y1="41.087640625" x2="326.01661875" y2="41.03878125" width="0.025" layer="94"/>
<wire x1="326.01661875" y1="41.03878125" x2="326.062340625" y2="41.18736875" width="0.025" layer="94"/>
<wire x1="326.062340625" y1="41.18736875" x2="326.02805" y2="41.1988" width="0.025" layer="94"/>
<wire x1="326.02805" y1="41.1988" x2="325.78801875" y2="41.26738125" width="0.025" layer="94"/>
<wire x1="325.78801875" y1="41.26738125" x2="325.5137" y2="41.35881875" width="0.025" layer="94"/>
<wire x1="325.5137" y1="41.35881875" x2="325.433690625" y2="41.393109375" width="0.025" layer="94"/>
<wire x1="325.433690625" y1="41.393109375" x2="325.319390625" y2="41.43883125" width="0.025" layer="94"/>
<wire x1="325.319390625" y1="41.43883125" x2="325.233659375" y2="41.481690625" width="0.025" layer="94"/>
<wire x1="325.233659375" y1="41.481690625" x2="325.10221875" y2="41.55313125" width="0.025" layer="94"/>
<wire x1="325.10221875" y1="41.55313125" x2="325.01078125" y2="41.614090625" width="0.025" layer="94"/>
<wire x1="325.01078125" y1="41.614090625" x2="324.93076875" y2="41.66743125" width="0.025" layer="94"/>
<wire x1="324.93076875" y1="41.66743125" x2="324.88505" y2="41.70171875" width="0.025" layer="94"/>
<wire x1="324.88505" y1="41.70171875" x2="324.8279" y2="41.747440625" width="0.025" layer="94"/>
<wire x1="324.8279" y1="41.747440625" x2="324.78218125" y2="41.78173125" width="0.025" layer="94"/>
<wire x1="324.78218125" y1="41.78173125" x2="324.66788125" y2="41.87316875" width="0.025" layer="94"/>
<wire x1="324.66788125" y1="41.87316875" x2="324.507859375" y2="42.033190625" width="0.025" layer="94"/>
<wire x1="324.507859375" y1="42.033190625" x2="324.47356875" y2="42.078909375" width="0.025" layer="94"/>
<wire x1="324.47356875" y1="42.078909375" x2="324.42785" y2="42.147490625" width="0.025" layer="94"/>
<wire x1="324.42785" y1="42.147490625" x2="324.393559375" y2="42.204640625" width="0.025" layer="94"/>
<wire x1="324.393559375" y1="42.204640625" x2="324.3707" y2="42.250359375" width="0.025" layer="94"/>
<wire x1="324.3707" y1="42.250359375" x2="324.336409375" y2="42.33036875" width="0.025" layer="94"/>
<wire x1="324.336409375" y1="42.33036875" x2="324.31355" y2="42.41038125" width="0.025" layer="94"/>
<wire x1="324.31355" y1="42.41038125" x2="324.30211875" y2="42.44466875" width="0.025" layer="94"/>
<wire x1="324.30211875" y1="42.44466875" x2="324.290690625" y2="42.50181875" width="0.025" layer="94"/>
<wire x1="324.290690625" y1="42.50181875" x2="324.279259375" y2="42.604690625" width="0.025" layer="94"/>
<wire x1="324.279259375" y1="42.604690625" x2="324.279259375" y2="42.764709375" width="0.025" layer="94"/>
<wire x1="324.279259375" y1="42.764709375" x2="324.290690625" y2="42.84471875" width="0.025" layer="94"/>
<wire x1="324.290690625" y1="42.84471875" x2="324.30211875" y2="42.90186875" width="0.025" layer="94"/>
<wire x1="324.30211875" y1="42.90186875" x2="324.347840625" y2="43.03903125" width="0.025" layer="94"/>
<wire x1="324.347840625" y1="43.03903125" x2="324.38213125" y2="43.107609375" width="0.025" layer="94"/>
<wire x1="324.38213125" y1="43.107609375" x2="324.41641875" y2="43.164759375" width="0.025" layer="94"/>
<wire x1="324.41641875" y1="43.164759375" x2="324.519290625" y2="43.30191875" width="0.025" layer="94"/>
<wire x1="324.519290625" y1="43.30191875" x2="324.70216875" y2="43.4848" width="0.025" layer="94"/>
<wire x1="324.70216875" y1="43.4848" x2="324.77075" y2="43.54195" width="0.025" layer="94"/>
<wire x1="324.77075" y1="43.54195" x2="324.81646875" y2="43.576240625" width="0.025" layer="94"/>
<wire x1="324.81646875" y1="43.576240625" x2="324.98678125" y2="43.68978125" width="0.025" layer="94"/>
<wire x1="324.98678125" y1="43.68978125" x2="325.10108125" y2="43.75843125" width="0.025" layer="94"/>
<wire x1="325.10108125" y1="43.75843125" x2="325.177659375" y2="43.796840625" width="0.025" layer="94"/>
<wire x1="325.177659375" y1="43.796840625" x2="325.376540625" y2="43.89628125" width="0.025" layer="94"/>
<wire x1="325.376540625" y1="43.89628125" x2="325.45655" y2="43.93056875" width="0.025" layer="94"/>
<wire x1="325.45655" y1="43.93056875" x2="325.53426875" y2="43.96143125" width="0.025" layer="94"/>
<wire x1="325.53426875" y1="43.96143125" x2="325.68515" y2="44.022009375" width="0.025" layer="94"/>
<wire x1="325.68515" y1="44.022009375" x2="325.82116875" y2="44.06735" width="0.025" layer="94"/>
<wire x1="325.82116875" y1="44.06735" x2="325.94346875" y2="44.10235" width="0.025" layer="94"/>
<wire x1="325.94346875" y1="44.10235" x2="326.06348125" y2="44.136590625" width="0.025" layer="94"/>
<wire x1="326.06348125" y1="44.136590625" x2="326.15378125" y2="44.15916875" width="0.025" layer="94"/>
<wire x1="326.15378125" y1="44.15916875" x2="326.230359375" y2="44.17631875" width="0.025" layer="94"/>
<wire x1="326.230359375" y1="44.17631875" x2="326.35951875" y2="44.204890625" width="0.025" layer="94"/>
<wire x1="326.35951875" y1="44.204890625" x2="326.47381875" y2="44.22775" width="0.025" layer="94"/>
<wire x1="326.47381875" y1="44.22775" x2="326.622409375" y2="44.250609375" width="0.025" layer="94"/>
<wire x1="326.622409375" y1="44.250609375" x2="326.78243125" y2="44.27346875" width="0.025" layer="94"/>
<wire x1="326.78243125" y1="44.27346875" x2="326.89673125" y2="44.2849" width="0.025" layer="94"/>
<wire x1="326.89673125" y1="44.2849" x2="327.091040625" y2="44.29633125" width="0.025" layer="94"/>
<wire x1="327.091040625" y1="44.29633125" x2="327.091040625" y2="44.033440625" width="0.025" layer="94"/>
<wire x1="327.091040625" y1="44.033440625" x2="326.919590625" y2="44.033440625" width="0.025" layer="94"/>
<wire x1="326.919590625" y1="44.033440625" x2="326.793859375" y2="44.022009375" width="0.025" layer="94"/>
<wire x1="326.793859375" y1="44.022009375" x2="326.71385" y2="44.01058125" width="0.025" layer="94"/>
<wire x1="326.71385" y1="44.01058125" x2="326.576690625" y2="43.98771875" width="0.025" layer="94"/>
<wire x1="326.576690625" y1="43.98771875" x2="326.462390625" y2="43.964859375" width="0.025" layer="94"/>
<wire x1="326.462390625" y1="43.964859375" x2="326.28065" y2="43.91941875" width="0.025" layer="94"/>
<wire x1="326.28065" y1="43.91941875" x2="326.12063125" y2="43.87375" width="0.025" layer="94"/>
<wire x1="326.12063125" y1="43.87375" x2="326.02805" y2="43.850559375" width="0.025" layer="94"/>
<wire x1="326.02805" y1="43.850559375" x2="325.8566" y2="43.793409375" width="0.025" layer="94"/>
<wire x1="325.8566" y1="43.793409375" x2="325.708009375" y2="43.73396875" width="0.025" layer="94"/>
<wire x1="325.708009375" y1="43.73396875" x2="325.604" y2="43.68996875" width="0.025" layer="94"/>
<wire x1="325.604" y1="43.68996875" x2="325.536559375" y2="43.65625" width="0.025" layer="94"/>
<wire x1="325.536559375" y1="43.65625" x2="325.45655" y2="43.61053125" width="0.025" layer="94"/>
<wire x1="325.45655" y1="43.61053125" x2="325.341109375" y2="43.541190625" width="0.025" layer="94"/>
<wire x1="325.341109375" y1="43.541190625" x2="325.205090625" y2="43.450509375" width="0.025" layer="94"/>
<wire x1="325.205090625" y1="43.450509375" x2="325.11365" y2="43.38193125" width="0.025" layer="94"/>
<wire x1="325.11365" y1="43.38193125" x2="325.033640625" y2="43.32478125" width="0.025" layer="94"/>
<wire x1="325.033640625" y1="43.32478125" x2="324.86133125" y2="43.152190625" width="0.025" layer="94"/>
<wire x1="324.86133125" y1="43.152190625" x2="324.828759375" y2="43.10875" width="0.025" layer="94"/>
<wire x1="324.828759375" y1="43.10875" x2="324.75931875" y2="43.004740625" width="0.025" layer="94"/>
<wire x1="324.75931875" y1="43.004740625" x2="324.70216875" y2="42.890440625" width="0.025" layer="94"/>
<wire x1="324.70216875" y1="42.890440625" x2="324.66788125" y2="42.78756875" width="0.025" layer="94"/>
<wire x1="324.66788125" y1="42.78756875" x2="324.65645" y2="42.74185" width="0.025" layer="94"/>
<wire x1="324.65645" y1="42.74185" x2="324.633590625" y2="42.626409375" width="0.025" layer="94"/>
<wire x1="324.633590625" y1="42.626409375" x2="324.633590625" y2="42.50181875" width="0.025" layer="94"/>
<wire x1="324.633590625" y1="42.50181875" x2="324.64501875" y2="42.421809375" width="0.025" layer="94"/>
<wire x1="324.64501875" y1="42.421809375" x2="324.66788125" y2="42.33036875" width="0.025" layer="94"/>
<wire x1="324.66788125" y1="42.33036875" x2="324.690740625" y2="42.261790625" width="0.025" layer="94"/>
<wire x1="324.690740625" y1="42.261790625" x2="324.7136" y2="42.204640625" width="0.025" layer="94"/>
<wire x1="324.7136" y1="42.204640625" x2="324.747890625" y2="42.147490625" width="0.025" layer="94"/>
<wire x1="324.747890625" y1="42.147490625" x2="324.793609375" y2="42.06748125" width="0.025" layer="94"/>
<wire x1="324.793609375" y1="42.06748125" x2="324.874759375" y2="41.96346875" width="0.025" layer="94"/>
<wire x1="324.874759375" y1="41.96346875" x2="324.976490625" y2="41.861740625" width="0.025" layer="94"/>
<wire x1="324.976490625" y1="41.861740625" x2="325.090790625" y2="41.75886875" width="0.025" layer="94"/>
<wire x1="325.090790625" y1="41.75886875" x2="325.193659375" y2="41.678859375" width="0.025" layer="94"/>
<wire x1="325.193659375" y1="41.678859375" x2="325.27366875" y2="41.621709375" width="0.025" layer="94"/>
<wire x1="325.27366875" y1="41.621709375" x2="325.433690625" y2="41.53026875" width="0.025" layer="94"/>
<wire x1="325.433690625" y1="41.53026875" x2="325.58228125" y2="41.461690625" width="0.025" layer="94"/>
<wire x1="325.58228125" y1="41.461690625" x2="325.68515" y2="41.41596875" width="0.025" layer="94"/>
<wire x1="325.68515" y1="41.41596875" x2="325.84516875" y2="41.35881875" width="0.025" layer="94"/>
<wire x1="325.84516875" y1="41.35881875" x2="325.91718125" y2="41.33481875" width="0.025" layer="94"/>
<wire x1="325.91718125" y1="41.33481875" x2="325.969759375" y2="41.317290625" width="0.025" layer="94"/>
<wire x1="325.969759375" y1="41.317290625" x2="326.084059375" y2="41.279190625" width="0.025" layer="94"/>
<wire x1="326.084059375" y1="41.279190625" x2="326.11925" y2="41.437690625" width="0.025" layer="94"/>
<wire x1="326.11925" y1="41.437690625" x2="325.99261875" y2="41.49646875" width="0.025" layer="94"/>
<wire x1="325.99261875" y1="41.49646875" x2="325.91375" y2="41.53026875" width="0.025" layer="94"/>
<wire x1="325.91375" y1="41.53026875" x2="325.81088125" y2="41.575990625" width="0.025" layer="94"/>
<wire x1="325.81088125" y1="41.575990625" x2="325.652" y2="41.66678125" width="0.025" layer="94"/>
<wire x1="325.652" y1="41.66678125" x2="325.55941875" y2="41.72458125" width="0.025" layer="94"/>
<wire x1="325.55941875" y1="41.72458125" x2="325.422259375" y2="41.83888125" width="0.025" layer="94"/>
<wire x1="325.422259375" y1="41.83888125" x2="325.365109375" y2="41.89603125" width="0.025" layer="94"/>
<wire x1="325.365109375" y1="41.89603125" x2="325.307959375" y2="41.964609375" width="0.025" layer="94"/>
<wire x1="325.307959375" y1="41.964609375" x2="325.21651875" y2="42.10176875" width="0.025" layer="94"/>
<wire x1="325.21651875" y1="42.10176875" x2="325.18223125" y2="42.18178125" width="0.025" layer="94"/>
<wire x1="325.18223125" y1="42.18178125" x2="325.15936875" y2="42.250359375" width="0.025" layer="94"/>
<wire x1="325.15936875" y1="42.250359375" x2="325.147940625" y2="42.318940625" width="0.025" layer="94"/>
<wire x1="325.147940625" y1="42.318940625" x2="325.147940625" y2="42.478959375" width="0.025" layer="94"/>
<wire x1="325.147940625" y1="42.478959375" x2="325.15936875" y2="42.547540625" width="0.025" layer="94"/>
<wire x1="325.15936875" y1="42.547540625" x2="325.1708" y2="42.593259375" width="0.025" layer="94"/>
<wire x1="325.1708" y1="42.593259375" x2="325.193659375" y2="42.650409375" width="0.025" layer="94"/>
<wire x1="325.193659375" y1="42.650409375" x2="325.22795" y2="42.718990625" width="0.025" layer="94"/>
<wire x1="325.22795" y1="42.718990625" x2="325.250809375" y2="42.75328125" width="0.025" layer="94"/>
<wire x1="325.250809375" y1="42.75328125" x2="325.2851" y2="42.799" width="0.025" layer="94"/>
<wire x1="325.2851" y1="42.799" x2="325.44511875" y2="42.95901875" width="0.025" layer="94"/>
<wire x1="325.44511875" y1="42.95901875" x2="325.5377" y2="43.028359375" width="0.025" layer="94"/>
<wire x1="325.5377" y1="43.028359375" x2="325.605140625" y2="43.07331875" width="0.025" layer="94"/>
<wire x1="325.605140625" y1="43.07331875" x2="325.68858125" y2="43.123609375" width="0.025" layer="94"/>
<wire x1="325.68858125" y1="43.123609375" x2="325.776590625" y2="43.176190625" width="0.025" layer="94"/>
<wire x1="325.776590625" y1="43.176190625" x2="325.84516875" y2="43.21048125" width="0.025" layer="94"/>
<wire x1="325.84516875" y1="43.21048125" x2="326.005190625" y2="43.279059375" width="0.025" layer="94"/>
<wire x1="326.005190625" y1="43.279059375" x2="326.09663125" y2="43.31335" width="0.025" layer="94"/>
<wire x1="326.09663125" y1="43.31335" x2="326.37095" y2="43.404790625" width="0.025" layer="94"/>
<wire x1="326.37095" y1="43.404790625" x2="326.49668125" y2="43.43908125" width="0.025" layer="94"/>
<wire x1="326.49668125" y1="43.43908125" x2="326.56411875" y2="43.454640625" width="0.025" layer="94"/>
<wire x1="326.56411875" y1="43.454640625" x2="326.64526875" y2="43.47336875" width="0.025" layer="94"/>
<wire x1="326.64526875" y1="43.47336875" x2="326.75956875" y2="43.49623125" width="0.025" layer="94"/>
<wire x1="326.75956875" y1="43.49623125" x2="326.93101875" y2="43.519090625" width="0.025" layer="94"/>
<wire x1="326.93101875" y1="43.519090625" x2="327.06818125" y2="43.53051875" width="0.025" layer="94"/>
<wire x1="327.06818125" y1="43.53051875" x2="327.205340625" y2="43.53051875" width="0.025" layer="94"/>
<wire x1="327.205340625" y1="43.53051875" x2="327.205340625" y2="43.35906875" width="0.025" layer="94"/>
<wire x1="327.205340625" y1="43.35906875" x2="327.17105" y2="43.35906875" width="0.025" layer="94"/>
<wire x1="327.17105" y1="43.35906875" x2="327.04531875" y2="43.347640625" width="0.025" layer="94"/>
<wire x1="327.04531875" y1="43.347640625" x2="326.95388125" y2="43.336209375" width="0.025" layer="94"/>
<wire x1="326.95388125" y1="43.336209375" x2="326.81671875" y2="43.31335" width="0.025" layer="94"/>
<wire x1="326.81671875" y1="43.31335" x2="326.71385" y2="43.290490625" width="0.025" layer="94"/>
<wire x1="326.71385" y1="43.290490625" x2="326.565259375" y2="43.2562" width="0.025" layer="94"/>
<wire x1="326.565259375" y1="43.2562" x2="326.48525" y2="43.233340625" width="0.025" layer="94"/>
<wire x1="326.48525" y1="43.233340625" x2="326.41666875" y2="43.21048125" width="0.025" layer="94"/>
<wire x1="326.41666875" y1="43.21048125" x2="326.32523125" y2="43.176190625" width="0.025" layer="94"/>
<wire x1="326.32523125" y1="43.176190625" x2="326.21206875" y2="43.13093125" width="0.025" layer="94"/>
<wire x1="326.21206875" y1="43.13093125" x2="326.005190625" y2="43.0276" width="0.025" layer="94"/>
<wire x1="326.005190625" y1="43.0276" x2="325.948040625" y2="42.993309375" width="0.025" layer="94"/>
<wire x1="325.948040625" y1="42.993309375" x2="325.84516875" y2="42.92473125" width="0.025" layer="94"/>
<wire x1="325.84516875" y1="42.92473125" x2="325.79945" y2="42.890440625" width="0.025" layer="94"/>
<wire x1="325.79945" y1="42.890440625" x2="325.7423" y2="42.84471875" width="0.025" layer="94"/>
<wire x1="325.7423" y1="42.84471875" x2="325.708009375" y2="42.81043125" width="0.025" layer="94"/>
<wire x1="325.708009375" y1="42.81043125" x2="325.63943125" y2="42.73041875" width="0.025" layer="94"/>
<wire x1="325.63943125" y1="42.73041875" x2="325.593709375" y2="42.661840625" width="0.025" layer="94"/>
<wire x1="325.593709375" y1="42.661840625" x2="325.55941875" y2="42.593259375" width="0.025" layer="94"/>
<wire x1="325.55941875" y1="42.593259375" x2="325.536559375" y2="42.536109375" width="0.025" layer="94"/>
<wire x1="325.536559375" y1="42.536109375" x2="325.5137" y2="42.4561" width="0.025" layer="94"/>
<wire x1="325.5137" y1="42.4561" x2="325.50226875" y2="42.39895" width="0.025" layer="94"/>
<wire x1="325.50226875" y1="42.39895" x2="325.50226875" y2="42.261790625" width="0.025" layer="94"/>
<wire x1="325.50226875" y1="42.261790625" x2="325.5137" y2="42.204640625" width="0.025" layer="94"/>
<wire x1="325.5137" y1="42.204640625" x2="325.52513125" y2="42.15891875" width="0.025" layer="94"/>
<wire x1="325.52513125" y1="42.15891875" x2="325.547609375" y2="42.09148125" width="0.025" layer="94"/>
<wire x1="325.547609375" y1="42.09148125" x2="325.57085" y2="42.04461875" width="0.025" layer="94"/>
<wire x1="325.57085" y1="42.04461875" x2="325.605140625" y2="41.98746875" width="0.025" layer="94"/>
<wire x1="325.605140625" y1="41.98746875" x2="325.67371875" y2="41.89603125" width="0.025" layer="94"/>
<wire x1="325.67371875" y1="41.89603125" x2="325.75373125" y2="41.81601875" width="0.025" layer="94"/>
<wire x1="325.75373125" y1="41.81601875" x2="325.84516875" y2="41.747440625" width="0.025" layer="94"/>
<wire x1="325.84516875" y1="41.747440625" x2="325.95833125" y2="41.67955" width="0.025" layer="94"/>
<wire x1="325.95833125" y1="41.67955" x2="326.04061875" y2="41.63256875" width="0.025" layer="94"/>
<wire x1="326.04061875" y1="41.63256875" x2="326.108059375" y2="41.59885" width="0.025" layer="94"/>
<wire x1="326.108059375" y1="41.59885" x2="326.165509375" y2="41.57713125" width="0.025" layer="94"/>
<wire x1="326.165509375" y1="41.57713125" x2="326.35951875" y2="42.307509375" width="0.025" layer="94"/>
<wire x1="326.35951875" y1="42.307509375" x2="328.8284" y2="42.307509375" width="0.025" layer="94"/>
<wire x1="328.8284" y1="42.307509375" x2="329.022709375" y2="41.575990625" width="0.025" layer="94"/>
<wire x1="329.022709375" y1="41.575990625" x2="329.087859375" y2="41.597140625" width="0.025" layer="94"/>
<wire x1="329.087859375" y1="41.597140625" x2="329.14158125" y2="41.624" width="0.025" layer="94"/>
<wire x1="329.14158125" y1="41.624" x2="329.25245" y2="41.67961875" width="0.025" layer="94"/>
<wire x1="329.25245" y1="41.67961875" x2="329.42161875" y2="41.7924" width="0.025" layer="94"/>
<wire x1="329.42161875" y1="41.7924" x2="329.548490625" y2="41.918890625" width="0.025" layer="94"/>
<wire x1="329.548490625" y1="41.918890625" x2="329.6285" y2="42.033190625" width="0.025" layer="94"/>
<wire x1="329.6285" y1="42.033190625" x2="329.662790625" y2="42.1132" width="0.025" layer="94"/>
<wire x1="329.662790625" y1="42.1132" x2="329.68593125" y2="42.18291875" width="0.025" layer="94"/>
<wire x1="329.68593125" y1="42.18291875" x2="329.69708125" y2="42.2275" width="0.025" layer="94"/>
<wire x1="329.69708125" y1="42.2275" x2="329.708509375" y2="42.307509375" width="0.025" layer="94"/>
<wire x1="329.708509375" y1="42.307509375" x2="329.708509375" y2="42.35323125" width="0.025" layer="94"/>
<wire x1="329.708509375" y1="42.35323125" x2="329.69708125" y2="42.433240625" width="0.025" layer="94"/>
<wire x1="329.69708125" y1="42.433240625" x2="329.67421875" y2="42.52468125" width="0.025" layer="94"/>
<wire x1="329.67421875" y1="42.52468125" x2="329.651359375" y2="42.58183125" width="0.025" layer="94"/>
<wire x1="329.651359375" y1="42.58183125" x2="329.61706875" y2="42.650409375" width="0.025" layer="94"/>
<wire x1="329.61706875" y1="42.650409375" x2="329.570490625" y2="42.72013125" width="0.025" layer="94"/>
<wire x1="329.570490625" y1="42.72013125" x2="329.537059375" y2="42.764709375" width="0.025" layer="94"/>
<wire x1="329.537059375" y1="42.764709375" x2="329.43533125" y2="42.866440625" width="0.025" layer="94"/>
<wire x1="329.43533125" y1="42.866440625" x2="329.377040625" y2="42.9133" width="0.025" layer="94"/>
<wire x1="329.377040625" y1="42.9133" x2="329.33131875" y2="42.947590625" width="0.025" layer="94"/>
<wire x1="329.33131875" y1="42.947590625" x2="329.21701875" y2="43.01616875" width="0.025" layer="94"/>
<wire x1="329.21701875" y1="43.01616875" x2="329.137009375" y2="43.061890625" width="0.025" layer="94"/>
<wire x1="329.137009375" y1="43.061890625" x2="329.06843125" y2="43.09618125" width="0.025" layer="94"/>
<wire x1="329.06843125" y1="43.09618125" x2="328.965559375" y2="43.1419" width="0.025" layer="94"/>
<wire x1="328.965559375" y1="43.1419" x2="328.908409375" y2="43.164759375" width="0.025" layer="94"/>
<wire x1="328.908409375" y1="43.164759375" x2="328.81696875" y2="43.19905" width="0.025" layer="94"/>
<wire x1="328.81696875" y1="43.19905" x2="328.7141" y2="43.233340625" width="0.025" layer="94"/>
<wire x1="328.7141" y1="43.233340625" x2="328.634090625" y2="43.2562" width="0.025" layer="94"/>
<wire x1="328.634090625" y1="43.2562" x2="328.54265" y2="43.279059375" width="0.025" layer="94"/>
<wire x1="328.54265" y1="43.279059375" x2="328.43978125" y2="43.30191875" width="0.025" layer="94"/>
<wire x1="328.43978125" y1="43.30191875" x2="328.32661875" y2="43.32478125" width="0.025" layer="94"/>
<wire x1="328.32661875" y1="43.32478125" x2="328.255759375" y2="43.336209375" width="0.025" layer="94"/>
<wire x1="328.255759375" y1="43.336209375" x2="328.15516875" y2="43.347640625" width="0.025" layer="94"/>
<wire x1="328.15516875" y1="43.347640625" x2="328.075159375" y2="43.353709375" width="0.025" layer="94"/>
<wire x1="328.075159375" y1="43.353709375" x2="328.00658125" y2="43.35906875" width="0.025" layer="94"/>
<wire x1="328.00658125" y1="43.35906875" x2="328.005440625" y2="43.53051875" width="0.025" layer="94"/>
<wire x1="328.005440625" y1="43.53051875" x2="328.176890625" y2="43.53051875" width="0.025" layer="94"/>
<wire x1="328.176890625" y1="43.53051875" x2="328.291190625" y2="43.519090625" width="0.025" layer="94"/>
<wire x1="328.291190625" y1="43.519090625" x2="328.3712" y2="43.507659375" width="0.025" layer="94"/>
<wire x1="328.3712" y1="43.507659375" x2="328.43978125" y2="43.49623125" width="0.025" layer="94"/>
<wire x1="328.43978125" y1="43.49623125" x2="328.61123125" y2="43.461940625" width="0.025" layer="94"/>
<wire x1="328.61123125" y1="43.461940625" x2="328.70266875" y2="43.43908125" width="0.025" layer="94"/>
<wire x1="328.70266875" y1="43.43908125" x2="328.78268125" y2="43.41621875" width="0.025" layer="94"/>
<wire x1="328.78268125" y1="43.41621875" x2="328.892409375" y2="43.38306875" width="0.025" layer="94"/>
<wire x1="328.892409375" y1="43.38306875" x2="329.010140625" y2="43.34798125" width="0.025" layer="94"/>
<wire x1="329.010140625" y1="43.34798125" x2="329.079859375" y2="43.32191875" width="0.025" layer="94"/>
<wire x1="329.079859375" y1="43.32191875" x2="329.194159375" y2="43.279059375" width="0.025" layer="94"/>
<wire x1="329.194159375" y1="43.279059375" x2="329.3096" y2="43.23276875" width="0.025" layer="94"/>
<wire x1="329.3096" y1="43.23276875" x2="329.44561875" y2="43.164759375" width="0.025" layer="94"/>
<wire x1="329.44561875" y1="43.164759375" x2="329.50276875" y2="43.13046875" width="0.025" layer="94"/>
<wire x1="329.50276875" y1="43.13046875" x2="329.548490625" y2="43.107609375" width="0.025" layer="94"/>
<wire x1="329.548490625" y1="43.107609375" x2="329.61706875" y2="43.061890625" width="0.025" layer="94"/>
<wire x1="329.61706875" y1="43.061890625" x2="329.75423125" y2="42.95901875" width="0.025" layer="94"/>
<wire x1="329.75423125" y1="42.95901875" x2="329.81138125" y2="42.9133" width="0.025" layer="94"/>
<wire x1="329.891390625" y1="42.833290625" x2="329.89253125" y2="42.83215" width="0.025" layer="94"/>
<wire x1="329.89253125" y1="42.83215" x2="329.937109375" y2="42.776140625" width="0.025" layer="94"/>
<wire x1="329.937109375" y1="42.776140625" x2="329.95996875" y2="42.74185" width="0.025" layer="94"/>
<wire x1="329.95996875" y1="42.74185" x2="329.9954" y2="42.682409375" width="0.025" layer="94"/>
<wire x1="329.9954" y1="42.682409375" x2="330.03998125" y2="42.5704" width="0.025" layer="94"/>
<wire x1="330.03998125" y1="42.5704" x2="330.051409375" y2="42.512109375" width="0.025" layer="94"/>
<wire x1="330.051409375" y1="42.512109375" x2="330.062840625" y2="42.421809375" width="0.025" layer="94"/>
<wire x1="330.062840625" y1="42.421809375" x2="330.062840625" y2="42.35323125" width="0.025" layer="94"/>
<wire x1="330.062840625" y1="42.35323125" x2="330.051409375" y2="42.261790625" width="0.025" layer="94"/>
<wire x1="330.051409375" y1="42.261790625" x2="330.03998125" y2="42.21606875" width="0.025" layer="94"/>
<wire x1="330.03998125" y1="42.21606875" x2="330.01711875" y2="42.15891875" width="0.025" layer="94"/>
<wire x1="330.01711875" y1="42.15891875" x2="329.98283125" y2="42.090340625" width="0.025" layer="94"/>
<wire x1="329.98283125" y1="42.090340625" x2="329.95996875" y2="42.05605" width="0.025" layer="94"/>
<wire x1="329.95996875" y1="42.05605" x2="329.90281875" y2="41.976040625" width="0.025" layer="94"/>
<wire x1="329.90281875" y1="41.976040625" x2="329.834240625" y2="41.89603125" width="0.025" layer="94"/>
<wire x1="329.834240625" y1="41.89603125" x2="329.7428" y2="41.804590625" width="0.025" layer="94"/>
<wire x1="329.7428" y1="41.804590625" x2="329.6285" y2="41.71315" width="0.025" layer="94"/>
<wire x1="329.6285" y1="41.71315" x2="329.55878125" y2="41.666790625" width="0.025" layer="94"/>
<wire x1="329.55878125" y1="41.666790625" x2="329.45705" y2="41.61028125" width="0.025" layer="94"/>
<wire x1="329.45705" y1="41.61028125" x2="329.319890625" y2="41.5417" width="0.025" layer="94"/>
<wire x1="329.319890625" y1="41.5417" x2="329.1633" y2="41.474590625" width="0.025" layer="94"/>
<wire x1="329.1633" y1="41.474590625" x2="329.06843125" y2="41.43883125" width="0.025" layer="94"/>
<wire x1="329.06843125" y1="41.43883125" x2="329.10271875" y2="41.278809375" width="0.025" layer="94"/>
<wire x1="329.10271875" y1="41.278809375" x2="329.137009375" y2="41.290240625" width="0.025" layer="94"/>
<wire x1="329.137009375" y1="41.290240625" x2="329.21701875" y2="41.3131" width="0.025" layer="94"/>
<wire x1="329.21701875" y1="41.3131" x2="329.3999" y2="41.37025" width="0.025" layer="94"/>
<wire x1="329.3999" y1="41.37025" x2="329.57135" y2="41.43883125" width="0.025" layer="94"/>
<wire x1="329.57135" y1="41.43883125" x2="329.834240625" y2="41.564559375" width="0.025" layer="94"/>
<wire x1="329.834240625" y1="41.564559375" x2="329.891390625" y2="41.59885" width="0.025" layer="94"/>
<wire x1="329.891390625" y1="41.59885" x2="329.994259375" y2="41.66743125" width="0.025" layer="94"/>
<wire x1="329.994259375" y1="41.66743125" x2="330.051409375" y2="41.71315" width="0.025" layer="94"/>
<wire x1="330.051409375" y1="41.71315" x2="330.14285" y2="41.78173125" width="0.025" layer="94"/>
<wire x1="330.14285" y1="41.78173125" x2="330.177140625" y2="41.81601875" width="0.025" layer="94"/>
<wire x1="330.177140625" y1="41.81601875" x2="330.25715" y2="41.8846" width="0.025" layer="94"/>
<wire x1="330.25715" y1="41.8846" x2="330.30286875" y2="41.93031875" width="0.025" layer="94"/>
<wire x1="330.30286875" y1="41.93031875" x2="330.37145" y2="42.01033125" width="0.025" layer="94"/>
<wire x1="330.37145" y1="42.01033125" x2="330.405740625" y2="42.05605" width="0.025" layer="94"/>
<wire x1="330.405740625" y1="42.05605" x2="330.47478125" y2="42.171490625" width="0.025" layer="94"/>
<wire x1="330.47478125" y1="42.171490625" x2="330.520040625" y2="42.28465" width="0.025" layer="94"/>
<wire x1="330.520040625" y1="42.28465" x2="330.54251875" y2="42.352090625" width="0.025" layer="94"/>
<wire x1="330.54251875" y1="42.352090625" x2="330.565759375" y2="42.478959375" width="0.025" layer="94"/>
<wire x1="330.565759375" y1="42.478959375" x2="330.565759375" y2="42.650409375" width="0.025" layer="94"/>
<wire x1="330.565759375" y1="42.650409375" x2="330.55433125" y2="42.718990625" width="0.025" layer="94"/>
<wire x1="330.55433125" y1="42.718990625" x2="330.5429" y2="42.776140625" width="0.025" layer="94"/>
<wire x1="330.5429" y1="42.776140625" x2="330.520040625" y2="42.84471875" width="0.025" layer="94"/>
<wire x1="330.520040625" y1="42.84471875" x2="330.49718125" y2="42.90186875" width="0.025" layer="94"/>
<wire x1="330.49718125" y1="42.90186875" x2="330.451459375" y2="42.993309375" width="0.025" layer="94"/>
<wire x1="330.451459375" y1="42.993309375" x2="330.41716875" y2="43.050459375" width="0.025" layer="94"/>
<wire x1="330.41716875" y1="43.050459375" x2="330.394309375" y2="43.08475" width="0.025" layer="94"/>
<wire x1="330.394309375" y1="43.08475" x2="330.36001875" y2="43.13046875" width="0.025" layer="94"/>
<wire x1="330.36001875" y1="43.13046875" x2="330.291440625" y2="43.21048125" width="0.025" layer="94"/>
<wire x1="330.291440625" y1="43.21048125" x2="330.234290625" y2="43.26763125" width="0.025" layer="94"/>
<wire x1="330.234290625" y1="43.26763125" x2="330.15428125" y2="43.336209375" width="0.025" layer="94"/>
<wire x1="330.15428125" y1="43.336209375" x2="330.01711875" y2="43.43908125" width="0.025" layer="94"/>
<wire x1="330.01711875" y1="43.43908125" x2="329.879959375" y2="43.53051875" width="0.025" layer="94"/>
<wire x1="329.879959375" y1="43.53051875" x2="329.765659375" y2="43.5991" width="0.025" layer="94"/>
<wire x1="329.765659375" y1="43.5991" x2="329.68565" y2="43.64481875" width="0.025" layer="94"/>
<wire x1="329.68565" y1="43.64481875" x2="329.52563125" y2="43.72483125" width="0.025" layer="94"/>
<wire x1="329.52563125" y1="43.72483125" x2="329.44561875" y2="43.75911875" width="0.025" layer="94"/>
<wire x1="329.44561875" y1="43.75911875" x2="329.1713" y2="43.850559375" width="0.025" layer="94"/>
<wire x1="329.1713" y1="43.850559375" x2="329.04556875" y2="43.88485" width="0.025" layer="94"/>
<wire x1="329.04556875" y1="43.88485" x2="328.965559375" y2="43.907709375" width="0.025" layer="94"/>
<wire x1="328.965559375" y1="43.907709375" x2="328.691240625" y2="43.976290625" width="0.025" layer="94"/>
<wire x1="328.691240625" y1="43.976290625" x2="328.634090625" y2="43.98771875" width="0.025" layer="94"/>
<wire x1="328.634090625" y1="43.98771875" x2="328.565509375" y2="43.99915" width="0.025" layer="94"/>
<wire x1="328.565509375" y1="43.99915" x2="328.394059375" y2="44.022009375" width="0.025" layer="94"/>
<wire x1="328.394059375" y1="44.022009375" x2="328.2569" y2="44.033440625" width="0.025" layer="94"/>
<wire x1="328.2569" y1="44.033440625" x2="328.119740625" y2="44.033440625" width="0.025" layer="94"/>
<wire x1="328.119740625" y1="44.033440625" x2="328.119740625" y2="44.29633125" width="0.025" layer="94"/>
<wire x1="328.119740625" y1="44.29633125" x2="328.165459375" y2="44.29633125" width="0.025" layer="94"/>
<wire x1="328.165459375" y1="44.29633125" x2="328.32548125" y2="44.2849" width="0.025" layer="94"/>
<wire x1="328.32548125" y1="44.2849" x2="328.42835" y2="44.27346875" width="0.025" layer="94"/>
<wire x1="328.42835" y1="44.27346875" x2="328.519790625" y2="44.262040625" width="0.025" layer="94"/>
<wire x1="328.519790625" y1="44.262040625" x2="328.5998" y2="44.250609375" width="0.025" layer="94"/>
<wire x1="328.5998" y1="44.250609375" x2="328.72553125" y2="44.22775" width="0.025" layer="94"/>
<wire x1="328.72553125" y1="44.22775" x2="328.952990625" y2="44.18203125" width="0.025" layer="94"/>
<wire x1="328.952990625" y1="44.18203125" x2="329.22845" y2="44.11345" width="0.025" layer="94"/>
<wire x1="329.22845" y1="44.11345" x2="329.38846875" y2="44.06773125" width="0.025" layer="94"/>
<wire x1="329.38846875" y1="44.06773125" x2="329.52563125" y2="44.022009375" width="0.025" layer="94"/>
<wire x1="329.52563125" y1="44.022009375" x2="329.69708125" y2="43.95343125" width="0.025" layer="94"/>
<wire x1="329.69708125" y1="43.95343125" x2="329.90281875" y2="43.861990625" width="0.025" layer="94"/>
<wire x1="329.90281875" y1="43.861990625" x2="330.04111875" y2="43.79271875" width="0.025" layer="94"/>
<wire x1="330.04111875" y1="43.79271875" x2="330.14285" y2="43.736259375" width="0.025" layer="94"/>
<wire x1="330.14285" y1="43.736259375" x2="330.2" y2="43.70196875" width="0.025" layer="94"/>
<wire x1="330.2" y1="43.70196875" x2="330.337159375" y2="43.61053125" width="0.025" layer="94"/>
<wire x1="330.337159375" y1="43.61053125" x2="330.38288125" y2="43.576240625" width="0.025" layer="94"/>
<wire x1="330.38288125" y1="43.576240625" x2="330.55433125" y2="43.43908125" width="0.025" layer="94"/>
<wire x1="330.55433125" y1="43.43908125" x2="330.601190625" y2="43.39221875" width="0.025" layer="94"/>
<wire x1="330.601190625" y1="43.39221875" x2="330.644140625" y2="43.34193125" width="0.025" layer="94"/>
<wire x1="330.644140625" y1="43.34193125" x2="330.70291875" y2="43.279059375" width="0.025" layer="94"/>
<wire x1="330.70291875" y1="43.279059375" x2="330.7715" y2="43.18761875" width="0.025" layer="94"/>
<wire x1="330.7715" y1="43.18761875" x2="330.81721875" y2="43.119040625" width="0.025" layer="94"/>
<wire x1="330.81721875" y1="43.119040625" x2="330.862940625" y2="43.0276" width="0.025" layer="94"/>
<wire x1="330.862940625" y1="43.0276" x2="330.8858" y2="42.95901875" width="0.025" layer="94"/>
<wire x1="330.8858" y1="42.95901875" x2="330.908659375" y2="42.879009375" width="0.025" layer="94"/>
<wire x1="330.908659375" y1="42.879009375" x2="330.920090625" y2="42.81043125" width="0.025" layer="94"/>
<wire x1="330.920090625" y1="42.81043125" x2="330.93151875" y2="42.69613125" width="0.025" layer="94"/>
<wire x1="330.93151875" y1="42.69613125" x2="330.93151875" y2="42.63898125" width="0.025" layer="94"/>
<wire x1="330.93151875" y1="42.63898125" x2="330.920090625" y2="42.536109375" width="0.025" layer="94"/>
<wire x1="330.920090625" y1="42.536109375" x2="330.908659375" y2="42.478959375" width="0.025" layer="94"/>
<wire x1="330.908659375" y1="42.478959375" x2="330.8858" y2="42.38751875" width="0.025" layer="94"/>
<wire x1="330.8858" y1="42.38751875" x2="330.87436875" y2="42.35323125" width="0.025" layer="94"/>
<wire x1="330.87436875" y1="42.35323125" x2="330.82865" y2="42.23893125" width="0.025" layer="94"/>
<wire x1="330.82865" y1="42.23893125" x2="330.8051" y2="42.19206875" width="0.025" layer="94"/>
<wire x1="330.8051" y1="42.19206875" x2="330.770640625" y2="42.13491875" width="0.025" layer="94"/>
<wire x1="330.770640625" y1="42.13491875" x2="330.70291875" y2="42.04461875" width="0.025" layer="94"/>
<wire x1="330.70291875" y1="42.04461875" x2="330.6572" y2="41.98746875" width="0.025" layer="94"/>
<wire x1="330.6572" y1="41.98746875" x2="330.577190625" y2="41.907459375" width="0.025" layer="94"/>
<wire x1="330.577190625" y1="41.907459375" x2="330.49718125" y2="41.83888125" width="0.025" layer="94"/>
<wire x1="330.49718125" y1="41.83888125" x2="330.44003125" y2="41.793159375" width="0.025" layer="94"/>
<wire x1="330.44003125" y1="41.793159375" x2="330.394309375" y2="41.75886875" width="0.025" layer="94"/>
<wire x1="330.394309375" y1="41.75886875" x2="330.337159375" y2="41.71315" width="0.025" layer="94"/>
<wire x1="330.337159375" y1="41.71315" x2="330.291440625" y2="41.678859375" width="0.025" layer="94"/>
<wire x1="330.291440625" y1="41.678859375" x2="330.18856875" y2="41.61028125" width="0.025" layer="94"/>
<wire x1="330.18856875" y1="41.61028125" x2="330.14285" y2="41.575990625" width="0.025" layer="94"/>
<wire x1="330.14285" y1="41.575990625" x2="330.0857" y2="41.5417" width="0.025" layer="94"/>
<wire x1="330.0857" y1="41.5417" x2="329.834240625" y2="41.41596875" width="0.025" layer="94"/>
<wire x1="329.834240625" y1="41.41596875" x2="329.719940625" y2="41.37025" width="0.025" layer="94"/>
<wire x1="329.719940625" y1="41.37025" x2="329.594209375" y2="41.32453125" width="0.025" layer="94"/>
<wire x1="329.594209375" y1="41.32453125" x2="329.44333125" y2="41.278109375" width="0.025" layer="94"/>
<wire x1="329.44333125" y1="41.278109375" x2="329.29703125" y2="41.233090625" width="0.025" layer="94"/>
<wire x1="329.29703125" y1="41.233090625" x2="329.137009375" y2="41.18736875" width="0.025" layer="94"/>
<wire x1="329.137009375" y1="41.18736875" x2="329.1713" y2="41.03878125" width="0.025" layer="94"/>
<wire x1="329.1713" y1="41.03878125" x2="329.26616875" y2="41.059640625" width="0.025" layer="94"/>
<wire x1="329.26616875" y1="41.059640625" x2="329.50163125" y2="41.1185" width="0.025" layer="94"/>
<wire x1="329.50163125" y1="41.1185" x2="329.6045" y2="41.147859375" width="0.025" layer="94"/>
<wire x1="329.6045" y1="41.147859375" x2="329.743940625" y2="41.18775" width="0.025" layer="94"/>
<wire x1="329.743940625" y1="41.18775" x2="329.948540625" y2="41.25595" width="0.025" layer="94"/>
<wire x1="329.948540625" y1="41.25595" x2="330.03998125" y2="41.290240625" width="0.025" layer="94"/>
<wire x1="330.03998125" y1="41.290240625" x2="330.21143125" y2="41.35881875" width="0.025" layer="94"/>
<wire x1="330.21143125" y1="41.35881875" x2="330.48575" y2="41.49598125" width="0.025" layer="94"/>
<wire x1="330.48575" y1="41.49598125" x2="330.60005" y2="41.564559375" width="0.025" layer="94"/>
<wire x1="330.60005" y1="41.564559375" x2="330.69035" y2="41.62101875" width="0.025" layer="94"/>
<wire x1="330.69035" y1="41.62101875" x2="330.737209375" y2="41.656" width="0.025" layer="94"/>
<wire x1="330.737209375" y1="41.656" x2="330.805790625" y2="41.70171875" width="0.025" layer="94"/>
<wire x1="330.805790625" y1="41.70171875" x2="330.851509375" y2="41.736009375" width="0.025" layer="94"/>
<wire x1="330.851509375" y1="41.736009375" x2="331.080109375" y2="41.918890625" width="0.025" layer="94"/>
<wire x1="331.080109375" y1="41.918890625" x2="331.2287" y2="42.06748125" width="0.025" layer="94"/>
<wire x1="331.2287" y1="42.06748125" x2="331.308709375" y2="42.17035" width="0.025" layer="94"/>
<wire x1="331.308709375" y1="42.17035" x2="331.33156875" y2="42.204640625" width="0.025" layer="94"/>
<wire x1="331.33156875" y1="42.204640625" x2="331.365859375" y2="42.261790625" width="0.025" layer="94"/>
<wire x1="331.365859375" y1="42.261790625" x2="331.434440625" y2="42.39895" width="0.025" layer="94"/>
<wire x1="331.434440625" y1="42.39895" x2="331.4573" y2="42.4561" width="0.025" layer="94"/>
<wire x1="331.4573" y1="42.4561" x2="331.46873125" y2="42.490390625" width="0.025" layer="94"/>
<wire x1="331.46873125" y1="42.490390625" x2="331.491590625" y2="42.58183125" width="0.025" layer="94"/>
<wire x1="331.491590625" y1="42.58183125" x2="331.50301875" y2="42.650409375" width="0.025" layer="94"/>
<wire x1="331.50301875" y1="42.650409375" x2="331.51445" y2="42.764709375" width="0.025" layer="94"/>
<wire x1="331.51445" y1="42.764709375" x2="331.51445" y2="42.84471875" width="0.025" layer="94"/>
<wire x1="331.51445" y1="42.84471875" x2="331.50988125" y2="42.89615" width="0.025" layer="94"/>
<wire x1="331.50988125" y1="42.89615" x2="331.50301875" y2="42.97045" width="0.025" layer="94"/>
<wire x1="331.50301875" y1="42.97045" x2="331.480159375" y2="43.08475" width="0.025" layer="94"/>
<wire x1="331.480159375" y1="43.08475" x2="331.434440625" y2="43.221909375" width="0.025" layer="94"/>
<wire x1="331.434440625" y1="43.221909375" x2="331.41158125" y2="43.279059375" width="0.025" layer="94"/>
<wire x1="331.41158125" y1="43.279059375" x2="331.36643125" y2="43.369359375" width="0.025" layer="94"/>
<wire x1="331.36643125" y1="43.369359375" x2="331.27518125" y2="43.50651875" width="0.025" layer="94"/>
<wire x1="331.27518125" y1="43.50651875" x2="331.24013125" y2="43.55338125" width="0.025" layer="94"/>
<wire x1="331.24013125" y1="43.55338125" x2="331.194409375" y2="43.61053125" width="0.025" layer="94"/>
<wire x1="331.194409375" y1="43.61053125" x2="330.98866875" y2="43.81626875" width="0.025" layer="94"/>
<wire x1="330.98866875" y1="43.81626875" x2="330.920090625" y2="43.87341875" width="0.025" layer="94"/>
<wire x1="330.920090625" y1="43.87341875" x2="330.862940625" y2="43.919140625" width="0.025" layer="94"/>
<wire x1="330.862940625" y1="43.919140625" x2="330.7715" y2="43.98771875" width="0.025" layer="94"/>
<wire x1="330.7715" y1="43.98771875" x2="330.66863125" y2="44.0563" width="0.025" layer="94"/>
<wire x1="330.66863125" y1="44.0563" x2="330.610340625" y2="44.09135" width="0.025" layer="94"/>
<wire x1="330.610340625" y1="44.09135" x2="330.577190625" y2="44.11345" width="0.025" layer="94"/>
<wire x1="330.577190625" y1="44.11345" x2="330.520040625" y2="44.147740625" width="0.025" layer="94"/>
<wire x1="330.520040625" y1="44.147740625" x2="330.24571875" y2="44.2849" width="0.025" layer="94"/>
<wire x1="330.24571875" y1="44.2849" x2="329.95768125" y2="44.399959375" width="0.025" layer="94"/>
<wire x1="329.95768125" y1="44.399959375" x2="329.651359375" y2="44.50206875" width="0.025" layer="94"/>
<wire x1="329.651359375" y1="44.50206875" x2="329.4902" y2="44.54806875" width="0.025" layer="94"/>
<wire x1="329.4902" y1="44.54806875" x2="329.21701875" y2="44.61636875" width="0.025" layer="94"/>
<wire x1="329.21701875" y1="44.61636875" x2="328.87411875" y2="44.68495" width="0.025" layer="94"/>
<wire x1="328.87411875" y1="44.68495" x2="328.736959375" y2="44.707809375" width="0.025" layer="94"/>
<wire x1="328.736959375" y1="44.707809375" x2="328.65695" y2="44.719240625" width="0.025" layer="94"/>
<wire x1="328.65695" y1="44.719240625" x2="328.47406875" y2="44.7421" width="0.025" layer="94"/>
<wire x1="328.47406875" y1="44.7421" x2="328.370059375" y2="44.75353125" width="0.025" layer="94"/>
<wire x1="328.370059375" y1="44.75353125" x2="328.222609375" y2="44.764959375" width="0.025" layer="94"/>
<wire x1="328.222609375" y1="44.764959375" x2="328.222609375" y2="45.03928125" width="0.025" layer="94"/>
<wire x1="328.222609375" y1="45.03928125" x2="328.279759375" y2="45.03928125" width="0.025" layer="94"/>
<wire x1="328.279759375" y1="45.03928125" x2="328.42835" y2="45.02785" width="0.025" layer="94"/>
<wire x1="328.42835" y1="45.02785" x2="328.55408125" y2="45.01641875" width="0.025" layer="94"/>
<wire x1="328.55408125" y1="45.01641875" x2="328.65695" y2="45.004990625" width="0.025" layer="94"/>
<wire x1="328.65695" y1="45.004990625" x2="328.736959375" y2="44.994990625" width="0.025" layer="94"/>
<wire x1="328.736959375" y1="44.994990625" x2="328.79983125" y2="44.98713125" width="0.025" layer="94"/>
<wire x1="328.79983125" y1="44.98713125" x2="328.93126875" y2="44.9707" width="0.025" layer="94"/>
<wire x1="328.93126875" y1="44.9707" x2="329.137009375" y2="44.936409375" width="0.025" layer="94"/>
<wire x1="329.137009375" y1="44.936409375" x2="329.251309375" y2="44.91355" width="0.025" layer="94"/>
<wire x1="329.251309375" y1="44.91355" x2="329.29703125" y2="44.90211875" width="0.025" layer="94"/>
<wire x1="329.29703125" y1="44.90211875" x2="329.3999" y2="44.879259375" width="0.025" layer="94"/>
<wire x1="329.3999" y1="44.879259375" x2="329.45705" y2="44.86783125" width="0.025" layer="94"/>
<wire x1="329.45705" y1="44.86783125" x2="329.63993125" y2="44.822109375" width="0.025" layer="94"/>
<wire x1="329.63993125" y1="44.822109375" x2="329.719940625" y2="44.79925" width="0.025" layer="94"/>
<wire x1="329.719940625" y1="44.79925" x2="329.81138125" y2="44.776390625" width="0.025" layer="94"/>
<wire x1="329.81138125" y1="44.776390625" x2="329.891390625" y2="44.75353125" width="0.025" layer="94"/>
<wire x1="329.891390625" y1="44.75353125" x2="330.165709375" y2="44.662090625" width="0.025" layer="94"/>
<wire x1="330.165709375" y1="44.662090625" x2="330.25715" y2="44.6278" width="0.025" layer="94"/>
<wire x1="330.25715" y1="44.6278" x2="330.3143" y2="44.604940625" width="0.025" layer="94"/>
<wire x1="330.3143" y1="44.604940625" x2="330.394309375" y2="44.57065" width="0.025" layer="94"/>
<wire x1="330.394309375" y1="44.57065" x2="330.508609375" y2="44.52493125" width="0.025" layer="94"/>
<wire x1="330.508609375" y1="44.52493125" x2="330.87436875" y2="44.34205" width="0.025" layer="94"/>
<wire x1="330.87436875" y1="44.34205" x2="331.04581875" y2="44.23918125" width="0.025" layer="94"/>
<wire x1="331.04581875" y1="44.23918125" x2="331.080109375" y2="44.21631875" width="0.025" layer="94"/>
<wire x1="331.080109375" y1="44.21631875" x2="331.137259375" y2="44.1706" width="0.025" layer="94"/>
<wire x1="331.137259375" y1="44.1706" x2="331.36471875" y2="44.000009375" width="0.025" layer="94"/>
<wire x1="331.36471875" y1="44.000009375" x2="331.451590625" y2="43.91341875" width="0.025" layer="94"/>
<wire x1="331.451590625" y1="43.91341875" x2="331.62875" y2="43.736259375" width="0.025" layer="94"/>
<wire x1="331.62875" y1="43.736259375" x2="331.67446875" y2="43.679109375" width="0.025" layer="94"/>
<wire x1="331.67446875" y1="43.679109375" x2="331.74305" y2="43.58766875" width="0.025" layer="94"/>
<wire x1="331.74305" y1="43.58766875" x2="331.777340625" y2="43.53051875" width="0.025" layer="94"/>
<wire x1="331.777340625" y1="43.53051875" x2="331.823059375" y2="43.450509375" width="0.025" layer="94"/>
<wire x1="331.823059375" y1="43.450509375" x2="331.85735" y2="43.38193125" width="0.025" layer="94"/>
<wire x1="331.85735" y1="43.38193125" x2="331.880209375" y2="43.32478125" width="0.025" layer="94"/>
<wire x1="331.880209375" y1="43.32478125" x2="331.90306875" y2="43.2562" width="0.025" layer="94"/>
<wire x1="331.90306875" y1="43.2562" x2="331.92593125" y2="43.164759375" width="0.025" layer="94"/>
<wire x1="331.92593125" y1="43.164759375" x2="331.937359375" y2="43.095040625" width="0.025" layer="94"/>
<wire x1="331.937359375" y1="43.095040625" x2="331.948790625" y2="42.993309375" width="0.025" layer="94"/>
<wire x1="331.948790625" y1="42.993309375" x2="331.948790625" y2="42.84471875" width="0.025" layer="94"/>
<wire x1="331.948790625" y1="42.84471875" x2="331.937359375" y2="42.75328125" width="0.025" layer="94"/>
<wire x1="331.937359375" y1="42.75328125" x2="331.92593125" y2="42.69613125" width="0.025" layer="94"/>
<wire x1="331.92593125" y1="42.69613125" x2="331.90306875" y2="42.604690625" width="0.025" layer="94"/>
<wire x1="331.90306875" y1="42.604690625" x2="331.886609375" y2="42.55783125" width="0.025" layer="94"/>
<wire x1="331.886609375" y1="42.55783125" x2="331.84591875" y2="42.4561" width="0.025" layer="94"/>
<wire x1="331.84591875" y1="42.4561" x2="331.777340625" y2="42.318940625" width="0.025" layer="94"/>
<wire x1="331.777340625" y1="42.318940625" x2="331.708759375" y2="42.21606875" width="0.025" layer="94"/>
<wire x1="331.708759375" y1="42.21606875" x2="331.67446875" y2="42.17035" width="0.025" layer="94"/>
<wire x1="331.67446875" y1="42.17035" x2="331.56016875" y2="42.033190625" width="0.025" layer="94"/>
<wire x1="331.56016875" y1="42.033190625" x2="331.40015" y2="41.87316875" width="0.025" layer="94"/>
<wire x1="331.40015" y1="41.87316875" x2="331.262990625" y2="41.75886875" width="0.025" layer="94"/>
<wire x1="331.262990625" y1="41.75886875" x2="331.17155" y2="41.690290625" width="0.025" layer="94"/>
<wire x1="331.17155" y1="41.690290625" x2="331.034390625" y2="41.59885" width="0.025" layer="94"/>
<wire x1="331.034390625" y1="41.59885" x2="330.977240625" y2="41.564559375" width="0.025" layer="94"/>
<wire x1="330.977240625" y1="41.564559375" x2="330.89723125" y2="41.518840625" width="0.025" layer="94"/>
<wire x1="330.89723125" y1="41.518840625" x2="330.84008125" y2="41.48455" width="0.025" layer="94"/>
<wire x1="330.84008125" y1="41.48455" x2="330.5429" y2="41.335959375" width="0.025" layer="94"/>
<wire x1="330.5429" y1="41.335959375" x2="330.462890625" y2="41.30166875" width="0.025" layer="94"/>
<wire x1="330.462890625" y1="41.30166875" x2="330.348590625" y2="41.25595" width="0.025" layer="94"/>
<wire x1="330.348590625" y1="41.25595" x2="330.25715" y2="41.221659375" width="0.025" layer="94"/>
<wire x1="330.25715" y1="41.221659375" x2="330.13141875" y2="41.175940625" width="0.025" layer="94"/>
<wire x1="330.13141875" y1="41.175940625" x2="329.994259375" y2="41.13021875" width="0.025" layer="94"/>
<wire x1="329.994259375" y1="41.13021875" x2="329.879959375" y2="41.09593125" width="0.025" layer="94"/>
<wire x1="329.879959375" y1="41.09593125" x2="329.75423125" y2="41.061640625" width="0.025" layer="94"/>
<wire x1="329.75423125" y1="41.061640625" x2="329.67421875" y2="41.03878125" width="0.025" layer="94"/>
<wire x1="329.67421875" y1="41.03878125" x2="329.52563125" y2="41.004490625" width="0.025" layer="94"/>
<wire x1="329.52563125" y1="41.004490625" x2="329.319890625" y2="40.95876875" width="0.025" layer="94"/>
<wire x1="329.319890625" y1="40.95876875" x2="329.194159375" y2="40.935909375" width="0.025" layer="94"/>
<wire x1="328.35108125" y1="37.03828125" x2="327.58253125" y2="40.78731875" width="0.025" layer="94"/>
<wire x1="327.58253125" y1="40.78731875" x2="326.86038125" y2="37.084" width="0.025" layer="94"/>
<wire x1="326.86038125" y1="37.084" x2="326.805290625" y2="37.01541875" width="0.025" layer="94"/>
<wire x1="271.78" y1="236.22" x2="271.78" y2="231.14" width="0.025" layer="94"/>
<wire x1="271.78" y1="231.14" x2="271.78" y2="226.06" width="0.025" layer="94"/>
<wire x1="271.78" y1="226.06" x2="279.4" y2="226.06" width="0.025" layer="94"/>
<wire x1="279.4" y1="226.06" x2="289.56" y2="226.06" width="0.025" layer="94"/>
<wire x1="289.56" y1="226.06" x2="299.72" y2="226.06" width="0.025" layer="94"/>
<wire x1="299.72" y1="226.06" x2="309.88" y2="226.06" width="0.025" layer="94"/>
<wire x1="309.88" y1="226.06" x2="375.92" y2="226.06" width="0.025" layer="94"/>
<wire x1="271.78" y1="231.14" x2="375.92" y2="231.14" width="0.025" layer="94"/>
<wire x1="279.4" y1="236.22" x2="279.4" y2="226.06" width="0.025" layer="94"/>
<wire x1="289.56" y1="236.22" x2="289.56" y2="226.06" width="0.025" layer="94"/>
<wire x1="299.72" y1="236.22" x2="299.72" y2="226.06" width="0.025" layer="94"/>
<wire x1="309.88" y1="236.22" x2="309.88" y2="226.06" width="0.025" layer="94"/>
<text x="272.796" y="35.306" size="1.778" layer="94" font="vector" ratio="12">DRAWN
BY</text>
<text x="272.542" y="28.702" size="1.778" layer="94" font="vector" ratio="12">CHECKED
BY</text>
<text x="272.542" y="22.352" size="1.778" layer="94" font="vector" ratio="12">DESIGN
APPROVAL</text>
<text x="287.02" y="42.164" size="1.778" layer="94" font="vector" ratio="12">INIT</text>
<text x="294.386" y="42.164" size="1.778" layer="94" font="vector" ratio="12">DATE</text>
<text x="333.502" y="33.528" size="1.6764" layer="94" font="vector" ratio="12">JOHN MEZZALINGUA ASSOCIATES
 7645 HENRY CLAY BOULEVARD
  LIVERPOOL, NEW YORK 13088
       TEL. 315-431-7100</text>
<text x="307.34" y="22.86" size="1.27" layer="94" font="vector" ratio="12">THIS DRAWING AND SPECIFICATIONS ARE THE PROPERTY OF
JOHN MEZZALINGUA ASSOCIATES AND SHALL NOT BE 
REPRODICED, COPIED, OR USED AS A BASIS FOR MANUFACTURING OR
SALE OF EQUIPMENT OR DEVICES WITHOUT WRITTEN PERMISSION.</text>
<text x="305.562" y="19.812" size="1.27" layer="94" font="vector" ratio="12">TITLE: </text>
<text x="305.562" y="13.462" size="1.27" layer="94" font="vector" ratio="12">DWG. NO.</text>
<text x="370.332" y="13.462" size="1.27" layer="94" font="vector" ratio="12">REV.</text>
<text x="360.172" y="7.112" size="1.27" layer="94" font="vector" ratio="12">SIZE:   B</text>
<text x="323.342" y="7.112" size="1.27" layer="94" font="vector" ratio="12">SHEET</text>
<text x="305.562" y="7.112" size="1.27" layer="94" font="vector" ratio="12">SCALE: NONE</text>
<text x="317.5" y="10.16" size="1.778" layer="94" font="vector">&gt;DRAWING_NAME</text>
<text x="332.486" y="5.842" size="1.27" layer="94" font="vector">&gt;SHEET</text>
<text x="273.304" y="233.68" size="1.778" layer="94" font="vector" ratio="12">REV</text>
<text x="281.178" y="233.68" size="1.778" layer="94" font="vector" ratio="12">ZONE</text>
<text x="291.592" y="233.68" size="1.778" layer="94" font="vector" ratio="12">DATE</text>
<text x="302.26" y="233.68" size="1.778" layer="94" font="vector" ratio="12">ECO</text>
<text x="333.502" y="233.426" size="1.778" layer="94" font="vector" ratio="12">DESCRIPTION</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="A3_LOC_JMA_RELEASE">
<gates>
<gate name="G$1" symbol="A3_LOC_JMA_RELEASE" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="JMA_LBR_v2" urn="urn:adsk.eagle:library:8413580">
<packages>
<package name="RESC1005X40" urn="urn:adsk.eagle:footprint:3811164/1" library_version="47">
<description>CHIP, 1 X 0.5 X 0.4 mm body
&lt;p&gt;CHIP package with body size 1 X 0.5 X 0.4 mm&lt;/p&gt;</description>
<wire x1="0.525" y1="0.614" x2="-0.525" y2="0.614" width="0.12" layer="21"/>
<wire x1="0.525" y1="-0.614" x2="-0.525" y2="-0.614" width="0.12" layer="21"/>
<wire x1="0.525" y1="-0.275" x2="-0.525" y2="-0.275" width="0.12" layer="51"/>
<wire x1="-0.525" y1="-0.275" x2="-0.525" y2="0.275" width="0.12" layer="51"/>
<wire x1="-0.525" y1="0.275" x2="0.525" y2="0.275" width="0.12" layer="51"/>
<wire x1="0.525" y1="0.275" x2="0.525" y2="-0.275" width="0.12" layer="51"/>
<smd name="1" x="-0.475" y="0" dx="0.55" dy="0.6" layer="1"/>
<smd name="2" x="0.475" y="0" dx="0.55" dy="0.6" layer="1"/>
<text x="0" y="1.249" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.249" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="RESC2012X70" urn="urn:adsk.eagle:footprint:3811170/1" library_version="47">
<description>CHIP, 2 X 1.25 X 0.7 mm body
&lt;p&gt;CHIP package with body size 2 X 1.25 X 0.7 mm&lt;/p&gt;</description>
<wire x1="1.1" y1="1.0036" x2="-1.1" y2="1.0036" width="0.12" layer="21"/>
<wire x1="1.1" y1="-1.0036" x2="-1.1" y2="-1.0036" width="0.12" layer="21"/>
<wire x1="1.1" y1="-0.675" x2="-1.1" y2="-0.675" width="0.12" layer="51"/>
<wire x1="-1.1" y1="-0.675" x2="-1.1" y2="0.675" width="0.12" layer="51"/>
<wire x1="-1.1" y1="0.675" x2="1.1" y2="0.675" width="0.12" layer="51"/>
<wire x1="1.1" y1="0.675" x2="1.1" y2="-0.675" width="0.12" layer="51"/>
<smd name="1" x="-0.94" y="0" dx="1.0354" dy="1.3791" layer="1"/>
<smd name="2" x="0.94" y="0" dx="1.0354" dy="1.3791" layer="1"/>
<text x="0" y="1.6386" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.6386" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC2012X127" urn="urn:adsk.eagle:footprint:3793140/1" library_version="47">
<description>CHIP, 2.01 X 1.25 X 1.27 mm body
&lt;p&gt;CHIP package with body size 2.01 X 1.25 X 1.27 mm&lt;/p&gt;</description>
<wire x1="1.105" y1="1.0467" x2="-1.105" y2="1.0467" width="0.12" layer="21"/>
<wire x1="1.105" y1="-1.0467" x2="-1.105" y2="-1.0467" width="0.12" layer="21"/>
<wire x1="1.105" y1="-0.725" x2="-1.105" y2="-0.725" width="0.12" layer="51"/>
<wire x1="-1.105" y1="-0.725" x2="-1.105" y2="0.725" width="0.12" layer="51"/>
<wire x1="-1.105" y1="0.725" x2="1.105" y2="0.725" width="0.12" layer="51"/>
<wire x1="1.105" y1="0.725" x2="1.105" y2="-0.725" width="0.12" layer="51"/>
<smd name="1" x="-0.8804" y="0" dx="1.1646" dy="1.4653" layer="1"/>
<smd name="2" x="0.8804" y="0" dx="1.1646" dy="1.4653" layer="1"/>
<text x="0" y="1.6817" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.6817" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC1608X55" urn="urn:adsk.eagle:footprint:3793129/2" library_version="43">
<description>CHIP, 1.6 X 0.8 X 0.55 mm body
&lt;p&gt;CHIP package with body size 1.6 X 0.8 X 0.55 mm&lt;/p&gt;</description>
<wire x1="0.85" y1="0.6516" x2="-0.85" y2="0.6516" width="0.12" layer="21"/>
<wire x1="0.85" y1="-0.6516" x2="-0.85" y2="-0.6516" width="0.12" layer="21"/>
<wire x1="0.85" y1="-0.45" x2="-0.85" y2="-0.45" width="0.12" layer="51"/>
<wire x1="-0.85" y1="-0.45" x2="-0.85" y2="0.45" width="0.12" layer="51"/>
<wire x1="-0.85" y1="0.45" x2="0.85" y2="0.45" width="0.12" layer="51"/>
<wire x1="0.85" y1="0.45" x2="0.85" y2="-0.45" width="0.12" layer="51"/>
<smd name="1" x="-0.7704" y="0" dx="0.8884" dy="0.9291" layer="1"/>
<smd name="2" x="0.7704" y="0" dx="0.8884" dy="0.9291" layer="1"/>
<text x="0" y="1.4136" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.4136" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="SOP50P490X110-10" urn="urn:adsk.eagle:footprint:3921299/1" library_version="47">
<description>10-SOP, 0.5 mm pitch, 4.9 mm span, 3 X 3 X 1.1 mm body
&lt;p&gt;10-pin SOP package with 0.5 mm pitch, 4.9 mm span with body size 3 X 3 X 1.1 mm&lt;/p&gt;</description>
<circle x="-2.2175" y="1.659" radius="0.25" width="0" layer="21"/>
<wire x1="-1.55" y1="1.409" x2="-1.55" y2="1.55" width="0.12" layer="21"/>
<wire x1="-1.55" y1="1.55" x2="1.55" y2="1.55" width="0.12" layer="21"/>
<wire x1="1.55" y1="1.55" x2="1.55" y2="1.409" width="0.12" layer="21"/>
<wire x1="-1.55" y1="-1.409" x2="-1.55" y2="-1.55" width="0.12" layer="21"/>
<wire x1="-1.55" y1="-1.55" x2="1.55" y2="-1.55" width="0.12" layer="21"/>
<wire x1="1.55" y1="-1.55" x2="1.55" y2="-1.409" width="0.12" layer="21"/>
<wire x1="1.55" y1="-1.55" x2="-1.55" y2="-1.55" width="0.12" layer="51"/>
<wire x1="-1.55" y1="-1.55" x2="-1.55" y2="1.55" width="0.12" layer="51"/>
<wire x1="-1.55" y1="1.55" x2="1.55" y2="1.55" width="0.12" layer="51"/>
<wire x1="1.55" y1="1.55" x2="1.55" y2="-1.55" width="0.12" layer="51"/>
<smd name="1" x="-2.1496" y="1" dx="1.4709" dy="0.31" layer="1"/>
<smd name="2" x="-2.1496" y="0.5" dx="1.4709" dy="0.31" layer="1"/>
<smd name="3" x="-2.1496" y="0" dx="1.4709" dy="0.31" layer="1"/>
<smd name="4" x="-2.1496" y="-0.5" dx="1.4709" dy="0.31" layer="1"/>
<smd name="5" x="-2.1496" y="-1" dx="1.4709" dy="0.31" layer="1"/>
<smd name="6" x="2.1496" y="-1" dx="1.4709" dy="0.31" layer="1"/>
<smd name="7" x="2.1496" y="-0.5" dx="1.4709" dy="0.31" layer="1"/>
<smd name="8" x="2.1496" y="0" dx="1.4709" dy="0.31" layer="1"/>
<smd name="9" x="2.1496" y="0.5" dx="1.4709" dy="0.31" layer="1"/>
<smd name="10" x="2.1496" y="1" dx="1.4709" dy="0.31" layer="1"/>
<text x="0" y="2.544" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.185" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="TO457P1000X238-3" urn="urn:adsk.eagle:footprint:3920003/3" library_version="47">
<description>3-TO, DPAK, 4.57 mm pitch, 10 mm span, 6.6 X 6.1 X 2.38 mm body
&lt;p&gt;3-pin TO, DPAK package with 4.57 mm pitch, 10 mm span with body size 6.6 X 6.1 X 2.38 mm&lt;/p&gt;</description>
<circle x="-4.75" y="3.3836" radius="0.25" width="0" layer="21"/>
<wire x1="6.221" y1="3.0207" x2="6.221" y2="3.365" width="0.12" layer="21"/>
<wire x1="6.221" y1="3.365" x2="-1.015" y2="3.365" width="0.12" layer="21"/>
<wire x1="-1.015" y1="3.365" x2="-1.015" y2="-3.365" width="0.12" layer="21"/>
<wire x1="-1.015" y1="-3.365" x2="6.221" y2="-3.365" width="0.12" layer="21"/>
<wire x1="6.221" y1="-3.365" x2="6.221" y2="-3.0207" width="0.12" layer="21"/>
<wire x1="6.221" y1="-3.365" x2="-1.015" y2="-3.365" width="0.12" layer="51"/>
<wire x1="-1.015" y1="-3.365" x2="-1.015" y2="3.365" width="0.12" layer="51"/>
<wire x1="-1.015" y1="3.365" x2="6.221" y2="3.365" width="0.12" layer="51"/>
<wire x1="6.221" y1="3.365" x2="6.221" y2="-3.365" width="0.12" layer="51"/>
<smd name="1" x="-4.75" y="2.285" dx="1.6078" dy="1.1891" layer="1"/>
<smd name="2" x="-4.75" y="-2.285" dx="1.6078" dy="1.1891" layer="1"/>
<smd name="3" x="2.545" y="0" dx="6.0178" dy="5.5334" layer="1"/>
<text x="0" y="4.2686" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-4" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="POWERPAK_SO-8" urn="urn:adsk.eagle:footprint:8413717/1" library_version="43">
<smd name="1" x="-2.67" y="1.905" dx="1.27" dy="0.61" layer="1"/>
<smd name="2" x="-2.67" y="0.635" dx="1.27" dy="0.61" layer="1"/>
<smd name="3" x="-2.67" y="-0.635" dx="1.27" dy="0.61" layer="1"/>
<smd name="4" x="-2.67" y="-1.905" dx="1.27" dy="0.61" layer="1"/>
<smd name="5" x="2.795" y="-1.905" dx="1.02" dy="0.61" layer="1"/>
<smd name="6" x="2.795" y="-0.635" dx="1.02" dy="0.61" layer="1"/>
<smd name="7" x="2.795" y="0.635" dx="1.02" dy="0.61" layer="1"/>
<smd name="8" x="2.795" y="1.905" dx="1.02" dy="0.61" layer="1"/>
<smd name="9" x="0.69" y="0" dx="3.81" dy="3.91" layer="1"/>
<wire x1="-3.075" y1="2.575" x2="3.075" y2="2.575" width="0.127" layer="21"/>
<wire x1="-3.075" y1="-2.575" x2="3.075" y2="-2.575" width="0.127" layer="21"/>
<wire x1="-3.075" y1="2.575" x2="3.075" y2="2.575" width="0.127" layer="51"/>
<wire x1="-3.075" y1="-2.575" x2="3.075" y2="-2.575" width="0.127" layer="51"/>
<text x="-3.81" y="3.175" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.81" y="-4.445" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="DO214AC_SMA" urn="urn:adsk.eagle:footprint:15671032/3" library_version="47">
<wire x1="3.43" y1="-2.02" x2="-3.43" y2="-2.02" width="0.127" layer="21"/>
<wire x1="-3.43" y1="-2.02" x2="-3.43" y2="2.02" width="0.127" layer="21"/>
<wire x1="-3.43" y1="2.02" x2="3.43" y2="2.02" width="0.127" layer="21"/>
<wire x1="3.43" y1="2.02" x2="3.43" y2="-2.02" width="0.127" layer="21" style="shortdash"/>
<wire x1="3.43" y1="-2.02" x2="-3.43" y2="-2.02" width="0.127" layer="21"/>
<wire x1="-3.43" y1="-2.02" x2="-3.43" y2="2.02" width="0.127" layer="21"/>
<wire x1="-3.43" y1="2.02" x2="3.43" y2="2.02" width="0.127" layer="21"/>
<wire x1="3.43" y1="2.02" x2="3.43" y2="-2.02" width="0.127" layer="21" style="shortdash"/>
<wire x1="3.65" y1="-2" x2="3.65" y2="2" width="0.127" layer="21"/>
<wire x1="3.65" y1="-2" x2="3.65" y2="2" width="0.127" layer="21"/>
<smd name="1" x="-2.25" y="0" dx="1.7" dy="2.5" layer="1" roundness="25"/>
<smd name="2" x="2.25" y="0" dx="1.7" dy="2.5" layer="1" roundness="25"/>
<text x="2" y="3" size="0.8128" layer="25" font="vector" ratio="15" rot="R180">&gt;NAME</text>
<text x="2" y="-2.25" size="0.635" layer="27" font="vector" ratio="10" rot="R180">&gt;VALUE</text>
</package>
<package name="DIOC5226X110N" urn="urn:adsk.eagle:footprint:16113287/1" library_version="47">
<description>Chip, 5.20 X 2.60 X 1.10 mm body
&lt;p&gt;Chip package with body size 5.20 X 2.60 X 1.10 mm&lt;/p&gt;</description>
<wire x1="2.8" y1="1.7934" x2="-3.4079" y2="1.7934" width="0.12" layer="21"/>
<wire x1="-3.4079" y1="1.7934" x2="-3.4079" y2="-1.7934" width="0.12" layer="21"/>
<wire x1="-3.4079" y1="-1.7934" x2="2.8" y2="-1.7934" width="0.12" layer="21"/>
<wire x1="2.8" y1="-1.475" x2="-2.8" y2="-1.475" width="0.12" layer="51"/>
<wire x1="-2.8" y1="-1.475" x2="-2.8" y2="1.475" width="0.12" layer="51"/>
<wire x1="-2.8" y1="1.475" x2="2.8" y2="1.475" width="0.12" layer="51"/>
<wire x1="2.8" y1="1.475" x2="2.8" y2="-1.475" width="0.12" layer="51"/>
<smd name="1" x="-2.1729" y="0" dx="1.962" dy="2.9589" layer="1"/>
<smd name="2" x="2.1729" y="0" dx="1.962" dy="2.9589" layer="1"/>
<text x="0" y="2.4284" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.4284" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="TP_1.5MM" urn="urn:adsk.eagle:footprint:8413705/1" library_version="44">
<pad name="1" x="0" y="0" drill="1" diameter="1.5" rot="R180"/>
<text x="-1.905" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<rectangle x1="-0.35" y1="-0.35" x2="0.35" y2="0.35" layer="51"/>
</package>
<package name="TP_1.5MM_PAD" urn="urn:adsk.eagle:footprint:8413706/1" library_version="44">
<pad name="1" x="0" y="0" drill="1" diameter="1.5" rot="R180"/>
<text x="-1.27" y="4.445" size="1.27" layer="25">&gt;NAME</text>
<rectangle x1="-0.35" y1="-0.35" x2="0.35" y2="0.35" layer="51"/>
<rectangle x1="-2" y1="-2.5" x2="8" y2="2.5" layer="1"/>
</package>
<package name="TP_1.7MM" urn="urn:adsk.eagle:footprint:8413707/1" library_version="44">
<pad name="1" x="0" y="0" drill="1" diameter="1.6764" rot="R180"/>
<text x="-1.905" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<rectangle x1="-0.35" y1="-0.35" x2="0.35" y2="0.35" layer="51"/>
</package>
<package name="C120H40" urn="urn:adsk.eagle:footprint:8413699/1" library_version="44">
<pad name="1" x="0" y="0" drill="0.6" diameter="1.2" thermals="no"/>
<text x="0.9" y="-0.1" size="0.254" layer="25">&gt;NAME</text>
</package>
<package name="C131H51" urn="urn:adsk.eagle:footprint:8413700/1" library_version="44">
<pad name="1" x="0" y="0" drill="0.71" diameter="1.31" thermals="no"/>
<text x="1.2" y="0.1" size="0.254" layer="25">&gt;NAME</text>
<text x="1.2" y="-0.2" size="0.254" layer="21">&gt;LABEL</text>
</package>
<package name="C406H326" urn="urn:adsk.eagle:footprint:8413701/1" library_version="44">
<pad name="1" x="0" y="0" drill="3.26" diameter="4.06" thermals="no"/>
<text x="2.2" y="1.1" size="0.254" layer="25">&gt;NAME</text>
<text x="2.2" y="0.8" size="0.254" layer="21">&gt;LABEL</text>
</package>
<package name="C491H411" urn="urn:adsk.eagle:footprint:8413702/1" library_version="44">
<pad name="1" x="0" y="0" drill="4.31" diameter="4.91" thermals="no"/>
<text x="3.2" y="1.1" size="0.254" layer="25">&gt;NAME</text>
<text x="3.2" y="0.8" size="0.254" layer="21">&gt;LABEL</text>
</package>
<package name="CIR_PAD_1MM" urn="urn:adsk.eagle:footprint:8413703/1" library_version="44">
<text x="-1.905" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<smd name="P$1" x="0" y="0" dx="1" dy="1" layer="1" roundness="100"/>
</package>
<package name="CIR_PAD_2MM" urn="urn:adsk.eagle:footprint:8413704/1" library_version="44">
<text x="-1.905" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<smd name="P$1" x="0" y="0" dx="2" dy="2" layer="1" roundness="100"/>
</package>
<package name="DIOM5226X292" urn="urn:adsk.eagle:footprint:3793195/2" library_version="47">
<description>MOLDED BODY, 5.265 X 2.6 X 2.92 mm body
&lt;p&gt;MOLDED BODY package with body size 5.265 X 2.6 X 2.92 mm&lt;/p&gt;</description>
<wire x1="2.8" y1="1.45" x2="-2.8" y2="1.45" width="0.12" layer="21"/>
<wire x1="-3.6186" y1="1.4614" x2="-3.6186" y2="-1.4614" width="0.12" layer="21"/>
<wire x1="-2.8" y1="-1.45" x2="2.8" y2="-1.45" width="0.12" layer="21"/>
<wire x1="2.8" y1="-1.45" x2="-2.8" y2="-1.45" width="0.12" layer="51"/>
<wire x1="-2.8" y1="-1.45" x2="-2.8" y2="1.45" width="0.12" layer="51"/>
<wire x1="-2.8" y1="1.45" x2="2.8" y2="1.45" width="0.12" layer="51"/>
<wire x1="2.8" y1="1.45" x2="2.8" y2="-1.45" width="0.12" layer="51"/>
<smd name="K" x="-2.164" y="0" dx="2.2812" dy="1.5153" layer="1"/>
<smd name="A" x="2.164" y="0" dx="2.2812" dy="1.5153" layer="1"/>
<text x="0" y="2.085" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.085" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="DIOM5436X265" urn="urn:adsk.eagle:footprint:3793203/2" library_version="47">
<description>MOLDED BODY, 5.4 X 3.6 X 2.65 mm body
&lt;p&gt;MOLDED BODY package with body size 5.4 X 3.6 X 2.65 mm&lt;/p&gt;</description>
<wire x1="-2.795" y1="1.95" x2="2.795" y2="1.95" width="0.12" layer="21"/>
<wire x1="-3.6171" y1="1.95" x2="-3.6171" y2="-1.95" width="0.12" layer="21"/>
<wire x1="2.795" y1="-1.95" x2="-2.795" y2="-1.95" width="0.12" layer="21"/>
<wire x1="2.795" y1="-1.95" x2="-2.795" y2="-1.95" width="0.12" layer="51"/>
<wire x1="-2.795" y1="-1.95" x2="-2.795" y2="1.95" width="0.12" layer="51"/>
<wire x1="-2.795" y1="1.95" x2="2.795" y2="1.95" width="0.12" layer="51"/>
<wire x1="2.795" y1="1.95" x2="2.795" y2="-1.95" width="0.12" layer="51"/>
<smd name="K" x="-2.2127" y="0" dx="2.1808" dy="2.0291" layer="1"/>
<smd name="A" x="2.2127" y="0" dx="2.1808" dy="2.0291" layer="1"/>
<text x="0" y="2.585" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.585" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="DIOM7959X265" urn="urn:adsk.eagle:footprint:3793209/2" library_version="47">
<description>MOLDED BODY, 7.94 X 5.95 X 2.65 mm body
&lt;p&gt;MOLDED BODY package with body size 7.94 X 5.95 X 2.65 mm&lt;/p&gt;</description>
<wire x1="4.065" y1="3.15" x2="-4.07" y2="3.15" width="0.12" layer="21"/>
<wire x1="-4.8871" y1="3.15" x2="-4.8871" y2="-3.15" width="0.12" layer="21"/>
<wire x1="-4.07" y1="-3.15" x2="4.065" y2="-3.15" width="0.12" layer="21"/>
<wire x1="4.065" y1="-3.15" x2="-4.065" y2="-3.15" width="0.12" layer="51"/>
<wire x1="-4.065" y1="-3.15" x2="-4.065" y2="3.15" width="0.12" layer="51"/>
<wire x1="-4.065" y1="3.15" x2="4.065" y2="3.15" width="0.12" layer="51"/>
<wire x1="4.065" y1="3.15" x2="4.065" y2="-3.15" width="0.12" layer="51"/>
<smd name="K" x="-3.4827" y="0" dx="2.1808" dy="3.1202" layer="1"/>
<smd name="A" x="3.4827" y="0" dx="2.1808" dy="3.1202" layer="1"/>
<text x="0" y="3.785" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-3.785" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="SOD2512X100" urn="urn:adsk.eagle:footprint:3793220/2" library_version="47">
<description>SOD, 2.5 mm span, 1.7 X 1.25 X 1 mm body
&lt;p&gt;SOD package with 2.5 mm span with body size 1.7 X 1.25 X 1 mm&lt;/p&gt;</description>
<wire x1="-0.9" y1="0.675" x2="0.9" y2="0.675" width="0.12" layer="21"/>
<wire x1="-2.0217" y1="0.675" x2="-2.0217" y2="-0.675" width="0.12" layer="21"/>
<wire x1="0.9" y1="-0.675" x2="-0.9" y2="-0.675" width="0.12" layer="21"/>
<wire x1="0.9" y1="-0.675" x2="-0.9" y2="-0.675" width="0.12" layer="51"/>
<wire x1="-0.9" y1="-0.675" x2="-0.9" y2="0.675" width="0.12" layer="51"/>
<wire x1="-0.9" y1="0.675" x2="0.9" y2="0.675" width="0.12" layer="51"/>
<wire x1="0.9" y1="0.675" x2="0.9" y2="-0.675" width="0.12" layer="51"/>
<smd name="K" x="-1.1968" y="0" dx="1.0218" dy="0.4971" layer="1"/>
<smd name="A" x="1.1968" y="0" dx="1.0218" dy="0.4971" layer="1"/>
<text x="0" y="1.31" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.31" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="SOD3716X135" urn="urn:adsk.eagle:footprint:3793234/2" library_version="47">
<description>SOD, 3.7 mm span, 2.7 X 1.6 X 1.35 mm body
&lt;p&gt;SOD package with 3.7 mm span with body size 2.7 X 1.6 X 1.35 mm&lt;/p&gt;</description>
<wire x1="-1.425" y1="0.9" x2="1.425" y2="0.9" width="0.12" layer="21"/>
<wire x1="-2.5991" y1="0.9" x2="-2.5991" y2="-0.9" width="0.12" layer="21"/>
<wire x1="1.425" y1="-0.9" x2="-1.425" y2="-0.9" width="0.12" layer="21"/>
<wire x1="1.425" y1="-0.9" x2="-1.425" y2="-0.9" width="0.12" layer="51"/>
<wire x1="-1.425" y1="-0.9" x2="-1.425" y2="0.9" width="0.12" layer="51"/>
<wire x1="-1.425" y1="0.9" x2="1.425" y2="0.9" width="0.12" layer="51"/>
<wire x1="1.425" y1="0.9" x2="1.425" y2="-0.9" width="0.12" layer="51"/>
<smd name="K" x="-1.7116" y="0" dx="1.147" dy="0.7891" layer="1"/>
<smd name="A" x="1.7116" y="0" dx="1.147" dy="0.7891" layer="1"/>
<text x="0" y="1.535" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.535" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="SOT65P210X110-3" urn="urn:adsk.eagle:footprint:3809952/2" library_version="47">
<description>3-SOT23, 0.65 mm pitch, 2.1 mm span, 2 X 1.25 X 1.1 mm body
&lt;p&gt;3-pin SOT23 package with 0.65 mm pitch, 2.1 mm span with body size 2 X 1.25 X 1.1 mm&lt;/p&gt;</description>
<circle x="-1.0698" y="1.409" radius="0.25" width="0" layer="21"/>
<wire x1="-0.675" y1="1.219" x2="0.675" y2="1.219" width="0.12" layer="21"/>
<wire x1="0.675" y1="1.219" x2="0.675" y2="0.509" width="0.12" layer="21"/>
<wire x1="-0.675" y1="-1.219" x2="0.675" y2="-1.219" width="0.12" layer="21"/>
<wire x1="0.675" y1="-1.219" x2="0.675" y2="-0.509" width="0.12" layer="21"/>
<wire x1="0.675" y1="-1.05" x2="-0.675" y2="-1.05" width="0.12" layer="51"/>
<wire x1="-0.675" y1="-1.05" x2="-0.675" y2="1.05" width="0.12" layer="51"/>
<wire x1="-0.675" y1="1.05" x2="0.675" y2="1.05" width="0.12" layer="51"/>
<wire x1="0.675" y1="1.05" x2="0.675" y2="-1.05" width="0.12" layer="51"/>
<smd name="A" x="-0.9704" y="0.65" dx="0.9884" dy="0.51" layer="1"/>
<smd name="NC" x="-0.9704" y="-0.65" dx="0.9884" dy="0.51" layer="1"/>
<smd name="K" x="0.9704" y="0" dx="0.9884" dy="0.51" layer="1"/>
<text x="0" y="2.294" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.854" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="SOT95P280X135-3" urn="urn:adsk.eagle:footprint:3809968/2" library_version="47">
<description>3-SOT23, 0.95 mm pitch, 2.8 mm span, 2.9 X 1.6 X 1.35 mm body
&lt;p&gt;3-pin SOT23 package with 0.95 mm pitch, 2.8 mm span with body size 2.9 X 1.6 X 1.35 mm&lt;/p&gt;</description>
<circle x="-1.3538" y="1.7786" radius="0.25" width="0" layer="21"/>
<wire x1="-0.85" y1="1.5886" x2="0.85" y2="1.5886" width="0.12" layer="21"/>
<wire x1="0.85" y1="1.5886" x2="0.85" y2="0.5786" width="0.12" layer="21"/>
<wire x1="-0.85" y1="-1.5886" x2="0.85" y2="-1.5886" width="0.12" layer="21"/>
<wire x1="0.85" y1="-1.5886" x2="0.85" y2="-0.5786" width="0.12" layer="21"/>
<wire x1="0.85" y1="-1.5" x2="-0.85" y2="-1.5" width="0.12" layer="51"/>
<wire x1="-0.85" y1="-1.5" x2="-0.85" y2="1.5" width="0.12" layer="51"/>
<wire x1="-0.85" y1="1.5" x2="0.85" y2="1.5" width="0.12" layer="51"/>
<wire x1="0.85" y1="1.5" x2="0.85" y2="-1.5" width="0.12" layer="51"/>
<smd name="A" x="-1.2644" y="0.95" dx="1.1864" dy="0.6492" layer="1"/>
<smd name="NC" x="-1.2644" y="-0.95" dx="1.1864" dy="0.6492" layer="1"/>
<smd name="K" x="1.2644" y="0" dx="1.1864" dy="0.6492" layer="1"/>
<text x="0" y="2.6636" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.2236" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="SODFL4725X110" urn="urn:adsk.eagle:footprint:3921258/2" library_version="47">
<description>SODFL, 4.7 mm span, 3.8 X 2.5 X 1.1 mm body
&lt;p&gt;SODFL package with 4.7 mm span with body size 3.8 X 2.5 X 1.1 mm&lt;/p&gt;</description>
<wire x1="-3.0192" y1="1.35" x2="-3.0192" y2="-1.35" width="0.12" layer="21"/>
<wire x1="2" y1="-1.35" x2="-2" y2="-1.35" width="0.12" layer="51"/>
<wire x1="-2" y1="-1.35" x2="-2" y2="1.35" width="0.12" layer="51"/>
<wire x1="-2" y1="1.35" x2="2" y2="1.35" width="0.12" layer="51"/>
<wire x1="2" y1="1.35" x2="2" y2="-1.35" width="0.12" layer="51"/>
<smd name="K" x="-1.9923" y="0" dx="1.4257" dy="1.9202" layer="1"/>
<smd name="A" x="1.9923" y="0" dx="1.4257" dy="1.9202" layer="1"/>
<text x="0" y="1.985" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.985" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="DIOC0502X50" urn="urn:adsk.eagle:footprint:3948091/2" library_version="47">
<description>CHIP, 0.5 X 0.25 X 0.5 mm body
&lt;p&gt;CHIP package with body size 0.5 X 0.25 X 0.5 mm&lt;/p&gt;</description>
<wire x1="-0.754" y1="0.235" x2="-0.754" y2="-0.235" width="0.12" layer="21"/>
<wire x1="0.275" y1="-0.15" x2="-0.275" y2="-0.15" width="0.12" layer="51"/>
<wire x1="-0.275" y1="-0.15" x2="-0.275" y2="0.15" width="0.12" layer="51"/>
<wire x1="-0.275" y1="0.15" x2="0.275" y2="0.15" width="0.12" layer="51"/>
<wire x1="0.275" y1="0.15" x2="0.275" y2="-0.15" width="0.12" layer="51"/>
<smd name="K" x="-0.275" y="0" dx="0.45" dy="0.35" layer="1"/>
<smd name="A" x="0.275" y="0" dx="0.45" dy="0.35" layer="1"/>
<text x="0" y="1.124" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.124" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="DIOC1005X84" urn="urn:adsk.eagle:footprint:3948102/2" library_version="47">
<description>CHIP, 1.07 X 0.56 X 0.84 mm body
&lt;p&gt;CHIP package with body size 1.07 X 0.56 X 0.84 mm&lt;/p&gt;</description>
<wire x1="-1.0741" y1="0.4165" x2="-1.0741" y2="-0.4165" width="0.12" layer="21"/>
<wire x1="0.61" y1="-0.345" x2="-0.61" y2="-0.345" width="0.12" layer="51"/>
<wire x1="-0.61" y1="-0.345" x2="-0.61" y2="0.345" width="0.12" layer="51"/>
<wire x1="-0.61" y1="0.345" x2="0.61" y2="0.345" width="0.12" layer="51"/>
<wire x1="0.61" y1="0.345" x2="0.61" y2="-0.345" width="0.12" layer="51"/>
<smd name="K" x="-0.51" y="0" dx="0.6202" dy="0.713" layer="1"/>
<smd name="A" x="0.51" y="0" dx="0.6202" dy="0.713" layer="1"/>
<text x="0" y="1.3055" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.3055" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="DIOC1206X63" urn="urn:adsk.eagle:footprint:3948142/2" library_version="47">
<description>CHIP, 1.27 X 0.6 X 0.63 mm body
&lt;p&gt;CHIP package with body size 1.27 X 0.6 X 0.63 mm&lt;/p&gt;</description>
<wire x1="-1.1741" y1="0.4365" x2="-1.1741" y2="-0.4365" width="0.12" layer="21"/>
<wire x1="0.71" y1="-0.365" x2="-0.71" y2="-0.365" width="0.12" layer="51"/>
<wire x1="-0.71" y1="-0.365" x2="-0.71" y2="0.365" width="0.12" layer="51"/>
<wire x1="-0.71" y1="0.365" x2="0.71" y2="0.365" width="0.12" layer="51"/>
<wire x1="0.71" y1="0.365" x2="0.71" y2="-0.365" width="0.12" layer="51"/>
<smd name="K" x="-0.545" y="0" dx="0.7502" dy="0.753" layer="1"/>
<smd name="A" x="0.545" y="0" dx="0.7502" dy="0.753" layer="1"/>
<text x="0" y="1.3255" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.3255" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="DIOC1608X84" urn="urn:adsk.eagle:footprint:3948148/2" library_version="47">
<description>CHIP, 1.63 X 0.81 X 0.84 mm body
&lt;p&gt;CHIP package with body size 1.63 X 0.81 X 0.84 mm&lt;/p&gt;</description>
<wire x1="-1.5041" y1="0.5415" x2="-1.5041" y2="-0.5415" width="0.12" layer="21"/>
<wire x1="0.89" y1="-0.47" x2="-0.89" y2="-0.47" width="0.12" layer="51"/>
<wire x1="-0.89" y1="-0.47" x2="-0.89" y2="0.47" width="0.12" layer="51"/>
<wire x1="-0.89" y1="0.47" x2="0.89" y2="0.47" width="0.12" layer="51"/>
<wire x1="0.89" y1="0.47" x2="0.89" y2="-0.47" width="0.12" layer="51"/>
<smd name="K" x="-0.845" y="0" dx="0.8102" dy="0.963" layer="1"/>
<smd name="A" x="0.845" y="0" dx="0.8102" dy="0.963" layer="1"/>
<text x="0" y="1.4305" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.4305" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="DIOC2012X70" urn="urn:adsk.eagle:footprint:3948151/2" library_version="47">
<description>CHIP, 2 X 1.25 X 0.7 mm body
&lt;p&gt;CHIP package with body size 2 X 1.25 X 0.7 mm&lt;/p&gt;</description>
<wire x1="-1.7117" y1="0.7496" x2="-1.7117" y2="-0.7496" width="0.12" layer="21"/>
<wire x1="1.1" y1="-0.675" x2="-1.1" y2="-0.675" width="0.12" layer="51"/>
<wire x1="-1.1" y1="-0.675" x2="-1.1" y2="0.675" width="0.12" layer="51"/>
<wire x1="-1.1" y1="0.675" x2="1.1" y2="0.675" width="0.12" layer="51"/>
<wire x1="1.1" y1="0.675" x2="1.1" y2="-0.675" width="0.12" layer="51"/>
<smd name="K" x="-0.94" y="0" dx="1.0354" dy="1.3791" layer="1"/>
<smd name="A" x="0.94" y="0" dx="1.0354" dy="1.3791" layer="1"/>
<text x="0" y="1.6386" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.6386" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="DIOC3216X84" urn="urn:adsk.eagle:footprint:3948169/2" library_version="47">
<description>CHIP, 3.2 X 1.6 X 0.84 mm body
&lt;p&gt;CHIP package with body size 3.2 X 1.6 X 0.84 mm&lt;/p&gt;</description>
<wire x1="-2.2891" y1="0.9365" x2="-2.2891" y2="-0.9365" width="0.12" layer="21"/>
<wire x1="1.675" y1="-0.865" x2="-1.675" y2="-0.865" width="0.12" layer="51"/>
<wire x1="-1.675" y1="-0.865" x2="-1.675" y2="0.865" width="0.12" layer="51"/>
<wire x1="-1.675" y1="0.865" x2="1.675" y2="0.865" width="0.12" layer="51"/>
<wire x1="1.675" y1="0.865" x2="1.675" y2="-0.865" width="0.12" layer="51"/>
<smd name="K" x="-1.5131" y="0" dx="1.044" dy="1.753" layer="1"/>
<smd name="A" x="1.5131" y="0" dx="1.044" dy="1.753" layer="1"/>
<text x="0" y="1.8255" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.8255" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="DIOC5324X84" urn="urn:adsk.eagle:footprint:3948172/2" library_version="47">
<description>CHIP, 5.31 X 2.49 X 0.84 mm body
&lt;p&gt;CHIP package with body size 5.31 X 2.49 X 0.84 mm&lt;/p&gt;</description>
<wire x1="-3.3441" y1="1.3815" x2="-3.3441" y2="-1.3815" width="0.12" layer="21"/>
<wire x1="2.73" y1="-1.31" x2="-2.73" y2="-1.31" width="0.12" layer="51"/>
<wire x1="-2.73" y1="-1.31" x2="-2.73" y2="1.31" width="0.12" layer="51"/>
<wire x1="-2.73" y1="1.31" x2="2.73" y2="1.31" width="0.12" layer="51"/>
<wire x1="2.73" y1="1.31" x2="2.73" y2="-1.31" width="0.12" layer="51"/>
<smd name="K" x="-2.555" y="0" dx="1.0702" dy="2.643" layer="1"/>
<smd name="A" x="2.555" y="0" dx="1.0702" dy="2.643" layer="1"/>
<text x="0" y="2.2705" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.2705" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="SOIC127P600X175-8" urn="urn:adsk.eagle:footprint:3833981/2" library_version="47">
<description>8-SOIC, 1.27 mm pitch, 6 mm span, 4.9 X 3.9 X 1.75 mm body
&lt;p&gt;8-pin SOIC package with 1.27 mm pitch, 6 mm span with body size 4.9 X 3.9 X 1.75 mm&lt;/p&gt;</description>
<circle x="-2.7288" y="2.7086" radius="0.25" width="0" layer="21"/>
<wire x1="-2" y1="2.5186" x2="2" y2="2.5186" width="0.12" layer="21"/>
<wire x1="-2" y1="-2.5186" x2="2" y2="-2.5186" width="0.12" layer="21"/>
<wire x1="2" y1="-2.5" x2="-2" y2="-2.5" width="0.12" layer="51"/>
<wire x1="-2" y1="-2.5" x2="-2" y2="2.5" width="0.12" layer="51"/>
<wire x1="-2" y1="2.5" x2="2" y2="2.5" width="0.12" layer="51"/>
<wire x1="2" y1="2.5" x2="2" y2="-2.5" width="0.12" layer="51"/>
<smd name="1" x="-2.4734" y="1.905" dx="1.9685" dy="0.5991" layer="1"/>
<smd name="2" x="-2.4734" y="0.635" dx="1.9685" dy="0.5991" layer="1"/>
<smd name="3" x="-2.4734" y="-0.635" dx="1.9685" dy="0.5991" layer="1"/>
<smd name="4" x="-2.4734" y="-1.905" dx="1.9685" dy="0.5991" layer="1"/>
<smd name="5" x="2.4734" y="-1.905" dx="1.9685" dy="0.5991" layer="1"/>
<smd name="6" x="2.4734" y="-0.635" dx="1.9685" dy="0.5991" layer="1"/>
<smd name="7" x="2.4734" y="0.635" dx="1.9685" dy="0.5991" layer="1"/>
<smd name="8" x="2.4734" y="1.905" dx="1.9685" dy="0.5991" layer="1"/>
<text x="0" y="3.5936" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-3.1536" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="SOT230P700X180-4" urn="urn:adsk.eagle:footprint:3834047/1" library_version="47">
<description>4-SOT223, 2.3 mm pitch, 7 mm span, 6.5 X 3.5 X 1.8 mm body
&lt;p&gt;4-pin SOT223 package with 2.3 mm pitch, 7 mm span with body size 6.5 X 3.5 X 1.8 mm&lt;/p&gt;</description>
<circle x="-2.9276" y="3.2847" radius="0.25" width="0" layer="21"/>
<wire x1="-1.85" y1="3.0347" x2="-1.85" y2="3.35" width="0.12" layer="21"/>
<wire x1="-1.85" y1="3.35" x2="1.85" y2="3.35" width="0.12" layer="21"/>
<wire x1="1.85" y1="3.35" x2="1.85" y2="1.8847" width="0.12" layer="21"/>
<wire x1="-1.85" y1="-3.0347" x2="-1.85" y2="-3.35" width="0.12" layer="21"/>
<wire x1="-1.85" y1="-3.35" x2="1.85" y2="-3.35" width="0.12" layer="21"/>
<wire x1="1.85" y1="-3.35" x2="1.85" y2="-1.8847" width="0.12" layer="21"/>
<wire x1="1.85" y1="-3.35" x2="-1.85" y2="-3.35" width="0.12" layer="51"/>
<wire x1="-1.85" y1="-3.35" x2="-1.85" y2="3.35" width="0.12" layer="51"/>
<wire x1="-1.85" y1="3.35" x2="1.85" y2="3.35" width="0.12" layer="51"/>
<wire x1="1.85" y1="3.35" x2="1.85" y2="-3.35" width="0.12" layer="51"/>
<smd name="1" x="-2.9226" y="2.3" dx="2.1651" dy="0.9615" layer="1"/>
<smd name="2" x="-2.9226" y="0" dx="2.1651" dy="0.9615" layer="1"/>
<smd name="3" x="-2.9226" y="-2.3" dx="2.1651" dy="0.9615" layer="1"/>
<smd name="4" x="2.9226" y="0" dx="2.1651" dy="3.2615" layer="1"/>
<text x="0" y="4.1697" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-3.985" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="RESC0603X30" urn="urn:adsk.eagle:footprint:3811152/1" library_version="47">
<description>CHIP, 0.6 X 0.3 X 0.3 mm body
&lt;p&gt;CHIP package with body size 0.6 X 0.3 X 0.3 mm&lt;/p&gt;</description>
<wire x1="0.315" y1="0.5124" x2="-0.315" y2="0.5124" width="0.12" layer="21"/>
<wire x1="0.315" y1="-0.5124" x2="-0.315" y2="-0.5124" width="0.12" layer="21"/>
<wire x1="0.315" y1="-0.165" x2="-0.315" y2="-0.165" width="0.12" layer="51"/>
<wire x1="-0.315" y1="-0.165" x2="-0.315" y2="0.165" width="0.12" layer="51"/>
<wire x1="-0.315" y1="0.165" x2="0.315" y2="0.165" width="0.12" layer="51"/>
<wire x1="0.315" y1="0.165" x2="0.315" y2="-0.165" width="0.12" layer="51"/>
<smd name="1" x="-0.325" y="0" dx="0.4469" dy="0.3969" layer="1"/>
<smd name="2" x="0.325" y="0" dx="0.4469" dy="0.3969" layer="1"/>
<text x="0" y="1.1474" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.1474" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="RESC1608X55" urn="urn:adsk.eagle:footprint:3811167/1" library_version="47">
<description>CHIP, 1.6 X 0.85 X 0.55 mm body
&lt;p&gt;CHIP package with body size 1.6 X 0.85 X 0.55 mm&lt;/p&gt;</description>
<wire x1="0.875" y1="0.8036" x2="-0.875" y2="0.8036" width="0.12" layer="21"/>
<wire x1="0.875" y1="-0.8036" x2="-0.875" y2="-0.8036" width="0.12" layer="21"/>
<wire x1="0.875" y1="-0.475" x2="-0.875" y2="-0.475" width="0.12" layer="51"/>
<wire x1="-0.875" y1="-0.475" x2="-0.875" y2="0.475" width="0.12" layer="51"/>
<wire x1="-0.875" y1="0.475" x2="0.875" y2="0.475" width="0.12" layer="51"/>
<wire x1="0.875" y1="0.475" x2="0.875" y2="-0.475" width="0.12" layer="51"/>
<smd name="1" x="-0.7996" y="0" dx="0.8709" dy="0.9791" layer="1"/>
<smd name="2" x="0.7996" y="0" dx="0.8709" dy="0.9791" layer="1"/>
<text x="0" y="1.4386" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.4386" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="RESC3115X70" urn="urn:adsk.eagle:footprint:3811174/1" library_version="47">
<description>CHIP, 3.125 X 1.55 X 0.7 mm body
&lt;p&gt;CHIP package with body size 3.125 X 1.55 X 0.7 mm&lt;/p&gt;</description>
<wire x1="1.625" y1="1.1536" x2="-1.625" y2="1.1536" width="0.12" layer="21"/>
<wire x1="1.625" y1="-1.1536" x2="-1.625" y2="-1.1536" width="0.12" layer="21"/>
<wire x1="1.625" y1="-0.825" x2="-1.625" y2="-0.825" width="0.12" layer="51"/>
<wire x1="-1.625" y1="-0.825" x2="-1.625" y2="0.825" width="0.12" layer="51"/>
<wire x1="-1.625" y1="0.825" x2="1.625" y2="0.825" width="0.12" layer="51"/>
<wire x1="1.625" y1="0.825" x2="1.625" y2="-0.825" width="0.12" layer="51"/>
<smd name="1" x="-1.4449" y="0" dx="1.0841" dy="1.6791" layer="1"/>
<smd name="2" x="1.4449" y="0" dx="1.0841" dy="1.6791" layer="1"/>
<text x="0" y="1.7886" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.7886" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="RESC3225X70" urn="urn:adsk.eagle:footprint:3811185/1" library_version="47">
<description>CHIP, 3.2 X 2.5 X 0.7 mm body
&lt;p&gt;CHIP package with body size 3.2 X 2.5 X 0.7 mm&lt;/p&gt;</description>
<wire x1="1.7" y1="1.6717" x2="-1.7" y2="1.6717" width="0.12" layer="21"/>
<wire x1="1.7" y1="-1.6717" x2="-1.7" y2="-1.6717" width="0.12" layer="21"/>
<wire x1="1.7" y1="-1.35" x2="-1.7" y2="-1.35" width="0.12" layer="51"/>
<wire x1="-1.7" y1="-1.35" x2="-1.7" y2="1.35" width="0.12" layer="51"/>
<wire x1="-1.7" y1="1.35" x2="1.7" y2="1.35" width="0.12" layer="51"/>
<wire x1="1.7" y1="1.35" x2="1.7" y2="-1.35" width="0.12" layer="51"/>
<smd name="1" x="-1.49" y="0" dx="1.1354" dy="2.7153" layer="1"/>
<smd name="2" x="1.49" y="0" dx="1.1354" dy="2.7153" layer="1"/>
<text x="0" y="2.3067" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.3067" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="RESC4532X70" urn="urn:adsk.eagle:footprint:3811192/1" library_version="47">
<description>CHIP, 4.5 X 3.2 X 0.7 mm body
&lt;p&gt;CHIP package with body size 4.5 X 3.2 X 0.7 mm&lt;/p&gt;</description>
<wire x1="2.35" y1="2.0217" x2="-2.35" y2="2.0217" width="0.12" layer="21"/>
<wire x1="2.35" y1="-2.0217" x2="-2.35" y2="-2.0217" width="0.12" layer="21"/>
<wire x1="2.35" y1="-1.7" x2="-2.35" y2="-1.7" width="0.12" layer="51"/>
<wire x1="-2.35" y1="-1.7" x2="-2.35" y2="1.7" width="0.12" layer="51"/>
<wire x1="-2.35" y1="1.7" x2="2.35" y2="1.7" width="0.12" layer="51"/>
<wire x1="2.35" y1="1.7" x2="2.35" y2="-1.7" width="0.12" layer="51"/>
<smd name="1" x="-2.14" y="0" dx="1.1354" dy="3.4153" layer="1"/>
<smd name="2" x="2.14" y="0" dx="1.1354" dy="3.4153" layer="1"/>
<text x="0" y="2.6567" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.6567" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="RESC5025X70" urn="urn:adsk.eagle:footprint:3811199/1" library_version="47">
<description>CHIP, 5 X 2.5 X 0.7 mm body
&lt;p&gt;CHIP package with body size 5 X 2.5 X 0.7 mm&lt;/p&gt;</description>
<wire x1="2.6" y1="1.6717" x2="-2.6" y2="1.6717" width="0.12" layer="21"/>
<wire x1="2.6" y1="-1.6717" x2="-2.6" y2="-1.6717" width="0.12" layer="21"/>
<wire x1="2.6" y1="-1.35" x2="-2.6" y2="-1.35" width="0.12" layer="51"/>
<wire x1="-2.6" y1="-1.35" x2="-2.6" y2="1.35" width="0.12" layer="51"/>
<wire x1="-2.6" y1="1.35" x2="2.6" y2="1.35" width="0.12" layer="51"/>
<wire x1="2.6" y1="1.35" x2="2.6" y2="-1.35" width="0.12" layer="51"/>
<smd name="1" x="-2.34" y="0" dx="1.2354" dy="2.7153" layer="1"/>
<smd name="2" x="2.34" y="0" dx="1.2354" dy="2.7153" layer="1"/>
<text x="0" y="2.3067" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.3067" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="RESC5750X229" urn="urn:adsk.eagle:footprint:3811213/1" library_version="47">
<description>CHIP, 5.7 X 5 X 2.29 mm body
&lt;p&gt;CHIP package with body size 5.7 X 5 X 2.29 mm&lt;/p&gt;</description>
<wire x1="3.05" y1="3.0179" x2="-3.05" y2="3.0179" width="0.12" layer="21"/>
<wire x1="3.05" y1="-3.0179" x2="-3.05" y2="-3.0179" width="0.12" layer="21"/>
<wire x1="3.05" y1="-2.7" x2="-3.05" y2="-2.7" width="0.12" layer="51"/>
<wire x1="-3.05" y1="-2.7" x2="-3.05" y2="2.7" width="0.12" layer="51"/>
<wire x1="-3.05" y1="2.7" x2="3.05" y2="2.7" width="0.12" layer="51"/>
<wire x1="3.05" y1="2.7" x2="3.05" y2="-2.7" width="0.12" layer="51"/>
<smd name="1" x="-2.6355" y="0" dx="1.5368" dy="5.4078" layer="1"/>
<smd name="2" x="2.6355" y="0" dx="1.5368" dy="5.4078" layer="1"/>
<text x="0" y="3.6529" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-3.6529" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="RESC6432X70" urn="urn:adsk.eagle:footprint:3811285/1" library_version="47">
<description>CHIP, 6.4 X 3.2 X 0.7 mm body
&lt;p&gt;CHIP package with body size 6.4 X 3.2 X 0.7 mm&lt;/p&gt;</description>
<wire x1="3.3" y1="2.0217" x2="-3.3" y2="2.0217" width="0.12" layer="21"/>
<wire x1="3.3" y1="-2.0217" x2="-3.3" y2="-2.0217" width="0.12" layer="21"/>
<wire x1="3.3" y1="-1.7" x2="-3.3" y2="-1.7" width="0.12" layer="51"/>
<wire x1="-3.3" y1="-1.7" x2="-3.3" y2="1.7" width="0.12" layer="51"/>
<wire x1="-3.3" y1="1.7" x2="3.3" y2="1.7" width="0.12" layer="51"/>
<wire x1="3.3" y1="1.7" x2="3.3" y2="-1.7" width="0.12" layer="51"/>
<smd name="1" x="-3.04" y="0" dx="1.2354" dy="3.4153" layer="1"/>
<smd name="2" x="3.04" y="0" dx="1.2354" dy="3.4153" layer="1"/>
<text x="0" y="2.6567" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.6567" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="RESC2037X52" urn="urn:adsk.eagle:footprint:3950970/1" library_version="47">
<description>CHIP, 2 X 3.75 X 0.52 mm body
&lt;p&gt;CHIP package with body size 2 X 3.75 X 0.52 mm&lt;/p&gt;</description>
<wire x1="1.1" y1="2.3442" x2="-1.1" y2="2.3442" width="0.12" layer="21"/>
<wire x1="1.1" y1="-2.3442" x2="-1.1" y2="-2.3442" width="0.12" layer="21"/>
<wire x1="1.1" y1="-2.025" x2="-1.1" y2="-2.025" width="0.12" layer="51"/>
<wire x1="-1.1" y1="-2.025" x2="-1.1" y2="2.025" width="0.12" layer="51"/>
<wire x1="-1.1" y1="2.025" x2="1.1" y2="2.025" width="0.12" layer="51"/>
<wire x1="1.1" y1="2.025" x2="1.1" y2="-2.025" width="0.12" layer="51"/>
<smd name="1" x="-0.94" y="0" dx="1.0354" dy="4.0603" layer="1"/>
<smd name="2" x="0.94" y="0" dx="1.0354" dy="4.0603" layer="1"/>
<text x="0" y="2.9792" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.9792" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC1005X60" urn="urn:adsk.eagle:footprint:3793110/1" library_version="47">
<description>CHIP, 1.025 X 0.525 X 0.6 mm body
&lt;p&gt;CHIP package with body size 1.025 X 0.525 X 0.6 mm&lt;/p&gt;</description>
<wire x1="0.55" y1="0.6325" x2="-0.55" y2="0.6325" width="0.12" layer="21"/>
<wire x1="0.55" y1="-0.6325" x2="-0.55" y2="-0.6325" width="0.12" layer="21"/>
<wire x1="0.55" y1="-0.3" x2="-0.55" y2="-0.3" width="0.12" layer="51"/>
<wire x1="-0.55" y1="-0.3" x2="-0.55" y2="0.3" width="0.12" layer="51"/>
<wire x1="-0.55" y1="0.3" x2="0.55" y2="0.3" width="0.12" layer="51"/>
<wire x1="0.55" y1="0.3" x2="0.55" y2="-0.3" width="0.12" layer="51"/>
<smd name="1" x="-0.4875" y="0" dx="0.5621" dy="0.6371" layer="1"/>
<smd name="2" x="0.4875" y="0" dx="0.5621" dy="0.6371" layer="1"/>
<text x="0" y="1.2675" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.2675" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC1220X107" urn="urn:adsk.eagle:footprint:3793123/1" library_version="47">
<description>CHIP, 1.25 X 2 X 1.07 mm body
&lt;p&gt;CHIP package with body size 1.25 X 2 X 1.07 mm&lt;/p&gt;</description>
<wire x1="0.725" y1="1.4217" x2="-0.725" y2="1.4217" width="0.12" layer="21"/>
<wire x1="0.725" y1="-1.4217" x2="-0.725" y2="-1.4217" width="0.12" layer="21"/>
<wire x1="0.725" y1="-1.1" x2="-0.725" y2="-1.1" width="0.12" layer="51"/>
<wire x1="-0.725" y1="-1.1" x2="-0.725" y2="1.1" width="0.12" layer="51"/>
<wire x1="-0.725" y1="1.1" x2="0.725" y2="1.1" width="0.12" layer="51"/>
<wire x1="0.725" y1="1.1" x2="0.725" y2="-1.1" width="0.12" layer="51"/>
<smd name="1" x="-0.552" y="0" dx="0.7614" dy="2.2153" layer="1"/>
<smd name="2" x="0.552" y="0" dx="0.7614" dy="2.2153" layer="1"/>
<text x="0" y="2.0567" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.0567" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC1632X168" urn="urn:adsk.eagle:footprint:3793133/1" library_version="47">
<description>CHIP, 1.6 X 3.2 X 1.68 mm body
&lt;p&gt;CHIP package with body size 1.6 X 3.2 X 1.68 mm&lt;/p&gt;</description>
<wire x1="0.9" y1="2.0217" x2="-0.9" y2="2.0217" width="0.12" layer="21"/>
<wire x1="0.9" y1="-2.0217" x2="-0.9" y2="-2.0217" width="0.12" layer="21"/>
<wire x1="0.9" y1="-1.7" x2="-0.9" y2="-1.7" width="0.12" layer="51"/>
<wire x1="-0.9" y1="-1.7" x2="-0.9" y2="1.7" width="0.12" layer="51"/>
<wire x1="-0.9" y1="1.7" x2="0.9" y2="1.7" width="0.12" layer="51"/>
<wire x1="0.9" y1="1.7" x2="0.9" y2="-1.7" width="0.12" layer="51"/>
<smd name="1" x="-0.786" y="0" dx="0.9434" dy="3.4153" layer="1"/>
<smd name="2" x="0.786" y="0" dx="0.9434" dy="3.4153" layer="1"/>
<text x="0" y="2.6567" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.6567" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC3215X168" urn="urn:adsk.eagle:footprint:3793146/1" library_version="47">
<description>CHIP, 3.2 X 1.5 X 1.68 mm body
&lt;p&gt;CHIP package with body size 3.2 X 1.5 X 1.68 mm&lt;/p&gt;</description>
<wire x1="1.7" y1="1.1286" x2="-1.7" y2="1.1286" width="0.12" layer="21"/>
<wire x1="1.7" y1="-1.1286" x2="-1.7" y2="-1.1286" width="0.12" layer="21"/>
<wire x1="1.7" y1="-0.8" x2="-1.7" y2="-0.8" width="0.12" layer="51"/>
<wire x1="-1.7" y1="-0.8" x2="-1.7" y2="0.8" width="0.12" layer="51"/>
<wire x1="-1.7" y1="0.8" x2="1.7" y2="0.8" width="0.12" layer="51"/>
<wire x1="1.7" y1="0.8" x2="1.7" y2="-0.8" width="0.12" layer="51"/>
<smd name="1" x="-1.4913" y="0" dx="1.1327" dy="1.6291" layer="1"/>
<smd name="2" x="1.4913" y="0" dx="1.1327" dy="1.6291" layer="1"/>
<text x="0" y="1.7636" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.7636" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC3225X168" urn="urn:adsk.eagle:footprint:3793150/1" library_version="47">
<description>CHIP, 3.2 X 2.5 X 1.68 mm body
&lt;p&gt;CHIP package with body size 3.2 X 2.5 X 1.68 mm&lt;/p&gt;</description>
<wire x1="1.7" y1="1.6717" x2="-1.7" y2="1.6717" width="0.12" layer="21"/>
<wire x1="1.7" y1="-1.6717" x2="-1.7" y2="-1.6717" width="0.12" layer="21"/>
<wire x1="1.7" y1="-1.35" x2="-1.7" y2="-1.35" width="0.12" layer="51"/>
<wire x1="-1.7" y1="-1.35" x2="-1.7" y2="1.35" width="0.12" layer="51"/>
<wire x1="-1.7" y1="1.35" x2="1.7" y2="1.35" width="0.12" layer="51"/>
<wire x1="1.7" y1="1.35" x2="1.7" y2="-1.35" width="0.12" layer="51"/>
<smd name="1" x="-1.4913" y="0" dx="1.1327" dy="2.7153" layer="1"/>
<smd name="2" x="1.4913" y="0" dx="1.1327" dy="2.7153" layer="1"/>
<text x="0" y="2.3067" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.3067" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC4520X168" urn="urn:adsk.eagle:footprint:3793156/1" library_version="47">
<description>CHIP, 4.5 X 2.03 X 1.68 mm body
&lt;p&gt;CHIP package with body size 4.5 X 2.03 X 1.68 mm&lt;/p&gt;</description>
<wire x1="2.375" y1="1.4602" x2="-2.375" y2="1.4602" width="0.12" layer="21"/>
<wire x1="2.375" y1="-1.4602" x2="-2.375" y2="-1.4602" width="0.12" layer="21"/>
<wire x1="2.375" y1="-1.14" x2="-2.375" y2="-1.14" width="0.12" layer="51"/>
<wire x1="-2.375" y1="-1.14" x2="-2.375" y2="1.14" width="0.12" layer="51"/>
<wire x1="-2.375" y1="1.14" x2="2.375" y2="1.14" width="0.12" layer="51"/>
<wire x1="2.375" y1="1.14" x2="2.375" y2="-1.14" width="0.12" layer="51"/>
<smd name="1" x="-2.1266" y="0" dx="1.2091" dy="2.2923" layer="1"/>
<smd name="2" x="2.1266" y="0" dx="1.2091" dy="2.2923" layer="1"/>
<text x="0" y="2.0952" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.0952" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC4532X218" urn="urn:adsk.eagle:footprint:3793162/1" library_version="47">
<description>CHIP, 4.5 X 3.2 X 2.18 mm body
&lt;p&gt;CHIP package with body size 4.5 X 3.2 X 2.18 mm&lt;/p&gt;</description>
<wire x1="2.375" y1="2.0217" x2="-2.375" y2="2.0217" width="0.12" layer="21"/>
<wire x1="2.375" y1="-2.0217" x2="-2.375" y2="-2.0217" width="0.12" layer="21"/>
<wire x1="2.375" y1="-1.7" x2="-2.375" y2="-1.7" width="0.12" layer="51"/>
<wire x1="-2.375" y1="-1.7" x2="-2.375" y2="1.7" width="0.12" layer="51"/>
<wire x1="-2.375" y1="1.7" x2="2.375" y2="1.7" width="0.12" layer="51"/>
<wire x1="2.375" y1="1.7" x2="2.375" y2="-1.7" width="0.12" layer="51"/>
<smd name="1" x="-2.1266" y="0" dx="1.2091" dy="3.4153" layer="1"/>
<smd name="2" x="2.1266" y="0" dx="1.2091" dy="3.4153" layer="1"/>
<text x="0" y="2.6567" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.6567" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC4564X218" urn="urn:adsk.eagle:footprint:3793169/1" library_version="47">
<description>CHIP, 4.5 X 6.4 X 2.18 mm body
&lt;p&gt;CHIP package with body size 4.5 X 6.4 X 2.18 mm&lt;/p&gt;</description>
<wire x1="2.375" y1="3.6452" x2="-2.375" y2="3.6452" width="0.12" layer="21"/>
<wire x1="2.375" y1="-3.6452" x2="-2.375" y2="-3.6452" width="0.12" layer="21"/>
<wire x1="2.375" y1="-3.325" x2="-2.375" y2="-3.325" width="0.12" layer="51"/>
<wire x1="-2.375" y1="-3.325" x2="-2.375" y2="3.325" width="0.12" layer="51"/>
<wire x1="-2.375" y1="3.325" x2="2.375" y2="3.325" width="0.12" layer="51"/>
<wire x1="2.375" y1="3.325" x2="2.375" y2="-3.325" width="0.12" layer="51"/>
<smd name="1" x="-2.1266" y="0" dx="1.2091" dy="6.6623" layer="1"/>
<smd name="2" x="2.1266" y="0" dx="1.2091" dy="6.6623" layer="1"/>
<text x="0" y="4.2802" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-4.2802" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC5650X218" urn="urn:adsk.eagle:footprint:3793172/1" library_version="47">
<description>CHIP, 5.64 X 5.08 X 2.18 mm body
&lt;p&gt;CHIP package with body size 5.64 X 5.08 X 2.18 mm&lt;/p&gt;</description>
<wire x1="2.895" y1="2.9852" x2="-2.895" y2="2.9852" width="0.12" layer="21"/>
<wire x1="2.895" y1="-2.9852" x2="-2.895" y2="-2.9852" width="0.12" layer="21"/>
<wire x1="2.895" y1="-2.665" x2="-2.895" y2="-2.665" width="0.12" layer="51"/>
<wire x1="-2.895" y1="-2.665" x2="-2.895" y2="2.665" width="0.12" layer="51"/>
<wire x1="-2.895" y1="2.665" x2="2.895" y2="2.665" width="0.12" layer="51"/>
<wire x1="2.895" y1="2.665" x2="2.895" y2="-2.665" width="0.12" layer="51"/>
<smd name="1" x="-2.6854" y="0" dx="1.1393" dy="5.3423" layer="1"/>
<smd name="2" x="2.6854" y="0" dx="1.1393" dy="5.3423" layer="1"/>
<text x="0" y="3.6202" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-3.6202" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC5563X218" urn="urn:adsk.eagle:footprint:3793175/1" library_version="47">
<description>CHIP, 5.59 X 6.35 X 2.18 mm body
&lt;p&gt;CHIP package with body size 5.59 X 6.35 X 2.18 mm&lt;/p&gt;</description>
<wire x1="2.92" y1="3.6202" x2="-2.92" y2="3.6202" width="0.12" layer="21"/>
<wire x1="2.92" y1="-3.6202" x2="-2.92" y2="-3.6202" width="0.12" layer="21"/>
<wire x1="2.92" y1="-3.3" x2="-2.92" y2="-3.3" width="0.12" layer="51"/>
<wire x1="-2.92" y1="-3.3" x2="-2.92" y2="3.3" width="0.12" layer="51"/>
<wire x1="-2.92" y1="3.3" x2="2.92" y2="3.3" width="0.12" layer="51"/>
<wire x1="2.92" y1="3.3" x2="2.92" y2="-3.3" width="0.12" layer="51"/>
<smd name="1" x="-2.6716" y="0" dx="1.2091" dy="6.6123" layer="1"/>
<smd name="2" x="2.6716" y="0" dx="1.2091" dy="6.6123" layer="1"/>
<text x="0" y="4.2552" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-4.2552" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC9198X218" urn="urn:adsk.eagle:footprint:3793179/1" library_version="47">
<description>CHIP, 9.14 X 9.82 X 2.18 mm body
&lt;p&gt;CHIP package with body size 9.14 X 9.82 X 2.18 mm&lt;/p&gt;</description>
<wire x1="4.76" y1="5.2799" x2="-4.76" y2="5.2799" width="0.12" layer="21"/>
<wire x1="4.76" y1="-5.2799" x2="-4.76" y2="-5.2799" width="0.12" layer="21"/>
<wire x1="4.76" y1="-4.91" x2="-4.76" y2="-4.91" width="0.12" layer="51"/>
<wire x1="-4.76" y1="-4.91" x2="-4.76" y2="4.91" width="0.12" layer="51"/>
<wire x1="-4.76" y1="4.91" x2="4.76" y2="4.91" width="0.12" layer="51"/>
<wire x1="4.76" y1="4.91" x2="4.76" y2="-4.91" width="0.12" layer="51"/>
<smd name="1" x="-4.4571" y="0" dx="1.314" dy="9.9318" layer="1"/>
<smd name="2" x="4.4571" y="0" dx="1.314" dy="9.9318" layer="1"/>
<text x="0" y="5.9149" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-5.9149" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="RESC1005X40" urn="urn:adsk.eagle:package:3811154/1" type="model" library_version="47">
<description>CHIP, 1 X 0.5 X 0.4 mm body
&lt;p&gt;CHIP package with body size 1 X 0.5 X 0.4 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESC1005X40"/>
</packageinstances>
</package3d>
<package3d name="RESC2012X70" urn="urn:adsk.eagle:package:3811169/1" type="model" library_version="47">
<description>CHIP, 2 X 1.25 X 0.7 mm body
&lt;p&gt;CHIP package with body size 2 X 1.25 X 0.7 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESC2012X70"/>
</packageinstances>
</package3d>
<package3d name="CAPC2012X127" urn="urn:adsk.eagle:package:3793139/1" type="model" library_version="47">
<description>CHIP, 2.01 X 1.25 X 1.27 mm body
&lt;p&gt;CHIP package with body size 2.01 X 1.25 X 1.27 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC2012X127"/>
</packageinstances>
</package3d>
<package3d name="CAPC1608X55" urn="urn:adsk.eagle:package:3793126/2" type="model" library_version="43">
<description>CHIP, 1.6 X 0.8 X 0.55 mm body
&lt;p&gt;CHIP package with body size 1.6 X 0.8 X 0.55 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC1608X55"/>
</packageinstances>
</package3d>
<package3d name="SOP50P490X110-10" urn="urn:adsk.eagle:package:3921298/1" type="model" library_version="47">
<description>10-SOP, 0.5 mm pitch, 4.9 mm span, 3 X 3 X 1.1 mm body
&lt;p&gt;10-pin SOP package with 0.5 mm pitch, 4.9 mm span with body size 3 X 3 X 1.1 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="SOP50P490X110-10"/>
</packageinstances>
</package3d>
<package3d name="TO457P1000X238-3" urn="urn:adsk.eagle:package:3920001/3" type="model" library_version="47">
<description>3-TO, DPAK, 4.57 mm pitch, 10 mm span, 6.6 X 6.1 X 2.38 mm body
&lt;p&gt;3-pin TO, DPAK package with 4.57 mm pitch, 10 mm span with body size 6.6 X 6.1 X 2.38 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="TO457P1000X238-3"/>
</packageinstances>
</package3d>
<package3d name="POWERPAK_SO-8" urn="urn:adsk.eagle:package:8414082/1" type="box" library_version="43">
<packageinstances>
<packageinstance name="POWERPAK_SO-8"/>
</packageinstances>
</package3d>
<package3d name="DO214AC_SMA" urn="urn:adsk.eagle:package:15671034/4" type="model" library_version="47">
<packageinstances>
<packageinstance name="DO214AC_SMA"/>
</packageinstances>
</package3d>
<package3d name="DIOC5226X110N" urn="urn:adsk.eagle:package:16113232/1" type="model" library_version="47">
<description>Chip, 5.20 X 2.60 X 1.10 mm body
&lt;p&gt;Chip package with body size 5.20 X 2.60 X 1.10 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="DIOC5226X110N"/>
</packageinstances>
</package3d>
<package3d name="TP_1.5MM" urn="urn:adsk.eagle:package:8414069/2" type="empty" library_version="44">
<packageinstances>
<packageinstance name="TP_1.5MM"/>
</packageinstances>
</package3d>
<package3d name="TP_1.5MM_PAD" urn="urn:adsk.eagle:package:8414070/2" type="empty" library_version="44">
<packageinstances>
<packageinstance name="TP_1.5MM_PAD"/>
</packageinstances>
</package3d>
<package3d name="TP_1.7MM" urn="urn:adsk.eagle:package:8414071/2" type="empty" library_version="44">
<packageinstances>
<packageinstance name="TP_1.7MM"/>
</packageinstances>
</package3d>
<package3d name="C120H40" urn="urn:adsk.eagle:package:8414063/2" type="empty" library_version="44">
<packageinstances>
<packageinstance name="C120H40"/>
</packageinstances>
</package3d>
<package3d name="C131H51" urn="urn:adsk.eagle:package:8414064/2" type="empty" library_version="44">
<packageinstances>
<packageinstance name="C131H51"/>
</packageinstances>
</package3d>
<package3d name="C406H326" urn="urn:adsk.eagle:package:8414065/2" type="empty" library_version="44">
<packageinstances>
<packageinstance name="C406H326"/>
</packageinstances>
</package3d>
<package3d name="C491H411" urn="urn:adsk.eagle:package:8414066/2" type="empty" library_version="44">
<packageinstances>
<packageinstance name="C491H411"/>
</packageinstances>
</package3d>
<package3d name="CIR_PAD_1MM" urn="urn:adsk.eagle:package:8414067/2" type="empty" library_version="44">
<packageinstances>
<packageinstance name="CIR_PAD_1MM"/>
</packageinstances>
</package3d>
<package3d name="CIR_PAD_2MM" urn="urn:adsk.eagle:package:8414068/2" type="empty" library_version="44">
<packageinstances>
<packageinstance name="CIR_PAD_2MM"/>
</packageinstances>
</package3d>
<package3d name="DIOM5226X292" urn="urn:adsk.eagle:package:3793189/2" type="model" library_version="47">
<description>MOLDED BODY, 5.265 X 2.6 X 2.92 mm body
&lt;p&gt;MOLDED BODY package with body size 5.265 X 2.6 X 2.92 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="DIOM5226X292"/>
</packageinstances>
</package3d>
<package3d name="DIOM5436X265" urn="urn:adsk.eagle:package:3793201/2" type="model" library_version="47">
<description>MOLDED BODY, 5.4 X 3.6 X 2.65 mm body
&lt;p&gt;MOLDED BODY package with body size 5.4 X 3.6 X 2.65 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="DIOM5436X265"/>
</packageinstances>
</package3d>
<package3d name="DIOM7959X265" urn="urn:adsk.eagle:package:3793207/2" type="model" library_version="47">
<description>MOLDED BODY, 7.94 X 5.95 X 2.65 mm body
&lt;p&gt;MOLDED BODY package with body size 7.94 X 5.95 X 2.65 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="DIOM7959X265"/>
</packageinstances>
</package3d>
<package3d name="SOD2512X100" urn="urn:adsk.eagle:package:3793216/2" type="model" library_version="47">
<description>SOD, 2.5 mm span, 1.7 X 1.25 X 1 mm body
&lt;p&gt;SOD package with 2.5 mm span with body size 1.7 X 1.25 X 1 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="SOD2512X100"/>
</packageinstances>
</package3d>
<package3d name="SOD3716X135" urn="urn:adsk.eagle:package:3793231/2" type="model" library_version="47">
<description>SOD, 3.7 mm span, 2.7 X 1.6 X 1.35 mm body
&lt;p&gt;SOD package with 3.7 mm span with body size 2.7 X 1.6 X 1.35 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="SOD3716X135"/>
</packageinstances>
</package3d>
<package3d name="SOT65P210X110-3" urn="urn:adsk.eagle:package:3809951/2" type="model" library_version="47">
<description>3-SOT23, 0.65 mm pitch, 2.1 mm span, 2 X 1.25 X 1.1 mm body
&lt;p&gt;3-pin SOT23 package with 0.65 mm pitch, 2.1 mm span with body size 2 X 1.25 X 1.1 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="SOT65P210X110-3"/>
</packageinstances>
</package3d>
<package3d name="SOT95P280X135-3" urn="urn:adsk.eagle:package:3809961/2" type="model" library_version="47">
<description>3-SOT23, 0.95 mm pitch, 2.8 mm span, 2.9 X 1.6 X 1.35 mm body
&lt;p&gt;3-pin SOT23 package with 0.95 mm pitch, 2.8 mm span with body size 2.9 X 1.6 X 1.35 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="SOT95P280X135-3"/>
</packageinstances>
</package3d>
<package3d name="SODFL4725X110" urn="urn:adsk.eagle:package:3921253/2" type="model" library_version="47">
<description>SODFL, 4.7 mm span, 3.8 X 2.5 X 1.1 mm body
&lt;p&gt;SODFL package with 4.7 mm span with body size 3.8 X 2.5 X 1.1 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="SODFL4725X110"/>
</packageinstances>
</package3d>
<package3d name="DIOC0502X50" urn="urn:adsk.eagle:package:3948086/2" type="model" library_version="47">
<description>CHIP, 0.5 X 0.25 X 0.5 mm body
&lt;p&gt;CHIP package with body size 0.5 X 0.25 X 0.5 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="DIOC0502X50"/>
</packageinstances>
</package3d>
<package3d name="DIOC1005X84" urn="urn:adsk.eagle:package:3948100/2" type="model" library_version="47">
<description>CHIP, 1.07 X 0.56 X 0.84 mm body
&lt;p&gt;CHIP package with body size 1.07 X 0.56 X 0.84 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="DIOC1005X84"/>
</packageinstances>
</package3d>
<package3d name="DIOC1206X63" urn="urn:adsk.eagle:package:3948141/2" type="model" library_version="47">
<description>CHIP, 1.27 X 0.6 X 0.63 mm body
&lt;p&gt;CHIP package with body size 1.27 X 0.6 X 0.63 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="DIOC1206X63"/>
</packageinstances>
</package3d>
<package3d name="DIOC1608X84" urn="urn:adsk.eagle:package:3948147/2" type="model" library_version="47">
<description>CHIP, 1.63 X 0.81 X 0.84 mm body
&lt;p&gt;CHIP package with body size 1.63 X 0.81 X 0.84 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="DIOC1608X84"/>
</packageinstances>
</package3d>
<package3d name="DIOC2012X70" urn="urn:adsk.eagle:package:3948150/2" type="model" library_version="47">
<description>CHIP, 2 X 1.25 X 0.7 mm body
&lt;p&gt;CHIP package with body size 2 X 1.25 X 0.7 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="DIOC2012X70"/>
</packageinstances>
</package3d>
<package3d name="DIOC3216X84" urn="urn:adsk.eagle:package:3948168/2" type="model" library_version="47">
<description>CHIP, 3.2 X 1.6 X 0.84 mm body
&lt;p&gt;CHIP package with body size 3.2 X 1.6 X 0.84 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="DIOC3216X84"/>
</packageinstances>
</package3d>
<package3d name="DIOC5324X84" urn="urn:adsk.eagle:package:3948171/2" type="model" library_version="47">
<description>CHIP, 5.31 X 2.49 X 0.84 mm body
&lt;p&gt;CHIP package with body size 5.31 X 2.49 X 0.84 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="DIOC5324X84"/>
</packageinstances>
</package3d>
<package3d name="SOIC127P600X175-8" urn="urn:adsk.eagle:package:3833977/2" type="model" library_version="47">
<description>8-SOIC, 1.27 mm pitch, 6 mm span, 4.9 X 3.9 X 1.75 mm body
&lt;p&gt;8-pin SOIC package with 1.27 mm pitch, 6 mm span with body size 4.9 X 3.9 X 1.75 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="SOIC127P600X175-8"/>
</packageinstances>
</package3d>
<package3d name="SOT230P700X180-4" urn="urn:adsk.eagle:package:3834044/1" type="model" library_version="47">
<description>4-SOT223, 2.3 mm pitch, 7 mm span, 6.5 X 3.5 X 1.8 mm body
&lt;p&gt;4-pin SOT223 package with 2.3 mm pitch, 7 mm span with body size 6.5 X 3.5 X 1.8 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="SOT230P700X180-4"/>
</packageinstances>
</package3d>
<package3d name="RESC0603X30" urn="urn:adsk.eagle:package:3811151/1" type="model" library_version="47">
<description>CHIP, 0.6 X 0.3 X 0.3 mm body
&lt;p&gt;CHIP package with body size 0.6 X 0.3 X 0.3 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESC0603X30"/>
</packageinstances>
</package3d>
<package3d name="RESC1608X55" urn="urn:adsk.eagle:package:3811166/1" type="model" library_version="47">
<description>CHIP, 1.6 X 0.85 X 0.55 mm body
&lt;p&gt;CHIP package with body size 1.6 X 0.85 X 0.55 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESC1608X55"/>
</packageinstances>
</package3d>
<package3d name="RESC3115X70" urn="urn:adsk.eagle:package:3811173/1" type="model" library_version="47">
<description>CHIP, 3.125 X 1.55 X 0.7 mm body
&lt;p&gt;CHIP package with body size 3.125 X 1.55 X 0.7 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESC3115X70"/>
</packageinstances>
</package3d>
<package3d name="RESC3225X70" urn="urn:adsk.eagle:package:3811180/1" type="model" library_version="47">
<description>CHIP, 3.2 X 2.5 X 0.7 mm body
&lt;p&gt;CHIP package with body size 3.2 X 2.5 X 0.7 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESC3225X70"/>
</packageinstances>
</package3d>
<package3d name="RESC4532X70" urn="urn:adsk.eagle:package:3811188/1" type="model" library_version="47">
<description>CHIP, 4.5 X 3.2 X 0.7 mm body
&lt;p&gt;CHIP package with body size 4.5 X 3.2 X 0.7 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESC4532X70"/>
</packageinstances>
</package3d>
<package3d name="RESC5025X70" urn="urn:adsk.eagle:package:3811197/1" type="model" library_version="47">
<description>CHIP, 5 X 2.5 X 0.7 mm body
&lt;p&gt;CHIP package with body size 5 X 2.5 X 0.7 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESC5025X70"/>
</packageinstances>
</package3d>
<package3d name="RESC5750X229" urn="urn:adsk.eagle:package:3811212/1" type="model" library_version="47">
<description>CHIP, 5.7 X 5 X 2.29 mm body
&lt;p&gt;CHIP package with body size 5.7 X 5 X 2.29 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESC5750X229"/>
</packageinstances>
</package3d>
<package3d name="RESC6432X70" urn="urn:adsk.eagle:package:3811284/1" type="model" library_version="47">
<description>CHIP, 6.4 X 3.2 X 0.7 mm body
&lt;p&gt;CHIP package with body size 6.4 X 3.2 X 0.7 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESC6432X70"/>
</packageinstances>
</package3d>
<package3d name="RESC2037X52" urn="urn:adsk.eagle:package:3950966/1" type="model" library_version="47">
<description>CHIP, 2 X 3.75 X 0.52 mm body
&lt;p&gt;CHIP package with body size 2 X 3.75 X 0.52 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESC2037X52"/>
</packageinstances>
</package3d>
<package3d name="CAPC1005X60" urn="urn:adsk.eagle:package:3793108/1" type="model" library_version="47">
<description>CHIP, 1.025 X 0.525 X 0.6 mm body
&lt;p&gt;CHIP package with body size 1.025 X 0.525 X 0.6 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC1005X60"/>
</packageinstances>
</package3d>
<package3d name="CAPC1220X107" urn="urn:adsk.eagle:package:3793115/1" type="model" library_version="47">
<description>CHIP, 1.25 X 2 X 1.07 mm body
&lt;p&gt;CHIP package with body size 1.25 X 2 X 1.07 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC1220X107"/>
</packageinstances>
</package3d>
<package3d name="CAPC1632X168" urn="urn:adsk.eagle:package:3793131/1" type="model" library_version="47">
<description>CHIP, 1.6 X 3.2 X 1.68 mm body
&lt;p&gt;CHIP package with body size 1.6 X 3.2 X 1.68 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC1632X168"/>
</packageinstances>
</package3d>
<package3d name="CAPC3215X168" urn="urn:adsk.eagle:package:3793145/1" type="model" library_version="47">
<description>CHIP, 3.2 X 1.5 X 1.68 mm body
&lt;p&gt;CHIP package with body size 3.2 X 1.5 X 1.68 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC3215X168"/>
</packageinstances>
</package3d>
<package3d name="CAPC3225X168" urn="urn:adsk.eagle:package:3793149/1" type="model" library_version="47">
<description>CHIP, 3.2 X 2.5 X 1.68 mm body
&lt;p&gt;CHIP package with body size 3.2 X 2.5 X 1.68 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC3225X168"/>
</packageinstances>
</package3d>
<package3d name="CAPC4520X168" urn="urn:adsk.eagle:package:3793154/1" type="model" library_version="47">
<description>CHIP, 4.5 X 2.03 X 1.68 mm body
&lt;p&gt;CHIP package with body size 4.5 X 2.03 X 1.68 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC4520X168"/>
</packageinstances>
</package3d>
<package3d name="CAPC4532X218" urn="urn:adsk.eagle:package:3793159/1" type="model" library_version="47">
<description>CHIP, 4.5 X 3.2 X 2.18 mm body
&lt;p&gt;CHIP package with body size 4.5 X 3.2 X 2.18 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC4532X218"/>
</packageinstances>
</package3d>
<package3d name="CAPC4564X218" urn="urn:adsk.eagle:package:3793166/1" type="model" library_version="47">
<description>CHIP, 4.5 X 6.4 X 2.18 mm body
&lt;p&gt;CHIP package with body size 4.5 X 6.4 X 2.18 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC4564X218"/>
</packageinstances>
</package3d>
<package3d name="CAPC5650X218" urn="urn:adsk.eagle:package:3793171/1" type="model" library_version="47">
<description>CHIP, 5.64 X 5.08 X 2.18 mm body
&lt;p&gt;CHIP package with body size 5.64 X 5.08 X 2.18 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC5650X218"/>
</packageinstances>
</package3d>
<package3d name="CAPC5563X218" urn="urn:adsk.eagle:package:3793174/1" type="model" library_version="47">
<description>CHIP, 5.59 X 6.35 X 2.18 mm body
&lt;p&gt;CHIP package with body size 5.59 X 6.35 X 2.18 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC5563X218"/>
</packageinstances>
</package3d>
<package3d name="CAPC9198X218" urn="urn:adsk.eagle:package:3793178/1" type="model" library_version="47">
<description>CHIP, 9.14 X 9.82 X 2.18 mm body
&lt;p&gt;CHIP package with body size 9.14 X 9.82 X 2.18 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC9198X218"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="RESISTOR" urn="urn:adsk.eagle:symbol:8413611/1" library_version="43">
<text x="-2.54" y="1.524" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.2032" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.2032" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.2032" layer="94"/>
</symbol>
<symbol name="CAPACITOR" urn="urn:adsk.eagle:symbol:8413617/1" library_version="43">
<wire x1="-2.54" y1="0" x2="-0.762" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="0.762" y2="0" width="0.1524" layer="94"/>
<text x="-2.54" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-4.318" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-1.524" y1="-0.254" x2="2.54" y2="0.254" layer="94" rot="R90"/>
<rectangle x1="-2.54" y1="-0.254" x2="1.524" y2="0.254" layer="94" rot="R90"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="TI_TPS2491DGSR" urn="urn:adsk.eagle:symbol:8413655/1" library_version="43">
<wire x1="20.32" y1="0" x2="20.32" y2="-10.16" width="0.254" layer="94"/>
<wire x1="20.32" y1="-10.16" x2="0" y2="-10.16" width="0.254" layer="94"/>
<wire x1="0" y1="-10.16" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="20.32" y2="0" width="0.254" layer="94"/>
<pin name="10_VCC" x="-2.54" y="-2.54" visible="pad" length="short"/>
<pin name="1_EN" x="-2.54" y="-5.08" visible="pad" length="short"/>
<pin name="2_VREF" x="-2.54" y="-7.62" visible="pad" length="short"/>
<pin name="3_PROG" x="5.08" y="-12.7" visible="pad" length="short" rot="R90"/>
<pin name="4_TIMER" x="10.16" y="-12.7" visible="pad" length="short" rot="R90"/>
<pin name="5_GND" x="15.24" y="-12.7" visible="pad" length="short" rot="R90"/>
<pin name="6_PG" x="22.86" y="-2.54" visible="pad" length="short" rot="R180"/>
<pin name="9_SENSE" x="5.08" y="2.54" visible="pad" length="short" rot="R270"/>
<pin name="8_GATE" x="10.16" y="2.54" visible="pad" length="short" rot="R270"/>
<pin name="7_OUT" x="15.24" y="2.54" visible="pad" length="short" rot="R270"/>
<text x="7.112" y="-5.08" size="1.27" layer="94">TPS2491</text>
<text x="3.556" y="-1.27" size="0.8128" layer="94">SENSE</text>
<text x="8.89" y="-1.27" size="0.8128" layer="94">GATE</text>
<text x="14.224" y="-1.27" size="0.8128" layer="94">OUT</text>
<text x="18.542" y="-2.794" size="0.8128" layer="94">PG</text>
<text x="14.224" y="-9.652" size="0.8128" layer="94">GND</text>
<text x="8.89" y="-9.652" size="0.8128" layer="94">TIMER</text>
<text x="3.81" y="-9.652" size="0.8128" layer="94">PROG</text>
<text x="0.508" y="-8.128" size="0.8128" layer="94">VREF</text>
<text x="0.508" y="-5.588" size="0.8128" layer="94">EN</text>
<text x="0.508" y="-3.048" size="0.8128" layer="94">VCC</text>
<text x="-2.54" y="2.54" size="1.27" layer="95" ratio="15">&gt;NAME</text>
<text x="-2.54" y="5.08" size="1.27" layer="95" ratio="15">&gt;VALUE</text>
</symbol>
<symbol name="MOSFET-NCH" urn="urn:adsk.eagle:symbol:8413608/1" library_version="43">
<wire x1="-2.54" y1="-2.54" x2="-1.2192" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="0.762" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-0.762" width="0.254" layer="94"/>
<wire x1="0" y1="3.683" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="1.397" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.397" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-3.683" width="0.254" layer="94"/>
<wire x1="-1.143" y1="2.54" x2="-1.143" y2="-2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="0.508" width="0.1524" layer="94"/>
<wire x1="5.08" y1="0.508" x2="5.08" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="5.08" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="2.54" x2="5.08" y2="2.54" width="0.1524" layer="94"/>
<wire x1="5.588" y1="0.508" x2="5.08" y2="0.508" width="0.1524" layer="94"/>
<wire x1="5.08" y1="0.508" x2="4.572" y2="0.508" width="0.1524" layer="94"/>
<circle x="2.54" y="-2.54" radius="0.3592" width="0" layer="94"/>
<circle x="2.54" y="2.54" radius="0.3592" width="0" layer="94"/>
<text x="-11.43" y="0" size="1.778" layer="96">&gt;VALUE</text>
<text x="-11.43" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<pin name="S" x="2.54" y="-7.62" visible="off" length="middle" direction="pas" rot="R90"/>
<pin name="G" x="-5.08" y="-2.54" visible="off" length="short" direction="pas"/>
<pin name="D" x="2.54" y="7.62" visible="off" length="middle" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="5.08" y="0.508"/>
<vertex x="4.572" y="-0.254"/>
<vertex x="5.588" y="-0.254"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="0.254" y="0"/>
<vertex x="1.27" y="0.762"/>
<vertex x="1.27" y="-0.762"/>
</polygon>
</symbol>
<symbol name="DIODE_SCHOTTKY" urn="urn:adsk.eagle:symbol:8413616/1" library_version="43">
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.905" y1="1.27" x2="1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.905" y1="1.27" x2="1.905" y2="1.016" width="0.254" layer="94"/>
<wire x1="1.27" y1="-1.27" x2="0.635" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0.635" y1="-1.016" x2="0.635" y2="-1.27" width="0.254" layer="94"/>
<text x="-2.54" y="1.905" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.778" layer="96">&gt;VALUE</text>
<pin name="A" x="-3.81" y="0" visible="off" length="short" direction="pas"/>
<pin name="K" x="3.81" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="DIODE" urn="urn:adsk.eagle:symbol:8413615/1" library_version="43">
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<text x="-2.54" y="1.905" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.778" layer="96">&gt;VALUE</text>
<pin name="A" x="-3.81" y="0" visible="off" length="short" direction="pas"/>
<pin name="K" x="3.81" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="TP" urn="urn:adsk.eagle:symbol:8413657/1" library_version="44">
<wire x1="0.762" y1="1.778" x2="0" y2="0.254" width="0.254" layer="94"/>
<wire x1="0" y1="0.254" x2="-0.762" y2="1.778" width="0.254" layer="94"/>
<wire x1="-0.762" y1="1.778" x2="0.762" y2="1.778" width="0.254" layer="94" curve="-180"/>
<text x="-1.27" y="3.81" size="1.778" layer="95">&gt;NAME</text>
<pin name="PP" x="0" y="0" visible="off" length="short" direction="in" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="RESISTOR" urn="urn:adsk.eagle:component:8414463/1" prefix="R" uservalue="yes" library_version="47">
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="0201" package="RESC0603X30">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3811151/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402" package="RESC1005X40">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3811154/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="RESC1608X55">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3811166/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="RESC2012X70">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3811169/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="RESC3115X70">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3811173/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1210" package="RESC3225X70">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3811180/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1812" package="RESC4532X70">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3811188/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2010" package="RESC5025X70">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3811197/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2220" package="RESC5750X229">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3811212/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2512" package="RESC6432X70">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3811284/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1508_WIDE" package="RESC2037X52">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3950966/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAPACITOR" urn="urn:adsk.eagle:component:8414470/2" prefix="C" uservalue="yes" library_version="47">
<gates>
<gate name="G$1" symbol="CAPACITOR" x="0" y="0"/>
</gates>
<devices>
<device name="0402" package="CAPC1005X60">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793108/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0508" package="CAPC1220X107">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793115/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="CAPC1608X55">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793126/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0612" package="CAPC1632X168">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793131/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="CAPC2012X127">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793139/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="CAPC3215X168">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793145/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1210" package="CAPC3225X168">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793149/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1808" package="CAPC4520X168">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793154/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1812" package="CAPC4532X218">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793159/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1825" package="CAPC4564X218">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793166/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2220" package="CAPC5650X218">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793171/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2225" package="CAPC5563X218">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793174/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3640" package="CAPC9198X218">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793178/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TI_TPS2491DGSR" urn="urn:adsk.eagle:component:8414516/1" prefix="U" uservalue="yes" library_version="47">
<gates>
<gate name="G$1" symbol="TI_TPS2491DGSR" x="0" y="0"/>
</gates>
<devices>
<device name="10-TFSOP" package="SOP50P490X110-10">
<connects>
<connect gate="G$1" pin="10_VCC" pad="10"/>
<connect gate="G$1" pin="1_EN" pad="1"/>
<connect gate="G$1" pin="2_VREF" pad="2"/>
<connect gate="G$1" pin="3_PROG" pad="3"/>
<connect gate="G$1" pin="4_TIMER" pad="4"/>
<connect gate="G$1" pin="5_GND" pad="5"/>
<connect gate="G$1" pin="6_PG" pad="6"/>
<connect gate="G$1" pin="7_OUT" pad="7"/>
<connect gate="G$1" pin="8_GATE" pad="8"/>
<connect gate="G$1" pin="9_SENSE" pad="9"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3921298/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MOSFET_NCH" urn="urn:adsk.eagle:component:8414460/3" prefix="Q" uservalue="yes" library_version="47">
<gates>
<gate name="G$1" symbol="MOSFET-NCH" x="0" y="0"/>
</gates>
<devices>
<device name="SOIC-8" package="SOIC127P600X175-8">
<connects>
<connect gate="G$1" pin="D" pad="5 6 7 8"/>
<connect gate="G$1" pin="G" pad="4"/>
<connect gate="G$1" pin="S" pad="1 2 3"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3833977/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOT-223" package="SOT230P700X180-4">
<connects>
<connect gate="G$1" pin="D" pad="2 4"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="3"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3834044/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOT-23" package="SOT95P280X135-3">
<connects>
<connect gate="G$1" pin="D" pad="K"/>
<connect gate="G$1" pin="G" pad="NC"/>
<connect gate="G$1" pin="S" pad="A"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3809961/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TO-252-3_DPAK" package="TO457P1000X238-3">
<connects>
<connect gate="G$1" pin="D" pad="3"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3920001/3"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POWERPAK_SO-8" package="POWERPAK_SO-8">
<connects>
<connect gate="G$1" pin="D" pad="5 6 7 8 9"/>
<connect gate="G$1" pin="G" pad="4"/>
<connect gate="G$1" pin="S" pad="1 2 3"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414082/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DIODE_SCHOTTKY" urn="urn:adsk.eagle:component:8414469/8" prefix="D" uservalue="yes" library_version="47">
<gates>
<gate name="G$1" symbol="DIODE_SCHOTTKY" x="0" y="0"/>
</gates>
<devices>
<device name="SMA" package="DIOM5226X292">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793189/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMB" package="DIOM5436X265">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793201/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMC" package="DIOM7959X265">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793207/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOD-323" package="SOD2512X100">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793216/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOD-123" package="SOD3716X135">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793231/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOT-323" package="SOT65P210X110-3">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3809951/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOT-23" package="SOT95P280X135-3">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3809961/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOD-128" package="SODFL4725X110">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3921253/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0201" package="DIOC0502X50">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3948086/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402" package="DIOC1005X84">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3948100/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0502" package="DIOC1206X63">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3948141/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="DIOC1608X84">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3948147/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="DIOC2012X70">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3948150/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="DIOC3216X84">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3948168/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2010" package="DIOC5324X84">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3948171/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="DO214AC_SMA" package="DO214AC_SMA">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="K" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15671034/4"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DIODE" urn="urn:adsk.eagle:component:8414468/3" prefix="D" uservalue="yes" library_version="47">
<gates>
<gate name="G$1" symbol="DIODE" x="0" y="0"/>
</gates>
<devices>
<device name="SMA" package="DIOM5226X292">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793189/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMB" package="DIOM5436X265">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793201/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMC" package="DIOM7959X265">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793207/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOD-323" package="SOD2512X100">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793216/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOD-123" package="SOD3716X135">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793231/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOT-323" package="SOT65P210X110-3">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3809951/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOT-23" package="SOT95P280X135-3">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3809961/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOD-128" package="SODFL4725X110">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3921253/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0201" package="DIOC0502X50">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3948086/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402" package="DIOC1005X84">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3948100/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0502" package="DIOC1206X63">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3948141/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="DIOC1608X84">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3948147/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="DIOC2012X70">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3948150/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="DIOC3216X84">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3948168/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2010" package="DIOC5324X84">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3948171/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="DO_221AC_SMAF" package="DIOC5226X110N">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="K" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:16113232/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TP_CLASS_B" urn="urn:adsk.eagle:component:8414518/4" prefix="TP" uservalue="yes" library_version="44">
<gates>
<gate name="G$1" symbol="TP" x="0" y="0"/>
</gates>
<devices>
<device name="TP_1.5MM" package="TP_1.5MM">
<connects>
<connect gate="G$1" pin="PP" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414069/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TP_1.5MM_PAD" package="TP_1.5MM_PAD">
<connects>
<connect gate="G$1" pin="PP" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414070/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TP_1.7MM" package="TP_1.7MM">
<connects>
<connect gate="G$1" pin="PP" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414071/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C120H40" package="C120H40">
<connects>
<connect gate="G$1" pin="PP" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414063/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C131H51" package="C131H51">
<connects>
<connect gate="G$1" pin="PP" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414064/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C406H326" package="C406H326">
<connects>
<connect gate="G$1" pin="PP" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414065/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C491H411" package="C491H411">
<connects>
<connect gate="G$1" pin="PP" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414066/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CIR_PAD_1MM" package="CIR_PAD_1MM">
<connects>
<connect gate="G$1" pin="PP" pad="P$1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414067/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CIR_PAD_2MM" package="CIR_PAD_2MM">
<connects>
<connect gate="G$1" pin="PP" pad="P$1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414068/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="U$5" library="frames" library_urn="urn:adsk.eagle:library:229" deviceset="A3_LOC_JMA_RELEASE" device=""/>
<part name="SUPPLY102" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="R147" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="10K"/>
<part name="R148" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="56K"/>
<part name="R144" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="10K"/>
<part name="R143" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="8.87K"/>
<part name="C79" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0603" package3d_urn="urn:adsk.eagle:package:3793126/2" value="6.8nF"/>
<part name="SUPPLY103" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="R140" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="470K"/>
<part name="R145" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="10"/>
<part name="C76" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0805" package3d_urn="urn:adsk.eagle:package:3793139/1" value="3.3uF"/>
<part name="C81" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0603" package3d_urn="urn:adsk.eagle:package:3793126/2" value="22nF"/>
<part name="R141" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="1K"/>
<part name="U15" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TI_TPS2491DGSR" device="10-TFSOP" package3d_urn="urn:adsk.eagle:package:3921298/1" value="TPS2491DGSR"/>
<part name="R151" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:3811169/1" value="0.20"/>
<part name="Q1" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="MOSFET_NCH" device="TO-252-3_DPAK" package3d_urn="urn:adsk.eagle:package:3920001/3" value="BUK72150-55A,118"/>
<part name="R149" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:3811169/1" value="DNP"/>
<part name="R150" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:3811169/1" value="0"/>
<part name="SUPPLY63" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="C77" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0603" package3d_urn="urn:adsk.eagle:package:3793126/2" value="10nF"/>
<part name="R198" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="0"/>
<part name="D13" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="DIODE_SCHOTTKY" device="DO214AC_SMA" package3d_urn="urn:adsk.eagle:package:15671034/4" value="CDBA140-G "/>
<part name="D11" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="DIODE" device="DO_221AC_SMAF" package3d_urn="urn:adsk.eagle:package:16113232/1" value="SBRT3U45SAF-13"/>
<part name="TP1" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TP_CLASS_B" device="CIR_PAD_1MM" package3d_urn="urn:adsk.eagle:package:8414067/2"/>
</parts>
<sheets>
<sheet>
<plain>
<text x="139.7" y="231.14" size="1.27" layer="97">SBRT3U45SAF-13/SBRT3U45SA-13</text>
</plain>
<instances>
<instance part="U$5" gate="G$1" x="0" y="0" smashed="yes">
<attribute name="DRAWING_NAME" x="317.5" y="10.16" size="1.778" layer="94" font="vector"/>
<attribute name="SHEET" x="332.486" y="5.842" size="1.27" layer="94" font="vector"/>
</instance>
<instance part="SUPPLY102" gate="GND" x="165.1" y="165.1" smashed="yes">
<attribute name="VALUE" x="163.195" y="161.925" size="1.778" layer="96"/>
</instance>
<instance part="R147" gate="G$1" x="165.1" y="180.34" smashed="yes" rot="R270">
<attribute name="NAME" x="164.6174" y="173.228" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="164.592" y="182.88" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R148" gate="G$1" x="165.1" y="208.28" smashed="yes" rot="R270">
<attribute name="NAME" x="164.6174" y="201.168" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="164.592" y="210.82" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R144" gate="G$1" x="185.42" y="182.88" smashed="yes" rot="R270">
<attribute name="NAME" x="184.9374" y="175.768" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="184.912" y="185.42" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R143" gate="G$1" x="193.04" y="170.18" smashed="yes">
<attribute name="NAME" x="200.152" y="169.6974" size="1.27" layer="95" rot="R180"/>
<attribute name="VALUE" x="190.5" y="169.672" size="1.27" layer="96" rot="R180"/>
</instance>
<instance part="C79" gate="G$1" x="200.66" y="177.8" smashed="yes" rot="R90">
<attribute name="NAME" x="200.152" y="171.958" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="200.152" y="179.07" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="SUPPLY103" gate="GND" x="205.74" y="160.02" smashed="yes">
<attribute name="VALUE" x="203.835" y="156.845" size="1.778" layer="96"/>
</instance>
<instance part="R140" gate="G$1" x="226.06" y="213.36" smashed="yes" rot="R270">
<attribute name="NAME" x="225.5774" y="206.248" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="225.552" y="215.9" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R145" gate="G$1" x="208.28" y="213.36" smashed="yes" rot="R180">
<attribute name="NAME" x="201.168" y="213.8426" size="1.27" layer="95"/>
<attribute name="VALUE" x="210.82" y="213.868" size="1.27" layer="96"/>
</instance>
<instance part="C76" gate="G$1" x="233.68" y="215.9" smashed="yes" rot="R90">
<attribute name="NAME" x="231.14" y="214.63" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="237.236" y="214.63" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C81" gate="G$1" x="215.9" y="175.26" smashed="yes" rot="R90">
<attribute name="NAME" x="213.614" y="173.482" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="219.456" y="173.482" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="R141" gate="G$1" x="215.9" y="187.96" smashed="yes" rot="R270">
<attribute name="NAME" x="215.4174" y="180.848" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="215.392" y="190.5" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="U15" gate="G$1" x="190.5" y="198.12" smashed="yes"/>
<instance part="R151" gate="G$1" x="185.42" y="223.52" smashed="yes">
<attribute name="NAME" x="178.054" y="224.0026" size="1.27" layer="95"/>
<attribute name="VALUE" x="187.96" y="224.028" size="1.27" layer="96"/>
</instance>
<instance part="Q1" gate="G$1" x="213.36" y="220.98" smashed="yes" rot="R90">
<attribute name="NAME" x="208.28" y="226.06" size="1.27" layer="95" ratio="15"/>
<attribute name="VALUE" x="215.9" y="226.06" size="1.27" layer="96" ratio="15"/>
</instance>
<instance part="R149" gate="G$1" x="185.42" y="208.28" smashed="yes">
<attribute name="NAME" x="178.562" y="208.7626" size="1.27" layer="95"/>
<attribute name="VALUE" x="187.96" y="208.788" size="1.27" layer="96"/>
</instance>
<instance part="R150" gate="G$1" x="195.58" y="215.9" smashed="yes" rot="R90">
<attribute name="NAME" x="195.0974" y="209.042" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="195.072" y="218.44" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="SUPPLY63" gate="GND" x="233.68" y="203.2" smashed="yes">
<attribute name="VALUE" x="231.775" y="200.025" size="1.778" layer="96"/>
</instance>
<instance part="C77" gate="G$1" x="241.3" y="215.9" smashed="yes" rot="R90">
<attribute name="NAME" x="240.792" y="210.058" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="240.792" y="217.17" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="R198" gate="G$1" x="236.22" y="195.58" smashed="yes">
<attribute name="NAME" x="243.332" y="195.0974" size="1.27" layer="95" rot="R180"/>
<attribute name="VALUE" x="233.68" y="195.072" size="1.27" layer="96" rot="R180"/>
</instance>
<instance part="D13" gate="G$1" x="251.46" y="215.9" smashed="yes" rot="R90">
<attribute name="NAME" x="249.555" y="213.36" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="255.27" y="213.36" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="D11" gate="G$1" x="142.24" y="223.52" smashed="yes">
<attribute name="NAME" x="139.7" y="225.425" size="1.27" layer="95"/>
<attribute name="VALUE" x="139.7" y="228.6" size="1.27" layer="96"/>
</instance>
<instance part="TP1" gate="G$1" x="243.84" y="198.12" smashed="yes">
<attribute name="NAME" x="242.57" y="201.93" size="1.778" layer="95"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="VIN" class="0">
<segment>
<wire x1="138.43" y1="223.52" x2="119.38" y2="223.52" width="0.1524" layer="91"/>
<label x="119.38" y="223.52" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="D11" gate="G$1" pin="A"/>
</segment>
</net>
<net name="N$121" class="0">
<segment>
<pinref part="R151" gate="G$1" pin="1"/>
<wire x1="177.8" y1="208.28" x2="177.8" y2="223.52" width="0.1524" layer="91"/>
<wire x1="180.34" y1="223.52" x2="177.8" y2="223.52" width="0.1524" layer="91"/>
<pinref part="R149" gate="G$1" pin="1"/>
<wire x1="180.34" y1="208.28" x2="177.8" y2="208.28" width="0.1524" layer="91"/>
<wire x1="146.05" y1="223.52" x2="165.1" y2="223.52" width="0.1524" layer="91"/>
<junction x="177.8" y="223.52"/>
<pinref part="R148" gate="G$1" pin="1"/>
<wire x1="165.1" y1="223.52" x2="177.8" y2="223.52" width="0.1524" layer="91"/>
<wire x1="165.1" y1="213.36" x2="165.1" y2="223.52" width="0.1524" layer="91"/>
<junction x="165.1" y="223.52"/>
<pinref part="U15" gate="G$1" pin="10_VCC"/>
<wire x1="187.96" y1="195.58" x2="177.8" y2="195.58" width="0.1524" layer="91"/>
<wire x1="177.8" y1="195.58" x2="177.8" y2="208.28" width="0.1524" layer="91"/>
<junction x="177.8" y="208.28"/>
<pinref part="D11" gate="G$1" pin="K"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="R147" gate="G$1" pin="2"/>
<pinref part="SUPPLY102" gate="GND" pin="GND"/>
<wire x1="165.1" y1="175.26" x2="165.1" y2="167.64" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U15" gate="G$1" pin="5_GND"/>
<pinref part="SUPPLY103" gate="GND" pin="GND"/>
<wire x1="205.74" y1="185.42" x2="205.74" y2="170.18" width="0.1524" layer="91"/>
<pinref part="C79" gate="G$1" pin="1"/>
<wire x1="205.74" y1="170.18" x2="205.74" y2="167.64" width="0.1524" layer="91"/>
<wire x1="205.74" y1="167.64" x2="205.74" y2="162.56" width="0.1524" layer="91"/>
<wire x1="200.66" y1="172.72" x2="200.66" y2="170.18" width="0.1524" layer="91"/>
<wire x1="200.66" y1="170.18" x2="205.74" y2="170.18" width="0.1524" layer="91"/>
<junction x="205.74" y="170.18"/>
<pinref part="R143" gate="G$1" pin="2"/>
<wire x1="198.12" y1="170.18" x2="200.66" y2="170.18" width="0.1524" layer="91"/>
<junction x="200.66" y="170.18"/>
<pinref part="C81" gate="G$1" pin="1"/>
<wire x1="215.9" y1="170.18" x2="215.9" y2="167.64" width="0.1524" layer="91"/>
<wire x1="215.9" y1="167.64" x2="205.74" y2="167.64" width="0.1524" layer="91"/>
<junction x="205.74" y="167.64"/>
</segment>
<segment>
<pinref part="C76" gate="G$1" pin="1"/>
<pinref part="SUPPLY63" gate="GND" pin="GND"/>
<wire x1="233.68" y1="210.82" x2="233.68" y2="208.28" width="0.1524" layer="91"/>
<pinref part="C77" gate="G$1" pin="1"/>
<wire x1="233.68" y1="208.28" x2="233.68" y2="205.74" width="0.1524" layer="91"/>
<wire x1="241.3" y1="210.82" x2="241.3" y2="208.28" width="0.1524" layer="91"/>
<wire x1="241.3" y1="208.28" x2="233.68" y2="208.28" width="0.1524" layer="91"/>
<junction x="233.68" y="208.28"/>
<wire x1="251.46" y1="212.09" x2="251.46" y2="208.28" width="0.1524" layer="91"/>
<wire x1="251.46" y1="208.28" x2="241.3" y2="208.28" width="0.1524" layer="91"/>
<junction x="241.3" y="208.28"/>
<pinref part="D13" gate="G$1" pin="A"/>
</segment>
</net>
<net name="AISG_VIN" class="0">
<segment>
<wire x1="233.68" y1="223.52" x2="241.3" y2="223.52" width="0.1524" layer="91"/>
<label x="259.08" y="223.52" size="1.27" layer="95" xref="yes"/>
<pinref part="U15" gate="G$1" pin="7_OUT"/>
<wire x1="241.3" y1="223.52" x2="251.46" y2="223.52" width="0.1524" layer="91"/>
<wire x1="251.46" y1="223.52" x2="259.08" y2="223.52" width="0.1524" layer="91"/>
<wire x1="205.74" y1="200.66" x2="205.74" y2="208.28" width="0.1524" layer="91"/>
<wire x1="205.74" y1="208.28" x2="220.98" y2="208.28" width="0.1524" layer="91"/>
<wire x1="220.98" y1="208.28" x2="220.98" y2="223.52" width="0.1524" layer="91"/>
<pinref part="Q1" gate="G$1" pin="S"/>
<pinref part="R140" gate="G$1" pin="1"/>
<wire x1="226.06" y1="218.44" x2="226.06" y2="223.52" width="0.1524" layer="91"/>
<wire x1="226.06" y1="223.52" x2="220.98" y2="223.52" width="0.1524" layer="91"/>
<junction x="220.98" y="223.52"/>
<pinref part="C76" gate="G$1" pin="2"/>
<wire x1="233.68" y1="220.98" x2="233.68" y2="223.52" width="0.1524" layer="91"/>
<wire x1="233.68" y1="223.52" x2="226.06" y2="223.52" width="0.1524" layer="91"/>
<junction x="226.06" y="223.52"/>
<junction x="233.68" y="223.52"/>
<pinref part="C77" gate="G$1" pin="2"/>
<wire x1="241.3" y1="220.98" x2="241.3" y2="223.52" width="0.1524" layer="91"/>
<junction x="241.3" y="223.52"/>
<wire x1="251.46" y1="219.71" x2="251.46" y2="223.52" width="0.1524" layer="91"/>
<junction x="251.46" y="223.52"/>
<pinref part="D13" gate="G$1" pin="K"/>
</segment>
</net>
<net name="N$122" class="0">
<segment>
<pinref part="R144" gate="G$1" pin="2"/>
<wire x1="185.42" y1="177.8" x2="185.42" y2="175.26" width="0.1524" layer="91"/>
<wire x1="185.42" y1="175.26" x2="195.58" y2="175.26" width="0.1524" layer="91"/>
<pinref part="U15" gate="G$1" pin="3_PROG"/>
<wire x1="195.58" y1="175.26" x2="195.58" y2="185.42" width="0.1524" layer="91"/>
<pinref part="R143" gate="G$1" pin="1"/>
<wire x1="187.96" y1="170.18" x2="185.42" y2="170.18" width="0.1524" layer="91"/>
<wire x1="185.42" y1="170.18" x2="185.42" y2="175.26" width="0.1524" layer="91"/>
<junction x="185.42" y="175.26"/>
</segment>
</net>
<net name="N$141" class="0">
<segment>
<pinref part="R148" gate="G$1" pin="2"/>
<pinref part="R147" gate="G$1" pin="1"/>
<wire x1="165.1" y1="203.2" x2="165.1" y2="193.04" width="0.1524" layer="91"/>
<pinref part="U15" gate="G$1" pin="1_EN"/>
<wire x1="165.1" y1="193.04" x2="165.1" y2="185.42" width="0.1524" layer="91"/>
<wire x1="187.96" y1="193.04" x2="165.1" y2="193.04" width="0.1524" layer="91"/>
<junction x="165.1" y="193.04"/>
</segment>
</net>
<net name="N$192" class="0">
<segment>
<pinref part="U15" gate="G$1" pin="2_VREF"/>
<wire x1="187.96" y1="190.5" x2="185.42" y2="190.5" width="0.1524" layer="91"/>
<pinref part="R144" gate="G$1" pin="1"/>
<wire x1="185.42" y1="190.5" x2="185.42" y2="187.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$193" class="0">
<segment>
<pinref part="C79" gate="G$1" pin="2"/>
<pinref part="U15" gate="G$1" pin="4_TIMER"/>
<wire x1="200.66" y1="182.88" x2="200.66" y2="185.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$195" class="0">
<segment>
<pinref part="U15" gate="G$1" pin="6_PG"/>
<pinref part="R140" gate="G$1" pin="2"/>
<wire x1="213.36" y1="195.58" x2="226.06" y2="195.58" width="0.1524" layer="91"/>
<wire x1="226.06" y1="195.58" x2="226.06" y2="208.28" width="0.1524" layer="91"/>
<wire x1="226.06" y1="195.58" x2="231.14" y2="195.58" width="0.1524" layer="91"/>
<junction x="226.06" y="195.58"/>
<pinref part="R198" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$197" class="0">
<segment>
<pinref part="R141" gate="G$1" pin="2"/>
<pinref part="C81" gate="G$1" pin="2"/>
<wire x1="215.9" y1="182.88" x2="215.9" y2="180.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$199" class="0">
<segment>
<pinref part="U15" gate="G$1" pin="8_GATE"/>
<pinref part="R145" gate="G$1" pin="2"/>
<wire x1="200.66" y1="200.66" x2="200.66" y2="203.2" width="0.1524" layer="91"/>
<pinref part="R141" gate="G$1" pin="1"/>
<wire x1="200.66" y1="203.2" x2="200.66" y2="213.36" width="0.1524" layer="91"/>
<wire x1="200.66" y1="213.36" x2="203.2" y2="213.36" width="0.1524" layer="91"/>
<wire x1="200.66" y1="203.2" x2="215.9" y2="203.2" width="0.1524" layer="91"/>
<wire x1="215.9" y1="203.2" x2="215.9" y2="193.04" width="0.1524" layer="91"/>
<junction x="200.66" y="203.2"/>
</segment>
</net>
<net name="N$194" class="0">
<segment>
<pinref part="R151" gate="G$1" pin="2"/>
<wire x1="190.5" y1="223.52" x2="195.58" y2="223.52" width="0.1524" layer="91"/>
<pinref part="Q1" gate="G$1" pin="D"/>
<wire x1="195.58" y1="220.98" x2="195.58" y2="223.52" width="0.1524" layer="91"/>
<wire x1="195.58" y1="223.52" x2="205.74" y2="223.52" width="0.1524" layer="91"/>
<junction x="195.58" y="223.52"/>
<pinref part="R150" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$196" class="0">
<segment>
<pinref part="R145" gate="G$1" pin="1"/>
<pinref part="Q1" gate="G$1" pin="G"/>
<wire x1="213.36" y1="213.36" x2="215.9" y2="213.36" width="0.1524" layer="91"/>
<wire x1="215.9" y1="213.36" x2="215.9" y2="215.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$203" class="0">
<segment>
<pinref part="R150" gate="G$1" pin="1"/>
<pinref part="U15" gate="G$1" pin="9_SENSE"/>
<wire x1="195.58" y1="200.66" x2="195.58" y2="208.28" width="0.1524" layer="91"/>
<pinref part="R149" gate="G$1" pin="2"/>
<wire x1="195.58" y1="208.28" x2="195.58" y2="210.82" width="0.1524" layer="91"/>
<wire x1="190.5" y1="208.28" x2="195.58" y2="208.28" width="0.1524" layer="91"/>
<junction x="195.58" y="208.28"/>
</segment>
</net>
<net name="PG_OUT" class="0">
<segment>
<pinref part="R198" gate="G$1" pin="2"/>
<wire x1="241.3" y1="195.58" x2="243.84" y2="195.58" width="0.1524" layer="91"/>
<label x="246.38" y="195.58" size="1.778" layer="95" xref="yes"/>
<pinref part="TP1" gate="G$1" pin="PP"/>
<wire x1="243.84" y1="195.58" x2="246.38" y2="195.58" width="0.1524" layer="91"/>
<wire x1="243.84" y1="198.12" x2="243.84" y2="195.58" width="0.1524" layer="91"/>
<junction x="243.84" y="195.58"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
