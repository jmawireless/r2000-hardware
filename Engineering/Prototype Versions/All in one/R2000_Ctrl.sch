<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.5.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="yes" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="5" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="no" active="no"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="no" active="no"/>
<layer number="103" name="tMap" color="7" fill="1" visible="no" active="no"/>
<layer number="104" name="Name" color="7" fill="1" visible="no" active="no"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="no" active="no"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="no" active="no"/>
<layer number="107" name="Crop" color="7" fill="1" visible="no" active="no"/>
<layer number="108" name="fp8" color="7" fill="1" visible="yes" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="yes" active="yes"/>
<layer number="110" name="110" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="111" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="7" fill="1" visible="yes" active="yes"/>
<layer number="114" name="FRNTMAAT1" color="7" fill="1" visible="no" active="no"/>
<layer number="115" name="FRNTMAAT2" color="7" fill="1" visible="no" active="no"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="no" active="no"/>
<layer number="117" name="BACKMAAT1" color="7" fill="1" visible="no" active="no"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="yes" active="yes"/>
<layer number="119" name="KAP_TEKEN" color="7" fill="1" visible="no" active="no"/>
<layer number="120" name="KAP_MAAT1" color="7" fill="1" visible="no" active="no"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="no" active="no"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="no" active="no"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="top_silk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="130" name="SMDSTROOK" color="7" fill="1" visible="no" active="no"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="133" name="bottom_silk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="DrillLegend" color="7" fill="1" visible="no" active="no"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="14" fill="1" visible="no" active="no"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="no"/>
<layer number="201" name="201bmp" color="2" fill="1" visible="no" active="no"/>
<layer number="202" name="202bmp" color="3" fill="1" visible="no" active="no"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="no" active="no"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="no" active="no"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="no" active="no"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="no" active="no"/>
<layer number="207" name="207bmp" color="15" fill="10" visible="no" active="no"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="no" active="no"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="Eagle3D_PG1" color="7" fill="1" visible="no" active="no"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="no"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="OrgLBR" color="13" fill="1" visible="no" active="no"/>
<layer number="255" name="Accent" color="7" fill="1" visible="no" active="no"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="supply2" urn="urn:adsk.eagle:library:372">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
Please keep in mind, that these devices are necessary for the
automatic wiring of the supply signals.&lt;p&gt;
The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND" urn="urn:adsk.eagle:symbol:26990/1" library_version="2">
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="-1.27" y2="0" width="0.254" layer="94"/>
<text x="-1.905" y="-3.175" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" urn="urn:adsk.eagle:component:27037/1" prefix="SUPPLY" library_version="2">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="GND" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1" urn="urn:adsk.eagle:library:371">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="+3V3" urn="urn:adsk.eagle:symbol:26950/1" library_version="1">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+3V3" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="+3V3" urn="urn:adsk.eagle:component:26981/1" prefix="+3V3" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="+3V3" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="frames" urn="urn:adsk.eagle:library:229">
<description>&lt;b&gt;Frames for Sheet and Layout&lt;/b&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="A3_LOC_JMA_RELEASE">
<wire x1="0" y1="241.3" x2="63.5" y2="241.3" width="0.025" layer="94"/>
<wire x1="63.5" y1="241.3" x2="127" y2="241.3" width="0.025" layer="94"/>
<wire x1="127" y1="241.3" x2="190.5" y2="241.3" width="0.025" layer="94"/>
<wire x1="190.5" y1="241.3" x2="254" y2="241.3" width="0.025" layer="94"/>
<wire x1="254" y1="241.3" x2="317.5" y2="241.3" width="0.025" layer="94"/>
<wire x1="317.5" y1="241.3" x2="380.97883125" y2="241.3" width="0.025" layer="94"/>
<wire x1="380.97883125" y1="241.3" x2="380.97883125" y2="0.02116875" width="0.025" layer="94"/>
<wire x1="380.97883125" y1="0.02116875" x2="0" y2="0.02116875" width="0.025" layer="94"/>
<wire x1="0" y1="0.02116875" x2="0" y2="60.325" width="0.025" layer="94"/>
<wire x1="0" y1="60.325" x2="0" y2="120.65" width="0.025" layer="94"/>
<wire x1="0" y1="120.65" x2="0" y2="180.975" width="0.025" layer="94"/>
<wire x1="0" y1="180.975" x2="0" y2="241.3" width="0.025" layer="94"/>
<wire x1="5.079890625" y1="236.22041875" x2="375.898909375" y2="236.22041875" width="0.025" layer="94"/>
<wire x1="375.898909375" y1="236.22041875" x2="375.898909375" y2="5.10075" width="0.025" layer="94"/>
<wire x1="375.898909375" y1="5.10075" x2="5.079890625" y2="5.10075" width="0.025" layer="94"/>
<wire x1="5.079890625" y1="5.10075" x2="5.079890625" y2="236.22041875" width="0.025" layer="94"/>
<wire x1="317.5" y1="5.08" x2="317.5" y2="0" width="0.025" layer="94"/>
<wire x1="317.5" y1="236.22" x2="317.5" y2="241.3" width="0.025" layer="94"/>
<wire x1="254" y1="5.08" x2="254" y2="0" width="0.025" layer="94"/>
<wire x1="254" y1="236.22" x2="254" y2="241.3" width="0.025" layer="94"/>
<wire x1="190.5" y1="5.08" x2="190.5" y2="0" width="0.025" layer="94"/>
<wire x1="190.5" y1="236.22" x2="190.5" y2="241.3" width="0.025" layer="94"/>
<wire x1="127" y1="5.08" x2="127" y2="0" width="0.025" layer="94"/>
<wire x1="127" y1="236.22" x2="127" y2="241.3" width="0.025" layer="94"/>
<wire x1="63.5" y1="5.08" x2="63.5" y2="0" width="0.025" layer="94"/>
<wire x1="63.5" y1="236.22" x2="63.5" y2="241.3" width="0.025" layer="94"/>
<wire x1="5.08" y1="180.975" x2="0" y2="180.975" width="0.025" layer="94"/>
<wire x1="375.92" y1="180.975" x2="381" y2="180.975" width="0.025" layer="94"/>
<wire x1="5.08" y1="120.65" x2="0" y2="120.65" width="0.025" layer="94"/>
<wire x1="375.92" y1="120.65" x2="381" y2="120.65" width="0.025" layer="94"/>
<wire x1="5.08" y1="60.325" x2="0" y2="60.325" width="0.025" layer="94"/>
<wire x1="375.92" y1="60.325" x2="381" y2="60.325" width="0.025" layer="94"/>
<wire x1="271.78" y1="5.08" x2="271.78" y2="21.59" width="0.025" layer="94"/>
<wire x1="271.78" y1="21.59" x2="271.78" y2="27.94" width="0.025" layer="94"/>
<wire x1="271.78" y1="27.94" x2="271.78" y2="34.29" width="0.025" layer="94"/>
<wire x1="271.78" y1="34.29" x2="271.78" y2="40.64" width="0.025" layer="94"/>
<wire x1="271.78" y1="40.64" x2="271.78" y2="45.72" width="0.025" layer="94"/>
<wire x1="271.78" y1="45.72" x2="285.75" y2="45.72" width="0.025" layer="94"/>
<wire x1="285.75" y1="45.72" x2="293.37" y2="45.72" width="0.025" layer="94"/>
<wire x1="293.37" y1="45.72" x2="304.8" y2="45.72" width="0.025" layer="94"/>
<wire x1="304.8" y1="45.72" x2="375.92" y2="45.72" width="0.025" layer="94"/>
<wire x1="304.8" y1="5.08" x2="304.8" y2="8.89" width="0.025" layer="94"/>
<wire x1="304.8" y1="8.89" x2="304.8" y2="15.24" width="0.025" layer="94"/>
<wire x1="304.8" y1="15.24" x2="304.8" y2="21.59" width="0.025" layer="94"/>
<wire x1="304.8" y1="21.59" x2="304.8" y2="27.94" width="0.025" layer="94"/>
<wire x1="304.8" y1="27.94" x2="304.8" y2="34.29" width="0.025" layer="94"/>
<wire x1="304.8" y1="34.29" x2="304.8" y2="40.64" width="0.025" layer="94"/>
<wire x1="304.8" y1="40.64" x2="304.8" y2="45.72" width="0.025" layer="94"/>
<wire x1="271.78" y1="40.64" x2="304.8" y2="40.64" width="0.025" layer="94"/>
<wire x1="271.78" y1="34.29" x2="304.8" y2="34.29" width="0.025" layer="94"/>
<wire x1="271.78" y1="27.94" x2="304.8" y2="27.94" width="0.025" layer="94"/>
<wire x1="271.78" y1="21.59" x2="285.75" y2="21.59" width="0.025" layer="94"/>
<wire x1="285.75" y1="21.59" x2="293.37" y2="21.59" width="0.025" layer="94"/>
<wire x1="293.37" y1="21.59" x2="304.8" y2="21.59" width="0.025" layer="94"/>
<wire x1="304.8" y1="8.89" x2="322.58" y2="8.89" width="0.025" layer="94"/>
<wire x1="322.58" y1="8.89" x2="359.41" y2="8.89" width="0.025" layer="94"/>
<wire x1="359.41" y1="8.89" x2="369.57" y2="8.89" width="0.025" layer="94"/>
<wire x1="369.57" y1="8.89" x2="375.92" y2="8.89" width="0.025" layer="94"/>
<wire x1="322.58" y1="8.89" x2="322.58" y2="5.08" width="0.025" layer="94"/>
<wire x1="359.41" y1="8.89" x2="359.41" y2="5.08" width="0.025" layer="94"/>
<wire x1="304.8" y1="15.24" x2="369.57" y2="15.24" width="0.025" layer="94"/>
<wire x1="369.57" y1="15.24" x2="375.92" y2="15.24" width="0.025" layer="94"/>
<wire x1="369.57" y1="15.24" x2="369.57" y2="8.89" width="0.025" layer="94"/>
<wire x1="304.8" y1="21.59" x2="375.92" y2="21.59" width="0.025" layer="94"/>
<wire x1="285.75" y1="45.72" x2="285.75" y2="21.59" width="0.025" layer="94"/>
<wire x1="293.37" y1="45.72" x2="293.37" y2="21.59" width="0.025" layer="94"/>
<wire x1="307.34" y1="31.75" x2="373.38" y2="31.75" width="0.025" layer="94"/>
<wire x1="329.29703125" y1="44.90211875" x2="329.35418125" y2="44.890690625" width="0.025" layer="94"/>
<wire x1="329.35418125" y1="44.890690625" x2="329.3999" y2="44.879259375" width="0.025" layer="94"/>
<wire x1="329.81138125" y1="44.776390625" x2="329.84566875" y2="44.764959375" width="0.025" layer="94"/>
<wire x1="329.84566875" y1="44.764959375" x2="329.891390625" y2="44.75353125" width="0.025" layer="94"/>
<wire x1="331.137259375" y1="44.1706" x2="331.365859375" y2="43.99915" width="0.025" layer="94"/>
<wire x1="331.365859375" y1="43.99915" x2="331.451590625" y2="43.91341875" width="0.025" layer="94"/>
<wire x1="331.92593125" y1="43.164759375" x2="331.937359375" y2="43.09618125" width="0.025" layer="94"/>
<wire x1="331.937359375" y1="43.09618125" x2="331.948790625" y2="42.993309375" width="0.025" layer="94"/>
<wire x1="331.891640625" y1="42.5704" x2="331.90306875" y2="42.604690625" width="0.025" layer="94"/>
<wire x1="331.84591875" y1="42.4561" x2="331.891640625" y2="42.5704" width="0.025" layer="94"/>
<wire x1="329.262740625" y1="40.947340625" x2="329.319890625" y2="40.95876875" width="0.025" layer="94"/>
<wire x1="329.194159375" y1="40.935909375" x2="329.262740625" y2="40.947340625" width="0.025" layer="94"/>
<wire x1="329.194159375" y1="40.935909375" x2="331.44586875" y2="32.512" width="0.025" layer="94"/>
<wire x1="323.764909375" y1="32.512" x2="324.9625" y2="37.03828125" width="0.025" layer="94"/>
<wire x1="324.9625" y1="37.03828125" x2="325.993759375" y2="40.935909375" width="0.025" layer="94"/>
<wire x1="325.719440625" y1="40.993059375" x2="325.78801875" y2="40.98163125" width="0.025" layer="94"/>
<wire x1="325.63943125" y1="41.01591875" x2="325.719440625" y2="40.993059375" width="0.025" layer="94"/>
<wire x1="325.45655" y1="41.061640625" x2="325.536559375" y2="41.03878125" width="0.025" layer="94"/>
<wire x1="325.3994" y1="41.07306875" x2="325.45655" y2="41.061640625" width="0.025" layer="94"/>
<wire x1="325.136509375" y1="41.15308125" x2="325.21651875" y2="41.13021875" width="0.025" layer="94"/>
<wire x1="325.06793125" y1="41.175940625" x2="325.136509375" y2="41.15308125" width="0.025" layer="94"/>
<wire x1="324.976490625" y1="41.21023125" x2="325.06793125" y2="41.175940625" width="0.025" layer="94"/>
<wire x1="324.97076875" y1="41.21251875" x2="324.976490625" y2="41.21023125" width="0.025" layer="94"/>
<wire x1="324.64501875" y1="41.347390625" x2="324.747890625" y2="41.30166875" width="0.025" layer="94"/>
<wire x1="324.565009375" y1="41.38168125" x2="324.64501875" y2="41.347390625" width="0.025" layer="94"/>
<wire x1="324.450709375" y1="41.43883125" x2="324.45185" y2="41.438259375" width="0.025" layer="94"/>
<wire x1="324.393559375" y1="41.47311875" x2="324.450709375" y2="41.43883125" width="0.025" layer="94"/>
<wire x1="324.26783125" y1="41.5417" x2="324.347840625" y2="41.49598125" width="0.025" layer="94"/>
<wire x1="324.233540625" y1="41.564559375" x2="324.26783125" y2="41.5417" width="0.025" layer="94"/>
<wire x1="324.18781875" y1="41.58741875" x2="324.233540625" y2="41.564559375" width="0.025" layer="94"/>
<wire x1="324.1421" y1="41.621709375" x2="324.18781875" y2="41.58741875" width="0.025" layer="94"/>
<wire x1="324.08495" y1="41.656" x2="324.1421" y2="41.621709375" width="0.025" layer="94"/>
<wire x1="323.822059375" y1="41.861740625" x2="323.85635" y2="41.82745" width="0.025" layer="94"/>
<wire x1="323.776340625" y1="41.89603125" x2="323.822059375" y2="41.861740625" width="0.025" layer="94"/>
<wire x1="323.62775" y1="42.05605" x2="323.662040625" y2="42.01033125" width="0.025" layer="94"/>
<wire x1="323.593459375" y1="42.090340625" x2="323.62775" y2="42.05605" width="0.025" layer="94"/>
<wire x1="323.35343125" y1="42.46753125" x2="323.354" y2="42.466390625" width="0.025" layer="94"/>
<wire x1="323.33056875" y1="42.52468125" x2="323.35343125" y2="42.46753125" width="0.025" layer="94"/>
<wire x1="323.536309375" y1="43.690540625" x2="323.58203125" y2="43.736259375" width="0.025" layer="94"/>
<wire x1="323.58203125" y1="43.736259375" x2="323.62775" y2="43.793409375" width="0.025" layer="94"/>
<wire x1="323.78776875" y1="43.95343125" x2="323.84491875" y2="43.99915" width="0.025" layer="94"/>
<wire x1="323.84491875" y1="43.99915" x2="323.87806875" y2="44.0323" width="0.025" layer="94"/>
<wire x1="323.87806875" y1="44.0323" x2="323.879209375" y2="44.033440625" width="0.025" layer="94"/>
<wire x1="323.879209375" y1="44.033440625" x2="323.936359375" y2="44.079159375" width="0.025" layer="94"/>
<wire x1="323.936359375" y1="44.079159375" x2="324.0278" y2="44.147740625" width="0.025" layer="94"/>
<wire x1="324.164959375" y1="44.23918125" x2="324.222109375" y2="44.27346875" width="0.025" layer="94"/>
<wire x1="324.222109375" y1="44.27346875" x2="324.290690625" y2="44.319190625" width="0.025" layer="94"/>
<wire x1="324.290690625" y1="44.319190625" x2="324.336409375" y2="44.34205" width="0.025" layer="94"/>
<wire x1="324.336409375" y1="44.34205" x2="324.393559375" y2="44.376340625" width="0.025" layer="94"/>
<wire x1="324.62101875" y1="44.49006875" x2="324.622159375" y2="44.490640625" width="0.025" layer="94"/>
<wire x1="324.622159375" y1="44.490640625" x2="324.679309375" y2="44.5135" width="0.025" layer="94"/>
<wire x1="324.679309375" y1="44.5135" x2="324.72503125" y2="44.536359375" width="0.025" layer="94"/>
<wire x1="324.72503125" y1="44.536359375" x2="325.01078125" y2="44.650659375" width="0.025" layer="94"/>
<wire x1="325.01078125" y1="44.650659375" x2="325.35368125" y2="44.764959375" width="0.025" layer="94"/>
<wire x1="325.35368125" y1="44.764959375" x2="325.433690625" y2="44.78781875" width="0.025" layer="94"/>
<wire x1="325.433690625" y1="44.78781875" x2="325.479409375" y2="44.79925" width="0.025" layer="94"/>
<wire x1="325.479409375" y1="44.79925" x2="325.5137" y2="44.81068125" width="0.025" layer="94"/>
<wire x1="325.5137" y1="44.81068125" x2="325.7423" y2="44.86783125" width="0.025" layer="94"/>
<wire x1="325.7423" y1="44.86783125" x2="325.79945" y2="44.879259375" width="0.025" layer="94"/>
<wire x1="325.79945" y1="44.879259375" x2="325.84516875" y2="44.890690625" width="0.025" layer="94"/>
<wire x1="326.393809375" y1="44.69638125" x2="326.450959375" y2="44.707809375" width="0.025" layer="94"/>
<wire x1="326.32523125" y1="44.68495" x2="326.393809375" y2="44.69638125" width="0.025" layer="94"/>
<wire x1="325.936609375" y1="44.604940625" x2="325.993759375" y2="44.61636875" width="0.025" layer="94"/>
<wire x1="325.84516875" y1="44.58208125" x2="325.936609375" y2="44.604940625" width="0.025" layer="94"/>
<wire x1="325.765159375" y1="44.55921875" x2="325.84516875" y2="44.58208125" width="0.025" layer="94"/>
<wire x1="325.67371875" y1="44.536359375" x2="325.765159375" y2="44.55921875" width="0.025" layer="94"/>
<wire x1="325.5137" y1="44.490640625" x2="325.520559375" y2="44.4926" width="0.025" layer="94"/>
<wire x1="325.23938125" y1="44.3992" x2="325.5137" y2="44.490640625" width="0.025" layer="94"/>
<wire x1="324.98791875" y1="44.29633125" x2="325.06793125" y2="44.33061875" width="0.025" layer="94"/>
<wire x1="324.93076875" y1="44.27346875" x2="324.98791875" y2="44.29633125" width="0.025" layer="94"/>
<wire x1="324.61073125" y1="44.10201875" x2="324.679309375" y2="44.147740625" width="0.025" layer="94"/>
<wire x1="324.53071875" y1="44.0563" x2="324.531859375" y2="44.05695" width="0.025" layer="94"/>
<wire x1="324.531859375" y1="44.05695" x2="324.61073125" y2="44.10201875" width="0.025" layer="94"/>
<wire x1="324.462140625" y1="44.01058125" x2="324.53071875" y2="44.0563" width="0.025" layer="94"/>
<wire x1="324.233540625" y1="43.8277" x2="324.279259375" y2="43.87341875" width="0.025" layer="94"/>
<wire x1="324.164959375" y1="43.77055" x2="324.233540625" y2="43.8277" width="0.025" layer="94"/>
<wire x1="323.73061875" y1="42.51325" x2="323.74205" y2="42.478959375" width="0.025" layer="94"/>
<wire x1="323.74205" y1="42.478959375" x2="323.75348125" y2="42.433240625" width="0.025" layer="94"/>
<wire x1="323.75348125" y1="42.433240625" x2="323.776340625" y2="42.38751875" width="0.025" layer="94"/>
<wire x1="323.776340625" y1="42.38751875" x2="323.7992" y2="42.33036875" width="0.025" layer="94"/>
<wire x1="324.08495" y1="41.95318125" x2="324.1421" y2="41.907459375" width="0.025" layer="94"/>
<wire x1="324.1421" y1="41.907459375" x2="324.176390625" y2="41.87316875" width="0.025" layer="94"/>
<wire x1="324.41641875" y1="41.690290625" x2="324.462140625" y2="41.656" width="0.025" layer="94"/>
<wire x1="324.462140625" y1="41.656" x2="324.53071875" y2="41.61028125" width="0.025" layer="94"/>
<wire x1="324.64501875" y1="41.5417" x2="324.690740625" y2="41.518840625" width="0.025" layer="94"/>
<wire x1="324.690740625" y1="41.518840625" x2="324.72503125" y2="41.49598125" width="0.025" layer="94"/>
<wire x1="324.72503125" y1="41.49598125" x2="324.78218125" y2="41.461690625" width="0.025" layer="94"/>
<wire x1="324.919340625" y1="41.393109375" x2="324.976490625" y2="41.37025" width="0.025" layer="94"/>
<wire x1="324.976490625" y1="41.37025" x2="325.022209375" y2="41.347390625" width="0.025" layer="94"/>
<wire x1="325.46798125" y1="41.18736875" x2="325.536559375" y2="41.164509375" width="0.025" layer="94"/>
<wire x1="325.536559375" y1="41.164509375" x2="325.58228125" y2="41.15308125" width="0.025" layer="94"/>
<wire x1="325.58228125" y1="41.15308125" x2="325.61656875" y2="41.14165" width="0.025" layer="94"/>
<wire x1="325.61656875" y1="41.14165" x2="325.708009375" y2="41.118790625" width="0.025" layer="94"/>
<wire x1="325.708009375" y1="41.118790625" x2="325.7423" y2="41.107359375" width="0.025" layer="94"/>
<wire x1="325.7423" y1="41.107359375" x2="326.01661875" y2="41.03878125" width="0.025" layer="94"/>
<wire x1="325.45655" y1="41.38168125" x2="325.5137" y2="41.35881875" width="0.025" layer="94"/>
<wire x1="325.433690625" y1="41.393109375" x2="325.45655" y2="41.38168125" width="0.025" layer="94"/>
<wire x1="325.15936875" y1="41.518840625" x2="325.319390625" y2="41.43883125" width="0.025" layer="94"/>
<wire x1="325.10221875" y1="41.55313125" x2="325.15936875" y2="41.518840625" width="0.025" layer="94"/>
<wire x1="324.70216875" y1="43.4848" x2="324.747890625" y2="43.519090625" width="0.025" layer="94"/>
<wire x1="324.747890625" y1="43.519090625" x2="324.77075" y2="43.54195" width="0.025" layer="94"/>
<wire x1="324.98678125" y1="43.68978125" x2="324.98791875" y2="43.690540625" width="0.025" layer="94"/>
<wire x1="324.98791875" y1="43.690540625" x2="325.10221875" y2="43.75911875" width="0.025" layer="94"/>
<wire x1="325.10221875" y1="43.75911875" x2="325.177659375" y2="43.796840625" width="0.025" layer="94"/>
<wire x1="325.45655" y1="43.93056875" x2="325.68515" y2="44.022009375" width="0.025" layer="94"/>
<wire x1="325.82116875" y1="44.06735" x2="325.822309375" y2="44.06773125" width="0.025" layer="94"/>
<wire x1="325.822309375" y1="44.06773125" x2="326.062340625" y2="44.136309375" width="0.025" layer="94"/>
<wire x1="326.062340625" y1="44.136309375" x2="326.15378125" y2="44.15916875" width="0.025" layer="94"/>
<wire x1="326.15378125" y1="44.15916875" x2="326.35951875" y2="44.204890625" width="0.025" layer="94"/>
<wire x1="326.279509375" y1="43.919140625" x2="326.462390625" y2="43.964859375" width="0.025" layer="94"/>
<wire x1="326.119490625" y1="43.87341875" x2="326.279509375" y2="43.919140625" width="0.025" layer="94"/>
<wire x1="326.02805" y1="43.850559375" x2="326.119490625" y2="43.87341875" width="0.025" layer="94"/>
<wire x1="325.68515" y1="43.72483125" x2="325.8566" y2="43.793409375" width="0.025" layer="94"/>
<wire x1="325.605140625" y1="43.690540625" x2="325.68515" y2="43.72483125" width="0.025" layer="94"/>
<wire x1="325.604" y1="43.68996875" x2="325.605140625" y2="43.690540625" width="0.025" layer="94"/>
<wire x1="325.34225" y1="43.54195" x2="325.45655" y2="43.61053125" width="0.025" layer="94"/>
<wire x1="325.341109375" y1="43.541190625" x2="325.34225" y2="43.54195" width="0.025" layer="94"/>
<wire x1="324.862190625" y1="43.15333125" x2="325.033640625" y2="43.32478125" width="0.025" layer="94"/>
<wire x1="324.8279" y1="43.107609375" x2="324.862190625" y2="43.15333125" width="0.025" layer="94"/>
<wire x1="324.75931875" y1="43.004740625" x2="324.8279" y2="43.107609375" width="0.025" layer="94"/>
<wire x1="324.633590625" y1="42.62755" x2="324.65645" y2="42.74185" width="0.025" layer="94"/>
<wire x1="324.633590625" y1="42.626409375" x2="324.633590625" y2="42.62755" width="0.025" layer="94"/>
<wire x1="324.690740625" y1="42.261790625" x2="324.70216875" y2="42.23893125" width="0.025" layer="94"/>
<wire x1="324.70216875" y1="42.23893125" x2="324.7136" y2="42.204640625" width="0.025" layer="94"/>
<wire x1="324.7136" y1="42.204640625" x2="324.72503125" y2="42.18178125" width="0.025" layer="94"/>
<wire x1="324.72503125" y1="42.18178125" x2="324.747890625" y2="42.147490625" width="0.025" layer="94"/>
<wire x1="324.793609375" y1="42.06748125" x2="324.87361875" y2="41.964609375" width="0.025" layer="94"/>
<wire x1="324.87361875" y1="41.964609375" x2="324.874759375" y2="41.96346875" width="0.025" layer="94"/>
<wire x1="325.84516875" y1="41.35881875" x2="325.969759375" y2="41.317290625" width="0.025" layer="94"/>
<wire x1="326.084059375" y1="41.279190625" x2="326.0852" y2="41.278809375" width="0.025" layer="94"/>
<wire x1="326.0852" y1="41.278809375" x2="326.119490625" y2="41.43883125" width="0.025" layer="94"/>
<wire x1="326.0852" y1="41.461690625" x2="326.119490625" y2="41.43883125" width="0.025" layer="94"/>
<wire x1="325.993759375" y1="41.49598125" x2="326.0852" y2="41.461690625" width="0.025" layer="94"/>
<wire x1="325.99261875" y1="41.49646875" x2="325.993759375" y2="41.49598125" width="0.025" layer="94"/>
<wire x1="325.650859375" y1="41.66743125" x2="325.652" y2="41.66678125" width="0.025" layer="94"/>
<wire x1="325.55941875" y1="41.72458125" x2="325.650859375" y2="41.66743125" width="0.025" layer="94"/>
<wire x1="325.44511875" y1="42.95901875" x2="325.536559375" y2="43.0276" width="0.025" layer="94"/>
<wire x1="325.536559375" y1="43.0276" x2="325.5377" y2="43.028359375" width="0.025" layer="94"/>
<wire x1="325.605140625" y1="43.07331875" x2="325.776590625" y2="43.176190625" width="0.025" layer="94"/>
<wire x1="326.49668125" y1="43.43908125" x2="326.64526875" y2="43.47336875" width="0.025" layer="94"/>
<wire x1="326.75956875" y1="43.30191875" x2="326.81671875" y2="43.31335" width="0.025" layer="94"/>
<wire x1="326.71385" y1="43.290490625" x2="326.75956875" y2="43.30191875" width="0.025" layer="94"/>
<wire x1="326.6567" y1="43.279059375" x2="326.71385" y2="43.290490625" width="0.025" layer="94"/>
<wire x1="326.565259375" y1="43.2562" x2="326.6567" y2="43.279059375" width="0.025" layer="94"/>
<wire x1="326.35951875" y1="43.18761875" x2="326.41666875" y2="43.21048125" width="0.025" layer="94"/>
<wire x1="326.32523125" y1="43.176190625" x2="326.35951875" y2="43.18761875" width="0.025" layer="94"/>
<wire x1="326.21093125" y1="43.13046875" x2="326.32523125" y2="43.176190625" width="0.025" layer="94"/>
<wire x1="326.005190625" y1="43.0276" x2="326.21093125" y2="43.13046875" width="0.025" layer="94"/>
<wire x1="325.547609375" y1="42.09148125" x2="325.547990625" y2="42.090340625" width="0.025" layer="94"/>
<wire x1="325.547990625" y1="42.090340625" x2="325.57085" y2="42.04461875" width="0.025" layer="94"/>
<wire x1="325.84516875" y1="41.747440625" x2="325.95946875" y2="41.678859375" width="0.025" layer="94"/>
<wire x1="325.95946875" y1="41.678859375" x2="326.03948125" y2="41.633140625" width="0.025" layer="94"/>
<wire x1="326.03948125" y1="41.633140625" x2="326.108059375" y2="41.59885" width="0.025" layer="94"/>
<wire x1="326.108059375" y1="41.59885" x2="326.165209375" y2="41.575990625" width="0.025" layer="94"/>
<wire x1="326.165209375" y1="41.575990625" x2="326.35951875" y2="42.307509375" width="0.025" layer="94"/>
<wire x1="329.022709375" y1="41.575990625" x2="329.04556875" y2="41.575990625" width="0.025" layer="94"/>
<wire x1="329.04556875" y1="41.575990625" x2="329.251309375" y2="41.678859375" width="0.025" layer="94"/>
<wire x1="329.251309375" y1="41.678859375" x2="329.25245" y2="41.67961875" width="0.025" layer="94"/>
<wire x1="329.42161875" y1="41.7924" x2="329.422759375" y2="41.793159375" width="0.025" layer="94"/>
<wire x1="329.422759375" y1="41.793159375" x2="329.548490625" y2="41.918890625" width="0.025" layer="94"/>
<wire x1="329.548490625" y1="41.918890625" x2="329.58278125" y2="41.964609375" width="0.025" layer="94"/>
<wire x1="329.58278125" y1="41.964609375" x2="329.6285" y2="42.033190625" width="0.025" layer="94"/>
<wire x1="329.662790625" y1="42.1132" x2="329.68565" y2="42.18178125" width="0.025" layer="94"/>
<wire x1="329.68565" y1="42.18178125" x2="329.69708125" y2="42.2275" width="0.025" layer="94"/>
<wire x1="329.57135" y1="42.718990625" x2="329.61706875" y2="42.650409375" width="0.025" layer="94"/>
<wire x1="329.537059375" y1="42.764709375" x2="329.57135" y2="42.718990625" width="0.025" layer="94"/>
<wire x1="329.434190625" y1="42.86758125" x2="329.43533125" y2="42.866440625" width="0.025" layer="94"/>
<wire x1="329.377040625" y1="42.9133" x2="329.434190625" y2="42.86758125" width="0.025" layer="94"/>
<wire x1="329.01128125" y1="43.119040625" x2="329.06843125" y2="43.09618125" width="0.025" layer="94"/>
<wire x1="328.965559375" y1="43.1419" x2="329.01128125" y2="43.119040625" width="0.025" layer="94"/>
<wire x1="328.32548125" y1="43.32478125" x2="328.43978125" y2="43.30191875" width="0.025" layer="94"/>
<wire x1="328.2569" y1="43.336209375" x2="328.32548125" y2="43.32478125" width="0.025" layer="94"/>
<wire x1="328.15403125" y1="43.347640625" x2="328.2569" y2="43.336209375" width="0.025" layer="94"/>
<wire x1="328.005440625" y1="43.35906875" x2="328.15403125" y2="43.347640625" width="0.025" layer="94"/>
<wire x1="328.005440625" y1="43.35906875" x2="328.005440625" y2="43.53051875" width="0.025" layer="94"/>
<wire x1="328.78268125" y1="43.41621875" x2="329.010140625" y2="43.34798125" width="0.025" layer="94"/>
<wire x1="329.010140625" y1="43.34798125" x2="329.01128125" y2="43.347640625" width="0.025" layer="94"/>
<wire x1="329.01128125" y1="43.347640625" x2="329.194159375" y2="43.279059375" width="0.025" layer="94"/>
<wire x1="329.194159375" y1="43.279059375" x2="329.308459375" y2="43.233340625" width="0.025" layer="94"/>
<wire x1="329.308459375" y1="43.233340625" x2="329.3096" y2="43.23276875" width="0.025" layer="94"/>
<wire x1="329.81138125" y1="42.9133" x2="329.891390625" y2="42.833290625" width="0.025" layer="94"/>
<wire x1="329.891390625" y1="42.833290625" x2="329.937109375" y2="42.776140625" width="0.025" layer="94"/>
<wire x1="329.95996875" y1="42.74185" x2="329.994259375" y2="42.6847" width="0.025" layer="94"/>
<wire x1="329.994259375" y1="42.6847" x2="330.03998125" y2="42.5704" width="0.025" layer="94"/>
<wire x1="330.03998125" y1="42.5704" x2="330.051409375" y2="42.51325" width="0.025" layer="94"/>
<wire x1="330.051409375" y1="42.51325" x2="330.062840625" y2="42.421809375" width="0.025" layer="94"/>
<wire x1="329.55991875" y1="41.66743125" x2="329.6285" y2="41.71315" width="0.025" layer="94"/>
<wire x1="329.45705" y1="41.61028125" x2="329.55991875" y2="41.66743125" width="0.025" layer="94"/>
<wire x1="329.15986875" y1="41.47311875" x2="329.1633" y2="41.474590625" width="0.025" layer="94"/>
<wire x1="329.06843125" y1="41.43883125" x2="329.15986875" y2="41.47311875" width="0.025" layer="94"/>
<wire x1="329.137009375" y1="41.290240625" x2="329.18273125" y2="41.30166875" width="0.025" layer="94"/>
<wire x1="329.18273125" y1="41.30166875" x2="329.21701875" y2="41.3131" width="0.025" layer="94"/>
<wire x1="329.21701875" y1="41.3131" x2="329.262740625" y2="41.32453125" width="0.025" layer="94"/>
<wire x1="329.262740625" y1="41.32453125" x2="329.3999" y2="41.37025" width="0.025" layer="94"/>
<wire x1="329.57135" y1="41.43883125" x2="329.605640625" y2="41.450259375" width="0.025" layer="94"/>
<wire x1="329.605640625" y1="41.450259375" x2="329.834240625" y2="41.564559375" width="0.025" layer="94"/>
<wire x1="329.834240625" y1="41.564559375" x2="329.86853125" y2="41.58741875" width="0.025" layer="94"/>
<wire x1="329.86853125" y1="41.58741875" x2="329.891390625" y2="41.59885" width="0.025" layer="94"/>
<wire x1="330.30286875" y1="41.93031875" x2="330.348590625" y2="41.98746875" width="0.025" layer="94"/>
<wire x1="330.348590625" y1="41.98746875" x2="330.37145" y2="42.01033125" width="0.025" layer="94"/>
<wire x1="330.405740625" y1="42.05605" x2="330.47431875" y2="42.17035" width="0.025" layer="94"/>
<wire x1="330.47431875" y1="42.17035" x2="330.520040625" y2="42.28465" width="0.025" layer="94"/>
<wire x1="330.54251875" y1="42.352090625" x2="330.5429" y2="42.35323125" width="0.025" layer="94"/>
<wire x1="330.5429" y1="42.35323125" x2="330.55433125" y2="42.41038125" width="0.025" layer="94"/>
<wire x1="330.55433125" y1="42.41038125" x2="330.565759375" y2="42.478959375" width="0.025" layer="94"/>
<wire x1="328.72553125" y1="44.22775" x2="328.95413125" y2="44.18203125" width="0.025" layer="94"/>
<wire x1="328.95413125" y1="44.18203125" x2="329.22845" y2="44.11345" width="0.025" layer="94"/>
<wire x1="329.69708125" y1="43.95343125" x2="329.7428" y2="43.93056875" width="0.025" layer="94"/>
<wire x1="329.7428" y1="43.93056875" x2="329.79995" y2="43.907709375" width="0.025" layer="94"/>
<wire x1="329.79995" y1="43.907709375" x2="329.84566875" y2="43.88485" width="0.025" layer="94"/>
<wire x1="329.84566875" y1="43.88485" x2="329.90281875" y2="43.861990625" width="0.025" layer="94"/>
<wire x1="329.90281875" y1="43.861990625" x2="330.03998125" y2="43.793409375" width="0.025" layer="94"/>
<wire x1="330.03998125" y1="43.793409375" x2="330.09713125" y2="43.75911875" width="0.025" layer="94"/>
<wire x1="330.09713125" y1="43.75911875" x2="330.14285" y2="43.736259375" width="0.025" layer="94"/>
<wire x1="330.55433125" y1="43.43908125" x2="330.60005" y2="43.393359375" width="0.025" layer="94"/>
<wire x1="330.60005" y1="43.393359375" x2="330.66863125" y2="43.31335" width="0.025" layer="94"/>
<wire x1="330.66863125" y1="43.31335" x2="330.70291875" y2="43.279059375" width="0.025" layer="94"/>
<wire x1="330.805790625" y1="42.193209375" x2="330.82865" y2="42.23893125" width="0.025" layer="94"/>
<wire x1="330.7715" y1="42.136059375" x2="330.805790625" y2="42.193209375" width="0.025" layer="94"/>
<wire x1="330.70291875" y1="42.04461875" x2="330.7715" y2="42.136059375" width="0.025" layer="94"/>
<wire x1="329.29703125" y1="41.233090625" x2="329.594209375" y2="41.32453125" width="0.025" layer="94"/>
<wire x1="329.1713" y1="41.03878125" x2="329.18273125" y2="41.03878125" width="0.025" layer="94"/>
<wire x1="329.18273125" y1="41.03878125" x2="329.26616875" y2="41.059640625" width="0.025" layer="94"/>
<wire x1="329.26616875" y1="41.059640625" x2="329.50276875" y2="41.118790625" width="0.025" layer="94"/>
<wire x1="329.50276875" y1="41.118790625" x2="329.7428" y2="41.18736875" width="0.025" layer="94"/>
<wire x1="329.7428" y1="41.18736875" x2="329.743940625" y2="41.18775" width="0.025" layer="94"/>
<wire x1="329.948540625" y1="41.25595" x2="330.005690625" y2="41.278809375" width="0.025" layer="94"/>
<wire x1="330.005690625" y1="41.278809375" x2="330.03998125" y2="41.290240625" width="0.025" layer="94"/>
<wire x1="330.60005" y1="41.564559375" x2="330.634340625" y2="41.58741875" width="0.025" layer="94"/>
<wire x1="330.634340625" y1="41.58741875" x2="330.691490625" y2="41.621709375" width="0.025" layer="94"/>
<wire x1="330.691490625" y1="41.621709375" x2="330.737209375" y2="41.656" width="0.025" layer="94"/>
<wire x1="331.2287" y1="42.06748125" x2="331.27441875" y2="42.12463125" width="0.025" layer="94"/>
<wire x1="331.27441875" y1="42.12463125" x2="331.308709375" y2="42.17035" width="0.025" layer="94"/>
<wire x1="331.33156875" y1="42.204640625" x2="331.343" y2="42.2275" width="0.025" layer="94"/>
<wire x1="331.343" y1="42.2275" x2="331.365859375" y2="42.261790625" width="0.025" layer="94"/>
<wire x1="331.50301875" y1="42.97045" x2="331.51445" y2="42.84471875" width="0.025" layer="94"/>
<wire x1="331.365859375" y1="43.3705" x2="331.36643125" y2="43.369359375" width="0.025" layer="94"/>
<wire x1="331.27441875" y1="43.507659375" x2="331.365859375" y2="43.3705" width="0.025" layer="94"/>
<wire x1="331.24013125" y1="43.55338125" x2="331.27441875" y2="43.507659375" width="0.025" layer="94"/>
<wire x1="330.61148125" y1="44.090590625" x2="330.66863125" y2="44.0563" width="0.025" layer="94"/>
<wire x1="330.610340625" y1="44.09135" x2="330.61148125" y2="44.090590625" width="0.025" layer="94"/>
<wire x1="329.95996875" y1="44.3992" x2="330.24571875" y2="44.2849" width="0.025" layer="94"/>
<wire x1="329.651359375" y1="44.50206875" x2="329.95996875" y2="44.3992" width="0.025" layer="94"/>
<wire x1="329.491340625" y1="44.547790625" x2="329.651359375" y2="44.50206875" width="0.025" layer="94"/>
<wire x1="329.21701875" y1="44.61636875" x2="329.491340625" y2="44.547790625" width="0.025" layer="94"/>
<wire x1="328.3712" y1="44.75353125" x2="328.47406875" y2="44.7421" width="0.025" layer="94"/>
<wire x1="328.2569" y1="44.764959375" x2="328.3712" y2="44.75353125" width="0.025" layer="94"/>
<wire x1="328.222609375" y1="44.764959375" x2="328.2569" y2="44.764959375" width="0.025" layer="94"/>
<wire x1="327.58253125" y1="40.78731875" x2="328.519790625" y2="36.21531875" width="0.025" layer="94"/>
<wire x1="326.690990625" y1="36.21531875" x2="327.58253125" y2="40.78731875" width="0.025" layer="94"/>
<wire x1="326.690990625" y1="36.21531875" x2="326.86038125" y2="37.084" width="0.025" layer="94"/>
<wire x1="324.9746" y1="37.084" x2="323.764909375" y2="32.512" width="0.025" layer="94"/>
<wire x1="323.764909375" y1="32.512" x2="325.8566" y2="32.512" width="0.025" layer="94"/>
<wire x1="325.8566" y1="32.512" x2="326.336659375" y2="34.5694" width="0.025" layer="94"/>
<wire x1="326.336659375" y1="34.5694" x2="328.862690625" y2="34.5694" width="0.025" layer="94"/>
<wire x1="328.862690625" y1="34.5694" x2="329.319890625" y2="32.512" width="0.025" layer="94"/>
<wire x1="329.319890625" y1="32.512" x2="331.44586875" y2="32.512" width="0.025" layer="94"/>
<wire x1="331.44586875" y1="32.512" x2="330.22376875" y2="37.084" width="0.025" layer="94"/>
<wire x1="328.341709375" y1="37.084" x2="328.35108125" y2="37.03828125" width="0.025" layer="94"/>
<wire x1="328.35108125" y1="37.03828125" x2="328.519790625" y2="36.21531875" width="0.025" layer="94"/>
<wire x1="328.519790625" y1="36.21531875" x2="326.690990625" y2="36.21531875" width="0.025" layer="94"/>
<wire x1="314.41516875" y1="42.29608125" x2="314.41516875" y2="32.512" width="0.025" layer="94"/>
<wire x1="314.41516875" y1="32.512" x2="316.26683125" y2="32.512" width="0.025" layer="94"/>
<wire x1="316.26683125" y1="32.512" x2="316.26683125" y2="40.21581875" width="0.025" layer="94"/>
<wire x1="316.26683125" y1="40.21581875" x2="317.98133125" y2="32.512" width="0.025" layer="94"/>
<wire x1="317.98133125" y1="32.512" x2="319.55866875" y2="32.512" width="0.025" layer="94"/>
<wire x1="319.55866875" y1="32.512" x2="321.250309375" y2="40.21581875" width="0.025" layer="94"/>
<wire x1="321.250309375" y1="40.21581875" x2="321.250309375" y2="32.512" width="0.025" layer="94"/>
<wire x1="321.250309375" y1="32.512" x2="323.12483125" y2="32.512" width="0.025" layer="94"/>
<wire x1="323.12483125" y1="32.512" x2="323.12483125" y2="42.29608125" width="0.025" layer="94"/>
<wire x1="323.12483125" y1="42.29608125" x2="320.15303125" y2="42.29608125" width="0.025" layer="94"/>
<wire x1="320.15303125" y1="42.29608125" x2="318.767990625" y2="35.575240625" width="0.025" layer="94"/>
<wire x1="318.767990625" y1="35.575240625" x2="317.364109375" y2="42.29608125" width="0.025" layer="94"/>
<wire x1="317.364109375" y1="42.29608125" x2="314.41516875" y2="42.29608125" width="0.025" layer="94"/>
<wire x1="312.83783125" y1="35.392359375" x2="312.83783125" y2="42.3418" width="0.025" layer="94"/>
<wire x1="312.83783125" y1="42.3418" x2="310.78043125" y2="42.3418" width="0.025" layer="94"/>
<wire x1="310.78043125" y1="42.3418" x2="310.78043125" y2="35.1409" width="0.025" layer="94"/>
<wire x1="310.78043125" y1="35.1409" x2="310.75756875" y2="35.0266" width="0.025" layer="94"/>
<wire x1="310.75756875" y1="35.0266" x2="310.71185" y2="34.889440625" width="0.025" layer="94"/>
<wire x1="310.71185" y1="34.889440625" x2="310.63346875" y2="34.75228125" width="0.025" layer="94"/>
<wire x1="310.63346875" y1="34.75228125" x2="310.620409375" y2="34.72941875" width="0.025" layer="94"/>
<wire x1="310.620409375" y1="34.72941875" x2="310.55183125" y2="34.660840625" width="0.025" layer="94"/>
<wire x1="310.55183125" y1="34.660840625" x2="310.506109375" y2="34.619690625" width="0.025" layer="94"/>
<wire x1="310.506109375" y1="34.619690625" x2="310.41466875" y2="34.546540625" width="0.025" layer="94"/>
<wire x1="310.41466875" y1="34.546540625" x2="310.30036875" y2="34.477959375" width="0.025" layer="94"/>
<wire x1="310.30036875" y1="34.477959375" x2="310.18606875" y2="34.432240625" width="0.025" layer="94"/>
<wire x1="310.18606875" y1="34.432240625" x2="310.02605" y2="34.38651875" width="0.025" layer="94"/>
<wire x1="310.02605" y1="34.38651875" x2="309.84316875" y2="34.363659375" width="0.025" layer="94"/>
<wire x1="309.84316875" y1="34.363659375" x2="309.660290625" y2="34.363659375" width="0.025" layer="94"/>
<wire x1="309.660290625" y1="34.363659375" x2="309.56885" y2="34.3789" width="0.025" layer="94"/>
<wire x1="309.56885" y1="34.3789" x2="309.52313125" y2="34.38651875" width="0.025" layer="94"/>
<wire x1="309.52313125" y1="34.38651875" x2="309.477409375" y2="34.39958125" width="0.025" layer="94"/>
<wire x1="309.477409375" y1="34.39958125" x2="309.431690625" y2="34.41265" width="0.025" layer="94"/>
<wire x1="309.431690625" y1="34.41265" x2="309.363109375" y2="34.432240625" width="0.025" layer="94"/>
<wire x1="309.363109375" y1="34.432240625" x2="309.22595" y2="34.50081875" width="0.025" layer="94"/>
<wire x1="309.22595" y1="34.50081875" x2="309.088790625" y2="34.592259375" width="0.025" layer="94"/>
<wire x1="309.088790625" y1="34.592259375" x2="308.95163125" y2="34.72941875" width="0.025" layer="94"/>
<wire x1="308.95163125" y1="34.72941875" x2="308.70733125" y2="35.05515" width="0.025" layer="94"/>
<wire x1="308.70733125" y1="35.05515" x2="307.145690625" y2="33.81501875" width="0.025" layer="94"/>
<wire x1="307.145690625" y1="33.81501875" x2="307.259990625" y2="33.58641875" width="0.025" layer="94"/>
<wire x1="307.259990625" y1="33.58641875" x2="307.305709375" y2="33.525459375" width="0.025" layer="94"/>
<wire x1="307.305709375" y1="33.525459375" x2="307.34571875" y2="33.47211875" width="0.025" layer="94"/>
<wire x1="307.34571875" y1="33.47211875" x2="307.39715" y2="33.403540625" width="0.025" layer="94"/>
<wire x1="307.39715" y1="33.403540625" x2="307.43935" y2="33.35781875" width="0.025" layer="94"/>
<wire x1="307.43935" y1="33.35781875" x2="307.488590625" y2="33.30448125" width="0.025" layer="94"/>
<wire x1="307.488590625" y1="33.30448125" x2="307.523759375" y2="33.26638125" width="0.025" layer="94"/>
<wire x1="307.523759375" y1="33.26638125" x2="307.55716875" y2="33.23018125" width="0.025" layer="94"/>
<wire x1="307.55716875" y1="33.23018125" x2="307.587059375" y2="33.1978" width="0.025" layer="94"/>
<wire x1="307.587059375" y1="33.1978" x2="307.62575" y2="33.155890625" width="0.025" layer="94"/>
<wire x1="307.62575" y1="33.155890625" x2="307.65036875" y2="33.12921875" width="0.025" layer="94"/>
<wire x1="307.65036875" y1="33.12921875" x2="307.67146875" y2="33.106359375" width="0.025" layer="94"/>
<wire x1="307.67146875" y1="33.106359375" x2="307.762909375" y2="33.02323125" width="0.025" layer="94"/>
<wire x1="307.762909375" y1="33.02323125" x2="307.7972" y2="32.992059375" width="0.025" layer="94"/>
<wire x1="307.7972" y1="32.992059375" x2="307.85435" y2="32.940109375" width="0.025" layer="94"/>
<wire x1="307.85435" y1="32.940109375" x2="307.89778125" y2="32.90061875" width="0.025" layer="94"/>
<wire x1="307.89778125" y1="32.90061875" x2="307.92293125" y2="32.877759375" width="0.025" layer="94"/>
<wire x1="307.92293125" y1="32.877759375" x2="308.12866875" y2="32.7406" width="0.025" layer="94"/>
<wire x1="308.12866875" y1="32.7406" x2="308.174390625" y2="32.71011875" width="0.025" layer="94"/>
<wire x1="308.174390625" y1="32.71011875" x2="308.24296875" y2="32.6705" width="0.025" layer="94"/>
<wire x1="308.24296875" y1="32.6705" x2="308.288690625" y2="32.646109375" width="0.025" layer="94"/>
<wire x1="308.288690625" y1="32.646109375" x2="308.3687" y2="32.603440625" width="0.025" layer="94"/>
<wire x1="308.3687" y1="32.603440625" x2="308.42585" y2="32.572959375" width="0.025" layer="94"/>
<wire x1="308.42585" y1="32.572959375" x2="308.54015" y2="32.512" width="0.025" layer="94"/>
<wire x1="308.54015" y1="32.512" x2="308.66206875" y2="32.46628125" width="0.025" layer="94"/>
<wire x1="308.66206875" y1="32.46628125" x2="308.72303125" y2="32.44341875" width="0.025" layer="94"/>
<wire x1="308.72303125" y1="32.44341875" x2="308.905909375" y2="32.374840625" width="0.025" layer="94"/>
<wire x1="308.905909375" y1="32.374840625" x2="308.99735" y2="32.353740625" width="0.025" layer="94"/>
<wire x1="308.99735" y1="32.353740625" x2="309.203090625" y2="32.306259375" width="0.025" layer="94"/>
<wire x1="309.203090625" y1="32.306259375" x2="309.27166875" y2="32.295709375" width="0.025" layer="94"/>
<wire x1="309.27166875" y1="32.295709375" x2="309.35168125" y2="32.2834" width="0.025" layer="94"/>
<wire x1="309.35168125" y1="32.2834" x2="309.40883125" y2="32.274609375" width="0.025" layer="94"/>
<wire x1="309.40883125" y1="32.274609375" x2="309.477409375" y2="32.264059375" width="0.025" layer="94"/>
<wire x1="309.477409375" y1="32.264059375" x2="309.50026875" y2="32.260540625" width="0.025" layer="94"/>
<wire x1="309.50026875" y1="32.260540625" x2="309.68315" y2="32.2453" width="0.025" layer="94"/>
<wire x1="309.68315" y1="32.2453" x2="309.774590625" y2="32.23768125" width="0.025" layer="94"/>
<wire x1="309.774590625" y1="32.23768125" x2="310.048909375" y2="32.260540625" width="0.025" layer="94"/>
<wire x1="310.048909375" y1="32.260540625" x2="310.231790625" y2="32.2834" width="0.025" layer="94"/>
<wire x1="310.231790625" y1="32.2834" x2="310.460390625" y2="32.32911875" width="0.025" layer="94"/>
<wire x1="310.460390625" y1="32.32911875" x2="310.620409375" y2="32.374840625" width="0.025" layer="94"/>
<wire x1="310.620409375" y1="32.374840625" x2="310.82615" y2="32.44341875" width="0.025" layer="94"/>
<wire x1="310.82615" y1="32.44341875" x2="311.05475" y2="32.534859375" width="0.025" layer="94"/>
<wire x1="311.05475" y1="32.534859375" x2="311.374790625" y2="32.69488125" width="0.025" layer="94"/>
<wire x1="311.374790625" y1="32.69488125" x2="311.58053125" y2="32.832040625" width="0.025" layer="94"/>
<wire x1="311.58053125" y1="32.832040625" x2="311.649109375" y2="32.889190625" width="0.025" layer="94"/>
<wire x1="311.649109375" y1="32.889190625" x2="311.69483125" y2="32.927290625" width="0.025" layer="94"/>
<wire x1="311.69483125" y1="32.927290625" x2="311.763409375" y2="32.984440625" width="0.025" layer="94"/>
<wire x1="311.763409375" y1="32.984440625" x2="311.85485" y2="33.060640625" width="0.025" layer="94"/>
<wire x1="311.85485" y1="33.060640625" x2="312.106309375" y2="33.3121" width="0.025" layer="94"/>
<wire x1="312.106309375" y1="33.3121" x2="312.15203125" y2="33.373059375" width="0.025" layer="94"/>
<wire x1="312.15203125" y1="33.373059375" x2="312.174890625" y2="33.403540625" width="0.025" layer="94"/>
<wire x1="312.174890625" y1="33.403540625" x2="312.220609375" y2="33.4645" width="0.025" layer="94"/>
<wire x1="312.220609375" y1="33.4645" x2="312.258709375" y2="33.517840625" width="0.025" layer="94"/>
<wire x1="312.258709375" y1="33.517840625" x2="312.30443125" y2="33.58641875" width="0.025" layer="94"/>
<wire x1="312.30443125" y1="33.58641875" x2="312.334909375" y2="33.632140625" width="0.025" layer="94"/>
<wire x1="312.334909375" y1="33.632140625" x2="312.39586875" y2="33.72358125" width="0.025" layer="94"/>
<wire x1="312.39586875" y1="33.72358125" x2="312.42635" y2="33.7693" width="0.025" layer="94"/>
<wire x1="312.42635" y1="33.7693" x2="312.60923125" y2="34.135059375" width="0.025" layer="94"/>
<wire x1="312.60923125" y1="34.135059375" x2="312.65495" y2="34.249359375" width="0.025" layer="94"/>
<wire x1="312.65495" y1="34.249359375" x2="312.68108125" y2="34.3408" width="0.025" layer="94"/>
<wire x1="312.68108125" y1="34.3408" x2="312.7121" y2="34.4551" width="0.025" layer="94"/>
<wire x1="312.7121" y1="34.4551" x2="312.746390625" y2="34.61511875" width="0.025" layer="94"/>
<wire x1="312.746390625" y1="34.61511875" x2="312.76925" y2="34.72941875" width="0.025" layer="94"/>
<wire x1="312.76925" y1="34.72941875" x2="312.792109375" y2="34.889440625" width="0.025" layer="94"/>
<wire x1="312.792109375" y1="34.889440625" x2="312.809890625" y2="35.049459375" width="0.025" layer="94"/>
<wire x1="312.809890625" y1="35.049459375" x2="312.81673125" y2="35.118040625" width="0.025" layer="94"/>
<wire x1="312.81673125" y1="35.118040625" x2="312.823759375" y2="35.20948125" width="0.025" layer="94"/>
<wire x1="312.823759375" y1="35.20948125" x2="312.829040625" y2="35.278059375" width="0.025" layer="94"/>
<wire x1="312.829040625" y1="35.278059375" x2="312.83783125" y2="35.392359375" width="0.025" layer="94"/>
<wire x1="324.9625" y1="37.03828125" x2="325.251490625" y2="38.13625" width="0.025" layer="94"/>
<wire x1="325.251490625" y1="38.13625" x2="325.993759375" y2="40.935909375" width="0.025" layer="94"/>
<wire x1="325.993759375" y1="40.935909375" x2="325.90231875" y2="40.95876875" width="0.025" layer="94"/>
<wire x1="325.90231875" y1="40.95876875" x2="325.78801875" y2="40.98163125" width="0.025" layer="94"/>
<wire x1="325.78801875" y1="40.98163125" x2="325.63943125" y2="41.01591875" width="0.025" layer="94"/>
<wire x1="325.63943125" y1="41.01591875" x2="325.536559375" y2="41.03878125" width="0.025" layer="94"/>
<wire x1="325.536559375" y1="41.03878125" x2="325.3994" y2="41.07306875" width="0.025" layer="94"/>
<wire x1="325.3994" y1="41.07306875" x2="325.319390625" y2="41.09593125" width="0.025" layer="94"/>
<wire x1="325.319390625" y1="41.09593125" x2="325.21651875" y2="41.13021875" width="0.025" layer="94"/>
<wire x1="325.21651875" y1="41.13021875" x2="325.06793125" y2="41.175940625" width="0.025" layer="94"/>
<wire x1="325.06793125" y1="41.175940625" x2="324.97076875" y2="41.21251875" width="0.025" layer="94"/>
<wire x1="324.97076875" y1="41.21251875" x2="324.747890625" y2="41.30166875" width="0.025" layer="94"/>
<wire x1="324.747890625" y1="41.30166875" x2="324.565009375" y2="41.38168125" width="0.025" layer="94"/>
<wire x1="324.565009375" y1="41.38168125" x2="324.45185" y2="41.438259375" width="0.025" layer="94"/>
<wire x1="324.45185" y1="41.438259375" x2="324.393559375" y2="41.47311875" width="0.025" layer="94"/>
<wire x1="324.393559375" y1="41.47311875" x2="324.347840625" y2="41.49598125" width="0.025" layer="94"/>
<wire x1="324.347840625" y1="41.49598125" x2="324.18781875" y2="41.58741875" width="0.025" layer="94"/>
<wire x1="324.18781875" y1="41.58741875" x2="324.08495" y2="41.656" width="0.025" layer="94"/>
<wire x1="324.08495" y1="41.656" x2="323.85635" y2="41.82745" width="0.025" layer="94"/>
<wire x1="323.85635" y1="41.82745" x2="323.776340625" y2="41.89603125" width="0.025" layer="94"/>
<wire x1="323.776340625" y1="41.89603125" x2="323.662040625" y2="42.01033125" width="0.025" layer="94"/>
<wire x1="323.662040625" y1="42.01033125" x2="323.593459375" y2="42.090340625" width="0.025" layer="94"/>
<wire x1="323.593459375" y1="42.090340625" x2="323.547740625" y2="42.147490625" width="0.025" layer="94"/>
<wire x1="323.547740625" y1="42.147490625" x2="323.52488125" y2="42.18178125" width="0.025" layer="94"/>
<wire x1="323.52488125" y1="42.18178125" x2="323.490590625" y2="42.2275" width="0.025" layer="94"/>
<wire x1="323.490590625" y1="42.2275" x2="323.46773125" y2="42.261790625" width="0.025" layer="94"/>
<wire x1="323.46773125" y1="42.261790625" x2="323.433440625" y2="42.318940625" width="0.025" layer="94"/>
<wire x1="323.433440625" y1="42.318940625" x2="323.41058125" y2="42.35323125" width="0.025" layer="94"/>
<wire x1="323.41058125" y1="42.35323125" x2="323.354" y2="42.466390625" width="0.025" layer="94"/>
<wire x1="323.354" y1="42.466390625" x2="323.33056875" y2="42.52468125" width="0.025" layer="94"/>
<wire x1="323.33056875" y1="42.52468125" x2="323.29628125" y2="42.62755" width="0.025" layer="94"/>
<wire x1="323.29628125" y1="42.62755" x2="323.28485" y2="42.67326875" width="0.025" layer="94"/>
<wire x1="323.28485" y1="42.67326875" x2="323.27341875" y2="42.73041875" width="0.025" layer="94"/>
<wire x1="323.27341875" y1="42.73041875" x2="323.261990625" y2="42.81043125" width="0.025" layer="94"/>
<wire x1="323.261990625" y1="42.81043125" x2="323.250559375" y2="42.936159375" width="0.025" layer="94"/>
<wire x1="323.250559375" y1="42.936159375" x2="323.261990625" y2="43.061890625" width="0.025" layer="94"/>
<wire x1="323.261990625" y1="43.061890625" x2="323.27341875" y2="43.13046875" width="0.025" layer="94"/>
<wire x1="323.27341875" y1="43.13046875" x2="323.28485" y2="43.18761875" width="0.025" layer="94"/>
<wire x1="323.28485" y1="43.18761875" x2="323.29628125" y2="43.233340625" width="0.025" layer="94"/>
<wire x1="323.29628125" y1="43.233340625" x2="323.33056875" y2="43.336209375" width="0.025" layer="94"/>
<wire x1="323.33056875" y1="43.336209375" x2="323.35343125" y2="43.393359375" width="0.025" layer="94"/>
<wire x1="323.35343125" y1="43.393359375" x2="323.41058125" y2="43.507659375" width="0.025" layer="94"/>
<wire x1="323.41058125" y1="43.507659375" x2="323.44486875" y2="43.564809375" width="0.025" layer="94"/>
<wire x1="323.44486875" y1="43.564809375" x2="323.479159375" y2="43.61053125" width="0.025" layer="94"/>
<wire x1="323.479159375" y1="43.61053125" x2="323.536309375" y2="43.690540625" width="0.025" layer="94"/>
<wire x1="323.536309375" y1="43.690540625" x2="323.62775" y2="43.793409375" width="0.025" layer="94"/>
<wire x1="323.62775" y1="43.793409375" x2="323.78776875" y2="43.95343125" width="0.025" layer="94"/>
<wire x1="323.78776875" y1="43.95343125" x2="323.87806875" y2="44.0323" width="0.025" layer="94"/>
<wire x1="323.87806875" y1="44.0323" x2="323.9375" y2="44.08001875" width="0.025" layer="94"/>
<wire x1="323.9375" y1="44.08001875" x2="324.0278" y2="44.147740625" width="0.025" layer="94"/>
<wire x1="324.0278" y1="44.147740625" x2="324.164959375" y2="44.23918125" width="0.025" layer="94"/>
<wire x1="324.164959375" y1="44.23918125" x2="324.290690625" y2="44.319190625" width="0.025" layer="94"/>
<wire x1="324.290690625" y1="44.319190625" x2="324.393559375" y2="44.376340625" width="0.025" layer="94"/>
<wire x1="324.393559375" y1="44.376340625" x2="324.62101875" y2="44.49006875" width="0.025" layer="94"/>
<wire x1="324.62101875" y1="44.49006875" x2="324.72503125" y2="44.536359375" width="0.025" layer="94"/>
<wire x1="324.72503125" y1="44.536359375" x2="325.009640625" y2="44.6502" width="0.025" layer="94"/>
<wire x1="325.009640625" y1="44.6502" x2="325.10908125" y2="44.68343125" width="0.025" layer="94"/>
<wire x1="325.10908125" y1="44.68343125" x2="325.287390625" y2="44.742859375" width="0.025" layer="94"/>
<wire x1="325.287390625" y1="44.742859375" x2="325.35368125" y2="44.764959375" width="0.025" layer="94"/>
<wire x1="325.35368125" y1="44.764959375" x2="325.43483125" y2="44.7881" width="0.025" layer="94"/>
<wire x1="325.43483125" y1="44.7881" x2="325.5137" y2="44.81068125" width="0.025" layer="94"/>
<wire x1="325.5137" y1="44.81068125" x2="325.741159375" y2="44.867540625" width="0.025" layer="94"/>
<wire x1="325.741159375" y1="44.867540625" x2="325.84516875" y2="44.890690625" width="0.025" layer="94"/>
<wire x1="325.84516875" y1="44.890690625" x2="326.07376875" y2="44.936409375" width="0.025" layer="94"/>
<wire x1="326.07376875" y1="44.936409375" x2="326.14235" y2="44.947840625" width="0.025" layer="94"/>
<wire x1="326.14235" y1="44.947840625" x2="326.222359375" y2="44.95926875" width="0.025" layer="94"/>
<wire x1="326.222359375" y1="44.95926875" x2="326.35951875" y2="44.98213125" width="0.025" layer="94"/>
<wire x1="326.35951875" y1="44.98213125" x2="326.43953125" y2="44.993559375" width="0.025" layer="94"/>
<wire x1="326.43953125" y1="44.993559375" x2="326.622409375" y2="45.01641875" width="0.025" layer="94"/>
<wire x1="326.622409375" y1="45.01641875" x2="326.72528125" y2="45.02785" width="0.025" layer="94"/>
<wire x1="326.72528125" y1="45.02785" x2="326.862440625" y2="45.03928125" width="0.025" layer="94"/>
<wire x1="326.862440625" y1="45.03928125" x2="326.862440625" y2="44.764959375" width="0.025" layer="94"/>
<wire x1="326.862440625" y1="44.764959375" x2="326.75956875" y2="44.75353125" width="0.025" layer="94"/>
<wire x1="326.75956875" y1="44.75353125" x2="326.519540625" y2="44.719240625" width="0.025" layer="94"/>
<wire x1="326.519540625" y1="44.719240625" x2="326.450959375" y2="44.707809375" width="0.025" layer="94"/>
<wire x1="326.450959375" y1="44.707809375" x2="326.32523125" y2="44.68495" width="0.025" layer="94"/>
<wire x1="326.32523125" y1="44.68495" x2="326.09663125" y2="44.63923125" width="0.025" layer="94"/>
<wire x1="326.09663125" y1="44.63923125" x2="325.993759375" y2="44.61636875" width="0.025" layer="94"/>
<wire x1="325.993759375" y1="44.61636875" x2="325.84516875" y2="44.58208125" width="0.025" layer="94"/>
<wire x1="325.84516875" y1="44.58208125" x2="325.67371875" y2="44.536359375" width="0.025" layer="94"/>
<wire x1="325.67371875" y1="44.536359375" x2="325.520559375" y2="44.4926" width="0.025" layer="94"/>
<wire x1="325.520559375" y1="44.4926" x2="325.4154" y2="44.45786875" width="0.025" layer="94"/>
<wire x1="325.4154" y1="44.45786875" x2="325.23938125" y2="44.3992" width="0.025" layer="94"/>
<wire x1="325.23938125" y1="44.3992" x2="325.06793125" y2="44.33061875" width="0.025" layer="94"/>
<wire x1="325.06793125" y1="44.33061875" x2="324.93076875" y2="44.27346875" width="0.025" layer="94"/>
<wire x1="324.93076875" y1="44.27346875" x2="324.679309375" y2="44.147740625" width="0.025" layer="94"/>
<wire x1="324.679309375" y1="44.147740625" x2="324.531859375" y2="44.05695" width="0.025" layer="94"/>
<wire x1="324.531859375" y1="44.05695" x2="324.462140625" y2="44.01058125" width="0.025" layer="94"/>
<wire x1="324.462140625" y1="44.01058125" x2="324.279259375" y2="43.87341875" width="0.025" layer="94"/>
<wire x1="324.279259375" y1="43.87341875" x2="324.164959375" y2="43.77055" width="0.025" layer="94"/>
<wire x1="324.164959375" y1="43.77055" x2="324.03923125" y2="43.64481875" width="0.025" layer="94"/>
<wire x1="324.03923125" y1="43.64481875" x2="323.97065" y2="43.564809375" width="0.025" layer="94"/>
<wire x1="323.97065" y1="43.564809375" x2="323.90206875" y2="43.47336875" width="0.025" layer="94"/>
<wire x1="323.90206875" y1="43.47336875" x2="323.86778125" y2="43.41621875" width="0.025" layer="94"/>
<wire x1="323.86778125" y1="43.41621875" x2="323.822059375" y2="43.336209375" width="0.025" layer="94"/>
<wire x1="323.822059375" y1="43.336209375" x2="323.78776875" y2="43.26763125" width="0.025" layer="94"/>
<wire x1="323.78776875" y1="43.26763125" x2="323.75348125" y2="43.176190625" width="0.025" layer="94"/>
<wire x1="323.75348125" y1="43.176190625" x2="323.73061875" y2="43.107609375" width="0.025" layer="94"/>
<wire x1="323.73061875" y1="43.107609375" x2="323.707759375" y2="43.004740625" width="0.025" layer="94"/>
<wire x1="323.707759375" y1="43.004740625" x2="323.69633125" y2="42.90186875" width="0.025" layer="94"/>
<wire x1="323.69633125" y1="42.90186875" x2="323.6849" y2="42.821859375" width="0.025" layer="94"/>
<wire x1="323.6849" y1="42.821859375" x2="323.6849" y2="42.776140625" width="0.025" layer="94"/>
<wire x1="323.6849" y1="42.776140625" x2="323.69633125" y2="42.650409375" width="0.025" layer="94"/>
<wire x1="323.69633125" y1="42.650409375" x2="323.73061875" y2="42.51325" width="0.025" layer="94"/>
<wire x1="323.73061875" y1="42.51325" x2="323.75348125" y2="42.433240625" width="0.025" layer="94"/>
<wire x1="323.75348125" y1="42.433240625" x2="323.7992" y2="42.33036875" width="0.025" layer="94"/>
<wire x1="323.7992" y1="42.33036875" x2="323.822059375" y2="42.28465" width="0.025" layer="94"/>
<wire x1="323.822059375" y1="42.28465" x2="323.890640625" y2="42.18178125" width="0.025" layer="94"/>
<wire x1="323.890640625" y1="42.18178125" x2="323.95921875" y2="42.090340625" width="0.025" layer="94"/>
<wire x1="323.95921875" y1="42.090340625" x2="324.004940625" y2="42.033190625" width="0.025" layer="94"/>
<wire x1="324.004940625" y1="42.033190625" x2="324.08495" y2="41.95318125" width="0.025" layer="94"/>
<wire x1="324.08495" y1="41.95318125" x2="324.176390625" y2="41.87316875" width="0.025" layer="94"/>
<wire x1="324.176390625" y1="41.87316875" x2="324.24496875" y2="41.81601875" width="0.025" layer="94"/>
<wire x1="324.24496875" y1="41.81601875" x2="324.30211875" y2="41.7703" width="0.025" layer="94"/>
<wire x1="324.30211875" y1="41.7703" x2="324.347840625" y2="41.736009375" width="0.025" layer="94"/>
<wire x1="324.347840625" y1="41.736009375" x2="324.41641875" y2="41.690290625" width="0.025" layer="94"/>
<wire x1="324.41641875" y1="41.690290625" x2="324.53071875" y2="41.61028125" width="0.025" layer="94"/>
<wire x1="324.53071875" y1="41.61028125" x2="324.64501875" y2="41.5417" width="0.025" layer="94"/>
<wire x1="324.64501875" y1="41.5417" x2="324.78218125" y2="41.461690625" width="0.025" layer="94"/>
<wire x1="324.78218125" y1="41.461690625" x2="324.919340625" y2="41.393109375" width="0.025" layer="94"/>
<wire x1="324.919340625" y1="41.393109375" x2="325.022209375" y2="41.347390625" width="0.025" layer="94"/>
<wire x1="325.022209375" y1="41.347390625" x2="325.079359375" y2="41.32453125" width="0.025" layer="94"/>
<wire x1="325.079359375" y1="41.32453125" x2="325.15936875" y2="41.290240625" width="0.025" layer="94"/>
<wire x1="325.15936875" y1="41.290240625" x2="325.250809375" y2="41.25595" width="0.025" layer="94"/>
<wire x1="325.250809375" y1="41.25595" x2="325.38796875" y2="41.21023125" width="0.025" layer="94"/>
<wire x1="325.38796875" y1="41.21023125" x2="325.46798125" y2="41.18736875" width="0.025" layer="94"/>
<wire x1="325.46798125" y1="41.18736875" x2="325.58228125" y2="41.15308125" width="0.025" layer="94"/>
<wire x1="325.58228125" y1="41.15308125" x2="325.708009375" y2="41.118790625" width="0.025" layer="94"/>
<wire x1="325.708009375" y1="41.118790625" x2="325.82116875" y2="41.087640625" width="0.025" layer="94"/>
<wire x1="325.82116875" y1="41.087640625" x2="326.01661875" y2="41.03878125" width="0.025" layer="94"/>
<wire x1="326.01661875" y1="41.03878125" x2="326.062340625" y2="41.18736875" width="0.025" layer="94"/>
<wire x1="326.062340625" y1="41.18736875" x2="326.02805" y2="41.1988" width="0.025" layer="94"/>
<wire x1="326.02805" y1="41.1988" x2="325.78801875" y2="41.26738125" width="0.025" layer="94"/>
<wire x1="325.78801875" y1="41.26738125" x2="325.5137" y2="41.35881875" width="0.025" layer="94"/>
<wire x1="325.5137" y1="41.35881875" x2="325.433690625" y2="41.393109375" width="0.025" layer="94"/>
<wire x1="325.433690625" y1="41.393109375" x2="325.319390625" y2="41.43883125" width="0.025" layer="94"/>
<wire x1="325.319390625" y1="41.43883125" x2="325.233659375" y2="41.481690625" width="0.025" layer="94"/>
<wire x1="325.233659375" y1="41.481690625" x2="325.10221875" y2="41.55313125" width="0.025" layer="94"/>
<wire x1="325.10221875" y1="41.55313125" x2="325.01078125" y2="41.614090625" width="0.025" layer="94"/>
<wire x1="325.01078125" y1="41.614090625" x2="324.93076875" y2="41.66743125" width="0.025" layer="94"/>
<wire x1="324.93076875" y1="41.66743125" x2="324.88505" y2="41.70171875" width="0.025" layer="94"/>
<wire x1="324.88505" y1="41.70171875" x2="324.8279" y2="41.747440625" width="0.025" layer="94"/>
<wire x1="324.8279" y1="41.747440625" x2="324.78218125" y2="41.78173125" width="0.025" layer="94"/>
<wire x1="324.78218125" y1="41.78173125" x2="324.66788125" y2="41.87316875" width="0.025" layer="94"/>
<wire x1="324.66788125" y1="41.87316875" x2="324.507859375" y2="42.033190625" width="0.025" layer="94"/>
<wire x1="324.507859375" y1="42.033190625" x2="324.47356875" y2="42.078909375" width="0.025" layer="94"/>
<wire x1="324.47356875" y1="42.078909375" x2="324.42785" y2="42.147490625" width="0.025" layer="94"/>
<wire x1="324.42785" y1="42.147490625" x2="324.393559375" y2="42.204640625" width="0.025" layer="94"/>
<wire x1="324.393559375" y1="42.204640625" x2="324.3707" y2="42.250359375" width="0.025" layer="94"/>
<wire x1="324.3707" y1="42.250359375" x2="324.336409375" y2="42.33036875" width="0.025" layer="94"/>
<wire x1="324.336409375" y1="42.33036875" x2="324.31355" y2="42.41038125" width="0.025" layer="94"/>
<wire x1="324.31355" y1="42.41038125" x2="324.30211875" y2="42.44466875" width="0.025" layer="94"/>
<wire x1="324.30211875" y1="42.44466875" x2="324.290690625" y2="42.50181875" width="0.025" layer="94"/>
<wire x1="324.290690625" y1="42.50181875" x2="324.279259375" y2="42.604690625" width="0.025" layer="94"/>
<wire x1="324.279259375" y1="42.604690625" x2="324.279259375" y2="42.764709375" width="0.025" layer="94"/>
<wire x1="324.279259375" y1="42.764709375" x2="324.290690625" y2="42.84471875" width="0.025" layer="94"/>
<wire x1="324.290690625" y1="42.84471875" x2="324.30211875" y2="42.90186875" width="0.025" layer="94"/>
<wire x1="324.30211875" y1="42.90186875" x2="324.347840625" y2="43.03903125" width="0.025" layer="94"/>
<wire x1="324.347840625" y1="43.03903125" x2="324.38213125" y2="43.107609375" width="0.025" layer="94"/>
<wire x1="324.38213125" y1="43.107609375" x2="324.41641875" y2="43.164759375" width="0.025" layer="94"/>
<wire x1="324.41641875" y1="43.164759375" x2="324.519290625" y2="43.30191875" width="0.025" layer="94"/>
<wire x1="324.519290625" y1="43.30191875" x2="324.70216875" y2="43.4848" width="0.025" layer="94"/>
<wire x1="324.70216875" y1="43.4848" x2="324.77075" y2="43.54195" width="0.025" layer="94"/>
<wire x1="324.77075" y1="43.54195" x2="324.81646875" y2="43.576240625" width="0.025" layer="94"/>
<wire x1="324.81646875" y1="43.576240625" x2="324.98678125" y2="43.68978125" width="0.025" layer="94"/>
<wire x1="324.98678125" y1="43.68978125" x2="325.10108125" y2="43.75843125" width="0.025" layer="94"/>
<wire x1="325.10108125" y1="43.75843125" x2="325.177659375" y2="43.796840625" width="0.025" layer="94"/>
<wire x1="325.177659375" y1="43.796840625" x2="325.376540625" y2="43.89628125" width="0.025" layer="94"/>
<wire x1="325.376540625" y1="43.89628125" x2="325.45655" y2="43.93056875" width="0.025" layer="94"/>
<wire x1="325.45655" y1="43.93056875" x2="325.53426875" y2="43.96143125" width="0.025" layer="94"/>
<wire x1="325.53426875" y1="43.96143125" x2="325.68515" y2="44.022009375" width="0.025" layer="94"/>
<wire x1="325.68515" y1="44.022009375" x2="325.82116875" y2="44.06735" width="0.025" layer="94"/>
<wire x1="325.82116875" y1="44.06735" x2="325.94346875" y2="44.10235" width="0.025" layer="94"/>
<wire x1="325.94346875" y1="44.10235" x2="326.06348125" y2="44.136590625" width="0.025" layer="94"/>
<wire x1="326.06348125" y1="44.136590625" x2="326.15378125" y2="44.15916875" width="0.025" layer="94"/>
<wire x1="326.15378125" y1="44.15916875" x2="326.230359375" y2="44.17631875" width="0.025" layer="94"/>
<wire x1="326.230359375" y1="44.17631875" x2="326.35951875" y2="44.204890625" width="0.025" layer="94"/>
<wire x1="326.35951875" y1="44.204890625" x2="326.47381875" y2="44.22775" width="0.025" layer="94"/>
<wire x1="326.47381875" y1="44.22775" x2="326.622409375" y2="44.250609375" width="0.025" layer="94"/>
<wire x1="326.622409375" y1="44.250609375" x2="326.78243125" y2="44.27346875" width="0.025" layer="94"/>
<wire x1="326.78243125" y1="44.27346875" x2="326.89673125" y2="44.2849" width="0.025" layer="94"/>
<wire x1="326.89673125" y1="44.2849" x2="327.091040625" y2="44.29633125" width="0.025" layer="94"/>
<wire x1="327.091040625" y1="44.29633125" x2="327.091040625" y2="44.033440625" width="0.025" layer="94"/>
<wire x1="327.091040625" y1="44.033440625" x2="326.919590625" y2="44.033440625" width="0.025" layer="94"/>
<wire x1="326.919590625" y1="44.033440625" x2="326.793859375" y2="44.022009375" width="0.025" layer="94"/>
<wire x1="326.793859375" y1="44.022009375" x2="326.71385" y2="44.01058125" width="0.025" layer="94"/>
<wire x1="326.71385" y1="44.01058125" x2="326.576690625" y2="43.98771875" width="0.025" layer="94"/>
<wire x1="326.576690625" y1="43.98771875" x2="326.462390625" y2="43.964859375" width="0.025" layer="94"/>
<wire x1="326.462390625" y1="43.964859375" x2="326.28065" y2="43.91941875" width="0.025" layer="94"/>
<wire x1="326.28065" y1="43.91941875" x2="326.12063125" y2="43.87375" width="0.025" layer="94"/>
<wire x1="326.12063125" y1="43.87375" x2="326.02805" y2="43.850559375" width="0.025" layer="94"/>
<wire x1="326.02805" y1="43.850559375" x2="325.8566" y2="43.793409375" width="0.025" layer="94"/>
<wire x1="325.8566" y1="43.793409375" x2="325.708009375" y2="43.73396875" width="0.025" layer="94"/>
<wire x1="325.708009375" y1="43.73396875" x2="325.604" y2="43.68996875" width="0.025" layer="94"/>
<wire x1="325.604" y1="43.68996875" x2="325.536559375" y2="43.65625" width="0.025" layer="94"/>
<wire x1="325.536559375" y1="43.65625" x2="325.45655" y2="43.61053125" width="0.025" layer="94"/>
<wire x1="325.45655" y1="43.61053125" x2="325.341109375" y2="43.541190625" width="0.025" layer="94"/>
<wire x1="325.341109375" y1="43.541190625" x2="325.205090625" y2="43.450509375" width="0.025" layer="94"/>
<wire x1="325.205090625" y1="43.450509375" x2="325.11365" y2="43.38193125" width="0.025" layer="94"/>
<wire x1="325.11365" y1="43.38193125" x2="325.033640625" y2="43.32478125" width="0.025" layer="94"/>
<wire x1="325.033640625" y1="43.32478125" x2="324.86133125" y2="43.152190625" width="0.025" layer="94"/>
<wire x1="324.86133125" y1="43.152190625" x2="324.828759375" y2="43.10875" width="0.025" layer="94"/>
<wire x1="324.828759375" y1="43.10875" x2="324.75931875" y2="43.004740625" width="0.025" layer="94"/>
<wire x1="324.75931875" y1="43.004740625" x2="324.70216875" y2="42.890440625" width="0.025" layer="94"/>
<wire x1="324.70216875" y1="42.890440625" x2="324.66788125" y2="42.78756875" width="0.025" layer="94"/>
<wire x1="324.66788125" y1="42.78756875" x2="324.65645" y2="42.74185" width="0.025" layer="94"/>
<wire x1="324.65645" y1="42.74185" x2="324.633590625" y2="42.626409375" width="0.025" layer="94"/>
<wire x1="324.633590625" y1="42.626409375" x2="324.633590625" y2="42.50181875" width="0.025" layer="94"/>
<wire x1="324.633590625" y1="42.50181875" x2="324.64501875" y2="42.421809375" width="0.025" layer="94"/>
<wire x1="324.64501875" y1="42.421809375" x2="324.66788125" y2="42.33036875" width="0.025" layer="94"/>
<wire x1="324.66788125" y1="42.33036875" x2="324.690740625" y2="42.261790625" width="0.025" layer="94"/>
<wire x1="324.690740625" y1="42.261790625" x2="324.7136" y2="42.204640625" width="0.025" layer="94"/>
<wire x1="324.7136" y1="42.204640625" x2="324.747890625" y2="42.147490625" width="0.025" layer="94"/>
<wire x1="324.747890625" y1="42.147490625" x2="324.793609375" y2="42.06748125" width="0.025" layer="94"/>
<wire x1="324.793609375" y1="42.06748125" x2="324.874759375" y2="41.96346875" width="0.025" layer="94"/>
<wire x1="324.874759375" y1="41.96346875" x2="324.976490625" y2="41.861740625" width="0.025" layer="94"/>
<wire x1="324.976490625" y1="41.861740625" x2="325.090790625" y2="41.75886875" width="0.025" layer="94"/>
<wire x1="325.090790625" y1="41.75886875" x2="325.193659375" y2="41.678859375" width="0.025" layer="94"/>
<wire x1="325.193659375" y1="41.678859375" x2="325.27366875" y2="41.621709375" width="0.025" layer="94"/>
<wire x1="325.27366875" y1="41.621709375" x2="325.433690625" y2="41.53026875" width="0.025" layer="94"/>
<wire x1="325.433690625" y1="41.53026875" x2="325.58228125" y2="41.461690625" width="0.025" layer="94"/>
<wire x1="325.58228125" y1="41.461690625" x2="325.68515" y2="41.41596875" width="0.025" layer="94"/>
<wire x1="325.68515" y1="41.41596875" x2="325.84516875" y2="41.35881875" width="0.025" layer="94"/>
<wire x1="325.84516875" y1="41.35881875" x2="325.91718125" y2="41.33481875" width="0.025" layer="94"/>
<wire x1="325.91718125" y1="41.33481875" x2="325.969759375" y2="41.317290625" width="0.025" layer="94"/>
<wire x1="325.969759375" y1="41.317290625" x2="326.084059375" y2="41.279190625" width="0.025" layer="94"/>
<wire x1="326.084059375" y1="41.279190625" x2="326.11925" y2="41.437690625" width="0.025" layer="94"/>
<wire x1="326.11925" y1="41.437690625" x2="325.99261875" y2="41.49646875" width="0.025" layer="94"/>
<wire x1="325.99261875" y1="41.49646875" x2="325.91375" y2="41.53026875" width="0.025" layer="94"/>
<wire x1="325.91375" y1="41.53026875" x2="325.81088125" y2="41.575990625" width="0.025" layer="94"/>
<wire x1="325.81088125" y1="41.575990625" x2="325.652" y2="41.66678125" width="0.025" layer="94"/>
<wire x1="325.652" y1="41.66678125" x2="325.55941875" y2="41.72458125" width="0.025" layer="94"/>
<wire x1="325.55941875" y1="41.72458125" x2="325.422259375" y2="41.83888125" width="0.025" layer="94"/>
<wire x1="325.422259375" y1="41.83888125" x2="325.365109375" y2="41.89603125" width="0.025" layer="94"/>
<wire x1="325.365109375" y1="41.89603125" x2="325.307959375" y2="41.964609375" width="0.025" layer="94"/>
<wire x1="325.307959375" y1="41.964609375" x2="325.21651875" y2="42.10176875" width="0.025" layer="94"/>
<wire x1="325.21651875" y1="42.10176875" x2="325.18223125" y2="42.18178125" width="0.025" layer="94"/>
<wire x1="325.18223125" y1="42.18178125" x2="325.15936875" y2="42.250359375" width="0.025" layer="94"/>
<wire x1="325.15936875" y1="42.250359375" x2="325.147940625" y2="42.318940625" width="0.025" layer="94"/>
<wire x1="325.147940625" y1="42.318940625" x2="325.147940625" y2="42.478959375" width="0.025" layer="94"/>
<wire x1="325.147940625" y1="42.478959375" x2="325.15936875" y2="42.547540625" width="0.025" layer="94"/>
<wire x1="325.15936875" y1="42.547540625" x2="325.1708" y2="42.593259375" width="0.025" layer="94"/>
<wire x1="325.1708" y1="42.593259375" x2="325.193659375" y2="42.650409375" width="0.025" layer="94"/>
<wire x1="325.193659375" y1="42.650409375" x2="325.22795" y2="42.718990625" width="0.025" layer="94"/>
<wire x1="325.22795" y1="42.718990625" x2="325.250809375" y2="42.75328125" width="0.025" layer="94"/>
<wire x1="325.250809375" y1="42.75328125" x2="325.2851" y2="42.799" width="0.025" layer="94"/>
<wire x1="325.2851" y1="42.799" x2="325.44511875" y2="42.95901875" width="0.025" layer="94"/>
<wire x1="325.44511875" y1="42.95901875" x2="325.5377" y2="43.028359375" width="0.025" layer="94"/>
<wire x1="325.5377" y1="43.028359375" x2="325.605140625" y2="43.07331875" width="0.025" layer="94"/>
<wire x1="325.605140625" y1="43.07331875" x2="325.68858125" y2="43.123609375" width="0.025" layer="94"/>
<wire x1="325.68858125" y1="43.123609375" x2="325.776590625" y2="43.176190625" width="0.025" layer="94"/>
<wire x1="325.776590625" y1="43.176190625" x2="325.84516875" y2="43.21048125" width="0.025" layer="94"/>
<wire x1="325.84516875" y1="43.21048125" x2="326.005190625" y2="43.279059375" width="0.025" layer="94"/>
<wire x1="326.005190625" y1="43.279059375" x2="326.09663125" y2="43.31335" width="0.025" layer="94"/>
<wire x1="326.09663125" y1="43.31335" x2="326.37095" y2="43.404790625" width="0.025" layer="94"/>
<wire x1="326.37095" y1="43.404790625" x2="326.49668125" y2="43.43908125" width="0.025" layer="94"/>
<wire x1="326.49668125" y1="43.43908125" x2="326.56411875" y2="43.454640625" width="0.025" layer="94"/>
<wire x1="326.56411875" y1="43.454640625" x2="326.64526875" y2="43.47336875" width="0.025" layer="94"/>
<wire x1="326.64526875" y1="43.47336875" x2="326.75956875" y2="43.49623125" width="0.025" layer="94"/>
<wire x1="326.75956875" y1="43.49623125" x2="326.93101875" y2="43.519090625" width="0.025" layer="94"/>
<wire x1="326.93101875" y1="43.519090625" x2="327.06818125" y2="43.53051875" width="0.025" layer="94"/>
<wire x1="327.06818125" y1="43.53051875" x2="327.205340625" y2="43.53051875" width="0.025" layer="94"/>
<wire x1="327.205340625" y1="43.53051875" x2="327.205340625" y2="43.35906875" width="0.025" layer="94"/>
<wire x1="327.205340625" y1="43.35906875" x2="327.17105" y2="43.35906875" width="0.025" layer="94"/>
<wire x1="327.17105" y1="43.35906875" x2="327.04531875" y2="43.347640625" width="0.025" layer="94"/>
<wire x1="327.04531875" y1="43.347640625" x2="326.95388125" y2="43.336209375" width="0.025" layer="94"/>
<wire x1="326.95388125" y1="43.336209375" x2="326.81671875" y2="43.31335" width="0.025" layer="94"/>
<wire x1="326.81671875" y1="43.31335" x2="326.71385" y2="43.290490625" width="0.025" layer="94"/>
<wire x1="326.71385" y1="43.290490625" x2="326.565259375" y2="43.2562" width="0.025" layer="94"/>
<wire x1="326.565259375" y1="43.2562" x2="326.48525" y2="43.233340625" width="0.025" layer="94"/>
<wire x1="326.48525" y1="43.233340625" x2="326.41666875" y2="43.21048125" width="0.025" layer="94"/>
<wire x1="326.41666875" y1="43.21048125" x2="326.32523125" y2="43.176190625" width="0.025" layer="94"/>
<wire x1="326.32523125" y1="43.176190625" x2="326.21206875" y2="43.13093125" width="0.025" layer="94"/>
<wire x1="326.21206875" y1="43.13093125" x2="326.005190625" y2="43.0276" width="0.025" layer="94"/>
<wire x1="326.005190625" y1="43.0276" x2="325.948040625" y2="42.993309375" width="0.025" layer="94"/>
<wire x1="325.948040625" y1="42.993309375" x2="325.84516875" y2="42.92473125" width="0.025" layer="94"/>
<wire x1="325.84516875" y1="42.92473125" x2="325.79945" y2="42.890440625" width="0.025" layer="94"/>
<wire x1="325.79945" y1="42.890440625" x2="325.7423" y2="42.84471875" width="0.025" layer="94"/>
<wire x1="325.7423" y1="42.84471875" x2="325.708009375" y2="42.81043125" width="0.025" layer="94"/>
<wire x1="325.708009375" y1="42.81043125" x2="325.63943125" y2="42.73041875" width="0.025" layer="94"/>
<wire x1="325.63943125" y1="42.73041875" x2="325.593709375" y2="42.661840625" width="0.025" layer="94"/>
<wire x1="325.593709375" y1="42.661840625" x2="325.55941875" y2="42.593259375" width="0.025" layer="94"/>
<wire x1="325.55941875" y1="42.593259375" x2="325.536559375" y2="42.536109375" width="0.025" layer="94"/>
<wire x1="325.536559375" y1="42.536109375" x2="325.5137" y2="42.4561" width="0.025" layer="94"/>
<wire x1="325.5137" y1="42.4561" x2="325.50226875" y2="42.39895" width="0.025" layer="94"/>
<wire x1="325.50226875" y1="42.39895" x2="325.50226875" y2="42.261790625" width="0.025" layer="94"/>
<wire x1="325.50226875" y1="42.261790625" x2="325.5137" y2="42.204640625" width="0.025" layer="94"/>
<wire x1="325.5137" y1="42.204640625" x2="325.52513125" y2="42.15891875" width="0.025" layer="94"/>
<wire x1="325.52513125" y1="42.15891875" x2="325.547609375" y2="42.09148125" width="0.025" layer="94"/>
<wire x1="325.547609375" y1="42.09148125" x2="325.57085" y2="42.04461875" width="0.025" layer="94"/>
<wire x1="325.57085" y1="42.04461875" x2="325.605140625" y2="41.98746875" width="0.025" layer="94"/>
<wire x1="325.605140625" y1="41.98746875" x2="325.67371875" y2="41.89603125" width="0.025" layer="94"/>
<wire x1="325.67371875" y1="41.89603125" x2="325.75373125" y2="41.81601875" width="0.025" layer="94"/>
<wire x1="325.75373125" y1="41.81601875" x2="325.84516875" y2="41.747440625" width="0.025" layer="94"/>
<wire x1="325.84516875" y1="41.747440625" x2="325.95833125" y2="41.67955" width="0.025" layer="94"/>
<wire x1="325.95833125" y1="41.67955" x2="326.04061875" y2="41.63256875" width="0.025" layer="94"/>
<wire x1="326.04061875" y1="41.63256875" x2="326.108059375" y2="41.59885" width="0.025" layer="94"/>
<wire x1="326.108059375" y1="41.59885" x2="326.165509375" y2="41.57713125" width="0.025" layer="94"/>
<wire x1="326.165509375" y1="41.57713125" x2="326.35951875" y2="42.307509375" width="0.025" layer="94"/>
<wire x1="326.35951875" y1="42.307509375" x2="328.8284" y2="42.307509375" width="0.025" layer="94"/>
<wire x1="328.8284" y1="42.307509375" x2="329.022709375" y2="41.575990625" width="0.025" layer="94"/>
<wire x1="329.022709375" y1="41.575990625" x2="329.087859375" y2="41.597140625" width="0.025" layer="94"/>
<wire x1="329.087859375" y1="41.597140625" x2="329.14158125" y2="41.624" width="0.025" layer="94"/>
<wire x1="329.14158125" y1="41.624" x2="329.25245" y2="41.67961875" width="0.025" layer="94"/>
<wire x1="329.25245" y1="41.67961875" x2="329.42161875" y2="41.7924" width="0.025" layer="94"/>
<wire x1="329.42161875" y1="41.7924" x2="329.548490625" y2="41.918890625" width="0.025" layer="94"/>
<wire x1="329.548490625" y1="41.918890625" x2="329.6285" y2="42.033190625" width="0.025" layer="94"/>
<wire x1="329.6285" y1="42.033190625" x2="329.662790625" y2="42.1132" width="0.025" layer="94"/>
<wire x1="329.662790625" y1="42.1132" x2="329.68593125" y2="42.18291875" width="0.025" layer="94"/>
<wire x1="329.68593125" y1="42.18291875" x2="329.69708125" y2="42.2275" width="0.025" layer="94"/>
<wire x1="329.69708125" y1="42.2275" x2="329.708509375" y2="42.307509375" width="0.025" layer="94"/>
<wire x1="329.708509375" y1="42.307509375" x2="329.708509375" y2="42.35323125" width="0.025" layer="94"/>
<wire x1="329.708509375" y1="42.35323125" x2="329.69708125" y2="42.433240625" width="0.025" layer="94"/>
<wire x1="329.69708125" y1="42.433240625" x2="329.67421875" y2="42.52468125" width="0.025" layer="94"/>
<wire x1="329.67421875" y1="42.52468125" x2="329.651359375" y2="42.58183125" width="0.025" layer="94"/>
<wire x1="329.651359375" y1="42.58183125" x2="329.61706875" y2="42.650409375" width="0.025" layer="94"/>
<wire x1="329.61706875" y1="42.650409375" x2="329.570490625" y2="42.72013125" width="0.025" layer="94"/>
<wire x1="329.570490625" y1="42.72013125" x2="329.537059375" y2="42.764709375" width="0.025" layer="94"/>
<wire x1="329.537059375" y1="42.764709375" x2="329.43533125" y2="42.866440625" width="0.025" layer="94"/>
<wire x1="329.43533125" y1="42.866440625" x2="329.377040625" y2="42.9133" width="0.025" layer="94"/>
<wire x1="329.377040625" y1="42.9133" x2="329.33131875" y2="42.947590625" width="0.025" layer="94"/>
<wire x1="329.33131875" y1="42.947590625" x2="329.21701875" y2="43.01616875" width="0.025" layer="94"/>
<wire x1="329.21701875" y1="43.01616875" x2="329.137009375" y2="43.061890625" width="0.025" layer="94"/>
<wire x1="329.137009375" y1="43.061890625" x2="329.06843125" y2="43.09618125" width="0.025" layer="94"/>
<wire x1="329.06843125" y1="43.09618125" x2="328.965559375" y2="43.1419" width="0.025" layer="94"/>
<wire x1="328.965559375" y1="43.1419" x2="328.908409375" y2="43.164759375" width="0.025" layer="94"/>
<wire x1="328.908409375" y1="43.164759375" x2="328.81696875" y2="43.19905" width="0.025" layer="94"/>
<wire x1="328.81696875" y1="43.19905" x2="328.7141" y2="43.233340625" width="0.025" layer="94"/>
<wire x1="328.7141" y1="43.233340625" x2="328.634090625" y2="43.2562" width="0.025" layer="94"/>
<wire x1="328.634090625" y1="43.2562" x2="328.54265" y2="43.279059375" width="0.025" layer="94"/>
<wire x1="328.54265" y1="43.279059375" x2="328.43978125" y2="43.30191875" width="0.025" layer="94"/>
<wire x1="328.43978125" y1="43.30191875" x2="328.32661875" y2="43.32478125" width="0.025" layer="94"/>
<wire x1="328.32661875" y1="43.32478125" x2="328.255759375" y2="43.336209375" width="0.025" layer="94"/>
<wire x1="328.255759375" y1="43.336209375" x2="328.15516875" y2="43.347640625" width="0.025" layer="94"/>
<wire x1="328.15516875" y1="43.347640625" x2="328.075159375" y2="43.353709375" width="0.025" layer="94"/>
<wire x1="328.075159375" y1="43.353709375" x2="328.00658125" y2="43.35906875" width="0.025" layer="94"/>
<wire x1="328.00658125" y1="43.35906875" x2="328.005440625" y2="43.53051875" width="0.025" layer="94"/>
<wire x1="328.005440625" y1="43.53051875" x2="328.176890625" y2="43.53051875" width="0.025" layer="94"/>
<wire x1="328.176890625" y1="43.53051875" x2="328.291190625" y2="43.519090625" width="0.025" layer="94"/>
<wire x1="328.291190625" y1="43.519090625" x2="328.3712" y2="43.507659375" width="0.025" layer="94"/>
<wire x1="328.3712" y1="43.507659375" x2="328.43978125" y2="43.49623125" width="0.025" layer="94"/>
<wire x1="328.43978125" y1="43.49623125" x2="328.61123125" y2="43.461940625" width="0.025" layer="94"/>
<wire x1="328.61123125" y1="43.461940625" x2="328.70266875" y2="43.43908125" width="0.025" layer="94"/>
<wire x1="328.70266875" y1="43.43908125" x2="328.78268125" y2="43.41621875" width="0.025" layer="94"/>
<wire x1="328.78268125" y1="43.41621875" x2="328.892409375" y2="43.38306875" width="0.025" layer="94"/>
<wire x1="328.892409375" y1="43.38306875" x2="329.010140625" y2="43.34798125" width="0.025" layer="94"/>
<wire x1="329.010140625" y1="43.34798125" x2="329.079859375" y2="43.32191875" width="0.025" layer="94"/>
<wire x1="329.079859375" y1="43.32191875" x2="329.194159375" y2="43.279059375" width="0.025" layer="94"/>
<wire x1="329.194159375" y1="43.279059375" x2="329.3096" y2="43.23276875" width="0.025" layer="94"/>
<wire x1="329.3096" y1="43.23276875" x2="329.44561875" y2="43.164759375" width="0.025" layer="94"/>
<wire x1="329.44561875" y1="43.164759375" x2="329.50276875" y2="43.13046875" width="0.025" layer="94"/>
<wire x1="329.50276875" y1="43.13046875" x2="329.548490625" y2="43.107609375" width="0.025" layer="94"/>
<wire x1="329.548490625" y1="43.107609375" x2="329.61706875" y2="43.061890625" width="0.025" layer="94"/>
<wire x1="329.61706875" y1="43.061890625" x2="329.75423125" y2="42.95901875" width="0.025" layer="94"/>
<wire x1="329.75423125" y1="42.95901875" x2="329.81138125" y2="42.9133" width="0.025" layer="94"/>
<wire x1="329.891390625" y1="42.833290625" x2="329.89253125" y2="42.83215" width="0.025" layer="94"/>
<wire x1="329.89253125" y1="42.83215" x2="329.937109375" y2="42.776140625" width="0.025" layer="94"/>
<wire x1="329.937109375" y1="42.776140625" x2="329.95996875" y2="42.74185" width="0.025" layer="94"/>
<wire x1="329.95996875" y1="42.74185" x2="329.9954" y2="42.682409375" width="0.025" layer="94"/>
<wire x1="329.9954" y1="42.682409375" x2="330.03998125" y2="42.5704" width="0.025" layer="94"/>
<wire x1="330.03998125" y1="42.5704" x2="330.051409375" y2="42.512109375" width="0.025" layer="94"/>
<wire x1="330.051409375" y1="42.512109375" x2="330.062840625" y2="42.421809375" width="0.025" layer="94"/>
<wire x1="330.062840625" y1="42.421809375" x2="330.062840625" y2="42.35323125" width="0.025" layer="94"/>
<wire x1="330.062840625" y1="42.35323125" x2="330.051409375" y2="42.261790625" width="0.025" layer="94"/>
<wire x1="330.051409375" y1="42.261790625" x2="330.03998125" y2="42.21606875" width="0.025" layer="94"/>
<wire x1="330.03998125" y1="42.21606875" x2="330.01711875" y2="42.15891875" width="0.025" layer="94"/>
<wire x1="330.01711875" y1="42.15891875" x2="329.98283125" y2="42.090340625" width="0.025" layer="94"/>
<wire x1="329.98283125" y1="42.090340625" x2="329.95996875" y2="42.05605" width="0.025" layer="94"/>
<wire x1="329.95996875" y1="42.05605" x2="329.90281875" y2="41.976040625" width="0.025" layer="94"/>
<wire x1="329.90281875" y1="41.976040625" x2="329.834240625" y2="41.89603125" width="0.025" layer="94"/>
<wire x1="329.834240625" y1="41.89603125" x2="329.7428" y2="41.804590625" width="0.025" layer="94"/>
<wire x1="329.7428" y1="41.804590625" x2="329.6285" y2="41.71315" width="0.025" layer="94"/>
<wire x1="329.6285" y1="41.71315" x2="329.55878125" y2="41.666790625" width="0.025" layer="94"/>
<wire x1="329.55878125" y1="41.666790625" x2="329.45705" y2="41.61028125" width="0.025" layer="94"/>
<wire x1="329.45705" y1="41.61028125" x2="329.319890625" y2="41.5417" width="0.025" layer="94"/>
<wire x1="329.319890625" y1="41.5417" x2="329.1633" y2="41.474590625" width="0.025" layer="94"/>
<wire x1="329.1633" y1="41.474590625" x2="329.06843125" y2="41.43883125" width="0.025" layer="94"/>
<wire x1="329.06843125" y1="41.43883125" x2="329.10271875" y2="41.278809375" width="0.025" layer="94"/>
<wire x1="329.10271875" y1="41.278809375" x2="329.137009375" y2="41.290240625" width="0.025" layer="94"/>
<wire x1="329.137009375" y1="41.290240625" x2="329.21701875" y2="41.3131" width="0.025" layer="94"/>
<wire x1="329.21701875" y1="41.3131" x2="329.3999" y2="41.37025" width="0.025" layer="94"/>
<wire x1="329.3999" y1="41.37025" x2="329.57135" y2="41.43883125" width="0.025" layer="94"/>
<wire x1="329.57135" y1="41.43883125" x2="329.834240625" y2="41.564559375" width="0.025" layer="94"/>
<wire x1="329.834240625" y1="41.564559375" x2="329.891390625" y2="41.59885" width="0.025" layer="94"/>
<wire x1="329.891390625" y1="41.59885" x2="329.994259375" y2="41.66743125" width="0.025" layer="94"/>
<wire x1="329.994259375" y1="41.66743125" x2="330.051409375" y2="41.71315" width="0.025" layer="94"/>
<wire x1="330.051409375" y1="41.71315" x2="330.14285" y2="41.78173125" width="0.025" layer="94"/>
<wire x1="330.14285" y1="41.78173125" x2="330.177140625" y2="41.81601875" width="0.025" layer="94"/>
<wire x1="330.177140625" y1="41.81601875" x2="330.25715" y2="41.8846" width="0.025" layer="94"/>
<wire x1="330.25715" y1="41.8846" x2="330.30286875" y2="41.93031875" width="0.025" layer="94"/>
<wire x1="330.30286875" y1="41.93031875" x2="330.37145" y2="42.01033125" width="0.025" layer="94"/>
<wire x1="330.37145" y1="42.01033125" x2="330.405740625" y2="42.05605" width="0.025" layer="94"/>
<wire x1="330.405740625" y1="42.05605" x2="330.47478125" y2="42.171490625" width="0.025" layer="94"/>
<wire x1="330.47478125" y1="42.171490625" x2="330.520040625" y2="42.28465" width="0.025" layer="94"/>
<wire x1="330.520040625" y1="42.28465" x2="330.54251875" y2="42.352090625" width="0.025" layer="94"/>
<wire x1="330.54251875" y1="42.352090625" x2="330.565759375" y2="42.478959375" width="0.025" layer="94"/>
<wire x1="330.565759375" y1="42.478959375" x2="330.565759375" y2="42.650409375" width="0.025" layer="94"/>
<wire x1="330.565759375" y1="42.650409375" x2="330.55433125" y2="42.718990625" width="0.025" layer="94"/>
<wire x1="330.55433125" y1="42.718990625" x2="330.5429" y2="42.776140625" width="0.025" layer="94"/>
<wire x1="330.5429" y1="42.776140625" x2="330.520040625" y2="42.84471875" width="0.025" layer="94"/>
<wire x1="330.520040625" y1="42.84471875" x2="330.49718125" y2="42.90186875" width="0.025" layer="94"/>
<wire x1="330.49718125" y1="42.90186875" x2="330.451459375" y2="42.993309375" width="0.025" layer="94"/>
<wire x1="330.451459375" y1="42.993309375" x2="330.41716875" y2="43.050459375" width="0.025" layer="94"/>
<wire x1="330.41716875" y1="43.050459375" x2="330.394309375" y2="43.08475" width="0.025" layer="94"/>
<wire x1="330.394309375" y1="43.08475" x2="330.36001875" y2="43.13046875" width="0.025" layer="94"/>
<wire x1="330.36001875" y1="43.13046875" x2="330.291440625" y2="43.21048125" width="0.025" layer="94"/>
<wire x1="330.291440625" y1="43.21048125" x2="330.234290625" y2="43.26763125" width="0.025" layer="94"/>
<wire x1="330.234290625" y1="43.26763125" x2="330.15428125" y2="43.336209375" width="0.025" layer="94"/>
<wire x1="330.15428125" y1="43.336209375" x2="330.01711875" y2="43.43908125" width="0.025" layer="94"/>
<wire x1="330.01711875" y1="43.43908125" x2="329.879959375" y2="43.53051875" width="0.025" layer="94"/>
<wire x1="329.879959375" y1="43.53051875" x2="329.765659375" y2="43.5991" width="0.025" layer="94"/>
<wire x1="329.765659375" y1="43.5991" x2="329.68565" y2="43.64481875" width="0.025" layer="94"/>
<wire x1="329.68565" y1="43.64481875" x2="329.52563125" y2="43.72483125" width="0.025" layer="94"/>
<wire x1="329.52563125" y1="43.72483125" x2="329.44561875" y2="43.75911875" width="0.025" layer="94"/>
<wire x1="329.44561875" y1="43.75911875" x2="329.1713" y2="43.850559375" width="0.025" layer="94"/>
<wire x1="329.1713" y1="43.850559375" x2="329.04556875" y2="43.88485" width="0.025" layer="94"/>
<wire x1="329.04556875" y1="43.88485" x2="328.965559375" y2="43.907709375" width="0.025" layer="94"/>
<wire x1="328.965559375" y1="43.907709375" x2="328.691240625" y2="43.976290625" width="0.025" layer="94"/>
<wire x1="328.691240625" y1="43.976290625" x2="328.634090625" y2="43.98771875" width="0.025" layer="94"/>
<wire x1="328.634090625" y1="43.98771875" x2="328.565509375" y2="43.99915" width="0.025" layer="94"/>
<wire x1="328.565509375" y1="43.99915" x2="328.394059375" y2="44.022009375" width="0.025" layer="94"/>
<wire x1="328.394059375" y1="44.022009375" x2="328.2569" y2="44.033440625" width="0.025" layer="94"/>
<wire x1="328.2569" y1="44.033440625" x2="328.119740625" y2="44.033440625" width="0.025" layer="94"/>
<wire x1="328.119740625" y1="44.033440625" x2="328.119740625" y2="44.29633125" width="0.025" layer="94"/>
<wire x1="328.119740625" y1="44.29633125" x2="328.165459375" y2="44.29633125" width="0.025" layer="94"/>
<wire x1="328.165459375" y1="44.29633125" x2="328.32548125" y2="44.2849" width="0.025" layer="94"/>
<wire x1="328.32548125" y1="44.2849" x2="328.42835" y2="44.27346875" width="0.025" layer="94"/>
<wire x1="328.42835" y1="44.27346875" x2="328.519790625" y2="44.262040625" width="0.025" layer="94"/>
<wire x1="328.519790625" y1="44.262040625" x2="328.5998" y2="44.250609375" width="0.025" layer="94"/>
<wire x1="328.5998" y1="44.250609375" x2="328.72553125" y2="44.22775" width="0.025" layer="94"/>
<wire x1="328.72553125" y1="44.22775" x2="328.952990625" y2="44.18203125" width="0.025" layer="94"/>
<wire x1="328.952990625" y1="44.18203125" x2="329.22845" y2="44.11345" width="0.025" layer="94"/>
<wire x1="329.22845" y1="44.11345" x2="329.38846875" y2="44.06773125" width="0.025" layer="94"/>
<wire x1="329.38846875" y1="44.06773125" x2="329.52563125" y2="44.022009375" width="0.025" layer="94"/>
<wire x1="329.52563125" y1="44.022009375" x2="329.69708125" y2="43.95343125" width="0.025" layer="94"/>
<wire x1="329.69708125" y1="43.95343125" x2="329.90281875" y2="43.861990625" width="0.025" layer="94"/>
<wire x1="329.90281875" y1="43.861990625" x2="330.04111875" y2="43.79271875" width="0.025" layer="94"/>
<wire x1="330.04111875" y1="43.79271875" x2="330.14285" y2="43.736259375" width="0.025" layer="94"/>
<wire x1="330.14285" y1="43.736259375" x2="330.2" y2="43.70196875" width="0.025" layer="94"/>
<wire x1="330.2" y1="43.70196875" x2="330.337159375" y2="43.61053125" width="0.025" layer="94"/>
<wire x1="330.337159375" y1="43.61053125" x2="330.38288125" y2="43.576240625" width="0.025" layer="94"/>
<wire x1="330.38288125" y1="43.576240625" x2="330.55433125" y2="43.43908125" width="0.025" layer="94"/>
<wire x1="330.55433125" y1="43.43908125" x2="330.601190625" y2="43.39221875" width="0.025" layer="94"/>
<wire x1="330.601190625" y1="43.39221875" x2="330.644140625" y2="43.34193125" width="0.025" layer="94"/>
<wire x1="330.644140625" y1="43.34193125" x2="330.70291875" y2="43.279059375" width="0.025" layer="94"/>
<wire x1="330.70291875" y1="43.279059375" x2="330.7715" y2="43.18761875" width="0.025" layer="94"/>
<wire x1="330.7715" y1="43.18761875" x2="330.81721875" y2="43.119040625" width="0.025" layer="94"/>
<wire x1="330.81721875" y1="43.119040625" x2="330.862940625" y2="43.0276" width="0.025" layer="94"/>
<wire x1="330.862940625" y1="43.0276" x2="330.8858" y2="42.95901875" width="0.025" layer="94"/>
<wire x1="330.8858" y1="42.95901875" x2="330.908659375" y2="42.879009375" width="0.025" layer="94"/>
<wire x1="330.908659375" y1="42.879009375" x2="330.920090625" y2="42.81043125" width="0.025" layer="94"/>
<wire x1="330.920090625" y1="42.81043125" x2="330.93151875" y2="42.69613125" width="0.025" layer="94"/>
<wire x1="330.93151875" y1="42.69613125" x2="330.93151875" y2="42.63898125" width="0.025" layer="94"/>
<wire x1="330.93151875" y1="42.63898125" x2="330.920090625" y2="42.536109375" width="0.025" layer="94"/>
<wire x1="330.920090625" y1="42.536109375" x2="330.908659375" y2="42.478959375" width="0.025" layer="94"/>
<wire x1="330.908659375" y1="42.478959375" x2="330.8858" y2="42.38751875" width="0.025" layer="94"/>
<wire x1="330.8858" y1="42.38751875" x2="330.87436875" y2="42.35323125" width="0.025" layer="94"/>
<wire x1="330.87436875" y1="42.35323125" x2="330.82865" y2="42.23893125" width="0.025" layer="94"/>
<wire x1="330.82865" y1="42.23893125" x2="330.8051" y2="42.19206875" width="0.025" layer="94"/>
<wire x1="330.8051" y1="42.19206875" x2="330.770640625" y2="42.13491875" width="0.025" layer="94"/>
<wire x1="330.770640625" y1="42.13491875" x2="330.70291875" y2="42.04461875" width="0.025" layer="94"/>
<wire x1="330.70291875" y1="42.04461875" x2="330.6572" y2="41.98746875" width="0.025" layer="94"/>
<wire x1="330.6572" y1="41.98746875" x2="330.577190625" y2="41.907459375" width="0.025" layer="94"/>
<wire x1="330.577190625" y1="41.907459375" x2="330.49718125" y2="41.83888125" width="0.025" layer="94"/>
<wire x1="330.49718125" y1="41.83888125" x2="330.44003125" y2="41.793159375" width="0.025" layer="94"/>
<wire x1="330.44003125" y1="41.793159375" x2="330.394309375" y2="41.75886875" width="0.025" layer="94"/>
<wire x1="330.394309375" y1="41.75886875" x2="330.337159375" y2="41.71315" width="0.025" layer="94"/>
<wire x1="330.337159375" y1="41.71315" x2="330.291440625" y2="41.678859375" width="0.025" layer="94"/>
<wire x1="330.291440625" y1="41.678859375" x2="330.18856875" y2="41.61028125" width="0.025" layer="94"/>
<wire x1="330.18856875" y1="41.61028125" x2="330.14285" y2="41.575990625" width="0.025" layer="94"/>
<wire x1="330.14285" y1="41.575990625" x2="330.0857" y2="41.5417" width="0.025" layer="94"/>
<wire x1="330.0857" y1="41.5417" x2="329.834240625" y2="41.41596875" width="0.025" layer="94"/>
<wire x1="329.834240625" y1="41.41596875" x2="329.719940625" y2="41.37025" width="0.025" layer="94"/>
<wire x1="329.719940625" y1="41.37025" x2="329.594209375" y2="41.32453125" width="0.025" layer="94"/>
<wire x1="329.594209375" y1="41.32453125" x2="329.44333125" y2="41.278109375" width="0.025" layer="94"/>
<wire x1="329.44333125" y1="41.278109375" x2="329.29703125" y2="41.233090625" width="0.025" layer="94"/>
<wire x1="329.29703125" y1="41.233090625" x2="329.137009375" y2="41.18736875" width="0.025" layer="94"/>
<wire x1="329.137009375" y1="41.18736875" x2="329.1713" y2="41.03878125" width="0.025" layer="94"/>
<wire x1="329.1713" y1="41.03878125" x2="329.26616875" y2="41.059640625" width="0.025" layer="94"/>
<wire x1="329.26616875" y1="41.059640625" x2="329.50163125" y2="41.1185" width="0.025" layer="94"/>
<wire x1="329.50163125" y1="41.1185" x2="329.6045" y2="41.147859375" width="0.025" layer="94"/>
<wire x1="329.6045" y1="41.147859375" x2="329.743940625" y2="41.18775" width="0.025" layer="94"/>
<wire x1="329.743940625" y1="41.18775" x2="329.948540625" y2="41.25595" width="0.025" layer="94"/>
<wire x1="329.948540625" y1="41.25595" x2="330.03998125" y2="41.290240625" width="0.025" layer="94"/>
<wire x1="330.03998125" y1="41.290240625" x2="330.21143125" y2="41.35881875" width="0.025" layer="94"/>
<wire x1="330.21143125" y1="41.35881875" x2="330.48575" y2="41.49598125" width="0.025" layer="94"/>
<wire x1="330.48575" y1="41.49598125" x2="330.60005" y2="41.564559375" width="0.025" layer="94"/>
<wire x1="330.60005" y1="41.564559375" x2="330.69035" y2="41.62101875" width="0.025" layer="94"/>
<wire x1="330.69035" y1="41.62101875" x2="330.737209375" y2="41.656" width="0.025" layer="94"/>
<wire x1="330.737209375" y1="41.656" x2="330.805790625" y2="41.70171875" width="0.025" layer="94"/>
<wire x1="330.805790625" y1="41.70171875" x2="330.851509375" y2="41.736009375" width="0.025" layer="94"/>
<wire x1="330.851509375" y1="41.736009375" x2="331.080109375" y2="41.918890625" width="0.025" layer="94"/>
<wire x1="331.080109375" y1="41.918890625" x2="331.2287" y2="42.06748125" width="0.025" layer="94"/>
<wire x1="331.2287" y1="42.06748125" x2="331.308709375" y2="42.17035" width="0.025" layer="94"/>
<wire x1="331.308709375" y1="42.17035" x2="331.33156875" y2="42.204640625" width="0.025" layer="94"/>
<wire x1="331.33156875" y1="42.204640625" x2="331.365859375" y2="42.261790625" width="0.025" layer="94"/>
<wire x1="331.365859375" y1="42.261790625" x2="331.434440625" y2="42.39895" width="0.025" layer="94"/>
<wire x1="331.434440625" y1="42.39895" x2="331.4573" y2="42.4561" width="0.025" layer="94"/>
<wire x1="331.4573" y1="42.4561" x2="331.46873125" y2="42.490390625" width="0.025" layer="94"/>
<wire x1="331.46873125" y1="42.490390625" x2="331.491590625" y2="42.58183125" width="0.025" layer="94"/>
<wire x1="331.491590625" y1="42.58183125" x2="331.50301875" y2="42.650409375" width="0.025" layer="94"/>
<wire x1="331.50301875" y1="42.650409375" x2="331.51445" y2="42.764709375" width="0.025" layer="94"/>
<wire x1="331.51445" y1="42.764709375" x2="331.51445" y2="42.84471875" width="0.025" layer="94"/>
<wire x1="331.51445" y1="42.84471875" x2="331.50988125" y2="42.89615" width="0.025" layer="94"/>
<wire x1="331.50988125" y1="42.89615" x2="331.50301875" y2="42.97045" width="0.025" layer="94"/>
<wire x1="331.50301875" y1="42.97045" x2="331.480159375" y2="43.08475" width="0.025" layer="94"/>
<wire x1="331.480159375" y1="43.08475" x2="331.434440625" y2="43.221909375" width="0.025" layer="94"/>
<wire x1="331.434440625" y1="43.221909375" x2="331.41158125" y2="43.279059375" width="0.025" layer="94"/>
<wire x1="331.41158125" y1="43.279059375" x2="331.36643125" y2="43.369359375" width="0.025" layer="94"/>
<wire x1="331.36643125" y1="43.369359375" x2="331.27518125" y2="43.50651875" width="0.025" layer="94"/>
<wire x1="331.27518125" y1="43.50651875" x2="331.24013125" y2="43.55338125" width="0.025" layer="94"/>
<wire x1="331.24013125" y1="43.55338125" x2="331.194409375" y2="43.61053125" width="0.025" layer="94"/>
<wire x1="331.194409375" y1="43.61053125" x2="330.98866875" y2="43.81626875" width="0.025" layer="94"/>
<wire x1="330.98866875" y1="43.81626875" x2="330.920090625" y2="43.87341875" width="0.025" layer="94"/>
<wire x1="330.920090625" y1="43.87341875" x2="330.862940625" y2="43.919140625" width="0.025" layer="94"/>
<wire x1="330.862940625" y1="43.919140625" x2="330.7715" y2="43.98771875" width="0.025" layer="94"/>
<wire x1="330.7715" y1="43.98771875" x2="330.66863125" y2="44.0563" width="0.025" layer="94"/>
<wire x1="330.66863125" y1="44.0563" x2="330.610340625" y2="44.09135" width="0.025" layer="94"/>
<wire x1="330.610340625" y1="44.09135" x2="330.577190625" y2="44.11345" width="0.025" layer="94"/>
<wire x1="330.577190625" y1="44.11345" x2="330.520040625" y2="44.147740625" width="0.025" layer="94"/>
<wire x1="330.520040625" y1="44.147740625" x2="330.24571875" y2="44.2849" width="0.025" layer="94"/>
<wire x1="330.24571875" y1="44.2849" x2="329.95768125" y2="44.399959375" width="0.025" layer="94"/>
<wire x1="329.95768125" y1="44.399959375" x2="329.651359375" y2="44.50206875" width="0.025" layer="94"/>
<wire x1="329.651359375" y1="44.50206875" x2="329.4902" y2="44.54806875" width="0.025" layer="94"/>
<wire x1="329.4902" y1="44.54806875" x2="329.21701875" y2="44.61636875" width="0.025" layer="94"/>
<wire x1="329.21701875" y1="44.61636875" x2="328.87411875" y2="44.68495" width="0.025" layer="94"/>
<wire x1="328.87411875" y1="44.68495" x2="328.736959375" y2="44.707809375" width="0.025" layer="94"/>
<wire x1="328.736959375" y1="44.707809375" x2="328.65695" y2="44.719240625" width="0.025" layer="94"/>
<wire x1="328.65695" y1="44.719240625" x2="328.47406875" y2="44.7421" width="0.025" layer="94"/>
<wire x1="328.47406875" y1="44.7421" x2="328.370059375" y2="44.75353125" width="0.025" layer="94"/>
<wire x1="328.370059375" y1="44.75353125" x2="328.222609375" y2="44.764959375" width="0.025" layer="94"/>
<wire x1="328.222609375" y1="44.764959375" x2="328.222609375" y2="45.03928125" width="0.025" layer="94"/>
<wire x1="328.222609375" y1="45.03928125" x2="328.279759375" y2="45.03928125" width="0.025" layer="94"/>
<wire x1="328.279759375" y1="45.03928125" x2="328.42835" y2="45.02785" width="0.025" layer="94"/>
<wire x1="328.42835" y1="45.02785" x2="328.55408125" y2="45.01641875" width="0.025" layer="94"/>
<wire x1="328.55408125" y1="45.01641875" x2="328.65695" y2="45.004990625" width="0.025" layer="94"/>
<wire x1="328.65695" y1="45.004990625" x2="328.736959375" y2="44.994990625" width="0.025" layer="94"/>
<wire x1="328.736959375" y1="44.994990625" x2="328.79983125" y2="44.98713125" width="0.025" layer="94"/>
<wire x1="328.79983125" y1="44.98713125" x2="328.93126875" y2="44.9707" width="0.025" layer="94"/>
<wire x1="328.93126875" y1="44.9707" x2="329.137009375" y2="44.936409375" width="0.025" layer="94"/>
<wire x1="329.137009375" y1="44.936409375" x2="329.251309375" y2="44.91355" width="0.025" layer="94"/>
<wire x1="329.251309375" y1="44.91355" x2="329.29703125" y2="44.90211875" width="0.025" layer="94"/>
<wire x1="329.29703125" y1="44.90211875" x2="329.3999" y2="44.879259375" width="0.025" layer="94"/>
<wire x1="329.3999" y1="44.879259375" x2="329.45705" y2="44.86783125" width="0.025" layer="94"/>
<wire x1="329.45705" y1="44.86783125" x2="329.63993125" y2="44.822109375" width="0.025" layer="94"/>
<wire x1="329.63993125" y1="44.822109375" x2="329.719940625" y2="44.79925" width="0.025" layer="94"/>
<wire x1="329.719940625" y1="44.79925" x2="329.81138125" y2="44.776390625" width="0.025" layer="94"/>
<wire x1="329.81138125" y1="44.776390625" x2="329.891390625" y2="44.75353125" width="0.025" layer="94"/>
<wire x1="329.891390625" y1="44.75353125" x2="330.165709375" y2="44.662090625" width="0.025" layer="94"/>
<wire x1="330.165709375" y1="44.662090625" x2="330.25715" y2="44.6278" width="0.025" layer="94"/>
<wire x1="330.25715" y1="44.6278" x2="330.3143" y2="44.604940625" width="0.025" layer="94"/>
<wire x1="330.3143" y1="44.604940625" x2="330.394309375" y2="44.57065" width="0.025" layer="94"/>
<wire x1="330.394309375" y1="44.57065" x2="330.508609375" y2="44.52493125" width="0.025" layer="94"/>
<wire x1="330.508609375" y1="44.52493125" x2="330.87436875" y2="44.34205" width="0.025" layer="94"/>
<wire x1="330.87436875" y1="44.34205" x2="331.04581875" y2="44.23918125" width="0.025" layer="94"/>
<wire x1="331.04581875" y1="44.23918125" x2="331.080109375" y2="44.21631875" width="0.025" layer="94"/>
<wire x1="331.080109375" y1="44.21631875" x2="331.137259375" y2="44.1706" width="0.025" layer="94"/>
<wire x1="331.137259375" y1="44.1706" x2="331.36471875" y2="44.000009375" width="0.025" layer="94"/>
<wire x1="331.36471875" y1="44.000009375" x2="331.451590625" y2="43.91341875" width="0.025" layer="94"/>
<wire x1="331.451590625" y1="43.91341875" x2="331.62875" y2="43.736259375" width="0.025" layer="94"/>
<wire x1="331.62875" y1="43.736259375" x2="331.67446875" y2="43.679109375" width="0.025" layer="94"/>
<wire x1="331.67446875" y1="43.679109375" x2="331.74305" y2="43.58766875" width="0.025" layer="94"/>
<wire x1="331.74305" y1="43.58766875" x2="331.777340625" y2="43.53051875" width="0.025" layer="94"/>
<wire x1="331.777340625" y1="43.53051875" x2="331.823059375" y2="43.450509375" width="0.025" layer="94"/>
<wire x1="331.823059375" y1="43.450509375" x2="331.85735" y2="43.38193125" width="0.025" layer="94"/>
<wire x1="331.85735" y1="43.38193125" x2="331.880209375" y2="43.32478125" width="0.025" layer="94"/>
<wire x1="331.880209375" y1="43.32478125" x2="331.90306875" y2="43.2562" width="0.025" layer="94"/>
<wire x1="331.90306875" y1="43.2562" x2="331.92593125" y2="43.164759375" width="0.025" layer="94"/>
<wire x1="331.92593125" y1="43.164759375" x2="331.937359375" y2="43.095040625" width="0.025" layer="94"/>
<wire x1="331.937359375" y1="43.095040625" x2="331.948790625" y2="42.993309375" width="0.025" layer="94"/>
<wire x1="331.948790625" y1="42.993309375" x2="331.948790625" y2="42.84471875" width="0.025" layer="94"/>
<wire x1="331.948790625" y1="42.84471875" x2="331.937359375" y2="42.75328125" width="0.025" layer="94"/>
<wire x1="331.937359375" y1="42.75328125" x2="331.92593125" y2="42.69613125" width="0.025" layer="94"/>
<wire x1="331.92593125" y1="42.69613125" x2="331.90306875" y2="42.604690625" width="0.025" layer="94"/>
<wire x1="331.90306875" y1="42.604690625" x2="331.886609375" y2="42.55783125" width="0.025" layer="94"/>
<wire x1="331.886609375" y1="42.55783125" x2="331.84591875" y2="42.4561" width="0.025" layer="94"/>
<wire x1="331.84591875" y1="42.4561" x2="331.777340625" y2="42.318940625" width="0.025" layer="94"/>
<wire x1="331.777340625" y1="42.318940625" x2="331.708759375" y2="42.21606875" width="0.025" layer="94"/>
<wire x1="331.708759375" y1="42.21606875" x2="331.67446875" y2="42.17035" width="0.025" layer="94"/>
<wire x1="331.67446875" y1="42.17035" x2="331.56016875" y2="42.033190625" width="0.025" layer="94"/>
<wire x1="331.56016875" y1="42.033190625" x2="331.40015" y2="41.87316875" width="0.025" layer="94"/>
<wire x1="331.40015" y1="41.87316875" x2="331.262990625" y2="41.75886875" width="0.025" layer="94"/>
<wire x1="331.262990625" y1="41.75886875" x2="331.17155" y2="41.690290625" width="0.025" layer="94"/>
<wire x1="331.17155" y1="41.690290625" x2="331.034390625" y2="41.59885" width="0.025" layer="94"/>
<wire x1="331.034390625" y1="41.59885" x2="330.977240625" y2="41.564559375" width="0.025" layer="94"/>
<wire x1="330.977240625" y1="41.564559375" x2="330.89723125" y2="41.518840625" width="0.025" layer="94"/>
<wire x1="330.89723125" y1="41.518840625" x2="330.84008125" y2="41.48455" width="0.025" layer="94"/>
<wire x1="330.84008125" y1="41.48455" x2="330.5429" y2="41.335959375" width="0.025" layer="94"/>
<wire x1="330.5429" y1="41.335959375" x2="330.462890625" y2="41.30166875" width="0.025" layer="94"/>
<wire x1="330.462890625" y1="41.30166875" x2="330.348590625" y2="41.25595" width="0.025" layer="94"/>
<wire x1="330.348590625" y1="41.25595" x2="330.25715" y2="41.221659375" width="0.025" layer="94"/>
<wire x1="330.25715" y1="41.221659375" x2="330.13141875" y2="41.175940625" width="0.025" layer="94"/>
<wire x1="330.13141875" y1="41.175940625" x2="329.994259375" y2="41.13021875" width="0.025" layer="94"/>
<wire x1="329.994259375" y1="41.13021875" x2="329.879959375" y2="41.09593125" width="0.025" layer="94"/>
<wire x1="329.879959375" y1="41.09593125" x2="329.75423125" y2="41.061640625" width="0.025" layer="94"/>
<wire x1="329.75423125" y1="41.061640625" x2="329.67421875" y2="41.03878125" width="0.025" layer="94"/>
<wire x1="329.67421875" y1="41.03878125" x2="329.52563125" y2="41.004490625" width="0.025" layer="94"/>
<wire x1="329.52563125" y1="41.004490625" x2="329.319890625" y2="40.95876875" width="0.025" layer="94"/>
<wire x1="329.319890625" y1="40.95876875" x2="329.194159375" y2="40.935909375" width="0.025" layer="94"/>
<wire x1="328.35108125" y1="37.03828125" x2="327.58253125" y2="40.78731875" width="0.025" layer="94"/>
<wire x1="327.58253125" y1="40.78731875" x2="326.86038125" y2="37.084" width="0.025" layer="94"/>
<wire x1="326.86038125" y1="37.084" x2="326.805290625" y2="37.01541875" width="0.025" layer="94"/>
<wire x1="271.78" y1="236.22" x2="271.78" y2="231.14" width="0.025" layer="94"/>
<wire x1="271.78" y1="231.14" x2="271.78" y2="226.06" width="0.025" layer="94"/>
<wire x1="271.78" y1="226.06" x2="279.4" y2="226.06" width="0.025" layer="94"/>
<wire x1="279.4" y1="226.06" x2="289.56" y2="226.06" width="0.025" layer="94"/>
<wire x1="289.56" y1="226.06" x2="299.72" y2="226.06" width="0.025" layer="94"/>
<wire x1="299.72" y1="226.06" x2="309.88" y2="226.06" width="0.025" layer="94"/>
<wire x1="309.88" y1="226.06" x2="375.92" y2="226.06" width="0.025" layer="94"/>
<wire x1="271.78" y1="231.14" x2="375.92" y2="231.14" width="0.025" layer="94"/>
<wire x1="279.4" y1="236.22" x2="279.4" y2="226.06" width="0.025" layer="94"/>
<wire x1="289.56" y1="236.22" x2="289.56" y2="226.06" width="0.025" layer="94"/>
<wire x1="299.72" y1="236.22" x2="299.72" y2="226.06" width="0.025" layer="94"/>
<wire x1="309.88" y1="236.22" x2="309.88" y2="226.06" width="0.025" layer="94"/>
<text x="272.796" y="35.306" size="1.778" layer="94" font="vector" ratio="12">DRAWN
BY</text>
<text x="272.542" y="28.702" size="1.778" layer="94" font="vector" ratio="12">CHECKED
BY</text>
<text x="272.542" y="22.352" size="1.778" layer="94" font="vector" ratio="12">DESIGN
APPROVAL</text>
<text x="287.02" y="42.164" size="1.778" layer="94" font="vector" ratio="12">INIT</text>
<text x="294.386" y="42.164" size="1.778" layer="94" font="vector" ratio="12">DATE</text>
<text x="333.502" y="33.528" size="1.6764" layer="94" font="vector" ratio="12">JOHN MEZZALINGUA ASSOCIATES
 7645 HENRY CLAY BOULEVARD
  LIVERPOOL, NEW YORK 13088
       TEL. 315-431-7100</text>
<text x="307.34" y="22.86" size="1.27" layer="94" font="vector" ratio="12">THIS DRAWING AND SPECIFICATIONS ARE THE PROPERTY OF
JOHN MEZZALINGUA ASSOCIATES AND SHALL NOT BE 
REPRODICED, COPIED, OR USED AS A BASIS FOR MANUFACTURING OR
SALE OF EQUIPMENT OR DEVICES WITHOUT WRITTEN PERMISSION.</text>
<text x="305.562" y="19.812" size="1.27" layer="94" font="vector" ratio="12">TITLE: </text>
<text x="305.562" y="13.462" size="1.27" layer="94" font="vector" ratio="12">DWG. NO.</text>
<text x="370.332" y="13.462" size="1.27" layer="94" font="vector" ratio="12">REV.</text>
<text x="360.172" y="7.112" size="1.27" layer="94" font="vector" ratio="12">SIZE:   B</text>
<text x="323.342" y="7.112" size="1.27" layer="94" font="vector" ratio="12">SHEET</text>
<text x="305.562" y="7.112" size="1.27" layer="94" font="vector" ratio="12">SCALE: NONE</text>
<text x="317.5" y="10.16" size="1.778" layer="94" font="vector">&gt;DRAWING_NAME</text>
<text x="332.486" y="5.842" size="1.27" layer="94" font="vector">&gt;SHEET</text>
<text x="273.304" y="233.68" size="1.778" layer="94" font="vector" ratio="12">REV</text>
<text x="281.178" y="233.68" size="1.778" layer="94" font="vector" ratio="12">ZONE</text>
<text x="291.592" y="233.68" size="1.778" layer="94" font="vector" ratio="12">DATE</text>
<text x="302.26" y="233.68" size="1.778" layer="94" font="vector" ratio="12">ECO</text>
<text x="333.502" y="233.426" size="1.778" layer="94" font="vector" ratio="12">DESCRIPTION</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="A3_LOC_JMA_RELEASE">
<gates>
<gate name="G$1" symbol="A3_LOC_JMA_RELEASE" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="JMA_LBR_v2" urn="urn:adsk.eagle:library:8413580">
<packages>
<package name="TAG-CONNECT_TC2050-IDC-NL" urn="urn:adsk.eagle:footprint:16170704/2" library_version="47">
<wire x1="-2.7305" y1="3.683" x2="4.0005" y2="3.683" width="0.127" layer="21"/>
<wire x1="4.0005" y1="3.683" x2="4.0005" y2="-8.763" width="0.127" layer="21"/>
<wire x1="4.0005" y1="-8.763" x2="-2.7305" y2="-8.763" width="0.127" layer="21"/>
<wire x1="-2.7305" y1="-8.763" x2="-2.7305" y2="3.683" width="0.127" layer="21"/>
<wire x1="-2.7305" y1="3.683" x2="4.0005" y2="3.683" width="0.127" layer="51"/>
<wire x1="4.0005" y1="3.683" x2="4.0005" y2="-8.763" width="0.127" layer="51"/>
<wire x1="4.0005" y1="-8.763" x2="-2.7305" y2="-8.763" width="0.127" layer="51"/>
<wire x1="-2.7305" y1="-8.763" x2="-2.7305" y2="3.683" width="0.127" layer="51"/>
<wire x1="-2.9805" y1="3.933" x2="4.2505" y2="3.933" width="0.05" layer="39"/>
<wire x1="4.2505" y1="3.933" x2="4.2505" y2="-9.013" width="0.05" layer="39"/>
<wire x1="4.2505" y1="-9.013" x2="-2.9805" y2="-9.013" width="0.05" layer="39"/>
<wire x1="-2.9805" y1="-9.013" x2="-2.9805" y2="3.933" width="0.05" layer="39"/>
<circle x="-3.365" y="0" radius="0.1" width="0.2" layer="21"/>
<circle x="-3.365" y="0" radius="0.1" width="0.2" layer="51"/>
<text x="-3.365" y="4.46" size="1.778" layer="25">&gt;NAME</text>
<text x="-3.365" y="-11.54" size="1.778" layer="27">&gt;VALUE</text>
<rectangle x1="0" y1="-5.08" x2="1.27" y2="0" layer="42"/>
<rectangle x1="0" y1="-5.08" x2="1.27" y2="0" layer="43"/>
<rectangle x1="0.508" y1="-5.08" x2="0.762" y2="0" layer="41"/>
<rectangle x1="0" y1="-0.762" x2="1.27" y2="-0.508" layer="41"/>
<rectangle x1="0" y1="-2.032" x2="1.27" y2="-1.778" layer="41"/>
<rectangle x1="0" y1="-3.302" x2="1.27" y2="-3.048" layer="41"/>
<rectangle x1="0" y1="-4.572" x2="1.27" y2="-4.318" layer="41"/>
<smd name="1" x="0" y="0" dx="0.787" dy="0.787" layer="1" roundness="100" rot="R270" cream="no"/>
<smd name="2" x="0" y="-1.27" dx="0.787" dy="0.787" layer="1" roundness="100" rot="R270" cream="no"/>
<smd name="3" x="0" y="-2.54" dx="0.787" dy="0.787" layer="1" roundness="100" rot="R270" cream="no"/>
<smd name="4" x="0" y="-3.81" dx="0.787" dy="0.787" layer="1" roundness="100" rot="R270" cream="no"/>
<smd name="5" x="0" y="-5.08" dx="0.787" dy="0.787" layer="1" roundness="100" rot="R270" cream="no"/>
<smd name="6" x="1.27" y="-5.08" dx="0.787" dy="0.787" layer="1" roundness="100" rot="R90" cream="no"/>
<smd name="7" x="1.27" y="-3.81" dx="0.787" dy="0.787" layer="1" roundness="100" rot="R90" cream="no"/>
<smd name="8" x="1.27" y="-2.54" dx="0.787" dy="0.787" layer="1" roundness="100" rot="R90" cream="no"/>
<smd name="9" x="1.27" y="-1.27" dx="0.787" dy="0.787" layer="1" roundness="100" rot="R90" cream="no"/>
<smd name="10" x="1.27" y="0" dx="0.787" dy="0.787" layer="1" roundness="100" rot="R90" cream="no"/>
<hole x="1.65" y="-6.35" drill="0.991"/>
<hole x="-0.38" y="-6.35" drill="0.991"/>
<hole x="0.635" y="1.27" drill="0.991"/>
</package>
<package name="CIR_PAD_1MM" urn="urn:adsk.eagle:footprint:8413703/1" library_version="40" library_locally_modified="yes">
<text x="-1.905" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<smd name="P$1" x="0" y="0" dx="1" dy="1" layer="1" roundness="100"/>
</package>
<package name="TP_1.5MM" urn="urn:adsk.eagle:footprint:8413705/1" library_version="40" library_locally_modified="yes">
<pad name="1" x="0" y="0" drill="1" diameter="1.5" rot="R180"/>
<text x="-1.905" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<rectangle x1="-0.35" y1="-0.35" x2="0.35" y2="0.35" layer="51"/>
</package>
<package name="TP_1.5MM_PAD" urn="urn:adsk.eagle:footprint:8413706/1" library_version="40" library_locally_modified="yes">
<pad name="1" x="0" y="0" drill="1" diameter="1.5" rot="R180"/>
<text x="-1.27" y="4.445" size="1.27" layer="25">&gt;NAME</text>
<rectangle x1="-0.35" y1="-0.35" x2="0.35" y2="0.35" layer="51"/>
<rectangle x1="-2" y1="-2.5" x2="8" y2="2.5" layer="1"/>
</package>
<package name="TP_1.7MM" urn="urn:adsk.eagle:footprint:8413707/1" library_version="40" library_locally_modified="yes">
<pad name="1" x="0" y="0" drill="1" diameter="1.6764" rot="R180"/>
<text x="-1.905" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<rectangle x1="-0.35" y1="-0.35" x2="0.35" y2="0.35" layer="51"/>
</package>
<package name="C120H40" urn="urn:adsk.eagle:footprint:8413699/1" library_version="40" library_locally_modified="yes">
<pad name="1" x="0" y="0" drill="0.6" diameter="1.2" thermals="no"/>
<text x="0.9" y="-0.1" size="0.254" layer="25">&gt;NAME</text>
</package>
<package name="C131H51" urn="urn:adsk.eagle:footprint:8413700/1" library_version="40" library_locally_modified="yes">
<pad name="1" x="0" y="0" drill="0.71" diameter="1.31" thermals="no"/>
<text x="1.2" y="0.1" size="0.254" layer="25">&gt;NAME</text>
<text x="1.2" y="-0.2" size="0.254" layer="21">&gt;LABEL</text>
</package>
<package name="C406H326" urn="urn:adsk.eagle:footprint:8413701/1" library_version="40" library_locally_modified="yes">
<pad name="1" x="0" y="0" drill="3.26" diameter="4.06" thermals="no"/>
<text x="2.2" y="1.1" size="0.254" layer="25">&gt;NAME</text>
<text x="2.2" y="0.8" size="0.254" layer="21">&gt;LABEL</text>
</package>
<package name="C491H411" urn="urn:adsk.eagle:footprint:8413702/1" library_version="40" library_locally_modified="yes">
<pad name="1" x="0" y="0" drill="4.31" diameter="4.91" thermals="no"/>
<text x="3.2" y="1.1" size="0.254" layer="25">&gt;NAME</text>
<text x="3.2" y="0.8" size="0.254" layer="21">&gt;LABEL</text>
</package>
<package name="CIR_PAD_2MM" urn="urn:adsk.eagle:footprint:8413704/1" library_version="40" library_locally_modified="yes">
<text x="-1.905" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<smd name="P$1" x="0" y="0" dx="2" dy="2" layer="1" roundness="100"/>
</package>
<package name="RESC1005X40" urn="urn:adsk.eagle:footprint:3811164/1" library_version="47">
<description>CHIP, 1 X 0.5 X 0.4 mm body
&lt;p&gt;CHIP package with body size 1 X 0.5 X 0.4 mm&lt;/p&gt;</description>
<wire x1="0.525" y1="0.614" x2="-0.525" y2="0.614" width="0.12" layer="21"/>
<wire x1="0.525" y1="-0.614" x2="-0.525" y2="-0.614" width="0.12" layer="21"/>
<wire x1="0.525" y1="-0.275" x2="-0.525" y2="-0.275" width="0.12" layer="51"/>
<wire x1="-0.525" y1="-0.275" x2="-0.525" y2="0.275" width="0.12" layer="51"/>
<wire x1="-0.525" y1="0.275" x2="0.525" y2="0.275" width="0.12" layer="51"/>
<wire x1="0.525" y1="0.275" x2="0.525" y2="-0.275" width="0.12" layer="51"/>
<smd name="1" x="-0.475" y="0" dx="0.55" dy="0.6" layer="1"/>
<smd name="2" x="0.475" y="0" dx="0.55" dy="0.6" layer="1"/>
<text x="0" y="1.249" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.249" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="INDM4040X300" urn="urn:adsk.eagle:footprint:3890468/1" library_version="47">
<description>MOLDED BODY, 4 X 4 X 3 mm body
&lt;p&gt;MOLDED BODY package with body size 4 X 4 X 3 mm&lt;/p&gt;</description>
<wire x1="-2" y1="2.1699" x2="2" y2="2.1699" width="0.12" layer="21"/>
<wire x1="-2" y1="-2.1699" x2="2" y2="-2.1699" width="0.12" layer="21"/>
<wire x1="2" y1="-2" x2="-2" y2="-2" width="0.12" layer="51"/>
<wire x1="-2" y1="-2" x2="-2" y2="2" width="0.12" layer="51"/>
<wire x1="-2" y1="2" x2="2" y2="2" width="0.12" layer="51"/>
<wire x1="2" y1="2" x2="2" y2="-2" width="0.12" layer="51"/>
<smd name="1" x="-1.5795" y="0" dx="1.9528" dy="3.7118" layer="1"/>
<smd name="2" x="1.5795" y="0" dx="1.9528" dy="3.7118" layer="1"/>
<text x="0" y="2.8049" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.8049" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="RADIAL_THT" urn="urn:adsk.eagle:footprint:8413685/1" library_version="40" library_locally_modified="yes">
<pad name="1" x="1.27" y="0" drill="0.6" diameter="1" rot="R180"/>
<pad name="2" x="-1.27" y="0" drill="0.6" diameter="1" rot="R180"/>
<text x="-3.175" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="4.445" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<circle x="0" y="0" radius="2.60108125" width="0.127" layer="21"/>
</package>
<package name="QFP50P1600X1600X120-100" urn="urn:adsk.eagle:footprint:3791946/1" library_version="47">
<description>100-QFP, 0.5 mm pitch, 16 mm span, 14 X 14 X 1.2 mm body
&lt;p&gt;100-pin QFP package with 0.5 mm pitch, 16 mm lead span1 X 16 mm lead span2 with body size 14 X 14 X 1.2 mm&lt;/p&gt;</description>
<circle x="-7.7788" y="6.644" radius="0.25" width="0" layer="21"/>
<wire x1="-7.1" y1="6.394" x2="-7.1" y2="7.1" width="0.12" layer="21"/>
<wire x1="-7.1" y1="7.1" x2="-6.394" y2="7.1" width="0.12" layer="21"/>
<wire x1="7.1" y1="6.394" x2="7.1" y2="7.1" width="0.12" layer="21"/>
<wire x1="7.1" y1="7.1" x2="6.394" y2="7.1" width="0.12" layer="21"/>
<wire x1="7.1" y1="-6.394" x2="7.1" y2="-7.1" width="0.12" layer="21"/>
<wire x1="7.1" y1="-7.1" x2="6.394" y2="-7.1" width="0.12" layer="21"/>
<wire x1="-7.1" y1="-6.394" x2="-7.1" y2="-7.1" width="0.12" layer="21"/>
<wire x1="-7.1" y1="-7.1" x2="-6.394" y2="-7.1" width="0.12" layer="21"/>
<wire x1="7.1" y1="-7.1" x2="-7.1" y2="-7.1" width="0.12" layer="51"/>
<wire x1="-7.1" y1="-7.1" x2="-7.1" y2="7.1" width="0.12" layer="51"/>
<wire x1="-7.1" y1="7.1" x2="7.1" y2="7.1" width="0.12" layer="51"/>
<wire x1="7.1" y1="7.1" x2="7.1" y2="-7.1" width="0.12" layer="51"/>
<smd name="1" x="-7.6783" y="6" dx="1.5588" dy="0.28" layer="1"/>
<smd name="2" x="-7.6783" y="5.5" dx="1.5588" dy="0.28" layer="1"/>
<smd name="3" x="-7.6783" y="5" dx="1.5588" dy="0.28" layer="1"/>
<smd name="4" x="-7.6783" y="4.5" dx="1.5588" dy="0.28" layer="1"/>
<smd name="5" x="-7.6783" y="4" dx="1.5588" dy="0.28" layer="1"/>
<smd name="6" x="-7.6783" y="3.5" dx="1.5588" dy="0.28" layer="1"/>
<smd name="7" x="-7.6783" y="3" dx="1.5588" dy="0.28" layer="1"/>
<smd name="8" x="-7.6783" y="2.5" dx="1.5588" dy="0.28" layer="1"/>
<smd name="9" x="-7.6783" y="2" dx="1.5588" dy="0.28" layer="1"/>
<smd name="10" x="-7.6783" y="1.5" dx="1.5588" dy="0.28" layer="1"/>
<smd name="11" x="-7.6783" y="1" dx="1.5588" dy="0.28" layer="1"/>
<smd name="12" x="-7.6783" y="0.5" dx="1.5588" dy="0.28" layer="1"/>
<smd name="13" x="-7.6783" y="0" dx="1.5588" dy="0.28" layer="1"/>
<smd name="14" x="-7.6783" y="-0.5" dx="1.5588" dy="0.28" layer="1"/>
<smd name="15" x="-7.6783" y="-1" dx="1.5588" dy="0.28" layer="1"/>
<smd name="16" x="-7.6783" y="-1.5" dx="1.5588" dy="0.28" layer="1"/>
<smd name="17" x="-7.6783" y="-2" dx="1.5588" dy="0.28" layer="1"/>
<smd name="18" x="-7.6783" y="-2.5" dx="1.5588" dy="0.28" layer="1"/>
<smd name="19" x="-7.6783" y="-3" dx="1.5588" dy="0.28" layer="1"/>
<smd name="20" x="-7.6783" y="-3.5" dx="1.5588" dy="0.28" layer="1"/>
<smd name="21" x="-7.6783" y="-4" dx="1.5588" dy="0.28" layer="1"/>
<smd name="22" x="-7.6783" y="-4.5" dx="1.5588" dy="0.28" layer="1"/>
<smd name="23" x="-7.6783" y="-5" dx="1.5588" dy="0.28" layer="1"/>
<smd name="24" x="-7.6783" y="-5.5" dx="1.5588" dy="0.28" layer="1"/>
<smd name="25" x="-7.6783" y="-6" dx="1.5588" dy="0.28" layer="1"/>
<smd name="26" x="-6" y="-7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="27" x="-5.5" y="-7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="28" x="-5" y="-7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="29" x="-4.5" y="-7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="30" x="-4" y="-7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="31" x="-3.5" y="-7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="32" x="-3" y="-7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="33" x="-2.5" y="-7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="34" x="-2" y="-7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="35" x="-1.5" y="-7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="36" x="-1" y="-7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="37" x="-0.5" y="-7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="38" x="0" y="-7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="39" x="0.5" y="-7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="40" x="1" y="-7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="41" x="1.5" y="-7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="42" x="2" y="-7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="43" x="2.5" y="-7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="44" x="3" y="-7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="45" x="3.5" y="-7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="46" x="4" y="-7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="47" x="4.5" y="-7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="48" x="5" y="-7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="49" x="5.5" y="-7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="50" x="6" y="-7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="51" x="7.6783" y="-6" dx="1.5588" dy="0.28" layer="1"/>
<smd name="52" x="7.6783" y="-5.5" dx="1.5588" dy="0.28" layer="1"/>
<smd name="53" x="7.6783" y="-5" dx="1.5588" dy="0.28" layer="1"/>
<smd name="54" x="7.6783" y="-4.5" dx="1.5588" dy="0.28" layer="1"/>
<smd name="55" x="7.6783" y="-4" dx="1.5588" dy="0.28" layer="1"/>
<smd name="56" x="7.6783" y="-3.5" dx="1.5588" dy="0.28" layer="1"/>
<smd name="57" x="7.6783" y="-3" dx="1.5588" dy="0.28" layer="1"/>
<smd name="58" x="7.6783" y="-2.5" dx="1.5588" dy="0.28" layer="1"/>
<smd name="59" x="7.6783" y="-2" dx="1.5588" dy="0.28" layer="1"/>
<smd name="60" x="7.6783" y="-1.5" dx="1.5588" dy="0.28" layer="1"/>
<smd name="61" x="7.6783" y="-1" dx="1.5588" dy="0.28" layer="1"/>
<smd name="62" x="7.6783" y="-0.5" dx="1.5588" dy="0.28" layer="1"/>
<smd name="63" x="7.6783" y="0" dx="1.5588" dy="0.28" layer="1"/>
<smd name="64" x="7.6783" y="0.5" dx="1.5588" dy="0.28" layer="1"/>
<smd name="65" x="7.6783" y="1" dx="1.5588" dy="0.28" layer="1"/>
<smd name="66" x="7.6783" y="1.5" dx="1.5588" dy="0.28" layer="1"/>
<smd name="67" x="7.6783" y="2" dx="1.5588" dy="0.28" layer="1"/>
<smd name="68" x="7.6783" y="2.5" dx="1.5588" dy="0.28" layer="1"/>
<smd name="69" x="7.6783" y="3" dx="1.5588" dy="0.28" layer="1"/>
<smd name="70" x="7.6783" y="3.5" dx="1.5588" dy="0.28" layer="1"/>
<smd name="71" x="7.6783" y="4" dx="1.5588" dy="0.28" layer="1"/>
<smd name="72" x="7.6783" y="4.5" dx="1.5588" dy="0.28" layer="1"/>
<smd name="73" x="7.6783" y="5" dx="1.5588" dy="0.28" layer="1"/>
<smd name="74" x="7.6783" y="5.5" dx="1.5588" dy="0.28" layer="1"/>
<smd name="75" x="7.6783" y="6" dx="1.5588" dy="0.28" layer="1"/>
<smd name="76" x="6" y="7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="77" x="5.5" y="7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="78" x="5" y="7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="79" x="4.5" y="7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="80" x="4" y="7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="81" x="3.5" y="7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="82" x="3" y="7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="83" x="2.5" y="7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="84" x="2" y="7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="85" x="1.5" y="7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="86" x="1" y="7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="87" x="0.5" y="7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="88" x="0" y="7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="89" x="-0.5" y="7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="90" x="-1" y="7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="91" x="-1.5" y="7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="92" x="-2" y="7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="93" x="-2.5" y="7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="94" x="-3" y="7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="95" x="-3.5" y="7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="96" x="-4" y="7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="97" x="-4.5" y="7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="98" x="-5" y="7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="99" x="-5.5" y="7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<smd name="100" x="-6" y="7.6783" dx="1.5588" dy="0.28" layer="1" rot="R90"/>
<text x="0" y="9.0927" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-9.0927" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC1005X60" urn="urn:adsk.eagle:footprint:3793110/1" library_version="47">
<description>CHIP, 1.025 X 0.525 X 0.6 mm body
&lt;p&gt;CHIP package with body size 1.025 X 0.525 X 0.6 mm&lt;/p&gt;</description>
<wire x1="0.55" y1="0.6325" x2="-0.55" y2="0.6325" width="0.12" layer="21"/>
<wire x1="0.55" y1="-0.6325" x2="-0.55" y2="-0.6325" width="0.12" layer="21"/>
<wire x1="0.55" y1="-0.3" x2="-0.55" y2="-0.3" width="0.12" layer="51"/>
<wire x1="-0.55" y1="-0.3" x2="-0.55" y2="0.3" width="0.12" layer="51"/>
<wire x1="-0.55" y1="0.3" x2="0.55" y2="0.3" width="0.12" layer="51"/>
<wire x1="0.55" y1="0.3" x2="0.55" y2="-0.3" width="0.12" layer="51"/>
<smd name="1" x="-0.4875" y="0" dx="0.5621" dy="0.6371" layer="1"/>
<smd name="2" x="0.4875" y="0" dx="0.5621" dy="0.6371" layer="1"/>
<text x="0" y="1.2675" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.2675" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC1608X55" urn="urn:adsk.eagle:footprint:3793129/2" library_version="40" library_locally_modified="yes">
<description>CHIP, 1.6 X 0.8 X 0.55 mm body
&lt;p&gt;CHIP package with body size 1.6 X 0.8 X 0.55 mm&lt;/p&gt;</description>
<wire x1="0.85" y1="0.6516" x2="-0.85" y2="0.6516" width="0.12" layer="21"/>
<wire x1="0.85" y1="-0.6516" x2="-0.85" y2="-0.6516" width="0.12" layer="21"/>
<wire x1="0.85" y1="-0.45" x2="-0.85" y2="-0.45" width="0.12" layer="51"/>
<wire x1="-0.85" y1="-0.45" x2="-0.85" y2="0.45" width="0.12" layer="51"/>
<wire x1="-0.85" y1="0.45" x2="0.85" y2="0.45" width="0.12" layer="51"/>
<wire x1="0.85" y1="0.45" x2="0.85" y2="-0.45" width="0.12" layer="51"/>
<smd name="1" x="-0.7704" y="0" dx="0.8884" dy="0.9291" layer="1"/>
<smd name="2" x="0.7704" y="0" dx="0.8884" dy="0.9291" layer="1"/>
<text x="0" y="1.4136" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.4136" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="SOIC127P600X175-8" urn="urn:adsk.eagle:footprint:3833981/2" library_version="47">
<description>8-SOIC, 1.27 mm pitch, 6 mm span, 4.9 X 3.9 X 1.75 mm body
&lt;p&gt;8-pin SOIC package with 1.27 mm pitch, 6 mm span with body size 4.9 X 3.9 X 1.75 mm&lt;/p&gt;</description>
<circle x="-2.7288" y="2.7086" radius="0.25" width="0" layer="21"/>
<wire x1="-2" y1="2.5186" x2="2" y2="2.5186" width="0.12" layer="21"/>
<wire x1="-2" y1="-2.5186" x2="2" y2="-2.5186" width="0.12" layer="21"/>
<wire x1="2" y1="-2.5" x2="-2" y2="-2.5" width="0.12" layer="51"/>
<wire x1="-2" y1="-2.5" x2="-2" y2="2.5" width="0.12" layer="51"/>
<wire x1="-2" y1="2.5" x2="2" y2="2.5" width="0.12" layer="51"/>
<wire x1="2" y1="2.5" x2="2" y2="-2.5" width="0.12" layer="51"/>
<smd name="1" x="-2.4734" y="1.905" dx="1.9685" dy="0.5991" layer="1"/>
<smd name="2" x="-2.4734" y="0.635" dx="1.9685" dy="0.5991" layer="1"/>
<smd name="3" x="-2.4734" y="-0.635" dx="1.9685" dy="0.5991" layer="1"/>
<smd name="4" x="-2.4734" y="-1.905" dx="1.9685" dy="0.5991" layer="1"/>
<smd name="5" x="2.4734" y="-1.905" dx="1.9685" dy="0.5991" layer="1"/>
<smd name="6" x="2.4734" y="-0.635" dx="1.9685" dy="0.5991" layer="1"/>
<smd name="7" x="2.4734" y="0.635" dx="1.9685" dy="0.5991" layer="1"/>
<smd name="8" x="2.4734" y="1.905" dx="1.9685" dy="0.5991" layer="1"/>
<text x="0" y="3.5936" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-3.1536" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CKSWITCHES_KXT3" urn="urn:adsk.eagle:footprint:8413680/1" library_version="40" library_locally_modified="yes">
<smd name="1" x="-1.625" y="0" dx="1.5" dy="0.55" layer="1" rot="R90"/>
<smd name="2" x="1.625" y="0" dx="1.5" dy="0.55" layer="1" rot="R90"/>
<wire x1="-2.1" y1="1.1" x2="-2.1" y2="-1.1" width="0.127" layer="21"/>
<wire x1="-2.1" y1="-1.1" x2="2.1" y2="-1.1" width="0.127" layer="21"/>
<wire x1="2.1" y1="-1.1" x2="2.1" y2="1.1" width="0.127" layer="21"/>
<wire x1="2.1" y1="1.1" x2="-2.1" y2="1.1" width="0.127" layer="21"/>
<wire x1="-2.1" y1="-1.1" x2="2.1" y2="-1.1" width="0.127" layer="51"/>
<wire x1="-2.1" y1="1.1" x2="-2.1" y2="-1.1" width="0.127" layer="51"/>
<wire x1="2.1" y1="1.1" x2="-2.1" y2="1.1" width="0.127" layer="51"/>
<wire x1="2.1" y1="-1.1" x2="2.1" y2="1.1" width="0.127" layer="51"/>
<text x="-2" y="1.5" size="0.4064" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="-2" y="-2" size="0.4064" layer="27" font="vector" ratio="15">&gt;VALUE</text>
</package>
<package name="OSCCC500X320X110" urn="urn:adsk.eagle:footprint:3849468/1" library_version="47">
<description>OSCILLATOR CORNERCONCAVE, 5 X 3.2 X 1.1 mm body
&lt;p&gt;OSCILLATOR CORNERCONCAVE package with body size 5 X 3.2 X 1.1 mm&lt;/p&gt;</description>
<circle x="-3.2349" y="-1.253" radius="0.25" width="0" layer="21"/>
<wire x1="-2.7909" y1="0.421" x2="-2.7909" y2="-0.421" width="0.12" layer="21"/>
<wire x1="2.7909" y1="0.421" x2="2.7909" y2="-0.421" width="0.12" layer="21"/>
<wire x1="-0.971" y1="1.8909" x2="0.971" y2="1.8909" width="0.12" layer="21"/>
<wire x1="-0.971" y1="-1.8909" x2="0.971" y2="-1.8909" width="0.12" layer="21"/>
<wire x1="2.575" y1="-1.675" x2="-2.575" y2="-1.675" width="0.12" layer="51"/>
<wire x1="-2.575" y1="-1.675" x2="-2.575" y2="1.675" width="0.12" layer="51"/>
<wire x1="-2.575" y1="1.675" x2="2.575" y2="1.675" width="0.12" layer="51"/>
<wire x1="2.575" y1="1.675" x2="2.575" y2="-1.675" width="0.12" layer="51"/>
<smd name="1" x="-1.978" y="-1.253" dx="1.5059" dy="1.1559" layer="1"/>
<smd name="2" x="1.978" y="-1.253" dx="1.5059" dy="1.1559" layer="1"/>
<smd name="3" x="1.978" y="1.253" dx="1.5059" dy="1.1559" layer="1"/>
<smd name="4" x="-1.978" y="1.253" dx="1.5059" dy="1.1559" layer="1"/>
<text x="0" y="2.5259" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.5259" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CRYSTAL_CYLINDRICAL_RADIAL" urn="urn:adsk.eagle:footprint:8413679/1" library_version="40" library_locally_modified="yes">
<smd name="1" x="-0.45" y="2.45" dx="1.1" dy="0.6" layer="1" rot="R90"/>
<smd name="2" x="0.45" y="2.45" dx="1.1" dy="0.6" layer="1" rot="R90"/>
<smd name="3" x="0" y="-3.1" dx="2.4" dy="1.5" layer="1" rot="R90"/>
<wire x1="-1" y1="2" x2="-1" y2="-4.5" width="0.127" layer="21"/>
<wire x1="1" y1="2" x2="1" y2="-4.5" width="0.127" layer="21"/>
<wire x1="1" y1="-4.5" x2="-1" y2="-4.5" width="0.127" layer="21"/>
<wire x1="-1" y1="2" x2="-1" y2="-4.5" width="0.127" layer="51"/>
<wire x1="-1" y1="-4.5" x2="1" y2="-4.5" width="0.127" layer="51"/>
<wire x1="1" y1="-4.5" x2="1" y2="2" width="0.127" layer="51"/>
<wire x1="-1" y1="2.75" x2="-1" y2="3.25" width="0.127" layer="51"/>
<wire x1="-1" y1="3.25" x2="1" y2="3.25" width="0.127" layer="51"/>
<wire x1="1" y1="3.25" x2="1" y2="2.75" width="0.127" layer="51"/>
<wire x1="-1" y1="2.75" x2="-1" y2="3.25" width="0.127" layer="21"/>
<wire x1="-1" y1="3.25" x2="1" y2="3.25" width="0.127" layer="21"/>
<wire x1="1" y1="3.25" x2="1" y2="2.75" width="0.127" layer="21"/>
<text x="2" y="2" size="0.4064" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="2" y="1" size="0.4064" layer="27" font="vector" ratio="15">&gt;VALUE</text>
</package>
<package name="LED_CUSTOM_0402/RADIAL" urn="urn:adsk.eagle:footprint:8413687/1" library_version="41" library_locally_modified="yes">
<smd name="A_2" x="0" y="0.45" dx="0.5" dy="0.7" layer="1" roundness="25" rot="R270"/>
<smd name="K_2" x="0" y="-0.45" dx="0.5" dy="0.7" layer="1" roundness="25" rot="R270"/>
<smd name="A_3" x="0" y="0.45" dx="0.5" dy="0.7" layer="16" roundness="25" rot="R270"/>
<smd name="K_3" x="0" y="-0.45" dx="0.5" dy="0.7" layer="16" roundness="25" rot="R270"/>
<pad name="A_1" x="0" y="1.27" drill="0.6" diameter="1.143" shape="square" rot="R270"/>
<pad name="K_1" x="0" y="-1.27" drill="0.6" diameter="1.143" shape="octagon" rot="R270"/>
<text x="1.27" y="-3.81" size="1.27" layer="25" rot="R180">&gt;NAME</text>
<text x="1.27" y="5.08" size="1.27" layer="27" rot="R180">&gt;VALUE</text>
<wire x1="-1.905" y1="0.385" x2="-0.635" y2="0.385" width="0.127" layer="21"/>
<wire x1="-0.635" y1="0.385" x2="-1.27" y2="-0.385" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-0.385" x2="-1.905" y2="0.385" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-0.385" x2="-1.905" y2="-0.385" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-0.385" x2="-0.635" y2="-0.385" width="0.127" layer="21"/>
<wire x1="-1.25" y1="1" x2="-1.25" y2="0.375" width="0.127" layer="21"/>
<wire x1="-1.25" y1="-0.375" x2="-1.25" y2="-1" width="0.127" layer="21"/>
<wire x1="-1.5" y1="-2.125" x2="1.5" y2="-2.125" width="0.127" layer="21"/>
<circle x="0" y="0" radius="2.60108125" width="0.127" layer="21"/>
</package>
<package name="LED_RADIAL" urn="urn:adsk.eagle:footprint:8413692/1" library_version="41" library_locally_modified="yes">
<pad name="A" x="1.27" y="0" drill="0.6" diameter="1" shape="square" rot="R180"/>
<pad name="K" x="-1.27" y="0" drill="0.6" diameter="1" rot="R180"/>
<text x="-2.54" y="2.794" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-4.064" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="0.385" y1="1.905" x2="0.385" y2="0.635" width="0.127" layer="21"/>
<wire x1="0.385" y1="0.635" x2="-0.385" y2="1.27" width="0.127" layer="21"/>
<wire x1="-0.385" y1="1.27" x2="0.385" y2="1.905" width="0.127" layer="21"/>
<wire x1="-0.385" y1="1.27" x2="-0.385" y2="1.905" width="0.127" layer="21"/>
<wire x1="-0.385" y1="1.27" x2="-0.385" y2="0.635" width="0.127" layer="21"/>
<wire x1="1" y1="1.25" x2="0.375" y2="1.25" width="0.127" layer="21"/>
<wire x1="-0.375" y1="1.25" x2="-1" y2="1.25" width="0.127" layer="21"/>
<wire x1="-2.125" y1="1.5" x2="-2.125" y2="-1.5" width="0.127" layer="21"/>
<circle x="0" y="0" radius="2.60108125" width="0.127" layer="21"/>
</package>
<package name="LEDC1005X60N" urn="urn:adsk.eagle:footprint:15620583/1" library_version="41" library_locally_modified="yes">
<wire x1="-0.52" y1="-0.93" x2="-0.52" y2="0.93" width="0.127" layer="21"/>
<wire x1="-0.52" y1="0.93" x2="0.52" y2="0.93" width="0.127" layer="21"/>
<wire x1="0.52" y1="0.93" x2="0.52" y2="-0.93" width="0.127" layer="21"/>
<wire x1="0.52" y1="-0.93" x2="-0.52" y2="-0.93" width="0.127" layer="21" style="shortdash"/>
<wire x1="-0.52" y1="-0.93" x2="-0.52" y2="0.93" width="0.127" layer="51"/>
<wire x1="-0.52" y1="0.93" x2="0.52" y2="0.93" width="0.127" layer="51"/>
<wire x1="0.52" y1="0.93" x2="0.52" y2="-0.93" width="0.127" layer="51"/>
<wire x1="0.52" y1="-0.93" x2="-0.52" y2="-0.93" width="0.127" layer="51" style="shortdash"/>
<wire x1="-0.5" y1="-1.15" x2="0.5" y2="-1.15" width="0.127" layer="21"/>
<wire x1="-0.5" y1="-1.15" x2="0.5" y2="-1.15" width="0.127" layer="51"/>
<smd name="1" x="0" y="0.45" dx="0.5" dy="0.7" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-0.45" dx="0.5" dy="0.7" layer="1" roundness="25" rot="R270"/>
<text x="0.635" y="1.27" size="0.889" layer="25" ratio="11" rot="R270">&gt;NAME</text>
<text x="-1.524" y="1.397" size="0.635" layer="27" font="vector" ratio="10" rot="R270">&gt;VALUE</text>
</package>
<package name="RESC0603X30" urn="urn:adsk.eagle:footprint:3811152/1" library_version="47">
<description>CHIP, 0.6 X 0.3 X 0.3 mm body
&lt;p&gt;CHIP package with body size 0.6 X 0.3 X 0.3 mm&lt;/p&gt;</description>
<wire x1="0.315" y1="0.5124" x2="-0.315" y2="0.5124" width="0.12" layer="21"/>
<wire x1="0.315" y1="-0.5124" x2="-0.315" y2="-0.5124" width="0.12" layer="21"/>
<wire x1="0.315" y1="-0.165" x2="-0.315" y2="-0.165" width="0.12" layer="51"/>
<wire x1="-0.315" y1="-0.165" x2="-0.315" y2="0.165" width="0.12" layer="51"/>
<wire x1="-0.315" y1="0.165" x2="0.315" y2="0.165" width="0.12" layer="51"/>
<wire x1="0.315" y1="0.165" x2="0.315" y2="-0.165" width="0.12" layer="51"/>
<smd name="1" x="-0.325" y="0" dx="0.4469" dy="0.3969" layer="1"/>
<smd name="2" x="0.325" y="0" dx="0.4469" dy="0.3969" layer="1"/>
<text x="0" y="1.1474" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.1474" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="RESC1608X55" urn="urn:adsk.eagle:footprint:3811167/1" library_version="47">
<description>CHIP, 1.6 X 0.85 X 0.55 mm body
&lt;p&gt;CHIP package with body size 1.6 X 0.85 X 0.55 mm&lt;/p&gt;</description>
<wire x1="0.875" y1="0.8036" x2="-0.875" y2="0.8036" width="0.12" layer="21"/>
<wire x1="0.875" y1="-0.8036" x2="-0.875" y2="-0.8036" width="0.12" layer="21"/>
<wire x1="0.875" y1="-0.475" x2="-0.875" y2="-0.475" width="0.12" layer="51"/>
<wire x1="-0.875" y1="-0.475" x2="-0.875" y2="0.475" width="0.12" layer="51"/>
<wire x1="-0.875" y1="0.475" x2="0.875" y2="0.475" width="0.12" layer="51"/>
<wire x1="0.875" y1="0.475" x2="0.875" y2="-0.475" width="0.12" layer="51"/>
<smd name="1" x="-0.7996" y="0" dx="0.8709" dy="0.9791" layer="1"/>
<smd name="2" x="0.7996" y="0" dx="0.8709" dy="0.9791" layer="1"/>
<text x="0" y="1.4386" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.4386" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="RESC2012X70" urn="urn:adsk.eagle:footprint:3811170/1" library_version="47">
<description>CHIP, 2 X 1.25 X 0.7 mm body
&lt;p&gt;CHIP package with body size 2 X 1.25 X 0.7 mm&lt;/p&gt;</description>
<wire x1="1.1" y1="1.0036" x2="-1.1" y2="1.0036" width="0.12" layer="21"/>
<wire x1="1.1" y1="-1.0036" x2="-1.1" y2="-1.0036" width="0.12" layer="21"/>
<wire x1="1.1" y1="-0.675" x2="-1.1" y2="-0.675" width="0.12" layer="51"/>
<wire x1="-1.1" y1="-0.675" x2="-1.1" y2="0.675" width="0.12" layer="51"/>
<wire x1="-1.1" y1="0.675" x2="1.1" y2="0.675" width="0.12" layer="51"/>
<wire x1="1.1" y1="0.675" x2="1.1" y2="-0.675" width="0.12" layer="51"/>
<smd name="1" x="-0.94" y="0" dx="1.0354" dy="1.3791" layer="1"/>
<smd name="2" x="0.94" y="0" dx="1.0354" dy="1.3791" layer="1"/>
<text x="0" y="1.6386" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.6386" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="RESC3115X70" urn="urn:adsk.eagle:footprint:3811174/1" library_version="47">
<description>CHIP, 3.125 X 1.55 X 0.7 mm body
&lt;p&gt;CHIP package with body size 3.125 X 1.55 X 0.7 mm&lt;/p&gt;</description>
<wire x1="1.625" y1="1.1536" x2="-1.625" y2="1.1536" width="0.12" layer="21"/>
<wire x1="1.625" y1="-1.1536" x2="-1.625" y2="-1.1536" width="0.12" layer="21"/>
<wire x1="1.625" y1="-0.825" x2="-1.625" y2="-0.825" width="0.12" layer="51"/>
<wire x1="-1.625" y1="-0.825" x2="-1.625" y2="0.825" width="0.12" layer="51"/>
<wire x1="-1.625" y1="0.825" x2="1.625" y2="0.825" width="0.12" layer="51"/>
<wire x1="1.625" y1="0.825" x2="1.625" y2="-0.825" width="0.12" layer="51"/>
<smd name="1" x="-1.4449" y="0" dx="1.0841" dy="1.6791" layer="1"/>
<smd name="2" x="1.4449" y="0" dx="1.0841" dy="1.6791" layer="1"/>
<text x="0" y="1.7886" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.7886" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="RESC3225X70" urn="urn:adsk.eagle:footprint:3811185/1" library_version="47">
<description>CHIP, 3.2 X 2.5 X 0.7 mm body
&lt;p&gt;CHIP package with body size 3.2 X 2.5 X 0.7 mm&lt;/p&gt;</description>
<wire x1="1.7" y1="1.6717" x2="-1.7" y2="1.6717" width="0.12" layer="21"/>
<wire x1="1.7" y1="-1.6717" x2="-1.7" y2="-1.6717" width="0.12" layer="21"/>
<wire x1="1.7" y1="-1.35" x2="-1.7" y2="-1.35" width="0.12" layer="51"/>
<wire x1="-1.7" y1="-1.35" x2="-1.7" y2="1.35" width="0.12" layer="51"/>
<wire x1="-1.7" y1="1.35" x2="1.7" y2="1.35" width="0.12" layer="51"/>
<wire x1="1.7" y1="1.35" x2="1.7" y2="-1.35" width="0.12" layer="51"/>
<smd name="1" x="-1.49" y="0" dx="1.1354" dy="2.7153" layer="1"/>
<smd name="2" x="1.49" y="0" dx="1.1354" dy="2.7153" layer="1"/>
<text x="0" y="2.3067" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.3067" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="RESC4532X70" urn="urn:adsk.eagle:footprint:3811192/1" library_version="47">
<description>CHIP, 4.5 X 3.2 X 0.7 mm body
&lt;p&gt;CHIP package with body size 4.5 X 3.2 X 0.7 mm&lt;/p&gt;</description>
<wire x1="2.35" y1="2.0217" x2="-2.35" y2="2.0217" width="0.12" layer="21"/>
<wire x1="2.35" y1="-2.0217" x2="-2.35" y2="-2.0217" width="0.12" layer="21"/>
<wire x1="2.35" y1="-1.7" x2="-2.35" y2="-1.7" width="0.12" layer="51"/>
<wire x1="-2.35" y1="-1.7" x2="-2.35" y2="1.7" width="0.12" layer="51"/>
<wire x1="-2.35" y1="1.7" x2="2.35" y2="1.7" width="0.12" layer="51"/>
<wire x1="2.35" y1="1.7" x2="2.35" y2="-1.7" width="0.12" layer="51"/>
<smd name="1" x="-2.14" y="0" dx="1.1354" dy="3.4153" layer="1"/>
<smd name="2" x="2.14" y="0" dx="1.1354" dy="3.4153" layer="1"/>
<text x="0" y="2.6567" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.6567" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="RESC5025X70" urn="urn:adsk.eagle:footprint:3811199/1" library_version="47">
<description>CHIP, 5 X 2.5 X 0.7 mm body
&lt;p&gt;CHIP package with body size 5 X 2.5 X 0.7 mm&lt;/p&gt;</description>
<wire x1="2.6" y1="1.6717" x2="-2.6" y2="1.6717" width="0.12" layer="21"/>
<wire x1="2.6" y1="-1.6717" x2="-2.6" y2="-1.6717" width="0.12" layer="21"/>
<wire x1="2.6" y1="-1.35" x2="-2.6" y2="-1.35" width="0.12" layer="51"/>
<wire x1="-2.6" y1="-1.35" x2="-2.6" y2="1.35" width="0.12" layer="51"/>
<wire x1="-2.6" y1="1.35" x2="2.6" y2="1.35" width="0.12" layer="51"/>
<wire x1="2.6" y1="1.35" x2="2.6" y2="-1.35" width="0.12" layer="51"/>
<smd name="1" x="-2.34" y="0" dx="1.2354" dy="2.7153" layer="1"/>
<smd name="2" x="2.34" y="0" dx="1.2354" dy="2.7153" layer="1"/>
<text x="0" y="2.3067" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.3067" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="RESC5750X229" urn="urn:adsk.eagle:footprint:3811213/1" library_version="47">
<description>CHIP, 5.7 X 5 X 2.29 mm body
&lt;p&gt;CHIP package with body size 5.7 X 5 X 2.29 mm&lt;/p&gt;</description>
<wire x1="3.05" y1="3.0179" x2="-3.05" y2="3.0179" width="0.12" layer="21"/>
<wire x1="3.05" y1="-3.0179" x2="-3.05" y2="-3.0179" width="0.12" layer="21"/>
<wire x1="3.05" y1="-2.7" x2="-3.05" y2="-2.7" width="0.12" layer="51"/>
<wire x1="-3.05" y1="-2.7" x2="-3.05" y2="2.7" width="0.12" layer="51"/>
<wire x1="-3.05" y1="2.7" x2="3.05" y2="2.7" width="0.12" layer="51"/>
<wire x1="3.05" y1="2.7" x2="3.05" y2="-2.7" width="0.12" layer="51"/>
<smd name="1" x="-2.6355" y="0" dx="1.5368" dy="5.4078" layer="1"/>
<smd name="2" x="2.6355" y="0" dx="1.5368" dy="5.4078" layer="1"/>
<text x="0" y="3.6529" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-3.6529" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="RESC6432X70" urn="urn:adsk.eagle:footprint:3811285/1" library_version="47">
<description>CHIP, 6.4 X 3.2 X 0.7 mm body
&lt;p&gt;CHIP package with body size 6.4 X 3.2 X 0.7 mm&lt;/p&gt;</description>
<wire x1="3.3" y1="2.0217" x2="-3.3" y2="2.0217" width="0.12" layer="21"/>
<wire x1="3.3" y1="-2.0217" x2="-3.3" y2="-2.0217" width="0.12" layer="21"/>
<wire x1="3.3" y1="-1.7" x2="-3.3" y2="-1.7" width="0.12" layer="51"/>
<wire x1="-3.3" y1="-1.7" x2="-3.3" y2="1.7" width="0.12" layer="51"/>
<wire x1="-3.3" y1="1.7" x2="3.3" y2="1.7" width="0.12" layer="51"/>
<wire x1="3.3" y1="1.7" x2="3.3" y2="-1.7" width="0.12" layer="51"/>
<smd name="1" x="-3.04" y="0" dx="1.2354" dy="3.4153" layer="1"/>
<smd name="2" x="3.04" y="0" dx="1.2354" dy="3.4153" layer="1"/>
<text x="0" y="2.6567" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.6567" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="RESC2037X52" urn="urn:adsk.eagle:footprint:3950970/1" library_version="47">
<description>CHIP, 2 X 3.75 X 0.52 mm body
&lt;p&gt;CHIP package with body size 2 X 3.75 X 0.52 mm&lt;/p&gt;</description>
<wire x1="1.1" y1="2.3442" x2="-1.1" y2="2.3442" width="0.12" layer="21"/>
<wire x1="1.1" y1="-2.3442" x2="-1.1" y2="-2.3442" width="0.12" layer="21"/>
<wire x1="1.1" y1="-2.025" x2="-1.1" y2="-2.025" width="0.12" layer="51"/>
<wire x1="-1.1" y1="-2.025" x2="-1.1" y2="2.025" width="0.12" layer="51"/>
<wire x1="-1.1" y1="2.025" x2="1.1" y2="2.025" width="0.12" layer="51"/>
<wire x1="1.1" y1="2.025" x2="1.1" y2="-2.025" width="0.12" layer="51"/>
<smd name="1" x="-0.94" y="0" dx="1.0354" dy="4.0603" layer="1"/>
<smd name="2" x="0.94" y="0" dx="1.0354" dy="4.0603" layer="1"/>
<text x="0" y="2.9792" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.9792" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="INDC1005X60" urn="urn:adsk.eagle:footprint:3810111/1" library_version="47">
<description>CHIP, 1 X 0.5 X 0.6 mm body
&lt;p&gt;CHIP package with body size 1 X 0.5 X 0.6 mm&lt;/p&gt;</description>
<wire x1="0.55" y1="0.6717" x2="-0.55" y2="0.6717" width="0.12" layer="21"/>
<wire x1="0.55" y1="-0.6717" x2="-0.55" y2="-0.6717" width="0.12" layer="21"/>
<wire x1="0.55" y1="-0.35" x2="-0.55" y2="-0.35" width="0.12" layer="51"/>
<wire x1="-0.55" y1="-0.35" x2="-0.55" y2="0.35" width="0.12" layer="51"/>
<wire x1="-0.55" y1="0.35" x2="0.55" y2="0.35" width="0.12" layer="51"/>
<wire x1="0.55" y1="0.35" x2="0.55" y2="-0.35" width="0.12" layer="51"/>
<smd name="1" x="-0.475" y="0" dx="0.5791" dy="0.7153" layer="1"/>
<smd name="2" x="0.475" y="0" dx="0.5791" dy="0.7153" layer="1"/>
<text x="0" y="1.3067" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.3067" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="INDC1608X95" urn="urn:adsk.eagle:footprint:3810120/1" library_version="47">
<description>CHIP, 1.6 X 0.8 X 0.95 mm body
&lt;p&gt;CHIP package with body size 1.6 X 0.8 X 0.95 mm&lt;/p&gt;</description>
<wire x1="0.875" y1="0.7991" x2="-0.875" y2="0.7991" width="0.12" layer="21"/>
<wire x1="0.875" y1="-0.7991" x2="-0.875" y2="-0.7991" width="0.12" layer="21"/>
<wire x1="0.875" y1="-0.475" x2="-0.875" y2="-0.475" width="0.12" layer="51"/>
<wire x1="-0.875" y1="-0.475" x2="-0.875" y2="0.475" width="0.12" layer="51"/>
<wire x1="-0.875" y1="0.475" x2="0.875" y2="0.475" width="0.12" layer="51"/>
<wire x1="0.875" y1="0.475" x2="0.875" y2="-0.475" width="0.12" layer="51"/>
<smd name="1" x="-0.7746" y="0" dx="0.9209" dy="0.9702" layer="1"/>
<smd name="2" x="0.7746" y="0" dx="0.9209" dy="0.9702" layer="1"/>
<text x="0" y="1.4341" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.4341" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="INDC2520X220" urn="urn:adsk.eagle:footprint:3810124/1" library_version="47">
<description>CHIP, 2.5 X 2 X 2.2 mm body
&lt;p&gt;CHIP package with body size 2.5 X 2 X 2.2 mm&lt;/p&gt;</description>
<wire x1="1.35" y1="1.4692" x2="-1.35" y2="1.4692" width="0.12" layer="21"/>
<wire x1="1.35" y1="-1.4692" x2="-1.35" y2="-1.4692" width="0.12" layer="21"/>
<wire x1="1.35" y1="-1.15" x2="-1.35" y2="-1.15" width="0.12" layer="51"/>
<wire x1="-1.35" y1="-1.15" x2="-1.35" y2="1.15" width="0.12" layer="51"/>
<wire x1="-1.35" y1="1.15" x2="1.35" y2="1.15" width="0.12" layer="51"/>
<wire x1="1.35" y1="1.15" x2="1.35" y2="-1.15" width="0.12" layer="51"/>
<smd name="1" x="-1.2283" y="0" dx="0.9588" dy="2.3103" layer="1"/>
<smd name="2" x="1.2283" y="0" dx="0.9588" dy="2.3103" layer="1"/>
<text x="0" y="2.1042" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.1042" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="INDC3225X135" urn="urn:adsk.eagle:footprint:3810699/1" library_version="47">
<description>CHIP, 3.2 X 2.5 X 1.35 mm body
&lt;p&gt;CHIP package with body size 3.2 X 2.5 X 1.35 mm&lt;/p&gt;</description>
<wire x1="1.7" y1="1.6717" x2="-1.7" y2="1.6717" width="0.12" layer="21"/>
<wire x1="1.7" y1="-1.6717" x2="-1.7" y2="-1.6717" width="0.12" layer="21"/>
<wire x1="1.7" y1="-1.35" x2="-1.7" y2="-1.35" width="0.12" layer="51"/>
<wire x1="-1.7" y1="-1.35" x2="-1.7" y2="1.35" width="0.12" layer="51"/>
<wire x1="-1.7" y1="1.35" x2="1.7" y2="1.35" width="0.12" layer="51"/>
<wire x1="1.7" y1="1.35" x2="1.7" y2="-1.35" width="0.12" layer="51"/>
<smd name="1" x="-1.4754" y="0" dx="1.1646" dy="2.7153" layer="1"/>
<smd name="2" x="1.4754" y="0" dx="1.1646" dy="2.7153" layer="1"/>
<text x="0" y="2.3067" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.3067" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="INDC4509X190" urn="urn:adsk.eagle:footprint:3810713/1" library_version="47">
<description>CHIP, 4.5 X 0.9 X 1.9 mm body
&lt;p&gt;CHIP package with body size 4.5 X 0.9 X 1.9 mm&lt;/p&gt;</description>
<wire x1="2.4" y1="0.9192" x2="-2.4" y2="0.9192" width="0.12" layer="21"/>
<wire x1="2.4" y1="-0.9192" x2="-2.4" y2="-0.9192" width="0.12" layer="21"/>
<wire x1="2.4" y1="-0.6" x2="-2.4" y2="-0.6" width="0.12" layer="51"/>
<wire x1="-2.4" y1="-0.6" x2="-2.4" y2="0.6" width="0.12" layer="51"/>
<wire x1="-2.4" y1="0.6" x2="2.4" y2="0.6" width="0.12" layer="51"/>
<wire x1="2.4" y1="0.6" x2="2.4" y2="-0.6" width="0.12" layer="51"/>
<smd name="1" x="-2.11" y="0" dx="1.2904" dy="1.2103" layer="1"/>
<smd name="2" x="2.11" y="0" dx="1.2904" dy="1.2103" layer="1"/>
<text x="0" y="1.5542" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.5542" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="INDC4532X175" urn="urn:adsk.eagle:footprint:3810720/1" library_version="47">
<description>CHIP, 4.5 X 3.2 X 1.75 mm body
&lt;p&gt;CHIP package with body size 4.5 X 3.2 X 1.75 mm&lt;/p&gt;</description>
<wire x1="2.4" y1="2.0217" x2="-2.4" y2="2.0217" width="0.12" layer="21"/>
<wire x1="2.4" y1="-2.0217" x2="-2.4" y2="-2.0217" width="0.12" layer="21"/>
<wire x1="2.4" y1="-1.7" x2="-2.4" y2="-1.7" width="0.12" layer="51"/>
<wire x1="-2.4" y1="-1.7" x2="-2.4" y2="1.7" width="0.12" layer="51"/>
<wire x1="-2.4" y1="1.7" x2="2.4" y2="1.7" width="0.12" layer="51"/>
<wire x1="2.4" y1="1.7" x2="2.4" y2="-1.7" width="0.12" layer="51"/>
<smd name="1" x="-2.0565" y="0" dx="1.3973" dy="3.4153" layer="1"/>
<smd name="2" x="2.0565" y="0" dx="1.3973" dy="3.4153" layer="1"/>
<text x="0" y="2.6567" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.6567" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="INDC5750X180" urn="urn:adsk.eagle:footprint:3810723/1" library_version="47">
<description>CHIP, 5.7 X 5 X 1.8 mm body
&lt;p&gt;CHIP package with body size 5.7 X 5 X 1.8 mm&lt;/p&gt;</description>
<wire x1="2.95" y1="2.9692" x2="-2.95" y2="2.9692" width="0.12" layer="21"/>
<wire x1="2.95" y1="-2.9692" x2="-2.95" y2="-2.9692" width="0.12" layer="21"/>
<wire x1="2.95" y1="-2.65" x2="-2.95" y2="-2.65" width="0.12" layer="51"/>
<wire x1="-2.95" y1="-2.65" x2="-2.95" y2="2.65" width="0.12" layer="51"/>
<wire x1="-2.95" y1="2.65" x2="2.95" y2="2.65" width="0.12" layer="51"/>
<wire x1="2.95" y1="2.65" x2="2.95" y2="-2.65" width="0.12" layer="51"/>
<smd name="1" x="-2.7004" y="0" dx="1.2146" dy="5.3103" layer="1"/>
<smd name="2" x="2.7004" y="0" dx="1.2146" dy="5.3103" layer="1"/>
<text x="0" y="3.6042" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-3.6042" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="INDC6350X200" urn="urn:adsk.eagle:footprint:3811097/1" library_version="47">
<description>CHIP, 6.3 X 5 X 2 mm body
&lt;p&gt;CHIP package with body size 6.3 X 5 X 2 mm&lt;/p&gt;</description>
<wire x1="3.25" y1="2.9692" x2="-3.25" y2="2.9692" width="0.12" layer="21"/>
<wire x1="3.25" y1="-2.9692" x2="-3.25" y2="-2.9692" width="0.12" layer="21"/>
<wire x1="3.25" y1="-2.65" x2="-3.25" y2="-2.65" width="0.12" layer="51"/>
<wire x1="-3.25" y1="-2.65" x2="-3.25" y2="2.65" width="0.12" layer="51"/>
<wire x1="-3.25" y1="2.65" x2="3.25" y2="2.65" width="0.12" layer="51"/>
<wire x1="3.25" y1="2.65" x2="3.25" y2="-2.65" width="0.12" layer="51"/>
<smd name="1" x="-3.0004" y="0" dx="1.2146" dy="5.3103" layer="1"/>
<smd name="2" x="3.0004" y="0" dx="1.2146" dy="5.3103" layer="1"/>
<text x="0" y="3.6042" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-3.6042" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="INDM4848X300" urn="urn:adsk.eagle:footprint:3837794/1" library_version="47">
<description>MOLDED BODY, 4.8 X 4.8 X 3 mm body
&lt;p&gt;MOLDED BODY package with body size 4.8 X 4.8 X 3 mm&lt;/p&gt;</description>
<wire x1="-2.5" y1="2.5" x2="2.5" y2="2.5" width="0.12" layer="21"/>
<wire x1="-2.5" y1="-2.5" x2="2.5" y2="-2.5" width="0.12" layer="21"/>
<wire x1="2.5" y1="-2.5" x2="-2.5" y2="-2.5" width="0.12" layer="51"/>
<wire x1="-2.5" y1="-2.5" x2="-2.5" y2="2.5" width="0.12" layer="51"/>
<wire x1="-2.5" y1="2.5" x2="2.5" y2="2.5" width="0.12" layer="51"/>
<wire x1="2.5" y1="2.5" x2="2.5" y2="-2.5" width="0.12" layer="51"/>
<smd name="1" x="-1.74" y="0" dx="2.5354" dy="4.2118" layer="1"/>
<smd name="2" x="1.74" y="0" dx="2.5354" dy="4.2118" layer="1"/>
<text x="0" y="3.135" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-3.135" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="INDM5050X430" urn="urn:adsk.eagle:footprint:3890170/1" library_version="47">
<description>MOLDED BODY, 5 X 5 X 4.3 mm body
&lt;p&gt;MOLDED BODY package with body size 5 X 5 X 4.3 mm&lt;/p&gt;</description>
<wire x1="-2.51" y1="2.5202" x2="2.51" y2="2.5202" width="0.12" layer="21"/>
<wire x1="-2.51" y1="-2.5202" x2="2.51" y2="-2.5202" width="0.12" layer="21"/>
<wire x1="2.51" y1="-2.51" x2="-2.51" y2="-2.51" width="0.12" layer="51"/>
<wire x1="-2.51" y1="-2.51" x2="-2.51" y2="2.51" width="0.12" layer="51"/>
<wire x1="-2.51" y1="2.51" x2="2.51" y2="2.51" width="0.12" layer="51"/>
<wire x1="2.51" y1="2.51" x2="2.51" y2="-2.51" width="0.12" layer="51"/>
<smd name="1" x="-1.8761" y="0" dx="2.3466" dy="4.4123" layer="1"/>
<smd name="2" x="1.8761" y="0" dx="2.3466" dy="4.4123" layer="1"/>
<text x="0" y="3.1552" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-3.1552" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="INDM4440X300" urn="urn:adsk.eagle:footprint:3890490/1" library_version="47">
<description>MOLDED BODY, 4.45 X 4.06 X 3 mm body
&lt;p&gt;MOLDED BODY package with body size 4.45 X 4.06 X 3 mm&lt;/p&gt;</description>
<wire x1="-2.35" y1="2.3349" x2="2.35" y2="2.3349" width="0.12" layer="21"/>
<wire x1="-2.35" y1="-2.3349" x2="2.35" y2="-2.3349" width="0.12" layer="21"/>
<wire x1="2.35" y1="-2.155" x2="-2.35" y2="-2.155" width="0.12" layer="51"/>
<wire x1="-2.35" y1="-2.155" x2="-2.35" y2="2.155" width="0.12" layer="51"/>
<wire x1="-2.35" y1="2.155" x2="2.35" y2="2.155" width="0.12" layer="51"/>
<wire x1="2.35" y1="2.155" x2="2.35" y2="-2.155" width="0.12" layer="51"/>
<smd name="1" x="-2.0191" y="0" dx="1.674" dy="4.0418" layer="1"/>
<smd name="2" x="2.0191" y="0" dx="1.674" dy="4.0418" layer="1"/>
<text x="0" y="2.9699" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.9699" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="INDM7070X450" urn="urn:adsk.eagle:footprint:3890495/1" library_version="47">
<description>MOLDED BODY, 7 X 7 X 4.5 mm body
&lt;p&gt;MOLDED BODY package with body size 7 X 7 X 4.5 mm&lt;/p&gt;</description>
<wire x1="-3.5" y1="3.6699" x2="3.5" y2="3.6699" width="0.12" layer="21"/>
<wire x1="-3.5" y1="-3.6699" x2="3.5" y2="-3.6699" width="0.12" layer="21"/>
<wire x1="3.5" y1="-3.5" x2="-3.5" y2="-3.5" width="0.12" layer="51"/>
<wire x1="-3.5" y1="-3.5" x2="-3.5" y2="3.5" width="0.12" layer="51"/>
<wire x1="-3.5" y1="3.5" x2="3.5" y2="3.5" width="0.12" layer="51"/>
<wire x1="3.5" y1="3.5" x2="3.5" y2="-3.5" width="0.12" layer="51"/>
<smd name="1" x="-3.175" y="0" dx="1.7618" dy="6.7118" layer="1"/>
<smd name="2" x="3.175" y="0" dx="1.7618" dy="6.7118" layer="1"/>
<text x="0" y="4.3049" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-4.3049" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="INDM4949X400" urn="urn:adsk.eagle:footprint:3904917/1" library_version="47">
<description>MOLDED BODY, 4.9 X 4.9 X 4 mm body
&lt;p&gt;MOLDED BODY package with body size 4.9 X 4.9 X 4 mm&lt;/p&gt;</description>
<wire x1="-2.45" y1="2.6199" x2="2.45" y2="2.6199" width="0.12" layer="21"/>
<wire x1="-2.45" y1="-2.6199" x2="2.45" y2="-2.6199" width="0.12" layer="21"/>
<wire x1="2.45" y1="-2.45" x2="-2.45" y2="-2.45" width="0.12" layer="51"/>
<wire x1="-2.45" y1="-2.45" x2="-2.45" y2="2.45" width="0.12" layer="51"/>
<wire x1="-2.45" y1="2.45" x2="2.45" y2="2.45" width="0.12" layer="51"/>
<wire x1="2.45" y1="2.45" x2="2.45" y2="-2.45" width="0.12" layer="51"/>
<smd name="1" x="-1.9795" y="0" dx="2.0528" dy="4.6118" layer="1"/>
<smd name="2" x="1.9795" y="0" dx="2.0528" dy="4.6118" layer="1"/>
<text x="0" y="3.2549" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-3.2549" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="INDM6363X500" urn="urn:adsk.eagle:footprint:3920874/1" library_version="47">
<description>MOLDED BODY, 6.3 X 6.3 X 5 mm body
&lt;p&gt;MOLDED BODY package with body size 6.3 X 6.3 X 5 mm&lt;/p&gt;</description>
<wire x1="-3.15" y1="3.3199" x2="3.15" y2="3.3199" width="0.12" layer="21"/>
<wire x1="-3.15" y1="-3.3199" x2="3.15" y2="-3.3199" width="0.12" layer="21"/>
<wire x1="3.15" y1="-3.15" x2="-3.15" y2="-3.15" width="0.12" layer="51"/>
<wire x1="-3.15" y1="-3.15" x2="-3.15" y2="3.15" width="0.12" layer="51"/>
<wire x1="-3.15" y1="3.15" x2="3.15" y2="3.15" width="0.12" layer="51"/>
<wire x1="3.15" y1="3.15" x2="3.15" y2="-3.15" width="0.12" layer="51"/>
<smd name="1" x="-2.475" y="0" dx="2.4618" dy="6.0118" layer="1"/>
<smd name="2" x="2.475" y="0" dx="2.4618" dy="6.0118" layer="1"/>
<text x="0" y="3.9549" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-3.9549" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="INDM238238X1016" urn="urn:adsk.eagle:footprint:3949007/1" library_version="47">
<description>MOLDED BODY, 23.88 X 23.88 X 10.16 mm body
&lt;p&gt;MOLDED BODY package with body size 23.88 X 23.88 X 10.16 mm&lt;/p&gt;</description>
<wire x1="-11.94" y1="12.1099" x2="11.94" y2="12.1099" width="0.12" layer="21"/>
<wire x1="-11.94" y1="-12.1099" x2="11.94" y2="-12.1099" width="0.12" layer="21"/>
<wire x1="11.94" y1="-11.94" x2="-11.94" y2="-11.94" width="0.12" layer="51"/>
<wire x1="-11.94" y1="-11.94" x2="-11.94" y2="11.94" width="0.12" layer="51"/>
<wire x1="-11.94" y1="11.94" x2="11.94" y2="11.94" width="0.12" layer="51"/>
<wire x1="11.94" y1="11.94" x2="11.94" y2="-11.94" width="0.12" layer="51"/>
<smd name="1" x="-11.035" y="0" dx="2.9218" dy="23.5918" layer="1"/>
<smd name="2" x="11.035" y="0" dx="2.9218" dy="23.5918" layer="1"/>
<text x="0" y="12.7449" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-12.7449" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="INDM8080X400" urn="urn:adsk.eagle:footprint:3950983/1" library_version="47">
<description>MOLDED BODY, 8 X 8 X 4 mm body
&lt;p&gt;MOLDED BODY package with body size 8 X 8 X 4 mm&lt;/p&gt;</description>
<wire x1="-4" y1="4.1699" x2="4" y2="4.1699" width="0.12" layer="21"/>
<wire x1="-4" y1="-4.1699" x2="4" y2="-4.1699" width="0.12" layer="21"/>
<wire x1="4" y1="-4" x2="-4" y2="-4" width="0.12" layer="51"/>
<wire x1="-4" y1="-4" x2="-4" y2="4" width="0.12" layer="51"/>
<wire x1="-4" y1="4" x2="4" y2="4" width="0.12" layer="51"/>
<wire x1="4" y1="4" x2="4" y2="-4" width="0.12" layer="51"/>
<smd name="1" x="-3.175" y="0" dx="2.7618" dy="7.7118" layer="1"/>
<smd name="2" x="3.175" y="0" dx="2.7618" dy="7.7118" layer="1"/>
<text x="0" y="4.8049" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-4.8049" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="INDM125125X650" urn="urn:adsk.eagle:footprint:3950989/1" library_version="47">
<description>MOLDED BODY, 12.5 X 12.5 X 6.5 mm body
&lt;p&gt;MOLDED BODY package with body size 12.5 X 12.5 X 6.5 mm&lt;/p&gt;</description>
<wire x1="-6.25" y1="6.4199" x2="6.25" y2="6.4199" width="0.12" layer="21"/>
<wire x1="-6.25" y1="-6.4199" x2="6.25" y2="-6.4199" width="0.12" layer="21"/>
<wire x1="6.25" y1="-6.25" x2="-6.25" y2="-6.25" width="0.12" layer="51"/>
<wire x1="-6.25" y1="-6.25" x2="-6.25" y2="6.25" width="0.12" layer="51"/>
<wire x1="-6.25" y1="6.25" x2="6.25" y2="6.25" width="0.12" layer="51"/>
<wire x1="6.25" y1="6.25" x2="6.25" y2="-6.25" width="0.12" layer="51"/>
<smd name="1" x="-5.125" y="0" dx="3.3618" dy="12.2118" layer="1"/>
<smd name="2" x="5.125" y="0" dx="3.3618" dy="12.2118" layer="1"/>
<text x="0" y="7.0549" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-7.0549" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC1220X107" urn="urn:adsk.eagle:footprint:3793123/1" library_version="47">
<description>CHIP, 1.25 X 2 X 1.07 mm body
&lt;p&gt;CHIP package with body size 1.25 X 2 X 1.07 mm&lt;/p&gt;</description>
<wire x1="0.725" y1="1.4217" x2="-0.725" y2="1.4217" width="0.12" layer="21"/>
<wire x1="0.725" y1="-1.4217" x2="-0.725" y2="-1.4217" width="0.12" layer="21"/>
<wire x1="0.725" y1="-1.1" x2="-0.725" y2="-1.1" width="0.12" layer="51"/>
<wire x1="-0.725" y1="-1.1" x2="-0.725" y2="1.1" width="0.12" layer="51"/>
<wire x1="-0.725" y1="1.1" x2="0.725" y2="1.1" width="0.12" layer="51"/>
<wire x1="0.725" y1="1.1" x2="0.725" y2="-1.1" width="0.12" layer="51"/>
<smd name="1" x="-0.552" y="0" dx="0.7614" dy="2.2153" layer="1"/>
<smd name="2" x="0.552" y="0" dx="0.7614" dy="2.2153" layer="1"/>
<text x="0" y="2.0567" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.0567" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC1632X168" urn="urn:adsk.eagle:footprint:3793133/1" library_version="47">
<description>CHIP, 1.6 X 3.2 X 1.68 mm body
&lt;p&gt;CHIP package with body size 1.6 X 3.2 X 1.68 mm&lt;/p&gt;</description>
<wire x1="0.9" y1="2.0217" x2="-0.9" y2="2.0217" width="0.12" layer="21"/>
<wire x1="0.9" y1="-2.0217" x2="-0.9" y2="-2.0217" width="0.12" layer="21"/>
<wire x1="0.9" y1="-1.7" x2="-0.9" y2="-1.7" width="0.12" layer="51"/>
<wire x1="-0.9" y1="-1.7" x2="-0.9" y2="1.7" width="0.12" layer="51"/>
<wire x1="-0.9" y1="1.7" x2="0.9" y2="1.7" width="0.12" layer="51"/>
<wire x1="0.9" y1="1.7" x2="0.9" y2="-1.7" width="0.12" layer="51"/>
<smd name="1" x="-0.786" y="0" dx="0.9434" dy="3.4153" layer="1"/>
<smd name="2" x="0.786" y="0" dx="0.9434" dy="3.4153" layer="1"/>
<text x="0" y="2.6567" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.6567" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC2012X127" urn="urn:adsk.eagle:footprint:3793140/1" library_version="47">
<description>CHIP, 2.01 X 1.25 X 1.27 mm body
&lt;p&gt;CHIP package with body size 2.01 X 1.25 X 1.27 mm&lt;/p&gt;</description>
<wire x1="1.105" y1="1.0467" x2="-1.105" y2="1.0467" width="0.12" layer="21"/>
<wire x1="1.105" y1="-1.0467" x2="-1.105" y2="-1.0467" width="0.12" layer="21"/>
<wire x1="1.105" y1="-0.725" x2="-1.105" y2="-0.725" width="0.12" layer="51"/>
<wire x1="-1.105" y1="-0.725" x2="-1.105" y2="0.725" width="0.12" layer="51"/>
<wire x1="-1.105" y1="0.725" x2="1.105" y2="0.725" width="0.12" layer="51"/>
<wire x1="1.105" y1="0.725" x2="1.105" y2="-0.725" width="0.12" layer="51"/>
<smd name="1" x="-0.8804" y="0" dx="1.1646" dy="1.4653" layer="1"/>
<smd name="2" x="0.8804" y="0" dx="1.1646" dy="1.4653" layer="1"/>
<text x="0" y="1.6817" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.6817" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC3215X168" urn="urn:adsk.eagle:footprint:3793146/1" library_version="47">
<description>CHIP, 3.2 X 1.5 X 1.68 mm body
&lt;p&gt;CHIP package with body size 3.2 X 1.5 X 1.68 mm&lt;/p&gt;</description>
<wire x1="1.7" y1="1.1286" x2="-1.7" y2="1.1286" width="0.12" layer="21"/>
<wire x1="1.7" y1="-1.1286" x2="-1.7" y2="-1.1286" width="0.12" layer="21"/>
<wire x1="1.7" y1="-0.8" x2="-1.7" y2="-0.8" width="0.12" layer="51"/>
<wire x1="-1.7" y1="-0.8" x2="-1.7" y2="0.8" width="0.12" layer="51"/>
<wire x1="-1.7" y1="0.8" x2="1.7" y2="0.8" width="0.12" layer="51"/>
<wire x1="1.7" y1="0.8" x2="1.7" y2="-0.8" width="0.12" layer="51"/>
<smd name="1" x="-1.4913" y="0" dx="1.1327" dy="1.6291" layer="1"/>
<smd name="2" x="1.4913" y="0" dx="1.1327" dy="1.6291" layer="1"/>
<text x="0" y="1.7636" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.7636" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC3225X168" urn="urn:adsk.eagle:footprint:3793150/1" library_version="47">
<description>CHIP, 3.2 X 2.5 X 1.68 mm body
&lt;p&gt;CHIP package with body size 3.2 X 2.5 X 1.68 mm&lt;/p&gt;</description>
<wire x1="1.7" y1="1.6717" x2="-1.7" y2="1.6717" width="0.12" layer="21"/>
<wire x1="1.7" y1="-1.6717" x2="-1.7" y2="-1.6717" width="0.12" layer="21"/>
<wire x1="1.7" y1="-1.35" x2="-1.7" y2="-1.35" width="0.12" layer="51"/>
<wire x1="-1.7" y1="-1.35" x2="-1.7" y2="1.35" width="0.12" layer="51"/>
<wire x1="-1.7" y1="1.35" x2="1.7" y2="1.35" width="0.12" layer="51"/>
<wire x1="1.7" y1="1.35" x2="1.7" y2="-1.35" width="0.12" layer="51"/>
<smd name="1" x="-1.4913" y="0" dx="1.1327" dy="2.7153" layer="1"/>
<smd name="2" x="1.4913" y="0" dx="1.1327" dy="2.7153" layer="1"/>
<text x="0" y="2.3067" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.3067" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC4520X168" urn="urn:adsk.eagle:footprint:3793156/1" library_version="47">
<description>CHIP, 4.5 X 2.03 X 1.68 mm body
&lt;p&gt;CHIP package with body size 4.5 X 2.03 X 1.68 mm&lt;/p&gt;</description>
<wire x1="2.375" y1="1.4602" x2="-2.375" y2="1.4602" width="0.12" layer="21"/>
<wire x1="2.375" y1="-1.4602" x2="-2.375" y2="-1.4602" width="0.12" layer="21"/>
<wire x1="2.375" y1="-1.14" x2="-2.375" y2="-1.14" width="0.12" layer="51"/>
<wire x1="-2.375" y1="-1.14" x2="-2.375" y2="1.14" width="0.12" layer="51"/>
<wire x1="-2.375" y1="1.14" x2="2.375" y2="1.14" width="0.12" layer="51"/>
<wire x1="2.375" y1="1.14" x2="2.375" y2="-1.14" width="0.12" layer="51"/>
<smd name="1" x="-2.1266" y="0" dx="1.2091" dy="2.2923" layer="1"/>
<smd name="2" x="2.1266" y="0" dx="1.2091" dy="2.2923" layer="1"/>
<text x="0" y="2.0952" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.0952" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC4532X218" urn="urn:adsk.eagle:footprint:3793162/1" library_version="47">
<description>CHIP, 4.5 X 3.2 X 2.18 mm body
&lt;p&gt;CHIP package with body size 4.5 X 3.2 X 2.18 mm&lt;/p&gt;</description>
<wire x1="2.375" y1="2.0217" x2="-2.375" y2="2.0217" width="0.12" layer="21"/>
<wire x1="2.375" y1="-2.0217" x2="-2.375" y2="-2.0217" width="0.12" layer="21"/>
<wire x1="2.375" y1="-1.7" x2="-2.375" y2="-1.7" width="0.12" layer="51"/>
<wire x1="-2.375" y1="-1.7" x2="-2.375" y2="1.7" width="0.12" layer="51"/>
<wire x1="-2.375" y1="1.7" x2="2.375" y2="1.7" width="0.12" layer="51"/>
<wire x1="2.375" y1="1.7" x2="2.375" y2="-1.7" width="0.12" layer="51"/>
<smd name="1" x="-2.1266" y="0" dx="1.2091" dy="3.4153" layer="1"/>
<smd name="2" x="2.1266" y="0" dx="1.2091" dy="3.4153" layer="1"/>
<text x="0" y="2.6567" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.6567" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC4564X218" urn="urn:adsk.eagle:footprint:3793169/1" library_version="47">
<description>CHIP, 4.5 X 6.4 X 2.18 mm body
&lt;p&gt;CHIP package with body size 4.5 X 6.4 X 2.18 mm&lt;/p&gt;</description>
<wire x1="2.375" y1="3.6452" x2="-2.375" y2="3.6452" width="0.12" layer="21"/>
<wire x1="2.375" y1="-3.6452" x2="-2.375" y2="-3.6452" width="0.12" layer="21"/>
<wire x1="2.375" y1="-3.325" x2="-2.375" y2="-3.325" width="0.12" layer="51"/>
<wire x1="-2.375" y1="-3.325" x2="-2.375" y2="3.325" width="0.12" layer="51"/>
<wire x1="-2.375" y1="3.325" x2="2.375" y2="3.325" width="0.12" layer="51"/>
<wire x1="2.375" y1="3.325" x2="2.375" y2="-3.325" width="0.12" layer="51"/>
<smd name="1" x="-2.1266" y="0" dx="1.2091" dy="6.6623" layer="1"/>
<smd name="2" x="2.1266" y="0" dx="1.2091" dy="6.6623" layer="1"/>
<text x="0" y="4.2802" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-4.2802" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC5650X218" urn="urn:adsk.eagle:footprint:3793172/1" library_version="47">
<description>CHIP, 5.64 X 5.08 X 2.18 mm body
&lt;p&gt;CHIP package with body size 5.64 X 5.08 X 2.18 mm&lt;/p&gt;</description>
<wire x1="2.895" y1="2.9852" x2="-2.895" y2="2.9852" width="0.12" layer="21"/>
<wire x1="2.895" y1="-2.9852" x2="-2.895" y2="-2.9852" width="0.12" layer="21"/>
<wire x1="2.895" y1="-2.665" x2="-2.895" y2="-2.665" width="0.12" layer="51"/>
<wire x1="-2.895" y1="-2.665" x2="-2.895" y2="2.665" width="0.12" layer="51"/>
<wire x1="-2.895" y1="2.665" x2="2.895" y2="2.665" width="0.12" layer="51"/>
<wire x1="2.895" y1="2.665" x2="2.895" y2="-2.665" width="0.12" layer="51"/>
<smd name="1" x="-2.6854" y="0" dx="1.1393" dy="5.3423" layer="1"/>
<smd name="2" x="2.6854" y="0" dx="1.1393" dy="5.3423" layer="1"/>
<text x="0" y="3.6202" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-3.6202" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC5563X218" urn="urn:adsk.eagle:footprint:3793175/1" library_version="47">
<description>CHIP, 5.59 X 6.35 X 2.18 mm body
&lt;p&gt;CHIP package with body size 5.59 X 6.35 X 2.18 mm&lt;/p&gt;</description>
<wire x1="2.92" y1="3.6202" x2="-2.92" y2="3.6202" width="0.12" layer="21"/>
<wire x1="2.92" y1="-3.6202" x2="-2.92" y2="-3.6202" width="0.12" layer="21"/>
<wire x1="2.92" y1="-3.3" x2="-2.92" y2="-3.3" width="0.12" layer="51"/>
<wire x1="-2.92" y1="-3.3" x2="-2.92" y2="3.3" width="0.12" layer="51"/>
<wire x1="-2.92" y1="3.3" x2="2.92" y2="3.3" width="0.12" layer="51"/>
<wire x1="2.92" y1="3.3" x2="2.92" y2="-3.3" width="0.12" layer="51"/>
<smd name="1" x="-2.6716" y="0" dx="1.2091" dy="6.6123" layer="1"/>
<smd name="2" x="2.6716" y="0" dx="1.2091" dy="6.6123" layer="1"/>
<text x="0" y="4.2552" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-4.2552" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC9198X218" urn="urn:adsk.eagle:footprint:3793179/1" library_version="47">
<description>CHIP, 9.14 X 9.82 X 2.18 mm body
&lt;p&gt;CHIP package with body size 9.14 X 9.82 X 2.18 mm&lt;/p&gt;</description>
<wire x1="4.76" y1="5.2799" x2="-4.76" y2="5.2799" width="0.12" layer="21"/>
<wire x1="4.76" y1="-5.2799" x2="-4.76" y2="-5.2799" width="0.12" layer="21"/>
<wire x1="4.76" y1="-4.91" x2="-4.76" y2="-4.91" width="0.12" layer="51"/>
<wire x1="-4.76" y1="-4.91" x2="-4.76" y2="4.91" width="0.12" layer="51"/>
<wire x1="-4.76" y1="4.91" x2="4.76" y2="4.91" width="0.12" layer="51"/>
<wire x1="4.76" y1="4.91" x2="4.76" y2="-4.91" width="0.12" layer="51"/>
<smd name="1" x="-4.4571" y="0" dx="1.314" dy="9.9318" layer="1"/>
<smd name="2" x="4.4571" y="0" dx="1.314" dy="9.9318" layer="1"/>
<text x="0" y="5.9149" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-5.9149" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="SOIC127P800X203-8" urn="urn:adsk.eagle:footprint:3852773/1" library_version="47">
<description>8-SOIC, 1.27 mm pitch, 8.005 mm span, 5.23 X 5.255 X 2.03 mm body
&lt;p&gt;8-pin SOIC package with 1.27 mm pitch, 8.005 mm span with body size 5.23 X 5.255 X 2.03 mm&lt;/p&gt;</description>
<circle x="-3.588" y="2.701" radius="0.25" width="0" layer="21"/>
<wire x1="-2.69" y1="2.451" x2="-2.69" y2="2.665" width="0.12" layer="21"/>
<wire x1="-2.69" y1="2.665" x2="2.69" y2="2.665" width="0.12" layer="21"/>
<wire x1="2.69" y1="2.665" x2="2.69" y2="2.451" width="0.12" layer="21"/>
<wire x1="-2.69" y1="-2.451" x2="-2.69" y2="-2.665" width="0.12" layer="21"/>
<wire x1="-2.69" y1="-2.665" x2="2.69" y2="-2.665" width="0.12" layer="21"/>
<wire x1="2.69" y1="-2.665" x2="2.69" y2="-2.451" width="0.12" layer="21"/>
<wire x1="2.69" y1="-2.665" x2="-2.69" y2="-2.665" width="0.12" layer="51"/>
<wire x1="-2.69" y1="-2.665" x2="-2.69" y2="2.665" width="0.12" layer="51"/>
<wire x1="-2.69" y1="2.665" x2="2.69" y2="2.665" width="0.12" layer="51"/>
<wire x1="2.69" y1="2.665" x2="2.69" y2="-2.665" width="0.12" layer="51"/>
<smd name="1" x="-3.6717" y="1.905" dx="1.6287" dy="0.584" layer="1"/>
<smd name="2" x="-3.6717" y="0.635" dx="1.6287" dy="0.584" layer="1"/>
<smd name="3" x="-3.6717" y="-0.635" dx="1.6287" dy="0.584" layer="1"/>
<smd name="4" x="-3.6717" y="-1.905" dx="1.6287" dy="0.584" layer="1"/>
<smd name="5" x="3.6717" y="-1.905" dx="1.6287" dy="0.584" layer="1"/>
<smd name="6" x="3.6717" y="-0.635" dx="1.6287" dy="0.584" layer="1"/>
<smd name="7" x="3.6717" y="0.635" dx="1.6287" dy="0.584" layer="1"/>
<smd name="8" x="3.6717" y="1.905" dx="1.6287" dy="0.584" layer="1"/>
<text x="0" y="3.586" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-3.3" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="OSCCC320X250X80" urn="urn:adsk.eagle:footprint:3849547/1" library_version="47">
<description>OSCILLATOR CORNERCONCAVE, 3.2 X 2.5 X 0.8 mm body
&lt;p&gt;OSCILLATOR CORNERCONCAVE package with body size 3.2 X 2.5 X 0.8 mm&lt;/p&gt;</description>
<circle x="-2.3599" y="-0.9" radius="0.25" width="0" layer="21"/>
<wire x1="-1.9159" y1="0.0401" x2="-1.9159" y2="-0.0401" width="0.12" layer="21"/>
<wire x1="1.9159" y1="0.0401" x2="1.9159" y2="-0.0401" width="0.12" layer="21"/>
<wire x1="-0.2901" y1="1.5659" x2="0.2901" y2="1.5659" width="0.12" layer="21"/>
<wire x1="-0.2901" y1="-1.5659" x2="0.2901" y2="-1.5659" width="0.12" layer="21"/>
<wire x1="1.65" y1="-1.3" x2="-1.65" y2="-1.3" width="0.12" layer="51"/>
<wire x1="-1.65" y1="-1.3" x2="-1.65" y2="1.3" width="0.12" layer="51"/>
<wire x1="-1.65" y1="1.3" x2="1.65" y2="1.3" width="0.12" layer="51"/>
<wire x1="1.65" y1="1.3" x2="1.65" y2="-1.3" width="0.12" layer="51"/>
<smd name="1" x="-1.2" y="-0.9" dx="1.3118" dy="1.2118" layer="1"/>
<smd name="2" x="1.2" y="-0.9" dx="1.3118" dy="1.2118" layer="1"/>
<smd name="3" x="1.2" y="0.9" dx="1.3118" dy="1.2118" layer="1"/>
<smd name="4" x="-1.2" y="0.9" dx="1.3118" dy="1.2118" layer="1"/>
<text x="0" y="2.2009" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.2009" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="LEDC1608X55" urn="urn:adsk.eagle:footprint:3905181/2" library_version="47">
<description>CHIP, 1.6 X 0.85 X 0.55 mm body
&lt;p&gt;CHIP package with body size 1.6 X 0.85 X 0.55 mm&lt;/p&gt;</description>
<wire x1="1.4319" y1="0.5496" x2="1.4319" y2="-0.5496" width="0.12" layer="21"/>
<wire x1="0.875" y1="-0.475" x2="-0.875" y2="-0.475" width="0.12" layer="51"/>
<wire x1="-0.875" y1="-0.475" x2="-0.875" y2="0.475" width="0.12" layer="51"/>
<wire x1="-0.875" y1="0.475" x2="0.875" y2="0.475" width="0.12" layer="51"/>
<wire x1="0.875" y1="0.475" x2="0.875" y2="-0.475" width="0.12" layer="51"/>
<smd name="A" x="-0.7996" y="0" dx="0.8709" dy="0.9791" layer="1"/>
<smd name="K" x="0.7996" y="0" dx="0.8709" dy="0.9791" layer="1"/>
<text x="0" y="1.4386" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.4386" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="LEDC2012X70" urn="urn:adsk.eagle:footprint:3905185/2" library_version="47">
<description>CHIP, 2 X 1.25 X 0.7 mm body
&lt;p&gt;CHIP package with body size 2 X 1.25 X 0.7 mm&lt;/p&gt;</description>
<wire x1="1.7173" y1="0.7496" x2="1.7173" y2="-0.7496" width="0.12" layer="21"/>
<wire x1="1.1" y1="-0.675" x2="-1.1" y2="-0.675" width="0.12" layer="51"/>
<wire x1="-1.1" y1="-0.675" x2="-1.1" y2="0.675" width="0.12" layer="51"/>
<wire x1="-1.1" y1="0.675" x2="1.1" y2="0.675" width="0.12" layer="51"/>
<wire x1="1.1" y1="0.675" x2="1.1" y2="-0.675" width="0.12" layer="51"/>
<smd name="A" x="-0.94" y="0" dx="1.0354" dy="1.3791" layer="1"/>
<smd name="K" x="0.94" y="0" dx="1.0354" dy="1.3791" layer="1"/>
<text x="0" y="1.6386" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.6386" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="CIR_PAD_1MM" urn="urn:adsk.eagle:package:8414067/2" type="empty" library_version="40" library_locally_modified="yes">
<packageinstances>
<packageinstance name="CIR_PAD_1MM"/>
</packageinstances>
</package3d>
<package3d name="TP_1.5MM" urn="urn:adsk.eagle:package:8414069/2" type="empty" library_version="40" library_locally_modified="yes">
<packageinstances>
<packageinstance name="TP_1.5MM"/>
</packageinstances>
</package3d>
<package3d name="TP_1.5MM_PAD" urn="urn:adsk.eagle:package:8414070/2" type="empty" library_version="40" library_locally_modified="yes">
<packageinstances>
<packageinstance name="TP_1.5MM_PAD"/>
</packageinstances>
</package3d>
<package3d name="TP_1.7MM" urn="urn:adsk.eagle:package:8414071/2" type="empty" library_version="40" library_locally_modified="yes">
<packageinstances>
<packageinstance name="TP_1.7MM"/>
</packageinstances>
</package3d>
<package3d name="C120H40" urn="urn:adsk.eagle:package:8414063/2" type="empty" library_version="40" library_locally_modified="yes">
<packageinstances>
<packageinstance name="C120H40"/>
</packageinstances>
</package3d>
<package3d name="C131H51" urn="urn:adsk.eagle:package:8414064/2" type="empty" library_version="40" library_locally_modified="yes">
<packageinstances>
<packageinstance name="C131H51"/>
</packageinstances>
</package3d>
<package3d name="C406H326" urn="urn:adsk.eagle:package:8414065/2" type="empty" library_version="40" library_locally_modified="yes">
<packageinstances>
<packageinstance name="C406H326"/>
</packageinstances>
</package3d>
<package3d name="C491H411" urn="urn:adsk.eagle:package:8414066/2" type="empty" library_version="40" library_locally_modified="yes">
<packageinstances>
<packageinstance name="C491H411"/>
</packageinstances>
</package3d>
<package3d name="CIR_PAD_2MM" urn="urn:adsk.eagle:package:8414068/2" type="empty" library_version="40" library_locally_modified="yes">
<packageinstances>
<packageinstance name="CIR_PAD_2MM"/>
</packageinstances>
</package3d>
<package3d name="RESC1005X40" urn="urn:adsk.eagle:package:3811154/1" type="model" library_version="47">
<description>CHIP, 1 X 0.5 X 0.4 mm body
&lt;p&gt;CHIP package with body size 1 X 0.5 X 0.4 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESC1005X40"/>
</packageinstances>
</package3d>
<package3d name="INDM4040X300" urn="urn:adsk.eagle:package:3890461/1" type="model" library_version="47">
<description>MOLDED BODY, 4 X 4 X 3 mm body
&lt;p&gt;MOLDED BODY package with body size 4 X 4 X 3 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="INDM4040X300"/>
</packageinstances>
</package3d>
<package3d name="RADIAL_THT" urn="urn:adsk.eagle:package:8414048/1" type="box" library_version="40" library_locally_modified="yes">
<packageinstances>
<packageinstance name="RADIAL_THT"/>
</packageinstances>
</package3d>
<package3d name="QFP50P1600X1600X120-100" urn="urn:adsk.eagle:package:3791942/1" type="model" library_version="47">
<description>100-QFP, 0.5 mm pitch, 16 mm span, 14 X 14 X 1.2 mm body
&lt;p&gt;100-pin QFP package with 0.5 mm pitch, 16 mm lead span1 X 16 mm lead span2 with body size 14 X 14 X 1.2 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="QFP50P1600X1600X120-100"/>
</packageinstances>
</package3d>
<package3d name="CAPC1005X60" urn="urn:adsk.eagle:package:3793108/1" type="model" library_version="47">
<description>CHIP, 1.025 X 0.525 X 0.6 mm body
&lt;p&gt;CHIP package with body size 1.025 X 0.525 X 0.6 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC1005X60"/>
</packageinstances>
</package3d>
<package3d name="CAPC1608X55" urn="urn:adsk.eagle:package:3793126/2" type="model" library_version="40" library_locally_modified="yes">
<description>CHIP, 1.6 X 0.8 X 0.55 mm body
&lt;p&gt;CHIP package with body size 1.6 X 0.8 X 0.55 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC1608X55"/>
</packageinstances>
</package3d>
<package3d name="SOIC127P600X175-8" urn="urn:adsk.eagle:package:3833977/2" type="model" library_version="47">
<description>8-SOIC, 1.27 mm pitch, 6 mm span, 4.9 X 3.9 X 1.75 mm body
&lt;p&gt;8-pin SOIC package with 1.27 mm pitch, 6 mm span with body size 4.9 X 3.9 X 1.75 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="SOIC127P600X175-8"/>
</packageinstances>
</package3d>
<package3d name="CKSWITCHES_KXT3" urn="urn:adsk.eagle:package:8414043/1" type="box" library_version="40" library_locally_modified="yes">
<packageinstances>
<packageinstance name="CKSWITCHES_KXT3"/>
</packageinstances>
</package3d>
<package3d name="OSCCC500X320X110" urn="urn:adsk.eagle:package:3849466/1" type="model" library_version="47">
<description>OSCILLATOR CORNERCONCAVE, 5 X 3.2 X 1.1 mm body
&lt;p&gt;OSCILLATOR CORNERCONCAVE package with body size 5 X 3.2 X 1.1 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="OSCCC500X320X110"/>
</packageinstances>
</package3d>
<package3d name="CRYSTAL_CYLINDRICAL_RADIAL" urn="urn:adsk.eagle:package:8414042/1" type="box" library_version="40" library_locally_modified="yes">
<packageinstances>
<packageinstance name="CRYSTAL_CYLINDRICAL_RADIAL"/>
</packageinstances>
</package3d>
<package3d name="LED_CUSTOM_0402/RADIAL" urn="urn:adsk.eagle:package:8414050/2" type="model" library_version="41" library_locally_modified="yes">
<packageinstances>
<packageinstance name="LED_CUSTOM_0402/RADIAL"/>
</packageinstances>
</package3d>
<package3d name="LED_RADIAL" urn="urn:adsk.eagle:package:8414056/2" type="model" library_version="41" library_locally_modified="yes">
<packageinstances>
<packageinstance name="LED_RADIAL"/>
</packageinstances>
</package3d>
<package3d name="LEDC1005X60N" urn="urn:adsk.eagle:package:15620674/1" type="box" library_version="41" library_locally_modified="yes">
<packageinstances>
<packageinstance name="LEDC1005X60N"/>
</packageinstances>
</package3d>
<package3d name="TAG-CONNECT_TC2050-IDC-NL" urn="urn:adsk.eagle:package:16170706/2" type="box" library_version="47">
<packageinstances>
<packageinstance name="TAG-CONNECT_TC2050-IDC-NL"/>
</packageinstances>
</package3d>
<package3d name="RESC0603X30" urn="urn:adsk.eagle:package:3811151/1" type="model" library_version="47">
<description>CHIP, 0.6 X 0.3 X 0.3 mm body
&lt;p&gt;CHIP package with body size 0.6 X 0.3 X 0.3 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESC0603X30"/>
</packageinstances>
</package3d>
<package3d name="RESC1608X55" urn="urn:adsk.eagle:package:3811166/1" type="model" library_version="47">
<description>CHIP, 1.6 X 0.85 X 0.55 mm body
&lt;p&gt;CHIP package with body size 1.6 X 0.85 X 0.55 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESC1608X55"/>
</packageinstances>
</package3d>
<package3d name="RESC2012X70" urn="urn:adsk.eagle:package:3811169/1" type="model" library_version="47">
<description>CHIP, 2 X 1.25 X 0.7 mm body
&lt;p&gt;CHIP package with body size 2 X 1.25 X 0.7 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESC2012X70"/>
</packageinstances>
</package3d>
<package3d name="RESC3115X70" urn="urn:adsk.eagle:package:3811173/1" type="model" library_version="47">
<description>CHIP, 3.125 X 1.55 X 0.7 mm body
&lt;p&gt;CHIP package with body size 3.125 X 1.55 X 0.7 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESC3115X70"/>
</packageinstances>
</package3d>
<package3d name="RESC3225X70" urn="urn:adsk.eagle:package:3811180/1" type="model" library_version="47">
<description>CHIP, 3.2 X 2.5 X 0.7 mm body
&lt;p&gt;CHIP package with body size 3.2 X 2.5 X 0.7 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESC3225X70"/>
</packageinstances>
</package3d>
<package3d name="RESC4532X70" urn="urn:adsk.eagle:package:3811188/1" type="model" library_version="47">
<description>CHIP, 4.5 X 3.2 X 0.7 mm body
&lt;p&gt;CHIP package with body size 4.5 X 3.2 X 0.7 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESC4532X70"/>
</packageinstances>
</package3d>
<package3d name="RESC5025X70" urn="urn:adsk.eagle:package:3811197/1" type="model" library_version="47">
<description>CHIP, 5 X 2.5 X 0.7 mm body
&lt;p&gt;CHIP package with body size 5 X 2.5 X 0.7 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESC5025X70"/>
</packageinstances>
</package3d>
<package3d name="RESC5750X229" urn="urn:adsk.eagle:package:3811212/1" type="model" library_version="47">
<description>CHIP, 5.7 X 5 X 2.29 mm body
&lt;p&gt;CHIP package with body size 5.7 X 5 X 2.29 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESC5750X229"/>
</packageinstances>
</package3d>
<package3d name="RESC6432X70" urn="urn:adsk.eagle:package:3811284/1" type="model" library_version="47">
<description>CHIP, 6.4 X 3.2 X 0.7 mm body
&lt;p&gt;CHIP package with body size 6.4 X 3.2 X 0.7 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESC6432X70"/>
</packageinstances>
</package3d>
<package3d name="RESC2037X52" urn="urn:adsk.eagle:package:3950966/1" type="model" library_version="47">
<description>CHIP, 2 X 3.75 X 0.52 mm body
&lt;p&gt;CHIP package with body size 2 X 3.75 X 0.52 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESC2037X52"/>
</packageinstances>
</package3d>
<package3d name="INDC1005X60" urn="urn:adsk.eagle:package:3810108/1" type="model" library_version="47">
<description>CHIP, 1 X 0.5 X 0.6 mm body
&lt;p&gt;CHIP package with body size 1 X 0.5 X 0.6 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="INDC1005X60"/>
</packageinstances>
</package3d>
<package3d name="INDC1608X95" urn="urn:adsk.eagle:package:3810118/1" type="model" library_version="47">
<description>CHIP, 1.6 X 0.8 X 0.95 mm body
&lt;p&gt;CHIP package with body size 1.6 X 0.8 X 0.95 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="INDC1608X95"/>
</packageinstances>
</package3d>
<package3d name="INDC2520X220" urn="urn:adsk.eagle:package:3810123/1" type="model" library_version="47">
<description>CHIP, 2.5 X 2 X 2.2 mm body
&lt;p&gt;CHIP package with body size 2.5 X 2 X 2.2 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="INDC2520X220"/>
</packageinstances>
</package3d>
<package3d name="INDC3225X135" urn="urn:adsk.eagle:package:3810128/1" type="model" library_version="47">
<description>CHIP, 3.2 X 2.5 X 1.35 mm body
&lt;p&gt;CHIP package with body size 3.2 X 2.5 X 1.35 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="INDC3225X135"/>
</packageinstances>
</package3d>
<package3d name="INDC4509X190" urn="urn:adsk.eagle:package:3810712/1" type="model" library_version="47">
<description>CHIP, 4.5 X 0.9 X 1.9 mm body
&lt;p&gt;CHIP package with body size 4.5 X 0.9 X 1.9 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="INDC4509X190"/>
</packageinstances>
</package3d>
<package3d name="INDC4532X175" urn="urn:adsk.eagle:package:3810715/1" type="model" library_version="47">
<description>CHIP, 4.5 X 3.2 X 1.75 mm body
&lt;p&gt;CHIP package with body size 4.5 X 3.2 X 1.75 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="INDC4532X175"/>
</packageinstances>
</package3d>
<package3d name="INDC5750X180" urn="urn:adsk.eagle:package:3810722/1" type="model" library_version="47">
<description>CHIP, 5.7 X 5 X 1.8 mm body
&lt;p&gt;CHIP package with body size 5.7 X 5 X 1.8 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="INDC5750X180"/>
</packageinstances>
</package3d>
<package3d name="INDC6350X200" urn="urn:adsk.eagle:package:3811095/1" type="model" library_version="47">
<description>CHIP, 6.3 X 5 X 2 mm body
&lt;p&gt;CHIP package with body size 6.3 X 5 X 2 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="INDC6350X200"/>
</packageinstances>
</package3d>
<package3d name="INDM4848X300" urn="urn:adsk.eagle:package:3837793/1" type="model" library_version="47">
<description>MOLDED BODY, 4.8 X 4.8 X 3 mm body
&lt;p&gt;MOLDED BODY package with body size 4.8 X 4.8 X 3 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="INDM4848X300"/>
</packageinstances>
</package3d>
<package3d name="INDM5050X430" urn="urn:adsk.eagle:package:3890169/1" type="model" library_version="47">
<description>MOLDED BODY, 5 X 5 X 4.3 mm body
&lt;p&gt;MOLDED BODY package with body size 5 X 5 X 4.3 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="INDM5050X430"/>
</packageinstances>
</package3d>
<package3d name="INDM4440X300" urn="urn:adsk.eagle:package:3890484/1" type="model" library_version="47">
<description>MOLDED BODY, 4.45 X 4.06 X 3 mm body
&lt;p&gt;MOLDED BODY package with body size 4.45 X 4.06 X 3 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="INDM4440X300"/>
</packageinstances>
</package3d>
<package3d name="INDM7070X450" urn="urn:adsk.eagle:package:3890494/1" type="model" library_version="47">
<description>MOLDED BODY, 7 X 7 X 4.5 mm body
&lt;p&gt;MOLDED BODY package with body size 7 X 7 X 4.5 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="INDM7070X450"/>
</packageinstances>
</package3d>
<package3d name="INDM4949X400" urn="urn:adsk.eagle:package:3904911/1" type="model" library_version="47">
<description>MOLDED BODY, 4.9 X 4.9 X 4 mm body
&lt;p&gt;MOLDED BODY package with body size 4.9 X 4.9 X 4 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="INDM4949X400"/>
</packageinstances>
</package3d>
<package3d name="INDM6363X500" urn="urn:adsk.eagle:package:3920872/1" type="model" library_version="47">
<description>MOLDED BODY, 6.3 X 6.3 X 5 mm body
&lt;p&gt;MOLDED BODY package with body size 6.3 X 6.3 X 5 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="INDM6363X500"/>
</packageinstances>
</package3d>
<package3d name="INDM238238X1016" urn="urn:adsk.eagle:package:3949005/1" type="model" library_version="47">
<description>MOLDED BODY, 23.88 X 23.88 X 10.16 mm body
&lt;p&gt;MOLDED BODY package with body size 23.88 X 23.88 X 10.16 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="INDM238238X1016"/>
</packageinstances>
</package3d>
<package3d name="INDM8080X400" urn="urn:adsk.eagle:package:3950981/1" type="model" library_version="47">
<description>MOLDED BODY, 8 X 8 X 4 mm body
&lt;p&gt;MOLDED BODY package with body size 8 X 8 X 4 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="INDM8080X400"/>
</packageinstances>
</package3d>
<package3d name="INDM125125X650" urn="urn:adsk.eagle:package:3950988/1" type="model" library_version="47">
<description>MOLDED BODY, 12.5 X 12.5 X 6.5 mm body
&lt;p&gt;MOLDED BODY package with body size 12.5 X 12.5 X 6.5 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="INDM125125X650"/>
</packageinstances>
</package3d>
<package3d name="CAPC1220X107" urn="urn:adsk.eagle:package:3793115/1" type="model" library_version="47">
<description>CHIP, 1.25 X 2 X 1.07 mm body
&lt;p&gt;CHIP package with body size 1.25 X 2 X 1.07 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC1220X107"/>
</packageinstances>
</package3d>
<package3d name="CAPC1632X168" urn="urn:adsk.eagle:package:3793131/1" type="model" library_version="47">
<description>CHIP, 1.6 X 3.2 X 1.68 mm body
&lt;p&gt;CHIP package with body size 1.6 X 3.2 X 1.68 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC1632X168"/>
</packageinstances>
</package3d>
<package3d name="CAPC2012X127" urn="urn:adsk.eagle:package:3793139/1" type="model" library_version="47">
<description>CHIP, 2.01 X 1.25 X 1.27 mm body
&lt;p&gt;CHIP package with body size 2.01 X 1.25 X 1.27 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC2012X127"/>
</packageinstances>
</package3d>
<package3d name="CAPC3215X168" urn="urn:adsk.eagle:package:3793145/1" type="model" library_version="47">
<description>CHIP, 3.2 X 1.5 X 1.68 mm body
&lt;p&gt;CHIP package with body size 3.2 X 1.5 X 1.68 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC3215X168"/>
</packageinstances>
</package3d>
<package3d name="CAPC3225X168" urn="urn:adsk.eagle:package:3793149/1" type="model" library_version="47">
<description>CHIP, 3.2 X 2.5 X 1.68 mm body
&lt;p&gt;CHIP package with body size 3.2 X 2.5 X 1.68 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC3225X168"/>
</packageinstances>
</package3d>
<package3d name="CAPC4520X168" urn="urn:adsk.eagle:package:3793154/1" type="model" library_version="47">
<description>CHIP, 4.5 X 2.03 X 1.68 mm body
&lt;p&gt;CHIP package with body size 4.5 X 2.03 X 1.68 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC4520X168"/>
</packageinstances>
</package3d>
<package3d name="CAPC4532X218" urn="urn:adsk.eagle:package:3793159/1" type="model" library_version="47">
<description>CHIP, 4.5 X 3.2 X 2.18 mm body
&lt;p&gt;CHIP package with body size 4.5 X 3.2 X 2.18 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC4532X218"/>
</packageinstances>
</package3d>
<package3d name="CAPC4564X218" urn="urn:adsk.eagle:package:3793166/1" type="model" library_version="47">
<description>CHIP, 4.5 X 6.4 X 2.18 mm body
&lt;p&gt;CHIP package with body size 4.5 X 6.4 X 2.18 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC4564X218"/>
</packageinstances>
</package3d>
<package3d name="CAPC5650X218" urn="urn:adsk.eagle:package:3793171/1" type="model" library_version="47">
<description>CHIP, 5.64 X 5.08 X 2.18 mm body
&lt;p&gt;CHIP package with body size 5.64 X 5.08 X 2.18 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC5650X218"/>
</packageinstances>
</package3d>
<package3d name="CAPC5563X218" urn="urn:adsk.eagle:package:3793174/1" type="model" library_version="47">
<description>CHIP, 5.59 X 6.35 X 2.18 mm body
&lt;p&gt;CHIP package with body size 5.59 X 6.35 X 2.18 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC5563X218"/>
</packageinstances>
</package3d>
<package3d name="CAPC9198X218" urn="urn:adsk.eagle:package:3793178/1" type="model" library_version="47">
<description>CHIP, 9.14 X 9.82 X 2.18 mm body
&lt;p&gt;CHIP package with body size 9.14 X 9.82 X 2.18 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC9198X218"/>
</packageinstances>
</package3d>
<package3d name="SOIC127P800X203-8" urn="urn:adsk.eagle:package:3852771/1" type="model" library_version="47">
<description>8-SOIC, 1.27 mm pitch, 8.005 mm span, 5.23 X 5.255 X 2.03 mm body
&lt;p&gt;8-pin SOIC package with 1.27 mm pitch, 8.005 mm span with body size 5.23 X 5.255 X 2.03 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="SOIC127P800X203-8"/>
</packageinstances>
</package3d>
<package3d name="OSCCC320X250X80" urn="urn:adsk.eagle:package:3849546/1" type="model" library_version="47">
<description>OSCILLATOR CORNERCONCAVE, 3.2 X 2.5 X 0.8 mm body
&lt;p&gt;OSCILLATOR CORNERCONCAVE package with body size 3.2 X 2.5 X 0.8 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="OSCCC320X250X80"/>
</packageinstances>
</package3d>
<package3d name="LEDC1608X55" urn="urn:adsk.eagle:package:3905179/3" type="model" library_version="47">
<description>CHIP, 1.6 X 0.85 X 0.55 mm body
&lt;p&gt;CHIP package with body size 1.6 X 0.85 X 0.55 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="LEDC1608X55"/>
</packageinstances>
</package3d>
<package3d name="LEDC2012X70" urn="urn:adsk.eagle:package:3905183/4" type="box" library_version="47">
<description>CHIP, 2 X 1.25 X 0.7 mm body
&lt;p&gt;CHIP package with body size 2 X 1.25 X 0.7 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="LEDC2012X70"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="PIN" urn="urn:adsk.eagle:symbol:8413622/1" library_version="40" library_locally_modified="yes">
<pin name="1" x="-5.08" y="0" visible="off"/>
<wire x1="-3.302" y1="0" x2="-1.27" y2="0" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="0" x2="2.54" y2="0" width="0.8128" layer="94"/>
<text x="-3.302" y="0.508" size="1.27" layer="95" ratio="15">&gt;NAME</text>
</symbol>
<symbol name="TP" urn="urn:adsk.eagle:symbol:8413657/1" library_version="40" library_locally_modified="yes">
<wire x1="0.762" y1="1.778" x2="0" y2="0.254" width="0.254" layer="94"/>
<wire x1="0" y1="0.254" x2="-0.762" y2="1.778" width="0.254" layer="94"/>
<wire x1="-0.762" y1="1.778" x2="0.762" y2="1.778" width="0.254" layer="94" curve="-180"/>
<text x="-1.27" y="3.81" size="1.778" layer="95">&gt;NAME</text>
<pin name="PP" x="0" y="0" visible="off" length="short" direction="in" rot="R90"/>
</symbol>
<symbol name="RESISTOR" urn="urn:adsk.eagle:symbol:8413611/1" library_version="40" library_locally_modified="yes">
<text x="-2.54" y="1.524" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.2032" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.2032" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.2032" layer="94"/>
</symbol>
<symbol name="INDUCTOR" urn="urn:adsk.eagle:symbol:8413612/1" library_version="40" library_locally_modified="yes">
<text x="-2.54" y="1.524" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="-1.27" y1="0" x2="0" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="0" y1="0" x2="1.27" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.254" layer="94" curve="-180"/>
</symbol>
<symbol name="ATMEL_ATSAMD51J-100" urn="urn:adsk.eagle:symbol:8413619/1" library_version="40" library_locally_modified="yes">
<pin name="1" x="-45.72" y="30.48" visible="pad" length="middle"/>
<pin name="2" x="-45.72" y="27.94" visible="pad" length="middle"/>
<pin name="3" x="-45.72" y="25.4" visible="pad" length="middle"/>
<pin name="4" x="-45.72" y="22.86" visible="pad" length="middle"/>
<pin name="5" x="-45.72" y="20.32" visible="pad" length="middle"/>
<pin name="6" x="-45.72" y="17.78" visible="pad" length="middle"/>
<pin name="7" x="-45.72" y="15.24" visible="pad" length="middle"/>
<pin name="8" x="-45.72" y="12.7" visible="pad" length="middle"/>
<pin name="9" x="-45.72" y="10.16" visible="pad" length="middle"/>
<pin name="10" x="-45.72" y="7.62" visible="pad" length="middle"/>
<pin name="11" x="-45.72" y="5.08" visible="pad" length="middle"/>
<pin name="12" x="-45.72" y="2.54" visible="pad" length="middle"/>
<pin name="13" x="-45.72" y="0" visible="pad" length="middle"/>
<pin name="14" x="-45.72" y="-2.54" visible="pad" length="middle"/>
<pin name="15" x="-45.72" y="-5.08" visible="pad" length="middle"/>
<pin name="16" x="-45.72" y="-7.62" visible="pad" length="middle"/>
<pin name="17" x="-45.72" y="-10.16" visible="pad" length="middle"/>
<pin name="18" x="-45.72" y="-12.7" visible="pad" length="middle"/>
<pin name="19" x="-45.72" y="-15.24" visible="pad" length="middle"/>
<pin name="20" x="-45.72" y="-17.78" visible="pad" length="middle"/>
<pin name="21" x="-45.72" y="-20.32" visible="pad" length="middle"/>
<pin name="22" x="-45.72" y="-22.86" visible="pad" length="middle"/>
<pin name="23" x="-45.72" y="-25.4" visible="pad" length="middle"/>
<pin name="24" x="-45.72" y="-27.94" visible="pad" length="middle"/>
<pin name="25" x="-45.72" y="-30.48" visible="pad" length="middle"/>
<pin name="26" x="-30.48" y="-45.72" visible="pad" length="middle" rot="R90"/>
<pin name="27" x="-27.94" y="-45.72" visible="pad" length="middle" rot="R90"/>
<pin name="28" x="-25.4" y="-45.72" visible="pad" length="middle" rot="R90"/>
<pin name="29" x="-22.86" y="-45.72" visible="pad" length="middle" rot="R90"/>
<pin name="30" x="-20.32" y="-45.72" visible="pad" length="middle" rot="R90"/>
<pin name="31" x="-17.78" y="-45.72" visible="pad" length="middle" rot="R90"/>
<pin name="32" x="-15.24" y="-45.72" visible="pad" length="middle" rot="R90"/>
<pin name="33" x="-12.7" y="-45.72" visible="pad" length="middle" rot="R90"/>
<pin name="34" x="-10.16" y="-45.72" visible="pad" length="middle" rot="R90"/>
<pin name="35" x="-7.62" y="-45.72" visible="pad" length="middle" rot="R90"/>
<pin name="36" x="-5.08" y="-45.72" visible="pad" length="middle" rot="R90"/>
<pin name="37" x="-2.54" y="-45.72" visible="pad" length="middle" rot="R90"/>
<pin name="38" x="0" y="-45.72" visible="pad" length="middle" rot="R90"/>
<pin name="39" x="2.54" y="-45.72" visible="pad" length="middle" rot="R90"/>
<pin name="40" x="5.08" y="-45.72" visible="pad" length="middle" rot="R90"/>
<pin name="41" x="7.62" y="-45.72" visible="pad" length="middle" rot="R90"/>
<pin name="42" x="10.16" y="-45.72" visible="pad" length="middle" rot="R90"/>
<pin name="43" x="12.7" y="-45.72" visible="pad" length="middle" rot="R90"/>
<pin name="44" x="15.24" y="-45.72" visible="pad" length="middle" rot="R90"/>
<pin name="45" x="17.78" y="-45.72" visible="pad" length="middle" rot="R90"/>
<pin name="46" x="20.32" y="-45.72" visible="pad" length="middle" rot="R90"/>
<pin name="47" x="22.86" y="-45.72" visible="pad" length="middle" rot="R90"/>
<pin name="48" x="25.4" y="-45.72" visible="pad" length="middle" rot="R90"/>
<pin name="49" x="27.94" y="-45.72" visible="pad" length="middle" rot="R90"/>
<pin name="50" x="30.48" y="-45.72" visible="pad" length="middle" rot="R90"/>
<pin name="51" x="45.72" y="-30.48" visible="pad" length="middle" rot="R180"/>
<pin name="52" x="45.72" y="-27.94" visible="pad" length="middle" rot="R180"/>
<pin name="53" x="45.72" y="-25.4" visible="pad" length="middle" rot="R180"/>
<pin name="54" x="45.72" y="-22.86" visible="pad" length="middle" rot="R180"/>
<pin name="55" x="45.72" y="-20.32" visible="pad" length="middle" rot="R180"/>
<pin name="56" x="45.72" y="-17.78" visible="pad" length="middle" rot="R180"/>
<pin name="57" x="45.72" y="-15.24" visible="pad" length="middle" rot="R180"/>
<pin name="58" x="45.72" y="-12.7" visible="pad" length="middle" rot="R180"/>
<pin name="59" x="45.72" y="-10.16" visible="pad" length="middle" rot="R180"/>
<pin name="60" x="45.72" y="-7.62" visible="pad" length="middle" rot="R180"/>
<pin name="61" x="45.72" y="-5.08" visible="pad" length="middle" rot="R180"/>
<pin name="62" x="45.72" y="-2.54" visible="pad" length="middle" rot="R180"/>
<pin name="63" x="45.72" y="0" visible="pad" length="middle" rot="R180"/>
<pin name="64" x="45.72" y="2.54" visible="pad" length="middle" rot="R180"/>
<pin name="65" x="45.72" y="5.08" visible="pad" length="middle" rot="R180"/>
<pin name="66" x="45.72" y="7.62" visible="pad" length="middle" rot="R180"/>
<pin name="67" x="45.72" y="10.16" visible="pad" length="middle" rot="R180"/>
<pin name="68" x="45.72" y="12.7" visible="pad" length="middle" rot="R180"/>
<pin name="69" x="45.72" y="15.24" visible="pad" length="middle" rot="R180"/>
<pin name="70" x="45.72" y="17.78" visible="pad" length="middle" rot="R180"/>
<pin name="71" x="45.72" y="20.32" visible="pad" length="middle" rot="R180"/>
<pin name="72" x="45.72" y="22.86" visible="pad" length="middle" rot="R180"/>
<pin name="73" x="45.72" y="25.4" visible="pad" length="middle" rot="R180"/>
<pin name="74" x="45.72" y="27.94" visible="pad" length="middle" rot="R180"/>
<pin name="75" x="45.72" y="30.48" visible="pad" length="middle" rot="R180"/>
<pin name="76" x="30.48" y="45.72" visible="pad" length="middle" rot="R270"/>
<pin name="77" x="27.94" y="45.72" visible="pad" length="middle" rot="R270"/>
<pin name="78" x="25.4" y="45.72" visible="pad" length="middle" rot="R270"/>
<pin name="79" x="22.86" y="45.72" visible="pad" length="middle" rot="R270"/>
<pin name="80" x="20.32" y="45.72" visible="pad" length="middle" rot="R270"/>
<pin name="81" x="17.78" y="45.72" visible="pad" length="middle" rot="R270"/>
<pin name="82" x="15.24" y="45.72" visible="pad" length="middle" rot="R270"/>
<pin name="83" x="12.7" y="45.72" visible="pad" length="middle" rot="R270"/>
<pin name="84" x="10.16" y="45.72" visible="pad" length="middle" rot="R270"/>
<pin name="85" x="7.62" y="45.72" visible="pad" length="middle" rot="R270"/>
<pin name="86" x="5.08" y="45.72" visible="pad" length="middle" rot="R270"/>
<pin name="87" x="2.54" y="45.72" visible="pad" length="middle" rot="R270"/>
<pin name="88" x="0" y="45.72" visible="pad" length="middle" rot="R270"/>
<pin name="89" x="-2.54" y="45.72" visible="pad" length="middle" rot="R270"/>
<pin name="90" x="-5.08" y="45.72" visible="pad" length="middle" rot="R270"/>
<pin name="91" x="-7.62" y="45.72" visible="pad" length="middle" rot="R270"/>
<pin name="92" x="-10.16" y="45.72" visible="pad" length="middle" rot="R270"/>
<pin name="93" x="-12.7" y="45.72" visible="pad" length="middle" rot="R270"/>
<pin name="94" x="-15.24" y="45.72" visible="pad" length="middle" rot="R270"/>
<pin name="95" x="-17.78" y="45.72" visible="pad" length="middle" rot="R270"/>
<pin name="96" x="-20.32" y="45.72" visible="pad" length="middle" rot="R270"/>
<pin name="97" x="-22.86" y="45.72" visible="pad" length="middle" rot="R270"/>
<pin name="98" x="-25.4" y="45.72" visible="pad" length="middle" rot="R270"/>
<pin name="99" x="-27.94" y="45.72" visible="pad" length="middle" rot="R270"/>
<pin name="100" x="-30.48" y="45.72" visible="pad" length="middle" rot="R270"/>
<text x="-39.624" y="30.48" size="1.016" layer="94" ratio="15">PA00</text>
<text x="-39.624" y="27.686" size="1.016" layer="94" ratio="15">PA01</text>
<text x="-39.624" y="25.4" size="1.016" layer="94" ratio="15">PC00</text>
<text x="-39.624" y="22.86" size="1.016" layer="94" ratio="15">PC01</text>
<text x="-39.624" y="20.32" size="1.016" layer="94" ratio="15">PC02</text>
<text x="-39.624" y="17.78" size="1.016" layer="94" ratio="15">PC03</text>
<text x="-39.624" y="15.24" size="1.016" layer="94" ratio="15">PA02</text>
<text x="-39.624" y="12.7" size="1.016" layer="94" ratio="15">PA03</text>
<text x="-39.624" y="10.16" size="1.016" layer="94" ratio="15">PB04</text>
<text x="-39.624" y="7.62" size="1.016" layer="94" ratio="15">PB05</text>
<text x="-39.624" y="5.08" size="1.016" layer="94" ratio="15">GNDANA</text>
<text x="-39.624" y="2.54" size="1.016" layer="94" ratio="15">VDDANA</text>
<text x="-39.624" y="0" size="1.016" layer="94" ratio="15">PB06</text>
<text x="-39.624" y="-2.54" size="1.016" layer="94" ratio="15">PB07</text>
<text x="-39.624" y="-5.08" size="1.016" layer="94" ratio="15">PB08</text>
<text x="-39.624" y="-7.62" size="1.016" layer="94" ratio="15">PB09</text>
<text x="-39.624" y="-10.16" size="1.016" layer="94" ratio="15">PA04</text>
<text x="-39.624" y="-12.7" size="1.016" layer="94" ratio="15">PA05</text>
<text x="-39.624" y="-15.24" size="1.016" layer="94" ratio="15">PA06</text>
<text x="-39.624" y="-17.78" size="1.016" layer="94" ratio="15">PA07</text>
<text x="-39.624" y="-20.32" size="1.016" layer="94" ratio="15">PC04</text>
<text x="-39.624" y="-22.86" size="1.016" layer="94" ratio="15">PC06</text>
<text x="-39.624" y="-25.4" size="1.016" layer="94" ratio="15">PC07</text>
<text x="-39.624" y="-27.94" size="1.016" layer="94" ratio="15">GND</text>
<text x="-39.624" y="-30.48" size="1.016" layer="94" ratio="15">VDDIOB</text>
<text x="-30.48" y="-39.624" size="1.016" layer="94" ratio="15" rot="R90">PA08</text>
<text x="-27.686" y="-39.624" size="1.016" layer="94" ratio="15" rot="R90">PA09</text>
<text x="-25.4" y="-39.624" size="1.016" layer="94" ratio="15" rot="R90">PA10</text>
<text x="-22.86" y="-39.624" size="1.016" layer="94" ratio="15" rot="R90">PA11</text>
<text x="-20.32" y="-39.624" size="1.016" layer="94" ratio="15" rot="R90">VDDIOB</text>
<text x="-17.78" y="-39.624" size="1.016" layer="94" ratio="15" rot="R90">GND</text>
<text x="-15.24" y="-39.624" size="1.016" layer="94" ratio="15" rot="R90">PB10</text>
<text x="-12.7" y="-39.624" size="1.016" layer="94" ratio="15" rot="R90">PB11</text>
<text x="-10.16" y="-39.624" size="1.016" layer="94" ratio="15" rot="R90">PB12</text>
<text x="-7.62" y="-39.624" size="1.016" layer="94" ratio="15" rot="R90">PB13</text>
<text x="-5.08" y="-39.624" size="1.016" layer="94" ratio="15" rot="R90">PB14</text>
<text x="-2.54" y="-39.624" size="1.016" layer="94" ratio="15" rot="R90">PB15</text>
<text x="0" y="-39.624" size="1.016" layer="94" ratio="15" rot="R90">GND</text>
<text x="2.54" y="-39.624" size="1.016" layer="94" ratio="15" rot="R90">VDDIO</text>
<text x="5.08" y="-39.624" size="1.016" layer="94" ratio="15" rot="R90">PC10</text>
<text x="7.62" y="-39.624" size="1.016" layer="94" ratio="15" rot="R90">PC11</text>
<text x="10.16" y="-39.624" size="1.016" layer="94" ratio="15" rot="R90">PC12</text>
<text x="12.7" y="-39.624" size="1.016" layer="94" ratio="15" rot="R90">PC13</text>
<text x="15.24" y="-39.624" size="1.016" layer="94" ratio="15" rot="R90">PC14</text>
<text x="17.78" y="-39.624" size="1.016" layer="94" ratio="15" rot="R90">PC15</text>
<text x="20.32" y="-39.624" size="1.016" layer="94" ratio="15" rot="R90">PA12</text>
<text x="22.86" y="-39.624" size="1.016" layer="94" ratio="15" rot="R90">PA13</text>
<text x="25.4" y="-39.624" size="1.016" layer="94" ratio="15" rot="R90">PA14</text>
<text x="27.94" y="-39.624" size="1.016" layer="94" ratio="15" rot="R90">PA15</text>
<text x="30.48" y="-39.624" size="1.016" layer="94" ratio="15" rot="R90">GND</text>
<text x="39.624" y="-30.48" size="1.016" layer="94" ratio="15" rot="R180">VDDIO</text>
<text x="39.624" y="-27.686" size="1.016" layer="94" ratio="15" rot="R180">PA16</text>
<text x="39.624" y="-25.4" size="1.016" layer="94" ratio="15" rot="R180">PA17</text>
<text x="39.624" y="-22.86" size="1.016" layer="94" ratio="15" rot="R180">PA18</text>
<text x="39.624" y="-20.32" size="1.016" layer="94" ratio="15" rot="R180">PA19</text>
<text x="39.624" y="-17.78" size="1.016" layer="94" ratio="15" rot="R180">PC16</text>
<text x="39.624" y="-15.24" size="1.016" layer="94" ratio="15" rot="R180">PC17</text>
<text x="39.624" y="-12.7" size="1.016" layer="94" ratio="15" rot="R180">PC18</text>
<text x="39.624" y="-10.16" size="1.016" layer="94" ratio="15" rot="R180">PC19</text>
<text x="39.624" y="-7.62" size="1.016" layer="94" ratio="15" rot="R180">PC20</text>
<text x="39.624" y="-5.08" size="1.016" layer="94" ratio="15" rot="R180">PC21</text>
<text x="39.624" y="-2.54" size="1.016" layer="94" ratio="15" rot="R180">GND</text>
<text x="39.624" y="0" size="1.016" layer="94" ratio="15" rot="R180">VDDIO</text>
<text x="39.624" y="2.54" size="1.016" layer="94" ratio="15" rot="R180">PB16</text>
<text x="39.624" y="5.08" size="1.016" layer="94" ratio="15" rot="R180">PB17</text>
<text x="39.624" y="7.62" size="1.016" layer="94" ratio="15" rot="R180">PB18</text>
<text x="39.624" y="10.16" size="1.016" layer="94" ratio="15" rot="R180">PB19</text>
<text x="39.624" y="12.7" size="1.016" layer="94" ratio="15" rot="R180">PB20</text>
<text x="39.624" y="15.24" size="1.016" layer="94" ratio="15" rot="R180">PB21</text>
<text x="39.624" y="17.78" size="1.016" layer="94" ratio="15" rot="R180">PA20</text>
<text x="39.624" y="20.32" size="1.016" layer="94" ratio="15" rot="R180">PA21</text>
<text x="39.624" y="22.86" size="1.016" layer="94" ratio="15" rot="R180">PA22</text>
<text x="39.624" y="25.4" size="1.016" layer="94" ratio="15" rot="R180">PA23</text>
<text x="39.624" y="27.94" size="1.016" layer="94" ratio="15" rot="R180">PA24</text>
<text x="39.624" y="30.48" size="1.016" layer="94" ratio="15" rot="R180">PA25</text>
<text x="30.48" y="39.624" size="1.016" layer="94" ratio="15" rot="R270">GND</text>
<text x="27.686" y="39.624" size="1.016" layer="94" ratio="15" rot="R270">VDDIO</text>
<text x="25.4" y="39.624" size="1.016" layer="94" ratio="15" rot="R270">PB22</text>
<text x="22.86" y="39.624" size="1.016" layer="94" ratio="15" rot="R270">PB23</text>
<text x="20.32" y="39.624" size="1.016" layer="94" ratio="15" rot="R270">PB24</text>
<text x="17.78" y="39.624" size="1.016" layer="94" ratio="15" rot="R270">PB25</text>
<text x="15.24" y="39.624" size="1.016" layer="94" ratio="15" rot="R270">PC24</text>
<text x="12.7" y="39.624" size="1.016" layer="94" ratio="15" rot="R270">PC25</text>
<text x="10.16" y="39.624" size="1.016" layer="94" ratio="15" rot="R270">PC26</text>
<text x="7.62" y="39.624" size="1.016" layer="94" ratio="15" rot="R270">PC27</text>
<text x="5.08" y="39.624" size="1.016" layer="94" ratio="15" rot="R270">PC28</text>
<text x="2.54" y="39.624" size="1.016" layer="94" ratio="15" rot="R270">PA27</text>
<text x="0" y="39.624" size="1.016" layer="94" ratio="15" rot="R270">RESETN</text>
<text x="-2.54" y="39.624" size="1.016" layer="94" ratio="15" rot="R270">VDDCORE</text>
<text x="-5.08" y="39.624" size="1.016" layer="94" ratio="15" rot="R270">GND</text>
<text x="-7.62" y="39.624" size="1.016" layer="94" ratio="15" rot="R270">VSW</text>
<text x="-10.16" y="39.624" size="1.016" layer="94" ratio="15" rot="R270">VDDIO</text>
<text x="-12.7" y="39.624" size="1.016" layer="94" ratio="15" rot="R270">PA30</text>
<text x="-15.24" y="39.624" size="1.016" layer="94" ratio="15" rot="R270">PA31</text>
<text x="-17.78" y="39.624" size="1.016" layer="94" ratio="15" rot="R270">PB30</text>
<text x="-20.32" y="39.624" size="1.016" layer="94" ratio="15" rot="R270">PB31</text>
<text x="-22.86" y="39.624" size="1.016" layer="94" ratio="15" rot="R270">PB00</text>
<text x="-25.4" y="39.624" size="1.016" layer="94" ratio="15" rot="R270">PB01</text>
<text x="-27.94" y="39.624" size="1.016" layer="94" ratio="15" rot="R270">PB02</text>
<text x="-30.48" y="39.624" size="1.016" layer="94" ratio="15" rot="R270">PB03</text>
<text x="-40.64" y="55.88" size="1.778" layer="95">&gt;NAME</text>
<text x="-40.64" y="53.34" size="1.778" layer="96">&gt;VALUE</text>
<wire x1="-40.64" y1="35.56" x2="-40.64" y2="-40.64" width="0.254" layer="94"/>
<wire x1="-40.64" y1="-40.64" x2="40.64" y2="-40.64" width="0.254" layer="94"/>
<wire x1="40.64" y1="-40.64" x2="40.64" y2="40.64" width="0.254" layer="94"/>
<wire x1="40.64" y1="40.64" x2="-35.56" y2="40.64" width="0.254" layer="94"/>
<wire x1="-35.56" y1="40.64" x2="-40.64" y2="35.56" width="0.254" layer="94"/>
</symbol>
<symbol name="CAPACITOR" urn="urn:adsk.eagle:symbol:8413617/1" library_version="40" library_locally_modified="yes">
<wire x1="-2.54" y1="0" x2="-0.762" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="0.762" y2="0" width="0.1524" layer="94"/>
<text x="-2.54" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-4.318" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-1.524" y1="-0.254" x2="2.54" y2="0.254" layer="94" rot="R90"/>
<rectangle x1="-2.54" y1="-0.254" x2="1.524" y2="0.254" layer="94" rot="R90"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="EEPROM_8PIN" urn="urn:adsk.eagle:symbol:8413595/1" library_version="40" library_locally_modified="yes">
<text x="0.508" y="-5.08" size="1.27" layer="94">!CS</text>
<text x="0.508" y="-7.62" size="1.27" layer="94">SO</text>
<text x="0.508" y="-10.16" size="1.27" layer="94">!WP</text>
<text x="0.508" y="-12.7" size="1.27" layer="94">VSS</text>
<text x="12.192" y="-12.7" size="1.27" layer="94" align="bottom-right">SI</text>
<text x="12.192" y="-10.16" size="1.27" layer="94" align="bottom-right">SCK</text>
<text x="12.192" y="-7.62" size="1.27" layer="94" align="bottom-right">!HOLD</text>
<text x="12.192" y="-5.08" size="1.27" layer="94" align="bottom-right">VCC</text>
<text x="-2.54" y="2.54" size="1.778" layer="95" ratio="15">&gt;NAME</text>
<text x="-2.54" y="0.508" size="1.778" layer="96" ratio="15">&gt;VALUE</text>
<wire x1="0" y1="-2.54" x2="0" y2="-15.24" width="0.254" layer="94"/>
<wire x1="0" y1="-15.24" x2="12.7" y2="-15.24" width="0.254" layer="94"/>
<wire x1="12.7" y1="-15.24" x2="12.7" y2="0" width="0.254" layer="94"/>
<wire x1="12.7" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="0" y2="-2.54" width="0.254" layer="94"/>
<pin name="1" x="-2.54" y="-5.08" visible="pad" length="short"/>
<pin name="2" x="-2.54" y="-7.62" visible="pad" length="short"/>
<pin name="3" x="-2.54" y="-10.16" visible="pad" length="short"/>
<pin name="4" x="-2.54" y="-12.7" visible="pad" length="short"/>
<pin name="5" x="15.24" y="-12.7" visible="pad" length="short" rot="R180"/>
<pin name="6" x="15.24" y="-10.16" visible="pad" length="short" rot="R180"/>
<pin name="7" x="15.24" y="-7.62" visible="pad" length="short" rot="R180"/>
<pin name="8" x="15.24" y="-5.08" visible="pad" length="short" rot="R180"/>
</symbol>
<symbol name="SWITCH" urn="urn:adsk.eagle:symbol:8413599/1" library_version="40" library_locally_modified="yes">
<pin name="1" x="-5.08" y="0" visible="pad" length="short"/>
<pin name="2" x="5.08" y="0" visible="pad" length="short" rot="R180"/>
<wire x1="-2.54" y1="0" x2="-1.016" y2="0" width="0.254" layer="94"/>
<wire x1="-1.016" y1="0" x2="0.508" y2="1.524" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="1.016" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="2.54" size="1.016" layer="95" font="vector" ratio="15">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.016" layer="96" font="vector" ratio="15">&gt;VALUE</text>
</symbol>
<symbol name="CRYSTAL_3-POS" urn="urn:adsk.eagle:symbol:8413601/1" library_version="40" library_locally_modified="yes">
<wire x1="1.016" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.016" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.381" y1="1.524" x2="-0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="-0.381" y1="-1.524" x2="0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="-1.524" x2="0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="1.524" x2="-0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="1.016" y1="1.778" x2="1.016" y2="0" width="0.254" layer="94"/>
<wire x1="1.016" y1="0" x2="1.016" y2="-1.778" width="0.254" layer="94"/>
<wire x1="-1.016" y1="1.778" x2="-1.016" y2="0" width="0.254" layer="94"/>
<wire x1="-1.016" y1="0" x2="-1.016" y2="-1.778" width="0.254" layer="94"/>
<wire x1="-1.778" y1="-1.524" x2="-1.778" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-1.778" y1="-2.54" x2="1.778" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.778" y1="-2.54" x2="1.778" y2="-1.524" width="0.254" layer="94"/>
<wire x1="-1.778" y1="2.54" x2="1.778" y2="2.54" width="0.254" layer="94"/>
<wire x1="1.778" y1="2.54" x2="1.778" y2="1.524" width="0.254" layer="94"/>
<wire x1="-1.778" y1="2.54" x2="-1.778" y2="1.524" width="0.254" layer="94"/>
<text x="2.54" y="2.54" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="2.54" y="4.064" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="1"/>
<pin name="3" x="0" y="-5.08" visible="pad" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
<symbol name="LED" urn="urn:adsk.eagle:symbol:8413629/1" library_version="41" library_locally_modified="yes">
<wire x1="-1.016" y1="-1.27" x2="1.524" y2="0" width="0.254" layer="94"/>
<wire x1="1.524" y1="0" x2="-1.016" y2="1.27" width="0.254" layer="94"/>
<wire x1="-1.016" y1="1.27" x2="-1.016" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.524" y1="1.27" x2="1.524" y2="0" width="0.254" layer="94"/>
<wire x1="1.524" y1="0" x2="1.524" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-0.635" y1="1.143" x2="0.635" y2="2.413" width="0.1524" layer="94"/>
<wire x1="0.635" y1="2.413" x2="-0.127" y2="2.286" width="0.1524" layer="94"/>
<wire x1="0.635" y1="2.413" x2="0.508" y2="1.651" width="0.1524" layer="94"/>
<wire x1="0.254" y1="0.762" x2="1.27" y2="1.778" width="0.1524" layer="94"/>
<wire x1="1.27" y1="1.778" x2="0.508" y2="1.651" width="0.1524" layer="94"/>
<wire x1="1.27" y1="1.778" x2="1.143" y2="1.016" width="0.1524" layer="94"/>
<pin name="A" x="-3.556" y="0" visible="off" length="short" direction="pas"/>
<pin name="K" x="4.064" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<text x="-3.556" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.556" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="TC2050-IDC-NL" urn="urn:adsk.eagle:component:16170707/2" prefix="J" uservalue="yes" library_version="47">
<description>Cbl Plug-Of-Nails 10-Pin</description>
<gates>
<gate name="-1" symbol="PIN" x="5.08" y="12.7"/>
<gate name="-2" symbol="PIN" x="5.08" y="10.16"/>
<gate name="-3" symbol="PIN" x="5.08" y="7.62"/>
<gate name="-4" symbol="PIN" x="5.08" y="5.08"/>
<gate name="-5" symbol="PIN" x="5.08" y="2.54"/>
<gate name="-6" symbol="PIN" x="5.08" y="0"/>
<gate name="-7" symbol="PIN" x="5.08" y="-2.54"/>
<gate name="-8" symbol="PIN" x="5.08" y="-5.08"/>
<gate name="-9" symbol="PIN" x="5.08" y="-7.62"/>
<gate name="-10" symbol="PIN" x="5.08" y="-10.16"/>
</gates>
<devices>
<device name="" package="TAG-CONNECT_TC2050-IDC-NL">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-10" pin="1" pad="10"/>
<connect gate="-2" pin="1" pad="2"/>
<connect gate="-3" pin="1" pad="3"/>
<connect gate="-4" pin="1" pad="4"/>
<connect gate="-5" pin="1" pad="5"/>
<connect gate="-6" pin="1" pad="6"/>
<connect gate="-7" pin="1" pad="7"/>
<connect gate="-8" pin="1" pad="8"/>
<connect gate="-9" pin="1" pad="9"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:16170706/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="AVAILABILITY" value="Unavailable"/>
<attribute name="DESCRIPTION" value="  Plug-Of-Nails™ Adapter Cable "/>
<attribute name="MF" value="Tag-Connect LLC"/>
<attribute name="MP" value="TC2050-IDC-NL"/>
<attribute name="PACKAGE" value="None"/>
<attribute name="PRICE" value="None"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TP_CLASS_B" urn="urn:adsk.eagle:component:8414518/4" prefix="TP" uservalue="yes" library_version="40" library_locally_modified="yes">
<gates>
<gate name="G$1" symbol="TP" x="0" y="0"/>
</gates>
<devices>
<device name="TP_1.5MM" package="TP_1.5MM">
<connects>
<connect gate="G$1" pin="PP" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414069/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TP_1.5MM_PAD" package="TP_1.5MM_PAD">
<connects>
<connect gate="G$1" pin="PP" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414070/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TP_1.7MM" package="TP_1.7MM">
<connects>
<connect gate="G$1" pin="PP" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414071/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C120H40" package="C120H40">
<connects>
<connect gate="G$1" pin="PP" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414063/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C131H51" package="C131H51">
<connects>
<connect gate="G$1" pin="PP" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414064/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C406H326" package="C406H326">
<connects>
<connect gate="G$1" pin="PP" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414065/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C491H411" package="C491H411">
<connects>
<connect gate="G$1" pin="PP" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414066/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CIR_PAD_1MM" package="CIR_PAD_1MM">
<connects>
<connect gate="G$1" pin="PP" pad="P$1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414067/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CIR_PAD_2MM" package="CIR_PAD_2MM">
<connects>
<connect gate="G$1" pin="PP" pad="P$1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414068/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RESISTOR" urn="urn:adsk.eagle:component:8414463/1" prefix="R" uservalue="yes" library_version="47">
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="0201" package="RESC0603X30">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3811151/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402" package="RESC1005X40">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3811154/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="RESC1608X55">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3811166/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="RESC2012X70">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3811169/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="RESC3115X70">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3811173/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1210" package="RESC3225X70">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3811180/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1812" package="RESC4532X70">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3811188/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2010" package="RESC5025X70">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3811197/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2220" package="RESC5750X229">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3811212/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2512" package="RESC6432X70">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3811284/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1508_WIDE" package="RESC2037X52">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3950966/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="INDUCTOR" urn="urn:adsk.eagle:component:8414465/1" prefix="L" uservalue="yes" library_version="47">
<gates>
<gate name="G$1" symbol="INDUCTOR" x="0" y="0"/>
</gates>
<devices>
<device name="0402" package="INDC1005X60">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3810108/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="INDC1608X95">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3810118/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1008" package="INDC2520X220">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3810123/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1210" package="INDC3225X135">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3810128/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1806" package="INDC4509X190">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3810712/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1812" package="INDC4532X175">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3810715/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2220" package="INDC5750X180">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3810722/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2520" package="INDC6350X200">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3811095/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="4.80MM_X_4.80MM_SMT" package="INDM4848X300">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3837793/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5.00MM_X_5.00MM_SMT" package="INDM5050X430">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3890169/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="4.00MM_X_4.00MM_SMT" package="INDM4040X300">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3890461/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="4.45MM_X_4.06MM_SMT" package="INDM4440X300">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3890484/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="7.00MM_X_7.00MM_SMT" package="INDM7070X450">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3890494/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="4.90MM_X_4.90MM_SMT" package="INDM4949X400">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3904911/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="RADIAL_THT" package="RADIAL_THT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414048/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="6.30MM_X_6.30MM_SMT" package="INDM6363X500">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3920872/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="23.88MM_X_23.88MM_SMT" package="INDM238238X1016">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3949005/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="8.00MM_X_8.00MM_SMT" package="INDM8080X400">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3950981/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="12.5MM_X_12.5MM_SMT" package="INDM125125X650">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3950988/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ATMEL_ATSAMD51N19" urn="urn:adsk.eagle:component:8414472/1" prefix="U" uservalue="yes" library_version="47">
<gates>
<gate name="G$1" symbol="ATMEL_ATSAMD51J-100" x="0" y="0"/>
</gates>
<devices>
<device name="TQFP-100" package="QFP50P1600X1600X120-100">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="100" pad="100"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="13" pad="13"/>
<connect gate="G$1" pin="14" pad="14"/>
<connect gate="G$1" pin="15" pad="15"/>
<connect gate="G$1" pin="16" pad="16"/>
<connect gate="G$1" pin="17" pad="17"/>
<connect gate="G$1" pin="18" pad="18"/>
<connect gate="G$1" pin="19" pad="19"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="20" pad="20"/>
<connect gate="G$1" pin="21" pad="21"/>
<connect gate="G$1" pin="22" pad="22"/>
<connect gate="G$1" pin="23" pad="23"/>
<connect gate="G$1" pin="24" pad="24"/>
<connect gate="G$1" pin="25" pad="25"/>
<connect gate="G$1" pin="26" pad="26"/>
<connect gate="G$1" pin="27" pad="27"/>
<connect gate="G$1" pin="28" pad="28"/>
<connect gate="G$1" pin="29" pad="29"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="30" pad="30"/>
<connect gate="G$1" pin="31" pad="31"/>
<connect gate="G$1" pin="32" pad="32"/>
<connect gate="G$1" pin="33" pad="33"/>
<connect gate="G$1" pin="34" pad="34"/>
<connect gate="G$1" pin="35" pad="35"/>
<connect gate="G$1" pin="36" pad="36"/>
<connect gate="G$1" pin="37" pad="37"/>
<connect gate="G$1" pin="38" pad="38"/>
<connect gate="G$1" pin="39" pad="39"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="40" pad="40"/>
<connect gate="G$1" pin="41" pad="41"/>
<connect gate="G$1" pin="42" pad="42"/>
<connect gate="G$1" pin="43" pad="43"/>
<connect gate="G$1" pin="44" pad="44"/>
<connect gate="G$1" pin="45" pad="45"/>
<connect gate="G$1" pin="46" pad="46"/>
<connect gate="G$1" pin="47" pad="47"/>
<connect gate="G$1" pin="48" pad="48"/>
<connect gate="G$1" pin="49" pad="49"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="50" pad="50"/>
<connect gate="G$1" pin="51" pad="51"/>
<connect gate="G$1" pin="52" pad="52"/>
<connect gate="G$1" pin="53" pad="53"/>
<connect gate="G$1" pin="54" pad="54"/>
<connect gate="G$1" pin="55" pad="55"/>
<connect gate="G$1" pin="56" pad="56"/>
<connect gate="G$1" pin="57" pad="57"/>
<connect gate="G$1" pin="58" pad="58"/>
<connect gate="G$1" pin="59" pad="59"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="60" pad="60"/>
<connect gate="G$1" pin="61" pad="61"/>
<connect gate="G$1" pin="62" pad="62"/>
<connect gate="G$1" pin="63" pad="63"/>
<connect gate="G$1" pin="64" pad="64"/>
<connect gate="G$1" pin="65" pad="65"/>
<connect gate="G$1" pin="66" pad="66"/>
<connect gate="G$1" pin="67" pad="67"/>
<connect gate="G$1" pin="68" pad="68"/>
<connect gate="G$1" pin="69" pad="69"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="70" pad="70"/>
<connect gate="G$1" pin="71" pad="71"/>
<connect gate="G$1" pin="72" pad="72"/>
<connect gate="G$1" pin="73" pad="73"/>
<connect gate="G$1" pin="74" pad="74"/>
<connect gate="G$1" pin="75" pad="75"/>
<connect gate="G$1" pin="76" pad="76"/>
<connect gate="G$1" pin="77" pad="77"/>
<connect gate="G$1" pin="78" pad="78"/>
<connect gate="G$1" pin="79" pad="79"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="80" pad="80"/>
<connect gate="G$1" pin="81" pad="81"/>
<connect gate="G$1" pin="82" pad="82"/>
<connect gate="G$1" pin="83" pad="83"/>
<connect gate="G$1" pin="84" pad="84"/>
<connect gate="G$1" pin="85" pad="85"/>
<connect gate="G$1" pin="86" pad="86"/>
<connect gate="G$1" pin="87" pad="87"/>
<connect gate="G$1" pin="88" pad="88"/>
<connect gate="G$1" pin="89" pad="89"/>
<connect gate="G$1" pin="9" pad="9"/>
<connect gate="G$1" pin="90" pad="90"/>
<connect gate="G$1" pin="91" pad="91"/>
<connect gate="G$1" pin="92" pad="92"/>
<connect gate="G$1" pin="93" pad="93"/>
<connect gate="G$1" pin="94" pad="94"/>
<connect gate="G$1" pin="95" pad="95"/>
<connect gate="G$1" pin="96" pad="96"/>
<connect gate="G$1" pin="97" pad="97"/>
<connect gate="G$1" pin="98" pad="98"/>
<connect gate="G$1" pin="99" pad="99"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3791942/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAPACITOR" urn="urn:adsk.eagle:component:8414470/2" prefix="C" uservalue="yes" library_version="47">
<gates>
<gate name="G$1" symbol="CAPACITOR" x="0" y="0"/>
</gates>
<devices>
<device name="0402" package="CAPC1005X60">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793108/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0508" package="CAPC1220X107">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793115/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="CAPC1608X55">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793126/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0612" package="CAPC1632X168">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793131/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="CAPC2012X127">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793139/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="CAPC3215X168">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793145/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1210" package="CAPC3225X168">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793149/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1808" package="CAPC4520X168">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793154/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1812" package="CAPC4532X218">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793159/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1825" package="CAPC4564X218">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793166/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2220" package="CAPC5650X218">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793171/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2225" package="CAPC5563X218">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793174/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3640" package="CAPC9198X218">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3793178/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="EEPROM_8PIN" urn="urn:adsk.eagle:component:8414432/1" prefix="U" uservalue="yes" library_version="47">
<gates>
<gate name="G$1" symbol="EEPROM_8PIN" x="0" y="0"/>
</gates>
<devices>
<device name="150-MIL" package="SOIC127P600X175-8">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3833977/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="208-MIL" package="SOIC127P800X203-8">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3852771/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CKSWITCHES_KXT3" urn="urn:adsk.eagle:component:8414442/1" prefix="SW" uservalue="yes" library_version="40" library_locally_modified="yes">
<gates>
<gate name="G$1" symbol="SWITCH" x="0" y="0"/>
</gates>
<devices>
<device name="KXT3" package="CKSWITCHES_KXT3">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414043/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CRYSTAL_3/4-POS" urn="urn:adsk.eagle:component:8414446/1" prefix="Y" uservalue="yes" library_version="47">
<gates>
<gate name="G$1" symbol="CRYSTAL_3-POS" x="0" y="0"/>
</gates>
<devices>
<device name="CYLINDRICAL_CAN_RADIAL" package="CRYSTAL_CYLINDRICAL_RADIAL">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414042/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="4-SMD_5.00MM_X_3.20MM" package="OSCCC500X320X110">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="3"/>
<connect gate="G$1" pin="3" pad="2 4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3849466/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="4-SMD_3.20MM_X_2.50MM" package="OSCCC320X250X80">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="3"/>
<connect gate="G$1" pin="3" pad="2 4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3849546/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LED" urn="urn:adsk.eagle:component:8414483/4" prefix="D" uservalue="yes" library_version="47">
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="LED_CUSTOM_0402/RADIAL" package="LED_CUSTOM_0402/RADIAL">
<connects>
<connect gate="G$1" pin="A" pad="A_1 A_2 A_3"/>
<connect gate="G$1" pin="K" pad="K_1 K_2 K_3"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414050/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="LEDC1608X55">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3905179/3"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="LEDC2012X70">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:3905183/4"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LED_RADIAL" package="LED_RADIAL">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8414056/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402" package="LEDC1005X60N">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="K" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15620674/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<groups>
<schematic_group name="PROGRAMMING_HEADER"/>
<schematic_group name="CRYSTAL_OSC"/>
<schematic_group name="PROCESSOR"/>
<schematic_group name="BIASING_CAPACITORS"/>
<schematic_group name="RESET"/>
<schematic_group name="CORE_BIASING_CAPS"/>
<schematic_group name="MEMORY"/>
</groups>
<parts>
<part name="U1" library="frames" library_urn="urn:adsk.eagle:library:229" deviceset="A3_LOC_JMA_RELEASE" device=""/>
<part name="SUPPLY1" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY2" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="C66" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="0.1uF"/>
<part name="C68" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="10nF"/>
<part name="C65" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="0.1uF"/>
<part name="C48" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="0.1uF"/>
<part name="C49" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="10nF"/>
<part name="C55" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="0.1uF"/>
<part name="C56" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="10nF"/>
<part name="C53" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="0.1uF"/>
<part name="C52" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="10nF"/>
<part name="L1" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="INDUCTOR" device="4.00MM_X_4.00MM_SMT" package3d_urn="urn:adsk.eagle:package:3890461/1" value="10uH"/>
<part name="C69" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0603" package3d_urn="urn:adsk.eagle:package:3793126/2" value="DNP"/>
<part name="SUPPLY3" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY4" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="R130" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="100K"/>
<part name="R132" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="100"/>
<part name="C72" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="0.1uF"/>
<part name="SW1" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CKSWITCHES_KXT3" device="KXT3" package3d_urn="urn:adsk.eagle:package:8414043/1" value="KXT331LHS"/>
<part name="+3V3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="SUPPLY5" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="R136" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="100K"/>
<part name="+3V5" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="SUPPLY6" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY7" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="C46" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="0.1uF"/>
<part name="C44" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="10nF"/>
<part name="C43" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="0.1uF"/>
<part name="C42" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="10nF"/>
<part name="C47" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="0.1uF"/>
<part name="C40" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0603" package3d_urn="urn:adsk.eagle:package:3793126/2" value="4.7uF"/>
<part name="U11" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="ATMEL_ATSAMD51N19" device="TQFP-100" package3d_urn="urn:adsk.eagle:package:3791942/1" value="SAM51N19/SAMD51N20"/>
<part name="X2" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CRYSTAL_3/4-POS" device="4-SMD_5.00MM_X_3.20MM" package3d_urn="urn:adsk.eagle:package:3849466/1" value="ECS_ECX-53B-DU"/>
<part name="C71" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="20pF"/>
<part name="C64" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="20pF"/>
<part name="SUPPLY10" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="C75" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="0.1uF"/>
<part name="R96" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="0"/>
<part name="TP26" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TP_CLASS_B" device="CIR_PAD_1MM" package3d_urn="urn:adsk.eagle:package:8414067/2"/>
<part name="C62" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="10nF"/>
<part name="C60" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="0.1uF"/>
<part name="TP25" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TP_CLASS_B" device="CIR_PAD_1MM" package3d_urn="urn:adsk.eagle:package:8414067/2"/>
<part name="+3V9" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="TP27" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TP_CLASS_B" device="CIR_PAD_1MM" package3d_urn="urn:adsk.eagle:package:8414067/2"/>
<part name="C45" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="10nF"/>
<part name="C63" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="10nF"/>
<part name="R135" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="100K"/>
<part name="R129" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="DNP"/>
<part name="R128" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="100"/>
<part name="TP9" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TP_CLASS_B" device="CIR_PAD_1MM" package3d_urn="urn:adsk.eagle:package:8414067/2"/>
<part name="TP10" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TP_CLASS_B" device="CIR_PAD_1MM" package3d_urn="urn:adsk.eagle:package:8414067/2"/>
<part name="TP13" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TP_CLASS_B" device="CIR_PAD_1MM" package3d_urn="urn:adsk.eagle:package:8414067/2"/>
<part name="TP24" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TP_CLASS_B" device="CIR_PAD_1MM" package3d_urn="urn:adsk.eagle:package:8414067/2"/>
<part name="TP23" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TP_CLASS_B" device="CIR_PAD_1MM" package3d_urn="urn:adsk.eagle:package:8414067/2"/>
<part name="R121" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="DNP"/>
<part name="R122" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="DNP"/>
<part name="R123" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="DNP"/>
<part name="R124" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="DNP"/>
<part name="R111" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="DNP"/>
<part name="R112" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="DNP"/>
<part name="R113" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="DNP"/>
<part name="R114" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="DNP"/>
<part name="SUPPLY98" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="+3V4" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="TP21" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TP_CLASS_B" device="CIR_PAD_1MM" package3d_urn="urn:adsk.eagle:package:8414067/2"/>
<part name="TP22" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TP_CLASS_B" device="CIR_PAD_1MM" package3d_urn="urn:adsk.eagle:package:8414067/2"/>
<part name="TP19" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TP_CLASS_B" device="CIR_PAD_1MM" package3d_urn="urn:adsk.eagle:package:8414067/2"/>
<part name="TP20" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TP_CLASS_B" device="CIR_PAD_1MM" package3d_urn="urn:adsk.eagle:package:8414067/2"/>
<part name="TP17" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TP_CLASS_B" device="CIR_PAD_1MM" package3d_urn="urn:adsk.eagle:package:8414067/2"/>
<part name="TP16" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TP_CLASS_B" device="CIR_PAD_1MM" package3d_urn="urn:adsk.eagle:package:8414067/2"/>
<part name="TP15" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TP_CLASS_B" device="CIR_PAD_1MM" package3d_urn="urn:adsk.eagle:package:8414067/2"/>
<part name="TP14" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TP_CLASS_B" device="CIR_PAD_1MM" package3d_urn="urn:adsk.eagle:package:8414067/2"/>
<part name="TP12" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TP_CLASS_B" device="CIR_PAD_1MM" package3d_urn="urn:adsk.eagle:package:8414067/2"/>
<part name="TP11" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TP_CLASS_B" device="CIR_PAD_1MM" package3d_urn="urn:adsk.eagle:package:8414067/2"/>
<part name="R85" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="510"/>
<part name="R86" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="510"/>
<part name="R87" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="510"/>
<part name="R88" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="510"/>
<part name="SUPPLY99" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="R117" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="DNP"/>
<part name="R118" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="DNP"/>
<part name="R119" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="DNP"/>
<part name="R120" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="DNP"/>
<part name="R107" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="DNP"/>
<part name="R108" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="DNP"/>
<part name="R109" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="DNP"/>
<part name="R110" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="DNP"/>
<part name="SUPPLY100" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="+3V7" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="TP28" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TP_CLASS_B" device="CIR_PAD_1MM" package3d_urn="urn:adsk.eagle:package:8414067/2"/>
<part name="TP29" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TP_CLASS_B" device="CIR_PAD_1MM" package3d_urn="urn:adsk.eagle:package:8414067/2"/>
<part name="U5" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="EEPROM_8PIN" device="150-MIL" package3d_urn="urn:adsk.eagle:package:3833977/2" value="W25x40CL/MX25V8035FM1I "/>
<part name="R36" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="10K"/>
<part name="R37" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="DNP"/>
<part name="R38" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="10K"/>
<part name="R47" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="10K"/>
<part name="R48" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="DNP"/>
<part name="R49" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="RESISTOR" device="0402" package3d_urn="urn:adsk.eagle:package:3811154/1" value="10K"/>
<part name="SUPPLY8" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY9" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="C20" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="CAPACITOR" device="0402" package3d_urn="urn:adsk.eagle:package:3793108/1" value="0.1uF"/>
<part name="+3V1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="J1" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TC2050-IDC-NL" device="" package3d_urn="urn:adsk.eagle:package:16170706/2"/>
<part name="TP1" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="TP_CLASS_B" device="CIR_PAD_1MM" package3d_urn="urn:adsk.eagle:package:8414067/2"/>
<part name="D10" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="LED" device="0402" package3d_urn="urn:adsk.eagle:package:15620674/1"/>
<part name="D9" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="LED" device="0402" package3d_urn="urn:adsk.eagle:package:15620674/1"/>
<part name="D8" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="LED" device="0402" package3d_urn="urn:adsk.eagle:package:15620674/1"/>
<part name="D7" library="JMA_LBR_v2" library_urn="urn:adsk.eagle:library:8413580" deviceset="LED" device="0402" package3d_urn="urn:adsk.eagle:package:15620674/1"/>
</parts>
<sheets>
<sheet>
<plain>
<text x="287.02" y="35.56" size="1.27" layer="94">RBG</text>
<text x="312.42" y="17.78" size="1.27" layer="94">RET 2000 </text>
<text x="370.84" y="10.16" size="1.27" layer="94">0</text>
<text x="130.556" y="154.94" size="1.27" layer="97" rot="R90" grouprefs="PROCESSOR">Replace Inductor with 0 ohm
if Linear regualtors used.</text>
<text x="124.46" y="180.34" size="1.27" layer="97" grouprefs="PROCESSOR">LQM18PN1R0MFHD </text>
<text x="325.12" y="78.74" size="1.27" layer="97" grouprefs="PROGRAMMING_HEADER">Optional #LTMM-105-02-F-D</text>
<text x="102.616" y="10.414" size="1.27" layer="97" grouprefs="PROCESSOR">MotorDriver1</text>
<text x="153.67" y="10.414" size="1.27" layer="97" grouprefs="PROCESSOR">MotorDriver2</text>
<text x="231.14" y="91.44" size="1.27" layer="97" rot="R90" grouprefs="PROCESSOR">AISG_3</text>
<text x="231.14" y="73.66" size="1.27" layer="97" rot="R90" grouprefs="PROCESSOR">AISG_1</text>
<text x="238.76" y="210.82" size="1.27" layer="97">VDDIO</text>
<text x="120.396" y="8.382" size="1.27" layer="97" grouprefs="PROCESSOR">External Memory</text>
<text x="231.14" y="60.96" size="1.27" layer="97" rot="R90" grouprefs="PROCESSOR">AISG_2</text>
<text x="231.14" y="104.14" size="1.27" layer="97" rot="R90" grouprefs="PROCESSOR">AISG_4</text>
<text x="145.542" y="10.414" size="1.27" layer="97" grouprefs="PROCESSOR">Debug</text>
<text x="299.72" y="165.1" size="1.27" layer="97" grouprefs="MEMORY">PROCESSOR-&gt;EEPROM </text>
</plain>
<instances>
<instance part="U1" gate="G$1" x="0" y="0" smashed="yes">
<attribute name="DRAWING_NAME" x="317.5" y="10.16" size="1.778" layer="94" font="vector"/>
<attribute name="SHEET" x="332.486" y="5.842" size="1.27" layer="94" font="vector"/>
</instance>
<instance part="SUPPLY1" gate="GND" x="83.82" y="35.56" smashed="yes" grouprefs="PROCESSOR">
<attribute name="VALUE" x="81.915" y="32.385" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY2" gate="GND" x="170.18" y="137.16" smashed="yes" grouprefs="PROCESSOR">
<attribute name="VALUE" x="168.275" y="133.985" size="1.27" layer="96"/>
</instance>
<instance part="C66" gate="G$1" x="304.8" y="198.12" smashed="yes" rot="R90" grouprefs="BIASING_CAPACITORS">
<attribute name="NAME" x="304.292" y="193.04" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="304.292" y="199.39" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C68" gate="G$1" x="309.88" y="198.12" smashed="yes" rot="R90" grouprefs="BIASING_CAPACITORS">
<attribute name="NAME" x="309.372" y="193.04" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="309.372" y="199.39" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C65" gate="G$1" x="220.98" y="27.94" smashed="yes" rot="R90" grouprefs="CORE_BIASING_CAPS">
<attribute name="NAME" x="220.472" y="22.86" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="220.472" y="29.21" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C48" gate="G$1" x="320.04" y="198.12" smashed="yes" rot="R90" grouprefs="BIASING_CAPACITORS">
<attribute name="NAME" x="319.532" y="193.04" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="319.532" y="199.39" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C49" gate="G$1" x="325.12" y="198.12" smashed="yes" rot="R90" grouprefs="BIASING_CAPACITORS">
<attribute name="NAME" x="324.612" y="193.04" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="324.612" y="199.39" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C55" gate="G$1" x="335.28" y="198.12" smashed="yes" rot="R90" grouprefs="BIASING_CAPACITORS">
<attribute name="NAME" x="334.772" y="193.04" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="334.772" y="199.39" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C56" gate="G$1" x="340.36" y="198.12" smashed="yes" rot="R90" grouprefs="BIASING_CAPACITORS">
<attribute name="NAME" x="339.852" y="193.04" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="339.852" y="199.39" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C53" gate="G$1" x="350.52" y="198.12" smashed="yes" rot="R90" grouprefs="BIASING_CAPACITORS">
<attribute name="NAME" x="350.012" y="193.04" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="350.012" y="199.39" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C52" gate="G$1" x="355.6" y="198.12" smashed="yes" rot="R90" grouprefs="BIASING_CAPACITORS">
<attribute name="NAME" x="355.092" y="193.04" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="355.092" y="199.39" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="L1" gate="G$1" x="127" y="144.78" smashed="yes" rot="R90" grouprefs="PROCESSOR">
<attribute name="NAME" x="126.492" y="138.684" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="126.492" y="147.828" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C69" gate="G$1" x="226.06" y="27.94" smashed="yes" rot="R90" grouprefs="CORE_BIASING_CAPS">
<attribute name="NAME" x="225.552" y="22.86" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="225.552" y="29.21" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="SUPPLY3" gate="GND" x="299.72" y="185.42" smashed="yes" grouprefs="BIASING_CAPACITORS">
<attribute name="VALUE" x="297.815" y="182.245" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY4" gate="GND" x="220.98" y="12.7" smashed="yes" grouprefs="CORE_BIASING_CAPS">
<attribute name="VALUE" x="226.441" y="13.335" size="1.27" layer="96" rot="R180"/>
</instance>
<instance part="R130" gate="G$1" x="271.78" y="81.28" smashed="yes" rot="MR90" grouprefs="RESET">
<attribute name="NAME" x="271.2974" y="78.74" size="1.27" layer="95" rot="MR270"/>
<attribute name="VALUE" x="271.272" y="88.138" size="1.27" layer="96" rot="MR270"/>
</instance>
<instance part="R132" gate="G$1" x="279.4" y="71.12" smashed="yes" rot="MR180" grouprefs="RESET">
<attribute name="NAME" x="276.86" y="71.6026" size="1.27" layer="95" rot="MR0"/>
<attribute name="VALUE" x="285.496" y="71.628" size="1.27" layer="96" rot="MR0"/>
</instance>
<instance part="C72" gate="G$1" x="271.78" y="63.5" smashed="yes" rot="MR90" grouprefs="RESET">
<attribute name="NAME" x="272.288" y="58.42" size="1.27" layer="95" ratio="10" rot="MR90"/>
<attribute name="VALUE" x="272.288" y="64.77" size="1.27" layer="96" ratio="10" rot="MR90"/>
</instance>
<instance part="SW1" gate="G$1" x="287.02" y="63.5" smashed="yes" rot="MR90" grouprefs="RESET">
<attribute name="NAME" x="287.528" y="58.42" size="1.27" layer="95" ratio="15" rot="MR90"/>
<attribute name="VALUE" x="289.56" y="58.42" size="1.27" layer="96" ratio="15" rot="MR90"/>
</instance>
<instance part="+3V3" gate="G$1" x="271.78" y="91.44" smashed="yes" rot="MR0" grouprefs="RESET">
<attribute name="VALUE" x="274.574" y="91.694" size="1.27" layer="96" rot="MR0"/>
</instance>
<instance part="SUPPLY5" gate="GND" x="287.02" y="50.8" smashed="yes" rot="MR0" grouprefs="RESET">
<attribute name="VALUE" x="292.481" y="52.197" size="1.27" layer="96" rot="MR0"/>
</instance>
<instance part="R136" gate="G$1" x="335.28" y="81.28" smashed="yes" rot="R180" grouprefs="PROGRAMMING_HEADER">
<attribute name="NAME" x="329.184" y="81.7626" size="1.27" layer="95"/>
<attribute name="VALUE" x="337.82" y="81.788" size="1.27" layer="96"/>
</instance>
<instance part="+3V5" gate="G$1" x="314.96" y="91.44" smashed="yes" grouprefs="PROGRAMMING_HEADER">
<attribute name="VALUE" x="312.42" y="86.36" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="SUPPLY6" gate="GND" x="307.34" y="53.34" smashed="yes" grouprefs="PROGRAMMING_HEADER">
<attribute name="VALUE" x="305.435" y="50.165" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY7" gate="GND" x="185.42" y="35.56" smashed="yes" grouprefs="PROCESSOR">
<attribute name="VALUE" x="183.515" y="32.385" size="1.27" layer="96"/>
</instance>
<instance part="C46" gate="G$1" x="289.56" y="198.12" smashed="yes" rot="R90" grouprefs="BIASING_CAPACITORS">
<attribute name="NAME" x="289.052" y="193.04" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="289.052" y="199.39" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C44" gate="G$1" x="294.64" y="198.12" smashed="yes" rot="R90" grouprefs="BIASING_CAPACITORS">
<attribute name="NAME" x="294.132" y="193.04" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="294.132" y="199.39" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C43" gate="G$1" x="274.32" y="198.12" smashed="yes" rot="R90" grouprefs="BIASING_CAPACITORS">
<attribute name="NAME" x="273.812" y="193.04" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="273.812" y="199.39" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C42" gate="G$1" x="279.4" y="198.12" smashed="yes" rot="R90" grouprefs="BIASING_CAPACITORS">
<attribute name="NAME" x="278.892" y="193.04" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="278.892" y="199.39" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C47" gate="G$1" x="259.08" y="198.12" smashed="yes" rot="R90" grouprefs="BIASING_CAPACITORS">
<attribute name="NAME" x="258.572" y="193.04" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="258.572" y="199.39" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C40" gate="G$1" x="264.16" y="198.12" smashed="yes" rot="R90" grouprefs="BIASING_CAPACITORS">
<attribute name="NAME" x="263.652" y="193.04" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="263.652" y="199.39" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="U11" gate="G$1" x="134.62" y="91.44" smashed="yes" grouprefs="PROCESSOR">
<attribute name="NAME" x="134.62" y="93.98" size="1.27" layer="95"/>
<attribute name="VALUE" x="121.92" y="96.52" size="1.27" layer="96"/>
</instance>
<instance part="X2" gate="G$1" x="208.28" y="185.42" smashed="yes" grouprefs="CRYSTAL_OSC">
<attribute name="NAME" x="207.264" y="190.246" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="201.676" y="188.468" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="C71" gate="G$1" x="200.66" y="177.8" smashed="yes" rot="R90" grouprefs="CRYSTAL_OSC">
<attribute name="NAME" x="200.152" y="172.72" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="200.152" y="179.07" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C64" gate="G$1" x="215.9" y="177.8" smashed="yes" rot="R90" grouprefs="CRYSTAL_OSC">
<attribute name="NAME" x="215.392" y="172.72" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="215.392" y="179.07" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="SUPPLY10" gate="GND" x="208.28" y="162.56" smashed="yes" rot="MR0" grouprefs="CRYSTAL_OSC">
<attribute name="VALUE" x="213.741" y="163.957" size="1.27" layer="96" rot="MR0"/>
</instance>
<instance part="C75" gate="G$1" x="302.26" y="66.04" smashed="yes" rot="R90" grouprefs="PROGRAMMING_HEADER">
<attribute name="NAME" x="301.752" y="60.96" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="301.752" y="67.31" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="R96" gate="G$1" x="71.12" y="63.5" smashed="yes" rot="MR0" grouprefs="PROCESSOR">
<attribute name="NAME" x="65.024" y="63.0174" size="1.27" layer="95" rot="MR180"/>
<attribute name="VALUE" x="73.66" y="62.992" size="1.27" layer="96" rot="MR180"/>
</instance>
<instance part="TP26" gate="G$1" x="243.84" y="205.74" smashed="yes" rot="R90" grouprefs="BIASING_CAPACITORS">
<attribute name="NAME" x="240.03" y="204.47" size="1.27" layer="95" rot="R90"/>
</instance>
<instance part="C62" gate="G$1" x="370.84" y="198.12" smashed="yes" rot="R90" grouprefs="BIASING_CAPACITORS">
<attribute name="NAME" x="370.332" y="193.04" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="370.332" y="199.39" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C60" gate="G$1" x="365.76" y="198.12" smashed="yes" rot="R90" grouprefs="BIASING_CAPACITORS">
<attribute name="NAME" x="365.252" y="193.04" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="365.252" y="199.39" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="TP25" gate="G$1" x="210.82" y="35.56" smashed="yes" rot="R90" grouprefs="CORE_BIASING_CAPS">
<attribute name="NAME" x="209.55" y="34.29" size="1.27" layer="95" rot="R180"/>
</instance>
<instance part="+3V9" gate="G$1" x="246.38" y="215.9" smashed="yes" grouprefs="BIASING_CAPACITORS">
<attribute name="VALUE" x="243.078" y="216.408" size="1.27" layer="96"/>
</instance>
<instance part="TP27" gate="G$1" x="243.84" y="190.5" smashed="yes" rot="R90" grouprefs="BIASING_CAPACITORS">
<attribute name="NAME" x="240.03" y="189.23" size="1.27" layer="95" rot="R90"/>
</instance>
<instance part="C45" gate="G$1" x="254" y="198.12" smashed="yes" rot="R90" grouprefs="BIASING_CAPACITORS">
<attribute name="NAME" x="253.492" y="193.04" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="253.492" y="199.39" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C63" gate="G$1" x="215.9" y="27.94" smashed="yes" rot="R90" grouprefs="CORE_BIASING_CAPS">
<attribute name="NAME" x="215.392" y="22.86" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="215.392" y="29.21" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="R135" gate="G$1" x="335.28" y="86.36" smashed="yes" rot="R180" grouprefs="PROGRAMMING_HEADER">
<attribute name="NAME" x="329.184" y="86.8426" size="1.27" layer="95"/>
<attribute name="VALUE" x="337.82" y="86.868" size="1.27" layer="96"/>
</instance>
<instance part="R129" gate="G$1" x="314.96" y="63.5" smashed="yes" rot="R180" grouprefs="PROGRAMMING_HEADER">
<attribute name="NAME" x="308.61" y="63.9826" size="1.27" layer="95"/>
<attribute name="VALUE" x="317.5" y="64.008" size="1.27" layer="96"/>
</instance>
<instance part="R128" gate="G$1" x="134.62" y="152.4" smashed="yes" rot="R270" grouprefs="PROCESSOR">
<attribute name="NAME" x="134.1374" y="146.05" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="134.112" y="154.94" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="TP9" gate="G$1" x="152.4" y="30.48" smashed="yes" rot="R180" grouprefs="PROCESSOR">
<attribute name="NAME" x="151.384" y="27.686" size="1.778" layer="95" rot="R270"/>
</instance>
<instance part="TP10" gate="G$1" x="210.82" y="71.12" smashed="yes" rot="R270" grouprefs="PROCESSOR">
<attribute name="NAME" x="213.614" y="70.104" size="1.778" layer="95"/>
</instance>
<instance part="TP13" gate="G$1" x="210.82" y="81.28" smashed="yes" rot="R270" grouprefs="PROCESSOR">
<attribute name="NAME" x="213.614" y="80.264" size="1.778" layer="95"/>
</instance>
<instance part="TP24" gate="G$1" x="210.82" y="101.6" smashed="yes" rot="R270" grouprefs="PROCESSOR">
<attribute name="NAME" x="213.614" y="100.584" size="1.778" layer="95"/>
</instance>
<instance part="TP23" gate="G$1" x="210.82" y="111.76" smashed="yes" rot="R270" grouprefs="PROCESSOR">
<attribute name="NAME" x="213.614" y="110.744" size="1.778" layer="95"/>
</instance>
<instance part="R121" gate="G$1" x="30.48" y="119.38" smashed="yes" rot="R270" grouprefs="PROCESSOR">
<attribute name="NAME" x="29.9974" y="113.03" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="29.972" y="121.92" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R122" gate="G$1" x="35.56" y="119.38" smashed="yes" rot="R270" grouprefs="PROCESSOR">
<attribute name="NAME" x="35.0774" y="113.03" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="35.052" y="121.92" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R123" gate="G$1" x="40.64" y="119.38" smashed="yes" rot="R270" grouprefs="PROCESSOR">
<attribute name="NAME" x="40.1574" y="113.03" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="40.132" y="121.92" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R124" gate="G$1" x="45.72" y="119.38" smashed="yes" rot="R270" grouprefs="PROCESSOR">
<attribute name="NAME" x="45.2374" y="113.03" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="45.212" y="121.92" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R111" gate="G$1" x="30.48" y="147.32" smashed="yes" rot="R270" grouprefs="PROCESSOR">
<attribute name="NAME" x="29.9974" y="140.97" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="29.972" y="149.86" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R112" gate="G$1" x="35.56" y="147.32" smashed="yes" rot="R270" grouprefs="PROCESSOR">
<attribute name="NAME" x="35.0774" y="140.97" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="35.052" y="149.86" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R113" gate="G$1" x="40.64" y="147.32" smashed="yes" rot="R270" grouprefs="PROCESSOR">
<attribute name="NAME" x="40.1574" y="140.97" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="40.132" y="149.86" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R114" gate="G$1" x="45.72" y="147.32" smashed="yes" rot="R270" grouprefs="PROCESSOR">
<attribute name="NAME" x="45.2374" y="140.97" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="45.212" y="149.86" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="SUPPLY98" gate="GND" x="30.48" y="104.14" smashed="yes" grouprefs="PROCESSOR">
<attribute name="VALUE" x="28.575" y="100.965" size="1.27" layer="96"/>
</instance>
<instance part="+3V4" gate="G$1" x="45.72" y="160.02" smashed="yes" grouprefs="PROCESSOR">
<attribute name="VALUE" x="42.418" y="160.528" size="1.27" layer="96"/>
</instance>
<instance part="TP21" gate="G$1" x="58.42" y="106.68" smashed="yes" rot="R90" grouprefs="PROCESSOR">
<attribute name="NAME" x="55.626" y="107.442" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="TP22" gate="G$1" x="58.42" y="104.14" smashed="yes" rot="R90" grouprefs="PROCESSOR">
<attribute name="NAME" x="55.626" y="104.902" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="TP19" gate="G$1" x="139.7" y="17.78" smashed="yes" rot="R180" grouprefs="PROCESSOR">
<attribute name="NAME" x="138.938" y="14.986" size="1.778" layer="95" rot="R270"/>
</instance>
<instance part="TP20" gate="G$1" x="142.24" y="17.78" smashed="yes" rot="R180" grouprefs="PROCESSOR">
<attribute name="NAME" x="141.478" y="14.986" size="1.778" layer="95" rot="R270"/>
</instance>
<instance part="TP17" gate="G$1" x="58.42" y="91.44" smashed="yes" rot="R90" grouprefs="PROCESSOR">
<attribute name="NAME" x="55.626" y="92.202" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="TP16" gate="G$1" x="58.42" y="88.9" smashed="yes" rot="R90" grouprefs="PROCESSOR">
<attribute name="NAME" x="55.626" y="89.662" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="TP15" gate="G$1" x="58.42" y="86.36" smashed="yes" rot="R90" grouprefs="PROCESSOR">
<attribute name="NAME" x="55.626" y="87.122" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="TP14" gate="G$1" x="58.42" y="83.82" smashed="yes" rot="R90" grouprefs="PROCESSOR">
<attribute name="NAME" x="55.626" y="84.582" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="TP12" gate="G$1" x="58.42" y="81.28" smashed="yes" rot="R90" grouprefs="PROCESSOR">
<attribute name="NAME" x="55.626" y="82.042" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="TP11" gate="G$1" x="58.42" y="78.74" smashed="yes" rot="R90" grouprefs="PROCESSOR">
<attribute name="NAME" x="55.626" y="79.502" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="R85" gate="G$1" x="45.72" y="25.4" smashed="yes" rot="MR0" grouprefs="PROCESSOR">
<attribute name="NAME" x="39.624" y="24.9174" size="1.27" layer="95" rot="MR180"/>
<attribute name="VALUE" x="48.26" y="24.892" size="1.27" layer="96" rot="MR180"/>
</instance>
<instance part="R86" gate="G$1" x="45.72" y="33.02" smashed="yes" rot="MR0" grouprefs="PROCESSOR">
<attribute name="NAME" x="39.624" y="32.5374" size="1.27" layer="95" rot="MR180"/>
<attribute name="VALUE" x="48.26" y="32.512" size="1.27" layer="96" rot="MR180"/>
</instance>
<instance part="R87" gate="G$1" x="45.72" y="40.64" smashed="yes" rot="MR0" grouprefs="PROCESSOR">
<attribute name="NAME" x="39.624" y="40.1574" size="1.27" layer="95" rot="MR180"/>
<attribute name="VALUE" x="48.26" y="40.132" size="1.27" layer="96" rot="MR180"/>
</instance>
<instance part="R88" gate="G$1" x="45.72" y="48.26" smashed="yes" rot="MR0" grouprefs="PROCESSOR">
<attribute name="NAME" x="39.624" y="47.7774" size="1.27" layer="95" rot="MR180"/>
<attribute name="VALUE" x="48.26" y="47.752" size="1.27" layer="96" rot="MR180"/>
</instance>
<instance part="SUPPLY99" gate="GND" x="22.86" y="20.32" smashed="yes" grouprefs="PROCESSOR">
<attribute name="VALUE" x="20.955" y="17.145" size="1.27" layer="96"/>
</instance>
<instance part="R117" gate="G$1" x="10.16" y="154.94" smashed="yes" rot="R270" grouprefs="PROCESSOR">
<attribute name="NAME" x="9.6774" y="148.59" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="9.652" y="157.48" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R118" gate="G$1" x="15.24" y="154.94" smashed="yes" rot="R270" grouprefs="PROCESSOR">
<attribute name="NAME" x="14.7574" y="148.59" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="14.732" y="157.48" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R119" gate="G$1" x="20.32" y="154.94" smashed="yes" rot="R270" grouprefs="PROCESSOR">
<attribute name="NAME" x="19.8374" y="148.59" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="19.812" y="157.48" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R120" gate="G$1" x="25.4" y="154.94" smashed="yes" rot="R270" grouprefs="PROCESSOR">
<attribute name="NAME" x="24.9174" y="148.59" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="24.892" y="157.48" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R107" gate="G$1" x="10.16" y="182.88" smashed="yes" rot="R270" grouprefs="PROCESSOR">
<attribute name="NAME" x="9.6774" y="176.53" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="9.652" y="185.42" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R108" gate="G$1" x="15.24" y="182.88" smashed="yes" rot="R270" grouprefs="PROCESSOR">
<attribute name="NAME" x="14.7574" y="176.53" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="14.732" y="185.42" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R109" gate="G$1" x="20.32" y="182.88" smashed="yes" rot="R270" grouprefs="PROCESSOR">
<attribute name="NAME" x="19.8374" y="176.53" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="19.812" y="185.42" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R110" gate="G$1" x="25.4" y="182.88" smashed="yes" rot="R270" grouprefs="PROCESSOR">
<attribute name="NAME" x="24.9174" y="176.53" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="24.892" y="185.42" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="SUPPLY100" gate="GND" x="10.16" y="139.7" smashed="yes" grouprefs="PROCESSOR">
<attribute name="VALUE" x="8.255" y="136.525" size="1.27" layer="96"/>
</instance>
<instance part="+3V7" gate="G$1" x="25.4" y="195.58" smashed="yes" grouprefs="PROCESSOR">
<attribute name="VALUE" x="22.098" y="196.088" size="1.27" layer="96"/>
</instance>
<instance part="TP28" gate="G$1" x="58.42" y="114.3" smashed="yes" rot="R90" grouprefs="PROCESSOR">
<attribute name="NAME" x="55.626" y="115.062" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="TP29" gate="G$1" x="58.42" y="116.84" smashed="yes" rot="R90" grouprefs="PROCESSOR">
<attribute name="NAME" x="55.626" y="117.602" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="U5" gate="G$1" x="299.72" y="137.16" smashed="yes" grouprefs="MEMORY">
<attribute name="NAME" x="299.72" y="139.954" size="1.27" layer="95" ratio="15"/>
<attribute name="VALUE" x="299.72" y="138.176" size="1.27" layer="96" ratio="15"/>
</instance>
<instance part="R36" gate="G$1" x="289.56" y="149.86" smashed="yes" rot="R90" grouprefs="MEMORY">
<attribute name="NAME" x="289.0774" y="143.764" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="289.052" y="152.4" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R37" gate="G$1" x="284.48" y="149.86" smashed="yes" rot="R90" grouprefs="MEMORY">
<attribute name="NAME" x="283.9974" y="143.51" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="283.972" y="152.4" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R38" gate="G$1" x="340.36" y="152.4" smashed="yes" rot="R90" grouprefs="MEMORY">
<attribute name="NAME" x="339.8774" y="146.304" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="339.852" y="154.94" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R47" gate="G$1" x="345.44" y="152.4" smashed="yes" rot="R90" grouprefs="MEMORY">
<attribute name="NAME" x="344.9574" y="146.304" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="344.932" y="154.94" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R48" gate="G$1" x="350.52" y="152.4" smashed="yes" rot="R90" grouprefs="MEMORY">
<attribute name="NAME" x="350.0374" y="146.304" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="350.012" y="154.94" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R49" gate="G$1" x="279.4" y="149.86" smashed="yes" rot="R90" grouprefs="MEMORY">
<attribute name="NAME" x="278.9174" y="143.256" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="278.892" y="152.4" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="SUPPLY8" gate="GND" x="289.56" y="119.38" smashed="yes" grouprefs="MEMORY">
<attribute name="VALUE" x="287.655" y="116.205" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY9" gate="GND" x="317.5" y="149.86" smashed="yes" grouprefs="MEMORY">
<attribute name="VALUE" x="315.595" y="146.685" size="1.27" layer="96"/>
</instance>
<instance part="C20" gate="G$1" x="325.12" y="157.48" smashed="yes" grouprefs="MEMORY">
<attribute name="NAME" x="323.85" y="160.02" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="323.85" y="153.67" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="+3V1" gate="G$1" x="363.22" y="167.64" smashed="yes" grouprefs="MEMORY">
<attribute name="VALUE" x="360.68" y="162.56" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="J1" gate="-1" x="327.66" y="73.66" smashed="yes">
<attribute name="NAME" x="324.358" y="74.168" size="1.27" layer="95" ratio="15"/>
<attribute name="MP" x="332.74" y="60.96" size="1.27" layer="96"/>
</instance>
<instance part="J1" gate="-2" x="327.66" y="71.12" smashed="yes">
<attribute name="NAME" x="324.358" y="71.628" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J1" gate="-3" x="327.66" y="68.58" smashed="yes">
<attribute name="NAME" x="324.358" y="69.088" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J1" gate="-4" x="327.66" y="66.04" smashed="yes">
<attribute name="NAME" x="324.358" y="66.548" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J1" gate="-5" x="327.66" y="63.5" smashed="yes">
<attribute name="NAME" x="324.358" y="64.008" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J1" gate="-6" x="347.98" y="63.5" smashed="yes" rot="R180">
<attribute name="NAME" x="351.282" y="62.992" size="1.27" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="J1" gate="-7" x="347.98" y="66.04" smashed="yes" rot="R180">
<attribute name="NAME" x="351.282" y="65.532" size="1.27" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="J1" gate="-8" x="347.98" y="68.58" smashed="yes" rot="R180">
<attribute name="NAME" x="351.282" y="68.072" size="1.27" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="J1" gate="-9" x="347.98" y="71.12" smashed="yes" rot="R180">
<attribute name="NAME" x="351.282" y="70.612" size="1.27" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="J1" gate="-10" x="347.98" y="73.66" smashed="yes" rot="R180">
<attribute name="NAME" x="351.282" y="73.152" size="1.27" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="TP1" gate="G$1" x="45.72" y="63.5" smashed="yes" rot="R90">
<attribute name="NAME" x="42.926" y="64.262" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="D10" gate="G$1" x="30.48" y="48.26" smashed="yes" rot="R180">
<attribute name="NAME" x="32.004" y="45.72" size="1.27" layer="95"/>
<attribute name="VALUE" x="34.036" y="51.562" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="D9" gate="G$1" x="30.48" y="40.64" smashed="yes" rot="R180">
<attribute name="NAME" x="32.004" y="38.1" size="1.27" layer="95"/>
<attribute name="VALUE" x="34.036" y="43.942" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="D8" gate="G$1" x="30.48" y="33.02" smashed="yes" rot="R180">
<attribute name="NAME" x="32.004" y="30.48" size="1.27" layer="95"/>
<attribute name="VALUE" x="34.036" y="36.322" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="D7" gate="G$1" x="30.48" y="25.4" smashed="yes" rot="R180">
<attribute name="NAME" x="32.004" y="22.86" size="1.27" layer="95"/>
<attribute name="VALUE" x="34.036" y="28.702" size="1.778" layer="96" rot="R180"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="SUPPLY2" gate="GND" pin="GND"/>
<wire x1="129.54" y1="142.24" x2="165.1" y2="142.24" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<wire x1="165.1" y1="142.24" x2="170.18" y2="142.24" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<wire x1="170.18" y1="142.24" x2="170.18" y2="139.7" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<wire x1="129.54" y1="137.16" x2="129.54" y2="142.24" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<wire x1="165.1" y1="137.16" x2="165.1" y2="142.24" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<junction x="165.1" y="142.24" grouprefs="PROCESSOR"/>
<pinref part="U11" gate="G$1" pin="76"/>
<pinref part="U11" gate="G$1" pin="90"/>
</segment>
<segment>
<wire x1="88.9" y1="96.52" x2="83.82" y2="96.52" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<pinref part="SUPPLY1" gate="GND" pin="GND"/>
<wire x1="83.82" y1="96.52" x2="83.82" y2="63.5" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<wire x1="83.82" y1="63.5" x2="83.82" y2="40.64" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<wire x1="83.82" y1="40.64" x2="83.82" y2="38.1" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<wire x1="88.9" y1="63.5" x2="83.82" y2="63.5" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<junction x="83.82" y="63.5" grouprefs="PROCESSOR"/>
<wire x1="116.84" y1="45.72" x2="116.84" y2="40.64" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<wire x1="116.84" y1="40.64" x2="83.82" y2="40.64" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<junction x="83.82" y="40.64" grouprefs="PROCESSOR"/>
<pinref part="U11" gate="G$1" pin="11"/>
<pinref part="U11" gate="G$1" pin="24"/>
<pinref part="U11" gate="G$1" pin="31"/>
</segment>
<segment>
<pinref part="C65" gate="G$1" pin="1"/>
<wire x1="220.98" y1="20.32" x2="220.98" y2="22.86" width="0.1524" layer="91" grouprefs="CORE_BIASING_CAPS"/>
<pinref part="C69" gate="G$1" pin="1"/>
<wire x1="226.06" y1="22.86" x2="226.06" y2="20.32" width="0.1524" layer="91" grouprefs="CORE_BIASING_CAPS"/>
<wire x1="226.06" y1="20.32" x2="220.98" y2="20.32" width="0.1524" layer="91" grouprefs="CORE_BIASING_CAPS"/>
<pinref part="SUPPLY4" gate="GND" pin="GND"/>
<wire x1="220.98" y1="15.24" x2="220.98" y2="17.78" width="0.1524" layer="91" grouprefs="CORE_BIASING_CAPS"/>
<junction x="220.98" y="20.32" grouprefs="CORE_BIASING_CAPS"/>
<pinref part="C63" gate="G$1" pin="1"/>
<wire x1="220.98" y1="17.78" x2="220.98" y2="20.32" width="0.1524" layer="91" grouprefs="CORE_BIASING_CAPS"/>
<wire x1="215.9" y1="22.86" x2="215.9" y2="17.78" width="0.1524" layer="91" grouprefs="CORE_BIASING_CAPS"/>
<wire x1="215.9" y1="17.78" x2="220.98" y2="17.78" width="0.1524" layer="91" grouprefs="CORE_BIASING_CAPS"/>
<junction x="220.98" y="17.78" grouprefs="CORE_BIASING_CAPS"/>
</segment>
<segment>
<pinref part="SW1" gate="G$1" pin="1"/>
<wire x1="287.02" y1="58.42" x2="287.02" y2="55.88" width="0.1524" layer="91" grouprefs="RESET"/>
<wire x1="287.02" y1="55.88" x2="271.78" y2="55.88" width="0.1524" layer="91" grouprefs="RESET"/>
<pinref part="C72" gate="G$1" pin="1"/>
<wire x1="271.78" y1="55.88" x2="271.78" y2="58.42" width="0.1524" layer="91" grouprefs="RESET"/>
<pinref part="SUPPLY5" gate="GND" pin="GND"/>
<wire x1="287.02" y1="53.34" x2="287.02" y2="55.88" width="0.1524" layer="91" grouprefs="RESET"/>
<junction x="287.02" y="55.88" grouprefs="RESET"/>
</segment>
<segment>
<wire x1="322.58" y1="71.12" x2="307.34" y2="71.12" width="0.1524" layer="91" grouprefs="PROGRAMMING_HEADER"/>
<wire x1="307.34" y1="71.12" x2="307.34" y2="68.58" width="0.1524" layer="91" grouprefs="PROGRAMMING_HEADER"/>
<wire x1="307.34" y1="63.5" x2="307.34" y2="58.42" width="0.1524" layer="91" grouprefs="PROGRAMMING_HEADER"/>
<wire x1="307.34" y1="58.42" x2="307.34" y2="55.88" width="0.1524" layer="91" grouprefs="PROGRAMMING_HEADER"/>
<wire x1="322.58" y1="68.58" x2="307.34" y2="68.58" width="0.1524" layer="91" grouprefs="PROGRAMMING_HEADER"/>
<junction x="307.34" y="68.58" grouprefs="PROGRAMMING_HEADER"/>
<wire x1="307.34" y1="68.58" x2="307.34" y2="63.5" width="0.1524" layer="91" grouprefs="PROGRAMMING_HEADER"/>
<junction x="307.34" y="63.5" grouprefs="PROGRAMMING_HEADER"/>
<pinref part="SUPPLY6" gate="GND" pin="GND"/>
<pinref part="C75" gate="G$1" pin="1"/>
<wire x1="309.88" y1="63.5" x2="307.34" y2="63.5" width="0.1524" layer="91" grouprefs="PROGRAMMING_HEADER"/>
<wire x1="307.34" y1="58.42" x2="302.26" y2="58.42" width="0.1524" layer="91" grouprefs="PROGRAMMING_HEADER"/>
<wire x1="302.26" y1="58.42" x2="302.26" y2="60.96" width="0.1524" layer="91" grouprefs="PROGRAMMING_HEADER"/>
<junction x="307.34" y="58.42" grouprefs="PROGRAMMING_HEADER"/>
<pinref part="R129" gate="G$1" pin="2"/>
<pinref part="J1" gate="-2" pin="1"/>
<pinref part="J1" gate="-3" pin="1"/>
</segment>
<segment>
<wire x1="180.34" y1="88.9" x2="185.42" y2="88.9" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<pinref part="SUPPLY7" gate="GND" pin="GND"/>
<wire x1="185.42" y1="88.9" x2="185.42" y2="40.64" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<wire x1="185.42" y1="40.64" x2="185.42" y2="38.1" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<wire x1="134.62" y1="45.72" x2="134.62" y2="40.64" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<wire x1="134.62" y1="40.64" x2="165.1" y2="40.64" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<junction x="185.42" y="40.64" grouprefs="PROCESSOR"/>
<pinref part="U11" gate="G$1" pin="38"/>
<pinref part="U11" gate="G$1" pin="62"/>
<pinref part="U11" gate="G$1" pin="50"/>
<wire x1="165.1" y1="40.64" x2="185.42" y2="40.64" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<wire x1="165.1" y1="45.72" x2="165.1" y2="40.64" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<junction x="165.1" y="40.64" grouprefs="PROCESSOR"/>
</segment>
<segment>
<pinref part="X2" gate="G$1" pin="3"/>
<pinref part="SUPPLY10" gate="GND" pin="GND"/>
<wire x1="208.28" y1="180.34" x2="208.28" y2="170.18" width="0.1524" layer="91" grouprefs="CRYSTAL_OSC"/>
<pinref part="C71" gate="G$1" pin="1"/>
<wire x1="208.28" y1="170.18" x2="208.28" y2="167.64" width="0.1524" layer="91" grouprefs="CRYSTAL_OSC"/>
<wire x1="208.28" y1="167.64" x2="208.28" y2="165.1" width="0.1524" layer="91" grouprefs="CRYSTAL_OSC"/>
<wire x1="200.66" y1="172.72" x2="200.66" y2="170.18" width="0.1524" layer="91" grouprefs="CRYSTAL_OSC"/>
<wire x1="200.66" y1="170.18" x2="208.28" y2="170.18" width="0.1524" layer="91" grouprefs="CRYSTAL_OSC"/>
<pinref part="C64" gate="G$1" pin="1"/>
<wire x1="215.9" y1="172.72" x2="215.9" y2="167.64" width="0.1524" layer="91" grouprefs="CRYSTAL_OSC"/>
<wire x1="215.9" y1="167.64" x2="208.28" y2="167.64" width="0.1524" layer="91" grouprefs="CRYSTAL_OSC"/>
<junction x="208.28" y="170.18" grouprefs="CRYSTAL_OSC"/>
<junction x="208.28" y="167.64" grouprefs="CRYSTAL_OSC"/>
</segment>
<segment>
<pinref part="C68" gate="G$1" pin="1"/>
<wire x1="304.8" y1="190.5" x2="309.88" y2="190.5" width="0.1524" layer="91" grouprefs="BIASING_CAPACITORS"/>
<wire x1="309.88" y1="190.5" x2="309.88" y2="193.04" width="0.1524" layer="91" grouprefs="BIASING_CAPACITORS"/>
<pinref part="C66" gate="G$1" pin="1"/>
<wire x1="304.8" y1="190.5" x2="304.8" y2="193.04" width="0.1524" layer="91" grouprefs="BIASING_CAPACITORS"/>
<pinref part="C49" gate="G$1" pin="1"/>
<wire x1="320.04" y1="190.5" x2="325.12" y2="190.5" width="0.1524" layer="91" grouprefs="BIASING_CAPACITORS"/>
<wire x1="325.12" y1="190.5" x2="325.12" y2="193.04" width="0.1524" layer="91" grouprefs="BIASING_CAPACITORS"/>
<pinref part="C48" gate="G$1" pin="1"/>
<wire x1="320.04" y1="190.5" x2="320.04" y2="193.04" width="0.1524" layer="91" grouprefs="BIASING_CAPACITORS"/>
<wire x1="309.88" y1="190.5" x2="320.04" y2="190.5" width="0.1524" layer="91" grouprefs="BIASING_CAPACITORS"/>
<junction x="309.88" y="190.5" grouprefs="BIASING_CAPACITORS"/>
<junction x="320.04" y="190.5" grouprefs="BIASING_CAPACITORS"/>
<pinref part="C56" gate="G$1" pin="1"/>
<wire x1="335.28" y1="190.5" x2="340.36" y2="190.5" width="0.1524" layer="91" grouprefs="BIASING_CAPACITORS"/>
<wire x1="340.36" y1="190.5" x2="340.36" y2="193.04" width="0.1524" layer="91" grouprefs="BIASING_CAPACITORS"/>
<pinref part="C55" gate="G$1" pin="1"/>
<wire x1="335.28" y1="190.5" x2="335.28" y2="193.04" width="0.1524" layer="91" grouprefs="BIASING_CAPACITORS"/>
<wire x1="325.12" y1="190.5" x2="335.28" y2="190.5" width="0.1524" layer="91" grouprefs="BIASING_CAPACITORS"/>
<junction x="335.28" y="190.5" grouprefs="BIASING_CAPACITORS"/>
<junction x="325.12" y="190.5" grouprefs="BIASING_CAPACITORS"/>
<pinref part="C52" gate="G$1" pin="1"/>
<wire x1="350.52" y1="190.5" x2="355.6" y2="190.5" width="0.1524" layer="91" grouprefs="BIASING_CAPACITORS"/>
<wire x1="355.6" y1="190.5" x2="355.6" y2="193.04" width="0.1524" layer="91" grouprefs="BIASING_CAPACITORS"/>
<pinref part="C53" gate="G$1" pin="1"/>
<wire x1="350.52" y1="190.5" x2="350.52" y2="193.04" width="0.1524" layer="91" grouprefs="BIASING_CAPACITORS"/>
<wire x1="337.82" y1="190.5" x2="340.36" y2="190.5" width="0.1524" layer="91" grouprefs="BIASING_CAPACITORS"/>
<junction x="350.52" y="190.5" grouprefs="BIASING_CAPACITORS"/>
<junction x="340.36" y="190.5" grouprefs="BIASING_CAPACITORS"/>
<wire x1="340.36" y1="190.5" x2="350.52" y2="190.5" width="0.1524" layer="91" grouprefs="BIASING_CAPACITORS"/>
<junction x="304.8" y="190.5" grouprefs="BIASING_CAPACITORS"/>
<pinref part="C44" gate="G$1" pin="1"/>
<wire x1="289.56" y1="190.5" x2="294.64" y2="190.5" width="0.1524" layer="91" grouprefs="BIASING_CAPACITORS"/>
<wire x1="294.64" y1="190.5" x2="294.64" y2="193.04" width="0.1524" layer="91" grouprefs="BIASING_CAPACITORS"/>
<pinref part="C46" gate="G$1" pin="1"/>
<wire x1="289.56" y1="190.5" x2="289.56" y2="193.04" width="0.1524" layer="91" grouprefs="BIASING_CAPACITORS"/>
<wire x1="279.4" y1="190.5" x2="289.56" y2="190.5" width="0.1524" layer="91" grouprefs="BIASING_CAPACITORS"/>
<junction x="289.56" y="190.5" grouprefs="BIASING_CAPACITORS"/>
<pinref part="C42" gate="G$1" pin="1"/>
<wire x1="274.32" y1="190.5" x2="279.4" y2="190.5" width="0.1524" layer="91" grouprefs="BIASING_CAPACITORS"/>
<wire x1="279.4" y1="190.5" x2="279.4" y2="193.04" width="0.1524" layer="91" grouprefs="BIASING_CAPACITORS"/>
<pinref part="C43" gate="G$1" pin="1"/>
<wire x1="274.32" y1="190.5" x2="274.32" y2="193.04" width="0.1524" layer="91" grouprefs="BIASING_CAPACITORS"/>
<wire x1="264.16" y1="190.5" x2="274.32" y2="190.5" width="0.1524" layer="91" grouprefs="BIASING_CAPACITORS"/>
<junction x="274.32" y="190.5" grouprefs="BIASING_CAPACITORS"/>
<junction x="279.4" y="190.5" grouprefs="BIASING_CAPACITORS"/>
<pinref part="C40" gate="G$1" pin="1"/>
<wire x1="259.08" y1="190.5" x2="264.16" y2="190.5" width="0.1524" layer="91" grouprefs="BIASING_CAPACITORS"/>
<wire x1="264.16" y1="190.5" x2="264.16" y2="193.04" width="0.1524" layer="91" grouprefs="BIASING_CAPACITORS"/>
<pinref part="C47" gate="G$1" pin="1"/>
<wire x1="259.08" y1="190.5" x2="259.08" y2="193.04" width="0.1524" layer="91" grouprefs="BIASING_CAPACITORS"/>
<junction x="264.16" y="190.5" grouprefs="BIASING_CAPACITORS"/>
<wire x1="294.64" y1="190.5" x2="299.72" y2="190.5" width="0.1524" layer="91" grouprefs="BIASING_CAPACITORS"/>
<junction x="294.64" y="190.5" grouprefs="BIASING_CAPACITORS"/>
<wire x1="299.72" y1="190.5" x2="304.8" y2="190.5" width="0.1524" layer="91" grouprefs="BIASING_CAPACITORS"/>
<wire x1="355.6" y1="190.5" x2="365.76" y2="190.5" width="0.1524" layer="91" grouprefs="BIASING_CAPACITORS"/>
<junction x="355.6" y="190.5" grouprefs="BIASING_CAPACITORS"/>
<pinref part="C60" gate="G$1" pin="1"/>
<wire x1="365.76" y1="190.5" x2="365.76" y2="193.04" width="0.1524" layer="91" grouprefs="BIASING_CAPACITORS"/>
<pinref part="C62" gate="G$1" pin="1"/>
<wire x1="365.76" y1="190.5" x2="370.84" y2="190.5" width="0.1524" layer="91" grouprefs="BIASING_CAPACITORS"/>
<wire x1="370.84" y1="190.5" x2="370.84" y2="193.04" width="0.1524" layer="91" grouprefs="BIASING_CAPACITORS"/>
<junction x="365.76" y="190.5" grouprefs="BIASING_CAPACITORS"/>
<pinref part="SUPPLY3" gate="GND" pin="GND"/>
<wire x1="299.72" y1="187.96" x2="299.72" y2="190.5" width="0.1524" layer="91" grouprefs="BIASING_CAPACITORS"/>
<junction x="299.72" y="190.5" grouprefs="BIASING_CAPACITORS"/>
<pinref part="TP27" gate="G$1" pin="PP"/>
<wire x1="243.84" y1="190.5" x2="254" y2="190.5" width="0.1524" layer="91" grouprefs="BIASING_CAPACITORS"/>
<junction x="259.08" y="190.5" grouprefs="BIASING_CAPACITORS"/>
<pinref part="C45" gate="G$1" pin="1"/>
<wire x1="254" y1="190.5" x2="259.08" y2="190.5" width="0.1524" layer="91" grouprefs="BIASING_CAPACITORS"/>
<wire x1="254" y1="193.04" x2="254" y2="190.5" width="0.1524" layer="91" grouprefs="BIASING_CAPACITORS"/>
<junction x="254" y="190.5" grouprefs="BIASING_CAPACITORS"/>
</segment>
<segment>
<pinref part="R121" gate="G$1" pin="2"/>
<wire x1="30.48" y1="114.3" x2="30.48" y2="109.22" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<wire x1="30.48" y1="109.22" x2="35.56" y2="109.22" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<pinref part="R124" gate="G$1" pin="2"/>
<wire x1="35.56" y1="109.22" x2="40.64" y2="109.22" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<wire x1="40.64" y1="109.22" x2="45.72" y2="109.22" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<wire x1="45.72" y1="109.22" x2="45.72" y2="114.3" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<pinref part="R123" gate="G$1" pin="2"/>
<wire x1="40.64" y1="114.3" x2="40.64" y2="109.22" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<junction x="40.64" y="109.22" grouprefs="PROCESSOR"/>
<pinref part="R122" gate="G$1" pin="2"/>
<wire x1="35.56" y1="114.3" x2="35.56" y2="109.22" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<junction x="35.56" y="109.22" grouprefs="PROCESSOR"/>
<pinref part="SUPPLY98" gate="GND" pin="GND"/>
<wire x1="30.48" y1="106.68" x2="30.48" y2="109.22" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<junction x="30.48" y="109.22" grouprefs="PROCESSOR"/>
</segment>
<segment>
<wire x1="26.416" y1="48.26" x2="22.86" y2="48.26" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<wire x1="22.86" y1="48.26" x2="22.86" y2="40.64" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<wire x1="22.86" y1="40.64" x2="22.86" y2="33.02" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<wire x1="22.86" y1="33.02" x2="22.86" y2="25.4" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<wire x1="22.86" y1="25.4" x2="26.416" y2="25.4" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<wire x1="26.416" y1="33.02" x2="22.86" y2="33.02" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<junction x="22.86" y="33.02" grouprefs="PROCESSOR"/>
<wire x1="26.416" y1="40.64" x2="22.86" y2="40.64" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<junction x="22.86" y="40.64" grouprefs="PROCESSOR"/>
<pinref part="SUPPLY99" gate="GND" pin="GND"/>
<wire x1="22.86" y1="22.86" x2="22.86" y2="25.4" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<junction x="22.86" y="25.4" grouprefs="PROCESSOR"/>
<pinref part="D10" gate="G$1" pin="K"/>
<pinref part="D9" gate="G$1" pin="K"/>
<pinref part="D8" gate="G$1" pin="K"/>
<pinref part="D7" gate="G$1" pin="K"/>
</segment>
<segment>
<pinref part="R117" gate="G$1" pin="2"/>
<wire x1="10.16" y1="149.86" x2="10.16" y2="144.78" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<wire x1="10.16" y1="144.78" x2="15.24" y2="144.78" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<pinref part="R120" gate="G$1" pin="2"/>
<wire x1="15.24" y1="144.78" x2="20.32" y2="144.78" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<wire x1="20.32" y1="144.78" x2="25.4" y2="144.78" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<wire x1="25.4" y1="144.78" x2="25.4" y2="149.86" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<pinref part="R119" gate="G$1" pin="2"/>
<wire x1="20.32" y1="149.86" x2="20.32" y2="144.78" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<junction x="20.32" y="144.78" grouprefs="PROCESSOR"/>
<pinref part="R118" gate="G$1" pin="2"/>
<wire x1="15.24" y1="149.86" x2="15.24" y2="144.78" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<junction x="15.24" y="144.78" grouprefs="PROCESSOR"/>
<pinref part="SUPPLY100" gate="GND" pin="GND"/>
<wire x1="10.16" y1="142.24" x2="10.16" y2="144.78" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<junction x="10.16" y="144.78" grouprefs="PROCESSOR"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="4"/>
<wire x1="297.18" y1="124.46" x2="289.56" y2="124.46" width="0.1524" layer="91" grouprefs="MEMORY"/>
<wire x1="289.56" y1="124.46" x2="289.56" y2="121.92" width="0.1524" layer="91" grouprefs="MEMORY"/>
<pinref part="SUPPLY8" gate="GND" pin="GND"/>
</segment>
<segment>
<wire x1="320.04" y1="157.48" x2="317.5" y2="157.48" width="0.1524" layer="91" grouprefs="MEMORY"/>
<wire x1="317.5" y1="157.48" x2="317.5" y2="152.4" width="0.1524" layer="91" grouprefs="MEMORY"/>
<pinref part="SUPPLY9" gate="GND" pin="GND"/>
<pinref part="C20" gate="G$1" pin="1"/>
</segment>
</net>
<net name="RSTN" class="0">
<segment>
<pinref part="R132" gate="G$1" pin="1"/>
<wire x1="274.32" y1="71.12" x2="271.78" y2="71.12" width="0.1524" layer="91" grouprefs="RESET"/>
<pinref part="C72" gate="G$1" pin="2"/>
<wire x1="271.78" y1="71.12" x2="271.78" y2="68.58" width="0.1524" layer="91" grouprefs="RESET"/>
<pinref part="R130" gate="G$1" pin="1"/>
<wire x1="271.78" y1="76.2" x2="271.78" y2="73.66" width="0.1524" layer="91" grouprefs="RESET"/>
<junction x="271.78" y="71.12" grouprefs="RESET"/>
<wire x1="271.78" y1="73.66" x2="271.78" y2="71.12" width="0.1524" layer="91" grouprefs="RESET"/>
<wire x1="271.78" y1="73.66" x2="266.7" y2="73.66" width="0.1524" layer="91" grouprefs="RESET"/>
<junction x="271.78" y="73.66" grouprefs="RESET"/>
<label x="266.7" y="73.66" size="1.27" layer="95" rot="MR0" xref="yes" grouprefs="RESET"/>
</segment>
<segment>
<wire x1="353.06" y1="63.5" x2="360.68" y2="63.5" width="0.1524" layer="91" grouprefs="PROGRAMMING_HEADER"/>
<label x="360.68" y="63.5" size="1.27" layer="95" xref="yes" grouprefs="PROGRAMMING_HEADER"/>
<pinref part="J1" gate="-6" pin="1"/>
</segment>
<segment>
<pinref part="R128" gate="G$1" pin="1"/>
<wire x1="134.62" y1="157.48" x2="134.62" y2="167.64" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<label x="134.62" y="160.02" size="1.778" layer="95" rot="R90" grouprefs="PROCESSOR"/>
<junction x="134.62" y="167.64" grouprefs="PROCESSOR"/>
</segment>
</net>
<net name="VDD_A" class="0">
<segment>
<wire x1="88.9" y1="93.98" x2="63.5" y2="93.98" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<label x="58.42" y="93.98" size="1.27" layer="95" rot="R180" xref="yes" grouprefs="PROCESSOR"/>
<pinref part="U11" gate="G$1" pin="12"/>
<pinref part="R96" gate="G$1" pin="2"/>
<wire x1="63.5" y1="93.98" x2="58.42" y2="93.98" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<wire x1="63.5" y1="93.98" x2="63.5" y2="63.5" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<junction x="63.5" y="93.98" grouprefs="PROCESSOR"/>
<wire x1="63.5" y1="63.5" x2="66.04" y2="63.5" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<wire x1="45.72" y1="63.5" x2="63.5" y2="63.5" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<junction x="63.5" y="63.5" grouprefs="PROCESSOR"/>
<pinref part="TP1" gate="G$1" pin="PP"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="L1" gate="G$1" pin="1"/>
<wire x1="127" y1="137.16" x2="127" y2="139.7" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<pinref part="U11" gate="G$1" pin="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="SW1" gate="G$1" pin="2"/>
<wire x1="287.02" y1="68.58" x2="287.02" y2="71.12" width="0.1524" layer="91" grouprefs="RESET"/>
<pinref part="R132" gate="G$1" pin="2"/>
<wire x1="287.02" y1="71.12" x2="284.48" y2="71.12" width="0.1524" layer="91" grouprefs="RESET"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<pinref part="R130" gate="G$1" pin="2"/>
<wire x1="271.78" y1="86.36" x2="271.78" y2="88.9" width="0.1524" layer="91" grouprefs="RESET"/>
<pinref part="+3V3" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<wire x1="322.58" y1="73.66" x2="314.96" y2="73.66" width="0.1524" layer="91" grouprefs="PROGRAMMING_HEADER"/>
<wire x1="314.96" y1="73.66" x2="314.96" y2="81.28" width="0.1524" layer="91" grouprefs="PROGRAMMING_HEADER"/>
<pinref part="+3V5" gate="G$1" pin="+3V3"/>
<wire x1="314.96" y1="81.28" x2="314.96" y2="86.36" width="0.1524" layer="91" grouprefs="PROGRAMMING_HEADER"/>
<wire x1="314.96" y1="86.36" x2="314.96" y2="88.9" width="0.1524" layer="91" grouprefs="PROGRAMMING_HEADER"/>
<wire x1="314.96" y1="73.66" x2="302.26" y2="73.66" width="0.1524" layer="91" grouprefs="PROGRAMMING_HEADER"/>
<junction x="314.96" y="73.66" grouprefs="PROGRAMMING_HEADER"/>
<pinref part="C75" gate="G$1" pin="2"/>
<wire x1="302.26" y1="73.66" x2="302.26" y2="71.12" width="0.1524" layer="91" grouprefs="PROGRAMMING_HEADER"/>
<pinref part="R135" gate="G$1" pin="2"/>
<wire x1="330.2" y1="86.36" x2="314.96" y2="86.36" width="0.1524" layer="91" grouprefs="PROGRAMMING_HEADER"/>
<junction x="314.96" y="86.36" grouprefs="PROGRAMMING_HEADER"/>
<pinref part="R136" gate="G$1" pin="2"/>
<wire x1="330.2" y1="81.28" x2="314.96" y2="81.28" width="0.1524" layer="91" grouprefs="PROGRAMMING_HEADER"/>
<junction x="314.96" y="81.28" grouprefs="PROGRAMMING_HEADER"/>
<pinref part="J1" gate="-1" pin="1"/>
</segment>
<segment>
<pinref part="C68" gate="G$1" pin="2"/>
<wire x1="304.8" y1="205.74" x2="309.88" y2="205.74" width="0.1524" layer="91" grouprefs="BIASING_CAPACITORS"/>
<wire x1="309.88" y1="205.74" x2="309.88" y2="203.2" width="0.1524" layer="91" grouprefs="BIASING_CAPACITORS"/>
<pinref part="C66" gate="G$1" pin="2"/>
<wire x1="304.8" y1="203.2" x2="304.8" y2="205.74" width="0.1524" layer="91" grouprefs="BIASING_CAPACITORS"/>
<pinref part="C49" gate="G$1" pin="2"/>
<wire x1="309.88" y1="205.74" x2="320.04" y2="205.74" width="0.1524" layer="91" grouprefs="BIASING_CAPACITORS"/>
<wire x1="320.04" y1="205.74" x2="325.12" y2="205.74" width="0.1524" layer="91" grouprefs="BIASING_CAPACITORS"/>
<wire x1="325.12" y1="205.74" x2="325.12" y2="203.2" width="0.1524" layer="91" grouprefs="BIASING_CAPACITORS"/>
<pinref part="C48" gate="G$1" pin="2"/>
<wire x1="320.04" y1="203.2" x2="320.04" y2="205.74" width="0.1524" layer="91" grouprefs="BIASING_CAPACITORS"/>
<junction x="320.04" y="205.74" grouprefs="BIASING_CAPACITORS"/>
<junction x="309.88" y="205.74" grouprefs="BIASING_CAPACITORS"/>
<pinref part="C56" gate="G$1" pin="2"/>
<wire x1="325.12" y1="205.74" x2="335.28" y2="205.74" width="0.1524" layer="91" grouprefs="BIASING_CAPACITORS"/>
<wire x1="335.28" y1="205.74" x2="340.36" y2="205.74" width="0.1524" layer="91" grouprefs="BIASING_CAPACITORS"/>
<wire x1="340.36" y1="205.74" x2="340.36" y2="203.2" width="0.1524" layer="91" grouprefs="BIASING_CAPACITORS"/>
<pinref part="C55" gate="G$1" pin="2"/>
<wire x1="335.28" y1="203.2" x2="335.28" y2="205.74" width="0.1524" layer="91" grouprefs="BIASING_CAPACITORS"/>
<junction x="335.28" y="205.74" grouprefs="BIASING_CAPACITORS"/>
<junction x="325.12" y="205.74" grouprefs="BIASING_CAPACITORS"/>
<pinref part="C52" gate="G$1" pin="2"/>
<wire x1="337.82" y1="205.74" x2="340.36" y2="205.74" width="0.1524" layer="91" grouprefs="BIASING_CAPACITORS"/>
<wire x1="340.36" y1="205.74" x2="350.52" y2="205.74" width="0.1524" layer="91" grouprefs="BIASING_CAPACITORS"/>
<wire x1="350.52" y1="205.74" x2="355.6" y2="205.74" width="0.1524" layer="91" grouprefs="BIASING_CAPACITORS"/>
<wire x1="355.6" y1="205.74" x2="355.6" y2="203.2" width="0.1524" layer="91" grouprefs="BIASING_CAPACITORS"/>
<pinref part="C53" gate="G$1" pin="2"/>
<wire x1="350.52" y1="203.2" x2="350.52" y2="205.74" width="0.1524" layer="91" grouprefs="BIASING_CAPACITORS"/>
<junction x="350.52" y="205.74" grouprefs="BIASING_CAPACITORS"/>
<junction x="340.36" y="205.74" grouprefs="BIASING_CAPACITORS"/>
<pinref part="C44" gate="G$1" pin="2"/>
<wire x1="279.4" y1="205.74" x2="289.56" y2="205.74" width="0.1524" layer="91" grouprefs="BIASING_CAPACITORS"/>
<wire x1="289.56" y1="205.74" x2="294.64" y2="205.74" width="0.1524" layer="91" grouprefs="BIASING_CAPACITORS"/>
<wire x1="294.64" y1="205.74" x2="294.64" y2="203.2" width="0.1524" layer="91" grouprefs="BIASING_CAPACITORS"/>
<pinref part="C46" gate="G$1" pin="2"/>
<wire x1="289.56" y1="203.2" x2="289.56" y2="205.74" width="0.1524" layer="91" grouprefs="BIASING_CAPACITORS"/>
<junction x="289.56" y="205.74" grouprefs="BIASING_CAPACITORS"/>
<pinref part="C42" gate="G$1" pin="2"/>
<wire x1="264.16" y1="205.74" x2="274.32" y2="205.74" width="0.1524" layer="91" grouprefs="BIASING_CAPACITORS"/>
<wire x1="274.32" y1="205.74" x2="279.4" y2="205.74" width="0.1524" layer="91" grouprefs="BIASING_CAPACITORS"/>
<wire x1="279.4" y1="205.74" x2="279.4" y2="203.2" width="0.1524" layer="91" grouprefs="BIASING_CAPACITORS"/>
<pinref part="C43" gate="G$1" pin="2"/>
<wire x1="274.32" y1="203.2" x2="274.32" y2="205.74" width="0.1524" layer="91" grouprefs="BIASING_CAPACITORS"/>
<junction x="274.32" y="205.74" grouprefs="BIASING_CAPACITORS"/>
<junction x="279.4" y="205.74" grouprefs="BIASING_CAPACITORS"/>
<pinref part="C40" gate="G$1" pin="2"/>
<wire x1="259.08" y1="205.74" x2="264.16" y2="205.74" width="0.1524" layer="91" grouprefs="BIASING_CAPACITORS"/>
<wire x1="264.16" y1="205.74" x2="264.16" y2="203.2" width="0.1524" layer="91" grouprefs="BIASING_CAPACITORS"/>
<pinref part="C47" gate="G$1" pin="2"/>
<wire x1="259.08" y1="203.2" x2="259.08" y2="205.74" width="0.1524" layer="91" grouprefs="BIASING_CAPACITORS"/>
<junction x="264.16" y="205.74" grouprefs="BIASING_CAPACITORS"/>
<wire x1="294.64" y1="205.74" x2="304.8" y2="205.74" width="0.1524" layer="91" grouprefs="BIASING_CAPACITORS"/>
<junction x="294.64" y="205.74" grouprefs="BIASING_CAPACITORS"/>
<junction x="304.8" y="205.74" grouprefs="BIASING_CAPACITORS"/>
<junction x="259.08" y="205.74" grouprefs="BIASING_CAPACITORS"/>
<pinref part="TP26" gate="G$1" pin="PP"/>
<wire x1="243.84" y1="205.74" x2="246.38" y2="205.74" width="0.1524" layer="91" grouprefs="BIASING_CAPACITORS"/>
<wire x1="246.38" y1="205.74" x2="254" y2="205.74" width="0.1524" layer="91" grouprefs="BIASING_CAPACITORS"/>
<wire x1="254" y1="205.74" x2="259.08" y2="205.74" width="0.1524" layer="91" grouprefs="BIASING_CAPACITORS"/>
<wire x1="355.6" y1="205.74" x2="365.76" y2="205.74" width="0.1524" layer="91" grouprefs="BIASING_CAPACITORS"/>
<junction x="355.6" y="205.74" grouprefs="BIASING_CAPACITORS"/>
<pinref part="C60" gate="G$1" pin="2"/>
<wire x1="365.76" y1="203.2" x2="365.76" y2="205.74" width="0.1524" layer="91" grouprefs="BIASING_CAPACITORS"/>
<pinref part="C62" gate="G$1" pin="2"/>
<wire x1="365.76" y1="205.74" x2="370.84" y2="205.74" width="0.1524" layer="91" grouprefs="BIASING_CAPACITORS"/>
<wire x1="370.84" y1="205.74" x2="370.84" y2="203.2" width="0.1524" layer="91" grouprefs="BIASING_CAPACITORS"/>
<junction x="365.76" y="205.74" grouprefs="BIASING_CAPACITORS"/>
<pinref part="+3V9" gate="G$1" pin="+3V3"/>
<wire x1="246.38" y1="213.36" x2="246.38" y2="205.74" width="0.1524" layer="91" grouprefs="BIASING_CAPACITORS"/>
<junction x="246.38" y="205.74" grouprefs="BIASING_CAPACITORS"/>
<pinref part="C45" gate="G$1" pin="2"/>
<wire x1="254" y1="203.2" x2="254" y2="205.74" width="0.1524" layer="91" grouprefs="BIASING_CAPACITORS"/>
<junction x="254" y="205.74" grouprefs="BIASING_CAPACITORS"/>
</segment>
<segment>
<wire x1="180.34" y1="91.44" x2="210.82" y2="91.44" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<label x="210.82" y="91.44" size="1.27" layer="95" xref="yes" grouprefs="PROCESSOR"/>
<pinref part="U11" gate="G$1" pin="63"/>
</segment>
<segment>
<pinref part="U11" gate="G$1" pin="51"/>
<wire x1="180.34" y1="60.96" x2="210.82" y2="60.96" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<label x="210.82" y="60.96" size="1.27" layer="95" xref="yes" grouprefs="PROCESSOR"/>
</segment>
<segment>
<wire x1="137.16" y1="45.72" x2="137.16" y2="30.48" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<label x="137.16" y="30.48" size="1.27" layer="95" rot="R270" xref="yes" grouprefs="PROCESSOR"/>
<pinref part="U11" gate="G$1" pin="39"/>
</segment>
<segment>
<wire x1="114.3" y1="45.72" x2="114.3" y2="30.48" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<label x="114.3" y="30.48" size="1.27" layer="95" rot="R270" xref="yes" grouprefs="PROCESSOR"/>
<pinref part="U11" gate="G$1" pin="30"/>
</segment>
<segment>
<wire x1="88.9" y1="60.96" x2="78.74" y2="60.96" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<label x="45.72" y="60.96" size="1.27" layer="95" rot="R180" xref="yes" grouprefs="PROCESSOR"/>
<pinref part="U11" gate="G$1" pin="25"/>
<pinref part="R96" gate="G$1" pin="1"/>
<wire x1="78.74" y1="60.96" x2="45.72" y2="60.96" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<wire x1="76.2" y1="63.5" x2="78.74" y2="63.5" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<wire x1="78.74" y1="63.5" x2="78.74" y2="60.96" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<junction x="78.74" y="60.96" grouprefs="PROCESSOR"/>
</segment>
<segment>
<wire x1="124.46" y1="137.16" x2="124.46" y2="167.64" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<label x="124.46" y="167.64" size="1.27" layer="95" rot="R90" xref="yes" grouprefs="PROCESSOR"/>
<pinref part="U11" gate="G$1" pin="92"/>
</segment>
<segment>
<wire x1="162.56" y1="137.16" x2="162.56" y2="167.64" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<label x="162.56" y="167.64" size="1.27" layer="95" rot="R90" xref="yes" grouprefs="PROCESSOR"/>
<pinref part="U11" gate="G$1" pin="77"/>
</segment>
<segment>
<pinref part="R111" gate="G$1" pin="1"/>
<wire x1="30.48" y1="152.4" x2="30.48" y2="154.94" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<wire x1="30.48" y1="154.94" x2="35.56" y2="154.94" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<pinref part="R114" gate="G$1" pin="1"/>
<wire x1="35.56" y1="154.94" x2="40.64" y2="154.94" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<wire x1="40.64" y1="154.94" x2="45.72" y2="154.94" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<wire x1="45.72" y1="154.94" x2="45.72" y2="152.4" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<pinref part="R112" gate="G$1" pin="1"/>
<wire x1="35.56" y1="152.4" x2="35.56" y2="154.94" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<junction x="35.56" y="154.94" grouprefs="PROCESSOR"/>
<pinref part="R113" gate="G$1" pin="1"/>
<wire x1="40.64" y1="152.4" x2="40.64" y2="154.94" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<junction x="40.64" y="154.94" grouprefs="PROCESSOR"/>
<wire x1="45.72" y1="154.94" x2="45.72" y2="157.48" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<junction x="45.72" y="154.94" grouprefs="PROCESSOR"/>
<pinref part="+3V4" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="R107" gate="G$1" pin="1"/>
<wire x1="10.16" y1="187.96" x2="10.16" y2="190.5" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<wire x1="10.16" y1="190.5" x2="15.24" y2="190.5" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<pinref part="R110" gate="G$1" pin="1"/>
<wire x1="15.24" y1="190.5" x2="20.32" y2="190.5" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<wire x1="20.32" y1="190.5" x2="25.4" y2="190.5" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<wire x1="25.4" y1="190.5" x2="25.4" y2="187.96" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<pinref part="R108" gate="G$1" pin="1"/>
<wire x1="15.24" y1="187.96" x2="15.24" y2="190.5" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<junction x="15.24" y="190.5" grouprefs="PROCESSOR"/>
<pinref part="R109" gate="G$1" pin="1"/>
<wire x1="20.32" y1="187.96" x2="20.32" y2="190.5" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<junction x="20.32" y="190.5" grouprefs="PROCESSOR"/>
<wire x1="25.4" y1="190.5" x2="25.4" y2="193.04" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<junction x="25.4" y="190.5" grouprefs="PROCESSOR"/>
<pinref part="+3V7" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="R49" gate="G$1" pin="2"/>
<wire x1="279.4" y1="154.94" x2="279.4" y2="162.56" width="0.1524" layer="91" grouprefs="MEMORY"/>
<pinref part="R48" gate="G$1" pin="2"/>
<wire x1="279.4" y1="162.56" x2="284.48" y2="162.56" width="0.1524" layer="91" grouprefs="MEMORY"/>
<wire x1="284.48" y1="162.56" x2="289.56" y2="162.56" width="0.1524" layer="91" grouprefs="MEMORY"/>
<wire x1="289.56" y1="162.56" x2="335.28" y2="162.56" width="0.1524" layer="91" grouprefs="MEMORY"/>
<wire x1="335.28" y1="162.56" x2="340.36" y2="162.56" width="0.1524" layer="91" grouprefs="MEMORY"/>
<wire x1="340.36" y1="162.56" x2="345.44" y2="162.56" width="0.1524" layer="91" grouprefs="MEMORY"/>
<wire x1="345.44" y1="162.56" x2="350.52" y2="162.56" width="0.1524" layer="91" grouprefs="MEMORY"/>
<wire x1="350.52" y1="162.56" x2="350.52" y2="157.48" width="0.1524" layer="91" grouprefs="MEMORY"/>
<pinref part="R47" gate="G$1" pin="2"/>
<wire x1="345.44" y1="157.48" x2="345.44" y2="162.56" width="0.1524" layer="91" grouprefs="MEMORY"/>
<junction x="345.44" y="162.56" grouprefs="MEMORY"/>
<pinref part="R38" gate="G$1" pin="2"/>
<wire x1="340.36" y1="157.48" x2="340.36" y2="162.56" width="0.1524" layer="91" grouprefs="MEMORY"/>
<junction x="340.36" y="162.56" grouprefs="MEMORY"/>
<pinref part="R36" gate="G$1" pin="2"/>
<wire x1="289.56" y1="154.94" x2="289.56" y2="162.56" width="0.1524" layer="91" grouprefs="MEMORY"/>
<junction x="289.56" y="162.56" grouprefs="MEMORY"/>
<pinref part="R37" gate="G$1" pin="2"/>
<wire x1="284.48" y1="154.94" x2="284.48" y2="162.56" width="0.1524" layer="91" grouprefs="MEMORY"/>
<junction x="284.48" y="162.56" grouprefs="MEMORY"/>
<pinref part="U5" gate="G$1" pin="8"/>
<wire x1="314.96" y1="132.08" x2="335.28" y2="132.08" width="0.1524" layer="91" grouprefs="MEMORY"/>
<wire x1="335.28" y1="132.08" x2="335.28" y2="157.48" width="0.1524" layer="91" grouprefs="MEMORY"/>
<junction x="335.28" y="162.56" grouprefs="MEMORY"/>
<pinref part="C20" gate="G$1" pin="2"/>
<wire x1="335.28" y1="157.48" x2="335.28" y2="162.56" width="0.1524" layer="91" grouprefs="MEMORY"/>
<wire x1="330.2" y1="157.48" x2="335.28" y2="157.48" width="0.1524" layer="91" grouprefs="MEMORY"/>
<junction x="335.28" y="157.48" grouprefs="MEMORY"/>
<wire x1="350.52" y1="162.56" x2="363.22" y2="162.56" width="0.1524" layer="91" grouprefs="MEMORY"/>
<junction x="350.52" y="162.56" grouprefs="MEMORY"/>
<pinref part="+3V1" gate="G$1" pin="+3V3"/>
<wire x1="363.22" y1="162.56" x2="363.22" y2="165.1" width="0.1524" layer="91" grouprefs="MEMORY"/>
</segment>
</net>
<net name="SWCLK" class="0">
<segment>
<wire x1="121.92" y1="137.16" x2="121.92" y2="167.64" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<pinref part="U11" gate="G$1" pin="93"/>
<label x="121.92" y="157.48" size="1.778" layer="95" rot="R90" grouprefs="PROCESSOR"/>
<junction x="121.92" y="167.64" grouprefs="PROCESSOR"/>
</segment>
<segment>
<wire x1="353.06" y1="71.12" x2="355.6" y2="71.12" width="0.1524" layer="91" grouprefs="PROGRAMMING_HEADER"/>
<label x="360.68" y="71.12" size="1.27" layer="95" xref="yes" grouprefs="PROGRAMMING_HEADER"/>
<pinref part="R136" gate="G$1" pin="1"/>
<wire x1="355.6" y1="71.12" x2="360.68" y2="71.12" width="0.1524" layer="91" grouprefs="PROGRAMMING_HEADER"/>
<wire x1="340.36" y1="81.28" x2="355.6" y2="81.28" width="0.1524" layer="91" grouprefs="PROGRAMMING_HEADER"/>
<wire x1="355.6" y1="81.28" x2="355.6" y2="71.12" width="0.1524" layer="91" grouprefs="PROGRAMMING_HEADER"/>
<junction x="355.6" y="71.12" grouprefs="PROGRAMMING_HEADER"/>
<pinref part="J1" gate="-9" pin="1"/>
</segment>
</net>
<net name="SWO" class="0">
<segment>
<wire x1="116.84" y1="137.16" x2="116.84" y2="167.64" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<pinref part="U11" gate="G$1" pin="95"/>
<label x="116.84" y="157.48" size="1.778" layer="95" rot="R90" grouprefs="PROCESSOR"/>
<junction x="116.84" y="167.64" grouprefs="PROCESSOR"/>
</segment>
<segment>
<wire x1="353.06" y1="68.58" x2="360.68" y2="68.58" width="0.1524" layer="91" grouprefs="PROGRAMMING_HEADER"/>
<label x="360.68" y="68.58" size="1.27" layer="95" xref="yes" grouprefs="PROGRAMMING_HEADER"/>
<pinref part="J1" gate="-8" pin="1"/>
</segment>
</net>
<net name="SWDIO" class="0">
<segment>
<wire x1="353.06" y1="73.66" x2="358.14" y2="73.66" width="0.1524" layer="91" grouprefs="PROGRAMMING_HEADER"/>
<label x="360.68" y="73.66" size="1.27" layer="95" xref="yes" grouprefs="PROGRAMMING_HEADER"/>
<pinref part="R135" gate="G$1" pin="1"/>
<wire x1="358.14" y1="73.66" x2="360.68" y2="73.66" width="0.1524" layer="91" grouprefs="PROGRAMMING_HEADER"/>
<wire x1="340.36" y1="86.36" x2="358.14" y2="86.36" width="0.1524" layer="91" grouprefs="PROGRAMMING_HEADER"/>
<wire x1="358.14" y1="86.36" x2="358.14" y2="73.66" width="0.1524" layer="91" grouprefs="PROGRAMMING_HEADER"/>
<junction x="358.14" y="73.66" grouprefs="PROGRAMMING_HEADER"/>
<pinref part="J1" gate="-10" pin="1"/>
</segment>
<segment>
<wire x1="119.38" y1="137.16" x2="119.38" y2="167.64" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<pinref part="U11" gate="G$1" pin="94"/>
<label x="119.38" y="157.48" size="1.778" layer="95" rot="R90" grouprefs="PROCESSOR"/>
<junction x="119.38" y="167.64" grouprefs="PROCESSOR"/>
</segment>
</net>
<net name="COM0_SDA" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="26"/>
<wire x1="104.14" y1="45.72" x2="104.14" y2="30.48" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<label x="104.14" y="30.48" size="1.27" layer="95" rot="R270" xref="yes" grouprefs="PROCESSOR"/>
</segment>
</net>
<net name="COM0_SCL" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="27"/>
<wire x1="106.68" y1="45.72" x2="106.68" y2="30.48" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<label x="106.68" y="30.48" size="1.27" layer="95" rot="R270" xref="yes" grouprefs="PROCESSOR"/>
</segment>
</net>
<net name="COM4_MISO" class="0">
<segment>
<label x="129.54" y="30.48" size="1.27" layer="95" rot="R270" xref="yes" grouprefs="PROCESSOR"/>
<pinref part="U11" gate="G$1" pin="36"/>
<wire x1="129.54" y1="45.72" x2="129.54" y2="30.48" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="2"/>
<pinref part="R37" gate="G$1" pin="1"/>
<wire x1="297.18" y1="129.54" x2="284.48" y2="129.54" width="0.1524" layer="91" grouprefs="MEMORY"/>
<wire x1="284.48" y1="129.54" x2="284.48" y2="144.78" width="0.1524" layer="91" grouprefs="MEMORY"/>
<wire x1="284.48" y1="129.54" x2="274.32" y2="129.54" width="0.1524" layer="91" grouprefs="MEMORY"/>
<junction x="284.48" y="129.54" grouprefs="MEMORY"/>
<label x="274.32" y="129.54" size="1.27" layer="95" rot="R180" xref="yes" grouprefs="MEMORY"/>
</segment>
</net>
<net name="COM4_SCK" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="35"/>
<wire x1="127" y1="45.72" x2="127" y2="30.48" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<label x="127" y="30.48" size="1.27" layer="95" rot="R270" xref="yes" grouprefs="PROCESSOR"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="6"/>
<pinref part="R47" gate="G$1" pin="1"/>
<wire x1="314.96" y1="127" x2="345.44" y2="127" width="0.1524" layer="91" grouprefs="MEMORY"/>
<wire x1="345.44" y1="127" x2="345.44" y2="147.32" width="0.1524" layer="91" grouprefs="MEMORY"/>
<wire x1="345.44" y1="127" x2="355.6" y2="127" width="0.1524" layer="91" grouprefs="MEMORY"/>
<junction x="345.44" y="127" grouprefs="MEMORY"/>
<label x="355.6" y="127" size="1.27" layer="95" xref="yes" grouprefs="MEMORY"/>
</segment>
</net>
<net name="COM4_CSN" class="0">
<segment>
<label x="121.92" y="30.48" size="1.27" layer="95" rot="R270" xref="yes" grouprefs="PROCESSOR"/>
<pinref part="U11" gate="G$1" pin="33"/>
<wire x1="121.92" y1="45.72" x2="121.92" y2="30.48" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="1"/>
<pinref part="R36" gate="G$1" pin="1"/>
<wire x1="297.18" y1="132.08" x2="289.56" y2="132.08" width="0.1524" layer="91" grouprefs="MEMORY"/>
<wire x1="289.56" y1="132.08" x2="289.56" y2="144.78" width="0.1524" layer="91" grouprefs="MEMORY"/>
<wire x1="289.56" y1="132.08" x2="274.32" y2="132.08" width="0.1524" layer="91" grouprefs="MEMORY"/>
<junction x="289.56" y="132.08" grouprefs="MEMORY"/>
<label x="274.32" y="132.08" size="1.27" layer="95" rot="R180" xref="yes" grouprefs="MEMORY"/>
</segment>
</net>
<net name="COM7_TXD_OUT" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="42"/>
<wire x1="144.78" y1="45.72" x2="144.78" y2="30.48" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<label x="144.78" y="30.48" size="1.27" layer="95" rot="R270" xref="yes" grouprefs="PROCESSOR"/>
</segment>
</net>
<net name="COM7_RXD_IN" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="43"/>
<wire x1="147.32" y1="45.72" x2="147.32" y2="30.48" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<label x="147.32" y="30.48" size="1.27" layer="95" rot="R270" xref="yes" grouprefs="PROCESSOR"/>
</segment>
</net>
<net name="COM7_EN" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="44"/>
<wire x1="149.86" y1="45.72" x2="149.86" y2="30.48" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<label x="149.86" y="30.48" size="1.27" layer="95" rot="R270" xref="yes" grouprefs="PROCESSOR"/>
</segment>
</net>
<net name="COM_7.3" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="45"/>
<wire x1="152.4" y1="45.72" x2="152.4" y2="30.48" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<pinref part="TP9" gate="G$1" pin="PP"/>
<label x="152.4" y="30.48" size="1.27" layer="95" rot="R90" grouprefs="PROCESSOR"/>
</segment>
</net>
<net name="COM2_SDA" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="46"/>
<wire x1="154.94" y1="45.72" x2="154.94" y2="30.48" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<label x="154.94" y="30.48" size="1.27" layer="95" rot="R270" xref="yes" grouprefs="PROCESSOR"/>
</segment>
</net>
<net name="COM2_SCL" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="47"/>
<wire x1="157.48" y1="45.72" x2="157.48" y2="30.48" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<label x="157.48" y="30.48" size="1.27" layer="95" rot="R270" xref="yes" grouprefs="PROCESSOR"/>
</segment>
</net>
<net name="COM5_TXD_OUT" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="64"/>
<wire x1="180.34" y1="93.98" x2="210.82" y2="93.98" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<label x="210.82" y="93.98" size="1.27" layer="95" xref="yes" grouprefs="PROCESSOR"/>
</segment>
</net>
<net name="COM5_RXD_IN" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="65"/>
<wire x1="180.34" y1="96.52" x2="210.82" y2="96.52" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<label x="210.82" y="96.52" size="1.27" layer="95" xref="yes" grouprefs="PROCESSOR"/>
</segment>
</net>
<net name="XOUT1" class="0">
<segment>
<pinref part="X2" gate="G$1" pin="1"/>
<wire x1="203.2" y1="185.42" x2="200.66" y2="185.42" width="0.1524" layer="91" grouprefs="CRYSTAL_OSC"/>
<pinref part="C71" gate="G$1" pin="2"/>
<wire x1="200.66" y1="185.42" x2="195.58" y2="185.42" width="0.1524" layer="91" grouprefs="CRYSTAL_OSC"/>
<wire x1="200.66" y1="182.88" x2="200.66" y2="185.42" width="0.1524" layer="91" grouprefs="CRYSTAL_OSC"/>
<junction x="200.66" y="185.42" grouprefs="CRYSTAL_OSC"/>
<label x="195.58" y="185.42" size="1.27" layer="95" rot="R180" xref="yes" grouprefs="CRYSTAL_OSC"/>
</segment>
<segment>
<pinref part="U11" gate="G$1" pin="79"/>
<wire x1="157.48" y1="137.16" x2="157.48" y2="167.64" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<label x="157.48" y="157.48" size="1.778" layer="95" rot="R90" grouprefs="PROCESSOR"/>
<junction x="157.48" y="167.64" grouprefs="PROCESSOR"/>
</segment>
</net>
<net name="XIN1" class="0">
<segment>
<pinref part="X2" gate="G$1" pin="2"/>
<wire x1="213.36" y1="185.42" x2="215.9" y2="185.42" width="0.1524" layer="91" grouprefs="CRYSTAL_OSC"/>
<pinref part="C64" gate="G$1" pin="2"/>
<wire x1="215.9" y1="185.42" x2="220.98" y2="185.42" width="0.1524" layer="91" grouprefs="CRYSTAL_OSC"/>
<wire x1="215.9" y1="182.88" x2="215.9" y2="185.42" width="0.1524" layer="91" grouprefs="CRYSTAL_OSC"/>
<junction x="215.9" y="185.42" grouprefs="CRYSTAL_OSC"/>
<label x="220.98" y="185.42" size="1.27" layer="95" xref="yes" grouprefs="CRYSTAL_OSC"/>
</segment>
<segment>
<pinref part="U11" gate="G$1" pin="78"/>
<wire x1="160.02" y1="137.16" x2="160.02" y2="167.64" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<label x="160.02" y="157.48" size="1.778" layer="95" rot="R90" grouprefs="PROCESSOR"/>
<junction x="160.02" y="167.64" grouprefs="PROCESSOR"/>
</segment>
</net>
<net name="COM5_EN" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="66"/>
<wire x1="180.34" y1="99.06" x2="210.82" y2="99.06" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<label x="210.82" y="99.06" size="1.27" layer="95" xref="yes" grouprefs="PROCESSOR"/>
</segment>
</net>
<net name="COM4_MOSI" class="0">
<segment>
<label x="124.46" y="30.48" size="1.27" layer="95" rot="R270" xref="yes" grouprefs="PROCESSOR"/>
<pinref part="U11" gate="G$1" pin="34"/>
<wire x1="124.46" y1="45.72" x2="124.46" y2="30.48" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="5"/>
<pinref part="R48" gate="G$1" pin="1"/>
<wire x1="314.96" y1="124.46" x2="350.52" y2="124.46" width="0.1524" layer="91" grouprefs="MEMORY"/>
<wire x1="350.52" y1="124.46" x2="350.52" y2="147.32" width="0.1524" layer="91" grouprefs="MEMORY"/>
<wire x1="350.52" y1="124.46" x2="355.6" y2="124.46" width="0.1524" layer="91" grouprefs="MEMORY"/>
<junction x="350.52" y="124.46" grouprefs="MEMORY"/>
<label x="355.6" y="124.46" size="1.27" layer="95" xref="yes" grouprefs="MEMORY"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<wire x1="322.58" y1="63.5" x2="320.04" y2="63.5" width="0.1524" layer="91" grouprefs="PROGRAMMING_HEADER"/>
<pinref part="R129" gate="G$1" pin="1"/>
<pinref part="J1" gate="-5" pin="1"/>
</segment>
</net>
<net name="CTRL2" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="86"/>
<wire x1="139.7" y1="137.16" x2="139.7" y2="167.64" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<label x="139.7" y="167.64" size="1.27" layer="95" rot="R90" xref="yes" grouprefs="PROCESSOR"/>
</segment>
</net>
<net name="CTRL1" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="87"/>
<wire x1="137.16" y1="137.16" x2="137.16" y2="167.64" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<label x="137.16" y="167.64" size="1.27" layer="95" rot="R90" xref="yes" grouprefs="PROCESSOR"/>
</segment>
</net>
<net name="CTRL3" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="85"/>
<wire x1="142.24" y1="137.16" x2="142.24" y2="167.64" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<label x="142.24" y="167.64" size="1.27" layer="95" rot="R90" xref="yes" grouprefs="PROCESSOR"/>
</segment>
</net>
<net name="CTRL4" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="84"/>
<wire x1="144.78" y1="137.16" x2="144.78" y2="167.64" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<label x="144.78" y="167.64" size="1.27" layer="95" rot="R90" xref="yes" grouprefs="PROCESSOR"/>
</segment>
</net>
<net name="N$86" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="20"/>
<wire x1="88.9" y1="73.66" x2="55.88" y2="73.66" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<wire x1="55.88" y1="73.66" x2="55.88" y2="40.64" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<pinref part="R87" gate="G$1" pin="1"/>
<wire x1="55.88" y1="40.64" x2="50.8" y2="40.64" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
</segment>
</net>
<net name="N$87" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="19"/>
<wire x1="88.9" y1="76.2" x2="53.34" y2="76.2" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<pinref part="R88" gate="G$1" pin="1"/>
<wire x1="50.8" y1="48.26" x2="53.34" y2="48.26" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<wire x1="53.34" y1="48.26" x2="53.34" y2="76.2" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
</segment>
</net>
<net name="HB1_SPEED" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="14"/>
<wire x1="88.9" y1="88.9" x2="58.42" y2="88.9" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<pinref part="TP16" gate="G$1" pin="PP"/>
<label x="66.04" y="88.9" size="1.778" layer="95" grouprefs="PROCESSOR"/>
</segment>
</net>
<net name="HB1_DIR" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="13"/>
<wire x1="88.9" y1="91.44" x2="58.42" y2="91.44" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<pinref part="TP17" gate="G$1" pin="PP"/>
<label x="66.04" y="91.44" size="1.778" layer="95" grouprefs="PROCESSOR"/>
</segment>
</net>
<net name="COM6_RXD_IN" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="57"/>
<wire x1="180.34" y1="76.2" x2="210.82" y2="76.2" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<label x="210.82" y="76.2" size="1.27" layer="95" xref="yes" grouprefs="PROCESSOR"/>
</segment>
</net>
<net name="COM6_EN" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="58"/>
<wire x1="180.34" y1="78.74" x2="210.82" y2="78.74" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<label x="210.82" y="78.74" size="1.27" layer="95" xref="yes" grouprefs="PROCESSOR"/>
</segment>
</net>
<net name="LB2_SPEED" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="59"/>
<wire x1="180.34" y1="81.28" x2="210.82" y2="81.28" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<pinref part="TP13" gate="G$1" pin="PP"/>
<label x="190.5" y="81.28" size="1.778" layer="95" grouprefs="PROCESSOR"/>
</segment>
</net>
<net name="LB2_DIR" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="55"/>
<wire x1="180.34" y1="71.12" x2="210.82" y2="71.12" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<pinref part="TP10" gate="G$1" pin="PP"/>
<label x="190.5" y="71.12" size="1.778" layer="95" grouprefs="PROCESSOR"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="88"/>
<pinref part="R128" gate="G$1" pin="2"/>
<wire x1="134.62" y1="137.16" x2="134.62" y2="147.32" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
</segment>
</net>
<net name="COM6_TXD_OUT" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="56"/>
<wire x1="180.34" y1="73.66" x2="210.82" y2="73.66" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<label x="210.82" y="73.66" size="1.27" layer="95" xref="yes" grouprefs="PROCESSOR"/>
</segment>
</net>
<net name="LB3_DIR" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="67"/>
<wire x1="180.34" y1="101.6" x2="210.82" y2="101.6" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<pinref part="TP24" gate="G$1" pin="PP"/>
<label x="190.5" y="101.6" size="1.778" layer="95" grouprefs="PROCESSOR"/>
</segment>
</net>
<net name="COM1_EN" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="54"/>
<wire x1="180.34" y1="68.58" x2="210.82" y2="68.58" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<label x="210.82" y="68.58" size="1.27" layer="95" xref="yes" grouprefs="PROCESSOR"/>
</segment>
</net>
<net name="COM1_RXD_IN" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="53"/>
<wire x1="180.34" y1="66.04" x2="210.82" y2="66.04" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<label x="210.82" y="66.04" size="1.27" layer="95" xref="yes" grouprefs="PROCESSOR"/>
</segment>
</net>
<net name="COM1_TXD_OUT" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="52"/>
<wire x1="180.34" y1="63.5" x2="210.82" y2="63.5" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<label x="210.82" y="63.5" size="1.27" layer="95" xref="yes" grouprefs="PROCESSOR"/>
</segment>
</net>
<net name="LB3_SPEED" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="71"/>
<wire x1="180.34" y1="111.76" x2="210.82" y2="111.76" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<pinref part="TP23" gate="G$1" pin="PP"/>
<label x="190.5" y="111.76" size="1.778" layer="95" grouprefs="PROCESSOR"/>
</segment>
</net>
<net name="COM3_EN" class="0">
<segment>
<label x="210.82" y="109.22" size="1.27" layer="95" xref="yes" grouprefs="PROCESSOR"/>
<pinref part="U11" gate="G$1" pin="70"/>
<wire x1="180.34" y1="109.22" x2="210.82" y2="109.22" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
</segment>
</net>
<net name="COM3_RXD_IN" class="0">
<segment>
<label x="210.82" y="106.68" size="1.27" layer="95" xref="yes" grouprefs="PROCESSOR"/>
<pinref part="U11" gate="G$1" pin="69"/>
<wire x1="180.34" y1="106.68" x2="210.82" y2="106.68" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
</segment>
</net>
<net name="COM3_TXD_OUT" class="0">
<segment>
<label x="210.82" y="104.14" size="1.27" layer="95" xref="yes" grouprefs="PROCESSOR"/>
<pinref part="U11" gate="G$1" pin="68"/>
<wire x1="180.34" y1="104.14" x2="210.82" y2="104.14" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
</segment>
</net>
<net name="!MEM_WP" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="32"/>
<wire x1="119.38" y1="45.72" x2="119.38" y2="30.48" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<label x="119.38" y="30.48" size="1.27" layer="95" rot="R270" xref="yes" grouprefs="PROCESSOR"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="3"/>
<wire x1="297.18" y1="127" x2="279.4" y2="127" width="0.1524" layer="91" grouprefs="MEMORY"/>
<pinref part="R49" gate="G$1" pin="1"/>
<wire x1="279.4" y1="127" x2="279.4" y2="144.78" width="0.1524" layer="91" grouprefs="MEMORY"/>
<wire x1="279.4" y1="127" x2="274.32" y2="127" width="0.1524" layer="91" grouprefs="MEMORY"/>
<junction x="279.4" y="127" grouprefs="MEMORY"/>
<label x="274.32" y="127" size="1.27" layer="95" rot="R180" xref="yes" grouprefs="MEMORY"/>
</segment>
</net>
<net name="!MEM_HOLD" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="37"/>
<wire x1="132.08" y1="45.72" x2="132.08" y2="30.48" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<label x="132.08" y="30.48" size="1.27" layer="95" rot="R270" xref="yes" grouprefs="PROCESSOR"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="7"/>
<pinref part="R38" gate="G$1" pin="1"/>
<wire x1="314.96" y1="129.54" x2="340.36" y2="129.54" width="0.1524" layer="91" grouprefs="MEMORY"/>
<wire x1="340.36" y1="129.54" x2="340.36" y2="147.32" width="0.1524" layer="91" grouprefs="MEMORY"/>
<wire x1="340.36" y1="129.54" x2="355.6" y2="129.54" width="0.1524" layer="91" grouprefs="MEMORY"/>
<junction x="340.36" y="129.54" grouprefs="MEMORY"/>
<label x="355.6" y="129.54" size="1.27" layer="95" xref="yes" grouprefs="MEMORY"/>
</segment>
</net>
<net name="N$70" class="0">
<segment>
<pinref part="R111" gate="G$1" pin="2"/>
<pinref part="R121" gate="G$1" pin="1"/>
<wire x1="30.48" y1="142.24" x2="30.48" y2="137.16" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<wire x1="30.48" y1="137.16" x2="30.48" y2="124.46" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<wire x1="76.2" y1="137.16" x2="30.48" y2="137.16" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<junction x="30.48" y="137.16" grouprefs="PROCESSOR"/>
<pinref part="U11" gate="G$1" pin="1"/>
<wire x1="88.9" y1="121.92" x2="76.2" y2="121.92" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<wire x1="76.2" y1="121.92" x2="76.2" y2="137.16" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
</segment>
</net>
<net name="N$71" class="0">
<segment>
<pinref part="R112" gate="G$1" pin="2"/>
<pinref part="R122" gate="G$1" pin="1"/>
<wire x1="35.56" y1="142.24" x2="35.56" y2="134.62" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<wire x1="35.56" y1="134.62" x2="35.56" y2="124.46" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<wire x1="73.66" y1="134.62" x2="35.56" y2="134.62" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<junction x="35.56" y="134.62" grouprefs="PROCESSOR"/>
<pinref part="U11" gate="G$1" pin="2"/>
<wire x1="88.9" y1="119.38" x2="73.66" y2="119.38" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<wire x1="73.66" y1="119.38" x2="73.66" y2="134.62" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
</segment>
</net>
<net name="N$74" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="21"/>
<wire x1="88.9" y1="71.12" x2="58.42" y2="71.12" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<pinref part="R86" gate="G$1" pin="1"/>
<wire x1="58.42" y1="71.12" x2="58.42" y2="33.02" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<wire x1="58.42" y1="33.02" x2="50.8" y2="33.02" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
</segment>
</net>
<net name="N$75" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="22"/>
<wire x1="88.9" y1="68.58" x2="60.96" y2="68.58" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<wire x1="60.96" y1="68.58" x2="60.96" y2="25.4" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<pinref part="R85" gate="G$1" pin="1"/>
<wire x1="60.96" y1="25.4" x2="50.8" y2="25.4" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
</segment>
</net>
<net name="N$85" class="0">
<segment>
<pinref part="R88" gate="G$1" pin="2"/>
<wire x1="34.036" y1="48.26" x2="40.64" y2="48.26" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<pinref part="D10" gate="G$1" pin="A"/>
</segment>
</net>
<net name="N$98" class="0">
<segment>
<pinref part="R87" gate="G$1" pin="2"/>
<wire x1="40.64" y1="40.64" x2="34.036" y2="40.64" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<pinref part="D9" gate="G$1" pin="A"/>
</segment>
</net>
<net name="N$100" class="0">
<segment>
<pinref part="R86" gate="G$1" pin="2"/>
<wire x1="40.64" y1="33.02" x2="34.036" y2="33.02" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<pinref part="D8" gate="G$1" pin="A"/>
</segment>
</net>
<net name="N$143" class="0">
<segment>
<pinref part="R85" gate="G$1" pin="2"/>
<wire x1="40.64" y1="25.4" x2="34.036" y2="25.4" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<pinref part="D7" gate="G$1" pin="A"/>
</segment>
</net>
<net name="N$84" class="0">
<segment>
<pinref part="R107" gate="G$1" pin="2"/>
<pinref part="R117" gate="G$1" pin="1"/>
<wire x1="10.16" y1="177.8" x2="10.16" y2="172.72" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<wire x1="10.16" y1="172.72" x2="10.16" y2="160.02" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<wire x1="111.76" y1="172.72" x2="10.16" y2="172.72" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<junction x="10.16" y="172.72" grouprefs="PROCESSOR"/>
<pinref part="U11" gate="G$1" pin="97"/>
<wire x1="111.76" y1="137.16" x2="111.76" y2="172.72" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
</segment>
</net>
<net name="N$151" class="0">
<segment>
<pinref part="R108" gate="G$1" pin="2"/>
<pinref part="R118" gate="G$1" pin="1"/>
<wire x1="15.24" y1="177.8" x2="15.24" y2="170.18" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<wire x1="15.24" y1="170.18" x2="15.24" y2="160.02" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<junction x="15.24" y="170.18" grouprefs="PROCESSOR"/>
<pinref part="U11" gate="G$1" pin="98"/>
<wire x1="109.22" y1="137.16" x2="109.22" y2="170.18" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<wire x1="109.22" y1="170.18" x2="15.24" y2="170.18" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
</segment>
</net>
<net name="N$190" class="0">
<segment>
<pinref part="R109" gate="G$1" pin="2"/>
<pinref part="R119" gate="G$1" pin="1"/>
<wire x1="20.32" y1="177.8" x2="20.32" y2="167.64" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<wire x1="20.32" y1="167.64" x2="20.32" y2="160.02" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<junction x="20.32" y="167.64" grouprefs="PROCESSOR"/>
<pinref part="U11" gate="G$1" pin="99"/>
<wire x1="106.68" y1="137.16" x2="106.68" y2="167.64" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<wire x1="106.68" y1="167.64" x2="20.32" y2="167.64" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
</segment>
</net>
<net name="N$191" class="0">
<segment>
<pinref part="R110" gate="G$1" pin="2"/>
<pinref part="R120" gate="G$1" pin="1"/>
<wire x1="25.4" y1="177.8" x2="25.4" y2="165.1" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<wire x1="25.4" y1="165.1" x2="25.4" y2="160.02" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<junction x="25.4" y="165.1" grouprefs="PROCESSOR"/>
<pinref part="U11" gate="G$1" pin="100"/>
<wire x1="104.14" y1="137.16" x2="104.14" y2="165.1" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<wire x1="104.14" y1="165.1" x2="25.4" y2="165.1" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
</segment>
</net>
<net name="VDDCORE" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="89"/>
<wire x1="132.08" y1="137.16" x2="132.08" y2="152.4" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<wire x1="132.08" y1="152.4" x2="127" y2="152.4" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<pinref part="L1" gate="G$1" pin="2"/>
<wire x1="127" y1="152.4" x2="127" y2="149.86" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<wire x1="132.08" y1="152.4" x2="132.08" y2="167.64" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<label x="132.08" y="167.64" size="1.27" layer="95" rot="R90" xref="yes" grouprefs="PROCESSOR"/>
<junction x="132.08" y="152.4" grouprefs="PROCESSOR"/>
</segment>
<segment>
<pinref part="TP25" gate="G$1" pin="PP"/>
<pinref part="C69" gate="G$1" pin="2"/>
<wire x1="210.82" y1="35.56" x2="215.9" y2="35.56" width="0.1524" layer="91" grouprefs="CORE_BIASING_CAPS"/>
<wire x1="215.9" y1="35.56" x2="220.98" y2="35.56" width="0.1524" layer="91" grouprefs="CORE_BIASING_CAPS"/>
<wire x1="220.98" y1="35.56" x2="226.06" y2="35.56" width="0.1524" layer="91" grouprefs="CORE_BIASING_CAPS"/>
<wire x1="226.06" y1="35.56" x2="226.06" y2="33.02" width="0.1524" layer="91" grouprefs="CORE_BIASING_CAPS"/>
<pinref part="C65" gate="G$1" pin="2"/>
<wire x1="220.98" y1="33.02" x2="220.98" y2="35.56" width="0.1524" layer="91" grouprefs="CORE_BIASING_CAPS"/>
<pinref part="C63" gate="G$1" pin="2"/>
<wire x1="215.9" y1="33.02" x2="215.9" y2="35.56" width="0.1524" layer="91" grouprefs="CORE_BIASING_CAPS"/>
<junction x="215.9" y="35.56" grouprefs="CORE_BIASING_CAPS"/>
<junction x="220.98" y="35.56" grouprefs="CORE_BIASING_CAPS"/>
<wire x1="226.06" y1="35.56" x2="226.06" y2="38.1" width="0.1524" layer="91" grouprefs="CORE_BIASING_CAPS"/>
<wire x1="226.06" y1="38.1" x2="210.82" y2="38.1" width="0.1524" layer="91" grouprefs="CORE_BIASING_CAPS"/>
<label x="210.82" y="38.1" size="1.27" layer="95" rot="R180" xref="yes" grouprefs="CORE_BIASING_CAPS"/>
</segment>
</net>
<net name="N$90" class="0">
<segment>
<pinref part="R124" gate="G$1" pin="1"/>
<pinref part="R114" gate="G$1" pin="2"/>
<wire x1="45.72" y1="142.24" x2="45.72" y2="129.54" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<pinref part="U11" gate="G$1" pin="6"/>
<wire x1="45.72" y1="129.54" x2="45.72" y2="124.46" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<wire x1="88.9" y1="109.22" x2="66.04" y2="109.22" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<wire x1="66.04" y1="109.22" x2="66.04" y2="129.54" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<wire x1="66.04" y1="129.54" x2="45.72" y2="129.54" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<junction x="45.72" y="129.54" grouprefs="PROCESSOR"/>
</segment>
</net>
<net name="N$91" class="0">
<segment>
<pinref part="R123" gate="G$1" pin="1"/>
<pinref part="R113" gate="G$1" pin="2"/>
<wire x1="40.64" y1="142.24" x2="40.64" y2="132.08" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<pinref part="U11" gate="G$1" pin="5"/>
<wire x1="40.64" y1="132.08" x2="40.64" y2="124.46" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<wire x1="88.9" y1="111.76" x2="68.58" y2="111.76" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<wire x1="68.58" y1="111.76" x2="68.58" y2="132.08" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<wire x1="68.58" y1="132.08" x2="40.64" y2="132.08" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<junction x="40.64" y="132.08" grouprefs="PROCESSOR"/>
</segment>
</net>
<net name="N$97" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="8"/>
<wire x1="88.9" y1="104.14" x2="58.42" y2="104.14" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<pinref part="TP22" gate="G$1" pin="PP"/>
</segment>
</net>
<net name="N$102" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="7"/>
<wire x1="88.9" y1="106.68" x2="58.42" y2="106.68" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<pinref part="TP21" gate="G$1" pin="PP"/>
<label x="71.12" y="106.68" size="1.778" layer="95" grouprefs="PROCESSOR"/>
</segment>
</net>
<net name="N$101" class="0">
<segment>
<pinref part="TP29" gate="G$1" pin="PP"/>
<pinref part="U11" gate="G$1" pin="3"/>
<wire x1="88.9" y1="116.84" x2="58.42" y2="116.84" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
</segment>
</net>
<net name="N$103" class="0">
<segment>
<pinref part="TP28" gate="G$1" pin="PP"/>
<pinref part="U11" gate="G$1" pin="4"/>
<wire x1="88.9" y1="114.3" x2="58.42" y2="114.3" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
</segment>
</net>
<net name="LB1_SPEED" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="41"/>
<wire x1="142.24" y1="45.72" x2="142.24" y2="17.78" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<pinref part="TP20" gate="G$1" pin="PP"/>
<label x="142.24" y="30.48" size="1.524" layer="95" rot="R90" grouprefs="PROCESSOR"/>
</segment>
</net>
<net name="LB1_DIR" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="40"/>
<wire x1="139.7" y1="45.72" x2="139.7" y2="17.78" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<pinref part="TP19" gate="G$1" pin="PP"/>
<label x="139.7" y="30.48" size="1.524" layer="95" rot="R90" grouprefs="PROCESSOR"/>
</segment>
</net>
<net name="HB3_DIR" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="17"/>
<wire x1="88.9" y1="81.28" x2="58.42" y2="81.28" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<pinref part="TP12" gate="G$1" pin="PP"/>
<label x="66.04" y="81.28" size="1.778" layer="95" grouprefs="PROCESSOR"/>
</segment>
</net>
<net name="HB3_SPEED" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="18"/>
<wire x1="88.9" y1="78.74" x2="58.42" y2="78.74" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<pinref part="TP11" gate="G$1" pin="PP"/>
<label x="66.04" y="78.74" size="1.778" layer="95" grouprefs="PROCESSOR"/>
</segment>
</net>
<net name="HB2_DIR" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="15"/>
<wire x1="88.9" y1="86.36" x2="58.42" y2="86.36" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<pinref part="TP15" gate="G$1" pin="PP"/>
<label x="66.04" y="86.36" size="1.778" layer="95" grouprefs="PROCESSOR"/>
</segment>
</net>
<net name="HB2_SPEED" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="16"/>
<wire x1="88.9" y1="83.82" x2="58.42" y2="83.82" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<pinref part="TP14" gate="G$1" pin="PP"/>
<label x="66.04" y="83.82" size="1.778" layer="95" grouprefs="PROCESSOR"/>
</segment>
</net>
<net name="AMP1801_ALARM" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="75"/>
<wire x1="180.34" y1="121.92" x2="210.82" y2="121.92" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<label x="210.82" y="121.92" size="1.27" layer="95" xref="yes" grouprefs="PROCESSOR"/>
</segment>
</net>
<net name="AMP1801_!RST" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="73"/>
<wire x1="180.34" y1="116.84" x2="210.82" y2="116.84" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<label x="210.82" y="116.84" size="1.27" layer="95" xref="yes" grouprefs="PROCESSOR"/>
</segment>
</net>
<net name="AMP1801_CMS" class="0">
<segment>
<wire x1="210.82" y1="119.38" x2="180.34" y2="119.38" width="0.1524" layer="91" grouprefs="PROCESSOR"/>
<pinref part="U11" gate="G$1" pin="74"/>
<label x="210.82" y="119.38" size="1.27" layer="95" xref="yes" grouprefs="PROCESSOR"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
<note version="9.5" severity="warning">
Since Version 9.5, EAGLE supports persistent groups with
schematics, and board files. Those persistent groups
will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
